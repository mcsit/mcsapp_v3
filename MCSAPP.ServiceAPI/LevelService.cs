﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.IServiceAPI;

namespace MCSAPP.ServiceAPI
{
    public class LevelService : ILevelService
    {
        private ILevelData _levelData;

        public LevelService()
        {
            if (_levelData == null)
                _levelData = ObjDataFactory.GetLevelData();
        }
        public List<LevelModel> GetMasterLevel(SearchModel model)
        {
            try
            {
                return _levelData.GetMasterLevel(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterLevel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<LevelNCRModel> GetMasterLevelNCR(SearchModel model)
        {
            try
            {
                return _levelData.GetMasterLevelNCR(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterLevelNCR", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }


        public List<LevelNGModel> GetMasterLevelNG(SearchModel model)
        {
            try
            {
                return _levelData.GetMasterLevelNG(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterLevelNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<LevelGroupModel> GetMasterLevelGroup(SearchModel model)
        {
            try
            {
                return _levelData.GetMasterLevelGroup(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterLevelGroup", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ProcessModel> GetMasterProcess(SearchModel model)
        {
            try
            {
                return _levelData.GetMasterProcess(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterProcess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<CertModel> GetMasterCert()
        {
            try
            {
                return _levelData.GetMasterCert();
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterCert", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<CertListModel> GetMasterCertList()
        {
            try
            {
                return _levelData.GetMasterCertList();
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterCertList", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetMasterCert(CertModel model)
        {
            try
            {
                return _levelData.SetMasterCert(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterCert", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetMasterGroup(LevelGroupModel model)
        {
            try
            {
                return _levelData.SetMasterGroup(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterGroup", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetMasterLevel(LevelModel model)
        {
            try
            {
                return _levelData.SetMasterLevel(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterLevel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetMasterLevelNCR(LevelNCRModel model)
        {
            try
            {
                return _levelData.SetMasterLevelNCR(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterLevelNCR", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetMasterLevelNG(LevelNGModel model)
        {
            try
            {
                return _levelData.SetMasterLevelNG(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterLevelNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetMasterProcess(ProcessModel model)
        {
            try
            {
                return _levelData.SetMasterProcess(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterProcess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteMasterCert(CertModel model)
        {
            try
            {
                return _levelData.DeleteMasterCert(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterCert", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteMasterGroup(LevelGroupModel model)
        {
            try
            {
                return _levelData.DeleteMasterGroup(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterGroup", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteMasterLevel(LevelModel model)
        {
            try
            {
                return _levelData.DeleteMasterLevel(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterLevel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteMasterLevelNCR(LevelNCRModel model)
        {
            try
            {
                return _levelData.DeleteMasterLevelNCR(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterLevelNCR", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DeleteMasterLevelNG(LevelNGModel model)
        {
            try
            {
                return _levelData.DeleteMasterLevelNG(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterLevelNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DeleteMasterProcess(ProcessModel model)
        {
            try
            {
                return _levelData.DeleteMasterProcess(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterProcess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public DataFilterModel GetPriceByProcess(ProcessModel model)
        {
            try
            {
                return _levelData.GetPriceByProcess(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetPriceByProcess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        #region Built Beam/Box by chaiwud.ta
        // 2018-02-10 by chaiwud.ta 
        public List<LevelBuiltModel> GetMasterBuiltLevel(SearchModel model)
        {
            try
            {
                return _levelData.GetMasterBuiltLevel(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterBuiltLevel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetMasterBuiltLevel(LevelBuiltModel model)
        {
            try
            {
                return _levelData.SetMasterBuiltLevel(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterBuiltLevel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DeleteMasterBuiltLevel(LevelBuiltModel model)
        {
            try
            {
                return _levelData.DeleteMasterBuiltLevel(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterBuiltLevel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        #endregion
    }
}
