﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.IServiceAPI;


namespace MCSAPP.ServiceAPI
{
    public class WeekService : IWeekService
    {
        private IWeekData _weekData;

        public WeekService()
        {
            if (_weekData == null)
                _weekData = ObjDataFactory.GetWeekData();
        }
        public List<DataFilterModel> GetDays(DateModel model)
        {
            try
            {
                return _weekData.GetDays(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDays", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }


        public List<DataFilterModel> GetWeeks(DateModel model)
        {
            try
            {
                return _weekData.GetWeeks(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWeeks", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DataFilterModel GetMaxWeeks(DateModel model)
        {
            try
            {
                return _weekData.GetMaxWeeks(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMaxWeeks", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetHeaderWeek(DateModel model)
        {
            try
            {
                return _weekData.GetHeaderWeek(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetHeaderWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<WeekModel> GetWeekByMonth(DateModel model)
        {
            try
            {
                return _weekData.GetWeekByMonth(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWeekByMonth", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<GoalModel> GetTxnMonthByWeek(DateModel model)
        {
            try
            {
                return _weekData.GetTxnMonthByWeek(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetTxnMonthByWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<GoalSumModel> GetSummaryByWork(DateModel model)
        {
            try
            {
                return _weekData.GetSummaryByWork(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryByWork", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<SararyModel> GetSararyMonthByWeek(DateModel model)
        {
            try
            {
                return _weekData.GetSararyMonthByWeek(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSararyMonthByWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public GoalHeaderModel GetSummaryWorkByCode(DateModel model)
        {
            var result = new GoalHeaderModel();
            try
            {
                var Goal = _weekData.GetSummaryWorkByCode(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryWorkByCode", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<SararyModel> GetSararyMonthByCode(DateModel model)
        {
            try
            {
                return _weekData.GetSararyMonthByCode(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSararyMonthByCode", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<SumSalaryModel> GetSalaryFab(DateModel model)
        {
            try
            {
                return _weekData.GetSalaryFab(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSalaryFab", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DayHeaderModel GetWorkLessGoal(DateModel model)
        {
            var result = new DayHeaderModel();
            try
            {
                var Day = _weekData.GetWorkLessGoal(model);
                var Head = _weekData.GetDays(model);
                if (Day != null)
                {
                    result.DayModel = Day;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWorkLessGoal", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<WorkModel> GetWorkMonth(DateModel model)
        {
            try
            {
                return _weekData.GetWorkMonth(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWorkMonth", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<WorkModel> GetWorkMonthByDay(DateModel model)
        {
            try
            {
                return _weekData.GetWorkMonthByDay(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWorkMonthByDay", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DayHeaderModel GetWorkByDay(DateModel model)
        {
            var result = new DayHeaderModel();
            try
            {
                var Day = _weekData.GetWorkByDay(model);
                var Head = _weekData.GetDays(model);
                if (Day != null)
                {
                    result.DayModel = Day;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWorkByDay", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public WeekHeaderModel GetWorkByWeek(DateModel model)
        {
            var result = new WeekHeaderModel();
            try
            {
                var Week = _weekData.GetWorkByWeek(model);
                var Head = _weekData.GetWeeks(model);
                if (Week != null)
                {
                    result.WeekModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWorkByWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DayHeaderModel GetSalaryByDay(DateModel model)
        {
            var result = new DayHeaderModel();
            try
            {
                var Day = _weekData.GetSalaryByDay(model);
                var Head = _weekData.GetDays(model);
                if (Day != null) {
                    result.DayModel = Day;
                }
                if (Head != null) {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSalaryByDay", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public WeekHeaderModel GetSalaryByWeek(DateModel model)
        {
            var result = new WeekHeaderModel();
            try
            {
                var Week = _weekData.GetSalaryByWeek(model);
                var Head = _weekData.GetWeeks(model);
                if (Week != null)
                {
                    result.WeekModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSalaryByWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DayHeaderModel GetSalaryGroupByDay(DateModel model)
        {
            var result = new DayHeaderModel();
            try
            {
                var Day = _weekData.GetSalaryGroupByDay(model);
                var Head = _weekData.GetDays(model);
                if (Day != null)
                {
                    result.DayModel = Day;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSalaryGroupByDay", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public WeekHeaderModel GetSalaryGroupByWeek(DateModel model)
        {
            var result = new WeekHeaderModel();
            try
            {
                var Week = _weekData.GetSalaryGroupByWeek(model);
                var Head = _weekData.GetWeeks(model);
                if (Week != null)
                {
                    result.WeekModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSalaryGroupByWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DayHeaderModel GetWorkByDayAndCode(DateModel model)
        {
            var result = new DayHeaderModel();
            try
            {
                var Day = _weekData.GetWorkByDayAndCode(model);
                var Head = _weekData.GetDays(model);
                if (Day != null)
                {
                    result.DayModel = Day;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWorkByDayAndCode", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public DayHeaderModel GetSalaryByDayAndCode(DateModel model)
        {
            var result = new DayHeaderModel();
            try
            {
                var Day = _weekData.GetSalaryByDayAndCode(model);
                var Head = _weekData.GetDays(model);
                if (Day != null)
                {
                    result.DayModel = Day;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSalaryByDayAndCode", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReviseModel> GetReviseByDayAndCode(DateModel model)
        {
            try
            {
                return _weekData.GetReviseByDayAndCode(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReviseByDayAndCode", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReviseModel> GetOtherByDayAndCode(DateModel model)
        {
            try
            {
                return _weekData.GetOtherByDayAndCode(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetOtherByDayAndCode", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }


        public NCRHeaderModel GetReportNCR(DateModel model)
        {
            var result = new NCRHeaderModel();
            try
            {
                var Week = _weekData.GetReportNCR(model);
              
                var Head = _weekData.GetHeaderWeek(model);
                if (Week != null)
                {
                    result.NCRModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportNCR", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public NCRHeaderModel GetReportVT(DateModel model)
        {
            var result = new NCRHeaderModel();
            try
            {
                var Week = _weekData.GetReportVT(model);
              
                var Head = _weekData.GetHeaderWeek(model);
                if (Week != null)
                {
                    result.NCRModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportVT", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public GoalHeaderModel GetSummaryWorkDayForWeek(DateModel model)
        {
            var result = new GoalHeaderModel();
            try
            {
                var Goal = _weekData.GetSummaryWorkDayForWeek(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryWorkDayForWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<SalarySummaryModel> GetSummarySalaryDayForWeek(DateModel model)
        {
            try
            {
                return _weekData.GetSummarySalaryDayForWeek(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryDayForWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public GoalHeaderModel GetSummaryWorkDayForWeekLess(DateModel model)
        {
            var result = new GoalHeaderModel();
            try
            {
                var Goal = _weekData.GetSummaryWorkDayForWeekLess(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryWorkDayForWeekLess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public GoalHeaderModel GetSummaryWorkDayForWeekWeldLess(DateModel model)
        {
            var result = new GoalHeaderModel();
            try
            {
                var Goal = _weekData.GetSummaryWorkDayForWeekWeldLess(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryWorkDayForWeekLess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<PlusModel> GetSummarySalaryMinus(DateModel model)
        {
            try
            {
                return _weekData.GetSummarySalaryMinus(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryMinus", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<PlusModel> GetSummarySalaryPlus(DateModel model)
        {
            try
            {
                return _weekData.GetSummarySalaryPlus(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryPlus", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<PlusModel> GetSummarySalaryTotal(DateModel model)
        {
            try
            {
                return _weekData.GetSummarySalaryTotal(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryTotal", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public SummaryWorkHeaderModel GetSummaryWorkDayForWeekWeld(DateModel model)
        {
            var result = new SummaryWorkHeaderModel();
            try
            {
                var Goal = _weekData.GetSummaryWorkDayForWeekWeld(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryWorkDayForWeekWeld", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public SummarySalaryHeaderModel GetSummarySalaryForWeekWeld(DateModel model)
        {
            var result = new SummarySalaryHeaderModel();
            try
            {
                var Goal = _weekData.GetSummarySalaryForWeekWeld(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryForWeekWeld", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public SummarySalaryHeaderModel GetSummarySalaryForWeekWeldSum(DateModel model)
        {
            var result = new SummarySalaryHeaderModel();
            try
            {
                var Goal = _weekData.GetSummarySalaryForWeekWeldSum(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryForWeekWeldSum", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }



        public SummarySalaryHeaderModel GetSummarySalaryForWeekSum(DateModel model)
        {
            var result = new SummarySalaryHeaderModel();
            try
            {
                var Goal = _weekData.GetSummarySalaryForWeekSum(model);
                var Head = _weekData.GetTypeProcess(model);
                if (Goal != null)
                {
                    result.GoalModel = Goal;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }

                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummarySalaryForWeekSum", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
    }
}
