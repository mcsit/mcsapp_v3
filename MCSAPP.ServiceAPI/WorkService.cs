﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.IServiceAPI;
using MCSAPP.ServiceGateway;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.ServiceAPI
{
    public class WorkService : IWorkService
    {
        private IWorkData _workdata;

        public WorkService()
        {
            if (_workdata == null)
                _workdata = ObjDataFactory.GetWorkData();
        }

        public AllSummaryForWorkModel GetSummaryForWorkByAutoGas1(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByAutoGas1(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas1", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForWorkModel GetSummaryForWorkByAutoGas2(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByAutoGas2(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas2", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForWorkModel GetSummaryForWorkByAutoGas3(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByAutoGas3(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas3", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForWorkModel GetSummaryForWorkByTaper(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByTaper(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForWorkModel GetSummaryForWorkByPart(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByPart(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailSummaryForWorkByAutoGas1(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByAutoGas1(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailSummaryForWorkByAutoGas2(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByAutoGas2(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailSummaryForWorkByAutoGas3(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByAutoGas3(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailSummaryForWorkByTaper(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByTaper(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailSummaryForWorkByPart(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByPart(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByTaper", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailForCheckSymbol(DateModel model)
        {
            try
            {
                return _workdata.GetDetailForCheckSymbol(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailForCheckSymbol", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas1(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByAutoGas1(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas1", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas2(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByAutoGas2(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas1", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas3(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByAutoGas3(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas1", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByTaper(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByTaper(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas1", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByPart(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByPart(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByAutoGas1", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllShowSummaryModel GetSummaryForGrinder(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForGrinder(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForGrinder", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public AllShowSummaryModel GetSummaryForCut(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForCut(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForCut", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2

        // Built Beam/Box
        // 2018-01-26 by chaiwud.ta 
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltUpBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltUpBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltUpBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltUpBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltUpBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltUpBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // ** 2018-02-01 chaiwud.ta
        // Saw Beam
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltSawBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltSawBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltSawBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltSawBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltSawBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltSawBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltSawBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // Adjust
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltAdjustBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltAdjustBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltAdjustBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltAdjustBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAdjustBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltAdjustBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltAdjustBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // Drill & Shot Blast
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltDrillShotBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltDrillShotBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltDrillShotBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltDrillShotBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltDrillShotBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByDrillShotBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // ** 2018-02-01 chaiwud.ta

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByOtherBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByOtherBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByOtherBeam(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByOtherBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByOtherBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBeam(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByOtherBeam(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByOtherBeam", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // Other Adjust Box
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByOtherBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByOtherBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByOtherBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByOtherBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByOtherBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByOtherBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByOtherBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        // Other Finishing
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherFinishing(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByOtherFinishing(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByOtherFinishing", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByOtherFinishing(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByOtherFinishing(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByOtherFinishing", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherFinishing(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByOtherFinishing(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByOtherFinishing", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        #endregion

        // Built Box
        // 2018-02-12 by chaiwud.ta
        // FabDiaphragmBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltFabDiaphragmBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltFabDiaphragmBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltFabDiaphragmBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltFabDiaphragmBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltFabDiaphragmBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltFabDiaphragmBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // BuiltUpBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltUpBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltUpBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltUpBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltUpBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltUpBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltUpBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // AutoRootBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltAutoRootBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltAutoRootBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltAutoRootBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltAutoRootBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAutoRootBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltAutoRootBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltAutoRootBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // DrillSesnetBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltDrillSesnetBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltDrillSesnetBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltDrillSesnetBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltDrillSesnetBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltDrillSesnetBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltDrillSesnetBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // GougingRepairBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltGougingRepairBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltGougingRepairBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltGougingRepairBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltGougingRepairBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltGougingRepairBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltGougingRepairBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        // FacingBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForWorkByBuiltFacingBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForWorkByBuiltFacingBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            try
            {
                return _workdata.GetDetailSummaryForWorkByBuiltFacingBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailSummaryForWorkByBuiltFacingBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFacingBox(DateModel model)
        {
            try
            {
                return _workdata.GetSummaryForSalaryByBuiltFacingBox(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSummaryForSalaryByBuiltFacingBox", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        #endregion 
    }
}
