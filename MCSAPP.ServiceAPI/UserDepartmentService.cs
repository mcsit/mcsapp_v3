﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.IServiceAPI;
using MCSAPP.ServiceGateway;

namespace MCSAPP.ServiceAPI
{
    public class UserDepartmentService :IUserDepartmentService
    {
        private IUserDepartmentData _userData;

        public UserDepartmentService()
        {
            if (_userData == null)
                _userData = ObjDataFactory.GetUserDepartmentData();
        }
        public List<DataValueModel> GetMainType(SearchModel model)
        {
            try
            {
                return _userData.GetMainType(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMainType", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetLevelType(SearchModel model)
        {
            try
            {
                return _userData.GetLevelType(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetLevelType", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetMainTypeForProcess(SearchModel model)
        {
            try
            {
                return _userData.GetMainTypeForProcess(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMainTypeForProcess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<DataValueModel> GetHeaderForProcess(SearchModel model)
        {
            try
            {
                return _userData.GetHeaderForProcess(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetHeaderForProcess", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<DepartmentModel> GetGroupName(SearchModel model)
        {
            try
            {
                return _userData.GetGroupName(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetGroupName", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UserModel> GetSearchUser(SearchModel model)
        {
            try
            {
                return _userData.GetSearchUser(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetSearchUser", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public UserAccountModel GetLogin(UserLoginModel model)
        {

            var userLogin = _userData.GetLogin(model);
            if (userLogin.EMP_CODE != null)
            {
                try
                {
                    var userID = new DataFilterModel() { Value = model.Username };
               
                    var accessId = 0;

                    var apiTrackingModel = new APITrackingConfigModel() { };
                    var tokenTracking = ConfigurationToken.GenerateConfigurationTokenCode(apiTrackingModel);
                    var userModel = new UserAccountModel() { UserProfile = userLogin, TrackingToken =tokenTracking,AccessId  =accessId};
                    return userModel;
                }
                catch (UnauthorizedAccessException uex)
                {
                    throw uex;
                }
            }
            else
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetLogin", ERROR_MSG = "Username or Password Invalid." });
                //logRepository.InsertLogAccess(model.IpAddress, model.BrowserName, model.Resolution, model.Device, model.Channel, "N", model.Username);
                //logger.Trace(string.Format("Username: {0} LoginInvalid", model.Username));
                throw new UnauthorizedAccessException("Username or Password Invalid.");
            }
        }


        public List<DataValueModel> GetHeaderWeek(DateModel model)
        {
            try
            {
                return _userData.GetHeaderWeek(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetHeaderWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetProjectForCheckSymbol(DateModel model)
        {
            try
            {
                return _userData.GetProjectForCheckSymbol(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetProjectForCheckSymbol", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetProjectItemForCheckSymbol(DateModel model)
        {
            try
            {
                return _userData.GetProjectItemForCheckSymbol(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetProjectItemForCheckSymbol", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetProjectForCheckSymbolData(DateModel model)
        {
            try
            {
                return _userData.GetProjectForCheckSymbolData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetProjectForCheckSymbolData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DataValueModel> GetProjectItemForCheckSymbolData(DateModel model)
        {
            try
            {
                return _userData.GetProjectItemForCheckSymbolData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetProjectItemForCheckSymbolData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
    }
}
