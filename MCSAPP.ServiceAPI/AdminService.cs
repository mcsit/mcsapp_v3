﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.IServiceAPI;
namespace MCSAPP.ServiceAPI
{
    public  class AdminService : IAdminService
    {
        private IAdminData _adminData;
        private IWeekData _weekData;
        private IUserDepartmentData _userData;
        private ILogData _logData;
        private IWorkData _workData;
        public AdminService()
        {
            if (_adminData == null)
                _adminData = ObjDataFactory.GetAdminData();

            if (_weekData == null)
                _weekData = ObjDataFactory.GetWeekData();

            if (_userData == null)
                _userData = ObjDataFactory.GetUserDepartmentData();


            if (_logData == null)
                _logData = ObjDataFactory.GetLogData();

            if (_workData == null)
                _workData = ObjDataFactory.GetWorkData();
        }


        public long WriteLogError(LogModel model)
        {
            try
            {
                return _logData.WriteLogError(model.PAGE_NAME,model.FUNC_NAME,model.ERROR_MSG);
            }
            catch (Exception e)
            {
                //new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "WriteLogError", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public long WriteLogActivity(LogModel model)
        {
            try
            {
                return _logData.WriteLogActivity(model.PAGE_NAME, model.FUNC_NAME, model.ACTION_MSG,model.USER_UPDATE);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "WriteLogActivity", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnAdjustGoalData(AdjustGoalModel model)
        {
            try
            {
                return _adminData.SetTxnAdjustGoalData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnAdjustGoalData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnNcrData(List<NcrDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model) {
                    if (m.MAIN != -1 || m.TEMPO != -1)
                    {
                        result = _adminData.SetTxnNcrData(m);
                        new AdminService().WriteLogActivity(new LogModel() { PAGE_NAME = "Admin", FUNC_NAME = "SetTxnNcrData", ACTION_MSG = string.Format("GROUP_NAME : {0} , MAIN : {1} , TEMPO : {2}", m.GROUP_NAME, m.MAIN, m.TEMPO), USER_UPDATE = m.USER_UPDATE });
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnNcrData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetNCRDataLog(List<NcrDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetNCRDataLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetNCRDataLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelNCRDetailLog(List<NcrDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelNCRDetailLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelNCRDetailLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnVTData(List<VTDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetTxnVTData(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnVTData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnDimensionData(DimDataModel model)
        {
            try
            {
                return _adminData.SetTxnDimensionData(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnDimensionData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnOtherData(OtherDataModel model)
        {
            try
            {
                return _adminData.SetTxnOtherData(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnOtherData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnMinusData(MinusDataModel model)
        {
            try
            {
                return _adminData.SetTxnMinusData(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnMinusData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetTxnWeekData(WeekDataModel model)
        {
            try
            {
                return _adminData.SetTxnWeekData(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnWeekData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetVTDetail(List<VTListModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetVTDetail(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetVTDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetVTDetailLog(List<VTListModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetVTDetailLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetVTDetailLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetVTDetailUpload(List<VTListModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetVTDetailUpload(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetVTDetailUpload", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelVTDetailLog(List<VTListModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelVTDetailLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelVTDetailLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<VTListModel> GetVTDetailLog(DateModel model)
        {
            try
            {
                return _adminData.GetVTDetailLog(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetVTDetailLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<VTListModel> GetVTDetailAll(DateModel model)
        {
            try
            {
                return _adminData.GetVTDetailAll(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetVTDetailAll", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public NCRDetailHeaderModel GetReportNCRDetail(DateModel model)
        {
            var result = new NCRDetailHeaderModel();
            try
            {
                var Week = _adminData.GetReportNCRDetail(model);

                var Head = _weekData.GetHeaderWeek(model);

                SearchModel sm = new SearchModel();
                sm.ALL = model.ALL;
                sm.DEPT_CODE = model.DEPT_CODE;

                var grp = _userData.GetGroupName(sm);

                if (Week != null)
                {
                    result.NCRModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }
                if (grp != null)
                {
                    result.GroupName = grp;
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportNCRDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public NGDetailHeaderModel GetReportNGDetail(DateModel model)
        {
            var result = new NGDetailHeaderModel();
            try
            {
                var Week = _adminData.GetReportNGDetail(model);

                var Head = _weekData.GetHeaderWeek(model);

                SearchModel sm = new SearchModel();
                sm.ALL = model.ALL;
                sm.DEPT_CODE = model.DEPT_CODE;

                var grp = _userData.GetGroupName(sm);

                if (Week != null)
                {
                    result.NGModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }
                if (grp != null)
                {
                    result.GroupName = grp;
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportNGDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<NcrDataModel> GetNCRLog(DateModel model)
        {
            try
            {
                return _adminData.GetNCRLog(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetNCRLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public VTDetailHeaderModel GetReportVTDetail(DateModel model)
        {
            var result = new VTDetailHeaderModel();
            try
            {
                var Week = _adminData.GetReportVTDetail(model);

                var Head = _weekData.GetHeaderWeek(model);

                SearchModel sm = new SearchModel();
                sm.ALL = model.ALL;
                sm.DEPT_CODE = model.DEPT_CODE;

                var grp = _userData.GetGroupName(sm);

                if (Week != null)
                {
                    result.NCRModel = Week;
                }
                if (Head != null)
                {
                    result.Header = Head;
                }
                if (grp != null)
                {
                    result.GroupName = grp;
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportVTDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public GoalSumModel GetWeekLessForAdjust(DateModel model)
        {
            try
            {
                return _adminData.GetWeekLessForAdjust(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetWeekLessForAdjust", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<GoalSumModel> GetAdjustGoalData(DateModel model)
        {
            try
            {
                return _adminData.GetAdjustGoalData(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetAdjustGoalData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<MonthModel> GetCalendar(DateModel model)
        {
            try
            {
                return _adminData.GetCalendar(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetCalendar", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        //public List<MinusDataModel> GetReportMinusData(ReportSearchModel model)
        //{
        //    try
        //    {
        //        return _adminData.GetReportMinusData(model);
        //    }
        //    catch (Exception e)
        //    {
        //          new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportMinusData", ERROR_MSG = e.Message });
        //        throw new Exception(e.Message);
        //    }
        //}

        //public List<OtherDataModel> GetReportOtherData(ReportSearchModel model)
        //{
        //    try
        //    {
        //        return _adminData.GetReportOtherData(model);
        //    }
        //    catch (Exception e)
        //    {
        //          new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportOtherData", ERROR_MSG = e.Message });
        //        throw new Exception(e.Message);
        //    }
        //}

        public ResultModel SetDesing(List<DesingModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetDesing(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetDesing", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetDesingLog(List<DesingModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetDesingLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetDesingLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelDesingLog(List<DesingModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelDesingLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnAdjustGoalData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<DesingModel> GetDesingLog(DateModel model)
        {
            try
            {
                return _adminData.GetDesingLog(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetTxnAdjustGoalData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadOther(List<OtherModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadOther(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadOtherLog(List<OtherModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadOtherLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadOtherLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadOtherLog(List<OtherModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadOtheLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadOtherLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<OtherModel> GetUploadOtherLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadOtheLog(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadOtherLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadWeek(List<UploadWeekModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadWeek(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadWeekLog(List<UploadWeekModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadWeekLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadWeekLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadWeekLog(List<UploadWeekModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadWeekLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadWeekLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadWeekModel> GetUploadWeekLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadWeekLog(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadWeekLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteAdjustGoal(DeleteModel model)
        {
            try
            {
                return  _adminData.DeleteAdjustGoal(model);
               
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteAdjustGoal", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteDesingChange(DeleteModel model)
        {
            try
            {
                return _adminData.DeleteDesingChange(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteDesingChange", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteDimension(DeleteModel model)
        {
            try
            {
                return _adminData.DeleteDimension(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteDimension", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteMinus(DeleteModel model)
        {
            try
            {
                return _adminData.DeleteMinus(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMinus", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteOther(DeleteModel model)
        {
            try
            {
                return _adminData.DeleteOther(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeletePlus(DeleteModel model)
        {
            try
            {
                return _adminData.DeletePlus(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeletePlus", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteVTData(DeleteModel model)
        {
            try
            {
                return _adminData.DeleteVTData(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteVTData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteWeek(DeleteModel model)
        {
            try
            {
                return _adminData.DeleteWeek(model);

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteWeek", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReportDesignChangeModel> GetReportDesingChangeData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportDesingChangeData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportDesingChangeData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReportDimensionModel> GetReportDimensionData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportDimensionData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportDimensionData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReportMinusModel> GetReportMinusData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportMinusData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportMinusData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReportDimensionModel> GetReportOtherData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportOtherData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportOtherData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<ReportDimensionModel> GetReportPlusData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportPlusData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportPlusData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<VTListModel> GetReportVTData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportVTData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportVTData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadWeekModel> GetReportWeekData(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetReportWeekData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetReportWeekData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadNG(List<NGDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    if (m.NG != -1 )
                    {
                        result = _adminData.SetUploadNG(m);
                        new AdminService().WriteLogActivity(new LogModel() { PAGE_NAME = "Admin", FUNC_NAME = "SetUploadNG", ACTION_MSG = string.Format("GROUP_NAME : {0} , NG : {1} ", m.GROUP_NAME, m.NG), USER_UPDATE = m.USER_UPDATE });
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadNGLog(List<NGDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadNGLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadNGLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadNGLog(List<NGDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadNGLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadNGLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<NGDataModel> GetUploadNGLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadNGLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadNGLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<NGDataModel> GetUploadNG(DateModel model)
        {
            try
            {
                return _adminData.GetUploadNG(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadNG(NGDataModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadNG(model);
               
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }


        public ResultModel UpdateUploadNG(NGDataModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                

                    result = _adminData.UpdateUploadNG(model);
                
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "UpdateUploadNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadActUT(List<UploadActUTModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {      
                    result = _adminData.SetUploadActUT(m);                  
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadActUT", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadActUTLog(List<UploadActUTModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadActUTLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadActUTLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadActUTLog(List<UploadActUTModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadActUTLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadActUTLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadActUT(UploadActUTModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadActUT(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadActUT", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadActUTModel> GetUploadActUTLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadActUTLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadActUTLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadActUTModel> GetUploadActUT(DateModel model)
        {
            try
            {
                return _adminData.GetUploadActUT(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadActUT", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadCutFinising(List<UploadCutFinisingModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadCutFinising(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadCutFinising", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadCutFinisingLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadCutFinisingLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadCutFinisingLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadCutFinisingLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadCutFinising(UploadCutFinisingModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadCutFinising(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadCutFinising", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadCutFinisingModel> GetUploadCutFinisingLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadCutFinisingLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadCutFinisingLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadCutFinisingModel> GetUploadCutFinising(DateModel model)
        {
            try
            {
                return _adminData.GetUploadCutFinising(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadCutFinising", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadGoal(List<UploadGoalModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadGoal(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadGoal", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadGoalLog(List<UploadGoalModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadGoalLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadGoalLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadGoalLog(List<UploadGoalModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadGoalLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadGoalLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadGoal(UploadGoalModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadGoal(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadGoal", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadGoalModel> GetUploadGoalLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadGoalLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadGoalLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadGoalModel> GetUploadGoal(DateModel model)
        {
            try
            {
                return _adminData.GetUploadGoal(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadGoal", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        //New NG
        public ResultModel SetUploadActUTNew(List<UploadActUTModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadActUTNew(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadActUTNew", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadActUTNewLog(List<UploadActUTModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadActUTNewLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadActUTNewLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadActUTNewLog(List<UploadActUTModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadActUTNewLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadActUTNewLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadActUTNew(UploadActUTModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadActUTNew(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadActUTNew", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadActUTModel> GetUploadActUTNewLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadActUTNewLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadActUTNewLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadActUTModel> GetUploadActUTNew(DateModel model)
        {
            try
            {
                return _adminData.GetUploadActUTNew(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadActUTNew", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        // NG+2
        public ResultModel SetUploadNGNew(List<NGDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    if (m.NG != -1)
                    {
                        result = _adminData.SetUploadNGNew(m);
                        new AdminService().WriteLogActivity(new LogModel() { PAGE_NAME = "Admin", FUNC_NAME = "SetUploadNGNew", ACTION_MSG = string.Format("GROUP_NAME : {0} , NG : {1} ", m.GROUP_NAME, m.NG), USER_UPDATE = m.USER_UPDATE });
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadNG", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadNGNewLog(List<NGDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadNGNewLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadNGNewLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadNGNewLog(List<NGDataModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadNGNewLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadNGNewLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadNGNew(NGDataModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadNGNew(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadNGNew", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<NGDataModel> GetUploadNGNewLog(DateModel model)
        {
            try
            {
                return _adminData.GetUploadNGNewLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadNGNewLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<NGDataModel> GetUploadNGNew(DateModel model)
        {
            try
            {
                return _adminData.GetUploadNGNew(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadNGNew", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadTypeModel> GetHeaderExcel(DateModel model)
        {
            try
            {
                return _workData.GetHeaderExcel(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetHeaderExcel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        //End Tab
        public ResultModel SetUploadEndTab(List<UploadEndTabModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _workData.SetUploadEndTab(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadEndTab", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadEndTabLog(List<UploadEndTabModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _workData.SetUploadEndTabLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadEndTabLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadEndTabLog(List<UploadEndTabModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _workData.DelUploadEndTabLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadEndTabLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadEndTab(UploadEndTabModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _workData.DelUploadEndTab(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadEndTab", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadEndTabModel> GetUploadEndTabLog(DateModel model)
        {
            try
            {
                return _workData.GetUploadEndTabLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadEndTabLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadEndTabModel> GetUploadEndTab(DateModel model)
        {
            try
            {
                return _workData.GetUploadEndTab(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadEndTab", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        //Cut Part 

        public ResultModel SetUploadCutPart(List<UploadCutPartModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _workData.SetUploadCutPart(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadCutPart", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetUploadCutPartLog(List<UploadCutPartModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _workData.SetUploadCutPartLog(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadCutPartLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadCutPartLog(List<UploadCutPartModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _workData.DelUploadCutPartLog(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadCutPartLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadCutPart(UploadCutPartModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _workData.DelUploadCutPart(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadCutPart", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadCutPartModel> GetUploadCutPartLog(DateModel model)
        {
            try
            {
                return _workData.GetUploadCutPartLog(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadCutPartLog", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadCutPartModel> GetUploadCutPart(DateModel model)
        {
            try
            {
                return _workData.GetUploadCutPart(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadCutPart", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        //
        public List<ProcessTypeModel> GetMasterProcessType(SearchModel model)
        {
            try
            {
                return _workData.GetMasterProcessType(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetMasterProcessType", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetMasterProcessType(ProcessTypeModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _workData.SetMasterProcessType(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetMasterProcessType", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DeleteMasterProcessType(ProcessTypeModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _workData.DeleteMasterProcessType(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DeleteMasterProcessType", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel SetCheckSymbol(List<PartDatilSummaryForWorkModel> data)
        {
            try
            {
                return _workData.SetCheckSymbol(data);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetCheckSymbol", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelCheckSymbolData(List<DataValueModel> data)
        {
            try
            {
                return _workData.DelCheckSymbolData(data);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelCheckSymbolData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public PartDetailModel GetDetailForCheckSymbolData(DateModel model)
        {
            try
            {
                return _workData.GetDetailForCheckSymbolData(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetDetailForCheckSymbolData", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        // 2018-02-12 by chaiwud.ta
        public ResultModel SetUploadGoalBuilt(List<UploadGoalBuiltModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadGoalBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadGoalBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetUploadGoalLogBuilt(List<UploadGoalBuiltModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadGoalLogBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadGoalLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DelUploadGoalLogBuilt(List<UploadGoalBuiltModel> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadGoalLogBuilt(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadGoalLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DelUploadGoalBuilt(UploadGoalBuiltModel model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadGoalBuilt(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadGoalBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<UploadGoalBuiltModel> GetUploadGoalLogBuilt(DateModel model)
        {
            try
            {
                return _adminData.GetUploadGoalLogBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadGoalLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<UploadGoalBuiltModel> GetUploadGoalBuilt(DateModel model)
        {
            try
            {
                return _adminData.GetUploadGoalBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadGoalBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        // UTRepairCheck
        // 2018-03-01 by chaiwud.ta
        public ResultModel SetUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadUTRepairCheckLogBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadUTRepairCheckLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetUploadUTRepairCheckBuilt(List<UTRepairCheckModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadUTRepairCheckBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadUTRepairCheckBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadUTRepairCheckLogBuilt(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadUTRepairCheckLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadUTRepairCheckBuilt(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadUTRepairCheckBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckLogBuilt(DateModel model)
        {
            try
            {
                return _adminData.GetUploadUTRepairCheckLogBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadUTRepairCheckLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckBuilt(ReportSearchModel model)
        {
            try
            {
                return _adminData.GetUploadUTRepairCheckBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadUTRepairCheckBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        #region UTDataByUser
        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        public ResultModel SetUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadUTDataByUserLogBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadUTDataByUserLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetUploadUTDataByUserBuilt(List<UTDataByUserModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadUTDataByUserBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadUTDataByUserBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadUTDataByUserLogBuilt(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadUTDataByUserLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadUTDataByUserBuilt(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadUTDataByUserBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UTDataByUserModelBuilt> GetUploadUTDataByUserLogBuilt(DateModel model)
        {
            try
            {
                return _adminData.GetUploadUTDataByUserLogBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadUTDataByUserLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<UTDataByUserModelBuilt> GetUploadUTDataByUserBuilt(DateModel model)
        {
            try
            {
                return _adminData.GetUploadUTDataByUserBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadUTDataByUserBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        #endregion

        #region NGDataBuilt
        // NGDataBuilt
        public ResultModel SetUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadNGDataLogBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadNGDataLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetUploadNGDataBuilt(List<NGDataModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadNGDataBuilt(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadUTDataByUserBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadNGDataLogBuilt(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadNGDataLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DelUploadNGDataBuilt(NGDataModelBuilt model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadNGDataBuilt(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadNGDataBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<NGDataModelBuilt> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model)
        {
            try
            {
                return _adminData.GetUploadNGDataLogBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadNGDataLogBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<NGDataModelBuilt> GetUploadNGDataBuilt(ReportSearchBuiltModel model)
        {
            try
            {
                return _adminData.GetUploadNGDataBuilt(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadNGDataBuilt", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        // CamberOther
        public ResultModel SetUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {
                    result = _adminData.SetUploadCamberDataLogOther(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadCamberDataLogOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel SetUploadCamberDataOther(List<CamberDataModelOther> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.SetUploadCamberDataOther(m);
                }
                return result;
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "SetUploadCamberDataOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public ResultModel DelUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            try
            {
                ResultModel result = new ResultModel();
                foreach (var m in model)
                {

                    result = _adminData.DelUploadCamberDataLogOther(m);
                }
                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadCamberDataLogOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public ResultModel DelUploadCamberDataOther(CamberDataModelOther model)
        {
            try
            {
                ResultModel result = new ResultModel();
                result = _adminData.DelUploadCamberDataOther(model);

                return result;

            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "DelUploadCamberDataOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<CamberDataModelOther> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model)
        {
            try
            {
                return _adminData.GetUploadCamberDataLogOther(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadCamberDataLogOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        public List<CamberDataModelOther> GetUploadCamberDataOther(ReportSearchOtherFinishModel model)
        {
            try
            {
                return _adminData.GetUploadCamberDataOther(model);
            }
            catch (Exception e)
            {
                new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetUploadCamberDataOther", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
        #endregion
        #endregion
    }
}
