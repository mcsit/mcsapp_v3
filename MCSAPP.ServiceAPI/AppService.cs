﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.IServiceAPI;

namespace MCSAPP.ServiceAPI
{
    public class AppService : IAppService
    {
        private IAppData _appData;
        public AppService()
        {
            if (_appData == null)
                _appData = ObjDataFactory.GetAppData();

            
        }

        public List<PartDetail2Model> GetPartDetail(DateModel model)
        {
            try
            {
                return _appData.GetPartDetail(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetPartDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<PartDetail2Model> GetOtherDetail(DateModel model)
        {
            try
            {
                return _appData.GetOtherDetail(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetOtherDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<VTListModel> GetVTDetail(DateModel model)
        {
            try
            {
                return _appData.GetVTDetail(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetVTDetail", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<VTListModel> GetVTForAdd(DateModel model)
        {
            try
            {
                return _appData.GetVTForAdd(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetVTForAdd", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }

        public List<UploadTypeModel> GetHeaderExcel(DateModel model)
        {
            try
            {
                return _appData.GetHeaderExcel(model);
            }
            catch (Exception e)
            {
                  new AdminService().WriteLogError(new LogModel() { PAGE_NAME = this.GetType().FullName, FUNC_NAME = "GetHeaderExcel", ERROR_MSG = e.Message });
                throw new Exception(e.Message);
            }
        }
    }
}
