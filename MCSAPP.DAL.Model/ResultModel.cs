﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class ResultModel
    {
        public string CODE { get; set; }
        public string MESSAGE { get; set; }
        public bool SUCCESS { get; set; }

        public ResultModel()
        {
        }
        public ResultModel(DataRow dr)
        {
            CODE = DataConverter.GetString(dr["ACTION_RESULT_CODE"]); 
            MESSAGE = DataConverter.GetString(dr["ACTION_RESULT_MSG"]);
            SUCCESS = DataConverter.GetString(dr["ACTION_RESULT_CODE"]) == "01";
        }

    }
}
