﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class SumSalaryModel
    {
        public int ROW_ID { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public double SALARY { get; set; }
        public double SALARY_POSITION { get; set; }
        public double SALARY_QUALITY { get; set; }
        public double SALARY_SUM { get; set; }

        public Nullable<int> TOTAL_COUNT { get; set; }


        public SumSalaryModel()
        {
        }
        public SumSalaryModel(DataRow dr)
        {
            ROW_ID = DataConverter.GetInteger(dr["ROW_ID"]);
            EMP_CODE = DataConverter.GetString(dr["EMP_CODE"]);
            FULL_NAME = DataConverter.GetString(dr["FULL_NAME"]);

            SALARY = DataConverter.GetDouble(dr["SALARY"]);
            SALARY_POSITION = DataConverter.GetDouble(dr["SALARY_POSITION"]);
            SALARY_QUALITY = DataConverter.GetDouble(dr["SALARY_QUALITY"]);
            SALARY_SUM = DataConverter.GetDouble(dr["SALARY_SUM"]);

            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
        }
    }
}
