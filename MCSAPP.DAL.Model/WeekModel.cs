﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public  class WeekModel
    {
        public int WEEK_SUB { get; set; }
        public string WEEK_NAME { get; set; }

        public WeekModel()
        {
        }
        public WeekModel(DataRow dr)
        {
            WEEK_SUB = DataConverter.GetInteger(dr["WEEK_SUB"]);
            WEEK_NAME = DataConverter.GetString(dr["WEEK_NAME"]); 
        }
    }
}
