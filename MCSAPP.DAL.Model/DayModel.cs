﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class DayModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public Nullable<double> D_01 { get; set; }
        public Nullable<double> D_02 { get; set; }
        public Nullable<double> D_03 { get; set; }
        public Nullable<double> D_04 { get; set; }
        public Nullable<double> D_05 { get; set; }
        public Nullable<double> D_06 { get; set; }
        public Nullable<double> D_07 { get; set; }
        public Nullable<double> D_08 { get; set; }
        public Nullable<double> D_09 { get; set; }
        public Nullable<double> D_10 { get; set; }
        public Nullable<double> D_11 { get; set; }
        public Nullable<double> D_12 { get; set; }
        public Nullable<double> D_13 { get; set; }
        public Nullable<double> D_14 { get; set; }
        public Nullable<double> D_15 { get; set; }
        public Nullable<double> D_16 { get; set; }
        public Nullable<double> D_17 { get; set; }
        public Nullable<double> D_18 { get; set; }
        public Nullable<double> D_19 { get; set; }
        public Nullable<double> D_20 { get; set; }
        public Nullable<double> D_21 { get; set; }
        public Nullable<double> D_22 { get; set; }
        public Nullable<double> D_23 { get; set; }
        public Nullable<double> D_24 { get; set; }
        public Nullable<double> D_25 { get; set; }
        public Nullable<double> D_26 { get; set; }
        public Nullable<double> D_27 { get; set; }
        public Nullable<double> D_28 { get; set; }
        public Nullable<double> D_29 { get; set; }
        public Nullable<double> D_30 { get; set; }
        public Nullable<double> D_31 { get; set; }
        public Nullable<double> SUM_WORK { get; set; }
        public Nullable<double> SUM_GOAL { get; set; }

       
    }
}
