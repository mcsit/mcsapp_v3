﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class WorkModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string PROJECT_NAME { get; set; }
        public int ITEM { get; set; }
        public int ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public double EMP_GOAL { get; set; }
        public double LEVEL_SALARY { get; set; }
        public double LEVEL_QUALITY { get; set; }
        public double LEVEL_PROCEDURE { get; set; }
        public double SALARY { get; set; }
        public double QUALITY { get; set; }
        public double PROCEDURE { get; set; }
        public double SUM_SALARY { get; set; }

        public WorkModel()
        {
        }
        public WorkModel(DataRow dr)
        {
            
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
            PROJECT_NAME = DataConverter.GetString(dr["PROJECT_NAME"]);
            ITEM = DataConverter.GetInteger(dr["ITEM"]);
            ACTUAL = DataConverter.GetInteger(dr["ACTUAL"]);
            ACTUAL_DATE = DataConverter.GetString(dr["ACTUAL_DATE"]);

        }
    }
}
