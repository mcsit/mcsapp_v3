﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class ReviseModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string PROJECT_NAME { get; set; }
        public int ITEM { get; set; }
        public int ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public Nullable<double> PRICE { get; set; }
        public Nullable<double> SALARY { get; set; } 
    }
}
