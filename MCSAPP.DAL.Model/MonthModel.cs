﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class MonthModel
    {
        public int WEEK_SUB { get; set; }
        public string MON { get; set; }
        public string TUE { get; set; }
        public string WED { get; set; }
        public string THU { get; set; }
        public string FRI { get; set; }
        public string SAT { get; set; }
        public string SUN { get; set; }
    }
}
