﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class LevelNCRModel
    {
        public int RUN_NUM { get; set; }
        public int LEVEL_ID { get; set; }
        public int LEVEL_GOAL { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public double MAIN_MIN { get; set; }
        public double MAIN_MAX { get; set; }
        public double TEMPO_MIN { get; set; }
        public double TEMPO_MAX { get; set; }
        public double PRICE_QULITY { get; set; }
        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }

       
    }
    public class LevelNGModel
    {
        public int RUN_NUM { get; set; }
        public int LEVEL_ID { get; set; }
        public int LEVEL_GOAL { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public double NG_MIN { get; set; }
        public double NG_MAX { get; set; }
        public string QUALITY_DESC { get; set; }
        public string PROCEDURE_DESC { get; set; }
        public int QUALITY { get; set; }
        public int PROCEDURE { get; set; }
        public double PRICE_QUALITY{ get; set; }
        public double PRICE_UT { get; set; }
        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }


    }
}
