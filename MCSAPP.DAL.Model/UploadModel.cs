﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class UploadModel
    {
        public string file { get; set; }
        public string fileName { get; set; }
        public string header { get; set; }
        public string month { get; set; }
        public int week { get; set; }
    }

    public class UploadTypeModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
