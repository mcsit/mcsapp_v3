﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class LevelModel
    {
        public int LEVEL_ID { get; set; }
        public string LEVEL_NAME { get; set; }
        public int LEVEL_GOAL { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public double  LEVEL_SALARY { get; set; }
        public double LEVEL_QUALITY { get; set; }
        public double LEVEL_PROCEDURE { get; set; }
        public double SALARY { get; set; }
        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }
        public int TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }

        public LevelModel()
        {
        }
        public LevelModel(DataRow dr)
        {
            LEVEL_ID = DataConverter.GetInteger(dr["LEVEL_ID"]);
            LEVEL_NAME = DataConverter.GetString(dr["LEVEL_NAME"]);
            LEVEL_GOAL = DataConverter.GetInteger(dr["LEVEL_GOAL"]);
            LEVEL_SALARY = DataConverter.GetDouble(dr["LEVEL_SALARY"]);
            LEVEL_QUALITY = DataConverter.GetDouble(dr["LEVEL_QUALITY"]);
            LEVEL_PROCEDURE = DataConverter.GetDouble(dr["LEVEL_PROCEDURE"]);
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
            SALARY = DataConverter.GetDouble(dr["SALARY"]);
            TYPE_NAME = DataConverter.GetString(dr["TYPE_NAME"]);
        }
    }
   
}
