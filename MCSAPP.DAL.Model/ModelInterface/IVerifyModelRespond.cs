﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.DAL.Model.ModelInterface
{
    public interface IVerifyModelRespond<T>
    {
        HttpStatusCode StatusCode { get; set; }

        string Message { get; set; }

        T Data { get; set; }
    }
}
