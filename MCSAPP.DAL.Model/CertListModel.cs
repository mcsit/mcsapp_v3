﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;


namespace MCSAPP.DAL.Model
{
    public class CertListModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public string CERT_NAME { get; set; }
        public string CERT_START { get; set; }
        public string CERT_END { get; set; }

        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }

        public CertListModel()
        {
        }
        public CertListModel(DataRow dr)
        {
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
            EMP_CODE = DataConverter.GetString(dr["EMP_CODE"]);
            FULL_NAME = DataConverter.GetString(dr["FULL_NAME"]);
            DEPT_CODE = DataConverter.GetString(dr["DEPT_CODE"]);
            DEPT_NAME = DataConverter.GetString(dr["DEPT_NAME"]);
            CERT_NAME = DataConverter.GetString(dr["CERT_NAME"]);
            CERT_START = DataConverter.GetString(dr["CERT_START"]);
            CERT_END = DataConverter.GetString(dr["CERT_END"]);

        }
    }
}
