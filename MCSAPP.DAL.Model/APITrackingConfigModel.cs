﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.DAL.Model
{
    public class APITrackingConfigModel
    {
        public APITrackingConfigModel()
        {
            ReadDbConfig = false;
            IsLogIncomming = false;
            WriteTraceLog = true;
            AccessId = 0;
            UserStamp = string.Empty;
        }

        public bool ReadDbConfig { get; set; }

        public bool IsLogIncomming { get; set; }

        public long AccessId { get; set; }

        public string UserStamp { get; set; }

        public bool WriteTraceLog { get; set; }        
    }
}
