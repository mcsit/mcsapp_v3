﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class WeeksModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public Nullable<double> WEEK_1 { get; set; }
        public Nullable<double> WEEK_2 { get; set; }
        public Nullable<double> WEEK_3 { get; set; }
        public Nullable<double> WEEK_4 { get; set; }
        public Nullable<double> WEEK_5 { get; set; }
    }
}
