﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace  MCSAPP.DAL.Model
{
    /// <summary>
    /// Use for filter requried Search value.
    /// </summary>
    public class DataValueModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
