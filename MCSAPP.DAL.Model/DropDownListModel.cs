﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class DropDownListModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
