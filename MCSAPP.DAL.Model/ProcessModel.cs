﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class ProcessModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int PROCESS_ID { get; set; }
        public string PROCESS_NAME { get; set; }
        public int PRICE_UNIT { get; set; }

        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }

        public int MAIN_PROCESS { get; set; }
        public string MAIN_PROCESS_NAME { get; set; }
        public int IS_LEVEL { get; set; }

        public ProcessModel()
        {
        }
        public ProcessModel(DataRow dr)
        {
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
            PROCESS_ID = DataConverter.GetInteger(dr["PROCESS_ID"]);
            PROCESS_NAME = DataConverter.GetString(dr["PROCESS_NAME"]);
            PRICE_UNIT = DataConverter.GetInteger(dr["PRICE_UNIT"]);
            MAIN_PROCESS = DataConverter.GetInteger(dr["MAIN_PROCESS"]);
            IS_LEVEL = DataConverter.GetInteger(dr["IS_LEVEL"]);
        }
    }
}
