﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class PlusModel
    {
        public int ROW_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public double FIX_SALARY { get; set; }
        public double POSITION_SALARY { get; set; }
        public double ALL_SALARY { get; set; }
        public double SUM_SALARY { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }

        public PlusModel()
        {
        }
        
    }
}
