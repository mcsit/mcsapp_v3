﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{

    public class NGDetailModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string DEPT_NAME { get; set; }
        public string NG_1 { get; set; }
        public string NG_2 { get; set; }
        public string NG_3 { get; set; }
        public string NG_4 { get; set; }
        public string NG_5 { get; set; }
        public string NG_6 { get; set; }
  
    }

}
