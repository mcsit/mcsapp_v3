﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class DesingModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string ROW_NUM { get; set; }
        public string GROUP_NAME { get; set; }
        public string NUMBER_DESING { get; set; }
        public string PROJECT { get; set; }
        public int ITEM { get; set; }
        public string DATE { get; set; }
        public string USER_UPDATE { get; set; }
    }
}
