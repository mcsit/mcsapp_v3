﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class UploadWeekModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string ROW_NUM { get; set; }
        public string WEEK_KEY { get; set; }
        public string WEEK_NO { get; set; }
        public string WEEK_DATE { get; set; }

        public string WEEK_YEAR { get; set; }
        public string WEEK_MONTH { get; set; }

        public string USER_UPDATE { get; set; }
    }

    public class UploadGoalModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string LEVEL_NAME { get; set; }
        public string LEVEL_ID { get; set; }
        public int LEVEL_GOAL { get; set; }

        public string USER_UPDATE { get; set; }
    }

    public class UploadActUTModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public double NG { get; set; }
        public string USER_UPDATE { get; set; }
    }
    public class UploadCutFinisingModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public int WEEK_SUB { get; set; }
        public string WEEK_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public double AMOUNT { get; set; }

        public string EMP_CODE_RECIVE { get; set; }
        public string FULL_NAME_RECIVE { get; set; }
        public string DEPT_NAME_RECIVE { get; set; }

        public string USER_UPDATE { get; set; }
    }

    #region Goal Built Beam/Box by chaiwud.ta
    // GoalB
    // 2018-02-12 by chaiwud.ta
    public class UploadGoalBuiltModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string LEVEL_NAME { get; set; }
        public string LEVEL_ID { get; set; }
        public decimal LEVEL_GOAL { get; set; }

        public string USER_UPDATE { get; set; }
    }

    // UTRepairCheck
    // 2018-03-01 by chaiwud.ta
    public class UTRepairCheckModelBuilt
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string PJ_NAME { get; set; }
        public int PD_ITEM { get; set; }
        public int ITEM_NO { get; set; }
        public string BUILT_TYPE { get; set; }
        public string PC_NAME { get; set; }
        public decimal ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string ACTUAL_CHECK { get; set; }
        public string FULLNAME_CHECK { get; set; }
        public string REMARK { get; set; }

        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
    }

    // UTDataByUser
    // 2018-03-15 by chaiwud.ta
    public class UTDataByUserModelBuilt
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public int WEEK_SUB { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string GROUP_NAME { get; set; }
        public decimal UT1 { get; set; }
        public decimal UT2 { get; set; }
        public string WEEK_NAME { get; set; }

        public string USER_UPDATE { get; set; }
        public string UPDATE_DATE { get; set; }
    }


    // 2018-04-10 AutoRoot_LoadNG
    public class NGDataModelBuilt
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }
        public int NO_ORDER { get; set; }

        public string DATE { get; set; }
        public string PROJECT { get; set; }
        public int PD_ITEM { get; set; }
        public string CODE { get; set; }
        public decimal SKIN_PLATE { get; set; }
        public decimal DIAPHRAGM { get; set; }
        public decimal SAW_NG_LENGTH { get; set; }
        public decimal SAW_NG_JOINT { get; set; }
        public decimal SAW_TOTAL { get; set; }
        public string SAW_D { get; set; }
        public string SAW_K { get; set; }
        public string SAW_WELDER { get; set; }
        public decimal AUTOROOT_NG_LENGTH { get; set; }
        public decimal AUTOROOT_NG_JOINT { get; set; }
        public decimal AUTOROOT_TOTAL { get; set; }
        public string AUTOROOT_D { get; set; }
        public string AUTOROOT_K { get; set; }
        public string AUTOROOT_WELDER { get; set; }
        public decimal ESW_NG_LENGTH { get; set; }
        public decimal ESW_NG_JOINT { get; set; }
        public decimal ESW_TOTAL { get; set; }
        public string ESW_D { get; set; }
        public string ESW_X { get; set; }
        public string ESW_SIDE { get; set; }
        public string ESW_WELDER { get; set; }

        public string USER_UPDATE { get; set; }
        public string UPDATE_DATE { get; set; }
    }
    
    // CamberOther
    public class CamberDataModelOther
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string PROJECT { get; set; }
        public int PD_ITEM { get; set; }
        public string CUTTING_PLAN { get; set; }
        public string BUILT_NO { get; set; }
        public string ACTUAL_GROUP { get; set; }
        public string ACTUAL_DATE { get; set; }
       
        public string USER_UPDATE { get; set; }
        public string UPDATE_DATE { get; set; }
    }
    #endregion
}
