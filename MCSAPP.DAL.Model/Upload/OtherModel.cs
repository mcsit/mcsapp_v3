﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class OtherModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string ROW_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public string TYPE_LOAD { get; set; }
        public string FULL_NAME { get; set; }
        public string PROCESS { get; set; }
        public double PRICE { get; set; }
        public int ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string USER_UPDATE { get; set; }
    }
}
