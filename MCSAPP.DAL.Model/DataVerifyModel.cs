﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using  MCSAPP.DAL.Model;
using  MCSAPP.DAL.Model.ModelInterface;

namespace  MCSAPP.DAL.Model
{
    public class DataVerifyModel<T> : IVerifyModelRespond<T> where T : class, new()
    {
        public DataVerifyModel()
        {
            StatusCode = HttpStatusCode.OK;
            Message = string.Empty;
            Data = new T();
        }

        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }
}
