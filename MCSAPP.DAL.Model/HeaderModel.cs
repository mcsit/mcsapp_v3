﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class HeaderModel
    {
    }
    public class DayHeaderModel
    {
        public List<DayModel> DayModel { get; set; }
        public List<DataFilterModel> Header { get; set; }
    }
    public class WeekHeaderModel
    {
        public List<WeeksModel> WeekModel { get; set; }
        public List<DataFilterModel> Header { get; set; }
    }
    public class NCRHeaderModel
    {
        public List<NCRModel> NCRModel { get; set; }
        public List<DataValueModel> Header { get; set; }
    }
    public class GoalHeaderModel
    {
        public List<GoalSumModel> GoalModel { get; set; }
        public List<DataValueModel> Header { get; set; }
    }
    public class SummaryWorkHeaderModel
    {
        public List<SummaryWorkModel> GoalModel { get; set; }
        public List<DataValueModel> Header { get; set; }
    }
    public class SummarySalaryHeaderModel
    {
        public List<SummarySalaryModel> GoalModel { get; set; }
        public List<DataValueModel> Header { get; set; }
    }
    public class NCRDetailHeaderModel
    {
        public List<NCRDetailModel> NCRModel { get; set; }
        public List<DataValueModel> Header { get; set; }
        public List<DepartmentModel> GroupName { get; set; }
    }

    public class NGDetailHeaderModel
    { 
        public List<NGDetailModel> NGModel { get; set; }
        public List<DataValueModel> Header { get; set; }
        public List<DepartmentModel> GroupName { get; set; }
    }
    public class VTDetailHeaderModel
    {
        public List<VTDetailModel> NCRModel { get; set; }
        public List<DataValueModel> Header { get; set; }
        public List<DepartmentModel> GroupName { get; set; }
    }
}
