﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;


namespace MCSAPP.DAL.Model
{
    public class DateModel
    {
        public string MONTH { get; set; }
        public string YEAR { get; set; }
        public string DAY { get; set; }
        public int WEEK { get; set; }
        public string EMP_CODE { get; set; }
        public string SORT { get; set; }
        public int SORT_TYPE { get; set; }
        public int TAKE { get; set; }
        public int SKIP { get; set; }
        public bool DETAIL { get; set; }
        public bool SUM { get; set; }
        public string TYPE_STORE { get; set; }
        public int ALL { get; set; }
        public string DEPT_CODE { get; set; }
        public string SEARCH { get; set; }
        public string USER_UPDATE { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string TYPE_INSERT { get; set; }

        public string TYPE_FILE { get; set; }
        public string TYPE_SHOW { get; set; }

        public bool EDIT { get; set; }


        public string PROCESS_TYPE { get; set; }

        public int PJ_ID { get; set; }
        public string PJ_NAME { get; set; }
        public int PD_ITEM { get; set; }
    }
}
