﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.DAL.Model.Account
{
    public class UserAccountModel
    {
        public string UserId { get; set; }

        public string Username { get; set; }

        public string Role { get; set; }

        public long AccessId { get; set; }

        public UserModel UserProfile { get; set; }

        public string TrackingToken { get; set; }
    }
}
