﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace  MCSAPP.DAL.Model.Account
{
    public class UserLoginModel
    {
        [Required(ErrorMessage = "Username has Required.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password has Required.")]
        public string Password { get; set; }

        public string Resolution { get; set; }

        public string Device { get; set; }

        public string BrowserName { get; set; }

        public string IpAddress { get; set; }

        public string Channel { get; set; }
    }
}
