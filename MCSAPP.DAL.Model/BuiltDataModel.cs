﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class BuiltDataModel
    {
        public string EMP_CODE { get; set; }
    }

    public class ReportSearchBuiltModel
    {
        public string EMP_CODE { get; set; }
        public int PROCESS_ID { get; set; }
        public string FULL_NAME { get; set; }
        public string GROUP { get; set; }
        public string SEARCH { get; set; }

        public string TYPE { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string YEAR { get; set; }
        public string MONTH { get; set; }
        public string SORT { get; set; }
        public int SORT_TYPE { get; set; }
        public int TAKE { get; set; }
        public int SKIP { get; set; }
        public string USER_UPDATE { get; set; }
        public int PROJECT { get; set; }
    }

    public class ReportNGDataModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }
        public int NO_ORDER { get; set; }

        public string DATE { get; set; }
        public string PROJECT { get; set; }
        public int PD_ITEM { get; set; }
        public string CODE { get; set; }
        public decimal SKIN_PLATE { get; set; }
        public decimal DIAPHRAGM { get; set; }
        public decimal SAW_NG_LENGTH { get; set; }
        public decimal SAW_NG_JOINT { get; set; }
        public decimal SAW_TOTAL { get; set; }
        public string SAW_D { get; set; }
        public string SAW_K { get; set; }
        public int SAW_WELDER { get; set; }
        public int AUTOROOT_NG_LENGTH { get; set; }
        public int AUTOROOT_NG_JOINT { get; set; }
        public int AUTOROOT_TOTAL { get; set; }
        public int AUTOROOT_D { get; set; }
        public int AUTOROOT_K { get; set; }
        public int AUTOROOT_WELDER { get; set; }
        public int ESW_NG_LENGTH { get; set; }
        public int ESW_NG_JOINT { get; set; }
        public int ESW_TOTAL { get; set; }
        public int ESW_D { get; set; }
        public int ESW_X { get; set; }
        public int ESW_SIDE { get; set; }
        public int ESW_WELDER { get; set; }

        public string USER_UPDATE { get; set; }
        public string UPDATE_DATE { get; set; }
    }
}
