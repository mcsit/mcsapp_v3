﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class AllShowSummaryModel
    {
        public List<DataValueModel> Header { get; set; }
        public List<ShowSummaryModel> Data { get; set; }
    }

    public class ShowSummaryModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string ROW_TYPE { get; set; }
        public string DATA_1 { get; set; }
        public string DATA_2 { get; set; }
        public string DATA_3 { get; set; }
        public string DATA_4 { get; set; }
        public string DATA_5 { get; set; }
        public string DATA_6 { get; set; }
        public string DATA_7 { get; set; }
        public string DATA_8 { get; set; }
        public string DATA_9 { get; set; }
        public string DATA_10 { get; set; }
    }
}
