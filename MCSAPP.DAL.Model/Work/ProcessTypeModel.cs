﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class ProcessTypeModel
    {

        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_ID { get; set; }
        public int ID { get; set; }
        public string MAIN_PROCESS { get; set; }
        public string PROCESS_NAME { get; set; }
      
        public string USER_UPDATE { get; set; }
        public string TIME_UPDATE { get; set; }
        public string TYPE { get; set; }
    }
}
