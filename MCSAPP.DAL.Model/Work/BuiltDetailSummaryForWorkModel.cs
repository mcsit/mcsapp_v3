﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model.Work
{
    public class BuiltDetailModel
    {
        public List<DataValueModel> Header { get; set; }
        public List<BuiltDetailSummaryForWorkModel> Data { get; set; }
    }
    public class BuiltDetailSummaryForWorkModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }

        public int ID { get; set; }
        public string PJ_NAME { get; set; }
        public int PD_ITEM { get; set; }
        public int ITEM_NO { get; set; }
        public string BUILT_TYPE_NAME { get; set; }
        public string CUTTING_PLAN { get; set; }
        public int BUILT_NO { get; set; }
        public string BUILT_SIZE { get; set; } // for box
        public decimal BUILT_LENGTH { get; set; } // for auto root
        public int HOLE { get; set; }
        public string PLACE { get; set; }
        public decimal BUILT_WEIGHT { get; set; }
        public decimal BUILT_PLAN { get; set; }
        public string PC_NAME { get; set; }
        public string UG_NAME { get; set; }
        public string PA_PLAN_DATE { get; set; }
        public string ACT_USER { get; set; }
        public string FULLNAME { get; set; }
        public string PA_ACTUAL_DATE { get; set; }
        public decimal BUILT_ACTUAL { get; set; }
        public int PA_NO { get; set; }
        public string USER_UPDATE { get; set; }

        // add for other detail
        public string PD_REV { get; set; }
        public string PD_CODE { get; set; }
        public string PD_DWG { get; set; }
        public string PD_BLOCK { get; set; }
    }
}
