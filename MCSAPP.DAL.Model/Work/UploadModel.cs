﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{

    //public class UploadModel
    //{
    //    public string file { get; set; }
    //    public string fileName { get; set; }
    //    public string header { get; set; }
    //    public string month { get; set; }
    //    public int week { get; set; }
    //}
    //public class UploadTypeModel
    //{
    //    public string Text { get; set; }
    //    public string Value { get; set; }
    //    public string Type { get; set; }
    //}
    public class UploadEndTabModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public double PRICE { get; set; }
        public string USER_UPDATE { get; set; }
    }

    public class UploadCutPartModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }
        public int RUN_ID { get; set; }

        public string WEEK_KEY { get; set; }
        public int WEEK_SUB { get; set; }
        public string WEEK_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public double AMOUNT { get; set; }

        public string EMP_CODE_RECIVE { get; set; }
        public string FULL_NAME_RECIVE { get; set; }
        public string DEPT_NAME_RECIVE { get; set; }

        public string USER_UPDATE { get; set; }
    }
}
