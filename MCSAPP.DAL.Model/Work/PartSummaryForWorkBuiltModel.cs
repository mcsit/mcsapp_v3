﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class AllSummaryForWorkBuiltModel
    {
        public List<DataValueModel> Header { get; set; }
        public List<PartSummaryForWorkBuiltModel> Data { get; set; }
    }

    public class PartSummaryForWorkBuiltModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string ROW_TYPE { get; set; }
        public int ROW_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public double EMP_GOAL { get; set; } // add
        public string GOAL_DAY { get; set; }
        public double GOAL_REAL { get; set; }
        public double GOAL_SUM { get; set; }
        public double DATA_1 { get; set; }
        public double DATA_2 { get; set; }
        public double DATA_3 { get; set; }
        public double DATA_4 { get; set; }
        public double DATA_5 { get; set; }
        public double DATA_6 { get; set; }
        public double DATA_7 { get; set; }
        public double DATA_8 { get; set; }
        public double DATA_9 { get; set; }
        public double DATA_10 { get; set; }
        public double DATA_11 { get; set; }
        public double DATA_12 { get; set; }
        public double DATA_13 { get; set; }
        public double DATA_14 { get; set; }
        public double DATA_15 { get; set; }
        public double DATA_16 { get; set; }
        public double DATA_17 { get; set; }
        public double DATA_18 { get; set; }
        public double DATA_19 { get; set; }
        public double DATA_20 { get; set; }
        public double DATA_21 { get; set; }
        public double DATA_22 { get; set; }
        public double DATA_23 { get; set; }
        public double DATA_24 { get; set; }
        public double DATA_25 { get; set; }
        public double DATA_26 { get; set; }
        public double DATA_27 { get; set; }
        public double DATA_28 { get; set; }
        public double DATA_29 { get; set; }
        public double DATA_30 { get; set; }
        public double DATA_31 { get; set; }
        public double DATA_32 { get; set; }
        public double DATA_33 { get; set; }
        public double DATA_34 { get; set; }
        public double DATA_35 { get; set; }
        public double DATA_36 { get; set; }
        public double DATA_37 { get; set; }
        public double DATA_38 { get; set; }
        public double DATA_39 { get; set; }

    }
}
