﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class PartDetailModel
    {
        public List<DataValueModel> Header { get; set; }
        public List<PartDatilSummaryForWorkModel> Data { get; set; }
    }

    public class PartDatilSummaryForWorkModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ROW_NUM { get; set; }

        public int ID { get; set; }
        public string PJ_NAME { get; set; }
        public int PD_ITEM { get; set; }
        public string PD_CODE { get; set; }
        public string SYMBOL { get; set; }
        public int PART_ITEM { get; set; }
        public string PART_SIZE { get; set; }
        public string CUTTING_PLAN { get; set; }  
        public string PROCESS { get; set; }
        public int HOLE { get; set; }
        public decimal TAPER { get; set; }
        public int PLAN_QTY { get; set; }
        public int ACT_QTY { get; set; }
        public decimal ACT_LENGTH { get; set; }
        public string ACT_GROUP { get; set; }
        public string ACTUAL_USER { get; set; }
        public string ACT_NAME { get; set; }
        public string ACT_DATE { get; set; }
        public int ACT_QTY_SAVE { get; set; }
        public string USER_UPDATE { get; set; }

    }
}
