﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class LogModel
    {
       public  string PAGE_NAME { get; set; }

        public string FUNC_NAME { get; set; }

        public string ERROR_MSG { get; set; }

        public string ACTION_MSG { get; set; }

        public string USER_UPDATE { get; set; }
    }
}
