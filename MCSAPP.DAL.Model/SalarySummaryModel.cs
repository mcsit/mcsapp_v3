﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class SalarySummaryModel
    {
        public int ROW_ID { get; set; }
        public string DATES { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public double LEVEL_SALARY { get; set; }
        public double LEVEL_QUALITY { get; set; }
        public double LEVEL_PROCEDURE { get; set; }
        public double SUM_PART { get; set; }
        public int OVER_GOAL { get; set; }
        public double FIX_SALARY { get; set; }
        public double SALARY { get; set; }
        public double SALARY_QUALITY { get; set; }
        public double SALARY_PROCEDURE { get; set; }
        public double OTHER_SALARY { get; set; }
        public double REVISE_SALARY { get; set; }
        public double WELD_SALARY { get; set; }
        public double DIMENSION_SALARY { get; set; }
        public double MAIN { get; set; }
        public double TEMPO { get; set; }
        public int VT_INSPEC { get; set; }
        public int VT_QPC { get; set; }
        public int VT_OTHER { get; set; }
        public int VT_DESIGN_CHANGE { get; set; }
        public double MINUS { get; set; }
        public double PLUS { get; set; }
        public double SUM_SALARY { get; set; }
        public double POSITION { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int PART { get; set; }
        public int OTHER { get; set; }
        public int REVISE { get; set; }
        public int WELD { get; set; }
        public int DIMENSION { get; set; }
        public double MINUS_2 { get; set; }
        public double POSITION_2 { get; set; }
        public double SUM_SALARY_2 { get; set; }
        public SalarySummaryModel()
        {
        }
     
    }
}
