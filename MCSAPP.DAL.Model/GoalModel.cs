﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;


namespace MCSAPP.DAL.Model
{
    public class GoalModel
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_GOAL { get; set; }
        public int D_01 { get; set; }
        public int D_02 { get; set; }
        public int D_03 { get; set; }
        public int D_04 { get; set; }
        public int D_05 { get; set; }
        public int D_06 { get; set; }
        public int D_07 { get; set; }
        public int D_08 { get; set; }
        public int SUM_GOAL { get; set; }
        public int SUM_WORK { get; set; }

        public Nullable<int> TOTAL_COUNT { get; set; }


        public GoalModel()
        {
        }
        public GoalModel(DataRow dr)
        {
            EMP_CODE = DataConverter.GetString(dr["EMP_CODE"]);
            FULL_NAME = DataConverter.GetString(dr["FULL_NAME"]);
            DEPT_NAME = DataConverter.GetString(dr["DEPT_NAME"]);
            EMP_GOAL = DataConverter.GetString(dr["EMP_GOAL"]);
            D_01 = DataConverter.GetInteger(dr["D_01"]);
            D_02 = DataConverter.GetInteger(dr["D_02"]);
            D_03 = DataConverter.GetInteger(dr["D_03"]);
            D_04 = DataConverter.GetInteger(dr["D_04"]);
            D_05 = DataConverter.GetInteger(dr["D_05"]);
            D_06 = DataConverter.GetInteger(dr["D_06"]);
            D_07 = DataConverter.GetInteger(dr["D_07"]);
            D_08 = DataConverter.GetInteger(dr["D_08"]);
            SUM_GOAL = DataConverter.GetInteger(dr["SUM_GOAL"]);
            SUM_WORK = DataConverter.GetInteger(dr["SUM_WORK"]);
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
        }
    }
}
