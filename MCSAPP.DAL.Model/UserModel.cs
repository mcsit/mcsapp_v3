﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class UserModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public string SUP_DEPT_CODE { get; set; }
        public string SUP_DEPT_NAME{ get; set; }
        public string ROLE_NAME { get; set; }
    }
}
