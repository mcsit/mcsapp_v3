﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class DepartmentModel
    {
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public string SUP_DEPT_CODE { get; set; }
        public string SUP_DEPT_NAME { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }

        public DepartmentModel()
        {
        }
        public DepartmentModel(DataRow dr)
        {
            DEPT_CODE = DataConverter.GetString(dr["DEPT_CODE"]);
            DEPT_NAME = DataConverter.GetString(dr["DEPT_NAME"]);
            SUP_DEPT_CODE = DataConverter.GetString(dr["SUP_DEPT_CODE"]);
            SUP_DEPT_NAME = DataConverter.GetString(dr["SUP_DEPT_NAME"]);
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
        }
    }

}
