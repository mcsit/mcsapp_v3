﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;


namespace  MCSAPP.DAL.Model
{
    /// <summary>
    /// Use for filter requried Search value.
    /// </summary>
    public class DataFilterModel
    {
        [Required(ErrorMessage = "Value has Required.")]
        public string Value { get; set; }
    }
}
