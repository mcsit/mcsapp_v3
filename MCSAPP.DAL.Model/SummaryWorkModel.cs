﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class SummaryWorkModel
    {
        public int ROW_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public int GOAL_DAY { get; set; }
        public int DATA_1 { get; set; }
        public int DATA_2 { get; set; }
        public int DATA_3 { get; set; }
        public int DATA_4 { get; set; }
        public int DATA_5 { get; set; }
        public int DATA_6 { get; set; }
        public int DATA_7 { get; set; }
        public int DATA_8 { get; set; }
        public int DATA_9 { get; set; }
        public int DATA_10 { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
       
    }
}
