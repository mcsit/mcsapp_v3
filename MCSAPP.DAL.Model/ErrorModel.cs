﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.DAL.Model
{
    public class ErrorModel
    {
        public string ErrorMessage { get; set; }

        public string InnerException { get; set; }
    }
}
