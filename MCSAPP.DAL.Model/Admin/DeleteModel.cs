﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class DeleteModel
    {
        public int ID { get; set; }
        public string WEEK_YEAR { get; set; }
        public string WEEK_MONTH { get; set; }
        public string USER_UPDATE { get; set; }
    }
}
