﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class ReportDesignChangeModel
    {
        public string ROW_NUM { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ID { get; set; }
        public string PROJECT { get; set; }
        public int ITEM { get; set; }
        public string DWG { get; set; }
        public string WRONG { get; set; }
        public string CHECK_DATE { get; set; }
    }

    public class ReportDimensionModel
    {
        public string ROW_NUM { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ID { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string PROCESS { get; set; }
        public double PRICE { get; set; }
        public double ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
    }

    public class ReportMinusModel
    {
        public string ROW_NUM { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int ID { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string REMARK { get; set; }
        public double PRICE { get; set; }
        public string ACTUAL_DATE { get; set; }
    }


}
