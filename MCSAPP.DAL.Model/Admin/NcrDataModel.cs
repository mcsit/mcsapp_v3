﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class NcrDataModel
    {
        public int ROW_NUM { get; set; }
        public string WEEK_KEY { get; set; }
        public int WEEK_SUB { get; set; }
        public string GROUP_NAME { get; set; }
        public double MAIN { get; set; }
        public double TEMPO { get; set; }
        public string USER_UPDATE { get; set; }

        public Nullable<int> TOTAL_COUNT { get; set; }
    }
    public class NGDataModel
    {
        public string RUN_ID { get; set; }
        public int ROW_NUM { get; set; }
        public string WEEK_KEY { get; set; }
        public int WEEK_SUB { get; set; }
        public string GROUP_NAME { get; set; }
        public string FULL_NAME { get; set; }

        public string EMP_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public double NG { get; set; }
        public double TOTAL_JOINT { get; set; }
        public double JOINT_NG { get; set; }
        public double PERCENT_NG { get; set; }
        public string USER_UPDATE { get; set; }

        public string WEEK_YEAR { get; set; }
        public string WEEK_NAME { get; set; }

        public Nullable<int> TOTAL_COUNT { get; set; }
    }
    public class VTDataModel
    {
        public string WEEK_KEY { get; set; }
        public int WEEK_SUB { get; set; }
        public string GROUP_NAME { get; set; }
        public float VT { get; set; }
        public string USER_UPDATE { get; set; }
    }
    public class DimDataModel
    {
        public string EMP_CODE { get; set; }
        public int PRICE { get; set; }
        public int ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string USER_UPDATE { get; set; }
    }
    public class OtherDataModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string EMP_CODE { get; set; }
        public double PRICE { get; set; }  
        public int ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public int PROCESS { get; set; }
        public string PROCESS_NAME { get; set; }
        public string USER_UPDATE { get; set; }

        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
    }

    public class WeekDataModel
    {
        public string WEEK_YEAR { get; set; }
        public string WEEK_MONTH { get; set; }
        public string WEEK_DAY { get; set; }
        public int WEEK_SUB { get; set; }
        public string WEEK_NO { get; set; }
        public string USER_UPDATE { get; set; }
    }

    public class AdjustGoalModel
    {
        public int ID { get; set; }
        public string WEEK_KEY { get; set; }
        public string EMP_CODE { get; set; }
        public int SUM_PART { get; set; }
        public int CURRENT_GOAL { get; set; }
        public int ADJUST_GOAL { get; set; }
        public int NEW_GOAL { get; set; }
        public string TYPE { get; set; }
        public string USER_UPDATE { get; set; }
        public int WEEK_NO { get; set; }
    }

    public class MinusDataModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string EMP_CODE { get; set; }
        public double PRICE { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string REMARK { get; set; }
        public string USER_UPDATE { get; set; }


        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
    }

    public class ReportSearchModel
    {
        public string EMP_CODE { get; set; }
        public int PROCESS_ID { get; set; }
        public string FULL_NAME { get; set; }
        public string GROUP { get; set; }
        public string SEARCH { get; set; }

        public string TYPE { get; set; }
        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string YEAR { get; set; }
        public string MONTH { get; set; }
        public string SORT { get; set; }
        public int SORT_TYPE { get; set; }
        public int TAKE { get; set; }
        public int SKIP { get; set; }
    }

  


}
