﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class VTListModel
    {
        public int ID { get; set; }
        public string ROW_NUM { get; set; }
        public string TYPE_VT { get; set; }
        public string STATUS { get; set; }
        public string PROJECT { get; set; }
        public int ITEM { get; set; }
        public string REV_NO { get; set; }
        public string CHECK_NO { get; set; }
        public int PART { get; set; }
        public string POINT { get; set; }
        public string PROBLEM { get; set; }
        public string DWG { get; set; }
        public string ACT { get; set; }
        public string WRONG { get; set; }
        public string EDIT { get; set; }
        public string CHECKER { get; set; }
        public string CHECK_DATE { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string TYPE_INSERT { get; set; }
        public string USER_UPDATE { get; set; }
    }
}
