﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public class PartDetail2Model
    {
        public int ROW_NUM { get; set; }
        public int PJ_ID { get; set; }
        public string PJ_NAME { get; set; }
        public int PD_ITEM { get; set; }
        public string REV_NO { get; set; }
        public string PD_CODE { get; set; }
        public string DESIGN_CHANGE { get; set; }
        public double PD_WEIGHT { get; set; }
        public double PD_LENGTH { get; set; }
        public string PC_NAME { get; set; }
        public string UG_NAME { get; set; }
        public string PA_PLAN_DATE { get; set; }
        public int FAB_PLAN { get; set; }
        public int FAB_ACTUAL { get; set; }
        public string ACTUAL_USER { get; set; }
        public string FULLNAME { get; set; }
        public string ACTUAL_DATE { get; set; }
        public int PA_NO { get; set; }

        public Nullable<int> TOTAL_COUNT { get; set; }
    }
}
