﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;


namespace MCSAPP.DAL.Model
{
    public class SararyModel
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_GOAL { get; set; }
        public double D_01 { get; set; }
        public double D_02 { get; set; }
        public double D_03 { get; set; }
        public double D_04 { get; set; }
        public double D_05 { get; set; }
        public double D_06 { get; set; }
        public double D_07 { get; set; }
        public double D_08 { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }


        public SararyModel()
        {
        }
        public SararyModel(DataRow dr)
        {
            EMP_CODE = DataConverter.GetString(dr["EMP_CODE"]);
            FULL_NAME = DataConverter.GetString(dr["FULL_NAME"]);
            DEPT_NAME = DataConverter.GetString(dr["DEPT_NAME"]);
            EMP_GOAL = DataConverter.GetString(dr["EMP_GOAL"]);
            D_01 = DataConverter.GetDouble(dr["D_01"]);
            D_02 = DataConverter.GetDouble(dr["D_02"]);
            D_03 = DataConverter.GetDouble(dr["D_03"]);
            D_04 = DataConverter.GetDouble(dr["D_04"]);
            D_05 = DataConverter.GetDouble(dr["D_05"]);
            D_06 = DataConverter.GetDouble(dr["D_06"]);
            D_07 = DataConverter.GetDouble(dr["D_07"]);
            D_08 = DataConverter.GetDouble(dr["D_08"]);
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
        }
    }
}
