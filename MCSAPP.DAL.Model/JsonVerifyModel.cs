﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model.ModelInterface;

namespace  MCSAPP.DAL.Model
{
    public class JsonVerifyModel : IVerifyModelRespond<string>
    {
        public JsonVerifyModel()
        {
            StatusCode = HttpStatusCode.OK;
            Message = string.Empty;
            Data = string.Empty;
        }

        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; set; }

        public string Data { get; set; }
    }
}
