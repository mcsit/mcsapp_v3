﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    class OtherFinishDataModel
    {
        public string EMP_CODE { get; set; }
    }

    public class ReportSearchOtherFinishModel
    {
        public string EMP_CODE { get; set; }

        public string START_DATE { get; set; }
        public string END_DATE { get; set; }
        public string GROUP { get; set; }
        public int PROJECT { get; set; }
        public int PD_ITEM { get; set; }
        public string BUILT_NO { get; set; }
        public string CUTTING_PLAN { get; set; }
        public string FULL_NAME { get; set; }
        public string SEARCH { get; set; }

        public string TYPE { get; set; }
   
        public string SORT { get; set; }
        public int SORT_TYPE { get; set; }
        public int TAKE { get; set; }
        public int SKIP { get; set; }
        public string USER_UPDATE { get; set; }
    }
}
