﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class CertModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int CERT_ID { get; set; }
        public string CERT_NAME { get; set; }
        public string CERT_SHORT_NAME { get; set; }

        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }

        public CertModel()
        {
        }
        public CertModel(DataRow dr)
        {
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
            CERT_ID = DataConverter.GetInteger(dr["CERT_ID"]);
            CERT_NAME = DataConverter.GetString(dr["CERT_NAME"]);
            CERT_SHORT_NAME = DataConverter.GetString(dr["CERT_SHORT_NAME"]);


        }
    }
}
