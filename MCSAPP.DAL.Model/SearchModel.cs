﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class SearchModel
    {
        public string DEPT_CODE { get; set; }
        public string SUP_DEPT_CODE { get; set; }
        public string SEARCH { get; set; }
        public string SORT { get; set; }
        public int SORT_TYPE { get; set; }
        public int TAKE { get; set; }
        public int SKIP { get; set; }
        public int ALL { get; set; }
        public int TYPE { get; set; }
    }
}
