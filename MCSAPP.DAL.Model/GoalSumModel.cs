﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class GoalSumModel
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public int GOAL_DAY { get; set; }
        public int PART { get; set; }
        public int OTHER { get; set; }
        public int REVISE { get; set; }
        public int WELD { get; set; }
        public int DIMENSION { get; set; }
        public int DEDUCT { get; set; }
        public int OVER_GOAL { get; set; }
        public string ADJUST_GOAL { get; set; }
        public string NEW_GOAL { get; set; }
        public int SUM_PART { get; set; }
        public string CURRENT_GOAL { get; set; }
        public string WEEK_KEY { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public int SUM_GOAL { get; set; }
        public int ID { get; set; }
        public int ROW_NUM { get; set; }
        public int WEEK_NO { get; set; }

        public GoalSumModel()
        {
        }
        public GoalSumModel(DataRow dr)
        {
            EMP_CODE = DataConverter.GetString(dr["EMP_CODE"]);
            FULL_NAME = DataConverter.GetString(dr["FULL_NAME"]);
            DEPT_NAME = DataConverter.GetString(dr["DEPT_NAME"]);
            EMP_GOAL = DataConverter.GetInteger(dr["EMP_GOAL"]);
            PART = DataConverter.GetInteger(dr["PART"]);
            OTHER = DataConverter.GetInteger(dr["OTHER"]);
            REVISE = DataConverter.GetInteger(dr["REVISE"]);
            WELD = DataConverter.GetInteger(dr["WELD"]);
            DIMENSION = DataConverter.GetInteger(dr["DIMENSION"]);
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
            WEEK_NO = DataConverter.GetInteger(dr["WEEK_NO"]);
        }
    }
}
