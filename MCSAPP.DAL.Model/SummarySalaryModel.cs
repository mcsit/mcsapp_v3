﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.DAL.Model
{
    public  class SummarySalaryModel
    {
        public int ROW_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string WORK_TYPE { get; set; }
        public string CERT { get; set; }
        public int EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public double GOAL_DAY { get; set; }
        public double DATA_1 { get; set; }
        public double DATA_2 { get; set; }
        public double DATA_3 { get; set; }
        public double DATA_4 { get; set; }
        public double DATA_5 { get; set; }
        public double DATA_6 { get; set; }
        public double DATA_7 { get; set; }
        public double DATA_8 { get; set; }
        public double DATA_9 { get; set; }
        public double DATA_10 { get; set; }
        public double DATA_11 { get; set; }
        public double DATA_12 { get; set; }
        public double DATA_13 { get; set; }
        public double DATA_14 { get; set; }
        public double DATA_15 { get; set; }
        public double DATA_16 { get; set; }
        public double DATA_17 { get; set; }
        public double DATA_18 { get; set; }
        public double DATA_19 { get; set; }
        public double DATA_20 { get; set; }
        public double DATA_21 { get; set; }
        public double DATA_22 { get; set; }
        public double DATA_23 { get; set; }
        public double DATA_24 { get; set; }
        public double DATA_25 { get; set; }
        public double DATA_26 { get; set; }
        public double DATA_27 { get; set; }
        public double DATA_28 { get; set; }
        public double DATA_29 { get; set; }
        public double DATA_30 { get; set; }
        public double DATA_31 { get; set; }
        public double DATA_32 { get; set; }
        public double DATA_33 { get; set; }
        public double DATA_34 { get; set; }
        public double DATA_35 { get; set; }
        public double DATA_36 { get; set; }
        public double DATA_37 { get; set; }
        public string DATA_38 { get; set; }
        public string DATA_39 { get; set; }
        public string DATA_40 { get; set; }
        public double DATA_41 { get; set; }
        public double DATA_42 { get; set; }
        public double DATA_43 { get; set; }
        public double DATA_44 { get; set; }
        public double DATA_45 { get; set; }
        public double DATA_46 { get; set; }
        public double DATA_47 { get; set; }
        public double DATA_48 { get; set; }
        public double DATA_49 { get; set; }
        public double DATA_50 { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
       
    }
}
