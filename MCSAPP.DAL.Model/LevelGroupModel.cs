﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class LevelGroupModel
    {
        public int LEVEL_ID { get; set; }
        public int GOAL_DAY_GROUP { get; set; }
        public double LEVEL_SALARY { get; set; }
        public double LEVEL_QUALITY { get; set; }
        public double LEVEL_PROCEDURE { get; set; }
        public double MIN_SALARY { get; set; }
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string TYPE_NAME { get; set; }
        public int TYPE_ID { get; set; }
        public string USER_UPDATE { get; set; }
        public string TYPE { get; set; }

        public LevelGroupModel()
        {
        }
        public LevelGroupModel(DataRow dr)
        {
            LEVEL_ID = DataConverter.GetInteger(dr["LEVEL_ID"]);
            GOAL_DAY_GROUP = DataConverter.GetInteger(dr["GOAL_DAY_GROUP"]);
            MIN_SALARY = DataConverter.GetInteger(dr["MIN_SALARY"]);
            LEVEL_SALARY = DataConverter.GetDouble(dr["LEVEL_SALARY"]);
            LEVEL_QUALITY = DataConverter.GetDouble(dr["LEVEL_QUALITY"]);
            LEVEL_PROCEDURE = DataConverter.GetDouble(dr["LEVEL_PROCEDURE"]);
            TOTAL_COUNT = DataConverter.GetIntegerNull(dr["TOTAL_COUNT"]);
        }
    }

}
