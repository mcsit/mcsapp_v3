﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace MCSAPP.DAL.Model
{
    public class NCRModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string DEPT_NAME { get; set; }
        public string WEEK_1 { get; set; }
        public string WEEK_2 { get; set; }
        public string WEEK_3 { get; set; }
        public string WEEK_4 { get; set; }
        public string WEEK_5 { get; set; }
    }
    public class NCRDetailModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string DEPT_NAME { get; set; }
        public string MAIN_1 { get; set; }
        public string MAIN_2 { get; set; }
        public string MAIN_3 { get; set; }
        public string MAIN_4 { get; set; }
        public string MAIN_5 { get; set; }
        public string MAIN_6 { get; set; }
        public string TEMPO_1 { get; set; }
        public string TEMPO_2 { get; set; }
        public string TEMPO_3 { get; set; }
        public string TEMPO_4 { get; set; }
        public string TEMPO_5 { get; set; }
        public string TEMPO_6 { get; set; }
    }
    public class VTDetailModel
    {
        public Nullable<int> TOTAL_COUNT { get; set; }
        public string DEPT_NAME { get; set; }
        public string INSPEC_1 { get; set; }
        public string INSPEC_2 { get; set; }
        public string INSPEC_3 { get; set; }
        public string INSPEC_4 { get; set; }
        public string INSPEC_5 { get; set; }
        public string INSPEC_6 { get; set; }
        public string QPC_1 { get; set; }
        public string QPC_2 { get; set; }
        public string QPC_3 { get; set; }
        public string QPC_4 { get; set; }
        public string QPC_5 { get; set; }
        public string QPC_6 { get; set; }
        public string OTHER_1 { get; set; }
        public string OTHER_2 { get; set; }
        public string OTHER_3 { get; set; }
        public string OTHER_4 { get; set; }
        public string OTHER_5 { get; set; }
        public string OTHER_6 { get; set; }
        public string DESIGN_1 { get; set; }
        public string DESIGN_2 { get; set; }
        public string DESIGN_3 { get; set; }
        public string DESIGN_4 { get; set; }
        public string DESIGN_5 { get; set; }
        public string DESIGN_6 { get; set; }
    }
}
