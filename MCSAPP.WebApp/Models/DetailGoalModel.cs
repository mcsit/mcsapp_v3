﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailGoalModel
    {
        public DetailGoalModel()
        {
            DetailGoalHistories = new List<DetailGoalHistory>();
            HeaderGoal = new List<HeaderGoal>();
        }

        public List<DetailGoalHistory> DetailGoalHistories { get; set; }
        public List<HeaderGoal> HeaderGoal { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailGoalHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public string D_01 { get; set; }
        public string D_02 { get; set; }
        public string D_03 { get; set; }
        public string D_04 { get; set; }
        public string D_05 { get; set; }
        public string D_06 { get; set; }
        public string D_07 { get; set; }
        public string D_08 { get; set; }
        public string D_09 { get; set; }
        public string D_10 { get; set; }
        public string D_11 { get; set; }
        public string D_12 { get; set; }
        public string D_13 { get; set; }
        public string D_14 { get; set; }
        public string D_15 { get; set; }
        public string D_16 { get; set; }
        public string D_17 { get; set; }
        public string D_18 { get; set; }
        public string D_19 { get; set; }
        public string D_20 { get; set; }
        public string D_21 { get; set; }
        public string D_22 { get; set; }
        public string D_23 { get; set; }
        public string D_24 { get; set; }
        public string D_25 { get; set; }
        public string D_26 { get; set; }
        public string D_27 { get; set; }
        public string D_28 { get; set; }
        public string D_29 { get; set; }
        public string D_30 { get; set; }
        public string D_31 { get; set; }
        public string SUM_GOAL { get; set; }
        public string SUM_WORK { get; set; }

    }
    public class HeaderGoal
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}