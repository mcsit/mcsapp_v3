﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailGoalSumModel
    {
        public DetailGoalSumModel()
        {
            DetailGoalSumHistories = new List<DetailGoalSumHistory>();
            HeaderGoalSum = new List<HeaderGoalSum>();
        }

        public List<DetailGoalSumHistory> DetailGoalSumHistories { get; set; }
        public List<HeaderGoalSum> HeaderGoalSum { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailGoalSumHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public string PART { get; set; }
        public string OTHER { get; set; }
        public string REVISE { get; set; }
        public string WELD { get; set; }
        public string DIMENSION { get; set; }
        public string OVER_GOAL { get; set; }
        public string SUM_GOAL { get; set; }
        public string DEDUCT { get; set; }
    }
    public class HeaderGoalSum
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}