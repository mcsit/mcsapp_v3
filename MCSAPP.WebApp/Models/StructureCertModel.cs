﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureCertModel
    {
        public StructureCertModel()
        {
            CertHistories = new List<CertHistory>();
            HeaderCert = new List<HeaderCert>();
        }

        public List<CertHistory> CertHistories { get; set; }
        public List<HeaderCert> HeaderCert { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class CertHistory
    {
        public int CERT_ID { get; set; }
        public string CERT_NAME { get; set; }
        public string CERT_SHORT_NAME { get; set; }
    }
    public class HeaderCert
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}