﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailSummarySalaryModel
    {
        public DetailSummarySalaryModel()
        {
            DetailSummarySalaryHistories = new List<DetailSummarySalaryHistory>();
            HeaderSummarySalary = new List<HeaderSummarySalary>();
        }

        public List<DetailSummarySalaryHistory> DetailSummarySalaryHistories { get; set; }
        public List<HeaderSummarySalary> HeaderSummarySalary { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailSummarySalaryHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string WORK_TYPE { get; set; }
        public string CERT { get; set; }
        public string EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public string DATA_1 { get; set; }
        public string DATA_2 { get; set; }
        public string DATA_3 { get; set; }
        public string DATA_4 { get; set; }
        public string DATA_5 { get; set; }
        public string DATA_6 { get; set; }
        public string DATA_7 { get; set; }
        public string DATA_8 { get; set; }
        public string DATA_9 { get; set; }
        public string DATA_10 { get; set; }
        public string DATA_11 { get; set; }
        public string DATA_12 { get; set; }
        public string DATA_13 { get; set; }
        public string DATA_14 { get; set; }
        public string DATA_15 { get; set; }
        public string DATA_16 { get; set; }
        public string DATA_17 { get; set; }
        public string DATA_18 { get; set; }
        public string DATA_19 { get; set; }
        public string DATA_20 { get; set; }
        public string DATA_21 { get; set; }
        public string DATA_22 { get; set; }
        public string DATA_23 { get; set; }
        public string DATA_24 { get; set; }
        public string DATA_25 { get; set; }
        public string DATA_26 { get; set; }
        public string DATA_27 { get; set; }
        public string DATA_28 { get; set; }
        public string DATA_29 { get; set; }
        public string DATA_30 { get; set; }
        public string DATA_31 { get; set; }
        public string DATA_32 { get; set; }
        public string DATA_33 { get; set; }
        public string DATA_34 { get; set; }
        public string DATA_35 { get; set; }
        public string DATA_36 { get; set; }
        public string DATA_37 { get; set; }
        public string DATA_38 { get; set; }
        public string DATA_39 { get; set; }
        public string DATA_40 { get; set; }
        public string DATA_41 { get; set; }
        public string DATA_42 { get; set; }
        public string DATA_43 { get; set; }
        public string DATA_44 { get; set; }
        public string DATA_45 { get; set; }
        public string DATA_46 { get; set; }
        public string DATA_47 { get; set; }
        public string DATA_48 { get; set; }
        public string DATA_49 { get; set; }
        public string DATA_50 { get; set; }
    }
    public class HeaderSummarySalary
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}