﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class SearchUserModel
    {
        public SearchUserModel()
        {
            DetailUserHistories = new List<DetailUserHistory>();
            HeaderUser = new List<HeaderUser>();
        }

        public List<DetailUserHistory> DetailUserHistories { get; set; }
        public List<HeaderUser> HeaderUser { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailUserHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string SUP_DEPT_NAME { get; set; }

    }
    public class HeaderUser
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}