﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailNCRModel
    {
        public DetailNCRModel()
        {
            DetailNCRHistories = new List<DetailNCRHistory>();
            HeaderNCR = new List<HeaderNCR>();
        }

        public List<DetailNCRHistory> DetailNCRHistories { get; set; }
        public List<HeaderNCR> HeaderNCR { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailNCRHistory
    {
        public string DEPT_NAME { get; set; }
        public string WEEK_1 { get; set; }
        public string WEEK_2 { get; set; }
        public string WEEK_3 { get; set; }
        public string WEEK_4 { get; set; }
        public string WEEK_5 { get; set; }
        public string ROW_NUM { get; set; }
        public string GROUP_NAME { get; set; }
        public string MAIN { get; set; }
        public string TEMPO { get; set; }
    }
    public class HeaderNCR
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
   
}