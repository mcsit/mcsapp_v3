﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailCutFinisingModel
    {
        public DetailCutFinisingModel()
        {
            DetailCutFinisingHistories = new List<DetailCutFinisingHistory>();
            HeaderCutFinising = new List<HeaderCutFinising>();
        }

        public List<DetailCutFinisingHistory> DetailCutFinisingHistories { get; set; }
        public List<HeaderCutFinising> HeaderCutFinising { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailCutFinisingHistory
    {
       
     
        public string ROW_NUM { get; set; }

        public string WEEK_KEY { get; set; }
        public string WEEK_SUB { get; set; }
        public string WEEK_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_CODE_RECIVE { get; set; }
        public string FULL_NAME_RECIVE { get; set; }
        public string DEPT_NAME_RECIVE { get; set; }
        public string AMOUNT { get; set; }
        public string RUN_ID { get; set; }
    }
    public class HeaderCutFinising
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
   
}