﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureProcessModel
    {
        public StructureProcessModel()
        {
            ProcessHistories = new List<ProcessHistory>();
            HeaderProcess = new List<HeaderProcess>();
        }

        public List<ProcessHistory> ProcessHistories { get; set; }
        public List<HeaderProcess> HeaderProcess { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class ProcessHistory
    {
        public int PROCESS_ID { get; set; }
        public string PROCESS_NAME { get; set; }
        public int PRICE_UNIT { get; set; }
        public string  MAIN_PROCESS_NAME { get; set; }

    }
    public class HeaderProcess
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}