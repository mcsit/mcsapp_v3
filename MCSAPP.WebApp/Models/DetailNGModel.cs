﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailNGModel
    {
        public DetailNGModel()
        {
            DetailNGHistories = new List<DetailNGHistory>();
            HeaderNG = new List<HeaderNG>();
        }

        public List<DetailNGHistory> DetailNGHistories { get; set; }
        public List<HeaderNG> HeaderNG { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailNGHistory
    {
        public string DEPT_NAME { get; set; }
        public string WEEK_1 { get; set; }
        public string WEEK_2 { get; set; }
        public string WEEK_3 { get; set; }
        public string WEEK_4 { get; set; }
        public string WEEK_5 { get; set; }
        public string WEEK_6 { get; set; }
        public string ROW_NUM { get; set; }
        public string GROUP_NAME { get; set; }
        public string NG { get; set; }
        public string WEEK_YEAR { get; set; }
        public string WEEK_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string RUN_ID { get; set; }
    }
    public class HeaderNG
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
   
}