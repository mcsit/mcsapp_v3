﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailWeekModel
    {
        public DetailWeekModel()
        {
            DetailWeekHistories = new List<DetailWeekHistory>();
            HeaderWeek = new List<HeaderWeek>();
        }

        public List<DetailWeekHistory> DetailWeekHistories { get; set; }
        public List<HeaderWeek> HeaderWeek { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailWeekHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public int EMP_GOAL { get; set; }
        public string WEEK_1 { get; set; }
        public string WEEK_2 { get; set; }
        public string WEEK_3 { get; set; }
        public string WEEK_4 { get; set; }
        public string WEEK_5 { get; set; }
    }
    public class HeaderWeek
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}