﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using  MCSAPP.Helper;
using  MCSAPP.Helper.Constant;

namespace  MCSAPP.WebApp.Models
{
    public class UserIdentity : IIdentity
    {
        private readonly string applicationType = SystemConstant.WebApplicationType;
        private readonly bool isAuthenticated;
        private readonly string username;

        public UserIdentity(string username)
        {
            this.username = username;
            isAuthenticated = GlobalHelper.IsNotNull(username);
        }

        public string AuthenticationType
        {
            get
            {
                return applicationType;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return isAuthenticated;
            }
        }

        public string Name
        {
            get
            {
                return username;
            }
        }
    }
}