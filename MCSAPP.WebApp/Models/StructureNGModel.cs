﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureNGModel
    {
        public StructureNGModel()
        {
            LevelNGHistories = new List<LevelNGHistory>();
            HeaderLevelNG = new List<HeaderLevelNG>();
        }

        public List<LevelNGHistory> LevelNGHistories { get; set; }
        public List<HeaderLevelNG> HeaderLevelNG { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class LevelNGHistory
    {
        
        public int LEVEL_ID { get; set; }
        public int LEVEL_GOAL { get; set; }
        public string NG_MIN { get; set; }
        public string NG_MAX { get; set; }
        public string QUALITY_DESC { get; set; }
        public string PROCEDURE_DESC { get; set; }
        public string PRICE_UT { get; set; }
        public string PRICE_QUALITY { get; set; }
        public string TYPE_NAME { get; set; }

    }
    public class HeaderLevelNG
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}