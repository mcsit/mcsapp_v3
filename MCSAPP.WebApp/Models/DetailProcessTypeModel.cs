﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailProcessTypeModel
    {
        public DetailProcessTypeModel()
        {
            ProcessTypeHistories = new List<ProcessTypeHistory>();
            HeaderProcessType = new List<HeaderProcessType>();
        }

        public List<ProcessTypeHistory> ProcessTypeHistories { get; set; }
        public List<HeaderProcessType> HeaderProcessType { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class ProcessTypeHistory
    {
        public string ROW_ID { get; set; }
        public string ID { get; set; }
        public string MAIN_PROCESS { get; set; }
        public string PROCESS_NAME { get; set; }

        public string USER_UPDATE { get; set; }
        public string TIME_UPDATE { get; set; }
        public string TYPE { get; set; }
    }
    public class HeaderProcessType
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}