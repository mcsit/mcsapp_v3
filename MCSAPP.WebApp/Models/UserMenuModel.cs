﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    /// <summary>
    /// UserMenu Model
    /// </summary>
    public class UserMenuModel
    {
        public UserMenuModel()
        {
            MenuFeatureModels = new List<MenuFeatureModel>();
        }
        public string UserFullName { get; set; }
        public string UserUpdate { get; set; }
        public List<MenuFeatureModel> MenuFeatureModels { get; set; }
    }

    /// <summary>
    /// Headmenu and Link
    /// </summary>
    public class MenuFeatureModel
    {
        public MenuFeatureModel()
        {
            MenuItemFeatureModels = new List<MenuItemFeatureModel>();
            RequrieRole = string.Empty;
            RequrieUsername = new List<string>();
            ActionName = string.Empty;
            MenuName = string.Empty;
            Link = string.Empty;
        }

        /// <summary>
        /// role Requrie
        /// </summary>
        public string RequrieRole { get; set; }

        /// <summary>
        /// username Requrie
        /// </summary>
        public List<string> RequrieUsername { get; set; }

        public string ActionName { get; set; }
        public string MenuName { get; set; }
        public string Link { get; set; }
        public int Sequence { get; set; }
        public List<MenuItemFeatureModel> MenuItemFeatureModels { get; set; }
    }

    /// <summary>
    /// SubMenu and Link
    /// </summary>
    public class MenuItemFeatureModel
    {
        public MenuItemFeatureModel()
        {
            ActionName = string.Empty;
            MenuName = string.Empty;
            Link = string.Empty;
            Sequence = 0;
        }
        public string ActionName { get; set; }
        public string MenuName { get; set; }
        public string Link { get; set; }
        public int Sequence { get; set; }
    }
}