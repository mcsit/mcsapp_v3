﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailReviseModel
    {
        public DetailReviseModel()
        {
            DetailReviseHistories = new List<DetailReviseHistory>();
            HeaderRevise = new List<HeaderRevise>();
        }

        public List<DetailReviseHistory> DetailReviseHistories { get; set; }
        public List<HeaderRevise> HeaderRevise { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailReviseHistory
    {
        public string PROJECT_NAME { get; set; }
        public int ITEM { get; set; }      
        public int ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string SALARY { get; set; }
        public string PRICE { get; set; }


    }
    public class HeaderRevise
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }

    }

}