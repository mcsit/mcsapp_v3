﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureModel
    {
        public StructureModel()
        {
            LevelHistories = new List<LevelHistory>();
            HeaderLevel = new List<HeaderLevel>();
        }

        public List<LevelHistory> LevelHistories { get; set; }
        public List<HeaderLevel> HeaderLevel { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class LevelHistory
    {
        public int LEVEL_ID { get; set; }
        public string LEVEL_NAME { get; set; }
        public int LEVEL_GOAL { get; set; }
        public string LEVEL_SALARY { get; set; }
        public string LEVEL_QUALITY { get; set; }
        public string LEVEL_PROCEDURE { get; set; }
        public string SALARY { get; set; }
        public string TYPE_NAME { get; set; }
    }
    public class HeaderLevel
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}