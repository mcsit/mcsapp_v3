﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailMinusModel
    {
        public DetailMinusModel()
        {
            MinusHistories = new List<MinusHistory>();
            HeaderMinus = new List<HeaderMinus>();
        }

        public List<MinusHistory> MinusHistories { get; set; }
        public List<HeaderMinus> HeaderMinus { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class MinusHistory
    {
        public string ROW_NUM { get; set; }
        public string ID { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string PRICE { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string REMARK { get; set; }
        
    }
    public class HeaderMinus
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}