﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailSummaryWorkModel
    {
        public DetailSummaryWorkModel()
        {
            DetailSummaryWorkHistories = new List<DetailSummaryWorkHistory>();
            HeaderSummaryWork = new List<HeaderSummaryWork>();
        }

        public List<DetailSummaryWorkHistory> DetailSummaryWorkHistories { get; set; }
        public List<HeaderSummaryWork> HeaderSummaryWork { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailSummaryWorkHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public string DATA_1 { get; set; }
        public string DATA_2 { get; set; }
        public string DATA_3 { get; set; }
        public string DATA_4 { get; set; }
        public string DATA_5 { get; set; }
        public string DATA_6 { get; set; }
        public string DATA_7 { get; set; }
        public string DATA_8 { get; set; }
        public string DATA_9 { get; set; }
        public string DATA_10 { get; set; }
    }
    public class HeaderSummaryWork
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}