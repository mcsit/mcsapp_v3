﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    // 2018-02-10 by chaiwud.ta 
    public class StructureBuiltModel
    {
        public StructureBuiltModel()
        {
            LevelBuiltHistories = new List<LevelBuiltHistory>();
            HeaderBuiltLevel = new List<HeaderBuiltLevel>();
        }

        public List<LevelBuiltHistory> LevelBuiltHistories { get; set; }
        public List<HeaderBuiltLevel> HeaderBuiltLevel { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class LevelBuiltHistory
    {
        public int LEVEL_ID { get; set; }
        public string LEVEL_NAME { get; set; }
        public string LEVEL_GOAL { get; set; }
        public string LEVEL_SALARY { get; set; }
        public string LEVEL_QUALITY { get; set; }
        public string LEVEL_PROCEDURE { get; set; }
        public string SALARY { get; set; }
        public string TYPE_NAME { get; set; }
    }
    public class HeaderBuiltLevel
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}