﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailSumSalaryModel
    {
        public DetailSumSalaryModel()
        {
            DetailSumSararyHistories = new List<DetailSumSalaryHistory>();
            HeaderSumSalary = new List<HeaderSumSalary>();
        }

        public List<DetailSumSalaryHistory> DetailSumSararyHistories { get; set; }
        public List<HeaderSumSalary> HeaderSumSalary { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
       
    }
    public class DetailSumSalaryHistory
    {
        public int ROW_ID { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }

        public double SALARY { get; set; }
        public double SALARY_POSITION { get; set; }
        public double SALARY_QUALITY{ get; set; }
        public double SALARY_SUM { get; set; }
 

    }
    public class HeaderSumSalary
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}