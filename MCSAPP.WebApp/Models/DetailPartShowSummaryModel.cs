﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailPartShowSummaryModel
    {
        public DetailPartShowSummaryModel()
        {
            DetailShowSummaryHistories = new List<DetailShowSummaryHistory>();
            HeaderShowSummary = new List<HeaderShowSummary>();
        }

        public List<DetailShowSummaryHistory> DetailShowSummaryHistories { get; set; }
        public List<HeaderShowSummary> HeaderShowSummary { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }

  
    public class DetailShowSummaryHistory
    {
      
        public string ROW_TYPE { get; set; }
        public string DATA_1 { get; set; }
        public string DATA_2 { get; set; }
        public string DATA_3 { get; set; }
        public string DATA_4 { get; set; }
        public string DATA_5 { get; set; }
        public string DATA_6 { get; set; }
        public string DATA_7 { get; set; }
        public string DATA_8 { get; set; }
        public string DATA_9 { get; set; }
        public string DATA_10 { get; set; }
       
    }
    public class HeaderShowSummary
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}