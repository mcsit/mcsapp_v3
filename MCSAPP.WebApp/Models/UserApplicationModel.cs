﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using MCSAPP.DAL.Model;

namespace  MCSAPP.WebApp.Models
{
    public class UserApplicationModel : IPrincipal
    {
        private readonly IIdentity userIdentity;

        public UserApplicationModel() { }

        public UserApplicationModel(string username)
        {
            userIdentity = new UserIdentity(username);
        }

        public string Username { get; set; }

        public string Role { get; set; }

        public long AccessId { get; set; }

        public UserModel UserModel { get; set; }

        public APITrackingConfigModel APITrackingConfig { get; set; }

        public IIdentity Identity
        {
            get
            {
                return userIdentity;
            }
        }

        public bool IsInRole(string requrieRole)
        {
            var roleList = requrieRole.Split(';');
            var userRoles = Role.Split(';');
            return roleList.Any(x => userRoles.Contains(x));
        }
    }
}