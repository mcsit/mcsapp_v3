﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    // UTReoairCheck
    // 2018-03-01 by chaiwud.ta
    public class DetailUTRepairCheckBuiltModel
    {
        public DetailUTRepairCheckBuiltModel()
        {
            UTRepairCheckBuiltHistories = new List<UTRepairCheckBuiltHistory>();
            HeaderUTRepairCheckBuilt = new List<HeaderUTRepairCheckBuilt>();
        }

        public List<UTRepairCheckBuiltHistory> UTRepairCheckBuiltHistories { get; set; }
        public List<HeaderUTRepairCheckBuilt> HeaderUTRepairCheckBuilt { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class UTRepairCheckBuiltHistory
    {
        public string ROW_NUM { get; set; }
        public string ID { get; set; }

        public string PJ_NAME { get; set; }
        public string PD_ITEM { get; set; }
        public string ITEM_NO { get; set; }
        public string BUILT_TYPE { get; set; }
        public string PC_NAME { get; set; }
        public string ACTUAL { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string ACTUAL_CHECK { get; set; }
        public string FULLNAME_CHECK { get; set; }
        public string REMARK { get; set; }

        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
    }
    public class HeaderUTRepairCheckBuilt
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}