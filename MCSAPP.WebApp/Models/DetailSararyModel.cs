﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailSararyModel
    {
        public DetailSararyModel()
        {
            DetailSararyHistories = new List<DetailSararyHistory>();
            HeaderSalary = new List<HeaderSalary>();
        }

        public List<DetailSararyHistory> DetailSararyHistories { get; set; }
        public List<HeaderSalary> HeaderSalary { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailSararyHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_GOAL { get; set; }
        public double D_01 { get; set; }
        public double D_02 { get; set; }
        public double D_03 { get; set; }
        public double D_04 { get; set; }
        public double D_05 { get; set; }
        public double D_06 { get; set; }
        public double D_07 { get; set; }
        public double D_08 { get; set; }

    }
    public class HeaderSalary
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string functionCall { get; set; }
        public string fieldType { get; set; }
    }

}