﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    // OtherCamber
    // 2018-04-23 by chaiwud.ta
    public class DetailCamberDataOtherModel
    {
        public DetailCamberDataOtherModel()
        {
            CamberDataOtherHistories = new List<CamberDataOtherHistory>();
            HeaderCamberDataOther = new List<HeaderCamberDataOther>();
        }

        public List<CamberDataOtherHistory> CamberDataOtherHistories { get; set; }
        public List<HeaderCamberDataOther> HeaderCamberDataOther { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }

    public class CamberDataOtherHistory
    {

        public string ROW_NUM { get; set; }
        public string RUN_ID { get; set; }
        public string NO_ORDER { get; set; }

        public string PROJECT { get; set; }
        public string PD_ITEM { get; set; }
        public string CUTTING_PLAN { get; set; }
        public string BUILT_NO { get; set; }
        public string ACTUAL_GROUP { get; set; }
        public string ACTUAL_DATE { get; set; }

        public string USER_UPDATE { get; set; }
        public string UPDATE_DATE { get; set; }
    }
    public class HeaderCamberDataOther
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}