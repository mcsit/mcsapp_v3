﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailPartDetailSummaryForWorkModel
    {
        public DetailPartDetailSummaryForWorkModel()
        {
            DetailPartDetailSummaryForWorkHistories = new List<DetailPartDetailSummaryForWorkHistory>();
            HeaderPartDetailSummaryForWork = new List<HeaderPartDetailSummaryForWork>();
        }

        public List<DetailPartDetailSummaryForWorkHistory> DetailPartDetailSummaryForWorkHistories { get; set; }
        public List<HeaderPartDetailSummaryForWork> HeaderPartDetailSummaryForWork { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }

  
    public class DetailPartDetailSummaryForWorkHistory
    {
        public string ROW_NUM { get; set; }
        public string PJ_NAME { get; set; }
        public string PD_ITEM { get; set; }
        public string PD_CODE { get; set; }
        public string SYMBOL { get; set; }
        public string PART_ITEM { get; set; }
        public string PART_SIZE { get; set; }
        public string CUTTING_PLAN { get; set; }
        public string PROCESS { get; set; }
        public string HOLE { get; set; }
        public string TAPER { get; set; }
        public string PLAN_QTY { get; set; }
        public string ACT_QTY { get; set; }
        public string ACT_LENGTH { get; set; }
        public string ACT_GROUP { get; set; }
        public string ACTUAL_USER { get; set; }
        public string ACT_NAME { get; set; }
        public string ACT_DATE { get; set; }
        public string ACT_QTY_SAVE { get; set; }
        public string ID { get; set; }
    }
    public class HeaderPartDetailSummaryForWork
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}