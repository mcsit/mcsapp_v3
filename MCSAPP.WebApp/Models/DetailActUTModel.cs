﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailActUTModel
    {
        public DetailActUTModel()
        {
            DetailActUTHistories = new List<DetailActUTHistory>();
            HeaderActUT = new List<HeaderActUT>();
        }

        public List<DetailActUTHistory> DetailActUTHistories { get; set; }
        public List<HeaderActUT> HeaderActUT { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailActUTHistory
    {
        public string DEPT_NAME { get; set; }
     
        public string ROW_NUM { get; set; }

        public string WEEK_KEY { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }

        public string RUN_ID { get; set; }
    }
    public class HeaderActUT
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
   
}