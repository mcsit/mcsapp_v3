﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailVTModel
    {
        public DetailVTModel()
        {
            VTHistories = new List<VTHistory>();
            HeaderVT = new List<HeaderVT>();
        }

        public List<VTHistory> VTHistories { get; set; }
        public List<HeaderVT> HeaderVT { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class VTHistory
    {
        public string ID { get; set; }
        public string ROW_NUM { get; set; }
        public string PROJECT { get; set; }
        public int ITEM { get; set; }
        public string REV_NO { get; set; }
        public string CHECK_NO { get; set; }
        public string PART { get; set; }
        public string POINT { get; set; }
        public string PROBLEM { get; set; }
        public string DWG { get; set; }
        public string ACT { get; set; }
        public string WRONG { get; set; }
        public string EDIT { get; set; }
        public string CHECKER { get; set; }
        public string CHECK_DATE { get; set; }

    }
    public class HeaderVT
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}