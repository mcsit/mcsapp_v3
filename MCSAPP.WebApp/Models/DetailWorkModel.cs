﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailWorkModel
    {
        public DetailWorkModel()
        {
            DetailWorkHistories = new List<DetailWorkHistory>();
            HeaderWork = new List<HeaderWork>();
        }

        public List<DetailWorkHistory> DetailWorkHistories { get; set; }
        public List<HeaderWork> HeaderWork { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailWorkHistory
    {
        public string PROJECT_NAME { get; set; }
        public int ITEM { get; set; }
        public string EMP_GOAL { get; set; }
        public string ACTUAL { get; set; }

        public string ACTUAL_DATE { get; set; }
        public string LEVEL_SALARY { get; set; }
        public string LEVEL_QUALITY { get; set; }
        public string LEVEL_PROCEDURE { get; set; }
        public string SALARY { get; set; }
        public string QUALITY { get; set; }
        public string PROCEDURE { get; set; }
        public string SUM_SALARY { get; set; }

    }
    public class HeaderWork
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }

}