﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailPlusModel
    {
        public DetailPlusModel()
        {
            DetailPlusHistories = new List<DetailPlusHistory>();
            HeaderPlus = new List<HeaderPlus>();
        }

        public List<DetailPlusHistory> DetailPlusHistories { get; set; }
        public List<HeaderPlus> HeaderPlus { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailPlusHistory
    {
        public int ROW_NUM { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string FIX_SALARY { get; set; }
        public string POSITION_SALARY { get; set; }
        public string ALL_SALARY { get; set; }
        public string SUM_SALARY { get; set; }


    }
    public class HeaderPlus
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}