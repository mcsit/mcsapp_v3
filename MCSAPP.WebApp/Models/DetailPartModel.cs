﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailPartModel
    {
        public DetailPartModel()
        {
            PartHistories = new List<PartHistory>();
            HeaderPart = new List<HeaderPart>();
        }

        public List<PartHistory> PartHistories { get; set; }
        public List<HeaderPart> HeaderPart { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class PartHistory
    {
        public int ROW_NUM { get; set; }
        public int PJ_ID { get; set; }
        public string PJ_NAME { get; set; }
        public int PD_ITEM { get; set; }
        public string REV_NO { get; set; }
        public string PD_CODE { get; set; }
        public string DESIGN_CHANGE { get; set; }
        public string PD_WEIGHT { get; set; }
        public string PD_LENGTH { get; set; }
        public string PC_NAME { get; set; }
        public string UG_NAME { get; set; }
        public string PA_PLAN_DATE { get; set; }
        public string FAB_PLAN { get; set; }
        public string FAB_ACTUAL { get; set; }
        public string ACTUAL_USER { get; set; }
        public string FULLNAME { get; set; }
        public string ACTUAL_DATE { get; set; }
        public int PA_NO { get; set; }
    }
    public class HeaderPart
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}