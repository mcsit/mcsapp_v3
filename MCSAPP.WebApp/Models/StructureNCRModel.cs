﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureNCRModel
    {
        public StructureNCRModel()
        {
            LevelNCRHistories = new List<LevelNCRHistory>();
            HeaderLevelNCR = new List<HeaderLevelNCR>();
        }

        public List<LevelNCRHistory> LevelNCRHistories { get; set; }
        public List<HeaderLevelNCR> HeaderLevelNCR { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class LevelNCRHistory
    {
        
        public int LEVEL_ID { get; set; }
        public int LEVEL_GOAL { get; set; }
        public string MAIN_MIN { get; set; }
        public string MAIN_MAX { get; set; }
        public string TEMPO_MIN { get; set; }
        public string TEMPO_MAX { get; set; }
        public string PRICE_QULITY { get; set; }
        public string TYPE_NAME { get; set; }

    }
    public class HeaderLevelNCR
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}