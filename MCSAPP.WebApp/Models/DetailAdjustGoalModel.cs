﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailAdjustGoalModel
    {
        public DetailAdjustGoalModel()
        {
            AdjustGoalHistories = new List<AdjustGoalHistory>();
            HeaderAdjustGoal = new List<HeaderAdjustGoal>();
        }

        public List<AdjustGoalHistory> AdjustGoalHistories { get; set; }
        public List<HeaderAdjustGoal> HeaderAdjustGoal { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class AdjustGoalHistory
    {
        public string ROW_NUM { get; set; }
        public string ID { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string WEEK_KEY { get; set; }
        public string SUM_PART { get; set; }
        public string CURRENT_GOAL { get; set; }
        public string ADJUST_GOAL { get; set; }
        public string NEW_GOAL { get; set; }
        public string WEEK_NO { get; set; }
    }
    public class HeaderAdjustGoal
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}