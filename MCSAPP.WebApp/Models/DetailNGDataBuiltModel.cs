﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailNGDataBuiltModel
    {
        public DetailNGDataBuiltModel()
        {
            NGDataBuiltHistories = new List<NGDataBuiltHistory>();
            HeaderNGDataBuilt = new List<HeaderNGDataBuilt>();
        }

        public List<NGDataBuiltHistory> NGDataBuiltHistories { get; set; }
        public List<HeaderNGDataBuilt> HeaderNGDataBuilt { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }

    public class NGDataBuiltHistory
    {
   
        public string ROW_NUM { get; set; }
        public string RUN_ID { get; set; }
        public string NO_ORDER { get; set; }

        public string DATE { get; set; }
        public string PROJECT { get; set; }
        public string PD_ITEM { get; set; }
        public string CODE { get; set; }
        public string SKIN_PLATE { get; set; }
        public string DIAPHRAGM { get; set; }
        public string SAW_NG_LENGTH { get; set; }
        public string SAW_NG_JOINT { get; set; }
        public string SAW_TOTAL { get; set; }
        public string SAW_D { get; set; }
        public string SAW_K { get; set; }
        public string SAW_WELDER { get; set; }
        public string AUTOROOT_NG_LENGTH { get; set; }
        public string AUTOROOT_NG_JOINT { get; set; }
        public string AUTOROOT_TOTAL { get; set; }
        public string AUTOROOT_D { get; set; }
        public string AUTOROOT_K { get; set; }
        public string AUTOROOT_WELDER { get; set; }
        public string ESW_NG_LENGTH { get; set; }
        public string ESW_NG_JOINT { get; set; }
        public string ESW_TOTAL { get; set; }
        public string ESW_D { get; set; }
        public string ESW_X { get; set; }
        public string ESW_SIDE { get; set; }
        public string ESW_WELDER { get; set; }

        public string USER_UPDATE { get; set; }
        public string UPDATE_DATE { get; set; }
    }
    public class HeaderNGDataBuilt
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}