﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureCertListModel
    {
        public StructureCertListModel()
        {
            CertListHistories = new List<CertListHistory>();
            HeaderCertList = new List<HeaderCertList>();
        }

        public List<CertListHistory> CertListHistories { get; set; }
        public List<HeaderCertList> HeaderCertList { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class CertListHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPT_NAME { get; set; }
        public string CERT_NAME { get; set; }
        public string CERT_START { get; set; }
        public string CERT_END { get; set; }
    }
    public class HeaderCertList
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }

    }
}