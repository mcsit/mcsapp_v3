﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailNGNewModel
    {
        public DetailNGNewModel()
        {
            DetailNGNewHistories = new List<DetailNGNewHistory>();
            HeaderNGNew = new List<HeaderNGNew>();
        }

        public List<DetailNGNewHistory> DetailNGNewHistories { get; set; }
        public List<HeaderNGNew> HeaderNGNew { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailNGNewHistory
    {
        public string DEPT_NAME { get; set; }
        public string WEEK_1 { get; set; }
        public string WEEK_2 { get; set; }
        public string WEEK_3 { get; set; }
        public string WEEK_4 { get; set; }
        public string WEEK_5 { get; set; }
        public string WEEK_6 { get; set; }
        public string ROW_NUM { get; set; }
        public string GROUP_NAME { get; set; }
        public string TOTAL_JOINT { get; set; }
        public string JOINT_NG { get; set; }
        public string PERCENT_NG { get; set; }

        public string WEEK_KEY { get; set; }
        public string WEEK_YEAR { get; set; }
        public string WEEK_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string RUN_ID { get; set; }
    }
    public class HeaderNGNew
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
   
}