﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailOtherModel
    {
        public DetailOtherModel()
        {
            OtherHistories = new List<OtherHistory>();
            HeaderOther = new List<HeaderOther>();
        }

        public List<OtherHistory> OtherHistories { get; set; }
        public List<HeaderOther> HeaderOther { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class OtherHistory
    {
        public string ROW_NUM { get; set; }
        public string ID { get; set; }
        public string EMP_CODE { get; set; }
      
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string PROCESS_NAME { get; set; }
        public string PRICE { get; set; }
        public string ACTUAL_DATE { get; set; }
        public string ACTUAL { get; set; }
        public string TYPE_LOAD { get; set; }

    }
    public class HeaderOther
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}