﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    // 2018-02-01 by chaiwud.ta
    public class DetailBuiltDetailSummaryForWorkModel
    {
        public DetailBuiltDetailSummaryForWorkModel()
        {
            DetailBuiltDetailSummaryForWorkHistories = new List<DetailBuiltDetailSummaryForWorkHistory>();
            HeaderBuiltDetailSummaryForWork = new List<HeaderBuiltDetailSummaryForWork>();
        }
        public List<DetailBuiltDetailSummaryForWorkHistory> DetailBuiltDetailSummaryForWorkHistories { get; set; }
        public List<HeaderBuiltDetailSummaryForWork> HeaderBuiltDetailSummaryForWork { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailBuiltDetailSummaryForWorkHistory
    {
        public string ROW_NUM { get; set; }
        public string PJ_NAME { get; set; }
        public string PD_ITEM { get; set; }
        public string ITEM_NO { get; set; }
        public string BUILT_TYPE_NAME { get; set; }
        public string CUTTING_PLAN { get; set; }
        public string BUILT_NO { get; set; }
        public string BUILT_SIZE { get; set; } // for built box
        public string BUILT_LENGTH { get; set; } // for Auto Root
        public string HOLE { get; set; }
        public string PLACE { get; set; }
        public string BUILT_WEIGHT { get; set; }
        public string BUILT_PLAN { get; set; }
        public string PC_NAME { get; set; }
        public string UG_NAME { get; set; }
        public string PA_PLAN_DATE { get; set; }
        public string ACT_USER { get; set; }
        public string FULLNAME { get; set; }
        public string PA_ACTUAL_DATE { get; set; }
        public string BUILT_ACTUAL { get; set; }
        public string PA_NO { get; set; }

        // add for other detail
        public string PD_REV { get; set; }
        public string PD_CODE { get; set; }
        public string PD_DWG { get; set; }
        public string PD_BLOCK { get; set; }


        public string ID { get; set; }
    }
    public class HeaderBuiltDetailSummaryForWork
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}