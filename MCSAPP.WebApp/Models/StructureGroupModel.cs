﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class StructureGroupModel
    {
        public StructureGroupModel()
        {
            LevelGroupHistories = new List<LevelGroupHistory>();
            HeaderLevelGroup = new List<HeaderLevelGroup>();
        }

        public List<LevelGroupHistory> LevelGroupHistories { get; set; }
        public List<HeaderLevelGroup> HeaderLevelGroup { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class LevelGroupHistory
    {
        public int LEVEL_ID { get; set; }
        public int GOAL_DAY_GROUP { get; set; }
        public string LEVEL_SALARY { get; set; }
        public string LEVEL_QUALITY { get; set; }
        public string LEVEL_PROCEDURE { get; set; }
        public string MIN_SALARY { get; set; }
        public string TYPE_NAME { get; set; }
    }
    public class HeaderLevelGroup
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }

    }
}