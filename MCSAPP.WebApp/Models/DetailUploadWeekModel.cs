﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailUploadWeekModel
    {
        public DetailUploadWeekModel()
        {
            UploadWeekHistories = new List<UploadWeekHistory>();
            HeaderUploadWeek = new List<HeaderUploadWeek>();
        }

        public List<UploadWeekHistory> UploadWeekHistories { get; set; }
        public List<HeaderUploadWeek> HeaderUploadWeek { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class UploadWeekHistory
    {
        public string ROW_NUM { get; set; }
        public string WEEK_KEY { get; set; }
        public string WEEK_NO { get; set; }
        public string WEEK_DATE { get; set; }
        public string WEEK_YEAR { get; set; }
        public string WEEK_MONTH { get; set; }
    }
    public class HeaderUploadWeek
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}