﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MCSAPP.WebApp.Helper;

namespace MCSAPP.WebApp.Models
{
    public class TransectionModel
    {
        private string reqCode;
        private string encodedReqCode;

        public TransectionModel()
        {
            DocType = "0";
            FromDate = string.Empty;
            ToDate = string.Empty;
        }

        public string ReqCode  { get { return reqCode; } set { reqCode = value; } }

        public string EncodedReqCode { get { return encodedReqCode; } }

        public string Status { get; set; }

        public string DocNumber { get; set; }

        public string Action { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string DocType { get; set; }

        public string SelectedState { get; set; }

        public void SetEncodedReqCode(string reqCode)
        {
            encodedReqCode = WebHelper.EncodeRequestCode(reqCode);
        }

        public string GetDecodedReqCode(string code)
        {
            try
            {
                var result = WebHelper.DecodeRequestCode(code);
                return result;
            }
            catch (Exception)
            {
                throw new UnauthorizedAccessException("มีการใช้งานที่ไม่ถูกต้อง");
            }
        }
    }
}