﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailDesingModel
    {
        public DetailDesingModel()
        {
            DesingHistories = new List<DesingHistory>();
            HeaderDesing = new List<HeaderDesing>();
        }

        public List<DesingHistory> DesingHistories { get; set; }
        public List<HeaderDesing> HeaderDesing { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DesingHistory
    {
        public string ROW_NUM { get; set; }
        public string ID { get; set; }
        public string GROUP_NAME { get; set; }
        public string NUMBER_DESING { get; set; }
        public string PROJECT { get; set; }
        public int ITEM { get; set; }
        public string DATE { get; set; }
        public string USER_UPDATE { get; set; }

    }
    public class HeaderDesing
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}