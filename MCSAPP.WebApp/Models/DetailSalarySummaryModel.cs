﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCSAPP.WebApp.Models
{
    public class DetailSalarySummaryModel
    {
        public DetailSalarySummaryModel()
        {
            DetailSalarySummaryHistories = new List<DetailSalarySummaryHistory>();
            HeaderSalarySummary = new List<HeaderSalarySummary>();
        }

        public List<DetailSalarySummaryHistory> DetailSalarySummaryHistories { get; set; }
        public List<HeaderSalarySummary> HeaderSalarySummary { get; set; }
        public int RowTotal { get; set; }
        public int PageTotal { get; set; }
    }
    public class DetailSalarySummaryHistory
    {
        public string EMP_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string DEPT_NAME { get; set; }
        public string EMP_GOAL { get; set; }
        public string GOAL { get; set; }
        public string LEVEL_SALARY { get; set; }
        public string LEVEL_QUALITY { get; set; }
        public string LEVEL_PROCEDURE { get; set; }
        public string SUM_PART { get; set; }
        public string OVER_GOAL { get; set; }
        public string FIX_SALARY { get; set; }
        public string SALARY { get; set; }
        public string SALARY_QUALITY { get; set; }
        public string SALARY_PROCEDURE { get; set; }
        public string OTHER_SALARY { get; set; }
        public string REVISE_SALARY { get; set; }
        public string WELD_SALARY { get; set; }
        public string DIMENSION_SALARY { get; set; }
        public string MAIN { get; set; }
        public string TEMPO { get; set; }
        public string VT_INSPEC { get; set; }
        public string VT_QPC { get; set; }
        public string VT_OTHER { get; set; }
        public string VT_DESIGN_CHANGE { get; set; }
        public string MINUS { get; set; }
        public string PLUS { get; set; }
        public string SUM_SALARY { get; set; }
        public string POSITION { get; set; }
        public string PART { get; set; }
        public string OTHER { get; set; }
        public string REVISE { get; set; }
        public string WELD { get; set; }
        public string DIMENSION { get; set; }
        public string MINUS_2 { get; set; }
        public string POSITION_2 { get; set; }
        public string SUM_SALARY_2 { get; set; }

    }
    public class HeaderSalarySummary
    {
        public string head { get; set; }
        public string field { get; set; }
        public bool cansort { get; set; }
        public string sort { get; set; }
        public string style { get; set; }
        public string fieldType { get; set; }
        public string functionCall { get; set; }
    }
}