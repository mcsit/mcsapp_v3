﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using Newtonsoft.Json;
using System.Reflection;
using System.ComponentModel;

namespace MCSAPP.WebApp.Controllers
{
    public class ReportController : BaseController
    {
        // GET: Report

        #region Old
        //public ActionResult Index()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSararyByCode()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportByFab()
        //{
        //    return View();
        //}
        //public ActionResult ReportByGroup()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSummarySarary()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSararyByGroup()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSummaryByEmp()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportWorkByDay()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportWorkByWeek()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportWorkByMonth()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSalaryByDay()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSalaryByWeek()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSalaryByMonth()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSalaryGroupByDay()
        //{
        //    var userProfile = Session["UserName"];
        //    if (userProfile == null)
        //    {
        //        return RedirectToAction("../Session");
        //    }
        //    return View();
        //}
        //public ActionResult ReportSalaryGroupByWeek()
        //{
        //    return View();
        //}
        #endregion

        public ActionResult ReportWorkLessGoal()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportWorkLessWeldGoal()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportNCR()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportNG()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportVT()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryWorkDayForWeek()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryWorkDayForWeekWeld()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryForWeekWeld()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryForWeekWeldSum()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryForWeekSum()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryDayForWeek()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryPlus()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryTotal()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummarySalaryMinus()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        [HttpPost]
        [Route("GetWeekByMonth")]
        public async Task<ActionResult> GetWeekByMonth(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewWeekServiceWrapperInstance().GetWeekByMonth(model);
            return OK(result);
        }

        [Route("GetGroupName")]
        [HttpPost]
        public async Task<ActionResult> GetGroupName(SearchModel model)
        {


            var result = await MCSServiceAPIFactory.NewWeekServiceWrapperInstance().GetGroupName(model);

            return OK(result);
        }

        [HttpPost]
        [Route("GetMaxWeeks")]
        public async Task<ActionResult> GetMaxWeeks(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewWeekServiceWrapperInstance().GetMaxWeeks(model);
            return OK(result.Value);
        }

        [HttpPost]
        [Route("GetHeaderWeek")]
        public async Task<ActionResult> GetHeaderWeek(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewWeekServiceWrapperInstance().GetHeaderWeek(model);
            return OK(result);
        }

        #region Un use
        ////[HttpPost]
        ////[Route("GetSummaryByWork")]
        ////public async Task<ActionResult> GetSummaryByWork(DateModel model)
        ////{
        ////    if (ModelState.IsValid)
        ////    {
        ////        //var targetUser = ApplicationUserProfile();


        ////        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        ////        var serviceResult = await service.GetSummaryByWork(model);
        ////        var result = MapDetailGoalModel(serviceResult,model.DETAIL);
        ////        return OK(result);
        ////    }
        ////    return OK("");
        ////}



        //[HttpPost]
        //[Route("GetSararyMonthByWeek")]
        //public async Task<ActionResult> GetSararyMonthByWeek(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSararyMonthByWeek(model);
        //        var result = MapDetailSararyModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailSararyModel MapDetailSararyModel(List<SararyModel> models)
        //{
        //    var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
        //    var result = new DetailSararyModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models)
        //    {
        //        var items = new DetailSararyHistory()
        //        {
        //            EMP_CODE = item.EMP_CODE,
        //            FULL_NAME = item.FULL_NAME,
        //            DEPT_NAME = item.DEPT_NAME,
        //            EMP_GOAL = item.EMP_GOAL,
        //            D_01 = item.D_01,
        //            D_02 = item.D_02,
        //            D_03 = item.D_03,
        //            D_04 = item.D_04,
        //            D_05 = item.D_05,
        //            D_06 = item.D_06,
        //            D_07 = item.D_07,
        //            D_08 = item.D_08,
        //        };
        //        result.DetailSararyHistories.Add(items);
        //    }

        //    foreach (Salary h in Enum.GetValues(typeof(Salary)))
        //    {
        //        if (DataConverter.GetString(h).Contains("D_")) {
        //            var heads = new HeaderSalary()
        //            {
        //                head = GetEnumDescription(h),
        //                fieldType = "linkDate",
        //                field = DataConverter.GetString(h),
        //                cansort = false,
        //                sort = DataConverter.GetString(h),
        //                style = "{ 'width' : '20px'} ",
        //                functionCall = "ShowDate"
        //            };
        //            result.HeaderSalary.Add(heads);
        //        }
        //        else
        //        {
        //            var heads = new HeaderSalary()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = false,
        //                sort = DataConverter.GetString(h),
        //                style = "{ 'width' : '20px'} ",
        //            };
        //            result.HeaderSalary.Add(heads);
        //        }

        //    }
        //    return result;
        //}


        //[HttpPost]
        //[Route("GetSummaryWorkByCode")]
        //public async Task<ActionResult> GetSummaryWorkByCode(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSummaryWorkByCode(model);
        //        var result = MapDetailGoalModel(serviceResult,model.DETAIL,false);
        //        return OK(result);
        //    }
        //    return OK("");
        //}


        //[HttpPost]
        //[Route("GetSararyMonthByCode")]
        //public async Task<ActionResult> GetSararyMonthByCode(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSararyMonthByCode(model);
        //        var result = MapDetailSararyModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}



        //[HttpPost]
        //[Route("GetSalaryFab")]
        //public async Task<ActionResult> GetSalaryFab(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSalaryFab(model);
        //        var result = MapDetailSumSararyModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailSumSalaryModel MapDetailSumSararyModel(List<SumSalaryModel> models)
        //{
        //    var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
        //    var result = new DetailSumSalaryModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models)
        //    {
        //        var items = new DetailSumSalaryHistory()
        //        {
        //            EMP_CODE = item.EMP_CODE,
        //            FULL_NAME = item.FULL_NAME,

        //            SALARY = item.SALARY,
        //            SALARY_POSITION = item.SALARY_POSITION,
        //            SALARY_QUALITY = item.SALARY_QUALITY,
        //            SALARY_SUM = item.SALARY_SUM,

        //        };
        //        result.DetailSumSararyHistories.Add(items);
        //    }

        //    foreach (SumSalary h in Enum.GetValues(typeof(SumSalary)))
        //    {
        //        if (DataConverter.GetString(h) == "PART")
        //        {
        //            var heads = new HeaderSumSalary()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = true,
        //                sort = DataConverter.GetString(h),
        //                style = "{ 'width' : '20px'} ",
        //            };
        //            result.HeaderSumSalary.Add(heads);
        //        }
        //        else {
        //            var heads = new HeaderSumSalary()
        //            {
        //                head = GetEnumDescription(h),
        //                fieldType = "link",
        //                functionCall = "Show",
        //                field = DataConverter.GetString(h),
        //                cansort = true,
        //                sort = DataConverter.GetString(h),
        //                style = "{ 'vertical-align': 'initial', 'text-align': 'center' } ",
        //            };
        //            result.HeaderSumSalary.Add(heads);
        //        }


        //    }
        //    return result;
        //}


        //[HttpPost]
        //[Route("GetWorkLessGoal")]
        //public async Task<ActionResult> GetWorkLessGoal(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetWorkLessGoal(model);
        //        var result = MapDetailGoalModel(serviceResult, model.DETAIL, model.SUM);
        //        return OK(result);
        //    }
        //    return OK("");
        //}


        //[HttpPost]
        //[Route("GetWorkMonth")]
        //public async Task<ActionResult> GetWorkMonth(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetWorkMonth(model);
        //        var result = MapDetailModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailWorkModel MapDetailModel(List<WorkModel> models)
        //{
        //    var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
        //    var result = new DetailWorkModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models)
        //    {
        //        var items = new DetailWorkHistory()
        //        {
        //            PROJECT_NAME = item.PROJECT_NAME,
        //            ITEM = item.ITEM,
        //            ACTUAL =  DataConverter.GetNumberFormat(item.ACTUAL, 0) ,
        //            ACTUAL_DATE = item.ACTUAL_DATE,
        //            //EMP_GOAL = item.EMP_GOAL > 0 ? DataConverter.GetNumberFormat(item.EMP_GOAL, 2) : "0.00",
        //            //LEVEL_SALARY = item.LEVEL_SALARY > 0 ? DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2) : "0.00",
        //            //LEVEL_QUALITY = item.LEVEL_QUALITY > 0 ? DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2) : "0.00",
        //            //LEVEL_PROCEDURE = item.LEVEL_PROCEDURE > 0 ? DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2) : "0.00",
        //            //SALARY = item.SALARY > 0 ? DataConverter.GetNumberFormat(item.SALARY, 2) : "0.00",
        //            //QUALITY = item.QUALITY > 0 ? DataConverter.GetNumberFormat(item.QUALITY, 2) : "0.00",
        //            //PROCEDURE = item.PROCEDURE > 0 ? DataConverter.GetNumberFormat(item.PROCEDURE, 2) : "0.00",
        //            //SUM_SALARY = item.SUM_SALARY > 0 ? DataConverter.GetNumberFormat(item.SUM_SALARY, 2) : "0.00",
        //        };
        //        result.DetailWorkHistories.Add(items);
        //    }
        //    foreach (Project h in Enum.GetValues(typeof(Project)))
        //    {

        //        var heads = new HeaderWork()
        //        {
        //            head = GetEnumDescription(h),
        //            field = DataConverter.GetString(h),
        //            cansort = false,
        //            sort = DataConverter.GetString(h),
        //            style = (GetEnumDescription(h).Contains("โปรเจค") ? "vertical-align:initial;text-align:left" : "vertical-align:initial;text-align:center"),
        //        };
        //        result.HeaderWork.Add(heads);
        //    }
        //    foreach (Product h in Enum.GetValues(typeof(Product)))
        //    {

        //        var heads = new HeaderWork()
        //        {
        //            head = GetEnumDescription(h),
        //            field = DataConverter.GetString(h),
        //            cansort = false,
        //            sort = DataConverter.GetString(h),
        //            style = (GetEnumDescription(h).Contains("จำนวน") ? "vertical-align:initial;text-align:right;width:150px;" : "vertical-align:initial;text-align:center;width:200px;"),
        //        };
        //        result.HeaderWork.Add(heads);
        //    }
        //    //foreach (Work h in Enum.GetValues(typeof(Work)))
        //    //{

        //    //    var heads = new HeaderWork()
        //    //    {
        //    //        head = GetEnumDescription(h),
        //    //        field = DataConverter.GetString(h),
        //    //        cansort = false,
        //    //        sort = DataConverter.GetString(h),
        //    //        style = GetEnumDescription(h).Contains("ราคา")
        //    //                || DataConverter.GetString(h).Contains("LEVEL")
        //    //                || DataConverter.GetString(h).Contains("เงิน") ? "vertical-align:initial;text-align:right" :
        //    //                                    (GetEnumDescription(h).Contains("โปรเจค") ? "vertical-align:initial;text-align:left" : "vertical-align:initial;text-align:center"),
        //    //    };
        //    //    result.HeaderWork.Add(heads);
        //    //}
        //    return result;
        //}

        //[HttpPost]
        //[Route("GetWorkMonthByDay")]
        //public async Task<ActionResult> GetWorkMonthByDay(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetWorkMonthByDay(model);
        //        var result = MapDetailModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}

        //[HttpPost]
        //[Route("GetWorkByDay")]
        //public async Task<ActionResult> GetWorkByDay(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetWorkByDay(model);
        //        var result = MapDayIntModel(serviceResult,model.DETAIL);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailDayModel MapDayIntModel(DayHeaderModel models, bool detail)
        //{
        //    var totalRecord = models.DayModel.Any() ? (int)models.DayModel.First().TOTAL_COUNT : 0;
        //    var result = new DetailDayModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models.DayModel)
        //    {
        //        var items = new DetailDayHistory();
        //        //{
        //        items.EMP_CODE = item.EMP_CODE;
        //        items.FULL_NAME = item.FULL_NAME;
        //        items.DEPT_NAME = item.DEPT_NAME;
        //        items.EMP_GOAL = item.EMP_GOAL;
        //        items.D_01 = item.D_01 != null ? DataConverter.GetNumberFormat(item.D_01, 0) : "0";
        //        items.D_02 = item.D_02 != null ? DataConverter.GetNumberFormat(item.D_02, 0) : "0";
        //        items.D_03 = item.D_03 != null ? DataConverter.GetNumberFormat(item.D_03, 0) : "0";
        //        items.D_04 = item.D_04 != null ? DataConverter.GetNumberFormat(item.D_04, 0) : "0";
        //        items.D_05 = item.D_05 != null ? DataConverter.GetNumberFormat(item.D_05, 0) : "0";
        //        items.D_06 = item.D_06 != null ? DataConverter.GetNumberFormat(item.D_06, 0) : "0";
        //        items.D_07 = item.D_07 != null ? DataConverter.GetNumberFormat(item.D_07, 0) : "0";
        //        items.D_08 = item.D_08 != null ? DataConverter.GetNumberFormat(item.D_08, 0) : "0";
        //        items.D_09 = item.D_09 != null ? DataConverter.GetNumberFormat(item.D_09, 0) : "0";
        //        items.D_10 = item.D_10 != null ? DataConverter.GetNumberFormat(item.D_10, 0) : "0";
        //        items.D_11 = item.D_11 != null ? DataConverter.GetNumberFormat(item.D_11, 0) : "0";
        //        items.D_12 = item.D_12 != null ? DataConverter.GetNumberFormat(item.D_12, 0) : "0";
        //        items.D_13 = item.D_13 != null ? DataConverter.GetNumberFormat(item.D_13, 0) : "0";
        //        items.D_14 = item.D_14 != null ? DataConverter.GetNumberFormat(item.D_14, 0) : "0";
        //        items.D_15 = item.D_15 != null ? DataConverter.GetNumberFormat(item.D_15, 0) : "0";
        //        items.D_16 = item.D_16 != null ? DataConverter.GetNumberFormat(item.D_16, 0) : "0";
        //        items.D_17 = item.D_17 != null ? DataConverter.GetNumberFormat(item.D_17, 0) : "0";
        //        items.D_18 = item.D_18 != null ? DataConverter.GetNumberFormat(item.D_18, 0) : "0";
        //        items.D_19 = item.D_19 != null ? DataConverter.GetNumberFormat(item.D_19, 0) : "0";
        //        items.D_20 = item.D_20 != null ? DataConverter.GetNumberFormat(item.D_20, 0) : "0";
        //        items.D_21 = item.D_21 != null ? DataConverter.GetNumberFormat(item.D_21, 0) : "0";
        //        items.D_22 = item.D_22 != null ? DataConverter.GetNumberFormat(item.D_22, 0) : "0";
        //        items.D_23 = item.D_23 != null ? DataConverter.GetNumberFormat(item.D_23, 0) : "0";
        //        items.D_24 = item.D_24 != null ? DataConverter.GetNumberFormat(item.D_24, 0) : "0";
        //        items.D_25 = item.D_25 != null ? DataConverter.GetNumberFormat(item.D_25, 0) : "0";
        //        items.D_26 = item.D_26 != null ? DataConverter.GetNumberFormat(item.D_26, 0) : "0";
        //        items.D_27 = item.D_27 != null ? DataConverter.GetNumberFormat(item.D_27, 0) : "0";
        //        items.D_28 = item.D_28 != null ? DataConverter.GetNumberFormat(item.D_28, 0) : "0";
        //        items.D_29 = item.D_29 != null ? DataConverter.GetNumberFormat(item.D_29, 0) : "0";
        //        items.D_30 = item.D_30 != null ? DataConverter.GetNumberFormat(item.D_30, 0) : "0";
        //        items.D_31 = item.D_31 != null ? DataConverter.GetNumberFormat(item.D_31, 0) : "0";
        //        // };
        //        result.DetailDayHistories.Add(items);
        //    }
        //    if (detail) {
        //        foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
        //        {

        //            var heads = new HeaderDay()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = false,
        //                sort = DataConverter.GetString(h),
        //               // style = "{ 'width' : '20px'} ",

        //            };
        //            result.HeaderDay.Add(heads);
        //        }
        //    }
        //    foreach (var head in models.Header)
        //    {

        //        var heads = new HeaderDay()
        //        {
        //            head = head.Value,
        //            field = "D_" + DataConverter.GetString(head.Value),
        //            cansort = false,
        //            sort = DataConverter.GetString(head.Value),
        //            style = "width:80px;vertical-align:initial;text-align:right",

        //            fieldType = "linkDate",
        //            functionCall = "ShowDate"
        //        };
        //        result.HeaderDay.Add(heads);
        //    }

        //    return result;
        //}

        //[HttpPost]
        //[Route("GetWorkByWeek")]
        //public async Task<ActionResult> GetWorkByWeek(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetWorkByWeek(model);
        //        var result = MapWeekIntModel(serviceResult, model.DETAIL);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailWeekModel MapWeekIntModel(WeekHeaderModel models, bool detail)
        //{
        //    var totalRecord = models.WeekModel.Any() ? (int)models.WeekModel.First().TOTAL_COUNT : 0;
        //    var result = new DetailWeekModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models.WeekModel)
        //    {
        //        var items = new DetailWeekHistory();
        //        //{
        //        items.EMP_CODE = item.EMP_CODE;
        //        items.FULL_NAME = item.FULL_NAME;
        //        items.DEPT_NAME = item.DEPT_NAME;
        //        items.EMP_GOAL = item.EMP_GOAL;
        //        items.WEEK_1 = item.WEEK_1 != null ? DataConverter.GetNumberFormat(item.WEEK_1, 0) : "0";
        //        items.WEEK_2 = item.WEEK_2 != null ? DataConverter.GetNumberFormat(item.WEEK_2, 0) : "0";
        //        items.WEEK_3 = item.WEEK_3 != null ? DataConverter.GetNumberFormat(item.WEEK_3, 0) : "0";
        //        items.WEEK_4 = item.WEEK_4 != null ? DataConverter.GetNumberFormat(item.WEEK_4, 0) : "0";
        //        items.WEEK_5 = item.WEEK_5 != null ? DataConverter.GetNumberFormat(item.WEEK_5, 0) : "0";

        //        // };
        //        result.DetailWeekHistories.Add(items);
        //    }
        //    if (detail)
        //    {
        //        foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
        //        {

        //            var heads = new HeaderWeek()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = false,
        //                sort = DataConverter.GetString(h),
        //               // style = "{ 'width' : '20px'} ",
        //            };
        //            result.HeaderWeek.Add(heads);
        //        }
        //    }
        //    foreach (var h in models.Header)
        //    {
        //        var heads = new HeaderWeek()
        //        {
        //            head = h.Value,
        //            field = "WEEK_" + DataConverter.GetString(h.Value),
        //            cansort = false,
        //            sort = "WEEK_" + DataConverter.GetString(h.Value),
        //            style = "width:80px;vertical-align:initial;text-align:right",
        //        };
        //        result.HeaderWeek.Add(heads);
        //    }
        //    return result;
        //}

        //[HttpPost]
        //[Route("GetSalaryByDay")]
        //public async Task<ActionResult> GetSalaryByDay(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSalaryByDay(model);
        //        var result = MapDayModel(serviceResult, model.DETAIL,true);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailDayModel MapDayModel(DayHeaderModel models, bool detail,bool emp)
        //{
        //    var totalRecord = models.DayModel.Any() ? (int)models.DayModel.First().TOTAL_COUNT : 0;
        //    var result = new DetailDayModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };

        //    foreach (var item in models.DayModel)
        //    {
        //        var items = new DetailDayHistory();
        //        //{
        //        items.EMP_CODE = item.EMP_CODE;
        //        items.FULL_NAME = item.FULL_NAME;
        //        items.DEPT_NAME = item.DEPT_NAME;
        //        items.EMP_GOAL = item.EMP_GOAL;
        //        items.D_01 = item.D_01 != null ? DataConverter.GetNumberFormat(item.D_01, 2): "0.00";
        //        items.D_02 = item.D_02 != null ? DataConverter.GetNumberFormat(item.D_02, 2): "0.00";
        //        items.D_03 = item.D_03 != null ? DataConverter.GetNumberFormat(item.D_03, 2): "0.00";
        //        items.D_04 = item.D_04 != null ? DataConverter.GetNumberFormat(item.D_04, 2): "0.00";
        //        items.D_05 = item.D_05 != null ? DataConverter.GetNumberFormat(item.D_05, 2): "0.00";
        //        items.D_06 = item.D_06 != null ? DataConverter.GetNumberFormat(item.D_06, 2): "0.00";
        //        items.D_07 = item.D_07 != null ? DataConverter.GetNumberFormat(item.D_07, 2): "0.00";
        //        items.D_08 = item.D_08 != null ? DataConverter.GetNumberFormat(item.D_08, 2): "0.00";
        //        items.D_09 = item.D_09 != null ? DataConverter.GetNumberFormat(item.D_09, 2) : "0.00";
        //        items.D_10 = item.D_10 != null ? DataConverter.GetNumberFormat(item.D_10, 2) : "0.00";
        //        items.D_11 = item.D_11 != null ? DataConverter.GetNumberFormat(item.D_11, 2) : "0.00";
        //        items.D_12 = item.D_12 != null ? DataConverter.GetNumberFormat(item.D_12, 2) : "0.00";
        //        items.D_13 = item.D_13 != null ? DataConverter.GetNumberFormat(item.D_13, 2) : "0.00";
        //        items.D_14 = item.D_14 != null ? DataConverter.GetNumberFormat(item.D_14, 2) : "0.00";
        //        items.D_15 = item.D_15 != null ? DataConverter.GetNumberFormat(item.D_15, 2) : "0.00";
        //        items.D_16 = item.D_16 != null ? DataConverter.GetNumberFormat(item.D_16, 2) : "0.00";
        //        items.D_17 = item.D_17 != null ? DataConverter.GetNumberFormat(item.D_17, 2) : "0.00";
        //        items.D_18 = item.D_18 != null ? DataConverter.GetNumberFormat(item.D_18, 2) : "0.00";
        //        items.D_19 = item.D_19 != null ? DataConverter.GetNumberFormat(item.D_19, 2) : "0.00";
        //        items.D_20 = item.D_20 != null ? DataConverter.GetNumberFormat(item.D_20, 2) : "0.00";
        //        items.D_21 = item.D_21 != null ? DataConverter.GetNumberFormat(item.D_21, 2) : "0.00";
        //        items.D_22 = item.D_22 != null ? DataConverter.GetNumberFormat(item.D_22, 2) : "0.00";
        //        items.D_23 = item.D_23 != null ? DataConverter.GetNumberFormat(item.D_23, 2) : "0.00";
        //        items.D_24 = item.D_24 != null ? DataConverter.GetNumberFormat(item.D_24, 2) : "0.00";
        //        items.D_25 = item.D_25 != null ? DataConverter.GetNumberFormat(item.D_25, 2) : "0.00";
        //        items.D_26 = item.D_26 != null ? DataConverter.GetNumberFormat(item.D_26, 2) : "0.00";
        //        items.D_27 = item.D_27 != null ? DataConverter.GetNumberFormat(item.D_27, 2) : "0.00";
        //        items.D_28 = item.D_28 != null ? DataConverter.GetNumberFormat(item.D_28, 2) : "0.00";
        //        items.D_29 = item.D_29 != null ? DataConverter.GetNumberFormat(item.D_29, 2) : "0.00";
        //        items.D_30 = item.D_30 != null ? DataConverter.GetNumberFormat(item.D_30, 2) : "0.00";
        //        items.D_31 = item.D_31 != null ? DataConverter.GetNumberFormat(item.D_31, 2) : "0.00";
        //        // };
        //        result.DetailDayHistories.Add(items);
        //    }
        //    if (detail)
        //    {
        //        foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
        //        {

        //            if (!emp && (DataConverter.GetString(h) == "EMP_CODE" || DataConverter.GetString(h) == "FULL_NAME"))
        //            {

        //            }
        //            else {
        //                var heads = new HeaderDay()
        //                {
        //                    head = GetEnumDescription(h),
        //                    field = DataConverter.GetString(h),
        //                    cansort = false,
        //                    sort = DataConverter.GetString(h),

        //                };
        //                result.HeaderDay.Add(heads);
        //            }            
        //        }
        //    }

        //    foreach (var head in models.Header)
        //    {

        //        var heads = new HeaderDay()
        //        {
        //            head = head.Value,
        //            field = "D_" + DataConverter.GetString(head.Value),
        //            cansort = false,
        //            sort = DataConverter.GetString(head.Value),
        //            style = "width:80px;vertical-align:initial;text-align:right",
        //            fieldType = "linkDate",
        //            functionCall = "ShowDate"
        //        };
        //        result.HeaderDay.Add(heads);
        //    }


        //    return result;
        //}

        //[HttpPost]
        //[Route("GetSalaryByWeek")]
        //public async Task<ActionResult> GetSalaryByWeek(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSalaryByWeek(model);
        //        var result = MapWeekModel(serviceResult, model.DETAIL,true);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailWeekModel MapWeekModel(WeekHeaderModel models, bool detail,bool emp)
        //{
        //    var totalRecord = models.WeekModel.Any() ? (int)models.WeekModel.First().TOTAL_COUNT : 0;
        //    var result = new DetailWeekModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models.WeekModel)
        //    {
        //        var items = new DetailWeekHistory();
        //        //{
        //        items.EMP_CODE = item.EMP_CODE;
        //        items.FULL_NAME = item.FULL_NAME;
        //        items.DEPT_NAME = item.DEPT_NAME;
        //        items.EMP_GOAL = item.EMP_GOAL;
        //        items.WEEK_1 = item.WEEK_1 != null ? DataConverter.GetNumberFormat(item.WEEK_1, 2) : "0.00";
        //        items.WEEK_2 = item.WEEK_2 != null ? DataConverter.GetNumberFormat(item.WEEK_2, 2) : "0.00";
        //        items.WEEK_3 = item.WEEK_3 != null ? DataConverter.GetNumberFormat(item.WEEK_3, 2) : "0.00";
        //        items.WEEK_4 = item.WEEK_4 != null ? DataConverter.GetNumberFormat(item.WEEK_4, 2) : "0.00";
        //        items.WEEK_5 = item.WEEK_5 != null ? DataConverter.GetNumberFormat(item.WEEK_5, 2) : "0.00";

        //        // };
        //        result.DetailWeekHistories.Add(items);
        //        //var items = new DetailWeekHistory()
        //        //{
        //        //    EMP_CODE = item.EMP_CODE,
        //        //    FULL_NAME = item.FULL_NAME,
        //        //    DEPT_NAME = item.DEPT_NAME,
        //        //    EMP_GOAL = item.EMP_GOAL,

        //        //    WEEK_1 = DataConverter.GetNumberFormat(item.WEEK_1, 2),
        //        //    WEEK_2 = DataConverter.GetNumberFormat(item.WEEK_2, 2),
        //        //    WEEK_3 = DataConverter.GetNumberFormat(item.WEEK_3, 2),
        //        //    WEEK_4 = DataConverter.GetNumberFormat(item.WEEK_4, 2),
        //        //    WEEK_5 = DataConverter.GetNumberFormat(item.WEEK_5, 2),

        //        //};
        //        //result.DetailWeekHistories.Add(items);
        //    }
        //    if (detail)
        //    {
        //        foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
        //        {
        //            if (!emp && (DataConverter.GetString(h) == "EMP_CODE" || DataConverter.GetString(h) == "FULL_NAME"))
        //            {

        //            }
        //            else {
        //                var heads = new HeaderWeek()
        //                {
        //                    head = GetEnumDescription(h),
        //                    field = DataConverter.GetString(h),
        //                    cansort = false,
        //                    sort = DataConverter.GetString(h),

        //                };
        //                result.HeaderWeek.Add(heads);
        //            }
        //        }
        //    }
        //    foreach (var h in models.Header)
        //    {
        //        var heads = new HeaderWeek()
        //        {
        //            head =  h.Value,
        //            field = "WEEK_" + DataConverter.GetString(h.Value),
        //            cansort = false,
        //            sort = "WEEK_" + DataConverter.GetString(h.Value),
        //            style = "width:80px;vertical-align:initial;text-align:right",
        //        };
        //        result.HeaderWeek.Add(heads);
        //    }
        //    return result;
        //}

        //[HttpPost]
        //[Route("GetSalaryGroupByDay")]
        //public async Task<ActionResult> GetSalaryGroupByDay(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSalaryGroupByDay(model);
        //        var result = MapDayModel(serviceResult, model.DETAIL,false);
        //        return OK(result);
        //    }
        //    return OK("");
        //}

        //[HttpPost]
        //[Route("GetSalaryByWeek")]
        //public async Task<ActionResult> GetSalaryGroupByWeek(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSalaryGroupByWeek(model);
        //        var result = MapWeekModel(serviceResult, model.DETAIL,false);
        //        return OK(result);
        //    }
        //    return OK("");
        //}

        [HttpPost]
        [Route("GetSearchUser")]
        public async Task<ActionResult> GetSearchUser(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSearchUser(model);
                var result = MapSearchUserModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public SearchUserModel MapSearchUserModel(List<UserModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new SearchUserModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new DetailUserHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    SUP_DEPT_NAME = item.SUP_DEPT_NAME

                };
                result.DetailUserHistories.Add(items);
            }

            foreach (UserData h in Enum.GetValues(typeof(UserData)))
            {

                var heads = new HeaderUser()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    //style = "{ 'width' : '20px'} ",
                };
                result.HeaderUser.Add(heads);


            }
            return result;
        }

        //[HttpPost]
        //[Route("GetSalaryByDayAndCode")]
        //public async Task<ActionResult> GetSalaryByDayAndCode(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetSalaryByDayAndCode(model);
        //        var result = MapDayModel(serviceResult, model.DETAIL,false);
        //        return OK(result);
        //    }
        //    return OK("");
        //}

        //[HttpPost]
        //[Route("GetReviseByDayAndCode")]
        //public async Task<ActionResult> GetReviseByDayAndCode(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetReviseByDayAndCode(model);
        //        var result = MapDetailReviseModel(serviceResult,true);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailReviseModel MapDetailReviseModel(List<ReviseModel> models,bool isProject)
        //{
        //    var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
        //    var result = new DetailReviseModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models)
        //    {
        //        var items = new DetailReviseHistory();

        //        if (isProject) {
        //            items.PROJECT_NAME = item.PROJECT_NAME;
        //            items.ITEM = item.ITEM;
        //        }

        //        items.ACTUAL_DATE = item.ACTUAL_DATE;
        //        items.ACTUAL = item.ACTUAL;
        //        items.PRICE = item.PRICE != null ? DataConverter.GetNumberFormat(item.PRICE, 2) : "0.00";
        //        items.SALARY = item.SALARY != null ? DataConverter.GetNumberFormat(item.SALARY, 2) : "0.00";
        //        result.DetailReviseHistories.Add(items);
        //    }
        //    if (isProject) {
        //        foreach (Project h in Enum.GetValues(typeof(Project)))
        //        {

        //            var heads = new HeaderRevise()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = true,
        //                sort = DataConverter.GetString(h),
        //                //style = "{ 'width' : '20px'} ",
        //            };
        //            result.HeaderRevise.Add(heads);
        //        }
        //    }
        //    foreach (Revise h in Enum.GetValues(typeof(Revise)))
        //    {

        //        var heads = new HeaderRevise()
        //        {
        //            head = GetEnumDescription(h),
        //            field = DataConverter.GetString(h),
        //            cansort = true,
        //            sort = DataConverter.GetString(h),
        //            //style = "{ 'width' : '20px'} ",
        //        };
        //        result.HeaderRevise.Add(heads);
        //    }
        //    return result;
        //}

        //[HttpPost]
        //[Route("GetOtherByDayAndCode")]
        //public async Task<ActionResult> GetOtherByDayAndCode(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
        //        var serviceResult = await service.GetOtherByDayAndCode(model);
        //        var result = MapDetailReviseModel(serviceResult, false);
        //        return OK(result);
        //    }
        //    return OK("");
        //}

        //public DetailGoalModel MapDetailGoalModel(DayHeaderModel models, bool detail, bool sum)
        //{
        //    var totalRecord = models.DayModel.Any() ? (int)models.DayModel.First().TOTAL_COUNT : 0;
        //    var result = new DetailGoalModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models.DayModel)
        //    {
        //        var items = new DetailGoalHistory();
        //        //{
        //        items.EMP_CODE = item.EMP_CODE;
        //        items.FULL_NAME = item.FULL_NAME;
        //        items.DEPT_NAME = item.DEPT_NAME;
        //        items.EMP_GOAL = item.EMP_GOAL;
        //        items.D_01 = item.D_01 != null ? DataConverter.GetNumberFormat(item.D_01, 2) : "0.00";
        //        items.D_02 = item.D_02 != null ? DataConverter.GetNumberFormat(item.D_02, 2) : "0.00";
        //        items.D_03 = item.D_03 != null ? DataConverter.GetNumberFormat(item.D_03, 2) : "0.00";
        //        items.D_04 = item.D_04 != null ? DataConverter.GetNumberFormat(item.D_04, 2) : "0.00";
        //        items.D_05 = item.D_05 != null ? DataConverter.GetNumberFormat(item.D_05, 2) : "0.00";
        //        items.D_06 = item.D_06 != null ? DataConverter.GetNumberFormat(item.D_06, 2) : "0.00";
        //        items.D_07 = item.D_07 != null ? DataConverter.GetNumberFormat(item.D_07, 2) : "0.00";
        //        items.D_08 = item.D_08 != null ? DataConverter.GetNumberFormat(item.D_08, 2) : "0.00";
        //        items.D_09 = item.D_09 != null ? DataConverter.GetNumberFormat(item.D_09, 2) : "0.00";
        //        items.D_10 = item.D_10 != null ? DataConverter.GetNumberFormat(item.D_10, 2) : "0.00";
        //        items.D_11 = item.D_11 != null ? DataConverter.GetNumberFormat(item.D_11, 2) : "0.00";
        //        items.D_12 = item.D_12 != null ? DataConverter.GetNumberFormat(item.D_12, 2) : "0.00";
        //        items.D_13 = item.D_13 != null ? DataConverter.GetNumberFormat(item.D_13, 2) : "0.00";
        //        items.D_14 = item.D_14 != null ? DataConverter.GetNumberFormat(item.D_14, 2) : "0.00";
        //        items.D_15 = item.D_15 != null ? DataConverter.GetNumberFormat(item.D_15, 2) : "0.00";
        //        items.D_16 = item.D_16 != null ? DataConverter.GetNumberFormat(item.D_16, 2) : "0.00";
        //        items.D_17 = item.D_17 != null ? DataConverter.GetNumberFormat(item.D_17, 2) : "0.00";
        //        items.D_18 = item.D_18 != null ? DataConverter.GetNumberFormat(item.D_18, 2) : "0.00";
        //        items.D_19 = item.D_19 != null ? DataConverter.GetNumberFormat(item.D_19, 2) : "0.00";
        //        items.D_20 = item.D_20 != null ? DataConverter.GetNumberFormat(item.D_20, 2) : "0.00";
        //        items.D_21 = item.D_21 != null ? DataConverter.GetNumberFormat(item.D_21, 2) : "0.00";
        //        items.D_22 = item.D_22 != null ? DataConverter.GetNumberFormat(item.D_22, 2) : "0.00";
        //        items.D_23 = item.D_23 != null ? DataConverter.GetNumberFormat(item.D_23, 2) : "0.00";
        //        items.D_24 = item.D_24 != null ? DataConverter.GetNumberFormat(item.D_24, 2) : "0.00";
        //        items.D_25 = item.D_25 != null ? DataConverter.GetNumberFormat(item.D_25, 2) : "0.00";
        //        items.D_26 = item.D_26 != null ? DataConverter.GetNumberFormat(item.D_26, 2) : "0.00";
        //        items.D_27 = item.D_27 != null ? DataConverter.GetNumberFormat(item.D_27, 2) : "0.00";
        //        items.D_28 = item.D_28 != null ? DataConverter.GetNumberFormat(item.D_28, 2) : "0.00";
        //        items.D_29 = item.D_29 != null ? DataConverter.GetNumberFormat(item.D_29, 2) : "0.00";
        //        items.D_30 = item.D_30 != null ? DataConverter.GetNumberFormat(item.D_30, 2) : "0.00";
        //        items.D_31 = item.D_31 != null ? DataConverter.GetNumberFormat(item.D_31, 2) : "0.00";

        //        // };
        //        //result.DetailDayHistories.Add(items);
        //        if (sum)
        //        {
        //            items.SUM_GOAL = item.SUM_GOAL != null ? DataConverter.GetNumberFormat(item.SUM_GOAL, 2) : "0.00";
        //            items.SUM_WORK = item.SUM_WORK != null ? DataConverter.GetNumberFormat(item.SUM_WORK, 2) : "0.00";
        //        }
        //        result.DetailGoalHistories.Add(items);
        //    }
        //    if (detail)
        //    {
        //        foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
        //        {
        //            if (DataConverter.GetString(h) == "EMP_CODE")
        //            {
        //                var heads = new HeaderGoal()
        //                {
        //                    head = GetEnumDescription(h),
        //                    field = DataConverter.GetString(h),
        //                    cansort = false,
        //                    sort = DataConverter.GetString(h),
        //                    // style = "{ 'width' : '20px'} ",
        //                    fieldType = "linkEmpCode",
        //                    functionCall = "Show"
        //                };
        //                result.HeaderGoal.Add(heads);
        //            }
        //            else
        //            {
        //                var heads = new HeaderGoal()
        //                {
        //                    head = GetEnumDescription(h),
        //                    field = DataConverter.GetString(h),
        //                    cansort = false,
        //                    sort = DataConverter.GetString(h),
        //                    // style = "{ 'width' : '20px'} ",
        //                };
        //                result.HeaderGoal.Add(heads);
        //            }
        //        }
        //    }
        //    foreach (var h in models.Header)
        //    {

        //        var heads = new HeaderGoal()
        //        {
        //            head = h.Value,
        //            field = "D_" + h.Value,
        //            cansort = false,
        //            sort = "D_" + h.Value,
        //            //style = "{ 'width' : '20px'} ",
        //        };
        //        result.HeaderGoal.Add(heads);


        //    }
        //    if (sum)
        //    {
        //        foreach (DetailSum h in Enum.GetValues(typeof(DetailSum)))
        //        {

        //            var heads = new HeaderGoal()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = false,
        //                sort = DataConverter.GetString(h),
        //                style = "{ 'width' : '20px'} ",
        //            };
        //            result.HeaderGoal.Add(heads);


        //        }
        //    }
        //    return result;
        //}
        #endregion

        [HttpPost]
        [Route("GetReportNCR")]
        public async Task<ActionResult> GetReportNCR(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetReportNCR(model);
                var result = MapNCRModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailNCRModel MapNCRModel(NCRHeaderModel models)
        {
            var totalRecord = models.NCRModel.Any() ? (int)models.NCRModel.First().TOTAL_COUNT : 0;
            var result = new DetailNCRModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.NCRModel)
            {
                var items = new DetailNCRHistory();
                //{

                items.DEPT_NAME = item.DEPT_NAME;

                items.WEEK_1 = item.WEEK_1;
                items.WEEK_2 = item.WEEK_2;
                items.WEEK_3 = item.WEEK_3;
                items.WEEK_4 = item.WEEK_4;
                items.WEEK_5 = item.WEEK_5;

                result.DetailNCRHistories.Add(items);

            }

            foreach (Dept h in Enum.GetValues(typeof(Dept)))
            {
              
                    var heads = new HeaderNCR()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = false,
                        sort = DataConverter.GetString(h),

                    };
                    result.HeaderNCR.Add(heads);
                
            }
          
            foreach (var h in models.Header)
            {
                var heads = new HeaderNCR()
                {
                    head = h.Text,
                    field = "WEEK_" + DataConverter.GetString(h.Value),
                    cansort = false,
                    sort = "WEEK_" + DataConverter.GetString(h.Value),
                    style = "width:150px;vertical-align:initial;text-align:center",
                };
                result.HeaderNCR.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetReportVT")]
        public async Task<ActionResult> GetReportVT(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetReportVT(model);
                var result = MapNCRModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryWorkDayForWeek")]
        public async Task<ActionResult> GetSummaryWorkDayForWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummaryWorkDayForWeek(model);
                var result = MapDetailGoalModel(serviceResult,true,false);
                return OK(result);
            }
            return OK("");
        }

        public DetailGoalSumModel MapDetailGoalModel(GoalHeaderModel models, bool detail, bool overgoal)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailGoalSumModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailGoalSumHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    PART = DataConverter.GetNumberFormat(item.PART, 0),
                    OTHER = DataConverter.GetNumberFormat(item.OTHER, 0),
                    REVISE = DataConverter.GetNumberFormat(item.REVISE, 0),
                    WELD = DataConverter.GetNumberFormat(item.WELD, 0),
                    DIMENSION = DataConverter.GetNumberFormat(item.DIMENSION, 0),
                    DEDUCT = DataConverter.GetNumberFormat(item.DEDUCT, 0),
                    SUM_GOAL = DataConverter.GetNumberFormat(item.SUM_GOAL, 0),
                    OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0)
                };
                result.DetailGoalSumHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                    var heads = new HeaderGoalSum()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderGoalSum.Add(heads);
                }
            }
            foreach (var head in models.Header)
            {

                var heads = new HeaderGoalSum()
                {
                    head = head.Value,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = "vertical-align:initial; text-align:right",
                    fieldType = "linkEmpProcess",
                    functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderGoalSum.Add(heads);
            }
            if (overgoal)
            {
                var headsum = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ที่ได้)",
                    field = "SUM_GOAL",
                    cansort = true,
                    sort = "SUM_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(headsum);
                var heads = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ขาด)",
                    field = "OVER_GOAL",
                    cansort = true,
                    sort = "OVER_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(heads);

            }
            return result;
        }

        public DetailGoalSumModel MapDetailGoalLessModel(GoalHeaderModel models, bool detail, bool overgoal)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailGoalSumModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailGoalSumHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    //PART = DataConverter.GetNumberFormat(item.PART, 0),
                    //OTHER = DataConverter.GetNumberFormat(item.OTHER, 0),
                    //REVISE = DataConverter.GetNumberFormat(item.REVISE, 0),
                    //WELD = DataConverter.GetNumberFormat(item.WELD, 0),
                    //DIMENSION = DataConverter.GetNumberFormat(item.DIMENSION, 0),
                    //DEDUCT = DataConverter.GetNumberFormat(item.DEDUCT, 0),
                    SUM_GOAL = DataConverter.GetNumberFormat(item.SUM_GOAL, 0),
                    OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0)
                };
                result.DetailGoalSumHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                    var heads = new HeaderGoalSum()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderGoalSum.Add(heads);
                }
            }
            //foreach (var head in models.Header)
            //{

            //    var heads = new HeaderGoalSum()
            //    {
            //        head = head.Value,

            //        field = head.Value,
            //        cansort = true,
            //        sort = head.Value,
            //        style = "vertical-align:initial; text-align:right",
            //        fieldType = "linkEmpProcess",
            //        functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
            //    };
            //    result.HeaderGoalSum.Add(heads);
            //}
            if (overgoal)
            {
                var headsum = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ที่ได้)",
                    field = "SUM_GOAL",
                    cansort = true,
                    sort = "SUM_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(headsum);
                var heads = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ขาด)",
                    field = "OVER_GOAL",
                    cansort = true,
                    sort = "OVER_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(heads);

            }
            return result;
        }

        [HttpPost]
        [Route("GetSummarySalaryDayForWeek")]
        public async Task<ActionResult> GetSummarySalaryDayForWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryDayForWeek(model);
                var result = MapDetailSalarySummaryModel(serviceResult,true);
                return OK(result);
            }
            return OK("");
        }
        public DetailSalarySummaryModel MapDetailSalarySummaryModel(List<SalarySummaryModel> models,bool detail)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailSalarySummaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new DetailSalarySummaryHistory();
                //{
                items.EMP_CODE = item.EMP_CODE;
                items.FULL_NAME = item.FULL_NAME;
                items.DEPT_NAME = item.DEPT_NAME;
                items.EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0); ;
                items.GOAL = item.GOAL;
                items.PART = DataConverter.GetNumberFormat(item.PART,0);
                items.OTHER = DataConverter.GetNumberFormat(item.OTHER, 0);
                items.REVISE = DataConverter.GetNumberFormat(item.REVISE,0);
                items.WELD = DataConverter.GetNumberFormat(item.WELD, 0);
                items.DIMENSION = DataConverter.GetNumberFormat(item.DIMENSION, 0);
                items.LEVEL_SALARY = DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2);
                items.LEVEL_QUALITY = DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2);
                items.LEVEL_PROCEDURE = DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2);
                items.SUM_PART =  DataConverter.GetNumberFormat(item.SUM_PART, 0) ;
                items.OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0);
                items.SALARY = DataConverter.GetNumberFormat(item.SALARY, 2);
                items.SALARY_QUALITY = DataConverter.GetNumberFormat(item.SALARY_QUALITY, 2) ;
                items.SALARY_PROCEDURE =  DataConverter.GetNumberFormat(item.SALARY_PROCEDURE, 2) ;
                items.OTHER_SALARY =  DataConverter.GetNumberFormat(item.OTHER_SALARY, 2) ;
                items.REVISE_SALARY =  DataConverter.GetNumberFormat(item.REVISE_SALARY, 2) ;
                items.WELD_SALARY = DataConverter.GetNumberFormat(item.WELD_SALARY, 2) ;
                items.DIMENSION_SALARY = DataConverter.GetNumberFormat(item.DIMENSION_SALARY, 2) ;
                items.MAIN =  DataConverter.GetNumberFormat(item.MAIN, 2) ;
                items.VT_INSPEC = item.VT_INSPEC > 0 ? DataConverter.GetNumberFormat(item.VT_INSPEC, 0) : "-";
                items.VT_QPC = item.VT_QPC > 0 ? DataConverter.GetNumberFormat(item.VT_QPC, 0) : "-";
                items.VT_OTHER = item.VT_OTHER > 0 ? DataConverter.GetNumberFormat(item.VT_OTHER, 0) : "-";
                items.VT_DESIGN_CHANGE = item.VT_DESIGN_CHANGE > 0 ? DataConverter.GetNumberFormat(item.VT_DESIGN_CHANGE, 0) : "-";

                items.MINUS =  DataConverter.GetNumberFormat(item.MINUS, 2) ;
                items.PLUS =DataConverter.GetNumberFormat(item.PLUS, 2) ;
                items.TEMPO = DataConverter.GetNumberFormat(item.TEMPO, 2);
                items.POSITION =  DataConverter.GetNumberFormat(item.POSITION, 2);
                items.SUM_SALARY =  DataConverter.GetNumberFormat(item.SUM_SALARY, 2) ;
                items.FIX_SALARY = DataConverter.GetNumberFormat(item.FIX_SALARY, 2) ;

                items.MINUS_2 = DataConverter.GetNumberFormat(item.MINUS_2, 2);
                items.POSITION_2 = DataConverter.GetNumberFormat(item.POSITION_2, 2);
                items.SUM_SALARY_2 = DataConverter.GetNumberFormat(item.SUM_SALARY_2, 2);
                result.DetailSalarySummaryHistories.Add(items);

                //items.LEVEL_SALARY = item.LEVEL_SALARY > 0 ? DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2) : "0.00";
                //items.LEVEL_QUALITY = item.LEVEL_QUALITY > 0 ? DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2) : "0.00";
                //items.LEVEL_PROCEDURE = item.LEVEL_PROCEDURE > 0 ? DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2) : "0.00";
                //items.SUM_PART = item.SUM_PART > 0 ? DataConverter.GetNumberFormat(item.SUM_PART, 0) : "0";
                //items.OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0);
                //items.SALARY = item.SALARY > 0 ? DataConverter.GetNumberFormat(item.SALARY, 2) : "0.00";
                //items.SALARY_QUALITY = item.SALARY_QUALITY > 0 ? DataConverter.GetNumberFormat(item.SALARY_QUALITY, 2) : "0.00";
                //items.SALARY_PROCEDURE = item.SALARY_PROCEDURE > 0 ? DataConverter.GetNumberFormat(item.SALARY_PROCEDURE, 2) : "0.00";
                //items.OTHER_SALARY = item.OTHER_SALARY > 0 ? DataConverter.GetNumberFormat(item.OTHER_SALARY, 2) : "0.00";
                //items.REVISE_SALARY = item.REVISE_SALARY > 0 ? DataConverter.GetNumberFormat(item.REVISE_SALARY, 2) : "0.00";
                //items.WELD_SALARY = item.WELD_SALARY > 0 ? DataConverter.GetNumberFormat(item.WELD_SALARY, 2) : "0.00";
                //items.DIMENSION_SALARY = item.DIMENSION_SALARY > 0 ? DataConverter.GetNumberFormat(item.DIMENSION_SALARY, 2) : "0.00";
                //items.MAIN = item.MAIN > 0 ? DataConverter.GetNumberFormat(item.MAIN, 2) : "0.00";
                //items.VT_INSPEC = item.VT_INSPEC > 0 ? DataConverter.GetNumberFormat(item.VT_INSPEC, 0) : "-";
                //items.VT_QPC = item.VT_QPC > 0 ? DataConverter.GetNumberFormat(item.VT_QPC, 0) : "-";
                //items.VT_OTHER = item.VT_OTHER > 0 ? DataConverter.GetNumberFormat(item.VT_OTHER, 0) : "-";
                //items.VT_DESIGN_CHANGE = item.VT_DESIGN_CHANGE > 0 ? DataConverter.GetNumberFormat(item.VT_DESIGN_CHANGE, 0) : "-";

                //items.MINUS = item.MINUS > 0 ? DataConverter.GetNumberFormat(item.MINUS, 2) : "0.00";
                //items.PLUS = item.PLUS > 0 ? DataConverter.GetNumberFormat(item.PLUS, 2) : "0.00";
                //items.TEMPO = item.TEMPO > 0 ? DataConverter.GetNumberFormat(item.TEMPO, 2) : "0.00";
                //items.POSITION = item.POSITION > 0 ? DataConverter.GetNumberFormat(item.POSITION, 2) : "0.00";
                //items.SUM_SALARY = item.SUM_SALARY > 0 ? DataConverter.GetNumberFormat(item.SUM_SALARY, 2) : "0.00";
                //items.FIX_SALARY = item.FIX_SALARY > 0 ? DataConverter.GetNumberFormat(item.FIX_SALARY, 2) : "0.00";
                //result.DetailSalarySummaryHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                   
                    var heads = new HeaderSalarySummary()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = DataConverter.GetString(h).Contains("GOAL")? "vertical-align:initial;text-align:center" : "",
                        //fieldType = "linkEmpCode",
                        //functionCall = "Show"
                    };
                    result.HeaderSalarySummary.Add(heads);
                   
                }
            }
            foreach (SalarySummary h in Enum.GetValues(typeof(SalarySummary)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "SUM_SALARY":
                        {
                            sty = "vertical-align:initial;text-align:right;background-color: powderblue;";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                }
                var heads = new HeaderSalarySummary()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                    // style = "{ 'width' : '20px'} ",
                    //fieldType = "linkEmpCode",
                    //functionCall = "Show"
                    fieldType = DataConverter.GetString(h).Contains("VT_") ? "linkVT" : "",
                    functionCall = DataConverter.GetString(h).Contains("VT_") ? "Show" :""
                };
                result.HeaderSalarySummary.Add(heads);

            }
            
            return result;
        }

        [HttpPost]
        [Route("GetSummaryWorkDayForWeekLess")]
        public async Task<ActionResult> GetSummaryWorkDayForWeekLess(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummaryWorkDayForWeekLess(model);
                var result = MapDetailGoalLessModel(serviceResult, true,true);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetSummaryWorkDayForWeekWeldLess")]
        public async Task<ActionResult> GetSummaryWorkDayForWeekWeldLess(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummaryWorkDayForWeekWeldLess(model);
                var result = MapDetailGoalLessModel(serviceResult, true, true);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetSummarySalaryPlus")]
        public async Task<ActionResult> GetSummarySalaryPlus(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryPlus(model);
                var result = MapDetailPlusModel(serviceResult,true);
                return OK(result);
            }
            return OK("");
        }
        public DetailPlusModel MapDetailPlusModel(List<PlusModel> models,bool detail)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailPlusModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new DetailPlusHistory();
                //{
                items.ROW_NUM = item.ROW_NUM;
                items.EMP_CODE = item.EMP_CODE;
                items.FULL_NAME = item.FULL_NAME;
                items.DEPT_NAME = item.DEPT_NAME;

                items.FIX_SALARY = DataConverter.GetNumberFormat(item.FIX_SALARY, 2) ;
                items.POSITION_SALARY =  DataConverter.GetNumberFormat(item.POSITION_SALARY, 2) ;
                items.ALL_SALARY = DataConverter.GetNumberFormat(item.ALL_SALARY, 2) ;
                items.SUM_SALARY =  DataConverter.GetNumberFormat(item.SUM_SALARY, 2) ;
           
                result.DetailPlusHistories.Add(items);
            }
            var headsRow = new HeaderPlus()
            {
                head = "ลำดับ",
                field = "ROW_NUM",
                cansort = false,
                sort =  "vertical-align:initial;text-align:center" ,
                //fieldType = "linkEmpCode",
                //functionCall = "Show"
            };
            result.HeaderPlus.Add(headsRow);
            if (detail)
            {
                foreach (DetailEmp h in Enum.GetValues(typeof(DetailEmp)))
                {

                    var heads = new HeaderPlus()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = false,
                        sort = DataConverter.GetString(h),
                        style = DataConverter.GetString(h).Contains("GOAL") ? "vertical-align:initial;text-align:center" : "",
                        //fieldType = "linkEmpCode",
                        //functionCall = "Show"
                    };
                    result.HeaderPlus.Add(heads);

                }
            }
            foreach (Plus h in Enum.GetValues(typeof(Plus)))
            {

                var heads = new HeaderPlus()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = false,
                    sort = DataConverter.GetString(h),
                    style = "vertical-align:initial;text-align:right",
   
                };
                result.HeaderPlus.Add(heads);

            }

            return result;
        }

        [HttpPost]
        [Route("GetSummarySalaryMinus")]
        public async Task<ActionResult> GetSummarySalaryMinus(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryMinus(model);
                var result = MapDetailPlusModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummarySalaryTotal")]
        public async Task<ActionResult> GetSummarySalaryTotal(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryTotal(model);
                var result = MapDetailPlusModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("CheckSession")]
        public  ActionResult CheckSession()
        {
          
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return OK(new ResultModel() { MESSAGE = "Session Expire", CODE = "55" }, string.Empty, HttpStatusCode.OK);
            }
            return OK(new ResultModel() { MESSAGE = string.Empty, CODE = "01" }, string.Empty, HttpStatusCode.OK);

        }

        [HttpPost]
        [Route("GetSummaryWorkDayForWeekWeld")]
        public async Task<ActionResult> GetSummaryWorkDayForWeekWeld(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummaryWorkDayForWeekWeld(model);
                var result = MapDetailSummaryWorkModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }

        public DetailSummaryWorkModel MapDetailSummaryWorkModel(SummaryWorkHeaderModel models, bool detail)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailSummaryWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailSummaryWorkHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 0),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 0),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 0),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 0),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 0),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 0),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 0)

                };
                result.DetailSummaryWorkHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                    var heads = new HeaderSummaryWork()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderSummaryWork.Add(heads);
                }
            }
            foreach (var head in models.Header)
            {

                var heads = new HeaderSummaryWork()
                {
                    head = head.Value,

                    field = head.Text,
                    cansort = true,
                    sort = head.Value,
                    style = "vertical-align:initial; text-align:right",
                    fieldType = "linkEmpProcess",
                    functionCall = "Show",
                    //  functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderSummaryWork.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetSummarySalaryForWeekWeld")]
        public async Task<ActionResult> GetSummarySalaryForWeekWeld(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryForWeekWeld(model);
                var result = MapDetailSummarSalaryModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }

        public DetailSummarySalaryModel MapDetailSummarSalaryModel(SummarySalaryHeaderModel models, bool detail)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailSummarySalaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailSummarySalaryHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    WORK_TYPE = item.WORK_TYPE,
                    CERT = item.CERT,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,

                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 0),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 0),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 0),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 0),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 0),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 0),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 0),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 0),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 0),
                    //DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    //DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    //DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_38 = item.DATA_38,
                    DATA_39 = item.DATA_39,
                    DATA_40 = item.DATA_40,
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 0),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 0),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 0),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 0),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 0),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 0),
                };
                result.DetailSummarySalaryHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoalWeld h in Enum.GetValues(typeof(DetailEmpGoalWeld)))
                {
                    var heads = new HeaderSummarySalary()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderSummarySalary.Add(heads);
                }
            }
            foreach (var head in models.Header)
            {

                var heads = new HeaderSummarySalary()
                {
                    head = head.Value,

                    field = head.Text,
                    cansort = true,
                    sort = head.Value,
                    style = "vertical-align:initial; text-align:right",
                    fieldType = DataConverter.GetString(head.Value).Contains("VT_") ? "linkVTWeld" : "",
                    functionCall = DataConverter.GetString(head.Value).Contains("VT_") ? "Show" : ""
                    //  functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderSummarySalary.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetSummarySalaryForWeekWeldSum")]
        public async Task<ActionResult> GetSummarySalaryForWeekWeldSum(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryForWeekWeldSum(model);
                var result = MapDetailSummarSalarySumModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetSummarySalaryForWeekSum")]
        public async Task<ActionResult> GetSummarySalaryForWeekSum(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryForWeekSum(model);
                var result = MapDetailSummarSalarySumModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }

        public DetailSummarySalaryModel MapDetailSummarSalarySumModel(SummarySalaryHeaderModel models, bool detail)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailSummarySalaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailSummarySalaryHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                };
                result.DetailSummarySalaryHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoalSum h in Enum.GetValues(typeof(DetailEmpGoalSum)))
                {
                    var heads = new HeaderSummarySalary()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderSummarySalary.Add(heads);
                }
            }
            foreach (var head in models.Header)
            {

                var heads = new HeaderSummarySalary()
                {
                    head = head.Value,

                    field = head.Text,
                    cansort = true,
                    sort = head.Value,
                    style = "vertical-align:initial; text-align:right",
                    fieldType = DataConverter.GetString(head.Value).Contains("VT_") ? "linkVTWeld" : "",
                    functionCall = DataConverter.GetString(head.Value).Contains("VT_") ? "Show" : ""
                    //  functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderSummarySalary.Add(heads);
            }
            return result;
        }


        #region Emun
        public enum DetailSum
        {
            [Description("เป้าหมายรวม")]
            SUM_GOAL = 0,

            [Description("เป้าหมายที่ได้")]
            SUM_WORK = 1,

        }

        public enum DetailEmp
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("กรุ๊ป")]
            DEPT_NAME = 2

        }

        public enum DetailEmpGoal
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("กรุ๊ป")]
            DEPT_NAME = 2,

           [Description("เป้าหมาย")]
            EMP_GOAL = 3,

            [Description("เป้าหมายรวม(วัน)")]
            GOAL = 4
        }
        public enum DetailEmpGoalWeld
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("กรุ๊ป")]
            DEPT_NAME = 2,

            [Description("ประเภท")]
            WORK_TYPE = 3,

            [Description("Certificate")]
            CERT = 4,

            [Description("เป้าหมาย")]
            EMP_GOAL = 5,

            [Description("เป้าหมายรวม(วัน)")]
            GOAL = 6
        }
        public enum DetailEmpGoalSum
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("กรุ๊ป")]
            DEPT_NAME = 2,

            [Description("เป้าหมาย")]
            EMP_GOAL = 3,
        }

        public enum SumSalary
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            //[Description("กรุ๊ป")]
            //DEPT_NAME = 2,

            [Description("เงินเดือน")]
            SALARY = 3,

            [Description("ค่าตำแหน่ง")]
            SALARY_POSITION = 4,

            [Description("ค่าคุณภาพและค่าขั้นตอน")]
            SALARY_QUALITY = 5,

            [Description("รวม")]
            SALARY_SUM = 6,
        }

        public enum GoalSum
        {
            [Description("เป้าหมาย")]
            EMP_GOAL = 0,

            [Description("PART")]
            PART = 1,

            [Description("OTHER")]
            OTHER = 3,

            [Description("REVISE")]
            REVISE = 4,

            [Description("WELD")]
            WELD = 5,

            [Description("DIMENSION")]
            DIMENSION = 6,
        }

        public enum Salary
        {
            [Description("01")]
            D_01 = 0,

            [Description("02")]
            D_02 = 1,

            [Description("03")]
            D_03 = 3,

            [Description("04")]
            D_04 = 4,

            [Description("05")]
            D_05 = 5,

            [Description("06")]
            D_06 = 6,
            [Description("07")]
            D_07 = 7,
            [Description("08")]
            D_08 = 8,
}

        public enum Work
        {
            [Description("โปรเจค")]
            PROJECT_NAME = 0,
            [Description("ไอเทม")]
            ITEM = 1,
            [Description("เป้าหมาย")]
            EMP_GOAL = 2,
            [Description("จำนวนที่ส่ง")]
            ACTUAL = 3,
            [Description("วันเวลาที่ส่ง")]
            ACTUAL_DATE = 4,
            [Description("pcs มาตรฐาน")]
            LEVEL_SALARY = 5,
            [Description("คุณภาพ")]
            LEVEL_QUALITY = 6,
            [Description("ขั้นตอน")]
            LEVEL_PROCEDURE = 7,
            [Description("เงินมาตรฐาน")]
            SALARY = 8,
            [Description("ราคาคุณภาพ")]
            QUALITY = 9,
            [Description("ราคาขั้นตอน")]
            PROCEDURE = 10,
            [Description("ราคารวม")]
            SUM_SALARY = 11,

        }

        public enum Day
        {
            [Description("01")]
            D_01 = 1,
            [Description("02")]
            D_02 = 2,
            [Description("03")]
            D_03 = 3,
            [Description("04")]
            D_04 = 4,
            [Description("05")]
            D_05 = 5,
            [Description("06")]
            D_06 = 6,
            [Description("07")]
            D_07 = 7,
            [Description("08")]
            D_08 = 8,
            [Description("09")]
            D_09 = 9,
            [Description("10")]
            D_10 = 10,
            [Description("11")]
            D_11 = 11,
            [Description("12")]
            D_12 = 12,
            [Description("13")]
            D_13 = 13,
            [Description("14")]
            D_14 = 14,
            [Description("15")]
            D_15 = 15,
            [Description("16")]
            D_16 = 16,
            [Description("17")]
            D_17 = 17,
            [Description("18")]
            D_18 = 18,
            [Description("19")]
            D_19 = 19,
            [Description("20")]
            D_20 = 20,
            [Description("21")]
            D_21 = 21,
            [Description("22")]
            D_22 = 22,
            [Description("23")]
            D_23 = 23,
            [Description("24")]
            D_24 = 24,
            [Description("25")]
            D_25 = 25,
            [Description("26")]
            D_26 = 26,
            [Description("27")]
            D_27 = 27,
            [Description("28")]
            D_28 = 28,
            [Description("29")]
            D_29 = 29,
            [Description("30")]
            D_30 = 30,
            [Description("31")]
            D_31 = 31,
        }

        public enum Week
        {
            [Description("1")]
            WEEK_1 = 0,

            [Description("2")]
            WEEK_2 = 1,

            [Description("3")]
            WEEK_3 = 3,

            [Description("4")]
            WEEK_4 = 4,

            [Description("5")]
            WEEK_5 = 5,

        }

        public enum UserData
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("ฝ่าย")]
            SUP_DEPT_NAME = 2,

           [Description("กรุ๊ป")]
            DEPT_NAME = 3

        }

        public enum Project
        {
            [Description("โปรเจค")]
            PROJECT_NAME = 0,

            [Description("ไอเทม")]
            ITEM = 1,
        }


        public enum Product
        {
            [Description("วันที่ส่ง")]
            ACTUAL_DATE = 0,

            [Description("จำนวน")]
            ACTUAL = 1,
        }


        public enum Revise
        {
            [Description("วันที่ส่ง")]
            ACTUAL_DATE = 0,

            [Description("จำนวน")]
            ACTUAL = 1,

            [Description("ราคา")]
            PRICE = 2,

            [Description("จำนวนเงิน")]
            SALARY = 3,
        }

        public enum Dept
        {
            [Description("กรุ๊ป")]
            DEPT_NAME = 0,



        }

        public enum SalarySummary
        {
            [Description("PART")]
            PART = 1,

            [Description("OTHER")]
            OTHER =2,

            [Description("REVISE")]
            REVISE =3,

            [Description("WELD")]
            WELD = 4,

            [Description("DIMENSION")]
            DIMENSION = 5,

            [Description("pcs มาตรฐาน")]
            LEVEL_SALARY = 11,
            [Description("คุณภาพ")]
            LEVEL_QUALITY =12,
            [Description("ขั้นตอน")]
            LEVEL_PROCEDURE = 13,
            [Description("รวม part")]
            SUM_PART = 14,
            [Description("เกินเป้าหมาย")]
            OVER_GOAL = 15,
            [Description("เงินมาตรฐาน")]
            SALARY = 16,
            [Description("ราคาคุณภาพ")]
            SALARY_QUALITY =17,
            [Description("ราคาขั้นตอน")]
            SALARY_PROCEDURE =18,
            [Description("ราคา Other")]
            OTHER_SALARY = 19,
            [Description("ราคา Revise")]
            REVISE_SALARY = 20,
            [Description("ราคา Weld")]
            WELD_SALARY = 21,
            [Description("ราคา Dimension")]
            DIMENSION_SALARY = 22,
            [Description("main")]
            MAIN =23,
            [Description("tempo")]
            TEMPO = 24,
            [Description("vt_inspec")]
            VT_INSPEC = 25,
            [Description("vt_qpc")]
            VT_QPC = 26,
            [Description("vt อื่นๆ")]
            VT_OTHER = 27,
            [Description("design change")]
            VT_DESIGN_CHANGE =28,
            [Description("หัก")]
            MINUS = 29,
            [Description("บวก")]
            PLUS = 30,
            [Description("ค่าตำแหน่ง")]
            POSITION = 31,
            [Description("รวมจ่าย")]
            SUM_SALARY = 32,
            [Description("ฐานเงินเดือน")]
            FIX_SALARY = 33,
            [Description("ค่าตำแหน่ง")]
            POSITION_2 = 34,
            [Description("ค่าคุณภาพงาน,ค่าขั้นตอนการทำงาน,เงินเพิ่มอื่นๆ")]
            MINUS_2 = 35,
            [Description("รวมทั้งสิ้น")]
            SUM_SALARY_2 = 46,
        }

        public enum Plus
        {
            [Description("เงินเดือน")]
            FIX_SALARY = 1,
            [Description("ค่าตำแหน่ง")]
            POSITION_SALARY = 2,
            [Description("ค่าคุณภาพงาน,ค่าขั้นตอนการทำงาน,เงินเพิ่มอื่นๆ")]
            ALL_SALARY = 3,
            [Description("รวมทั้งสิ้น")]
            SUM_SALARY = 4,
          

        }

        private string GetEnumDescription(Enum value) {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();      
        }

        #endregion


    }
}