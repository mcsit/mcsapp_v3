﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using Newtonsoft.Json;
using System.Reflection;
using System.ComponentModel;

namespace MCSAPP.WebApp.Controllers
{
    public class UserDepartmentController : BaseController
    {
        // GET: UserDepartment

        [Route("GetMainType")]
        [HttpPost]
        public async Task<ActionResult> GetMainType(SearchModel model)
        {


            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetMainType(model);

            return OK(result);
        }
        [Route("GetLevelType")]
        [HttpPost]
        public async Task<ActionResult> GetLevelType(SearchModel model)
        {


            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetLevelType(model);

            return OK(result);
        }
        [Route("GetMainTypeForProcess")]
        [HttpPost]
        public async Task<ActionResult> GetMainTypeForProcess(SearchModel model)
        {


            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetMainTypeForProcess(model);

            return OK(result);
        }

        [Route("GetHeaderForProcess")]
        [HttpPost]
        public async Task<ActionResult> GetHeaderForProcess(SearchModel model)
        {


            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetHeaderForProcess(model);

            return OK(result);
        }

        [Route("GetHeaderWeek")]
        [HttpPost]
        public async Task<ActionResult> GetHeaderWeek(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetHeaderWeek(model);

            return OK(result);
        }

        [Route("GetProjectForCheckSymbol")]
        [HttpPost]
        public async Task<ActionResult> GetProjectForCheckSymbol(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetProjectForCheckSymbol(model);

            return OK(result);
        }

        [Route("GetProjectItemForCheckSymbol")]
        [HttpPost]
        public async Task<ActionResult> GetProjectItemForCheckSymbol(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetProjectItemForCheckSymbol(model);

            return OK(result);
        }

        [Route("GetProjectForCheckSymbolData")]
        [HttpPost]
        public async Task<ActionResult> GetProjectForCheckSymbolData(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetProjectForCheckSymbolData(model);

            return OK(result);
        }

        [Route("GetProjectItemForCheckSymbolData")]
        [HttpPost]
        public async Task<ActionResult> GetProjectItemForCheckSymbolData(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetProjectItemForCheckSymbolData(model);

            return OK(result);
        }

        [Route("GetGroupName")]
        [HttpPost]
        public async Task<ActionResult> GetGroupName(SearchModel model)
        {
            var result = await MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance().GetGroupName(model);

            return OK(result);
        }

        [HttpPost]
        [Route("CheckSession")]
        public ActionResult CheckSession()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return OK(new ResultModel() { MESSAGE = "Session Expire", CODE = "55" }, string.Empty, HttpStatusCode.OK);
            }
            return OK(new ResultModel() { MESSAGE = string.Empty, CODE = "01" }, string.Empty, HttpStatusCode.OK);

        }
    }
}