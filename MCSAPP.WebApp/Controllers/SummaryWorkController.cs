﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;

namespace MCSAPP.WebApp.Controllers
{
    public class SummaryWorkController : BaseController
    {
        // GET: SummaryWork
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportSummaryForWorkByAutoGas1()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForWorkByAutoGas2()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForWorkByAutoGas3()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForWorkByTaper()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForWorkByPart()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        #region Work
        [HttpPost]
        [Route("GetSummaryForWorkByAutoGas1")]
        public async Task<ActionResult> GetSummaryForWorkByAutoGas1(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByAutoGas1(model);
                var result = MapDetailPart0SummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForWorkByAutoGas2")]
        public async Task<ActionResult> GetSummaryForWorkByAutoGas2(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByAutoGas2(model);
                var result = MapDetail2PartSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForWorkByAutoGas3")]
        public async Task<ActionResult> GetSummaryForWorkByAutoGas3(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByAutoGas3(model);
                var result = MapDetailPartSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForWorkByTaper")]
        public async Task<ActionResult> GetSummaryForWorkByTaper(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByTaper(model);
                var result = MapDetailPartSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForWorkByPart")]
        public async Task<ActionResult> GetSummaryForWorkByPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByPart(model);
                var result = MapDetailPart0SummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        public DetailPartSummaryForWorkModel MapDetailPart0SummaryForWorkModel(AllSummaryForWorkModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);
                var heads = new HeaderPartSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 6 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForWork.Add(heads);
            }
            return result;
        }

        public DetailPartSummaryForWorkModel MapDetailPartSummaryForWorkModel(AllSummaryForWorkModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkHistories.Add(items);
            }
            
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);
                var heads = new HeaderPartSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (isNumeric ? "linkPartDetail1" :"" ): "",
                    functionCall = head.Value.Contains("DATA") ?  "Show" : "",
                };
                result.HeaderPartSummaryForWork.Add(heads);
            }
            return result;
        }

        public DetailPartSummaryForWorkModel MapDetail2PartSummaryForWorkModel(AllSummaryForWorkModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    ROW_TYPE = item.ROW_TYPE,
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out  n);
                var heads = new HeaderPartSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (isNumeric ? "linkPartDetail2" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForWork.Add(heads);
            }
            return result;
        }

        #endregion

        [HttpPost]
        [Route("GetDetailSummaryForWorkByAutoGas1")]
        public async Task<ActionResult> GetDetailSummaryForWorkByAutoGas1(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByAutoGas1(model);
                var result = MapDetail1PartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetDetailSummaryForWorkByAutoGas2")]
        public async Task<ActionResult> GetDetailSummaryForWorkByAutoGas2(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByAutoGas2(model);
                var result = MapDetail2PartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetDetailSummaryForWorkByAutoGas3")]
        public async Task<ActionResult> GetDetailSummaryForWorkByAutoGas3(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByAutoGas3(model);
                var result = MapDetail2PartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetDetailSummaryForWorkByTaper")]
        public async Task<ActionResult> GetDetailSummaryForWorkByTaper(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByTaper(model);
                var result = MapDetail2PartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetDetailSummaryForWorkByPart")]
        public async Task<ActionResult> GetDetailSummaryForWorkByPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByPart(model);
                var result = MapDetail2PartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        public DetailPartDetailSummaryForWorkModel MapDetail1PartDatailSummaryForWorkModel(PartDetailModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartDetailSummaryForWorkHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = item.PJ_NAME,
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    PD_CODE = item.PD_CODE,
                    SYMBOL = item.SYMBOL,
                    PART_ITEM = DataConverter.GetString(item.PART_ITEM),
                    PART_SIZE = item.PART_SIZE,
                    CUTTING_PLAN = item.CUTTING_PLAN,
                    PROCESS = item.PROCESS,
                    HOLE = DataConverter.GetString(item.HOLE),
                    TAPER = DataConverter.GetNumberFormat(item.TAPER, 2),
                    PLAN_QTY = DataConverter.GetString(item.PLAN_QTY),
                    ACT_QTY = DataConverter.GetString(item.ACT_QTY),
                    ACT_LENGTH = DataConverter.GetNumberFormat(item.ACT_LENGTH, 2),
                    ACT_GROUP = item.ACT_GROUP,
                    ACTUAL_USER = item.ACTUAL_USER,
                    ACT_NAME = item.ACT_NAME,
                    ACT_DATE = item.ACT_DATE,
                  
                };
                result.DetailPartDetailSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderPartDetailSummaryForWork.Add(heads);
            }
            return result;
        }

        public DetailPartDetailSummaryForWorkModel MapDetail2PartDatailSummaryForWorkModel(PartDetailModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartDetailSummaryForWorkHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = item.PJ_NAME,
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    PD_CODE = item.PD_CODE,
                    SYMBOL = item.SYMBOL,
                    PART_ITEM = DataConverter.GetString(item.PART_ITEM),
                    PART_SIZE = item.PART_SIZE,
                    CUTTING_PLAN = item.CUTTING_PLAN,
                    PROCESS = item.PROCESS,
                    HOLE = DataConverter.GetString(item.HOLE),
                    TAPER = DataConverter.GetNumberFormat(item.TAPER, 2),
                    PLAN_QTY = DataConverter.GetString(item.PLAN_QTY),
                    ACT_QTY = DataConverter.GetString(item.ACT_QTY),
                    //ACT_LENGTH = DataConverter.GetNumberFormat(item.ACT_LENGTH, 2),
                    ACT_GROUP = item.ACT_GROUP,
                    ACTUAL_USER = item.ACTUAL_USER,
                    ACT_NAME = item.ACT_NAME,
                    ACT_DATE = item.ACT_DATE,

                };
                result.DetailPartDetailSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderPartDetailSummaryForWork.Add(heads);
            }
            return result;
        }

    }
}