﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;


namespace MCSAPP.WebApp.Controllers
{
    public class WeekController : BaseController
    {
        [Route("GetWeekByMonth")]
        [HttpGet]
        public async Task<ActionResult> GetWeekByMonth(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewWeekServiceWrapperInstance().GetWeekByMonth(model);
            return OK(result);
        }
    }
}