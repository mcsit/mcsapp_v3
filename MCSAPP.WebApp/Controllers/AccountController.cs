﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MCSAPP.DAL.Model.Account;
using MCSAPP.WebApp.Helper;
using MCSAPP.WebApp.Models;
using MCSAPP.Helper.Enum;
using System.Net;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.DAL.Model;

namespace MCSAPP.WebApp.Controllers
{
 
    public class AccountController : BaseController
    {
        [AuthorizeIdentityActionFilter(AllowAuthorize.Anonymous)]
        public ActionResult Page()
        {
            return View();
        }
        [AuthorizeIdentityActionFilter(AllowAuthorize.Anonymous)]
        public ActionResult UnAuthorize()
        {
            return View();
        }
        public ActionResult Logout()
        {
            LogOut();
            return RedirectToAction("../Account/Page");
        }
        [AuthorizeIdentityActionFilter(AllowAuthorize.Anonymous)]
        [APIAuthorizeFilter(false)]
        [Route("Login")]
        [HttpPost]
        public async Task<JsonResult> Login(UserLoginModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    model.IpAddress = Request.UserHostAddress;
                    model.BrowserName = Request.Browser.Type;
                    var result = await UserLogin(model);
                    new AdminController().WriteLogActivity(new LogModel() { PAGE_NAME = "Login", FUNC_NAME = "GetLogin", ACTION_MSG = "Login : " + model.Username, USER_UPDATE = model.Username });
                    return OK(result);
                }
                catch (UnauthorizedAccessException uex)
                {
                    new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Login", FUNC_NAME = "GetLogin", ERROR_MSG = "Login : " + model.Username + " Error : " + uex.Message });
                    return OK<UserApplicationModel>(uex.Message, HttpStatusCode.Unauthorized);

                }
            }
            else
            {
                var errorMessage = WebHelper.GetModelErrorToString(ModelState);
                new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Login", FUNC_NAME = "GetLogin", ERROR_MSG = "Login : " + model.Username + " Error : " + errorMessage });
                return OK<UserApplicationModel>(string.Format("Login Invalid : {0}", errorMessage), HttpStatusCode.Unauthorized);
            }
        }
 
    }
}