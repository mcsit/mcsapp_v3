﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.IO;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;

namespace MCSAPP.WebApp.Controllers
{
    public class UploadController : BaseController
    {
        // GET: Upload

        public ActionResult VTUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult NCRUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult NGUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult NGUploadNewManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult DesignUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult OtherUploadManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult WeekUploadManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ActUTUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ActUTUploadNewManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult CutFinisingUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult GoalUploadManagement()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        [HttpGet]
        public ActionResult LoadFile(string name)
        {


            //var PathSave = ConfigurationHelper.GetAppSettting("PathSave");


            //string fullPath = PathSave + "\\Exemple";
            //return File(fullPath, "application/vnd.ms-excel", type + ".xls");
            var strPath = System.IO.Path.Combine(Server.MapPath("/UploadFile/Example/"), name + ".xls");

            var strFileName = System.IO.Path.GetFileName(strPath);
            //Response.Clear();
            //Response.Buffer = true;
            //Response.AppendHeader("content-disposition", "attachment; filename=" + strFileName);
            //Response.ContentType = "Application/x-msexcel";
            //Response.WriteFile(strPath);
            //Response.End();
            //return RedirectToAction("Index");

            //// return File(filepath, MimeMapping.GetMimeMapping(filepath), name);
            //FileStream fs = new FileStream(strPath, FileMode.Open, FileAccess.Read);
            

            //// Create a byte array of file stream length
            //byte[] ImageData = new byte[fs.Length];
            //fs.Close();
            //fs.Dispose();
            Stream iStream= null;
            iStream = new FileStream(strPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            long dataToRead = iStream.Length;
            Byte[] buffer = new Byte[dataToRead];
            iStream.Read(buffer, 0, buffer.Length);
            iStream.Flush();
            iStream.Close();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename="+strFileName);
            // Replace filename with your custom Excel-sheet name.

            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            Response.BinaryWrite(buffer);
            Response.End();
            return RedirectToAction("Index");

            //return File(buffer, System.Net.Mime.MediaTypeNames.Application.Octet, strFileName);
        }

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        // 2018-02-12 by chaiwud.ta
        public ActionResult GoalUploadManagementBuilt()
        {

            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult RepairCheckUploadManagementBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult UTDataByUserUploadBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult NGDataUploadBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult CamberDataUploadOther()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        #endregion

    }
}