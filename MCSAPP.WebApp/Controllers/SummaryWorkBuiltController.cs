﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.WebApp.Controllers
{
    public class SummaryWorkBuiltController : BaseController
    {
        // GET: SummaryWorkBuilt
        #region Built Beam
        // chaiwud.ta
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ReportSummaryForWorkByBuiltUpBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForWorkBySawBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForWorkAdjustBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForWorkByDrillShotBlBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        #region Other Beam/Box
        public ActionResult ReportSummaryForWorkOtherBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForWorkOtherBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForWorkOtherFinishing()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        #endregion

        // #CBB1 #CBB2
        // Built Beam
        // 2018-01-31 chaiwud.ta

        // Built Up  Beam
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltUpBeam")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltUpBeam(model);
                var result = MapDetailBuiltSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data
        // Built Up Beam
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltUpBeam")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltUpBeam(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // Saw
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltSawBeam")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltSawBeam(model);
                var result = MapDetailBuiltSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data
        // Built Up Beam
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltSawBeam")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltSawBeam(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // Adjust
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltAdjustBeam")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltAdjustBeam(model);
                var result = MapDetailBuiltSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data
        // Built Up Beam
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltAdjustBeam")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltAdjustBeam(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // Drill & Shot Blast
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltDrillShotBeam")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltDrillShotBeam(model);
                var result = MapDetailBuiltSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data
        // Built Up Beam
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltDrillShotBeam")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltDrillShotBeam(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }
        #endregion

        #region Function Map
        // fn use
        // from  SummaryWorkController
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModel(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add for split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);
                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (isNumeric ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add for split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 6;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        // map diaphrgm
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelDiaphragmBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    //functionCall = head.Value.Contains("DATA") ? "Show" : "",
                    functionCall = head.Value == "DATA_1" ? "Show" : head.Value == "DATA_2" ? "ShowFab" : head.Value == "DATA_3" ? "ShowWeld" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }
        // map Built Up box
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelBuiltUpBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") ? "Show" : head.Value.Contains("DATA_4") ? "ShowFab" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }
        public DetailBuiltDetailSummaryForWorkModel MapDetailBuiltDatailSummaryForWorkModelBuiltBox(BuiltDetailModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailBuiltDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {
                // for Built
                var items = new DetailBuiltDetailSummaryForWorkHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = item.PJ_NAME,
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),

                    ITEM_NO = DataConverter.GetString(item.ITEM_NO),
                    BUILT_TYPE_NAME = item.BUILT_TYPE_NAME,
                    CUTTING_PLAN = item.CUTTING_PLAN,
                    BUILT_NO = DataConverter.GetString(item.BUILT_NO),
                    BUILT_SIZE = DataConverter.GetString(item.BUILT_SIZE),
                    PLACE = item.PLACE,
                    BUILT_WEIGHT = DataConverter.GetString(item.BUILT_WEIGHT),
                    BUILT_PLAN = DataConverter.GetString(item.BUILT_PLAN),
                    PC_NAME = item.PC_NAME,
                    UG_NAME = item.UG_NAME,
                    PA_PLAN_DATE = DataConverter.GetString(item.PA_PLAN_DATE),
                    ACT_USER = item.ACT_USER,
                    FULLNAME = item.FULLNAME,
                    PA_ACTUAL_DATE = DataConverter.GetString(item.PA_ACTUAL_DATE),
                    BUILT_ACTUAL = DataConverter.GetString(item.BUILT_ACTUAL),
                    PA_NO = DataConverter.GetString(item.PA_NO),
                };
                result.DetailBuiltDetailSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderBuiltDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderBuiltDetailSummaryForWork.Add(heads);
            }
            return result;
        }

        // Auto Root : 1.AUTO ROOT 2.DIAPHRAGM 3.TOPPLATE 4.GMAW FILLET 5.GMAW UT 6.OTHER
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelBuiltAutoRootBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") ? "Show" : head.Value.Contains("DATA_5") ? "ShowTopPlate" : head.Value.Contains("DATA_6") ? "ShowGMAW" : head.Value.Contains("DATA_7") ? "ShowGMAWUT" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        // DRILL & SESNET : 1.เชื่อม 2.เจาะ 3.เจียร์ 4.เผา 5.OTHER
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelBuiltDrillSesnetBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }
        // GOUGING + REPAIR : 1.GOUGING 2.REPAIR 3. GMAW FILLET 4.GMAW UT 5.OTHER
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelBuiltGougingRepairBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") ? "Show" : head.Value.Contains("DATA_3") ? "ShowGMAW" : head.Value.Contains("DATA_4") ? "ShowGMAWUT" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        // use
        public DetailBuiltDetailSummaryForWorkModel MapDetail2BuiltDatailSummaryForWorkModel(BuiltDetailModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailBuiltDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {
                // for Built
                var items = new DetailBuiltDetailSummaryForWorkHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = item.PJ_NAME,
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),

                    ITEM_NO = DataConverter.GetString(item.ITEM_NO),
                    BUILT_TYPE_NAME = item.BUILT_TYPE_NAME,
                    CUTTING_PLAN = item.CUTTING_PLAN,
                    BUILT_NO = DataConverter.GetString(item.BUILT_NO),
                    PLACE = item.PLACE,
                    BUILT_WEIGHT = DataConverter.GetString(item.BUILT_WEIGHT),
                    BUILT_PLAN = DataConverter.GetString(item.BUILT_PLAN),
                    PC_NAME = item.PC_NAME,
                    UG_NAME = item.UG_NAME,
                    PA_PLAN_DATE = DataConverter.GetString(item.PA_PLAN_DATE),
                    ACT_USER = item.ACT_USER,
                    FULLNAME = item.FULLNAME,
                    PA_ACTUAL_DATE = DataConverter.GetString(item.PA_ACTUAL_DATE),
                    BUILT_ACTUAL = DataConverter.GetString(item.BUILT_ACTUAL),
                    PA_NO = DataConverter.GetString(item.PA_NO),
                };
                result.DetailBuiltDetailSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderBuiltDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderBuiltDetailSummaryForWork.Add(heads);
            }
            return result;
        }

        public DetailBuiltDetailSummaryForWorkModel MapDetail2OtherBeamDatailSummaryForWorkModel(BuiltDetailModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailBuiltDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {
                // for Built
                var items = new DetailBuiltDetailSummaryForWorkHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = item.PJ_NAME,
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    PD_REV = DataConverter.GetString(item.PD_REV),
                    PD_CODE = DataConverter.GetString(item.PD_CODE),
                    PD_DWG = DataConverter.GetString(item.PD_DWG),
                    BUILT_SIZE = DataConverter.GetString(item.BUILT_SIZE),
                    PD_BLOCK = DataConverter.GetString(item.PD_BLOCK),
                    PLACE = item.PLACE,
                    BUILT_PLAN = DataConverter.GetString(item.BUILT_PLAN),
                    PC_NAME = item.PC_NAME,
                    UG_NAME = item.UG_NAME,
                    PA_PLAN_DATE = DataConverter.GetString(item.PA_PLAN_DATE),
                    ACT_USER = item.ACT_USER,
                    FULLNAME = item.FULLNAME,
                    PA_ACTUAL_DATE = DataConverter.GetString(item.PA_ACTUAL_DATE),
                    BUILT_ACTUAL = DataConverter.GetString(item.BUILT_ACTUAL),
                    PA_NO = DataConverter.GetString(item.PA_NO),

                    // --------------------------------     
                    ITEM_NO = DataConverter.GetString(item.ITEM_NO),
                    BUILT_TYPE_NAME = DataConverter.GetString(item.BUILT_TYPE_NAME),
                    CUTTING_PLAN = DataConverter.GetString(item.CUTTING_PLAN),
                    BUILT_NO = DataConverter.GetString(item.BUILT_NO),
                    BUILT_WEIGHT = DataConverter.GetString(item.BUILT_WEIGHT),
                };
                result.DetailBuiltDetailSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderBuiltDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("PD_ITEM") || head.Value.Contains("CUTTING_PLAN") || head.Value.Contains("BUILT_NO") || head.Value.Contains("PC_NAME") || head.Value.Contains("UG_NAME") || head.Value.Contains("ACTUAL_DATE") ? "vertical-align:initial; text-align:center" : "vertical-align:initial; text-align:left",
                };
                result.HeaderBuiltDetailSummaryForWork.Add(heads);
            }
            return result;
        }

        // autoroot
        public DetailBuiltDetailSummaryForWorkModel MapDetailAutotRootBuiltDatailSummaryForWorkModel(BuiltDetailModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailBuiltDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {
                // for Built
                var items = new DetailBuiltDetailSummaryForWorkHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = item.PJ_NAME,
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),

                    ITEM_NO = DataConverter.GetString(item.ITEM_NO),
                    BUILT_TYPE_NAME = item.BUILT_TYPE_NAME,
                    CUTTING_PLAN = item.CUTTING_PLAN,
                    BUILT_NO = DataConverter.GetString(item.BUILT_NO),
                    // Auto Root
                    BUILT_LENGTH = DataConverter.GetString(item.BUILT_LENGTH), // AUTO ROOT
                    HOLE = DataConverter.GetString(item.HOLE), // AUTO ROOT
                    PLACE = item.PLACE,
                    BUILT_WEIGHT = DataConverter.GetString(item.BUILT_WEIGHT),
                    BUILT_PLAN = DataConverter.GetString(item.BUILT_PLAN),
                    PC_NAME = item.PC_NAME,
                    UG_NAME = item.UG_NAME,
                    PA_PLAN_DATE = DataConverter.GetString(item.PA_PLAN_DATE),
                    ACT_USER = item.ACT_USER,
                    FULLNAME = item.FULLNAME,
                    PA_ACTUAL_DATE = DataConverter.GetString(item.PA_ACTUAL_DATE),
                    BUILT_ACTUAL = DataConverter.GetString(item.BUILT_ACTUAL),
                    PA_NO = DataConverter.GetString(item.PA_NO),
                };
                result.DetailBuiltDetailSummaryForWorkHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderBuiltDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderBuiltDetailSummaryForWork.Add(heads);
            }
            return result;
        }

        #endregion

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelOtherBeam(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetSummaryForWorkByOtherBeam")]
        public async Task<ActionResult> GetSummaryForWorkByOtherBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByOtherBeam(model);
                var result = MapDetailBuiltSummaryForWorkModelOtherBeam(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetDetailSummaryForWorkByOtherBeam")]
        public async Task<ActionResult> GetDetailSummaryForWorkByOtherBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByOtherBeam(model);
                var result = MapDetail2OtherBeamDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // Other Adjust Box
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelOtherBox(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,
                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") || head.Value.Contains("DATA_5") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetSummaryForWorkByOtherBox")]
        public async Task<ActionResult> GetSummaryForWorkByOtherBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByOtherBox(model);
                var result = MapDetailBuiltSummaryForWorkModelOtherBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetDetailSummaryForWorkByOtherBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByOtherBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByOtherBox(model);
                var result = MapDetail2OtherBeamDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // Other Finishing
        public DetailPartSummaryForWorkBuiltModel MapDetailBuiltSummaryForWorkModelOtherFinishing(AllSummaryForWorkBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForWorkBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForWorkBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                };
                result.DetailPartSummaryForWorkBuiltHistories.Add(items);
            }

            int CountHeader = 0;
            CountHeader = models.Header.Count;
            if (CountHeader > 0)
            {
                CountHeader -= 7;
            }
            foreach (var head in models.Header)
            {
                int n;
                var isNumeric = int.TryParse(head.Text, out n);

                var heads = new HeaderPartSummaryForWorkBuilt()
                {
                    head = head.Text,
                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < CountHeader ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") || head.Value.Contains("DATA_5") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForWorkBuilt.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetSummaryForWorkByOtherFinishing")]
        public async Task<ActionResult> GetSummaryForWorkByOtherFinishing(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByOtherFinishing(model);
                var result = MapDetailBuiltSummaryForWorkModelOtherFinishing(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetDetailSummaryForWorkByOtherFinishing")]
        public async Task<ActionResult> GetDetailSummaryForWorkByOtherFinishing(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByOtherFinishing(model);
                var result = MapDetail2OtherBeamDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }
        #endregion

        #region Built Box
        // 2018-02-12  by chaiwud.ta
        // FabDiaphragmBox
        public ActionResult ReportSummaryForWorkByFabDiaphragmBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // BuiltUpBox
        public ActionResult ReportSummaryForWorkByBuiltUpBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // AutoRootBox
        public ActionResult ReportSummaryForWorkByAutoRootBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // DrillSesnetBox
        public ActionResult ReportSummaryForWorkByDrillSesnetBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // GougingRepairBox
        public ActionResult ReportSummaryForWorkByGougingRepairBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // FacingBox
        public ActionResult ReportSummaryForWorkByFacingBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        // FabDiaphragmBox
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltFabDiaphragmBox")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltFabDiaphragmBox(model);
                var result = MapDetailBuiltSummaryForWorkModelDiaphragmBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data -> FabDiaphragmBox
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltFabDiaphragmBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltFabDiaphragmBox(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // BuiltUpBox
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltUpBox")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltUpBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltUpBox(model);
                var result = MapDetailBuiltSummaryForWorkModelBuiltUpBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data -> BuiltUpBox
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltUpBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltUpBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltUpBox(model);
                var result = MapDetailBuiltDatailSummaryForWorkModelBuiltBox(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // AutoRootBox
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltAutoRootBox")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltAutoRootBox(model);
                var result = MapDetailBuiltSummaryForWorkModelBuiltAutoRootBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data -> AutoRootBox
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltAutoRootBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltAutoRootBox(model);
                var result = MapDetailAutotRootBuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // DrillSesnetBox
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltDrillSesnetBox")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltDrillSesnetBox(model);
                var result = MapDetailBuiltSummaryForWorkModelBuiltDrillSesnetBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data -> DrillSesnetBox
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltDrillSesnetBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltDrillSesnetBox(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // GougingRepairBox
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltGougingRepairBox")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltGougingRepairBox(model);
                var result = MapDetailBuiltSummaryForWorkModelBuiltGougingRepairBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data -> GougingRepairBox
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltGougingRepairBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltGougingRepairBox(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }

        // FacingBox
        [HttpPost]
        [Route("GetSummaryForWorkByBuiltFacingBox")]
        public async Task<ActionResult> GetSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForWorkByBuiltFacingBox(model);
                var result = MapDetailBuiltSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // Get Actual Data -> FacingBox
        [HttpPost]
        [Route("GetDetailSummaryForWorkByBuiltFacingBox")]
        public async Task<ActionResult> GetDetailSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.DAY == null)
                    model.DAY = "0";
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailSummaryForWorkByBuiltFacingBox(model);
                var result = MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("OK");
        }
        #endregion
    }
}