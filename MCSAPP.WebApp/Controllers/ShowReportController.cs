﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;

namespace MCSAPP.WebApp.Controllers
{
    public class ShowReportController : BaseController
    {
        // GET: ShowReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportSummaryForGrinder()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForCut()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        [HttpPost]
        [Route("GetSummaryForGrinder")]
        public async Task<ActionResult> GetSummaryForGrinder(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForGrinder(model);
                var result = MapDetailShowSummaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }


        [HttpPost]
        [Route("GetSummaryForCut")]
        public async Task<ActionResult> GetSummaryForCut(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForCut(model);
                var result = MapDetailShowSummaryCutModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }


        public DetailPartShowSummaryModel MapDetailShowSummaryCutModel(AllShowSummaryModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartShowSummaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailShowSummaryHistory()
                {

                    DATA_1 = DataConverter.GetString(item.DATA_1),
                    DATA_2 = DataConverter.GetString(item.DATA_2),
                    DATA_3 = DataConverter.GetString(item.DATA_3),
                    DATA_4 = DataConverter.GetString(item.DATA_4),
                    DATA_5 = DataConverter.GetString(item.DATA_5),
                    DATA_6 = DataConverter.GetString(item.DATA_6),
                    DATA_7 = DataConverter.GetString(item.DATA_7),
                    DATA_8 = DataConverter.GetString(item.DATA_8),
                    DATA_9 = DataConverter.GetString(item.DATA_9),
                    DATA_10 = DataConverter.GetString(item.DATA_10),

                };
                result.DetailShowSummaryHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderShowSummary()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = !head.Value.Contains("DATA_1") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    //functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderShowSummary.Add(heads);
            }
            return result;
        }

        public DetailPartShowSummaryModel MapDetailShowSummaryModel(AllShowSummaryModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartShowSummaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailShowSummaryHistory()
                {

                    DATA_1 = DataConverter.GetString(item.DATA_1),
                    DATA_2 = item.ROW_TYPE.Equals("Footer") ? "" : DataConverter.GetNumberFormat(item.DATA_2, 3),
                    DATA_3 = item.ROW_TYPE.Equals("Footer") ? "" : DataConverter.GetNumberFormat(item.DATA_3, 1),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 3),
                    DATA_5 = DataConverter.GetString(item.DATA_5),
                    DATA_6 = DataConverter.GetString(item.DATA_6),
                    DATA_7 = DataConverter.GetString(item.DATA_7),
                    DATA_8 = DataConverter.GetString(item.DATA_8),
                    DATA_9 = DataConverter.GetString(item.DATA_9),
                    DATA_10 = DataConverter.GetString(item.DATA_10),
         
                };
                result.DetailShowSummaryHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderShowSummary()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = !head.Value.Contains("DATA_1") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    //functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderShowSummary.Add(heads);
            }
            return result;
        }

    }
}