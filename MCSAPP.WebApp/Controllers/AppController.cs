﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using System.Reflection;
using System.ComponentModel;

namespace MCSAPP.WebApp.Controllers
{
    public class AppController : BaseController
    {
        // GET: App
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("GetPartDetail")]
        public async Task<ActionResult> GetPartDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                var serviceResult = await service.GetPartDetail(model);
                var result = MapDetailPartModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartModel MapDetailPartModel(List<PartDetail2Model> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailPartModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new PartHistory()
                {
                    ROW_NUM = DataConverter.GetInteger(item.ROW_NUM),
                    PJ_ID = DataConverter.GetInteger(item.PJ_ID),
                    PJ_NAME = DataConverter.GetString(item.PJ_NAME),
                    PD_ITEM = DataConverter.GetInteger(item.PD_ITEM),
                    REV_NO = DataConverter.GetString(item.REV_NO),
                    PD_CODE = DataConverter.GetString(item.PD_CODE) ?? "",
                    DESIGN_CHANGE = DataConverter.GetString(item.DESIGN_CHANGE)?? "",
                    PD_WEIGHT = DataConverter.GetNumberFormat(item.PD_WEIGHT, 3) ?? "",
                    PD_LENGTH = DataConverter.GetNumberFormat(item.PD_LENGTH, 1) ?? "",
                    PC_NAME = DataConverter.GetString(item.PC_NAME),
                    UG_NAME = DataConverter.GetString(item.UG_NAME),
                    PA_PLAN_DATE = DataConverter.GetString(item.PA_PLAN_DATE),
                    FAB_PLAN = DataConverter.GetNumberFormat(item.FAB_PLAN, 0),
                    FAB_ACTUAL = DataConverter.GetNumberFormat(item.FAB_ACTUAL, 0),
                    ACTUAL_USER = DataConverter.GetString(item.ACTUAL_USER),
                    FULLNAME = DataConverter.GetString(item.FULLNAME),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),
                    PA_NO = DataConverter.GetInteger(item.PA_NO),
                };
                result.PartHistories.Add(items);
            }

            foreach (PartDetail h in Enum.GetValues(typeof(PartDetail)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ROW_NUM":
                    case "PD_ITEM":
                    case "REV_NO":
                    case "PA_NO":
                    case "UG_NAME":
                    case "ACTUAL_USER":
                    case "ACTUAL_DATE":
                        {
                            sty =  "vertical-align:initial; text-align:center";
                            break;
                        }
                    case "PD_WEIGHT":
                    case "PD_LENGTH":
                    case "FAB_PLAN":
                    case "FAB_ACTUAL":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderPart()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderPart.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetOtherDetail")]
        public async Task<ActionResult> GetOtherDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                var serviceResult = await service.GetOtherDetail(model);
                var result = MapDetailOtherModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartModel MapDetailOtherModel(List<PartDetail2Model> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailPartModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new PartHistory()
                {
                    ROW_NUM = DataConverter.GetInteger(item.ROW_NUM),                   
                    PC_NAME = DataConverter.GetString(item.PC_NAME),
                    UG_NAME = DataConverter.GetString(item.UG_NAME),                   
                    FAB_ACTUAL = DataConverter.GetNumberFormat(item.FAB_ACTUAL, 0),
                    ACTUAL_USER = DataConverter.GetString(item.ACTUAL_USER),
                    FULLNAME = DataConverter.GetString(item.FULLNAME),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),
       
                };
                result.PartHistories.Add(items);
            }

            foreach (OtherDetail h in Enum.GetValues(typeof(OtherDetail)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ROW_NUM":
                    case "PD_ITEM":
                    case "REV_NO":
                    case "PA_NO":
                    case "UG_NAME":
                    case "ACTUAL_USER":
                    case "ACTUAL_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    case "PD_WEIGHT":
                    case "PD_LENGTH":
                    case "FAB_PLAN":
                    case "FAB_ACTUAL":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderPart()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderPart.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetVTDetail")]
        public async Task<ActionResult> GetVTDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                var serviceResult = await service.GetVTDetail(model);
                var result = MapDetailVTModel(serviceResult,false);
                return OK(result);
            }
            return OK("");
        }
        public DetailVTModel MapDetailVTModel(List<VTListModel> models,bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailVTModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new VTHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    ITEM = DataConverter.GetInteger(item.ITEM),
                    REV_NO = DataConverter.GetString(item.REV_NO),
                    CHECK_NO = DataConverter.GetString(item.CHECK_NO),
                    PART = DataConverter.GetNumberFormat(item.PART, 0),
                    POINT = DataConverter.GetString(item.POINT),
                    PROBLEM = DataConverter.GetString(item.PROBLEM),
                    DWG = DataConverter.GetString(item.DWG) ?? "",
                    ACT = DataConverter.GetString(item.ACT) ?? "",
                    WRONG = DataConverter.GetString(item.WRONG),
                    EDIT = DataConverter.GetString(item.EDIT),
                    CHECKER = DataConverter.GetString(item.CHECKER),
                    CHECK_DATE = DataConverter.GetString(item.CHECK_DATE),
                };
                result.VTHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderVT()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderVT.Add(headschk);
            }
            foreach (VTDetail h in Enum.GetValues(typeof(VTDetail)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                   
                    case "ITEM":
                    case "CHECK_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderVT()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderVT.Add(heads);
            }
           
            return result;
        }

        [HttpPost]
        [Route("GetVTForAdd")]
        public async Task<ActionResult> GetVTForAdd(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                var serviceResult = await service.GetVTForAdd(model);
                var result = MapDetailVTModel(serviceResult,true);
                return OK(result);
            }
            return OK("");
        }


        [HttpPost]
        [Route("GetHeaderExcel")]
        public async Task<ActionResult> GetHeaderExcel(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewAppServiceWrapperInstance().GetHeaderExcel(model);
            return OK(result);
        }


        #region Header

        public enum PartDetail
        {
            [Description("No.")]
            ROW_NUM = 1,
            [Description("Project")]
            PJ_NAME = 2,
            [Description("Item")]
            PD_ITEM = 3,
            [Description("Rev.")]
            REV_NO = 4,
            [Description("Pa No.")]
            PA_NO = 5,
            [Description("Code")]
            PD_CODE = 6,
            [Description("Design Change")]
            DESIGN_CHANGE = 7,
            [Description("Weight")]
            PD_WEIGHT = 8,
            [Description("Length")]
            PD_LENGTH = 9,
            [Description("Process")]
            PC_NAME = 10,
            [Description("Group")]
            UG_NAME = 11,
            [Description("Plan Date")]
            PA_PLAN_DATE = 12,
            [Description("Plan")]
            FAB_PLAN = 13,
            [Description("Actual")]
            FAB_ACTUAL = 14,
            [Description("Sent By")]
            ACTUAL_USER = 15,
            [Description("Fullname")]
            FULLNAME = 16,
            [Description("Actual Date")]
            ACTUAL_DATE =17,


        }

        public enum OtherDetail
        {
            [Description("No.")]
            ROW_NUM = 1,
           
            [Description("Process")]
            PC_NAME = 10,
            [Description("Group")]
            UG_NAME = 11,
          
            [Description("Actual")]
            FAB_ACTUAL = 14,
            [Description("Sent By")]
            ACTUAL_USER = 15,
            [Description("Fullname")]
            FULLNAME = 16,
            [Description("Actual Date")]
            ACTUAL_DATE = 17,


        }

        public enum VTDetail
        {
            [Description("PROJECT")]
            PROJECT =1,
            [Description("ITEM")]
            ITEM =2,
            [Description("REV_NO")]
            REV_NO = 3,
            [Description("CHECK_NO")]
            CHECK_NO =4,
            [Description("PART")]
            PART = 5,
            [Description("POINT")]
            POINT = 6,
            [Description("PROBLEM")]
            PROBLEM = 7,
            [Description("DWG")]
            DWG = 8,
            [Description("ACT")]
            ACT =9,
            [Description("WRONG")]
            WRONG = 10,
            [Description("EDIT")]
            EDIT = 11,
            [Description("CHECKER")]
            CHECKER =12,
            [Description("CHECK_DATE")]
            CHECK_DATE = 13,


        }
        private string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        #endregion

    }
}