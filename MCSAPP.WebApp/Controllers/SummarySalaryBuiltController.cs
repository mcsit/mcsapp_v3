﻿using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MCSAPP.WebApp.Controllers
{
    public class SummarySalaryBuiltController : BaseController
    {
        // GET: 
        #region Built Beam
        // T.Chaiwud
        public ActionResult ReportSummaryForSalaryByBuiltUpBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForSalaryBySawBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForSalaryAdjustBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForSalaryByDrillShotBlBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        #region Other Beam/Box/Finishing
        public ActionResult ReportSummaryForSalaryOtherBeam()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ReportSummaryForSalaryOtherBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // 2018-07-26 T.Chaiwud
        public ActionResult ReportSummaryForSalaryOtherFinishing()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        #endregion

        /// <summary>
        /// #CBB1 #CBB2
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        // chaiwud.ta
        // Built Up Beam
        [HttpPost]
        [Route("GetSummaryForSalaryByBuiltUpBeam")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltUpBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltUpBeam(model);
                var result = MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        // Saw 
        [HttpPost]
        [Route("GetSummaryForSalaryByBuiltSawBeam")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltSawBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltSawBeam(model);
                var result = MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        // Adjust
        [HttpPost]
        [Route("GetSummaryForSalaryByBuiltAdjustBeam")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltAdjustBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltAdjustBeam(model);
                var result = MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        // Drill & Shot Blast
        [HttpPost]
        [Route("GetSummaryForSalaryByBuiltDrilShotBeam")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltDrillShotBeam(model);
                var result = MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltBeamSummaryForSalaryModel(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {
                 
                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 2 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }
        #endregion

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        [Route("GetSummaryForSalaryByOtherBeam")]
        public async Task<ActionResult> GetSummaryForSalaryByOtherBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByOtherBeam(model);
                var result = MapDetailSummaryForSalaryModelOtherBeam(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartSummaryForSalaryBuiltModel MapDetailSummaryForSalaryModelOtherBeam(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") ? "Show" :  "",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }

        // Other Adjust Box
        [Route("GetSummaryForSalaryByOtherBox")]
        public async Task<ActionResult> GetSummaryForSalaryByOtherBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByOtherBox(model);
                var result = MapDetailSummaryForSalaryModelOtherBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartSummaryForSalaryBuiltModel MapDetailSummaryForSalaryModelOtherBox(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 6 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") || head.Value.Contains("DATA_5") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }
        
        // Other Finishing 
        [Route("GetSummaryForSalaryByOtherFinishing")]
        public async Task<ActionResult> GetSummaryForSalaryByOtherFinishing(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByOtherFinishing(model);
                var result = MapDetailSummaryForSalaryModelOtherFinishing(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartSummaryForSalaryBuiltModel MapDetailSummaryForSalaryModelOtherFinishing(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 6 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") || head.Value.Contains("DATA_5") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }
        #endregion

        #region Built Box
        // 2018-02-12 by chaiwud.ta

        // FabDiaphragmBox
        public ActionResult ReportSummaryForSalaryByFabDiaphragmBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // BuiltUpBox
        public ActionResult ReportSummaryForSalaryByBuiltUpBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // AutoRootBox
        public ActionResult ReportSummaryForSalaryByAutoRootBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // DrillSesnetBox
        public ActionResult ReportSummaryForSalaryByDrillSesnetBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // GougingRepairBox
        public ActionResult ReportSummaryForSalaryByGougingRepairBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        // FacingBox
        public ActionResult ReportSummaryForSalaryByFacingBox()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        // FabDiaphragmBox
        [Route("GetSummaryForSalaryByBuiltFabDiaphragmBox")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltFabDiaphragmBox(model);
                var result = MapDetailBuiltBoxSummaryForSalaryModelDiaphragm(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // BuiltUpBox
        [Route("GetSummaryForSalaryByBuiltUpBox")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltUpBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltUpBox(model);
                var result = MapDetailBuiltBoxSummaryForSalaryModelBuiltUpBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // AutoRootBox
        [Route("GetSummaryForSalaryByBuiltAutoRootBox")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltAutoRootBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltAutoRootBox(model);
                var result = MapDetailBuiltAutoRootSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // DrillSesnetBox
        [Route("GetSummaryForSalaryByBuiltDrillSesnetBox")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltDrillSesnetBox(model);
                var result = MapDetailBuiltBoxSummaryForSalaryModelBuiltDrillSesnetBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // GougingRepairBox
        [Route("GetSummaryForSalaryByBuiltGougingRepairBox")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltGougingRepairBox(model);
                var result = MapDetailBuiltBoxSummaryForSalaryModelBuiltGougingRepairBox(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        // FacingBox
        [Route("GetSummaryForSalaryByBuiltFacingBox")]
        public async Task<ActionResult> GetSummaryForSalaryByBuiltFacingBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByBuiltFacingBox(model);
                var result = MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        // All
        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltBoxSummaryForSalaryModel(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 2 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }

        // Diaphragm
        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltBoxSummaryForSalaryModelDiaphragm(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new plit column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 4 ? "linkPartDetail1" : "") : "",
                    //functionCall = head.Value.Contains("DATA_1") ? "Show" : "ShowOther",
                    functionCall = head.Value.Contains("DATA_1") ? "Show" : head.Value.Contains("DATA_2") ? "ShowFab" : head.Value.Contains("DATA_3") ? "ShowWeld" : "",
                    //head.Value == "DATA_1" ? "Show" : head.Value == "DATA_2" ? "ShowFab" : head.Value == "DATA_3" ? "ShowWeld" : "ShowOther",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }
        // Built Up Box
        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltBoxSummaryForSalaryModelBuiltUpBox(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") ? "Show" : "ShowFab",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }

        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltBoxSummaryForSalaryModelBuiltDrillSesnetBox(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") ? "Show" : "ShowOther",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }

        // Gounging
        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltBoxSummaryForSalaryModelBuiltGougingRepairBox(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") ? "Show" : head.Value.Contains("DATA_3") ? "ShowGMAW" : head.Value.Contains("DATA_4") ? "ShowGMAWUT" : "ShowOther",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }

        // Auto Root
        public DetailPartSummaryForSalaryBuiltModel MapDetailBuiltAutoRootSummaryForSalaryModel(AllSummaryForSalaryBuiltModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryBuiltHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = item.EMP_GOAL, // add new split column
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryBuiltHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalaryBuilt()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 9 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA_1") || head.Value.Contains("DATA_2") || head.Value.Contains("DATA_3") || head.Value.Contains("DATA_4") || head.Value.Contains("DATA_5") ? "Show" : head.Value.Contains("DATA_6") ? "ShowTopPlate" : head.Value.Contains("DATA_7") ? "ShowGMAW" : head.Value.Contains("DATA_8") ? "ShowGMAWUT" : "ShowOther",
                };
                result.HeaderPartSummaryForSalaryBuilt.Add(heads);
            }
            return result;
        }
        #endregion
    }
}