﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using Newtonsoft.Json;
using System.Reflection;
using System.ComponentModel;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.WebApp.Controllers
{
    public class ExportController : BaseController
    {
        // GET: Export
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public async Task<ActionResult> ExportSummarySalaryDayForWeek(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH)
        {
            try
            {
                var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
                DateModel model = new DateModel();

                model.MONTH = MONTH;
                model.YEAR = YEAR;
                model.WEEK = DataConverter.GetInteger(WEEK);
                model.DEPT_CODE = DEPT_CODE;
                model.SEARCH = SEARCH;
                model.SORT = "";
                model.SORT_TYPE = 1;
                model.TAKE = 10000;
                model.SKIP = 0;

                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryDayForWeek(model);



                DetailSalarySummaryModel result = new ReportController().MapDetailSalarySummaryModel(serviceResult, true);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                    //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                    //// Load your collection "accounts"
                    //ws.Cells["A1"].LoadFromText(name);
                    //ws.Cells["A2"].LoadFromText(a);
                    //ws.Cells["A2"].LoadFromText(a);
                    if (model.WEEK == 0)
                    {
                        ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือนช่างประกอบ {0}-{1}", model.YEAR, model.MONTH);
                    }

                    else
                    {
                        ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือนชางประกอบ สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                    }
                    for (int i = 0; i < result.HeaderSalarySummary.Count; i++)
                    {
                        ws.Cells[2, i + 1].Value = result.HeaderSalarySummary[i].head;
                    }

                    using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderSalarySummary.Count])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        range.Style.Font.Color.SetColor(Color.Black);
                    }

                    var Export1 = serviceResult.Select(f => new
                    {
                        f.EMP_CODE,
                        f.FULL_NAME,
                        f.DEPT_NAME,
                        f.EMP_GOAL,
                        f.GOAL
                    });
                    var Export2 = serviceResult.Select(f => new
                    {
                        f.PART,
                        f.OTHER,
                        f.REVISE,
                        f.WELD,
                        f.DIMENSION,
                        f.LEVEL_SALARY,
                        f.LEVEL_QUALITY,
                        f.LEVEL_PROCEDURE,
                        f.SUM_PART,
                        f.OVER_GOAL,
                        f.SALARY,
                        f.SALARY_QUALITY,
                        f.SALARY_PROCEDURE,
                        f.OTHER_SALARY,
                        f.REVISE_SALARY,
                        f.WELD_SALARY,
                        f.DIMENSION_SALARY,


                    });
                    var Export3 = result.DetailSalarySummaryHistories.Select(f => new
                    {
                        f.MAIN,
                        f.TEMPO,
                        f.VT_INSPEC,
                        f.VT_QPC,
                        f.VT_OTHER,
                        f.VT_DESIGN_CHANGE,
                    });
                    var Export4 = serviceResult.Select(f => new
                    {
                        f.MINUS,
                        f.PLUS,
                        f.POSITION,
                        f.SUM_SALARY,
                        f.FIX_SALARY,
                        f.POSITION_2,
                        f.MINUS_2,
                        f.SUM_SALARY_2,
                    });
                    ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                    ws.SelectedRange[3, 6].LoadFromCollection(Export2);
                    ws.SelectedRange[3, 23].LoadFromCollection(Export3);
                    ws.SelectedRange[3, 29].LoadFromCollection(Export4);
                    ws.Cells["K:X"].Style.Numberformat.Format = "0.00";
                    ws.Cells["AC:AE"].Style.Numberformat.Format = "0.00";
                    ws.Calculate();
                    ws.Cells.AutoFitColumns();

                    Byte[] fileBytes = pck.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ExportSummarySalaryDayForWeek" + dateExport + ".xlsx");
                    // Replace filename with your custom Excel-sheet name.

                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Export", FUNC_NAME = "ExportSummarySalaryDayForWeek", ERROR_MSG = ex.Message });
                return RedirectToAction("../Report/ReportSummarySalaryDayForWeek");
            }
        }

        public async Task<ActionResult> ExportSummaryWorkDayForWeek(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE_STORE;

            var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
            var serviceResult = await service.GetSummaryWorkDayForWeek(model);

            DetailGoalSumModel result = new ReportController().MapDetailGoalModel(serviceResult, true, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                //// Load your collection "accounts"
                //ws.Cells["A1"].LoadFromText(name);
                //ws.Cells["A2"].LoadFromText(a);
                for (int i = 0; i < result.HeaderGoalSum.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderGoalSum[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderGoalSum.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export = serviceResult.GoalModel.Select(f => new { f.EMP_CODE, f.FULL_NAME, f.DEPT_NAME, f.EMP_GOAL, f.GOAL, f.PART, f.OTHER, f.REVISE, f.WELD, f.DIMENSION });

                ws.SelectedRange[2, 1].LoadFromCollection(Export);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSummaryWorkDayForWeek" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetPartDetail(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();
            model.EMP_CODE = ID;
            model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.TYPE_STORE = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
            var serviceResult = await service.GetPartDetail(model);

            DetailPartModel result = new AppController().MapDetailPartModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                //// Load your collection "accounts"
                //ws.Cells["A1"].LoadFromText(name);
                //ws.Cells["A2"].LoadFromText(a);
                for (int i = 0; i < result.HeaderPart.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderPart[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderPart.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.REV_NO,
                    f.PA_NO,
                    f.PD_CODE,
                    f.DESIGN_CHANGE,
                    f.PD_WEIGHT,
                    f.PD_LENGTH,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.FAB_PLAN,
                    f.FAB_ACTUAL,
                    f.ACTUAL_USER,
                    f.FULLNAME,
                    f.ACTUAL_DATE
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Export" + TYPE.ToLower() + "Detail" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetOtherDetail(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();
            model.EMP_CODE = ID;
            model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.TYPE_STORE = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
            var serviceResult = await service.GetOtherDetail(model);

            DetailPartModel result = new AppController().MapDetailOtherModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                //// Load your collection "accounts"
                //ws.Cells["A1"].LoadFromText(name);
                //ws.Cells["A2"].LoadFromText(a);
                for (int i = 0; i < result.HeaderPart.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderPart[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderPart.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.PartHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.FAB_ACTUAL,
                    f.ACTUAL_USER,
                    f.FULLNAME,
                    f.ACTUAL_DATE
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Export" + TYPE.ToLower() + "Detail" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetVTDetail(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();
            model.DEPT_CODE = ID;
            model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.TYPE_STORE = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
            var serviceResult = await service.GetVTDetail(model);

            DetailVTModel result = new AppController().MapDetailVTModel(serviceResult, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                //// Load your collection "accounts"
                //ws.Cells["A1"].LoadFromText(name);
                //ws.Cells["A2"].LoadFromText(a);
                for (int i = 0; i < result.HeaderVT.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderVT[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderVT.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.VTHistories.Select(f => new
                {
                    f.PROJECT,
                    f.ITEM,

                    f.REV_NO,
                    f.CHECK_NO,
                    f.PART,
                    f.POINT,
                    f.PROBLEM,
                    f.DWG,
                    f.ACT,
                    f.WRONG,
                    f.EDIT,
                    f.CHECKER,
                    f.CHECK_DATE

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Export" + TYPE.ToLower() + "Detail" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetSummarySalary(string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();

            var serviceResult = new List<PlusModel>();

            if (TYPE == "PLUS")
            {
                serviceResult = await service.GetSummarySalaryPlus(model);
            }
            else if (TYPE == "MINUS")
            {
                serviceResult = await service.GetSummarySalaryMinus(model);
            }
            else
            {
                serviceResult = await service.GetSummarySalaryTotal(model);
            }

            DetailPlusModel result = new ReportController().MapDetailPlusModel(serviceResult, true);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                //// Load your collection "accounts"
                //ws.Cells["A1"].LoadFromText(name);
                //ws.Cells["A2"].LoadFromText(a);
                for (int i = 0; i < result.HeaderPlus.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderPlus[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderPlus.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.ROW_NUM,
                    f.EMP_CODE,

                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.FIX_SALARY,
                    f.POSITION_SALARY,

                    f.ALL_SALARY,
                    f.SUM_SALARY,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSummarySalary" + TYPE.ToLower() + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDesingChange(string START_DATE, string END_DATE, string GROUP, string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchModel model = new ReportSearchModel();

            model.START_DATE = START_DATE;
            model.END_DATE = END_DATE;
            model.GROUP = GROUP;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<ReportDesignChangeModel>();

            serviceResult = await service.GetReportDesingChangeData(model);


            DetailDesingModel result = new AdminController().MapDetailDesingModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderDesing.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderDesing[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderDesing.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.WRONG,
                    f.DWG,

                    f.PROJECT,
                    f.ITEM,
                    f.CHECK_DATE,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportDesingChange" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDimension(string START_DATE, string END_DATE, string GROUP, string SEARCH, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchModel model = new ReportSearchModel();

            model.START_DATE = START_DATE;
            model.END_DATE = END_DATE;
            model.GROUP = GROUP;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();


            var serviceResult = new List<ReportDimensionModel>();

            if (TYPE == "DIMENSION")
                serviceResult = await service.GetReportDimensionData(model);
            else if (TYPE == "PLUS")
                serviceResult = await service.GetReportPlusData(model);
            else
                serviceResult = await service.GetReportOtherData(model);


            DetailOtherModel result = new AdminController().MapDetailDimensionModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderOther.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderOther[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderOther.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.ROW_NUM,
                    f.EMP_CODE,

                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.PROCESS,
                    f.PRICE,
                    f.ACTUAL,
                    f.ACTUAL_DATE
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Export" + TYPE + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetMinus(string START_DATE, string END_DATE, string GROUP, string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchModel model = new ReportSearchModel();

            model.START_DATE = START_DATE;
            model.END_DATE = END_DATE;
            model.GROUP = GROUP;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<ReportMinusModel>();

            serviceResult = await service.GetReportMinusData(model);


            DetailMinusModel result = new AdminController().MapDetailMinusModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderMinus.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderMinus[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderMinus.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.ROW_NUM,
                    f.EMP_CODE,

                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.PRICE,
                    f.ACTUAL_DATE,
                    f.REMARK
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportMinus" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetVT(string START_DATE, string END_DATE, string GROUP, string SEARCH, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchModel model = new ReportSearchModel();

            model.START_DATE = START_DATE;
            model.END_DATE = END_DATE;
            model.GROUP = GROUP;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE = TYPE;

            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<VTListModel>();

            serviceResult = await service.GetReportVTData(model);


            DetailVTModel result = new AdminController().MapDetailVTModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderVT.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderVT[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderVT.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {

                    f.PROJECT,
                    f.ITEM,
                    f.REV_NO,
                    f.CHECK_NO,
                    f.PART,
                    f.POINT,
                    f.PROBLEM,
                    f.DWG,
                    f.ACT,
                    f.WRONG,
                    f.EDIT,
                    f.CHECKER,
                    f.CHECK_DATE,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportVT" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportSummarySalaryDayForWeekWeld(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            try
            {
                var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
                DateModel model = new DateModel();

                model.MONTH = MONTH;
                model.YEAR = YEAR;
                model.WEEK = DataConverter.GetInteger(WEEK);
                model.DEPT_CODE = DEPT_CODE;
                model.SEARCH = SEARCH;
                model.SORT = "";
                model.SORT_TYPE = 1;
                model.TAKE = 10000;
                model.SKIP = 0;
                model.TYPE_STORE = TYPE_STORE;

                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryForWeekWeld(model);



                DetailSummarySalaryModel result = new ReportController().MapDetailSummarSalaryModel(serviceResult, true);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                    //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                    //// Load your collection "accounts"
                    //ws.Cells["A1"].LoadFromText(name);
                    //ws.Cells["A2"].LoadFromText(a);
                    if (model.WEEK == 0)
                    {
                        ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือนช่างเชื่อม {0}-{1}", model.YEAR, model.MONTH);
                    }

                    else
                    {
                        ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือนช่างเชื่อม สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                    }
                    for (int i = 0; i < result.HeaderSummarySalary.Count; i++)
                    {
                        ws.Cells[2, i + 1].Value = result.HeaderSummarySalary[i].head;
                    }

                    using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderSummarySalary.Count])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        range.Style.Font.Color.SetColor(Color.Black);
                    }

                    var Export = serviceResult.GoalModel.Select(f => new
                    {
                        f.EMP_CODE,
                        f.FULL_NAME,
                        f.DEPT_NAME,
                        f.WORK_TYPE,
                        f.CERT,
                        f.EMP_GOAL,
                        f.GOAL,
                        f.DATA_1,
                        f.DATA_2,
                        f.DATA_3,
                        f.DATA_4,
                        f.DATA_5,
                        f.DATA_6,
                        f.DATA_7
                        ,
                        f.DATA_8
                        ,
                        f.DATA_9,
                        f.DATA_10,
                        f.DATA_11,
                        f.DATA_37,
                        f.DATA_12,
                        f.DATA_27,
                        f.DATA_28,
                        f.DATA_29,
                        f.DATA_30,
                        f.DATA_31,
                        f.DATA_32,

                        f.DATA_13,
                        f.DATA_14,
                        f.DATA_15,
                        f.DATA_16,
                        f.DATA_17,
                        f.DATA_18,
                        f.DATA_19,
                        f.DATA_20,
                        f.DATA_21,
                        f.DATA_22,
                        f.DATA_23,
                        f.DATA_33,
                        f.DATA_34,
                        f.DATA_38,
                        f.DATA_39,
                        f.DATA_35,
                        f.DATA_40,
                        f.DATA_36,
                        f.DATA_41,
                        f.DATA_24,
                        f.DATA_25,
                        f.DATA_42,
                        f.DATA_26,
                        f.DATA_43,
                        f.DATA_44,
                        f.DATA_45,
                    });

                    ws.SelectedRange[3, 1].LoadFromCollection(Export);

                    ws.Cells.AutoFitColumns();

                    Byte[] fileBytes = pck.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ExportSummarySalaryDayForWeekWeld" + dateExport + ".xlsx");
                    // Replace filename with your custom Excel-sheet name.

                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Export", FUNC_NAME = "ExportSummarySalaryDayForWeek", ERROR_MSG = ex.Message });
                return RedirectToAction("../Report/ReportSummarySalaryForWeekWeld");
            }
        }

        public async Task<ActionResult> ExportSummaryWorkDayForWeekWeld(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE_STORE;

            var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
            var serviceResult = await service.GetSummaryWorkDayForWeekWeld(model);

            DetailSummaryWorkModel result = new ReportController().MapDetailSummaryWorkModel(serviceResult, true);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                //// Load your collection "accounts"
                //ws.Cells["A1"].LoadFromText(name);
                //ws.Cells["A2"].LoadFromText(a);
                for (int i = 0; i < result.HeaderSummaryWork.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderSummaryWork[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderSummaryWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export = serviceResult.GoalModel.Select(f => new { f.EMP_CODE, f.FULL_NAME, f.DEPT_NAME, f.EMP_GOAL, f.GOAL, f.DATA_1, f.DATA_2, f.DATA_3, f.DATA_4, f.DATA_5 });

                ws.SelectedRange[2, 1].LoadFromCollection(Export);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSummaryWorkDayForWeekWeld" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportSummarySalaryDayForWeekWeldSum(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            try
            {
                var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
                DateModel model = new DateModel();

                model.MONTH = MONTH;
                model.YEAR = YEAR;
                model.WEEK = DataConverter.GetInteger(WEEK);
                model.DEPT_CODE = DEPT_CODE;
                model.SEARCH = SEARCH;
                model.SORT = "";
                model.SORT_TYPE = 1;
                model.TAKE = 10000;
                model.SKIP = 0;
                model.TYPE_STORE = TYPE_STORE;

                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryForWeekWeldSum(model);



                DetailSummarySalaryModel result = new ReportController().MapDetailSummarSalarySumModel(serviceResult, true);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                    //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                    //// Load your collection "accounts"
                    //ws.Cells["A1"].LoadFromText(name);
                    //ws.Cells["A2"].LoadFromText(a);
                    for (int i = 0; i < result.HeaderSummarySalary.Count; i++)
                    {
                        ws.Cells[1, i + 1].Value = result.HeaderSummarySalary[i].head;
                    }

                    using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderSummarySalary.Count])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        range.Style.Font.Color.SetColor(Color.Black);
                    }

                    var Export = serviceResult.GoalModel.Select(f => new
                    {
                        f.EMP_CODE,
                        f.FULL_NAME,
                        f.DEPT_NAME,
                        f.EMP_GOAL,

                        f.DATA_26,
                        f.DATA_1,
                        f.DATA_2,
                        f.DATA_6,
                        f.DATA_23,
                        f.DATA_24,
                        f.DATA_35,
                        f.DATA_36,
                        f.DATA_33,
                        f.DATA_22,
                        f.DATA_37,
                        f.DATA_25,
                    });

                    ws.SelectedRange[2, 1].LoadFromCollection(Export);

                    ws.Cells.AutoFitColumns();

                    Byte[] fileBytes = pck.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ExportSummarySalaryDayForWeekWeldSum" + dateExport + ".xlsx");
                    // Replace filename with your custom Excel-sheet name.

                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Export", FUNC_NAME = "ExportSummarySalaryDayForWeekSum", ERROR_MSG = ex.Message });
                return RedirectToAction("../Report/ReportSummarySalaryForWeekWeldSum");
            }
        }


        public async Task<ActionResult> ExportSummarySalaryDayForWeekSum(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            try
            {
                var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
                DateModel model = new DateModel();

                model.MONTH = MONTH;
                model.YEAR = YEAR;
                model.WEEK = DataConverter.GetInteger(WEEK);
                model.DEPT_CODE = DEPT_CODE;
                model.SEARCH = SEARCH;
                model.SORT = "";
                model.SORT_TYPE = 1;
                model.TAKE = 10000;
                model.SKIP = 0;
                model.TYPE_STORE = TYPE_STORE;

                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummarySalaryForWeekSum(model);



                DetailSummarySalaryModel result = new ReportController().MapDetailSummarSalarySumModel(serviceResult, true);

                using (ExcelPackage pck = new ExcelPackage())
                {

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");
                    //// ws.Cells["A1"].LoadFromCollection(accounts, true);
                    //// Load your collection "accounts"
                    //ws.Cells["A1"].LoadFromText(name);
                    //ws.Cells["A2"].LoadFromText(a);
                    for (int i = 0; i < result.HeaderSummarySalary.Count; i++)
                    {
                        ws.Cells[1, i + 1].Value = result.HeaderSummarySalary[i].head;
                    }

                    using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderSummarySalary.Count])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        range.Style.Font.Color.SetColor(Color.Black);
                    }

                    var Export = serviceResult.GoalModel.Select(f => new
                    {
                        f.EMP_CODE,
                        f.FULL_NAME,
                        f.DEPT_NAME,
                        f.EMP_GOAL,
                        f.DATA_1,
                        f.DATA_2,
                        f.DATA_3,
                        f.DATA_4,
                        f.DATA_5,
                        f.DATA_6,
                        f.DATA_7,
                        f.DATA_8,
                        f.DATA_9,
                        f.DATA_10,
                        f.DATA_11,
                        f.DATA_12,
                    });

                    ws.SelectedRange[2, 1].LoadFromCollection(Export);

                    ws.Cells.AutoFitColumns();

                    Byte[] fileBytes = pck.GetAsByteArray();
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=ExportSummarySalaryDayForWeekSum" + dateExport + ".xlsx");
                    // Replace filename with your custom Excel-sheet name.

                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Export", FUNC_NAME = "ExportSummarySalaryDayForWeekSum", ERROR_MSG = ex.Message });
                return RedirectToAction("../Report/ReportSummarySalaryForWeekSum");
            }
        }

        public async Task<ActionResult> ExportGetNGNew(string MONTH, string YEAR, string DEPT_CODE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.DEPT_CODE = DEPT_CODE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<NGDataModel>();

            serviceResult = await service.GetUploadNGNew(model);


            DetailNGNewModel result = new AdminController().MapDetailNGNewModel(serviceResult, false, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderNGNew.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderNGNew[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderNGNew.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {

                    f.WEEK_KEY,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.TOTAL_JOINT,
                    f.JOINT_NG,
                    f.PERCENT_NG,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportVT" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetNG(string MONTH, string DEPT_CODE, string WEEK_SUB)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            // model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.DEPT_CODE = DEPT_CODE;
            model.WEEK = DataConverter.GetInteger(WEEK_SUB);
            model.SUM = false;
            model.EDIT = false;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<NGDataModel>();

            serviceResult = await service.GetUploadNG(model);


            DetailNGModel result = new AdminController().MapDetailNGEditModel(serviceResult, false, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderNG.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderNG[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderNG.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.WEEK_YEAR,
                    f.WEEK_NAME,
                    f.GROUP_NAME,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.NG,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportNG" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetWeek(string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchModel model = new ReportSearchModel();

            // model.YEAR = YEAR;
            model.SEARCH = SEARCH;

            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<UploadWeekModel>();

            serviceResult = await service.GetReportWeekData(model);


            DetailUploadWeekModel result = new AdminController().MapDetailWeekModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderUploadWeek.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderUploadWeek[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderUploadWeek.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.ROW_NUM,
                    f.WEEK_YEAR,
                    f.WEEK_MONTH


                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWeek" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetGoal(string MONTH, string YEAR, string DEPT_CODE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            // model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.DEPT_CODE = DEPT_CODE;

            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<UploadGoalModel>();

            serviceResult = await service.GetUploadGoal(model);


            DetailUploadGoalModel result = new AdminController().MapDetailGoalModel(serviceResult, false, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderUploadGoal.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderUploadGoal[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderUploadGoal.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.WEEK_KEY,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.LEVEL_NAME,
                    f.LEVEL_GOAL,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGoal" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetCutFinishing(string MONTH, string YEAR, string WEEK_SUB, string DEPT_CODE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            // model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK_SUB);
            model.DEPT_CODE = DEPT_CODE;

            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<UploadCutFinisingModel>();

            serviceResult = await service.GetUploadCutFinising(model);


            DetailCutFinisingModel result = new AdminController().MapDetailCutFinisingModel(serviceResult, false, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderCutFinising.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderCutFinising[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderCutFinising.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.WEEK_KEY,
                    f.WEEK_NAME,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.AMOUNT,
                    f.EMP_CODE_RECIVE,
                    f.FULL_NAME_RECIVE,
                    f.DEPT_NAME_RECIVE

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportCutFininshing" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }


        public async Task<ActionResult> ExportGetActUTNew(string MONTH, string YEAR, string DEPT_CODE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            // model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.DEPT_CODE = DEPT_CODE;

            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<UploadActUTModel>();

            serviceResult = await service.GetUploadActUTNew(model);


            DetailActUTNewModel result = new AdminController().MapDetailActUTNewModel(serviceResult, false, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderActUTNew.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderActUTNew[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderActUTNew.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.WEEK_KEY,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.NG,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetActUTNew" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetAdjustGoal(string MONTH, string YEAR, string DEPT_CODE, string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            // model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<GoalSumModel>();

            serviceResult = await service.GetAdjustGoalData(model);


            DetailAdjustGoalModel result = new AdminController().MapDetailAdjustGoalModel(serviceResult, true);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderAdjustGoal.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderAdjustGoal[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderAdjustGoal.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.WEEK_KEY,
                    f.SUM_PART,
                    f.CURRENT_GOAL,
                    f.ADJUST_GOAL,
                    f.NEW_GOAL
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetAdjustGoal" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetInsertStructure(string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            SearchModel model = new SearchModel();

            // model.YEAR = YEAR;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();

            var serviceResult = new List<LevelModel>();

            serviceResult = await service.GetLevel(model);


            StructureModel result = new LevelController().MapRequestMasterListToTodoListModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderLevel.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderLevel[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderLevel.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {

                    f.TYPE_NAME,
                    f.LEVEL_ID,
                    f.LEVEL_NAME,
                    f.LEVEL_GOAL,
                    f.LEVEL_SALARY,
                    f.LEVEL_PROCEDURE,
                    f.SALARY,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetInsertStructure" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetInsertStructureNCR(string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            SearchModel model = new SearchModel();

            // model.YEAR = YEAR;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();

            var serviceResult = new List<LevelNCRModel>();

            serviceResult = await service.GetLevelNCR(model);


            StructureNCRModel result = new LevelController().MapLevelNCRtModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderLevelNCR.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderLevelNCR[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderLevelNCR.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {

                    f.TYPE_NAME,
                    f.LEVEL_GOAL,
                    f.MAIN_MIN,
                    f.MAIN_MAX,
                    f.TEMPO_MIN,
                    f.TEMPO_MAX,
                    f.PRICE_QULITY,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetInsertStructureNCR" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetInsertStructureNG(string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            SearchModel model = new SearchModel();

            // model.YEAR = YEAR;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();

            var serviceResult = new List<LevelNGModel>();

            serviceResult = await service.GetLevelNG(model);


            StructureNGModel result = new LevelController().MapLevelNGModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderLevelNG.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderLevelNG[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderLevelNG.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {

                    f.TYPE_NAME,
                    f.LEVEL_GOAL,
                    f.NG_MIN,
                    f.NG_MAX,
                    f.PRICE_QUALITY,
                    f.PRICE_UT,
                    f.QUALITY_DESC,
                    f.PROCEDURE_DESC,

                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetInsertStructureNG" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }



        public async Task<ActionResult> ExportGetAutoGas1(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkModel();

            serviceResult = await service.GetSummaryForWorkByAutoGas1(model);


            DetailPartSummaryForWorkModel result = new SummaryWorkController().MapDetailPart0SummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน autogas1 {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน autogas1 สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }


                for (int i = 0; i < result.HeaderPartSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas1" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetAutoGas2(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkModel();

            serviceResult = await service.GetSummaryForWorkByAutoGas2(model);


            DetailPartSummaryForWorkModel result = new SummaryWorkController().MapDetail2PartSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน autogas2 {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน autogas2 สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWork.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas2" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetAutoGas3(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkModel();

            serviceResult = await service.GetSummaryForWorkByAutoGas3(model);


            DetailPartSummaryForWorkModel result = new SummaryWorkController().MapDetail2PartSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน autogas3 {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน autogas3 สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWork.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas3" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetTaper(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkModel();

            serviceResult = await service.GetSummaryForWorkByTaper(model);


            DetailPartSummaryForWorkModel result = new SummaryWorkController().MapDetail2PartSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน taper {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน taper สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWork.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportTaper" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetPart(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkModel();

            serviceResult = await service.GetSummaryForWorkByPart(model);


            DetailPartSummaryForWorkModel result = new SummaryWorkController().MapDetailPart0SummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน part {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน part สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }


                for (int i = 0; i < result.HeaderPartSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                ws.Cells["G:M"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportPart" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetSalaryAutoGas1(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryModel();

            serviceResult = await service.GetSummaryForSalaryByAutoGas1(model);


            DetailPartSummaryForSalaryModel result = new SummarySalaryController().MapDetailAutoGas1SummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน autogas1 {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน autogas1 สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }

                for (int i = 0; i < result.HeaderPartSummaryForSalary.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalary[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalary.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });

                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                    f.DATA_36,
                    f.DATA_37,
                    f.DATA_38,
                    f.DATA_39,
                    f.DATA_40,
                    f.DATA_41,
                    f.DATA_42,
                    f.DATA_43,
                    f.DATA_44,
                    f.DATA_45,
                    f.DATA_46,
                    f.DATA_47,
                    f.DATA_48,
                    f.DATA_49,
                });

                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalary.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetSalaryAutoGas1" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetSalaryAutoGas2(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryModel();

            serviceResult = await service.GetSummaryForSalaryByAutoGas2(model);


            DetailPartSummaryForSalaryModel result = new SummarySalaryController().MapDetailAutoGas2SummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน autogas2 {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน autogas2 สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }

                for (int i = 0; i < result.HeaderPartSummaryForSalary.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalary[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalary.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });

                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,

                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });

                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 7].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalary.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryAutoGas2" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetSalaryAutoGas3(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryModel();

            serviceResult = await service.GetSummaryForSalaryByAutoGas3(model);


            DetailPartSummaryForSalaryModel result = new SummarySalaryController().MapDetailAutoGas2SummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน autogas3 {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน autogas3 สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalary.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalary[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalary.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });

                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 7].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalary.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryAutogas3" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetSalaryTaper(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryModel();

            serviceResult = await service.GetSummaryForSalaryByTaper(model);


            DetailPartSummaryForSalaryModel result = new SummarySalaryController().MapDetailAutoGas2SummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน taper {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน taper สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalary.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalary[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalary.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 7].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalary.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryTaper" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetSalaryPart(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryModel();

            serviceResult = await service.GetSummaryForSalaryByPart(model);


            DetailPartSummaryForSalaryModel result = new SummarySalaryController().MapDetailPartSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน part {0}-{1}", model.YEAR, model.MONTH);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน part สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalary.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalary[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalary.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                    f.DATA_36,
                    f.DATA_37,
                    f.DATA_38,
                    f.DATA_39,
                    f.DATA_40,
                    f.DATA_41,
                    f.DATA_42,
                    f.DATA_43,
                    f.DATA_44,
                    f.DATA_45,
                    f.DATA_46,
                    f.DATA_47,
                    f.DATA_48,
                    f.DATA_49,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 7].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalary.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryPart" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailAutoGas1(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.PROCESS_TYPE = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new PartDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByAutoGas1(model);


            DetailPartDetailSummaryForWorkModel result = new SummaryWorkController().MapDetail1PartDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("รายละเอียด {3} สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE);


                for (int i = 0; i < result.HeaderPartDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ID,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.PD_CODE,
                    f.SYMBOL,
                    f.PART_ITEM,
                    f.PART_SIZE,
                    f.CUTTING_PLAN,
                    f.PROCESS,
                    f.HOLE,
                    f.TAPER,
                    f.PLAN_QTY,
                    f.ACT_QTY,
                    f.ACT_LENGTH,
                    f.ACT_GROUP,
                    f.ACTUAL_USER,
                    f.ACT_NAME,
                    f.ACT_DATE
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas1Detail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailAutoGas2(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new PartDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByAutoGas2(model);


            DetailPartDetailSummaryForWorkModel result = new SummaryWorkController().MapDetail2PartDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("รายละเอียดวันที่ {3} สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE);


                for (int i = 0; i < result.HeaderPartDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ID,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.PD_CODE,
                    f.SYMBOL,
                    f.PART_ITEM,
                    f.PART_SIZE,
                    f.CUTTING_PLAN,
                    f.PROCESS,
                    f.HOLE,
                    f.TAPER,
                    f.PLAN_QTY,
                    f.ACT_QTY,
                    f.ACT_GROUP,
                    f.ACTUAL_USER,
                    f.ACT_NAME,
                    f.ACT_DATE
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas2Detail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailAutoGas3(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new PartDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByAutoGas3(model);


            DetailPartDetailSummaryForWorkModel result = new SummaryWorkController().MapDetail2PartDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("รายละเอียดวันที่ {3} สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE);


                for (int i = 0; i < result.HeaderPartDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ID,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.PD_CODE,
                    f.SYMBOL,
                    f.PART_ITEM,
                    f.PART_SIZE,
                    f.CUTTING_PLAN,
                    f.PROCESS,
                    f.HOLE,
                    f.TAPER,
                    f.PLAN_QTY,
                    f.ACT_QTY,
                    f.ACT_GROUP,
                    f.ACTUAL_USER,
                    f.ACT_NAME,
                    f.ACT_DATE
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas3Detail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailTaper(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new PartDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByTaper(model);


            DetailPartDetailSummaryForWorkModel result = new SummaryWorkController().MapDetail2PartDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("รายละเอียดวันที่ {3} สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE);


                for (int i = 0; i < result.HeaderPartDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ID,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.PD_CODE,
                    f.SYMBOL,
                    f.PART_ITEM,
                    f.PART_SIZE,
                    f.CUTTING_PLAN,
                    f.PROCESS,
                    f.HOLE,
                    f.TAPER,
                    f.PLAN_QTY,
                    f.ACT_QTY,
                    f.ACT_GROUP,
                    f.ACTUAL_USER,
                    f.ACT_NAME,
                    f.ACT_DATE
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportTaperDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailPart(string ID, string MONTH, string YEAR, string WEEK, string TYPE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.PROCESS_TYPE = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new PartDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByPart(model);


            DetailPartDetailSummaryForWorkModel result = new SummaryWorkController().MapDetail1PartDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("รายละเอียด {3} สัปดาห์ ที่ {0} ของเดือน {1}-{2}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE);


                for (int i = 0; i < result.HeaderPartDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ID,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.PD_CODE,
                    f.SYMBOL,
                    f.PART_ITEM,
                    f.PART_SIZE,
                    f.CUTTING_PLAN,
                    f.PROCESS,
                    f.HOLE,
                    f.TAPER,
                    f.PLAN_QTY,
                    f.ACT_QTY,
                    f.ACT_LENGTH,
                    f.ACT_GROUP,
                    f.ACTUAL_USER,
                    f.ACT_NAME,
                    f.ACT_DATE
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:D1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportAutoGas1Detail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2

        // 2018-01-31 chaiwud.ta
        // Add Export Data

        // BuiltUp Beam
        public async Task<ActionResult> ExportGetWorkBuilt(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltUpBeam(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuilt(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltUpBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            string titleExcel = "";
            if (TYPE == "ผลงาน")
            {
                titleExcel = $"{PROCESS} สัปดาห์ ที่ {DataConverter.GetString(model.WEEK)} {matchRangWeek}";
            }
            else
            {
                titleExcel = $"{PROCESS} รายละเอียดวันที่ {TYPE}/{MONTH}/{YEAR} สัปดาห์ที่ {DataConverter.GetString(model.WEEK)}";
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{0}", titleExcel);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // Saw
        public async Task<ActionResult> ExportGetWorkBuiltSaw(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltSawBeam(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL,
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltSaw(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltSawBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            string titleExcel = "";
            if (TYPE == "ผลงาน")
            {
                titleExcel = $"{PROCESS} สัปดาห์ ที่ {DataConverter.GetString(model.WEEK)} {matchRangWeek}";
            }
            else
            {
                titleExcel = $"{PROCESS} รายละเอียดวันที่ {TYPE}/{MONTH}/{YEAR} สัปดาห์ที่ {DataConverter.GetString(model.WEEK)}";
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{0}", titleExcel);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // Adjust
        public async Task<ActionResult> ExportGetWorkBuiltAdjust(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltAdjustBeam(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion


            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column 
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltAdjust(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltAdjustBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            string titleExcel = "";
            if (TYPE == "ผลงาน")
            {
                titleExcel = $"{PROCESS} สัปดาห์ ที่ {DataConverter.GetString(model.WEEK)} {matchRangWeek}";
            }
            else
            {
                titleExcel = $"{PROCESS} รายละเอียดวันที่ {TYPE}/{MONTH}/{YEAR} สัปดาห์ที่ {DataConverter.GetString(model.WEEK)}";
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{0}", titleExcel);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // Drill & Shot Blast
        public async Task<ActionResult> ExportGetWorkBuiltDrillShot(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltDrillShotBeam(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split colum
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltDrillShot(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;
            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltDrillShotBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            string titleExcel = "";
            if (TYPE == "ผลงาน")
            {
                titleExcel = $"{PROCESS} สัปดาห์ ที่ {DataConverter.GetString(model.WEEK)} {matchRangWeek}";
            }
            else
            {
                titleExcel = $"{PROCESS} รายละเอียดวันที่ {TYPE}/{MONTH}/{YEAR} สัปดาห์ที่ {DataConverter.GetString(model.WEEK)}";
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{0}", titleExcel);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // Salary Export Built Beam
        public async Task<ActionResult> ExportGetSalaryBuiltUpBeam(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltUpBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Up Beam {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Up Beam สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltUpBeam" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltSawBeam(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltSawBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Saw Beam {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Saw Beam สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltSawBeam" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltAdjustBeam(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltAdjustBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Adjust Beam {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Adjust Beam สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltAdjustBeam" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltDrillShotBeam(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltDrillShotBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion


            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Cut Drill & Shot Blast Beam  {2}", model.YEAR, model.MONTH, matchRangWeek);
                }
                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Cut Drill & Shot Blast Beam สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltDrillShotBeam" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        // Level Built
        // 2018-02-10 by chaiwud.ta
        public async Task<ActionResult> ExportGetInsertStructureBuilt(string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            SearchModel model = new SearchModel();

            // model.YEAR = YEAR;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();

            var serviceResult = new List<LevelBuiltModel>();

            serviceResult = await service.GetBuiltLevel(model);


            StructureBuiltModel result = new LevelController().MapRequestMasterListToTodoListBuiltModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderBuiltLevel.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderBuiltLevel[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderBuiltLevel.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.TYPE_NAME,
                    f.LEVEL_ID,
                    f.LEVEL_NAME,
                    f.LEVEL_GOAL,
                    f.LEVEL_SALARY,
                    f.LEVEL_QUALITY,
                    f.LEVEL_PROCEDURE,
                    f.SALARY,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGetInsertStructureBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }
            return RedirectToAction("Index");
        }

        // Built Box
        // 2018-02-12 by chaiwud.ta
        // FabDiaphragmBox
        public async Task<ActionResult> ExportGetWorkBuiltFabDiaphragmBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltFabDiaphragmBox(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2); // edit add
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltFabDiaphragmBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltFabDiaphragmBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // BuiltUpBox
        public async Task<ActionResult> ExportGetWorkBuiltUpBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltUpBox(model); // contact store

            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltUpBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltUpBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetailBuiltDatailSummaryForWorkModelBuiltBox(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.BUILT_SIZE,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // AutoRootBox 
        public async Task<ActionResult> ExportGetWorkBuiltAutoRootBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltAutoRootBox(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltAutoRootBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltAutoRootBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetailAutotRootBuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.BUILT_LENGTH, // AutoRoot
                    f.HOLE, // AutoRoot
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // DrillSesnetBox
        public async Task<ActionResult> ExportGetWorkBuiltDrillSesnetBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltDrillSesnetBox(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltDrillSesnetBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltDrillSesnetBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // GougingRepairBox
        public async Task<ActionResult> ExportGetWorkBuiltGougingRepairBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltGougingRepairBox(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column 
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltGougingRepairBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltGougingRepairBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // FacingBox
        public async Task<ActionResult> ExportGetWorkBuiltFacingBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByBuiltFacingBox(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetDetailBuiltFacingBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByBuiltFacingBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            string titleExcel = "";
            if (TYPE == "ผลงาน")
            {
                titleExcel = $"{PROCESS} สัปดาห์ ที่ {DataConverter.GetString(model.WEEK)} {matchRangWeek}";
            }
            else
            {
                titleExcel = $"{PROCESS} รายละเอียดวันที่ {TYPE}/{MONTH}/{YEAR} สัปดาห์ที่ {DataConverter.GetString(model.WEEK)}";
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2BuiltDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{0}", titleExcel);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PJ_NAME,
                    f.PD_ITEM,
                    f.ITEM_NO,
                    f.BUILT_TYPE_NAME,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.PLACE,
                    f.BUILT_WEIGHT,
                    f.BUILT_PLAN,
                    f.PC_NAME,
                    f.UG_NAME,
                    f.PA_PLAN_DATE,
                    f.ACT_USER,
                    f.FULLNAME,
                    f.PA_ACTUAL_DATE,
                    f.BUILT_ACTUAL,
                    f.PA_NO
                });
                //var Export2 = serviceResult.Data.Select(f => new
                //{
                //    f.DATA_1,
                //    f.DATA_2,
                //    f.DATA_3,
                //    f.DATA_4,
                //    f.DATA_5,
                //    f.DATA_6,
                //});
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                //ws.SelectedRange[3, 7].LoadFromCollection(Export2);
                //ws.Cells["G:L"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkBuiltDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        // Salary Export Built Box
        public async Task<ActionResult> ExportGetSalaryBuiltFabDiaphragmBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltFabDiaphragmBox(model);

            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBeamSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Fab Diaphragm {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Fab Diaphragm สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltFabDiaphragm" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltUpBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltUpBox(model);

            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Up Box {3}", model.YEAR, model.MONTH, model.START_DATE, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Built Up Box สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltUpBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltAutoRootBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltAutoRootBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Auto Root Box {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Auto Root Box สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltAutoRootBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltDrillSesnetBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltDrillSesnetBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Drill & Sesnet Box {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Drill & Sesnet Box สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltDrillSesnetBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltGougingRepairBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltGougingRepairBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Gouging + Repair {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Gouging + Repair สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltGougingRepairBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltFacingBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByBuiltFacingBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Facing Box {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Facing Box สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalaryBuiltFacingBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetGoalBuilt(string MONTH, string YEAR, string DEPT_CODE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            // model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.DEPT_CODE = DEPT_CODE;

            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<UploadGoalBuiltModel>();

            serviceResult = await service.GetUploadGoalBuilt(model);


            DetailUploadGoalModel result = new AdminController().MapDetailGoalModelBuilt(serviceResult, false, false);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderUploadGoal.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderUploadGoal[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderUploadGoal.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.WEEK_KEY,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.LEVEL_NAME,
                    f.LEVEL_GOAL,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportGoal" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }
            return RedirectToAction("Index");
        }

        // Get StartDate LastDate of Week. Month, Year
        // $week $month $year
        public string GetDatefirstOfWeek(string rangeStr, string rangeYear)
        {
            string dateRange = "";
            if (!string.IsNullOrEmpty(rangeStr) && !string.IsNullOrEmpty(rangeYear))
            {
                string[] splitstr = rangeStr.Split('-');
                if (splitstr.Length > 1)
                {
                    splitstr[0] = splitstr[0].Replace(" ", "");
                    splitstr[1] = splitstr[1].Replace(" ", "");
                    dateRange = $"{splitstr[0].Trim()}/{rangeYear} - {splitstr[1].Trim()}/{rangeYear}";
                } else
                {
                    string newDateStr = rangeStr.Replace(" ", "");
                    dateRange = $"{newDateStr.Trim()}/{rangeYear}";
                }
            }
            return dateRange;
        }

        public string GetFirstLastOfMonth(string MONTH, string rangeYear)
        {
            string dateRange = "";

            if (!string.IsNullOrEmpty(MONTH) && !string.IsNullOrEmpty(rangeYear))
            {
                int _month = 0, _rangeYear = 0;
                _rangeYear = DataConverter.GetInteger(rangeYear);
                _month = DataConverter.GetInteger(MONTH);
                DateTime firstDayOfMonth = new DateTime(_rangeYear, _month, 1);
                DateTime lastDayofMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                dateRange = $"{firstDayOfMonth.ToString("dd/MM/yyyy")} - {lastDayofMonth.ToString("dd/MM/yyyy")}";
            }
            return dateRange;
        }

        public async Task<ActionResult> ExportGetUTDataByUser(string MONTH, string YEAR, string DEPT_CODE, int WEEK_SUB)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.YEAR = YEAR;
            model.MONTH = MONTH;
            model.WEEK = WEEK_SUB;
            model.DEPT_CODE = DEPT_CODE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<UTDataByUserModelBuilt>();

            serviceResult = await service.GetUploadUTDataByUserBuilt(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK_SUB).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailUTDataByUserBuiltModel result = new AdminController().MapDetailUTDataByUserDataModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderUTDataByUserBuilt.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderUTDataByUserBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderUTDataByUserBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = serviceResult.Select(f => new
                {
                    f.ROW_NUM,
                    f.WEEK_KEY,
                    f.WEEK_SUB,
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.GROUP_NAME,
                    f.UT1,
                    f.UT2,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportUTDataByUser" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }


        public async Task<ActionResult> ExportGetNGDataBuilt(string START_DATE, string END_DATE, int PROJECT, string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchBuiltModel model = new ReportSearchBuiltModel();


            /*
             START_DATE
             END_DATE
             PROJECT
             SEARCH
             */

            model.START_DATE = START_DATE;
            model.END_DATE = END_DATE;
            model.PROJECT = PROJECT;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<NGDataModelBuilt>();

            serviceResult = await service.GetUploadNGDataBuilt(model);


            DetailNGDataBuiltModel result = new AdminController().MapDetailNGDataDataModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderNGDataBuilt.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderNGDataBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderNGDataBuilt.Count - 1])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.NGDataBuiltHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.DATE,
                    f.PROJECT,
                    f.PD_ITEM,
                    f.CODE,
                    f.SKIN_PLATE,
                    f.DIAPHRAGM,
                    f.SAW_NG_LENGTH,
                    f.SAW_NG_JOINT,
                    f.SAW_TOTAL,
                    f.SAW_D,
                    f.SAW_K,
                    f.SAW_WELDER,
                    f.AUTOROOT_NG_LENGTH,
                    f.AUTOROOT_NG_JOINT,
                    f.AUTOROOT_TOTAL,
                    f.AUTOROOT_D,
                    f.AUTOROOT_K,
                    f.AUTOROOT_WELDER,
                    f.ESW_NG_LENGTH,
                    f.ESW_NG_JOINT,
                    f.ESW_TOTAL,
                    f.ESW_D,
                    f.ESW_X,
                    f.ESW_SIDE,
                    f.ESW_WELDER,
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportNGDataBuilt" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetCamberDataOther(string START_DATE, string END_DATE, string GROUP, string SEARCH)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            ReportSearchOtherFinishModel model = new ReportSearchOtherFinishModel();


            /*
             START_DATE
             END_DATE
             PROJECT
             SEARCH
             */

            model.START_DATE = START_DATE;
            model.END_DATE = END_DATE;
            model.PROJECT = 0;
            model.PD_ITEM = 0;
            model.SEARCH = SEARCH;
            model.CUTTING_PLAN = "";
            model.GROUP = GROUP;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;


            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();

            var serviceResult = new List<CamberDataModelOther>();

            serviceResult = await service.GetUploadCamberDataOther(model);


            DetailCamberDataOtherModel result = new AdminController().MapDetailCamberOtherDataDataModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                for (int i = 0; i < result.HeaderCamberDataOther.Count - 1; i++)
                {
                    ws.Cells[1, i + 1].Value = result.HeaderCamberDataOther[i].head;
                }

                using (ExcelRange range = ws.Cells[1, 1, 1, result.HeaderCamberDataOther.Count - 1])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.CamberDataOtherHistories.Select(f => new
                {
                    f.ROW_NUM,
                    f.PROJECT,
                    f.PD_ITEM,
                    f.CUTTING_PLAN,
                    f.BUILT_NO,
                    f.ACTUAL_GROUP,
                    f.ACTUAL_DATE
                });

                ws.SelectedRange[2, 1].LoadFromCollection(Export1);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportCamberDataOther" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }


        #region Other Beam/Box
        // Other Beam
        public async Task<ActionResult> ExportGetWorkBuiltOtherAdjustBeam(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByOtherBeam(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column 
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWork_OtherAdjustBeam" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltOtherAdjustBeam(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByOtherBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Other Adjust Beam  {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Other Adjust Beam สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalary_OtherAdjustBeam" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailBuiltOtherAdjustBeam(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByOtherBeam(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2OtherBeamDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                if (model.TYPE_STORE == "CAMBER")
                {
                    var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                    {
                        f.ROW_NUM,
                        f.PJ_NAME,
                        f.PD_ITEM,
                        f.CUTTING_PLAN,
                        f.BUILT_NO,
                        f.PC_NAME,
                       f.UG_NAME,
                       f.PA_ACTUAL_DATE
                    });
                    ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                }
                else
                {
                    var Export2 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                    {
                        f.ROW_NUM,
                        f.PJ_NAME,
                        f.PD_ITEM,
                        f.PD_REV,
                        f.PD_CODE,
                        f.PD_DWG,
                        f.BUILT_SIZE,
                        f.PD_BLOCK,
                        f.PLACE,
                        f.BUILT_PLAN,
                        f.PC_NAME,
                        f.UG_NAME,
                        f.PA_PLAN_DATE,
                        f.ACT_USER,
                        f.FULLNAME,
                        f.PA_ACTUAL_DATE,
                        f.BUILT_ACTUAL,
                        f.PA_NO
                    });
                    ws.SelectedRange[3, 1].LoadFromCollection(Export2);
                }
        
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkOtherDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        // Other Box
        #endregion
        public async Task<ActionResult> ExportGetWorkBuiltOtherAdjustBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForWorkBuiltModel();

            serviceResult = await service.GetSummaryForWorkByOtherBox(model); // contact store

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForWorkBuiltModel result = new SummaryWorkBuiltController().MapDetailBuiltSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {2} {3}", model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปผลงาน {3} สัปดาห์ ที่ {0} {4}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, PROCESS, matchRangWeek);
                }

                for (int i = 0; i < result.HeaderPartSummaryForWorkBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForWorkBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForWorkBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }
                var Export1 = result.DetailPartSummaryForWorkBuiltHistories.Select(f => new
                {
                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column 
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,
                });
                var Export2 = serviceResult.Data.Select(f => new
                {
                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                ws.SelectedRange[3, 8].LoadFromCollection(Export2);
                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;
                //   ws.Cells[3, serviceResult.Header.Count + 1, serviceResult.Data.Count  + 3,40].Clear();

                for (int i = result.HeaderPartSummaryForWorkBuilt.Count + 1; i < 50; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWork_AdjustBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        public async Task<ActionResult> ExportGetSalaryBuiltOtherAdjustBox(string MONTH, string YEAR, string WEEK, string DEPT_CODE, string SEARCH, string TYPE_STORE)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = DEPT_CODE;
            model.SEARCH = SEARCH;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new AllSummaryForSalaryBuiltModel();

            serviceResult = await service.GetSummaryForSalaryByOtherBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailPartSummaryForSalaryBuiltModel result = new SummarySalaryBuiltController().MapDetailBuiltBoxSummaryForSalaryModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");

                if (model.WEEK == 0)
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Other Adjust Box  {2}", model.YEAR, model.MONTH, matchRangWeek);
                }

                else
                {
                    ws.Cells[1, 1].Value = string.Format("สรุปเงินเดือน Other Adjust Box สัปดาห์ ที่ {0} {3}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, matchRangWeek);
                }


                for (int i = 0; i < result.HeaderPartSummaryForSalaryBuilt.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderPartSummaryForSalaryBuilt[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderPartSummaryForSalaryBuilt.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                var Export1 = result.DetailPartSummaryForSalaryBuiltHistories.Select(f => new
                {

                    f.EMP_CODE,
                    f.FULL_NAME,
                    f.DEPT_NAME,
                    f.EMP_GOAL, // add new split column
                    f.GOAL_DAY,
                    f.GOAL_REAL,
                    f.GOAL_SUM,

                });
                var Export2 = serviceResult.Data.Select(f => new
                {

                    f.DATA_1,
                    f.DATA_2,
                    f.DATA_3,
                    f.DATA_4,
                    f.DATA_5,
                    f.DATA_6,
                    f.DATA_7,
                    f.DATA_8,
                    f.DATA_9,
                    f.DATA_10,
                    f.DATA_11,
                    f.DATA_12,
                    f.DATA_13,
                    f.DATA_14,
                    f.DATA_15,
                    f.DATA_16,
                    f.DATA_17,
                    f.DATA_18,
                    f.DATA_19,
                    f.DATA_20,
                    f.DATA_21,
                    f.DATA_22,
                    f.DATA_23,
                    f.DATA_24,
                    f.DATA_25,
                    f.DATA_26,
                    f.DATA_27,
                    f.DATA_28,
                    f.DATA_29,
                    f.DATA_30,
                    f.DATA_31,
                    f.DATA_32,
                    f.DATA_33,
                    f.DATA_34,
                    f.DATA_35,
                });
                ws.SelectedRange[3, 1].LoadFromCollection(Export1);

                ws.SelectedRange[3, 8].LoadFromCollection(Export2);

                ws.Cells["G:BA"].Style.Numberformat.Format = "#,##0.00";
                ws.Cells["A1:H1"].Merge = true;

                for (int i = result.HeaderPartSummaryForSalaryBuilt.Count + 1; i < 70; i++)
                    ws.DeleteColumn(i);

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportSalary_AdjustBox" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ExportGetDetailBuiltOtherAdjustBox(string ID, string MONTH, string YEAR, string WEEK, string TYPE, string PROCESS)
        {
            var dateExport = DataConverter.GetDateTime_ddMMyyHHmm(DateTime.Now);
            DateModel model = new DateModel();

            model.MONTH = MONTH;
            model.YEAR = YEAR;
            model.WEEK = DataConverter.GetInteger(WEEK);
            model.DEPT_CODE = ID;

            model.SEARCH = "";
            model.DAY = TYPE;
            model.SORT = "";
            model.SORT_TYPE = 1;
            model.TAKE = 10000;
            model.SKIP = 0;
            model.TYPE_STORE = TYPE;

            var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();

            var serviceResult = new BuiltDetailModel();

            serviceResult = await service.GetDetailSummaryForWorkByOtherBox(model);

            #region RangeDate
            //-----      
            DateModel modelDate = new DateModel();
            var serviceDate = MCSServiceAPIFactory.NewUserDepartmentServiceWrapperInstance();
            var serviceResultDate = new List<DataValueModel>();
            serviceResultDate = await serviceDate.GetHeaderWeek(model);
            string matchRangWeek = "";
            string _WEEKStr = DataConverter.GetInteger(WEEK).ToString();
            if (_WEEKStr == "0")
            {
                matchRangWeek = $"ตั้งแต่วันที่ {GetFirstLastOfMonth(MONTH, YEAR)}";
            }
            else
            {
                matchRangWeek = (from s in serviceResultDate where s.Value == _WEEKStr select s.Text).FirstOrDefault();
                if (!string.IsNullOrEmpty(matchRangWeek))
                {
                    matchRangWeek = $"ตั้งแต่วันที่ {GetDatefirstOfWeek(matchRangWeek, YEAR)}";
                }
                else
                {
                    matchRangWeek = "";
                }
            }
            //-----
            #endregion

            DetailBuiltDetailSummaryForWorkModel result = new SummaryWorkBuiltController().MapDetail2OtherBeamDatailSummaryForWorkModel(serviceResult);

            using (ExcelPackage pck = new ExcelPackage())
            {

                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Sheet1");


                ws.Cells[1, 1].Value = string.Format("{4} รายละเอียด {3} สัปดาห์ ที่ {0} {5}", DataConverter.GetString(model.WEEK), model.YEAR, model.MONTH, TYPE, PROCESS, matchRangWeek);


                for (int i = 0; i < result.HeaderBuiltDetailSummaryForWork.Count; i++)
                {
                    ws.Cells[2, i + 1].Value = result.HeaderBuiltDetailSummaryForWork[i].head;
                }

                using (ExcelRange range = ws.Cells[2, 1, 2, result.HeaderBuiltDetailSummaryForWork.Count])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                }

                if (model.TYPE_STORE == "ADJUST BOX")
                {
                    var Export1 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                    {
                        f.ROW_NUM,
                        f.PJ_NAME,
                        f.PD_ITEM,
                        f.ITEM_NO,
                        f.BUILT_TYPE_NAME,
                        f.CUTTING_PLAN,
                        f.BUILT_NO,
                        f.PLACE,
                        f.BUILT_WEIGHT,
                        f.BUILT_PLAN,
                        f.PC_NAME,
                        f.UG_NAME,
                        f.PA_PLAN_DATE,
                        f.ACT_USER,
                        f.FULLNAME,
                        f.PA_ACTUAL_DATE,
                        f.BUILT_ACTUAL,
                        f.PA_NO
                    });
                    ws.SelectedRange[3, 1].LoadFromCollection(Export1);
                } else
                {
                    var Export2 = result.DetailBuiltDetailSummaryForWorkHistories.Select(f => new
                    {
                        f.ROW_NUM,
                        f.PJ_NAME,
                        f.PD_ITEM,
                        f.PD_REV,
                        f.PD_CODE,
                        f.PD_DWG,
                        f.BUILT_SIZE,
                        f.PD_BLOCK,
                        f.PLACE,
                        f.BUILT_PLAN,
                        f.PC_NAME,
                        f.UG_NAME,
                        f.PA_PLAN_DATE,
                        f.ACT_USER,
                        f.FULLNAME,
                        f.PA_ACTUAL_DATE,
                        f.BUILT_ACTUAL,
                        f.PA_NO
                    });
                    ws.SelectedRange[3, 1].LoadFromCollection(Export2);
                }
            
                ws.Cells["A1:H1"].Merge = true;

                ws.Cells.AutoFitColumns();
                Byte[] fileBytes = pck.GetAsByteArray();
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportWorkOtherDetail" + "_" + dateExport + ".xlsx");
                // Replace filename with your custom Excel-sheet name.

                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                Response.BinaryWrite(fileBytes);
                Response.End();
            }

            return RedirectToAction("Index");
        }
        #endregion
    }
}