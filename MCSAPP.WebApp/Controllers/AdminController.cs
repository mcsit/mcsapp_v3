﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using System.Reflection;
using System.ComponentModel;
using System.IO;

using System.Data;
using System.Data.OleDb;

namespace MCSAPP.WebApp.Controllers
{
    public class AdminController : BaseController
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NCRManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {   
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult NGManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult NGNewManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult GoalUploadManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ActUTManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ActUTNewManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult CutFinisingManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult VTManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult GoalManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult GoalWeldManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult WeekManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult PlusManagment()  
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();

        }
        public ActionResult MinusManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult AdjustGoalHistory()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult CheckVTManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult DesignChangeHistory()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult OtherHistory()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult MinusHistory()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult VTHistory()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult WeekHistory()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult EndTabManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult UploadEndTabManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult CutPartManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult UploadCutPartManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult ProcessTypeManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult CheckSymbolManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult SymbolManagement()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        [HttpPost]
        public void WriteLogError(LogModel model)
        {
            
           
            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
            service.WriteLogError(model);


        }


        [HttpPost]
        public void WriteLogActivity(LogModel model)
        {
         
            var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
            service.WriteLogActivity(model);


        }

        [HttpPost]
        public async Task<ActionResult> SetTxnAdjustGoalData(AdjustGoalModel model)
        {
            UserApplicationModel user = (UserApplicationModel)Session["UserName"];
          
            var username = user.UserModel.EMP_CODE;
            
            if (ModelState.IsValid)
            {         
                model.USER_UPDATE = username;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnAdjustGoalData(model);
                new AdminController().WriteLogActivity(new LogModel() { PAGE_NAME = "Admin", FUNC_NAME = "SetTxnAdjustGoalData", ACTION_MSG = string.Format("EMP_CODE : {0} , CURRENT_GOAL : {1} , NEW_GOAL : {2}",model.EMP_CODE,model.CURRENT_GOAL,model.NEW_GOAL), USER_UPDATE = username});
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";

            new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Admin", FUNC_NAME = "SetTxnAdjustGoalData", ERROR_MSG = "Login : " + username + " Error : " + errorString });

            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetTxnNcrData(List<NcrDataModel> model)
        {
            UserApplicationModel user = (UserApplicationModel)Session["UserName"];

            var username = user.UserModel.EMP_CODE;

            if (ModelState.IsValid)
            {              
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnNcrData(model);

                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            new AdminController().WriteLogError(new LogModel() { PAGE_NAME = "Admin", FUNC_NAME = "SetTxnNcrData", ERROR_MSG = "Login : " + username + " Error : " + errorString });
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetTxnVTData(List<VTDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var username = User.Identity.Name;
               // model.USER_UPDATE = username;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnVTData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetTxnDimensionData(DimDataModel model)
        {
            if (ModelState.IsValid)
            {
                var username = User.Identity.Name;
                model.USER_UPDATE = username;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnDimensionData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetTxnOtherData(OtherDataModel model)
        {
            if (ModelState.IsValid)
            {
                var username = User.Identity.Name;
                model.USER_UPDATE = username;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnOtherData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetTxnMinusData(MinusDataModel model)
        {
            if (ModelState.IsValid)
            {
                var username = User.Identity.Name;
                model.USER_UPDATE = username;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnMinusData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetTxnWeekData(WeekDataModel model)
        {
            if (ModelState.IsValid)
            {
                var username = User.Identity.Name;
                model.USER_UPDATE = username;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetTxnWeekData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetVTDetail(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {
              
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetVTDetail(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetVTDetailLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "VT_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "VT\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile) {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "VT", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count) {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<VTListModel> VTmodel = new List<VTListModel>();
                    VTListModel VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new VTListModel();
                            VT.PROJECT = DataConverter.GetString(dr[0]);
                            VT.ITEM = DataConverter.GetInteger(dr[1]);
                            VT.REV_NO = DataConverter.GetString(dr[2]);
                            VT.CHECK_NO = DataConverter.GetString(dr[3]);
                            VT.PART = DataConverter.GetInteger(dr[4]);
                            VT.POINT = DataConverter.GetString(dr[5]);
                            VT.PROBLEM = DataConverter.GetString(dr[6]);
                            VT.DWG = DataConverter.GetString(dr[7]);
                            VT.ACT = DataConverter.GetString(dr[8]);
                            VT.WRONG = DataConverter.GetString(dr[9]);
                            VT.EDIT = DataConverter.GetString(dr[10]);
                            VT.CHECKER = DataConverter.GetString(dr[11]);
                            if (model.month != DataConverter.GetDate_MMyyyy(Convert.ToDateTime(dr[12].ToString()))){
                                return OK(new ResultModel() { MESSAGE = "ตรวจสอบ CHECK_DATE ไม่ตรงกับเดือน/ปีที่เลือก", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                            }
                            VT.CHECK_DATE = DataConverter.GetDate_yyyMMdd(Convert.ToDateTime(dr[12].ToString()));
                            VT.STATUS = "";
                            VT.TYPE_VT = model.header;
                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetVTDetailLog(VTmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);

                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetVTDetailUpload(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetVTDetailUpload(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelVTDetailLog(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelVTDetailLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> SetNCRDataLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "NCR_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveNCR = PathSave + "NCR\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveNCR)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveNCR));

                    FileStream file = new FileStream(PathSaveNCR + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();
                    string excelConnectionString = string.Empty;

                    try
                    {
                       
                        //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        //connection String for xls file format.
                        if (fileExtension == ".xls")
                        {
                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveNCR + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {

                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveNCR + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                    }
                    catch {
                        return OK(new ResultModel() { MESSAGE = "Error Excel :" + excelConnectionString, CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }

                    try
                    {
                        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        DataTable dt = new DataTable();

                        dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt == null)
                        {
                            return null;
                        }

                        String[] excelSheets = new String[dt.Rows.Count];
                        int t = 0;
                        //excel data saves in temp file here.
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }
                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(ds);
                        }
                        excelConnection.Close();
                        excelConnection.Dispose();
                        excelConnection1.Close();
                        excelConnection1.Dispose();
                    }
                    catch {
                        return OK(new ResultModel() { MESSAGE = "Error Excel2 :" + excelConnectionString, CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    //Create Connection to Excel work book and add oledb namespace


                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveNCR + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveNCR + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "NCR", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<NcrDataModel> NCRmodel = new List<NcrDataModel>();
                    NcrDataModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new NcrDataModel();
                            NCR.GROUP_NAME = DataConverter.GetString(dr[0]);
                            NCR.MAIN = DataConverter.GetDouble(dr[1]);
                            NCR.TEMPO = DataConverter.GetDouble(dr[2]);
                            NCR.WEEK_KEY = model.month;
                            NCR.WEEK_SUB = model.week;
                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetNCRDataLog(NCRmodel);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelNCRDetailLog(List<NcrDataModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelNCRDetailLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetVTDetailAll")]
        public async Task<ActionResult> GetVTDetailAll(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetVTDetailAll(model);
                var result = MapDetailVTModel(serviceResult, false,true);
                return OK(result);
            }
            return OK("");
        }
        public DetailVTModel MapDetailVTModel(List<VTListModel> models, bool chk,bool type)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailVTModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new VTHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    ITEM = DataConverter.GetInteger(item.ITEM),
                    REV_NO = DataConverter.GetString(item.REV_NO),
                    CHECK_NO = DataConverter.GetString(item.CHECK_NO),
                    PART = DataConverter.GetNumberFormat(item.PART, 0),
                    POINT = DataConverter.GetString(item.POINT),
                    PROBLEM = DataConverter.GetString(item.PROBLEM),
                    DWG = DataConverter.GetString(item.DWG) ?? "",
                    ACT = DataConverter.GetString(item.ACT) ?? "",
                    WRONG = DataConverter.GetString(item.WRONG),
                    EDIT = DataConverter.GetString(item.EDIT),
                    CHECKER = DataConverter.GetString(item.CHECKER),
                    CHECK_DATE = DataConverter.GetString(item.CHECK_DATE),
                };
                result.VTHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderVT()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderVT.Add(headschk);
            }
            if (type)
            {

                var headschk = new HeaderVT()
                {
                    head = "ประเภท VT",
                    cansort = false,
                    field = "TYPE_VT",
                    sort = "",
                    style = "vertical-align:initial; text-align:left",
                  
                };
                result.HeaderVT.Add(headschk);

            }
            foreach (VTDetail h in Enum.GetValues(typeof(VTDetail)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {

                    case "ITEM":
                    case "CHECK_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderVT()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderVT.Add(heads);
            }
            if (type)
            {

                var headschk = new HeaderVT()
                {
                    head = "ประเภทบันทึก",
                    cansort = false,
                    field = "TYPE_INSERT",
                    sort = "",
                    style = "vertical-align:initial; text-align:left",

                };
                result.HeaderVT.Add(headschk);

            }
            return result;
        }

        [Route("GetVTDetailLog")]
        public async Task<ActionResult> GetVTDetailLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetVTDetailLog(model);
                var result = MapDetailVTModel(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetNCRLog")]
        public async Task<ActionResult> GetNCRLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetNCRLog(model);
                var result = MapDetailNCRModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailNCRModel MapDetailNCRModel(List<NcrDataModel> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNCRModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailNCRHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    MAIN = DataConverter.GetNumberFormat(item.MAIN, 2),
                    TEMPO = DataConverter.GetNumberFormat(item.TEMPO, 2),

                };
                result.DetailNCRHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderNCR()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderNCR.Add(headschk);
            }
           
            foreach (NCRDetail h in Enum.GetValues(typeof(NCRDetail)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {

                    case "MAIN":
                    case "TEMPO":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left;width:60%";
                            break;
                        }
                }
                var heads = new HeaderNCR()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNCR.Add(heads);
            }
           
            return result;
        }

        [HttpPost]
        [Route("GetReportNCRDetail")]
        public async Task<ActionResult> GetReportNCRDetail(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewAdminServiceWrapperInstance().GetReportNCRDetail(model);
            return OK(result);
        }
        [HttpPost]
        [Route("GetReportNGDetail")]
        public async Task<ActionResult> GetReportNGDetail(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewAdminServiceWrapperInstance().GetReportNGDetail(model);
            return OK(result);
        }
        [HttpPost]
        [Route("GetReportVTDetail")]
        public async Task<ActionResult> GetReportVTDetail(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewAdminServiceWrapperInstance().GetReportVTDetail(model);
            return OK(result);
        }

        [HttpPost]
        [Route("GetSummaryWorkDayForWeekWeldLess")]
        public async Task<ActionResult> GetSummaryWorkDayForWeekLess(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummaryWorkDayForWeekLess(model);
                var result = MapDetailGoalLessAModel(serviceResult, true, true);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetSummaryWorkDayForWeekWeldLess")]
        public async Task<ActionResult> GetSummaryWorkDayForWeekWeldLess(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetSummaryWorkDayForWeekWeldLess(model);
                var result = MapDetailGoalLessAModel(serviceResult, true, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailGoalSumModel MapDetailGoalModel(GoalHeaderModel models, bool detail, bool overgoal)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailGoalSumModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailGoalSumHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    PART = DataConverter.GetNumberFormat(item.PART, 0),
                    OTHER = DataConverter.GetNumberFormat(item.OTHER, 0),
                    REVISE = DataConverter.GetNumberFormat(item.REVISE, 0),
                    WELD = DataConverter.GetNumberFormat(item.WELD, 0),
                    DIMENSION = DataConverter.GetNumberFormat(item.DIMENSION, 0),
                    DEDUCT = DataConverter.GetNumberFormat(item.DEDUCT, 0),
                    OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0)
                };
                result.DetailGoalSumHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                    var heads = new HeaderGoalSum()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderGoalSum.Add(heads);
                }
            }
            foreach (var head in models.Header)
            {

                var heads = new HeaderGoalSum()
                {
                    head = head.Value,
                    fieldType = "linkEmpProcess",
                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = "vertical-align:initial; text-align:right",
                    functionCall = "Show"
                };
                result.HeaderGoalSum.Add(heads);
            }
            if (overgoal)
            {
                var heads = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ขาด)",
                    field = "OVER_GOAL",
                    cansort = true,
                    sort = "OVER_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(heads);

            }
            var headsCol = new HeaderGoalSum()
            {
                head = "GOAL",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "showGoal",
                field = "EMP_CODE",
                cansort = false,
                sort = string.Empty,
                style = "width:30px;vertical-align:initial; text-align:center",
                functionCall = "ShowGoal",
            };
            result.HeaderGoalSum.Add(headsCol);
            return result;
        }
        public DetailGoalSumModel MapDetailGoalLessAModel(GoalHeaderModel models, bool detail, bool overgoal)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailGoalSumModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailGoalSumHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    //PART = DataConverter.GetNumberFormat(item.PART, 0),
                    //OTHER = DataConverter.GetNumberFormat(item.OTHER, 0),
                    //REVISE = DataConverter.GetNumberFormat(item.REVISE, 0),
                    //WELD = DataConverter.GetNumberFormat(item.WELD, 0),
                    //DIMENSION = DataConverter.GetNumberFormat(item.DIMENSION, 0),
                    //DEDUCT = DataConverter.GetNumberFormat(item.DEDUCT, 0),
                    OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0)
                };
                result.DetailGoalSumHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                    var heads = new HeaderGoalSum()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderGoalSum.Add(heads);
                }
            }
            //foreach (var head in models.Header)
            //{

            //    var heads = new HeaderGoalSum()
            //    {
            //        head = head.Value,
            //        fieldType = "linkEmpProcess",
            //        field = head.Value,
            //        cansort = true,
            //        sort = head.Value,
            //        style = "vertical-align:initial; text-align:right",
            //        functionCall = "Show"
            //    };
            //    result.HeaderGoalSum.Add(heads);
            //}
            if (overgoal)
            {
                var heads = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ขาด)",
                    field = "OVER_GOAL",
                    cansort = true,
                    sort = "OVER_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(heads);

            }
            var headsCol = new HeaderGoalSum()
            {
                head = "GOAL",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "showGoal",
                field = "EMP_CODE",
                cansort = false,
                sort = string.Empty,
                style = "width:30px;vertical-align:initial; text-align:center",
                functionCall = "ShowGoal",
            };
            result.HeaderGoalSum.Add(headsCol);
            return result;
        }
        [HttpPost]
        [Route("GetWorkMonth")]
        public async Task<ActionResult> GetWorkMonth(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWeekServiceWrapperInstance();
                var serviceResult = await service.GetWorkMonth(model);
                var result = MapDetailModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailWorkModel MapDetailModel(List<WorkModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new DetailWorkHistory()
                {
                    PROJECT_NAME = item.PROJECT_NAME,
                    ITEM = item.ITEM,
                    ACTUAL = DataConverter.GetNumberFormat(item.ACTUAL, 0),
                    ACTUAL_DATE = item.ACTUAL_DATE,
                    //EMP_GOAL = item.EMP_GOAL > 0 ? DataConverter.GetNumberFormat(item.EMP_GOAL, 2) : "0.00",
                    //LEVEL_SALARY = item.LEVEL_SALARY > 0 ? DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2) : "0.00",
                    //LEVEL_QUALITY = item.LEVEL_QUALITY > 0 ? DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2) : "0.00",
                    //LEVEL_PROCEDURE = item.LEVEL_PROCEDURE > 0 ? DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2) : "0.00",
                    //SALARY = item.SALARY > 0 ? DataConverter.GetNumberFormat(item.SALARY, 2) : "0.00",
                    //QUALITY = item.QUALITY > 0 ? DataConverter.GetNumberFormat(item.QUALITY, 2) : "0.00",
                    //PROCEDURE = item.PROCEDURE > 0 ? DataConverter.GetNumberFormat(item.PROCEDURE, 2) : "0.00",
                    //SUM_SALARY = item.SUM_SALARY > 0 ? DataConverter.GetNumberFormat(item.SUM_SALARY, 2) : "0.00",
                };
                result.DetailWorkHistories.Add(items);
            }
            foreach (Project h in Enum.GetValues(typeof(Project)))
            {

                var heads = new HeaderWork()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = false,
                    sort = DataConverter.GetString(h),
                    style = (GetEnumDescription(h).Contains("โปรเจค") ? "vertical-align:initial;text-align:left" : "vertical-align:initial;text-align:center"),
                };
                result.HeaderWork.Add(heads);
            }
            foreach (Product h in Enum.GetValues(typeof(Product)))
            {

                var heads = new HeaderWork()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = false,
                    sort = DataConverter.GetString(h),
                    style = (GetEnumDescription(h).Contains("จำนวน") ? "vertical-align:initial;text-align:right;width:150px;" : "vertical-align:initial;text-align:center;width:200px;"),
                };
                result.HeaderWork.Add(heads);
            }

            return result;
        }

        [HttpPost]
        [Route("GetWeekLessForAdjust")]
        public async Task<ActionResult> GetWeekLessForAdjust(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewAdminServiceWrapperInstance().GetWeekLessForAdjust(model);
            return OK(result);
        }

        [HttpPost]
        [Route("GetAdjustGoalData")]
        public async Task<ActionResult> GetAdjustGoalData(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetAdjustGoalData(model);
                var result = MapDetailAdjustGoalModel(serviceResult,true);
                return OK(result);
            }
            return OK("");
        }
        public DetailAdjustGoalModel MapDetailAdjustGoalModel(List<GoalSumModel> models, bool detail)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailAdjustGoalModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new AdjustGoalHistory()
                {
                    ID = DataConverter.GetString(item.ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    WEEK_KEY = item.WEEK_KEY,

                    SUM_PART = DataConverter.GetNumberFormat(item.SUM_PART, 0),
                    ADJUST_GOAL = DataConverter.GetNumberFormat(item.ADJUST_GOAL, 0),
                    CURRENT_GOAL = DataConverter.GetNumberFormat(item.CURRENT_GOAL, 0),
                    NEW_GOAL = DataConverter.GetNumberFormat(item.NEW_GOAL, 0),
                    WEEK_NO = DataConverter.GetString(item.WEEK_NO) == "0" ? "-" : DataConverter.GetString(item.WEEK_NO),

                };
                result.AdjustGoalHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmp h in Enum.GetValues(typeof(DetailEmp)))
                {
                    var heads = new HeaderAdjustGoal()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderAdjustGoal.Add(heads);
                }
            }
            foreach (AdjustGola h in Enum.GetValues(typeof(AdjustGola)))
            {
                var heads = new HeaderAdjustGoal()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderAdjustGoal.Add(heads);
            }
            var headsCol = new HeaderAdjustGoal()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderAdjustGoal.Add(headsCol);
            return result;
        }

        //[HttpPost]
        //[Route("GetReportMinusData")]
        //public async Task<ActionResult> GetReportMinusData(ReportSearchModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
        //        var serviceResult = await service.GetReportMinusData(model);
        //        var result = MapDetailMinuslModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public DetailMinusModel MapDetailMinuslModel(List<MinusDataModel> models)
        //{
        //    var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
        //    var result = new DetailMinusModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models)
        //    {

        //        var items = new MinusHistory()
        //        {
        //            EMP_CODE = item.EMP_CODE,
        //            FULL_NAME = item.FULL_NAME,
        //            DEPT_NAME = item.DEPT_NAME,


        //            PRICE = DataConverter.GetNumberFormat(item.PRICE, 0),
        //            ACTUAL_DATE = item.ACTUAL_DATE,
        //            REMARK = item.REMARK,
                    

        //        };
        //        result.MinusHistories.Add(items);
        //    }
            
        //        foreach (DetailEmp h in Enum.GetValues(typeof(DetailEmp)))
        //        {
        //            var heads = new HeaderMinus()
        //            {
        //                head = GetEnumDescription(h),
        //                field = DataConverter.GetString(h),
        //                cansort = false,
        //                sort = DataConverter.GetString(h),
        //                style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
        //            };
        //            result.HeaderMinus.Add(heads);
        //        }
           
        //    return result;
        //}

        [HttpPost]
        [Route("GetCalendar")]
        public async Task<ActionResult> GetCalendar(DateModel model)
        {
            var result = await MCSServiceAPIFactory.NewAdminServiceWrapperInstance().GetCalendar(model);
            return OK(result);
        }

        [HttpPost]
        public async Task<ActionResult> SetDesingLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "DESING_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "DESING_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "DESING", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<DesingModel> VTmodel = new List<DesingModel>();
                    DesingModel VT = null;
                    UserApplicationModel user =  (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new DesingModel();
                            VT.GROUP_NAME = DataConverter.GetString(dr[0]);
                            VT.NUMBER_DESING = DataConverter.GetString(dr[1]);
                            VT.PROJECT = DataConverter.GetString(dr[2]);
                            VT.ITEM = DataConverter.GetInteger(dr[3]);
                            VT.DATE = DataConverter.GetDate_yyyMMdd(Convert.ToDateTime(dr[4].ToString()));
                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetDesingLog(VTmodel);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetDesing(List<DesingModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetDesing(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelDesingLog(List<DesingModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelDesingLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetDesingLog")]
        public async Task<ActionResult> GetDesingLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;
                

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetDesingLog(model);
                var result = MapDetailDesignModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailDesingModel MapDetailDesignModel(List<DesingModel> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailDesingModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DesingHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    ITEM = DataConverter.GetInteger(item.ITEM),
                    DATE = DataConverter.GetString(item.DATE),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    NUMBER_DESING = DataConverter.GetString(item.NUMBER_DESING),

                };
                result.DesingHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderDesing()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderDesing.Add(headschk);
            }
         
            foreach (Desing h in Enum.GetValues(typeof(Desing)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {

                    case "ITEM":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderDesing()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderDesing.Add(heads);
            }
           
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadOtherLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "OTHER_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "OTHER_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "OTHER", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<OtherModel> VTmodel = new List<OtherModel>();
                    OtherModel VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new OtherModel();
                            VT.EMP_CODE = DataConverter.GetString(dr[0]);
                            VT.PROCESS = DataConverter.GetString(dr[1]);
                            VT.PRICE = DataConverter.GetDouble(dr[2]);
                            VT.ACTUAL = DataConverter.GetInteger(dr[3]);
                            VT.ACTUAL_DATE = DataConverter.GetDate_yyyMMdd(Convert.ToDateTime(dr[4].ToString()));
                            VT.TYPE_LOAD = model.header;
                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadOtherLog(VTmodel);
                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadOther(List<OtherModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadOther(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadOtherLog(List<OtherModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadOtherLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadOtherLog")]
        public async Task<ActionResult> GetUploadOtherLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadOtherLog(model);
                var result = MapDetailOtherModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailOtherModel MapDetailOtherModel(List<OtherModel> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailOtherModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new OtherHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    PROCESS_NAME = DataConverter.GetString(item.PROCESS),
                    PRICE = DataConverter.GetString(item.PRICE),
                    ACTUAL = DataConverter.GetString(item.ACTUAL),
                    ACTUAL_DATE= DataConverter.GetString(item.ACTUAL_DATE),
                    TYPE_LOAD = DataConverter.GetString(item.TYPE_LOAD),
                };
                result.OtherHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderOther()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderOther.Add(headschk);
            }

            foreach (UploadOther h in Enum.GetValues(typeof(UploadOther)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ACTUAL":
                    case "ACTUAL_DATE":
                    case "PRICE":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderOther()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderOther.Add(heads);
            }

            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadWeekLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "WEEK_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "WEEK_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "WEEK", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<UploadWeekModel> VTmodel = new List<UploadWeekModel>();
                    UploadWeekModel VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new UploadWeekModel();
                            VT.WEEK_KEY = model.month;
                            VT.WEEK_NO = DataConverter.GetString(dr[0]);
                            VT.WEEK_DATE = DataConverter.GetDate_yyyMMdd(Convert.ToDateTime(dr[1].ToString()));

                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadWeekLog(VTmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);

                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadWeek(List<UploadWeekModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadWeek(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadWeekLog(List<UploadWeekModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model) {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadWeekLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadOtherLog")]
        public async Task<ActionResult> GetUploadWeekLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadWeekLog(model);
                var result = MapDetailUploadWeekrModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailUploadWeekModel MapDetailUploadWeekrModel(List<UploadWeekModel> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUploadWeekModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new UploadWeekHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_DATE = DataConverter.GetString(item.WEEK_DATE),
                    WEEK_NO = DataConverter.GetString(item.WEEK_NO),
                  
                };
                result.UploadWeekHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderUploadWeek()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderUploadWeek.Add(headschk);
            }

            foreach (UploadWeek h in Enum.GetValues(typeof(UploadWeek)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {

                    //case "DATE":

                    //    {
                    //        sty = "vertical-align:initial; text-align:right";
                    //        break;
                    //    }
                    case "WEEK_NO":
                    case "DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUploadWeek()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUploadWeek.Add(heads);
            }

            return result;
        }


        [HttpPost]
        [Route("GetReportDesingChangeData")]
        public async Task<ActionResult> GetReportDesingChangeData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
           
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportDesingChangeData(model);
                var result = MapDetailDesingModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailDesingModel MapDetailDesingModel(List<ReportDesignChangeModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailDesingModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DesingHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    ID = DataConverter.GetString(item.ID),
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    ITEM = DataConverter.GetInteger(item.ITEM),
                    NUMBER_DESING = DataConverter.GetString(item.DWG),
                    GROUP_NAME = DataConverter.GetString(item.WRONG),
                    DATE = DataConverter.GetString(item.CHECK_DATE),

                };
                result.DesingHistories.Add(items);
            }

            foreach (Desing h in Enum.GetValues(typeof(Desing)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "WEEK_NO":
                    case "DATE":
                    case "ITEM":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                
                var heads = new HeaderDesing()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderDesing.Add(heads);
            }
            var headsCol = new HeaderDesing()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderDesing.Add(headsCol);
            return result;
        }

        [HttpPost]
        [Route("GetReportDimensionData")]
        public async Task<ActionResult> GetReportDimensionData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportDimensionData(model);
                var result = MapDetailDimensionModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetReportOtherData")]
        public async Task<ActionResult> GetReportOtherData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportOtherData(model);
                var result = MapDetailDimensionModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        [HttpPost]
        [Route("GetReportPlusData")]
        public async Task<ActionResult> GetReportPlusData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportPlusData(model);
                var result = MapDetailDimensionModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailOtherModel MapDetailDimensionModel(List<ReportDimensionModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailOtherModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new OtherHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    ID = DataConverter.GetString(item.ID),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    PROCESS_NAME = DataConverter.GetString(item.PROCESS),
                    ACTUAL = DataConverter.GetString(item.ACTUAL),
                    PRICE = DataConverter.GetString(item.PRICE),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),
                };
                result.OtherHistories.Add(items);
            }

            foreach (AdminOther h in Enum.GetValues(typeof(AdminOther)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "PRICE":
                    case "ACTUAL":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "ACTUAL_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderOther()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderOther.Add(heads);
              
            }
            var headsCol = new HeaderOther()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderOther.Add(headsCol);

            return result;
        }

        [HttpPost]
        [Route("GetReportMinusData")]
        public async Task<ActionResult> GetReportMinusData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportMinusData(model);
                var result = MapDetailMinusModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailMinusModel MapDetailMinusModel(List<ReportMinusModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailMinusModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new MinusHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    ID = DataConverter.GetString(item.ID),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    REMARK = DataConverter.GetString(item.REMARK),
                    PRICE = DataConverter.GetString(item.PRICE),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),
                };
                result.MinusHistories.Add(items);
            }

            foreach (AdminMinus h in Enum.GetValues(typeof(AdminMinus)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "PRICE":
                    case "ACTUAL":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "ACTUAL_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderMinus()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderMinus.Add(heads);
            }
            var headsCol = new HeaderMinus()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderMinus.Add(headsCol);
            return result;
        }

        [HttpPost]
        [Route("GetReportVTData")]
        public async Task<ActionResult> GetReportVTData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportVTData(model);
                var result = MapDetailVTModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailVTModel MapDetailVTModel(List<VTListModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailVTModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new VTHistory()
                {
                    ID = DataConverter.GetString(item.ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    ITEM = DataConverter.GetInteger(item.ITEM),
                    REV_NO = DataConverter.GetString(item.REV_NO),
                    CHECK_NO = DataConverter.GetString(item.CHECK_NO),
                    PART = DataConverter.GetNumberFormat(item.PART, 0),
                    POINT = DataConverter.GetString(item.POINT),
                    PROBLEM = DataConverter.GetString(item.PROBLEM),
                    DWG = DataConverter.GetString(item.DWG) ?? "",
                    ACT = DataConverter.GetString(item.ACT) ?? "",
                    WRONG = DataConverter.GetString(item.WRONG),
                    EDIT = DataConverter.GetString(item.EDIT),
                    CHECKER = DataConverter.GetString(item.CHECKER),
                    CHECK_DATE = DataConverter.GetString(item.CHECK_DATE),
                };
                result.VTHistories.Add(items);
            }

            foreach (VTDetail h in Enum.GetValues(typeof(VTDetail)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {

                    case "ITEM":
                    case "CHECK_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderVT()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderVT.Add(heads);
            }
            var headsCol = new HeaderVT()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderVT.Add(headsCol);
            return result;
        }

        [HttpPost]
        [Route("GetReportWeekData")]
        public async Task<ActionResult> GetReportWeekData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetReportWeekData(model);
                var result = MapDetailWeekModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailUploadWeekModel MapDetailWeekModel(List<UploadWeekModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUploadWeekModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new UploadWeekHistory()
                {
                  
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_YEAR = DataConverter.GetString(item.WEEK_YEAR),
                    WEEK_MONTH = DataConverter.GetString(item.WEEK_MONTH),
                };
                result.UploadWeekHistories.Add(items);
            }

            foreach (AdminWeek h in Enum.GetValues(typeof(AdminWeek)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {

                    case "ITEM":
                    case "CHECK_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUploadWeek()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUploadWeek.Add(heads);
            }
            var headsCol = new HeaderUploadWeek()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "WEEK_KEY",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderUploadWeek.Add(headsCol);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> DeleteAdjustGoal(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
               
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteAdjustGoal(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteDesingChange(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteDesingChange(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteDimension(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteDimension(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMinus(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteMinus(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteOther(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteOther(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeletePlus(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeletePlus(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteVTData(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteVTData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteWeek(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteWeek(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));

            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        public async Task<ActionResult> SetUploadNGLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "NG_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "NG\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "NG", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<NGDataModel> NCRmodel = new List<NGDataModel>();
                    NGDataModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new NGDataModel();
                            NCR.GROUP_NAME = DataConverter.GetString(dr[0]);
                            NCR.NG = DataConverter.GetDouble(dr[1]);
                            
                            NCR.WEEK_KEY = model.month;
                            NCR.WEEK_SUB = model.week;
                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadNGLog(NCRmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);

                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadNG(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadNG(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadNGLog(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadNGLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> DelUploadNG(NGDataModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadNG(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadNGLog")]
        public async Task<ActionResult> GetUploadNGLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadNGLog(model);
                var result = MapDetailNGModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailNGModel MapDetailNGModel(List<NGDataModel> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNGModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailNGHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    NG = DataConverter.GetNumberFormat(item.NG, 2),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),

                };
                result.DetailNGHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderNG()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderNG.Add(headschk);
            }

            foreach (UploadNG h in Enum.GetValues(typeof(UploadNG)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "GROUP_NAME":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderNG()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNG.Add(heads);
            }

            return result;
        }
        [HttpPost]
        [Route("GetUploadNG")]
        public async Task<ActionResult> GetUploadNG(DateModel model)
        {
            if (ModelState.IsValid)
            {
       
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadNG(model);
                var result = MapDetailNGEditModel(serviceResult, false,model.EDIT);
                return OK(result);
            }
            return OK("");
        }
        public DetailNGModel MapDetailNGEditModel(List<NGDataModel> models, bool chk,bool Edit)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNGModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailNGHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    EMP_CODE = DataConverter.GetString(item.GROUP_NAME),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    WEEK_YEAR = DataConverter.GetString(item.WEEK_YEAR),
                    WEEK_NAME=  DataConverter.GetString(item.WEEK_NAME),
                    NG = DataConverter.GetNumberFormat(item.NG, 2),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                };
                result.DetailNGHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderNG()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderNG.Add(headschk);
            }

            foreach (UploadReportNG h in Enum.GetValues(typeof(UploadReportNG)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "EMP_CODE":
                    case "WEEK_YEAR":
                    case "WEEK_NAME":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    case "NG":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderNG()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNG.Add(heads);
            }
            if (Edit)
            {
                var headsEditCol = new HeaderNG()
                {
                    head = "UPDATE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "showNG",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowNG",
                };
                result.HeaderNG.Add(headsEditCol);

                var headsCol = new HeaderNG()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderNG.Add(headsCol);
            }
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> UpdateUploadNG(NGDataModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.UpdateUploadNG(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadActUTLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "ACT_UT_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "ACT_UT\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "ACT_UT", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<UploadActUTModel> NCRmodel = new List<UploadActUTModel>();
                    UploadActUTModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new UploadActUTModel();
                        
                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);

                            NCR.WEEK_KEY = model.month;
                        
                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadActUTLog(NCRmodel);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadActUT(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadActUT(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadActUTLog(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadActUTLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> DelUploadActUT(UploadActUTModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadActUT(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadActUTLog")]
        public async Task<ActionResult> GetUploadActUTLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadActUTLog(model);
                var result = MapDetailActUTModel(serviceResult, true,false);
                return OK(result);
            }
            return OK("");
        }
        public DetailActUTModel MapDetailActUTModel(List<UploadActUTModel> models, bool chk,bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailActUTModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailActUTHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),

                };
                result.DetailActUTHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderActUT()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderActUT.Add(headschk);
            }

            foreach (UploadActUT h in Enum.GetValues(typeof(UploadActUT)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "GROUP_NAME":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderActUT()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderActUT.Add(heads);
            }
            if (upload) {
                var headsCol = new HeaderActUT()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderActUT.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadActUT")]
        public async Task<ActionResult> GetUploadActUT(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadActUT(model);
                var result = MapDetailActUTModel(serviceResult, false,true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadCutFinisingLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "CUT_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "CUT\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "CUT", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<UploadCutFinisingModel> NCRmodel = new List<UploadCutFinisingModel>();
                    UploadCutFinisingModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new UploadCutFinisingModel();

                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);

                            NCR.WEEK_KEY = model.month;
                            NCR.AMOUNT = DataConverter.GetDouble(dr[1]);
                            NCR.EMP_CODE_RECIVE = DataConverter.GetString(dr[2]);
                            NCR.WEEK_SUB = model.week;

                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadCutFinisingLog(NCRmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadCutFinising(List<UploadCutFinisingModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadCutFinising(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadCutFinisingLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> DelUploadCutFinising(UploadCutFinisingModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadCutFinising(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadCutFinisingLog")]
        public async Task<ActionResult> GetUploadCutFinisingLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadCutFinisingLog(model);
                var result = MapDetailCutFinisingModel(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }
        public DetailCutFinisingModel MapDetailCutFinisingModel(List<UploadCutFinisingModel> models, bool chk, bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailCutFinisingModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailCutFinisingHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_NAME = DataConverter.GetString(item.WEEK_NAME),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    EMP_CODE_RECIVE= DataConverter.GetString(item.EMP_CODE_RECIVE),
                    FULL_NAME_RECIVE = DataConverter.GetString(item.FULL_NAME_RECIVE),
                    DEPT_NAME_RECIVE = DataConverter.GetString(item.DEPT_NAME_RECIVE),
                    AMOUNT = DataConverter.GetNumberFormat(item.AMOUNT,2),
                };
                result.DetailCutFinisingHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderCutFinising()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderCutFinising.Add(headschk);
            }

            foreach (UploadCutFinising h in Enum.GetValues(typeof(UploadCutFinising)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "AMOUNT":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderCutFinising()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderCutFinising.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderCutFinising()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderCutFinising.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadCutFinising")]
        public async Task<ActionResult> GetUploadCutFinising(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadCutFinising(model);
                var result = MapDetailCutFinisingModel(serviceResult, false, true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadGoalLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "GOAL_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "GOAL\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "GOAL", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<UploadGoalModel> NCRmodel = new List<UploadGoalModel>();
                    UploadGoalModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new UploadGoalModel();

                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);

                            NCR.WEEK_KEY = model.month;
                            NCR.LEVEL_NAME = DataConverter.GetString(dr[1]);
                          


                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadGoalLog(NCRmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadGoal(List<UploadGoalModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadGoal(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadGoalLog(List<UploadGoalModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadGoalLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> DelUploadGoal(UploadGoalModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadGoal(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadGoalLog")]
        public async Task<ActionResult> GetUploadGoalLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadGoalLog(model);
                var result = MapDetailGoalModel(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }
        public DetailUploadGoalModel MapDetailGoalModel(List<UploadGoalModel> models, bool chk, bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUploadGoalModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailUploadGoalHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    LEVEL_NAME = DataConverter.GetString(item.LEVEL_NAME),
                    LEVEL_GOAL = DataConverter.GetString(item.LEVEL_GOAL),
                };
                result.DetailUploadGoalHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderUploadGoal()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderUploadGoal.Add(headschk);
            }

            foreach (UploadGoal h in Enum.GetValues(typeof(UploadGoal)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "LEVEL_GOAL":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUploadGoal()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUploadGoal.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderUploadGoal()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderUploadGoal.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadGoal")]
        public async Task<ActionResult> GetUploadGoal(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadGoal(model);
                var result = MapDetailGoalModel(serviceResult, false, true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadNGNewLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "NG_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "NG\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "NG_NEW", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<NGDataModel> NCRmodel = new List<NGDataModel>();
                    NGDataModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new NGDataModel();
                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);
                            NCR.TOTAL_JOINT = DataConverter.GetDouble(dr[1]);
                            NCR.JOINT_NG = DataConverter.GetDouble(dr[2]);
                            NCR.PERCENT_NG = DataConverter.GetDouble(dr[3]);
                            NCR.GROUP_NAME = DataConverter.GetString(dr[4]);
                            NCR.WEEK_KEY = model.month;
                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadNGNewLog(NCRmodel);
                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadNGNew(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadNGNew(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadNGNewLog(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadNGNewLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> DelUploadNGNew(NGDataModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadNGNew(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadNGNewLog")]
        public async Task<ActionResult> GetUploadNGNewLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadNGNewLog(model);
                var result = MapDetailNGNewModel(serviceResult, true,false);
                return OK(result);
            }
            return OK("");
        }
        public DetailNGNewModel MapDetailNGNewModel(List<NGDataModel> models, bool chk,bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNGNewModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailNGNewHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    TOTAL_JOINT = DataConverter.GetNumberFormat(item.TOTAL_JOINT, 2),
                    JOINT_NG = DataConverter.GetNumberFormat(item.JOINT_NG, 2),
                    PERCENT_NG = DataConverter.GetNumberFormat(item.PERCENT_NG, 2),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                };
                result.DetailNGNewHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderNGNew()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderNGNew.Add(headschk);
            }

            foreach (UploadNGNew h in Enum.GetValues(typeof(UploadNGNew)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "GROUP_NAME":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderNGNew()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNGNew.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderNGNew()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderNGNew.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadNGNew")]
        public async Task<ActionResult> GetUploadNGNew(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadNGNew(model);
                var result = MapDetailNGNewModel(serviceResult, false,true);
                return OK(result);
            }
            return OK("");
        }
        public DetailNGNewModel MapDetailNGNewEditModel(List<NGDataModel> models, bool chk, bool Edit)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNGNewModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailNGNewHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_NAME = DataConverter.GetString(item.WEEK_NAME),
                    TOTAL_JOINT = DataConverter.GetNumberFormat(item.TOTAL_JOINT, 2),
                    JOINT_NG = DataConverter.GetNumberFormat(item.JOINT_NG, 2),
                    PERCENT_NG = DataConverter.GetNumberFormat(item.PERCENT_NG, 2),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                };
                result.DetailNGNewHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderNGNew()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderNGNew.Add(headschk);
            }

            foreach (UploadNGNew h in Enum.GetValues(typeof(UploadNGNew)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "EMP_CODE":
                    case "WEEK_YEAR":
                    case "WEEK_NAME":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    case "NG":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderNGNew()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNGNew.Add(heads);
            }
            if (Edit)
            {
                var headsEditCol = new HeaderNGNew()
                {
                    head = "UPDATE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "showNG",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowNG",
                };
                result.HeaderNGNew.Add(headsEditCol);

                var headsCol = new HeaderNGNew()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderNGNew.Add(headsCol);
            }
            return result;
        }

       
        [HttpPost]
        public async Task<ActionResult> SetUploadActUTNewLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "ACT_UT_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "ACT_UT\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "ACT_UT_NEW", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<UploadActUTModel> NCRmodel = new List<UploadActUTModel>();
                    UploadActUTModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new UploadActUTModel();

                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);
                            NCR.NG = DataConverter.GetDouble(dr[1]);
                            NCR.WEEK_KEY = model.month;

                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadActUTNewLog(NCRmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadActUTNew(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadActUTNew(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadActUTNewLog(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadActUTNewLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        [HttpPost]
        public async Task<ActionResult> DelUploadActUTNew(UploadActUTModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadActUTNew(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadActUTNewLog")]
        public async Task<ActionResult> GetUploadActUTNewLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadActUTNewLog(model);
                var result = MapDetailActUTNewModel(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }
        public DetailActUTNewModel MapDetailActUTNewModel(List<UploadActUTModel> models, bool chk, bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailActUTNewModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailActUTNewHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    NG = DataConverter.GetNumberFormat(item.NG, 2),

                };
                result.DetailActUTNewHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderActUTNew()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderActUTNew.Add(headschk);
            }

            foreach (UploadActUTNew h in Enum.GetValues(typeof(UploadActUTNew)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "GROUP_NAME":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderActUTNew()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderActUTNew.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderActUTNew()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderActUTNew.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadActUTNew")]
        public async Task<ActionResult> GetUploadActUTNew(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadActUTNew(model);
                var result = MapDetailActUTNewModel(serviceResult, false, true);
                return OK(result);
            }
            return OK("");
        }


        [HttpPost]
        public async Task<ActionResult> SetUploadEndTabLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "END_TAB_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "END_TAB\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "END_TAB", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<UploadEndTabModel> NCRmodel = new List<UploadEndTabModel>();
                    UploadEndTabModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new UploadEndTabModel();

                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);
                            NCR.PRICE = DataConverter.GetDouble(dr[1]);
                            NCR.WEEK_KEY = model.month;

                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadEndTabLog(NCRmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadEndTab(List<UploadEndTabModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadEndTab(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadEndTabLog(List<UploadEndTabModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadEndTabLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadEndTab(UploadEndTabModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadEndTab(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadEndTabLog")]
        public async Task<ActionResult> GetUploadEndTabLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadEndTabLog(model);
                var result = MapDetailEndTabModel(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }
        public DetailEndTabModel MapDetailEndTabModel(List<UploadEndTabModel> models, bool chk, bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailEndTabModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailEndTabHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    PRICE = DataConverter.GetNumberFormat(item.PRICE, 2),

                };
                result.DetailEndTabHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderEndTab()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderEndTab.Add(headschk);
            }

            foreach (UploadEndTab h in Enum.GetValues(typeof(UploadEndTab)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "GROUP_NAME":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderEndTab()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderEndTab.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderEndTab()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderEndTab.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadEndTab")]
        public async Task<ActionResult> GetUploadEndTab(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadEndTab(model);
                var result = MapDetailEndTabModel(serviceResult, false, true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadCutPartLog(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "CUT_PART_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "CUT_PART\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "CUT_PART", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }


                    List<UploadCutPartModel> NCRmodel = new List<UploadCutPartModel>();
                    UploadCutPartModel NCR = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            NCR = new UploadCutPartModel();

                            NCR.EMP_CODE = DataConverter.GetString(dr[0]);

                            NCR.WEEK_KEY = model.month;
                            NCR.AMOUNT = DataConverter.GetDouble(dr[1]);
                            NCR.EMP_CODE_RECIVE = DataConverter.GetString(dr[2]);
                            NCR.WEEK_SUB = model.week;

                            NCR.USER_UPDATE = user.UserModel.EMP_CODE;
                            NCRmodel.Add(NCR);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadCutPartLog(NCRmodel);

                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);


                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadCutPart(List<UploadCutPartModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadCutPart(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadCutPartLog(List<UploadCutPartModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadCutPartLog(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadCutPart(UploadCutPartModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadCutPart(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetUploadCutPartLog")]
        public async Task<ActionResult> GetUploadCutPartLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadCutPartLog(model);
                var result = MapDetailCutPartModel(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }

        public DetailCutPartModel MapDetailCutPartModel(List<UploadCutPartModel> models, bool chk, bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailCutPartModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailCutPartHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_NAME = DataConverter.GetString(item.WEEK_NAME),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    EMP_CODE_RECIVE = DataConverter.GetString(item.EMP_CODE_RECIVE),
                    FULL_NAME_RECIVE = DataConverter.GetString(item.FULL_NAME_RECIVE),
                    DEPT_NAME_RECIVE = DataConverter.GetString(item.DEPT_NAME_RECIVE),
                    AMOUNT = DataConverter.GetNumberFormat(item.AMOUNT, 2),
                };
                result.DetailCutPartHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderCutPart()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderCutPart.Add(headschk);
            }

            foreach (UploadCutPart h in Enum.GetValues(typeof(UploadCutPart)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "AMOUNT":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderCutPart()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderCutPart.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderCutPart()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderCutPart.Add(headsCol);
            }
            return result;
        }
        [HttpPost]
        [Route("GetUploadCutPart")]
        public async Task<ActionResult> GetUploadCutPart(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadCutPart(model);
                var result = MapDetailCutPartModel(serviceResult, false, true);
                return OK(result);
            }
            return OK("");
        }


        //[HttpPost]
        //[Route("GetMasterProcessData")]
        //public async Task<ActionResult> GetMasterProcessData(SearchModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //var targetUser = ApplicationUserProfile();


        //        var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
        //        var serviceResult = await service.GetMasterProcess(model);
        //        var result = MapMasterProcessDataModel(serviceResult);
        //        return OK(result);
        //    }
        //    return OK("");
        //}
        //public StructureProcessModel MapMasterProcessDataModel(List<ProcessModel> models)
        //{
        //    var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
        //    var result = new StructureProcessModel()
        //    {
        //        RowTotal = totalRecord,
        //        PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
        //    };
        //    foreach (var item in models)
        //    {
        //        var items = new ProcessHistory()
        //        {
        //            PROCESS_ID = item.PROCESS_ID,
        //            PROCESS_NAME = item.PROCESS_NAME,
        //            PRICE_UNIT = item.PRICE_UNIT,
        //            MAIN_PROCESS_NAME = item.MAIN_PROCESS_NAME,

        //        };
        //        result.ProcessHistories.Add(items);
        //    }

        //    foreach (Process h in Enum.GetValues(typeof(Process)))
        //    {

        //        var heads = new HeaderProcess()
        //        {
        //            head = GetEnumDescription(h),
        //            field = DataConverter.GetString(h),
        //            cansort = true,
        //            sort = DataConverter.GetString(h),
        //            style = DataConverter.GetString(h).Contains("PRICE") ? "vertical-align:initial;text-align:right" : "",
        //        };
        //        result.HeaderProcess.Add(heads);


        //    }
        //    var headsCol = new HeaderProcess()
        //    {
        //        head = "DELETE",
        //        //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
        //        fieldType = "delete",
        //        field = "PROCESS_ID",
        //        cansort = false,
        //        sort = string.Empty,
        //        style = "vertical-align:initial;text-align:center",
        //        functionCall = "ShowDelete",
        //    };
        //    result.HeaderProcess.Add(headsCol);
        //    return result;
        //}

        [HttpPost]
        public async Task<ActionResult> SetMasterProcessType(ProcessTypeModel model)
        {

            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetMasterProcessType(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterProcessType(ProcessTypeModel model)
        {

            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DeleteMasterProcessType(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetDetailForCheckSymbol")]
        public async Task<ActionResult> GetDetailForCheckSymbol(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetDetailForCheckSymbol(model);
                var result = MapDetail2PartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartDetailSummaryForWorkModel MapDetail2PartDatailSummaryForWorkModel(PartDetailModel models)
        {
            var totalRecord = 0;
            if (models.Data != null)
                totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;

            var result = new DetailPartDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            if (models.Data != null)
                foreach (var item in models.Data)
                {

                    var items = new DetailPartDetailSummaryForWorkHistory()
                    {
                        ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                        PJ_NAME = item.PJ_NAME,
                        PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                        PD_CODE = item.PD_CODE,
                        SYMBOL = item.SYMBOL,
                        PART_ITEM = DataConverter.GetString(item.PART_ITEM),
                        PART_SIZE = item.PART_SIZE,
                        CUTTING_PLAN = item.CUTTING_PLAN,
                        PROCESS = item.PROCESS,
                        HOLE = DataConverter.GetString(item.HOLE),
                        TAPER = DataConverter.GetNumberFormat(item.TAPER, 2),
                        PLAN_QTY = DataConverter.GetString(item.PLAN_QTY),
                        ACT_QTY = DataConverter.GetString(item.ACT_QTY),
                        //ACT_LENGTH = DataConverter.GetNumberFormat(item.ACT_LENGTH, 2),
                        ACT_GROUP = item.ACT_GROUP,
                        ACTUAL_USER = item.ACTUAL_USER,
                        ACT_NAME = item.ACT_NAME,
                        ACT_DATE = item.ACT_DATE,

                    };
                    result.DetailPartDetailSummaryForWorkHistories.Add(items);
                }


            var headschk = new HeaderPartDetailSummaryForWork()
            {
                head = "SELECT",
                cansort = false,
                field = "ROW_NUM",
                sort = "",
                style = "vertical-align:initial; text-align:center",
                fieldType = "checkbox",
            };
            result.HeaderPartDetailSummaryForWork.Add(headschk);


            foreach (var head in models.Header)
            {

                var heads = new HeaderPartDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderPartDetailSummaryForWork.Add(heads);
            }

            var headstext = new HeaderPartDetailSummaryForWork()
            {
                head = "ACT_SAVE",
                cansort = false,
                field = "ACT_QTY",
                sort = "",
                style = "vertical-align:initial; text-align:center",
                fieldType = "textbox",
            };
            result.HeaderPartDetailSummaryForWork.Add(headstext);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetCheckSymbol(List<PartDatilSummaryForWorkModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetCheckSymbol(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }



        [HttpPost]
        [Route("GetDetailForCheckSymbolData")]
        public async Task<ActionResult> GetDetailForCheckSymbolData(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetDetailForCheckSymbolData(model);
                var result = MapDetailDataPartDatailSummaryForWorkModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailPartDetailSummaryForWorkModel MapDetailDataPartDatailSummaryForWorkModel(PartDetailModel models)
        {
            var totalRecord = 0;
            if (models.Data != null)
                totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;

            var result = new DetailPartDetailSummaryForWorkModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            if (models.Data != null)
                foreach (var item in models.Data)
                {

                    var items = new DetailPartDetailSummaryForWorkHistory()
                    {
                        ID = DataConverter.GetString(item.ID),
                        ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                        PJ_NAME = item.PJ_NAME,
                        PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                        PD_CODE = item.PD_CODE,
                        SYMBOL = item.SYMBOL,
                        PART_ITEM = DataConverter.GetString(item.PART_ITEM),
                        PART_SIZE = item.PART_SIZE,
                        CUTTING_PLAN = item.CUTTING_PLAN,
                        PROCESS = item.PROCESS,
                        HOLE = DataConverter.GetString(item.HOLE),
                        TAPER = DataConverter.GetNumberFormat(item.TAPER, 2),
                        PLAN_QTY = DataConverter.GetString(item.PLAN_QTY),
                        ACT_QTY = DataConverter.GetString(item.ACT_QTY),
                        //ACT_LENGTH = DataConverter.GetNumberFormat(item.ACT_LENGTH, 2),
                        ACT_GROUP = item.ACT_GROUP,
                        ACTUAL_USER = item.ACTUAL_USER,
                        ACT_NAME = item.ACT_NAME,
                        ACT_DATE = item.ACT_DATE,

                    };
                    result.DetailPartDetailSummaryForWorkHistories.Add(items);
                }


            var headschk = new HeaderPartDetailSummaryForWork()
            {
                head = "SELECT",
                cansort = false,
                field = "ID",
                sort = "",
                style = "vertical-align:initial; text-align:center",
                fieldType = "checkbox",
            };
            result.HeaderPartDetailSummaryForWork.Add(headschk);


            foreach (var head in models.Header)
            {

                var heads = new HeaderPartDetailSummaryForWork()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    //fieldType = "linkEmpProcess",
                    // functionCall = head.Value == "PART" || head.Value == "REVISE" || head.Value == "WELD" ? "Show" : "ShowOther",
                };
                result.HeaderPartDetailSummaryForWork.Add(heads);
            }


            return result;
        }

        [HttpPost]
        public async Task<ActionResult> DelCheckSymbolData(List<DataValueModel> model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelCheckSymbolData(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        public enum UploadEndTab
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 1,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 2,
            [Description("กรุ๊ป")]
            DEPT_NAME = 3,
            [Description("PRICE")]
            PRICE = 4,

        }

        public enum UploadCutPart
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("วันที่")]
            WEEK_NAME = 1,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 2,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 3,
            [Description("กรุ๊ป")]
            DEPT_NAME = 4,
            [Description("จำนวนเงิน")]
            AMOUNT = 5,
            [Description("รหัสพนักงาน")]
            EMP_CODE_RECIVE = 6,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME_RECIVE = 7,
            [Description("กรุ๊ป")]
            DEPT_NAME_RECIVE = 8,

        }


        #region Emun
        public enum DetailEmp
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("กรุ๊ป")]
            DEPT_NAME = 2

        }
        public enum DetailEmpGoal
        {
            [Description("รหัสพนักงาน")]
            EMP_CODE = 0,

            [Description("ชื่่อนามสกุล")]
            FULL_NAME = 1,

            [Description("กรุ๊ป")]
            DEPT_NAME = 2,

            [Description("เป้าหมาย")]
            EMP_GOAL = 3,

            [Description("เป้าหมายรวม(วัน)")]
            GOAL = 4
        }

        public enum AdjustGola
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,

            [Description("Week")]
            WEEK_NO = 1,

            [Description("รวม Part")]
            SUM_PART = 2,

            [Description("เป้าหมายปัจจุบัน")]
            CURRENT_GOAL = 3,

            [Description("เป้าหมายเฉลี่ย")]
            ADJUST_GOAL = 4,

            [Description("เป้าหมายใหม่")]
            NEW_GOAL = 5,

        }

        public enum Project
        {
            [Description("โปรเจค")]
            PROJECT_NAME = 0,

            [Description("ไอเทม")]
            ITEM = 1,
        }

        public enum Product
        {
            [Description("วันที่ส่ง")]
            ACTUAL_DATE = 0,

            [Description("จำนวน")]
            ACTUAL = 1,
        }

        public enum Minus
        {
            [Description("ราคา")]
            PRICE = 0,

            [Description("วันที่")]
            ACTUAL_DATE = 1,

            [Description("หมายเหตุ")]
            REMARK = 2,
        }

        public enum Other
        {
            [Description("โปรเซส")]
            PROCESS_NAME = 0,

            [Description("ราคา")]
            PRICE = 1,

            [Description("จำนวน")]
            ACTUAL = 2,

            [Description("วันที่")]
            ACTUAL_DATE = 3,

           
        }
        public enum VTDetail
        {
            [Description("PROJECT")]
            PROJECT = 1,
            [Description("ITEM")]
            ITEM = 2,
            [Description("REV_NO")]
            REV_NO = 3,
            [Description("CHECK_NO")]
            CHECK_NO = 4,
            [Description("PART")]
            PART = 5,
            [Description("POINT")]
            POINT = 6,
            [Description("PROBLEM")]
            PROBLEM = 7,
            [Description("DWG")]
            DWG = 8,
            [Description("ACT")]
            ACT = 9,
            [Description("WRONG")]
            WRONG = 10,
            [Description("EDIT")]
            EDIT = 11,
            [Description("CHECKER")]
            CHECKER = 12,
            [Description("CHECK_DATE")]
            CHECK_DATE = 13,


        }

        public enum NCRDetail
        {
            [Description("กรุ๊ป")]
            GROUP_NAME = 1,
            [Description("MAIN")]
            MAIN = 2,
            [Description("TEMPO")]
            TEMPO = 3,
          


        }


        public enum UploadNG
        {
            [Description("รหัสพนักงาน")]
            GROUP_NAME = 1,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 2,
            [Description("กรุ๊ป")]
            DEPT_NAME = 3,
            [Description("% NG")]
            NG = 4,
      
        }
        public enum UploadActUT
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 1,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 2,
            [Description("กรุ๊ป")]
            DEPT_NAME = 3,
        

        }
        public enum UploadNGNew
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 1,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 2,
            [Description("กรุ๊ป")]
            DEPT_NAME = 3,
            [Description("TOTAL_JOINT")]
            TOTAL_JOINT = 4,
            [Description("JOINT_NG")]
            JOINT_NG = 5,
            [Description("PERCENT_NG")]
            PERCENT_NG = 6,

        }
        public enum UploadActUTNew
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 1,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 2,
            [Description("กรุ๊ป")]
            DEPT_NAME = 3,
            [Description("% NG")]
            NG = 4,


        }
        public enum UploadCutFinising
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("วันที่")]
            WEEK_NAME = 1,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 2,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 3,
            [Description("กรุ๊ป")]
            DEPT_NAME = 4,

            [Description("จำนวนเงิน")]
            AMOUNT = 5,
            [Description("รหัสพนักงาน")]
            EMP_CODE_RECIVE =6,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME_RECIVE = 7,
            [Description("กรุ๊ป")]
            DEPT_NAME_RECIVE = 8 ,

        }
        public enum UploadGoal
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 2,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 3,
            [Description("กรุ๊ป")]
            DEPT_NAME = 4,

            [Description("ชื่อเลเวล")]
            LEVEL_NAME = 5,
            [Description("เป้าหมาย")]
            LEVEL_GOAL = 6,


        }
        public enum UploadReportNG
        {
            [Description("ปี - เดือน")]
            WEEK_YEAR = 1,
            [Description("สัปดาห์")]
            WEEK_NAME = 2,
            [Description("รหัสพนักงาน")]
            GROUP_NAME = 3,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 4,
            [Description("กรุ๊ป")]
            DEPT_NAME = 5,
            [Description("% NG")]
            NG = 6,




        }
        public enum Desing
        {
            [Description("GROUP")]
            GROUP_NAME = 0,
            [Description("NUMBER_DESIGN")]
            NUMBER_DESING = 1,
            [Description("PROJECT")]
            PROJECT = 2,
            [Description("ITEM")]
            ITEM = 3,
            [Description("DATE")]
            DATE = 4,
            


        }

        public enum UploadOther
        {
            [Description("EMP_CODE")]
            EMP_CODE = 0,
            [Description("FULL_NAME")]
            FULL_NAME = 1,
            [Description("PROCESS")]
            PROCESS_NAME = 2,
            [Description("PRICE")]
            PRICE = 3,
            [Description("ACTUAL")]
            ACTUAL = 4,
            [Description("ACTUAL_DATE")]
            ACTUAL_DATE = 5,
            [Description("TYPE")]
            TYPE_LOAD = 6,



        }
        public enum AdminOther
        {
            [Description("ลำดับ")]
            ROW_NUM = 1,
            [Description("รหัสพนักงาน")]
            EMP_CODE =2,
            [Description("ชื่อ - นามสกุล")]
            FULL_NAME = 3,
            [Description("แผนก")]
            DEPT_NAME = 4,
            [Description("โปรเซส")]
            PROCESS_NAME = 5,
            [Description("ราคา")]
            PRICE = 6,
            [Description("จำนวน")]
            ACTUAL = 7,
            [Description("วันที่")]
            ACTUAL_DATE = 8,
      



        }

        public enum AdminMinus
        {
            [Description("ลำดับ")]
            ROW_NUM = 1,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 2,
            [Description("ชื่อ - นามสกุล")]
            FULL_NAME = 3,
            [Description("แผนก")]
            DEPT_NAME = 4,
            [Description("จำนวนเงิน")]
            PRICE = 5,
            [Description("วันที่")]
            ACTUAL_DATE = 7,

            [Description("หมายเหตุ")]
            REMARK =8,




        }
        public enum UploadWeek
        {
            [Description("WEEK")]
            WEEK_KEY = 0,
            [Description("WEEK_NO")]
            WEEK_NO = 1,
            [Description("DATE")]
            WEEK_DATE = 2,
           




        }
        public enum AdminWeek
        {
            [Description("ลำดับ")]
            ROW_NUM = 0,
            [Description("ปี")]
            WEEK_YEAR = 1,
            [Description("เดือน")]
            WEEK_MONTH = 2,





        }
        private string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }


        #endregion

        #region Built Beam/Box by chaiwud.ta
        // GoalB 
        // 2018-02-13 by chaiwud.ta
        public ActionResult GoalUploadManagementBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public DetailGoalSumModel MapDetailGoalModelBuilt(GoalHeaderModel models, bool detail, bool overgoal)
        {
            var totalRecord = models.GoalModel.Any() ? (int)models.GoalModel.First().TOTAL_COUNT : 0;
            var result = new DetailGoalSumModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.GoalModel)
            {

                var items = new DetailGoalSumHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    EMP_GOAL = DataConverter.GetNumberFormat(item.EMP_GOAL, 0),
                    GOAL = item.GOAL,
                    PART = DataConverter.GetNumberFormat(item.PART, 0),
                    OTHER = DataConverter.GetNumberFormat(item.OTHER, 0),
                    REVISE = DataConverter.GetNumberFormat(item.REVISE, 0),
                    WELD = DataConverter.GetNumberFormat(item.WELD, 0),
                    DIMENSION = DataConverter.GetNumberFormat(item.DIMENSION, 0),
                    DEDUCT = DataConverter.GetNumberFormat(item.DEDUCT, 0),
                    OVER_GOAL = DataConverter.GetNumberFormat(item.OVER_GOAL, 0)
                };
                result.DetailGoalSumHistories.Add(items);
            }
            if (detail)
            {
                foreach (DetailEmpGoal h in Enum.GetValues(typeof(DetailEmpGoal)))
                {
                    var heads = new HeaderGoalSum()
                    {
                        head = GetEnumDescription(h),
                        field = DataConverter.GetString(h),
                        cansort = true,
                        sort = DataConverter.GetString(h),
                        style = (DataConverter.GetString(h).Contains("GOAL")) ? "vertical-align:initial; text-align:center" : "",
                    };
                    result.HeaderGoalSum.Add(heads);
                }
            }
            foreach (var head in models.Header)
            {

                var heads = new HeaderGoalSum()
                {
                    head = head.Value,
                    fieldType = "linkEmpProcess",
                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = "vertical-align:initial; text-align:right",
                    functionCall = "Show"
                };
                result.HeaderGoalSum.Add(heads);
            }
            if (overgoal)
            {
                var heads = new HeaderGoalSum()
                {
                    head = "เป้าหมาย(ขาด)",
                    field = "OVER_GOAL",
                    cansort = true,
                    sort = "OVER_GOAL",
                    style = "vertical-align:initial; text-align:right",
                };
                result.HeaderGoalSum.Add(heads);

            }
            var headsCol = new HeaderGoalSum()
            {
                head = "GOAL",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "showGoal",
                field = "EMP_CODE",
                cansort = false,
                sort = string.Empty,
                style = "width:30px;vertical-align:initial; text-align:center",
                functionCall = "ShowGoal",
            };
            result.HeaderGoalSum.Add(headsCol);
            return result;
        }

        [HttpPost]
        [Route("GetUploadGoalLogBuilt")]
        public async Task<ActionResult> GetUploadGoalLogBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadGoalLogBuilt(model);
                var result = MapDetailGoalModelBuilt(serviceResult, true, false);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetUploadGoalBuilt")]
        public async Task<ActionResult> GetUploadGoalBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadGoalBuilt(model);
                var result = MapDetailGoalModelBuilt(serviceResult, false, true);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadGoalBuilt(List<UploadGoalBuiltModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadGoalBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        public DetailUploadGoalModel MapDetailGoalModelBuilt(List<UploadGoalBuiltModel> models, bool chk, bool upload)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUploadGoalModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new DetailUploadGoalHistory()
                {
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    DEPT_NAME = DataConverter.GetString(item.DEPT_NAME),
                    LEVEL_NAME = DataConverter.GetString(item.LEVEL_NAME),
                    LEVEL_GOAL = DataConverter.GetString(item.LEVEL_GOAL),
                };
                result.DetailUploadGoalHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderUploadGoal()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "RUN_ID",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderUploadGoal.Add(headschk);
            }

            foreach (UploadGoal h in Enum.GetValues(typeof(UploadGoal)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "LEVEL_GOAL":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }
                    case "EMP_CODE":
                    case "WEEK_KEY":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }
                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUploadGoal()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUploadGoal.Add(heads);
            }
            if (upload)
            {
                var headsCol = new HeaderUploadGoal()
                {
                    head = "DELETE",
                    //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                    fieldType = "delete",
                    field = "RUN_ID",
                    cansort = false,
                    sort = string.Empty,
                    style = "vertical-align:initial;text-align:center",
                    functionCall = "ShowDelete",
                };
                result.HeaderUploadGoal.Add(headsCol);
            }
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> DelUploadGoalBuilt(UploadGoalBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadGoalBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }

        [HttpPost]
        public async Task<ActionResult> DelUploadGoalLogBuilt(List<UploadGoalBuiltModel> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadGoalLogBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        // UTRepairCheck
        // 2018-02-28 by chaiwud.ta
        #region UTRepairCheck
        /// Page : RepairCheckUploadManagementBuilt
        /// Mapper : MapDetailUTRepairCheckBuilt
        /// GetUploadUTRepairCheckLogBuilt
        /// GetUploadUTRepairCheckBuilt
        /// 
        #region ENUM
        public enum UploadUTRepairCheckB
        {
            [Description("PJ_NAME")]
            PJ_NAME = 0,
            [Description("PD_ITEM")]
            PD_ITEM = 1,
            [Description("ITEM_NO")]
            ITEM_NO = 2,
            [Description("BUILT_TYPE")]
            BUILT_TYPE = 3,
            //[Description("ACTUAL")]
            //ACTUAL = 4,
            [Description("ACTUAL_DATE")]
            ACTUAL_DATE = 5,
            [Description("ACTUAL_CHECK")]
            ACTUAL_CHECK = 6,
            [Description("REMARK")]
            REMARK = 7,
        }

        public enum AdminUTRepairCheckB
        {
            [Description("ลำดับ")]
            ROW_NUM = 1,
            [Description("ชื่อโครงการ")]
            PJ_NAME = 2,
            [Description("ไอเทม")]
            PD_ITEM = 3,
            [Description("ท่อน")]
            ITEM_NO = 4,
            [Description("ประเภท")]
            BUILT_TYPE = 5,
            [Description("โพรเซส")]
            PC_NAME = 6,
            //[Description("จำนวน")]
            //ACTUAL = 7,
            [Description("วันที่ตรวจ")]
            ACTUAL_DATE = 8,
            [Description("รหัสผู้ตรวจ")]
            ACTUAL_CHECK = 9,
            [Description("ชื่อผู้ตรวจ")]
            FULLNAME_CHECK = 10,
            [Description("remark")]
            REMARK = 11,
        }
        #endregion
        #region UTRepair
        public ActionResult RepairCheckUploadManagementBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        [HttpPost]
        [Route("GetUploadUTRepairCheckLogBuilt")]
        public async Task<ActionResult> GetUploadUTRepairCheckLogBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadUTRepairCheckLogBuilt(model);
                var result = MapDetailUTRepairCheckModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailUTRepairCheckBuiltModel MapDetailUTRepairCheckModel(List<UTRepairCheckModelBuilt> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUTRepairCheckBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new UTRepairCheckBuiltHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    PJ_NAME = DataConverter.GetString(item.PJ_NAME),
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    ITEM_NO = DataConverter.GetString(item.ITEM_NO),
                    BUILT_TYPE = DataConverter.GetString(item.BUILT_TYPE),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),
                    ACTUAL_CHECK = DataConverter.GetString(item.ACTUAL_CHECK),
                    REMARK = DataConverter.GetString(item.REMARK),
                };
                result.UTRepairCheckBuiltHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderUTRepairCheckBuilt()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderUTRepairCheckBuilt.Add(headschk);
            }

            foreach (UploadUTRepairCheckB h in Enum.GetValues(typeof(UploadUTRepairCheckB)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ACTUAL":
                    case "ACTUAL_DATE":
                    case "ACTUAL_CHECK":
                        {
                            sty = "vertical-align:initial; text-align:right";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUTRepairCheckBuilt()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUTRepairCheckBuilt.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetUploadUTRepairCheckBuilt")]
        public async Task<ActionResult> GetUploadUTRepairCheckBuilt(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadUTRepairCheckBuilt(model);
                var result = MapDetailUTRepairCheckDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        public DetailUTRepairCheckBuiltModel MapDetailUTRepairCheckDataModel(List<UTRepairCheckModelBuilt> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUTRepairCheckBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new UTRepairCheckBuiltHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    ID = DataConverter.GetString(item.RUN_ID),
                    PJ_NAME = DataConverter.GetString(item.PJ_NAME),
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    ITEM_NO = DataConverter.GetString(item.ITEM_NO),
                    BUILT_TYPE = DataConverter.GetString(item.BUILT_TYPE),
                    PC_NAME = DataConverter.GetString(item.PC_NAME),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),
                    ACTUAL_CHECK = DataConverter.GetString(item.ACTUAL_CHECK),
                    FULLNAME_CHECK = DataConverter.GetString(item.FULLNAME_CHECK),
                    REMARK = DataConverter.GetString(item.REMARK),
                };
                result.UTRepairCheckBuiltHistories.Add(items);
            }

            foreach (AdminUTRepairCheckB h in Enum.GetValues(typeof(AdminUTRepairCheckB)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ACTUAL_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUTRepairCheckBuilt()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUTRepairCheckBuilt.Add(heads);

            }
            var headsCol = new HeaderUTRepairCheckBuilt()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderUTRepairCheckBuilt.Add(headsCol);

            return result;
        }


        [HttpPost]
        public async Task<ActionResult> SetUploadUTRepairCheckLogBuilt(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "UTRepairCheck_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "UTRepairCheck_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "UTCHECK", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<UTRepairCheckModelBuilt> VTmodel = new List<UTRepairCheckModelBuilt>();
                    UTRepairCheckModelBuilt VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new UTRepairCheckModelBuilt();
                            VT.PJ_NAME = DataConverter.GetString(dr[0]);
                            VT.PD_ITEM = DataConverter.GetInteger(dr[1]);
                            VT.ITEM_NO = DataConverter.GetInteger(dr[2]);
                            VT.BUILT_TYPE = DataConverter.GetString(dr[3]);
                            VT.ACTUAL_DATE = DataConverter.GetDate_yyyMMdd(Convert.ToDateTime(dr[4].ToString()));
                            VT.ACTUAL_CHECK = DataConverter.GetString(dr[5]);
                            VT.REMARK = DataConverter.GetString(dr[6]);
                            VT.UPDATE_BY = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadUTRepairCheckLogBuilt(VTmodel);
                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);
                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetUploadUTRepairCheckBuilt(List<UTRepairCheckModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadUTRepairCheckBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.UPDATE_BY = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadUTRepairCheckLogBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.UPDATE_BY = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadUTRepairCheckBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        #endregion
        #endregion

        #region UTDataByUser
        public enum UploadUTDataByUserB
        {
            [Description("ปี - เดือน")]
            WEEK_KEY = 0,
            [Description("สัปดาห์")]
            WEEK_SUB = 1,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 2,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 3,
            [Description("กรุ๊ป")]
            GROUP_NAME = 4,
            [Description("UT1")]
            UT1 = 5,
            [Description("UT2")]
            UT2 = 6,
        }
        public enum AdminUTDataByUserB
        {
            [Description("ลำดับ")]
            ROW_NUM = 0,
            [Description("ปี - เดือน")]
            WEEK_KEY = 1,
            [Description("สัปดาห์")]
            WEEK_SUB = 2,
            [Description("รหัสพนักงาน")]
            EMP_CODE = 3,
            [Description("ชื่อ - นามสุกล")]
            FULL_NAME = 4,
            [Description("กรุ๊ป")]
            GROUP_NAME = 5,
            [Description("UT1")]
            UT1 = 6,
            [Description("UT2")]
            UT2 = 7,
        }

        public ActionResult UTDataByUserUploadBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        [HttpPost]
        [Route("GetUploadUTDataByUserLogBuilt")]
        public async Task<ActionResult> GetUploadUTDataByUserLogBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadUTDataByUserLogBuilt(model);
                var result = MapDetailUTDataByUserModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailUTDataByUserBuiltModel MapDetailUTDataByUserModel(List<UTDataByUserModelBuilt> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUTDataByUserBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new UTDataByUserBuiltHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_SUB = DataConverter.GetString(item.WEEK_SUB),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    UT1 = DataConverter.GetString(item.UT1),
                    UT2 = DataConverter.GetString(item.UT2),
                };
                result.UTDataByUserBuiltHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderUTDataByUserBuilt()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderUTDataByUserBuilt.Add(headschk);
            }

            foreach (UploadUTDataByUserB h in Enum.GetValues(typeof(UploadUTDataByUserB)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "WEEK_KEY":
                    case "WEEK_SUB":
                    case "EMP_CODE":
                    case "GROUP_NAME":
                    case "UT1":
                    case "UT2":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUTDataByUserBuilt()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUTDataByUserBuilt.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetUploadUTDataByUserBuilt")]
        public async Task<ActionResult> GetUploadUTDataByUserBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadUTDataByUserBuilt(model);
                var result = MapDetailUTDataByUserDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailUTDataByUserBuiltModel MapDetailUTDataByUserDataModel(List<UTDataByUserModelBuilt> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailUTDataByUserBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new UTDataByUserBuiltHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    WEEK_KEY = DataConverter.GetString(item.WEEK_KEY),
                    WEEK_SUB = DataConverter.GetString(item.WEEK_SUB),
                    EMP_CODE = DataConverter.GetString(item.EMP_CODE),
                    FULL_NAME = DataConverter.GetString(item.FULL_NAME),
                    GROUP_NAME = DataConverter.GetString(item.GROUP_NAME),
                    UT1 = DataConverter.GetString(item.UT1),
                    UT2 = DataConverter.GetString(item.UT2),
                };
                result.UTDataByUserBuiltHistories.Add(items);
            }

            foreach (AdminUTDataByUserB h in Enum.GetValues(typeof(AdminUTDataByUserB)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ROW_NUM":
                    case "WEEK_KEY":
                    case "WEEK_SUB":
                    case "EMP_CODE":
                    case "GROUP_NAME":
                    case "UT1":
                    case "UT2":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderUTDataByUserBuilt()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderUTDataByUserBuilt.Add(heads);

            }
            var headsCol = new HeaderUTDataByUserBuilt()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "RUN_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderUTDataByUserBuilt.Add(headsCol);

            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadUTDataByUserLogBuilt(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "UTDataBuilt_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "UTDataBuilt_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "UTDATAUSER", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<UTDataByUserModelBuilt> VTmodel = new List<UTDataByUserModelBuilt>();
                    UTDataByUserModelBuilt VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new UTDataByUserModelBuilt();
                            VT.EMP_CODE = DataConverter.GetString(dr[0]);
                            VT.UT1 = DataConverter.GetDecimal(dr[1]);
                            VT.UT2 = DataConverter.GetDecimal(dr[2]);
                            VT.WEEK_KEY = model.month;
                            VT.WEEK_SUB = model.week;
                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadUTDataByUserLogBuilt(VTmodel);
                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);
                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        public async Task<ActionResult> SetUploadUTDataByUserBuilt(List<UTDataByUserModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadUTDataByUserBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        [HttpPost]
        public async Task<ActionResult> DelUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadUTDataByUserLogBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        [HttpPost]
        public async Task<ActionResult> DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadUTDataByUserBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        #endregion

        #region NGDataBuilt
        // 
        public enum UploadNGDataB
        {
            [Description("ID")]
            RUN_ID = 0,
            [Description("Date")]
            DATE = 1,
            [Description("Project")]
            PROJECT = 2,
            [Description("Item")]
            PD_ITEM = 3,
            [Description("Code")]
            CODE = 4,
            [Description("Skin Plate")]
            SKIN_PLATE = 5,
            [Description("Diaphragm")]
            DIAPHRAGM = 6,
            [Description("Saw NG Length")]
            SAW_NG_LENGTH = 7,
            [Description("Saw NG Joint")]
            SAW_NG_JOINT = 8,
            [Description("Saw Total")]
            SAW_TOTAL = 9,
            [Description("Saw D")]
            SAW_D = 10,
            [Description("Saw K")]
            SAW_K = 11,
            [Description("Saw Weld")]
            SAW_WELDER = 12,
            [Description("AutoRoot NG Length")]
            AUTOROOT_NG_LENGTH = 13,
            [Description("AutoRoot NG Joint")]
            AUTOROOT_NG_JOINT = 14,
            [Description("AutoRoot NG Total")]
            AUTOROOT_TOTAL = 15,
            [Description("AutoRoot D")]
            AUTOROOT_D = 16,
            [Description("AutoRoot K")]
            AUTOROOT_K = 17,
            [Description("AutoRoot Welder")]
            AUTOROOT_WELDER = 18,
            [Description("ESW NG Length")]
            ESW_NG_LENGTH = 19,
            [Description("ESW NG Joint")]
            ESW_NG_JOINT = 20,
            [Description("ESW Total")]
            ESW_TOTAL = 21,
            [Description("ESW D")]
            ESW_D = 22,
            [Description("ESW X")]
            ESW_X = 23,
            [Description("ESW Side")]
            ESW_SIDE = 24,
            [Description("ESW Welder")]
            ESW_WELDER = 25,
        }

        public enum AdminNGDataB
        {
            [Description("No.")]
            ROW_NUM = 0,
            [Description("Date")]
            DATE = 1,
            [Description("Project")]
            PROJECT = 2,
            [Description("Item")]
            PD_ITEM = 3,
            [Description("Code")]
            CODE = 4,
            [Description("Skin Plate")]
            SKIN_PLATE = 5,
            [Description("Diaphragm")]
            DIAPHRAGM = 6,
            [Description("Saw NG Length")]
            SAW_NG_LENGTH = 7,
            [Description("Saw NG Joint")]
            SAW_NG_JOINT = 8,
            [Description("Saw Total")]
            SAW_TOTAL = 9,
            [Description("Saw D")]
            SAW_D = 10,
            [Description("Saw K")]
            SAW_K = 11,
            [Description("Saw Weld")]
            SAW_WELDER = 12,
            [Description("AutoRoot NG Length")]
            AUTOROOT_NG_LENGTH = 13,
            [Description("AutoRoot NG Joint")]
            AUTOROOT_NG_JOINT = 14,
            [Description("AutoRoot NG Total")]
            AUTOROOT_TOTAL = 15,
            [Description("AutoRoot D")]
            AUTOROOT_D = 16,
            [Description("AutoRoot K")]
            AUTOROOT_K = 17,
            [Description("AutoRoot Welder")]
            AUTOROOT_WELDER = 18,
            [Description("ESW NG Length")]
            ESW_NG_LENGTH = 19,
            [Description("ESW NG Joint")]
            ESW_NG_JOINT = 20,
            [Description("ESW Total")]
            ESW_TOTAL = 21,
            [Description("ESW D")]
            ESW_D = 22,
            [Description("ESW X")]
            ESW_X = 23,
            [Description("ESW Side")]
            ESW_SIDE = 24,
            [Description("ESW Welder")]
            ESW_WELDER = 25,
        }


        public ActionResult NGDataUploadBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        [HttpPost]
        [Route("GetUploadNGDataLogBuilt")]
        public async Task<ActionResult> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadNGDataLogBuilt(model);
                var result = MapDetailNGDataModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailNGDataBuiltModel MapDetailNGDataModel(List<NGDataModelBuilt> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNGDataBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new NGDataBuiltHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    DATE = DataConverter.GetString(item.DATE),
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    //NO_ORDER = DataConverter.GetString(item.NO_ORDER),
                    CODE = DataConverter.GetString(item.CODE),
                    SKIN_PLATE = DataConverter.GetString(item.SKIN_PLATE),
                    DIAPHRAGM = DataConverter.GetString(item.DIAPHRAGM),
                    SAW_NG_LENGTH = item.SAW_NG_LENGTH < 0 ? "" : DataConverter.GetString(item.SAW_NG_LENGTH),
                    SAW_NG_JOINT = item.SAW_NG_JOINT < 0 ? "" : DataConverter.GetString(item.SAW_NG_JOINT),
                    SAW_TOTAL = item.SAW_TOTAL < 0 ? "" : DataConverter.GetString(item.SAW_TOTAL),
                    SAW_D = DataConverter.GetString(item.SAW_D),
                    SAW_K = DataConverter.GetString(item.SAW_K),
                    SAW_WELDER = DataConverter.GetString(item.SAW_WELDER),
                    AUTOROOT_NG_LENGTH = item.AUTOROOT_NG_LENGTH < 0 ? "" : DataConverter.GetString(item.AUTOROOT_NG_LENGTH),
                    AUTOROOT_NG_JOINT = item.AUTOROOT_NG_JOINT < 0 ? "" : DataConverter.GetString(item.AUTOROOT_NG_JOINT),
                    AUTOROOT_TOTAL = item.AUTOROOT_TOTAL < 0 ? "" : DataConverter.GetString(item.AUTOROOT_TOTAL),
                    AUTOROOT_D = DataConverter.GetString(item.AUTOROOT_D),
                    AUTOROOT_K = DataConverter.GetString(item.AUTOROOT_K),
                    AUTOROOT_WELDER = DataConverter.GetString(item.AUTOROOT_WELDER),
                    ESW_NG_LENGTH = item.ESW_NG_LENGTH < 0 ? "" : DataConverter.GetString(item.ESW_NG_LENGTH),
                    ESW_NG_JOINT = item.ESW_NG_JOINT < 0 ? "" : DataConverter.GetString(item.ESW_NG_JOINT),
                    ESW_TOTAL = item.ESW_TOTAL < 0 ? "" : DataConverter.GetString(item.ESW_TOTAL),
                    ESW_D = DataConverter.GetString(item.ESW_D),
                    ESW_X = DataConverter.GetString(item.ESW_X),
                    ESW_SIDE = DataConverter.GetString(item.ESW_SIDE),
                    ESW_WELDER = DataConverter.GetString(item.ESW_WELDER),
                    USER_UPDATE = DataConverter.GetString(item.USER_UPDATE),
                };
                result.NGDataBuiltHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderNGDataBuilt()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderNGDataBuilt.Add(headschk);
            }

            foreach (UploadNGDataB h in Enum.GetValues(typeof(UploadNGDataB)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    //case "WEEK_KEY":
                    //case "WEEK_SUB":
                    //case "EMP_CODE":
                    //case "GROUP_NAME":
                    //case "UT1":
                    //case "UT2":
                    //    {
                    //        sty = "vertical-align:initial; text-align:center";
                    //        break;
                    //    }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderNGDataBuilt()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNGDataBuilt.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetUploadNGDataBuilt")]
        public async Task<ActionResult> GetUploadNGDataBuilt(ReportSearchBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadNGDataBuilt(model);
                var result = MapDetailNGDataDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailNGDataBuiltModel MapDetailNGDataDataModel(List<NGDataModelBuilt> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailNGDataBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new NGDataBuiltHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),
                    DATE = DataConverter.GetString(item.DATE),

                    PROJECT = DataConverter.GetString(item.PROJECT),
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    NO_ORDER = DataConverter.GetString(item.NO_ORDER),
                    CODE = DataConverter.GetString(item.CODE),
                    SKIN_PLATE = DataConverter.GetString(item.SKIN_PLATE),
                    DIAPHRAGM = DataConverter.GetString(item.DIAPHRAGM),
                    SAW_NG_LENGTH = item.SAW_NG_LENGTH < 0 ? "" : DataConverter.GetString(item.SAW_NG_LENGTH),
                    SAW_NG_JOINT = item.SAW_NG_JOINT < 0 ? "" : DataConverter.GetString(item.SAW_NG_JOINT),
                    SAW_TOTAL = item.SAW_TOTAL < 0 ? "" : DataConverter.GetString(item.SAW_TOTAL),
                    SAW_D = DataConverter.GetString(item.SAW_D),
                    SAW_K = DataConverter.GetString(item.SAW_K),
                    SAW_WELDER = DataConverter.GetString(item.SAW_WELDER),
                    AUTOROOT_NG_LENGTH = item.AUTOROOT_NG_LENGTH < 0 ? "" : DataConverter.GetString(item.AUTOROOT_NG_LENGTH),
                    AUTOROOT_NG_JOINT = item.AUTOROOT_NG_JOINT < 0 ? "" : DataConverter.GetString(item.AUTOROOT_NG_JOINT),
                    AUTOROOT_TOTAL = item.AUTOROOT_TOTAL < 0 ? "" : DataConverter.GetString(item.AUTOROOT_TOTAL),
                    AUTOROOT_D = DataConverter.GetString(item.AUTOROOT_D),
                    AUTOROOT_K = DataConverter.GetString(item.AUTOROOT_K),
                    AUTOROOT_WELDER = DataConverter.GetString(item.AUTOROOT_WELDER),
                    ESW_NG_LENGTH = item.ESW_NG_LENGTH < 0 ? "" : DataConverter.GetString(item.ESW_NG_LENGTH),
                    ESW_NG_JOINT = item.ESW_NG_JOINT < 0 ? "" :DataConverter.GetString(item.ESW_NG_JOINT),
                    ESW_TOTAL = item.ESW_TOTAL < 0 ? "" : DataConverter.GetString(item.ESW_TOTAL),
                    ESW_D = DataConverter.GetString(item.ESW_D),
                    ESW_X = DataConverter.GetString(item.ESW_X),
                    ESW_SIDE = DataConverter.GetString(item.ESW_SIDE),
                    ESW_WELDER = DataConverter.GetString(item.ESW_WELDER),
                    USER_UPDATE = DataConverter.GetString(item.USER_UPDATE),
                };
                result.NGDataBuiltHistories.Add(items);
            }

            foreach (AdminNGDataB h in Enum.GetValues(typeof(AdminNGDataB)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    //case "ROW_NUM":
                    //case "WEEK_KEY":
                    //case "WEEK_SUB":
                    //case "EMP_CODE":
                    //case "GROUP_NAME":
                    //case "UT1":
                    //case "UT2":
                    //    {
                    //        sty = "vertical-align:initial; text-align:center";
                    //        break;
                    //    }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderNGDataBuilt()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderNGDataBuilt.Add(heads);

            }
            var headsCol = new HeaderNGDataBuilt()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "RUN_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderNGDataBuilt.Add(headsCol);

            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadNGDataLogBuilt(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "NGDataBuilt_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "NGDataBuilt_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "NGBUILTDATA", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<NGDataModelBuilt> VTmodel = new List<NGDataModelBuilt>();
                    NGDataModelBuilt VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new NGDataModelBuilt();
                            VT.DATE = DataConverter.GetDate_yyyMMdd(DataConverter.GetDateTime(dr[0]));
                            VT.PROJECT = DataConverter.GetString(dr[1]);
                            VT.PD_ITEM = DataConverter.GetInteger(dr[2]);
                            VT.CODE = DataConverter.GetString(dr[3]);
                            VT.SKIN_PLATE = DataConverter.GetDecimal(dr[4]) < 0 ? 0 : DataConverter.GetDecimal(dr[4]);
                            VT.DIAPHRAGM = DataConverter.GetDecimal(dr[5]) < 0 ? 0 : DataConverter.GetDecimal(dr[5]);
                            VT.SAW_NG_LENGTH = DataConverter.GetDecimal(dr[6]) < 0 ? -1 : DataConverter.GetDecimal(dr[6]);
                            VT.SAW_NG_JOINT = DataConverter.GetDecimal(dr[7]) < 0 ? -1 : DataConverter.GetDecimal(dr[7]);
                            VT.SAW_TOTAL = DataConverter.GetDecimal(dr[8]) < 0 ? -1 : DataConverter.GetDecimal(dr[8]);
                            VT.SAW_D = DataConverter.GetString(dr[9]);
                            VT.SAW_K = DataConverter.GetString(dr[10]);
                            VT.SAW_WELDER = DataConverter.GetString(dr[11]);
                            VT.AUTOROOT_NG_LENGTH = DataConverter.GetDecimal(dr[12]) < 0 ? -1 : DataConverter.GetDecimal(dr[12]);
                            VT.AUTOROOT_NG_JOINT = DataConverter.GetDecimal(dr[13]) < 0 ? -1 : DataConverter.GetDecimal(dr[13]);
                            VT.AUTOROOT_TOTAL = DataConverter.GetDecimal(dr[14]) < 0 ? -1 : DataConverter.GetDecimal(dr[14]);
                            VT.AUTOROOT_D = DataConverter.GetString(dr[15]);
                            VT.AUTOROOT_K = DataConverter.GetString(dr[16]);
                            VT.AUTOROOT_WELDER = DataConverter.GetString(dr[17]);
                            VT.ESW_NG_LENGTH = DataConverter.GetDecimal(dr[18]) < 0 ? -1 : DataConverter.GetDecimal(dr[18]);
                            VT.ESW_NG_JOINT = DataConverter.GetDecimal(dr[19]) < 0 ? -1 : DataConverter.GetDecimal(dr[19]);
                            VT.ESW_TOTAL = DataConverter.GetDecimal(dr[20]) < 0 ? -1 : DataConverter.GetDecimal(dr[20]);
                            VT.ESW_D = DataConverter.GetString(dr[21]);
                            VT.ESW_X = DataConverter.GetString(dr[22]);
                            VT.ESW_SIDE = DataConverter.GetString(dr[23]);
                            VT.ESW_WELDER = DataConverter.GetString(dr[24]);
                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadNGDataLogBuilt(VTmodel);
                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);
                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        public async Task<ActionResult> SetUploadNGDataBuilt(List<NGDataModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadNGDataBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        [HttpPost]
        public async Task<ActionResult> DelUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadNGDataLogBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        [HttpPost]
        public async Task<ActionResult> DelUploadNGDataBuilt(NGDataModelBuilt model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadNGDataBuilt(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        #endregion

        #region OtherCamber

        public ActionResult CamberDataUploadOther()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public enum AdminCamberDataO
        {
            [Description("No.")]
            ROW_NUM = 0,
            [Description("Project")]
            PROJECT = 1,
            [Description("Item")]
            PD_ITEM = 2,
            [Description("Cutting Plan")]
            CUTTING_PLAN = 3,
            [Description("Built No")]
            BUILT_NO = 4,
            [Description("User Group")]
            ACTUAL_GROUP = 5,
            [Description("Actual Date")]
            ACTUAL_DATE = 6,
        }


        public enum UploadCamberDataO
        {
            [Description("ID")]
            RUN_ID = 0,
            [Description("Project")]
            PROJECT = 1,
            [Description("Item")]
            PD_ITEM = 2,
            [Description("Cutting Plan")]
            CUTTING_PLAN = 3,
            [Description("Built No")]
            BUILT_NO = 4,
            [Description("User Group")]
            ACTUAL_GROUP = 5,
            [Description("Actual Date")]
            ACTUAL_DATE = 6,
            [Description("Update By")]
            USER_UPDATE = 7,
        }


        [HttpPost]
        [Route("GetUploadCamberDataLogOther")]
        public async Task<ActionResult> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadCamberDataLogOther(model);
                var result = MapDetailCamberOtherDataModel(serviceResult, true);
                return OK(result);
            }
            return OK("");
        }
        public DetailCamberDataOtherModel MapDetailCamberOtherDataModel(List<CamberDataModelOther> models, bool chk)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailCamberDataOtherModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new CamberDataOtherHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),                   
                    PROJECT = DataConverter.GetString(item.PROJECT),
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    CUTTING_PLAN = DataConverter.GetString(item.CUTTING_PLAN),
                    BUILT_NO = DataConverter.GetString(item.BUILT_NO),
                    ACTUAL_GROUP = DataConverter.GetString(item.ACTUAL_GROUP),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),

                    USER_UPDATE = DataConverter.GetString(item.USER_UPDATE),
                };
                result.CamberDataOtherHistories.Add(items);
            }
            if (chk)
            {

                var headschk = new HeaderCamberDataOther()
                {
                    head = "SELECT",
                    cansort = false,
                    field = "ROW_NUM",
                    sort = "",
                    style = "vertical-align:initial; text-align:center",
                    fieldType = "checkbox",
                };
                result.HeaderCamberDataOther.Add(headschk);
            }

            foreach (UploadCamberDataO h in Enum.GetValues(typeof(UploadCamberDataO)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "RUN_ID":
                    case "PD_ITEM":
                    case "CUTTING_PLAN":
                    case "BUILT_NO":
                    case "ACTUAL_GROUP":
                    case "ACTUAL_DATE":
                    case "USER_UPDATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderCamberDataOther()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderCamberDataOther.Add(heads);
            }
            return result;
        }

        [HttpPost]
        [Route("GetUploadCamberDataOther")]
        public async Task<ActionResult> GetUploadCamberDataOther(ReportSearchOtherFinishModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var serviceResult = await service.GetUploadCamberDataOther(model);
                var result = MapDetailCamberOtherDataDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public DetailCamberDataOtherModel MapDetailCamberOtherDataDataModel(List<CamberDataModelOther> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new DetailCamberDataOtherModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {

                var items = new CamberDataOtherHistory()
                {
                    ROW_NUM = DataConverter.GetString(item.ROW_NUM),
                    RUN_ID = DataConverter.GetString(item.RUN_ID),

                    PROJECT = DataConverter.GetString(item.PROJECT),
                    PD_ITEM = DataConverter.GetString(item.PD_ITEM),
                    CUTTING_PLAN = DataConverter.GetString(item.CUTTING_PLAN),
                    BUILT_NO = DataConverter.GetString(item.BUILT_NO),
                    ACTUAL_GROUP = DataConverter.GetString(item.ACTUAL_GROUP),
                    ACTUAL_DATE = DataConverter.GetString(item.ACTUAL_DATE),

                    USER_UPDATE = DataConverter.GetString(item.USER_UPDATE),
                };
                result.CamberDataOtherHistories.Add(items);
            }

            foreach (AdminCamberDataO h in Enum.GetValues(typeof(AdminCamberDataO)))
            {
                string sty = string.Empty;
                switch (h.ToString())
                {
                    case "ROW_NUM":
                    case "PD_ITEM":
                    case "CUTTING_PLAN":
                    case "BUILT_NO":
                    case "ACTUAL_GROUP":
                    case "ACTUAL_DATE":
                        {
                            sty = "vertical-align:initial; text-align:center";
                            break;
                        }

                    default:
                        {
                            sty = "vertical-align:initial; text-align:left";
                            break;
                        }
                }
                var heads = new HeaderCamberDataOther()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = DataConverter.GetString(h).Equals("ROW_NUM") ? false : true,
                    sort = DataConverter.GetString(h),
                    style = sty,
                };
                result.HeaderCamberDataOther.Add(heads);

            }
            var headsCol = new HeaderCamberDataOther()
            {
                head = "DELETE",
                fieldType = "delete",
                field = "RUN_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderCamberDataOther.Add(headsCol);

            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetUploadCamberDataLogOther(UploadModel model)
        {

            if (ModelState.IsValid)
            {
                DataSet ds = new DataSet();
                var filename = model.fileName;
                var sfile = Convert.FromBase64String(model.file);

                string fileExtension =
                                   System.IO.Path.GetExtension(filename);

                string filenameNew = "OTHER_CAMBER_" + DataConverter.GetDate_ddMMyyyyHHmmss(DateTime.Now) + fileExtension;

                var PathSave = ConfigurationHelper.GetAppSettting("PathSave");
                var PathSaveVT = PathSave + "OTHER_CAMBER_\\";
                if (filename != null && filename.Length > 0 && sfile != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(PathSaveVT)))
                        Directory.CreateDirectory(Path.GetDirectoryName(PathSaveVT));

                    FileStream file = new FileStream(PathSaveVT + filenameNew, FileMode.Create, FileAccess.ReadWrite);

                    file.Write(sfile, 0, sfile.Length);

                    file.Close();


                    string excelConnectionString = string.Empty;
                    //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filename + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + PathSaveVT + filenameNew + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    excelConnection.Close();
                    excelConnection.Dispose();
                    excelConnection1.Close();
                    excelConnection1.Dispose();

                    //--Delate File
                    bool IsDeleteFile = DataConverter.GetBoolean(ConfigurationHelper.GetAppSettting("IsDeleteFile", "false"));
                    if (IsDeleteFile)
                    {
                        if (System.IO.File.Exists(PathSaveVT + filenameNew))
                        {

                            System.IO.File.Delete(PathSaveVT + filenameNew);
                        }
                    }
                    var serviceApp = MCSServiceAPIFactory.NewAppServiceWrapperInstance();
                    List<UploadTypeModel> header = await serviceApp.GetHeaderExcel(new DateModel() { TYPE_FILE = "OTHER_CAMB", TYPE_SHOW = "U" });

                    int count = 0;
                    foreach (DataColumn column in ds.Tables[0].Columns)
                    {
                        if (ds.Tables[0].Columns.Count > header.Count)
                        {
                            return OK(new ResultModel() { MESSAGE = "รูปแบบไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        if (!column.ColumnName.Equals(header[count].Value))
                        {
                            return OK(new ResultModel() { MESSAGE = "หัวข้อของไฟล์ไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                        }
                        count++;
                    }

                    List<CamberDataModelOther> VTmodel = new List<CamberDataModelOther>();
                    CamberDataModelOther VT = null;
                    UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                    try
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            VT = new CamberDataModelOther();
                            VT.PROJECT = DataConverter.GetString(dr[0]);
                            VT.PD_ITEM = DataConverter.GetInteger(dr[1]);
                            VT.CUTTING_PLAN = DataConverter.GetString(dr[2]);
                            VT.BUILT_NO = DataConverter.GetString(dr[3]);
                            VT.ACTUAL_GROUP = DataConverter.GetString(dr[4]);
                            VT.ACTUAL_DATE = DataConverter.GetDate_yyyMMdd(DataConverter.GetDateTime(dr[5]));

                            VT.USER_UPDATE = user.UserModel.EMP_CODE;
                            VTmodel.Add(VT);
                        }
                    }
                    catch
                    {
                        return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ แต่ข้อมูลในไฟล์รูปแบบไม่ถูกต้อง", CODE = "02" }, string.Empty, HttpStatusCode.OK);
                    }
                    var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                    var serviceResult = await service.SetUploadCamberDataLogOther(VTmodel);
                    return OK(new ResultModel() { MESSAGE = serviceResult.MESSAGE, CODE = serviceResult.CODE }, string.Empty, HttpStatusCode.OK);
                }

                return OK(new ResultModel() { MESSAGE = "อัพโหลดไฟล์สำเร็จ", CODE = "01" }, string.Empty, HttpStatusCode.OK);

            }

            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        public async Task<ActionResult> SetUploadCamberDataOther(List<CamberDataModelOther> model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.SetUploadCamberDataOther(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        [HttpPost]
        public async Task<ActionResult> DelUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                foreach (var m in model)
                {
                    m.USER_UPDATE = user.UserModel.EMP_CODE;
                }
                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadCamberDataLogOther(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        [HttpPost]
        public async Task<ActionResult> DelUploadCamberDataOther(CamberDataModelOther model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];

                model.USER_UPDATE = user.UserModel.EMP_CODE;

                var service = MCSServiceAPIFactory.NewAdminServiceWrapperInstance();
                var result = await service.DelUploadCamberDataOther(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
        }
        #endregion

        #endregion
    }
}