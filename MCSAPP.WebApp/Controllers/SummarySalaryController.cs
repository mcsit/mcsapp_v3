﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
namespace MCSAPP.WebApp.Controllers
{
    public class SummarySalaryController : BaseController
    {
        // GET: SummarySalary
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportSummaryForSalaryByAutoGas1()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForSalaryByAutoGas2()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForSalaryByAutoGas3()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForSalaryByTaper()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult ReportSummaryForSalaryByPart()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        #region Salary
        [HttpPost]
        [Route("GetSummaryForSalaryByAutoGas1")]
        public async Task<ActionResult> GetSummaryForSalaryByAutoGas1(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByAutoGas1(model);
                var result = MapDetailAutoGas1SummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForSalaryByAutoGas2")]
        public async Task<ActionResult> GetSummaryForSalaryByAutoGas2(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByAutoGas2(model);
                var result = MapDetailAutoGas2SummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForSalaryByAutoGas3")]
        public async Task<ActionResult> GetSummaryForSalaryByAutoGas3(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByAutoGas3(model);
                var result = MapDetailAutoGas2SummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForSalaryByTaper")]
        public async Task<ActionResult> GetSummaryForSalaryByTaper(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByTaper(model);
                var result = MapDetailAutoGas2SummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        [HttpPost]
        [Route("GetSummaryForSalaryByPart")]
        public async Task<ActionResult> GetSummaryForSalaryByPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewWorkServiceWrapperInstance();
                var serviceResult = await service.GetSummaryForSalaryByPart(model);
                var result = MapDetailPartSummaryForSalaryModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }


        public DetailPartSummaryForSalaryModel MapDetailAutoGas1SummaryForSalaryModel(AllSummaryForSalaryModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 0),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 0),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 0),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 0),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalary()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 5 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForSalary.Add(heads);
            }
            return result;
        }

        public DetailPartSummaryForSalaryModel MapDetailAutoGas2SummaryForSalaryModel(AllSummaryForSalaryModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 0),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 0),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 0),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 0),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 2),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 2),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 2),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 2),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalary()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 2 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForSalary.Add(heads);
            }
            return result;
        }

        public DetailPartSummaryForSalaryModel MapDetailPartSummaryForSalaryModel(AllSummaryForSalaryModel models)
        {
            var totalRecord = models.Data.Any() ? (int)models.Data.First().TOTAL_COUNT : 0;
            var result = new DetailPartSummaryForSalaryModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models.Data)
            {

                var items = new DetailPartSummaryForSalaryHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    GOAL_DAY = item.GOAL_DAY,
                    GOAL_REAL = item.GOAL_REAL,
                    GOAL_SUM = item.GOAL_SUM,
                    DATA_1 = DataConverter.GetNumberFormat(item.DATA_1, 2),
                    DATA_2 = DataConverter.GetNumberFormat(item.DATA_2, 2),
                    DATA_3 = DataConverter.GetNumberFormat(item.DATA_3, 2),
                    DATA_4 = DataConverter.GetNumberFormat(item.DATA_4, 2),
                    DATA_5 = DataConverter.GetNumberFormat(item.DATA_5, 2),
                    DATA_6 = DataConverter.GetNumberFormat(item.DATA_6, 2),
                    DATA_7 = DataConverter.GetNumberFormat(item.DATA_7, 0),
                    DATA_8 = DataConverter.GetNumberFormat(item.DATA_8, 0),
                    DATA_9 = DataConverter.GetNumberFormat(item.DATA_9, 0),
                    DATA_10 = DataConverter.GetNumberFormat(item.DATA_10, 0),
                    DATA_11 = DataConverter.GetNumberFormat(item.DATA_11, 2),
                    DATA_12 = DataConverter.GetNumberFormat(item.DATA_12, 2),
                    DATA_13 = DataConverter.GetNumberFormat(item.DATA_13, 2),
                    DATA_14 = DataConverter.GetNumberFormat(item.DATA_14, 2),
                    DATA_15 = DataConverter.GetNumberFormat(item.DATA_15, 2),
                    DATA_16 = DataConverter.GetNumberFormat(item.DATA_16, 2),
                    DATA_17 = DataConverter.GetNumberFormat(item.DATA_17, 2),
                    DATA_18 = DataConverter.GetNumberFormat(item.DATA_18, 2),
                    DATA_19 = DataConverter.GetNumberFormat(item.DATA_19, 2),
                    DATA_20 = DataConverter.GetNumberFormat(item.DATA_20, 2),
                    DATA_21 = DataConverter.GetNumberFormat(item.DATA_21, 2),
                    DATA_22 = DataConverter.GetNumberFormat(item.DATA_22, 2),
                    DATA_23 = DataConverter.GetNumberFormat(item.DATA_23, 2),
                    DATA_24 = DataConverter.GetNumberFormat(item.DATA_24, 2),
                    DATA_25 = DataConverter.GetNumberFormat(item.DATA_25, 2),
                    DATA_26 = DataConverter.GetNumberFormat(item.DATA_26, 2),
                    DATA_27 = DataConverter.GetNumberFormat(item.DATA_27, 2),
                    DATA_28 = DataConverter.GetNumberFormat(item.DATA_28, 2),
                    DATA_29 = DataConverter.GetNumberFormat(item.DATA_29, 2),
                    DATA_30 = DataConverter.GetNumberFormat(item.DATA_30, 2),
                    DATA_31 = DataConverter.GetNumberFormat(item.DATA_31, 2),
                    DATA_32 = DataConverter.GetNumberFormat(item.DATA_32, 2),
                    DATA_33 = DataConverter.GetNumberFormat(item.DATA_33, 2),
                    DATA_34 = DataConverter.GetNumberFormat(item.DATA_34, 2),
                    DATA_35 = DataConverter.GetNumberFormat(item.DATA_35, 2),
                    DATA_36 = DataConverter.GetNumberFormat(item.DATA_36, 2),
                    DATA_37 = DataConverter.GetNumberFormat(item.DATA_37, 2),
                    DATA_38 = DataConverter.GetNumberFormat(item.DATA_38, 2),
                    DATA_39 = DataConverter.GetNumberFormat(item.DATA_39, 2),
                    DATA_40 = DataConverter.GetNumberFormat(item.DATA_40, 2),
                    DATA_41 = DataConverter.GetNumberFormat(item.DATA_41, 2),
                    DATA_42 = DataConverter.GetNumberFormat(item.DATA_42, 2),
                    DATA_43 = DataConverter.GetNumberFormat(item.DATA_43, 2),
                    DATA_44 = DataConverter.GetNumberFormat(item.DATA_44, 2),
                    DATA_45 = DataConverter.GetNumberFormat(item.DATA_45, 2),
                    DATA_46 = DataConverter.GetNumberFormat(item.DATA_46, 2),
                    DATA_47 = DataConverter.GetNumberFormat(item.DATA_47, 2),
                    DATA_48 = DataConverter.GetNumberFormat(item.DATA_48, 2),
                    DATA_49 = DataConverter.GetNumberFormat(item.DATA_49, 2),
                    DATA_50 = DataConverter.GetNumberFormat(item.DATA_50, 2),
                };
                result.DetailPartSummaryForSalaryHistories.Add(items);
            }

            foreach (var head in models.Header)
            {

                var heads = new HeaderPartSummaryForSalary()
                {
                    head = head.Text,

                    field = head.Value,
                    cansort = true,
                    sort = head.Value,
                    style = head.Value.Contains("DATA") ? "vertical-align:initial; text-align:right" : "vertical-align:initial; text-align:left",
                    fieldType = head.Value.Contains("DATA") ? (DataConverter.GetInteger(head.Value.Replace("DATA_", "")) < 7 ? "linkPartDetail1" : "") : "",
                    functionCall = head.Value.Contains("DATA") ? "Show" : "",
                };
                result.HeaderPartSummaryForSalary.Add(heads);
            }
            return result;
        }

        #endregion
    }
}