﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MCSAPP.WebApp.Models;
using MCSAPP.Helper.Extension;
using System.Web.Mvc.Filters;
using MCSAPP.DAL.Model;
using System.Net;
using MCSAPP.DAL.Model.Account;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using System.Threading.Tasks;
using MCSAPP.Helper;
using MCSAPP.WebFactory;
using System.Globalization;
using MCSAPP.Helper.Constant;
using MCSAPP.WebApp.Helper;

namespace MCSAPP.WebApp.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Login User by username and password. if UnAutherize return null
        /// </summary>
        /// <param name="userModel">Username password</param>
        /// <returns>UserProfile</returns>
        public async Task<UserApplicationModel> UserLogin(UserLoginModel model)
        {
            var browserName = Request.Browser.Type;
            var accountService = MCSServiceAPIFactory.NewAccountServiceWrapperInstance();
            var serviceRespond = await accountService.GetLogin(model);
            if (serviceRespond.StatusCode == HttpStatusCode.OK)
            {
                var result = WebHelper.MapUserAccountModelToUserApplicationModel(serviceRespond.Data);
                Session["UserName"] = result;
                ClearTransectionModel();
                return result;
            }
            throw new UnauthorizedAccessException(serviceRespond.Message);
        }

        /// <summary>
        /// Call when User ChangeLangue
        /// </summary>
        protected void ChangeLaungue()
        {
            //Response.Cookies.Remove("Language");
            var languageCookie = System.Web.HttpContext.Current.Request.Cookies["Language"];
            if (languageCookie == null)
            {
                languageCookie = new HttpCookie("Language");
                languageCookie.Value = WebHelper.GetCurrentCulture().Name == SystemConstant.ENCulture ? SystemConstant.THCulture : SystemConstant.ENCulture;
            }
            else
            {
                languageCookie.Value = (languageCookie.Value == SystemConstant.ENCulture) ? SystemConstant.THCulture : SystemConstant.ENCulture;
            }
            //ViewResource.Culture = new CultureInfo(languageCookie.Value);
            Response.SetCookie(languageCookie);
        }

        /// <summary>
        ///  GetCurrent UserLogin (in section)
        /// </summary>
        /// <returns>UserProfile</returns>
        public UserApplicationModel ApplicationUserProfile()
        {
            var result = (User is UserApplicationModel) ? (UserApplicationModel)User : new UserApplicationModel();
            return result;
        }


        protected void LogOut()
        {
            Session.Abandon();
            Session["UserName"] = null;
        }

        private JsonResult BehaviorJsonRespond<T>(T model)
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                throw new UnauthorizedAccessException("Session Expire");
            }
            if (Request.HttpMethod == "GET")
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(model, JsonRequestBehavior.DenyGet);
            }
        }

        /// <summary>
        /// Respon Action Service
        /// </summary>
        /// <typeparam name="T">Model (automatic new slove broken UI) </typeparam>
        /// <param name="model">model</param>
        /// <returns>new type T</returns>
        protected JsonResult OK<T>(T model, string message = "", HttpStatusCode statusCode = HttpStatusCode.OK) where T : class, new()
        {
            var dataVerifyResponds = new DataVerifyModel<T>() { Data = model, Message = message, StatusCode = statusCode };
            return BehaviorJsonRespond(dataVerifyResponds);
        }

        protected JsonResult OK<T>(string message = "", HttpStatusCode statusCode = HttpStatusCode.OK) where T : class, new()
        {
            var dataVerifyResponds = new DataVerifyModel<T>() { Data = new T(), Message = message, StatusCode = statusCode };
            return BehaviorJsonRespond(dataVerifyResponds);
        }

        protected JsonResult OK<T>(List<T> model) where T : class, new()
        {
            List<T> result = model ?? new List<T>();
            return BehaviorJsonRespond(result);
        }

        /// <summary>
        /// Respond Content
        /// </summary>
        /// <param name="content">String Content</param>
        /// <returns>HTML Content</returns>
        protected ActionResult OK(string content)
        {
            return Content(content);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorizationContext"></param>
        protected override void OnAuthorization(AuthorizationContext authorizationContext)

        {

            var userProfile = HttpContext.Session["UserName"];
            if (GlobalHelper.IsNotNull(userProfile) && (userProfile is UserApplicationModel))
            {
                var userApplication = (UserApplicationModel)userProfile;
                //var route = (System.Web.Routing.Route)RouteData.Route;
                //var urlArr = route.Url.Split('/');
                //if (urlArr.Any())
                //{
                //    if(urlArr.First().ToLower() == "admin" && userApplication.Role != "ADMIN")
                //    {
                //        authorizationContext.HttpContext.User = null;
                //        return;
                //    }
                //}

                //authorizationContext.HttpContext.User = userApplication;
                //var apiTrackingModel = new APITrackingConfigModel() { AccessId = userApplication.AccessId, UserStamp = userApplication.Username, IsLogIncomming = true, WriteTraceLog = true };
                //ServiceLoggerFactory.NewLogServiceInstance(apiTrackingModel);
            }
        }

        /// <summary>
        /// Filter menu to Layout
        /// </summary>
        /// <param name="asyncResult"></param>
        protected override void EndExecute(IAsyncResult asyncResult)
        {
            //if (Request.HttpMethod == "GET")
            //{
            //    var userMenuCollections = GetMenuFeatureFromUserApplication();
            //    ViewData["UserMenuCollections"] = userMenuCollections;
            //}
            base.EndExecute(asyncResult);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {

            base.OnResultExecuted(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            if (filterContext.Exception.GetType() == typeof(UnauthorizedAccessException) && filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                filterContext.HttpContext.Response.StatusCode = 401;



                filterContext.Result = new JsonResult()
                {
                    Data = filterContext.Exception.Message,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    ContentType = "application/json",

                };

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();

            }
            else
            {
                base.OnException(filterContext);
            }
        }



        #region Runtime Value
        protected void SetTransectionTmpModel(TransectionModel model)
        {
            if (model != null)
            {
                model.ReqCode = model.ReqCode ?? string.Empty;
                if (!string.IsNullOrWhiteSpace(model.ReqCode))
                {
                    model.SetEncodedReqCode(model.ReqCode);
                }
                Session["transection"] = model;
            }
        }

        protected TransectionModel GetTransectionTmpModel()
        {
            if (Session["transection"] != null && (Session["transection"] is TransectionModel))
            {
                return (TransectionModel)Session["transection"];
            }
            return null;
        }

        protected void ClearTransectionModel()
        {
            Session["transection"] = null;
        }

        #endregion
    }
}