﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model;
using System.Threading.Tasks;
using MCSAPP.ServiceAPIFactory.ServiceAPI;
using MCSAPP.Helper.Constant;
using MCSAPP.WebFactory;
using MCSAPP.Helper;
using MCSAPP.WebApp.Models;
using System.Reflection;
using System.ComponentModel;

namespace MCSAPP.WebApp.Controllers
{
    public class LevelController : BaseController
    {
        // GET: Level
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InsertStructure()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult InsertStructureNCR()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult InsertStructureNG()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult InsertStructurePosition()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult InsertProcess()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult InsertCert()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public ActionResult InsertCertList()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        [HttpPost]
        [Route("GetMasterLevelData")]
        public async Task<ActionResult> GetMasterLevelData(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();
     
               
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetLevel(model);
                var result = MapRequestMasterListToTodoListModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureModel MapRequestMasterListToTodoListModel(List<LevelModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new LevelHistory()
                {
                    LEVEL_ID = item.LEVEL_ID,
                    LEVEL_NAME = item.LEVEL_NAME,
                    LEVEL_GOAL = item.LEVEL_GOAL,
                    LEVEL_SALARY = DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2),
                    LEVEL_PROCEDURE = DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2),
                   // LEVEL_QUALITY = DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2),
                    SALARY = DataConverter.GetNumberFormat(item.SALARY, 2),
                    TYPE_NAME = item.TYPE_NAME,
                };
                result.LevelHistories.Add(items);
            }
            foreach (Level h in Enum.GetValues(typeof(Level)))
            {

                var heads = new HeaderLevel()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = DataConverter.GetString(h).Contains("QUALITY") 
                            || DataConverter.GetString(h).Contains("SALARY")
                            || DataConverter.GetString(h).Contains("PROCEDURE") ? "vertical-align:initial; text-align:right" : "",
                };
                result.HeaderLevel.Add(heads);


            }
            var headsCol = new HeaderLevel()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "LEVEL_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderLevel.Add(headsCol);
            return result;
        }

        [HttpPost]
        [Route("GetMasterLevelNCRData")]
        public async Task<ActionResult> GetMasterLevelNCRData(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetLevelNCR(model);
                var result = MapLevelNCRtModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureNCRModel MapLevelNCRtModel(List<LevelNCRModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureNCRModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new LevelNCRHistory()
                {
                    LEVEL_ID = item.LEVEL_ID,
                
                    LEVEL_GOAL = item.LEVEL_GOAL,
                    MAIN_MIN = DataConverter.GetNumberFormat(item.MAIN_MIN, 2),
                    MAIN_MAX = DataConverter.GetNumberFormat(item.MAIN_MAX, 2),
                    TEMPO_MIN = DataConverter.GetNumberFormat(item.TEMPO_MIN, 2),
                    TEMPO_MAX = DataConverter.GetNumberFormat(item.TEMPO_MAX, 2),
                    PRICE_QULITY = DataConverter.GetNumberFormat(item.PRICE_QULITY, 2),
                    TYPE_NAME = item.TYPE_NAME,
                };
                result.LevelNCRHistories.Add(items);
            }
            foreach (LevelNCR h in Enum.GetValues(typeof(LevelNCR)))
            {

                var heads = new HeaderLevelNCR()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = DataConverter.GetString(h).Contains("MAIN")
                            || DataConverter.GetString(h).Contains("TEMPO")
                            || DataConverter.GetString(h).Contains("PRICE") ? "vertical-align:initial; text-align:right" : "",
                };
                result.HeaderLevelNCR.Add(heads);


            }
            var headsCol = new HeaderLevelNCR()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "LEVEL_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderLevelNCR.Add(headsCol);
            return result;
        }

        [HttpPost]
        [Route("GetMasterLevelNGData")]
        public async Task<ActionResult> GetMasterLevelNGData(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetLevelNG(model);
                var result = MapLevelNGModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureNGModel MapLevelNGModel(List<LevelNGModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureNGModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new LevelNGHistory()
                {
                    LEVEL_ID = item.LEVEL_ID,

                    LEVEL_GOAL = item.LEVEL_GOAL,
                    NG_MIN = DataConverter.GetNumberFormat(item.NG_MIN, 2),
                    NG_MAX = DataConverter.GetNumberFormat(item.NG_MAX, 2),
                    QUALITY_DESC = DataConverter.GetString(item.QUALITY_DESC),
                    PROCEDURE_DESC = DataConverter.GetString(item.PROCEDURE_DESC),
                    PRICE_UT = DataConverter.GetNumberFormat(item.PRICE_UT, 2),
                    PRICE_QUALITY = DataConverter.GetNumberFormat(item.PRICE_QUALITY, 2),
                    TYPE_NAME = item.TYPE_NAME,
                };
                result.LevelNGHistories.Add(items);
            }
            foreach (LevelNG h in Enum.GetValues(typeof(LevelNG)))
            {

                var heads = new HeaderLevelNG()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = DataConverter.GetString(h).Contains("NG")
                            || DataConverter.GetString(h).Contains("PRICE") ? "vertical-align:initial; text-align:right" : "",
                };
                result.HeaderLevelNG.Add(heads);


            }
            var headsCol = new HeaderLevelNG()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "LEVEL_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderLevelNG.Add(headsCol);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetMasterLevelData(LevelModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterLevel(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> SetMasterLevelNCRData(LevelNCRModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterLevelNCR(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        public async Task<ActionResult> SetMasterLevelNGData(LevelNGModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterLevelNG(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterLevelData(LevelModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterLevel(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterLevelNCRData(LevelNCRModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterLevelNCR(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        public async Task<ActionResult> DeleteMasterLevelNGData(LevelNGModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterLevelNG(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        [HttpPost]
        [Route("GetMasterLevelGroupData")]
        public async Task<ActionResult> GetMasterLevelGroupData(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetMasterLevelGroup(model);
                var result = MapMasterLevelGroupDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureGroupModel MapMasterLevelGroupDataModel(List<LevelGroupModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureGroupModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new LevelGroupHistory()
                {
                    LEVEL_ID = item.LEVEL_ID,
                    GOAL_DAY_GROUP= item.GOAL_DAY_GROUP,
                    TYPE_NAME = item.TYPE_NAME,
                    MIN_SALARY = DataConverter.GetNumberFormat(item.MIN_SALARY, 2),
                    LEVEL_SALARY = DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2),
                  
                    LEVEL_PROCEDURE = DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2),
                   
                    LEVEL_QUALITY = DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2),
                  

                };
                result.LevelGroupHistories.Add(items);
            }
            foreach (LevelGroup h in Enum.GetValues(typeof(LevelGroup)))
            {

                var heads = new HeaderLevelGroup()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = false,
                    sort = DataConverter.GetString(h),
                    style = DataConverter.GetString(h).Contains("LEVEL")
                         
                            || DataConverter.GetString(h).Contains("SALARY") ? "vertical-align:initial; text-align:right" : "",
                };
                result.HeaderLevelGroup.Add(heads);


            }
            var headsCol = new HeaderLevelGroup()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "LEVEL_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderLevelGroup.Add(headsCol);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetMasterGroupData(LevelGroupModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterGroup(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterGroupData(LevelGroupModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterGroup(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetMasterProcessData")]
        public async Task<ActionResult> GetMasterProcessData(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetMasterProcess(model);
                var result = MapMasterProcessDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureProcessModel MapMasterProcessDataModel(List<ProcessModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureProcessModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new ProcessHistory()
                {
                    PROCESS_ID = item.PROCESS_ID,
                    PROCESS_NAME = item.PROCESS_NAME,
                    PRICE_UNIT = item.PRICE_UNIT,
                    MAIN_PROCESS_NAME =item.MAIN_PROCESS_NAME,

                };
                result.ProcessHistories.Add(items);
            }
           
            foreach (Process h in Enum.GetValues(typeof(Process)))
            {

                var heads = new HeaderProcess()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = DataConverter.GetString(h).Contains("PRICE") ? "vertical-align:initial;text-align:right":"",
                };
                result.HeaderProcess.Add(heads);


            }
            var headsCol = new HeaderProcess()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "PROCESS_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderProcess.Add(headsCol);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetMasterProcessData(ProcessModel model)
        {

            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterProcess(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);
  
        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterProcessData(ProcessModel model)
        {

            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterProcess(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetMasterCertData")]
        public async Task<ActionResult> GetMasterCertData()
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetMasterCert();
                var result = MapMasterCertDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureCertModel MapMasterCertDataModel(List<CertModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureCertModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new CertHistory()
                {
                    CERT_ID = item.CERT_ID,
                    CERT_NAME = item.CERT_NAME,
                    CERT_SHORT_NAME = item.CERT_SHORT_NAME,

                };
                result.CertHistories.Add(items);
            }
            foreach (Cert h in Enum.GetValues(typeof(Cert)))
            {

                var heads = new HeaderCert()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = false,
                    sort = DataConverter.GetString(h),
                    style = h.ToString().Contains("ID") ? "vertical-align:initial;text-align:center;width:150px" : "vertical-align:initial;text-align:left;width:450px",
                };
                result.HeaderCert.Add(heads);


            }
            var headsCol = new HeaderCert()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "CERT_ID",
              
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center;width:50px",
                functionCall = "ShowDelete",
            };
            result.HeaderCert.Add(headsCol);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetMasterCertData(CertModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterCert(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterCertData(CertModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterCert(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        [Route("GetMasterCertListData")]
        public async Task<ActionResult> GetMasterCertListData()
        {
            if (ModelState.IsValid)
            {
                //var targetUser = ApplicationUserProfile();


                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetMasterCertList();
                var result = MapMasterCertListDataModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }
        public StructureCertListModel MapMasterCertListDataModel(List<CertListModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureCertListModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new CertListHistory()
                {
                    EMP_CODE = item.EMP_CODE,
                    FULL_NAME = item.FULL_NAME,
                    DEPT_NAME = item.DEPT_NAME,
                    CERT_NAME = item.CERT_NAME,
                    CERT_START = item.CERT_START,
                    CERT_END = item.CERT_END,

                };
                result.CertListHistories.Add(items);
            }
            foreach (CertList h in Enum.GetValues(typeof(CertList)))
            {

                var heads = new HeaderCertList()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = false,
                    sort = DataConverter.GetString(h),
                    style = "{ 'width' : '20px'} ",
                };
                result.HeaderCertList.Add(heads);


            }
            return result;
        }


        [HttpPost]
        [Route("GetProcessList")]
        public async Task<ActionResult> GetProcessList()
        {
            SearchModel model = new SearchModel();

            var result = await MCSServiceAPIFactory.NewLevelServiceWrapperInstance().GetMasterProcess(model);
            return OK(result);
        }

        [HttpPost]
        [Route("GetPriceByProcess")]
        public async Task<ActionResult> GetPriceByProcess(ProcessModel model)
        {
            var result = await MCSServiceAPIFactory.NewLevelServiceWrapperInstance().GetPriceByProcess(model);
            return OK(result);
        }


        [HttpPost]
        public async Task<ActionResult> ExportInsertStucture(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var username = User.Identity.Name;
                
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetLevel(model);
                var result = MapRequestMasterListToTodoListModel(serviceResult);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }


        public enum Level
        {
            //[Description("ประเภท")]
            //LEVEL_TYPE = 0,
            [Description("ประเภท")]
            TYPE_NAME = 1,
            [Description("Level ID")]
            LEVEL_ID = 2,
         
            [Description("Level Name")]
            LEVEL_NAME = 3,

            [Description("เป้าหมายเฉลี่ย/วัน")]
            LEVEL_GOAL = 4,

            [Description("ราคาบาท/psc(เป้าหมาย)")]
            LEVEL_SALARY = 5,

            //[Description("ราคาบาท/psc(คุณภาพ)")]
            //LEVEL_QUALITY = 6,
            [Description("ราคาบาท/psc(ขั้นตอนการทำงาน)")]
            LEVEL_PROCEDURE = 7,
            [Description("ฐานเงินเดือน")]
            SALARY = 8,
        }

        public enum LevelNCR
        {
            //[Description("ประเภท")]
            //LEVEL_TYPE = 0,
            [Description("ประเภท")]
            TYPE_NAME = 1,

            //[Description("Level ID")]
            //LEVEL_ID = 2,

            [Description("เป้าหมาย")]
            LEVEL_GOAL = 3,

            [Description("%MAIN ตำสุด")]
            MAIN_MIN = 4,

            [Description("%MAIN สูงสุด")]
            MAIN_MAX = 5,

            [Description("%TEMPO ตำสุด")]
            TEMPO_MIN = 6,

            [Description("%TEMPO สูงสุด")]
            TEMPO_MAX = 7,

            [Description("ราคาบาท/psc(ขั้นตอนคุณถาพ)")]
            PRICE_QULITY = 8,

            //[Description("ฐานเงินเดือน")]
            //SALARY = 8,
        }
        public enum LevelNG
        {
           
            [Description("ประเภท")]
            TYPE_NAME = 1,

            [Description("เป้าหมาย")]
            LEVEL_GOAL = 3,

            [Description("%NG ตำสุด")]
            NG_MIN = 4,

            [Description("%NG สูงสุด")]
            NG_MAX = 5,

            [Description("ราคาบาท/psc(ขั้นตอนคุณถาพ)")]
            PRICE_QUALITY = 6,

            [Description("ราคาบาท/psc(มาตรฐาน)")]
            PRICE_UT = 7,


            [Description("คุณภาพ")]
            QUALITY_DESC = 8,

            [Description("ขั้นตอน")]
            PROCEDURE_DESC = 9,

            //[Description("ฐานเงินเดือน")]
            //SALARY = 8,
        }
        public enum LevelGroup
        {
            //[Description("ประเภท")]
            //LEVEL_TYPE = 0,

            [Description("Level ID")]
            LEVEL_ID = 1,

            [Description("เป้าหมายรวมทั้งกรุ๊ป")]
            GOAL_DAY_GROUP = 3,

           

            [Description("ราคาบาท/psc(เป้าหมาย)")]
            LEVEL_SALARY = 5,

            [Description("ราคาบาท/psc(คุณภาพ)")]
            LEVEL_QUALITY = 6,
            [Description("ราคาบาท/psc(ขั้นตอนการทำงาน)")]
            LEVEL_PROCEDURE = 7,
            [Description("เงินเดือนขั้นต่ำ")]
            MIN_SALARY = 8,
        }

        public enum Process
        {
            [Description("โปรเซสหลัก")]
            MAIN_PROCESS_NAME = 0,

            [Description("รหัสโปรเซส")]
            PROCESS_ID = 1,

            [Description("ชื่อโปรเซส")]
            PROCESS_NAME = 2,

            [Description("ราคาต่อหน่วย")]
            PRICE_UNIT = 3,

        }

        public enum Cert
        {

            [Description("รหัส")]
            CERT_ID = 1,

            [Description("ชื่อ")]
            CERT_NAME = 2,

            [Description("ชื่อย่อ")]
            CERT_SHORT_NAME = 3,

        }

        public enum CertList
        {

            [Description("รหัส")]
            EMP_CODE = 1,

            [Description("ชื่อ - นามสกุล")]
            FULL_NAME = 2,

            //[Description("รหัสกรุ๊ป")]
            //DEPT_CODE= 3,

            [Description("ชื่อกรุ๊ป")]
            DEPT_NAME = 4,

            [Description("ชื่อ Certificate")]
            CERT_NAME = 5,

            [Description("วันที่ได้รับ Certificate")]
            CERT_START = 6,

            [Description("วันหมดอายุ Certificate")]
            CERT_END = 7,

        }

        private string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        #region Built Beam/Box by chaiwud.ta
        // LevelBuilt
        // 2018-02-10 by chaiwud.ta
        public ActionResult InsertStructureBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }

        public ActionResult InsertStructurePositionBuilt()
        {
            var userProfile = Session["UserName"];
            if (userProfile == null)
            {
                return RedirectToAction("../Session");
            }
            return View();
        }
        public enum LevelBuilt
        {
            //[Description("ประเภท")]
            //LEVEL_TYPE = 0,
            [Description("ประเภท")]
            TYPE_NAME = 1,
            [Description("Level ID")]
            LEVEL_ID = 2,

            [Description("Level Name")]
            LEVEL_NAME = 3,

            [Description("เป้าหมายเฉลี่ย/วัน")]
            LEVEL_GOAL = 4,

            [Description("ราคาบาท/psc(เป้าหมาย)")]
            LEVEL_SALARY = 5,

            [Description("ราคาบาท/psc(คุณภาพ)")]
            LEVEL_QUALITY = 6,
            [Description("ราคาบาท/psc(ขั้นตอนการทำงาน)")]
            LEVEL_PROCEDURE = 7,
            [Description("ฐานเงินเดือน")]
            SALARY = 8,
        }

        [HttpPost]
        [Route("GetMasterLevelDataBuilt")]
        public async Task<ActionResult> GetMasterLevelDataBuilt(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var serviceResult = await service.GetBuiltLevel(model);
                var result = MapRequestMasterListToTodoListBuiltModel(serviceResult);
                return OK(result);
            }
            return OK("");
        }

        public StructureBuiltModel MapRequestMasterListToTodoListBuiltModel(List<LevelBuiltModel> models)
        {
            var totalRecord = models.Any() ? (int)models.First().TOTAL_COUNT : 0;
            var result = new StructureBuiltModel()
            {
                RowTotal = totalRecord,
                PageTotal = (int)Decimal.Ceiling(((decimal)totalRecord / 10))
            };
            foreach (var item in models)
            {
                var items = new LevelBuiltHistory()
                {
                    LEVEL_ID = item.LEVEL_ID,
                    LEVEL_NAME = item.LEVEL_NAME,
                    LEVEL_GOAL = DataConverter.GetNumberFormat(item.LEVEL_GOAL, 2),
                    LEVEL_SALARY = DataConverter.GetNumberFormat(item.LEVEL_SALARY, 2),
                    LEVEL_QUALITY = DataConverter.GetNumberFormat(item.LEVEL_QUALITY, 2),
                    LEVEL_PROCEDURE = DataConverter.GetNumberFormat(item.LEVEL_PROCEDURE, 2),
                    SALARY = DataConverter.GetNumberFormat(item.SALARY, 2),
                    TYPE_NAME = item.TYPE_NAME,
                };
                result.LevelBuiltHistories.Add(items);
            }
            foreach (LevelBuilt h in Enum.GetValues(typeof(LevelBuilt)))
            {

                var heads = new HeaderBuiltLevel()
                {
                    head = GetEnumDescription(h),
                    field = DataConverter.GetString(h),
                    cansort = true,
                    sort = DataConverter.GetString(h),
                    style = DataConverter.GetString(h).Contains("QUALITY")
                            || DataConverter.GetString(h).Contains("SALARY")
                            || DataConverter.GetString(h).Contains("PROCEDURE") ? "vertical-align:initial; text-align:right" : "",
                };
                result.HeaderBuiltLevel.Add(heads);
            }
            var headsCol = new HeaderBuiltLevel()
            {
                head = "DELETE",
                //  fieldtemplate = "function (record) { var id = record.data.USERNAME;var elementId = 'admin_' + id; record.cell.innerHTML = '<div class='g-check'><input class='admin-ck' id=' + elementId + ' type='checkbox name='checkbox' value=' + id + '><label for=' + elementId + '><span><span class='collapse'>|</span></span></label></div>'return record; ",
                fieldType = "delete",
                field = "LEVEL_ID",
                cansort = false,
                sort = string.Empty,
                style = "vertical-align:initial;text-align:center",
                functionCall = "ShowDelete",
            };
            result.HeaderBuiltLevel.Add(headsCol);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult> SetMasterLevelDataBuilt(LevelBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.SetMasterBuiltLevel(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }

        [HttpPost]
        public async Task<ActionResult> DeleteMasterLevelDataBuilt(LevelBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                UserApplicationModel user = (UserApplicationModel)Session["UserName"];
                model.USER_UPDATE = user.UserModel.EMP_CODE;
                var service = MCSServiceAPIFactory.NewLevelServiceWrapperInstance();
                var result = await service.DeleteMasterBuiltLevel(model);
                return OK(result);
            }
            var allErrors = ModelState.Values.SelectMany(v => v.Errors).FirstOrDefault();
            var errorString = allErrors != null ? allErrors.ErrorMessage : "";
            //var errorString = string.Join(",", allErrors.Select(x => x.ErrorMessage));
            return OK(new ResultModel() { MESSAGE = $"{errorString}", CODE = "99" }, $"{errorString}", HttpStatusCode.ExpectationFailed);

        }
        #endregion
    }
}