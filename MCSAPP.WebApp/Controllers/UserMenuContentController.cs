﻿using MCSAPP.DAL.Model;
using MCSAPP.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace MCSAPP.WebApp.Controllers
{
    public class UserMenuContentController : Controller
    {
        // GET: UserMenuContent
        [Route("GenerateUserMenu")]
        [HttpGet]
        public JsonResult GenerateUserMenu()//แก้ request ก่อน manager approve
        {
            if (HttpContext.Session["UserName"] != null)
            {
                try
                {
                    var userProfile_ = HttpContext.Session["UserName"];
                    var userProfile = (UserApplicationModel)userProfile_;
                    UserMenuModel userProfileMenu = new UserMenuModel();

                    var model = GetMenuFeatureFromUserApplication();
                    userProfileMenu.UserFullName = userProfile.UserModel.FULL_NAME;
                    userProfileMenu.MenuFeatureModels = model;
                    userProfileMenu.UserUpdate = userProfile.UserModel.EMP_CODE;

                    var dataVerifyResponds = new DataVerifyModel<UserMenuModel>() { Data = userProfileMenu, Message = "", StatusCode = System.Net.HttpStatusCode.OK };
                    if (Request.HttpMethod == "GET")
                    {
                        return Json(dataVerifyResponds, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(dataVerifyResponds, JsonRequestBehavior.DenyGet);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("userProfile is null");
                }

            }
            else
            {
                throw new Exception("userProfile is null");
            }



        }
        public List<MenuFeatureModel> GetMenuFeatureFromUserApplication()
        {
            var userProfile_ = HttpContext.Session["UserName"];
            var result = new List<MenuFeatureModel>();
            if (userProfile_ is UserApplicationModel)
            {
                var userProfile = (UserApplicationModel)userProfile_;
                var listTargetMenu = GetAllmenuFeature();
                // var allUserRoles = userProfile.Role.Split(';').ToList();
                // result = listTargetMenu.Where(x => userProfile.Role.Contains(x.RequrieRole) || (allUserRoles.Contains(x.RequrieRole) || x.RequrieRole == string.Empty) || x.RequrieUsername.Contains(userProfile.Role)).ToList();
                result = listTargetMenu;
            }
            return result;
        }

        /// <summary>
        /// Seed Menu (from Database or Fixed)
        /// </summary>
        /// <returns>MenuModel for render in Patial view Layout</returns>
        public List<MenuFeatureModel> GetAllmenuFeature()
        {
            var Host = ConfigurationManager.AppSettings["WebHost"].ToString();
            var result = new List<MenuFeatureModel>() {
                new MenuFeatureModel() { MenuName = "รายงานสรุป (FAB)", ActionName="", Link = "", Sequence = 1, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปรายวัน", ActionName="../"+Host+"Report" ,Link = "ReportSummaryWorkDayForWeek", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน", ActionName="../"+Host+"Report",Link = "ReportSummarySalaryDayForWeek", Sequence = 2 },
                } },
                 new MenuFeatureModel() { MenuName = "รายงานสรุป (WELD)", ActionName="", Link = "", Sequence = 1, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                       new MenuItemFeatureModel() { MenuName = "สรุปรายวัน", ActionName="../"+Host+"Report" ,Link = "ReportSummaryWorkDayForWeekWeld", Sequence = 1 },
                       new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน", ActionName="../"+Host+"Report",Link = "ReportSummarySalaryForWeekWeld", Sequence = 2 },
                } },
                new MenuFeatureModel() { MenuName = "รายงานสรุปผลงาน (PART)", ActionName="", Link = "", Sequence = 1, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (AUTOGAS1)", ActionName="../"+Host+"SummaryWork" ,Link = "ReportSummaryForWorkByAutoGas1", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (AUTOGAS2)", ActionName="../"+Host+"SummaryWork" ,Link = "ReportSummaryForWorkByAutoGas2", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (AUTOGAS3)", ActionName="../"+Host+"SummaryWork" ,Link = "ReportSummaryForWorkByAutoGas3", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (TAPER)", ActionName="../"+Host+"SummaryWork" ,Link = "ReportSummaryForWorkByTaper", Sequence = 4 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (PART)", ActionName="../"+Host+"SummaryWork" ,Link = "ReportSummaryForWorkByPart", Sequence = 5 },
                } },
                new MenuFeatureModel() { MenuName = "รายงานสรุปเงินเดือน (PART)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (AUTOGAS1)", ActionName="../"+Host+"SummarySalary" ,Link = "ReportSummaryForSalaryByAutoGas1", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (AUTOGAS2)", ActionName="../"+Host+"SummarySalary" ,Link = "ReportSummaryForSalaryByAutoGas2", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (AUTOGAS3)", ActionName="../"+Host+"SummarySalary" ,Link = "ReportSummaryForSalaryByAutoGas3", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (TAPER)", ActionName="../"+Host+"SummarySalary" ,Link = "ReportSummaryForSalaryByTaper", Sequence = 4 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (PART)", ActionName="../"+Host+"SummarySalary" ,Link = "ReportSummaryForSalaryByPart", Sequence = 5 },
                } },
                 new MenuFeatureModel() { MenuName = "รายงานสรุปผลงาน (Built Beam)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Built Up Beam)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByBuiltUpBeam", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Saw)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkBySawBeam", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Adjust)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkAdjustBeam", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Cut Drill & Shot Blast)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByDrillShotBlBeam", Sequence = 4 },
                } },
                 new MenuFeatureModel() { MenuName = "รายงานสรุปเงินเดือน (Built Beam)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Built Up Beam)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByBuiltUpBeam", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Saw)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryBySawBeam", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Adjust)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryAdjustBeam", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Cut Drill & Shot Blast)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByDrillShotBlBeam", Sequence = 4 },
                } },
                  new MenuFeatureModel() { MenuName = "รายงานสรุปผลงาน (Built Box)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Fab Diaphragh Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByFabDiaphragmBox", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Built Up Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByBuiltUpBox", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Auto Root Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByAutoRootBox", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Drill & Sesnet Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByDrillSesnetBox", Sequence = 4 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Gouging & Repair Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByGougingRepairBox", Sequence = 5 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Adjust Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkOtherBox", Sequence = 6 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Facing Box)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkByFacingBox", Sequence = 7 },
                } },
                 new MenuFeatureModel() { MenuName = "รายงานสรุปเงินเดือน (Built Box)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Fab Diaphragh Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByFabDiaphragmBox", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Built Up Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByBuiltUpBox", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Auto Root Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByAutoRootBox", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Drill & Sesnet Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByDrillSesnetBox", Sequence = 4 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Gouging & Repair Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByGougingRepairBox", Sequence = 5 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Adjust Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryOtherBox", Sequence = 6 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Facing Box)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryByFacingBox", Sequence = 7 },
                } },
                #region OTher Beam/Box
                     new MenuFeatureModel() { MenuName = "รายงานสรุปผลงาน (Other)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Other Adjust Beam)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkOtherBeam", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผลงาน (Other Finishing)", ActionName="../"+Host+"SummaryWorkBuilt" ,Link = "ReportSummaryForWorkOtherFinishing", Sequence = 2 },
                    } },
                     new MenuFeatureModel() { MenuName = "รายงานสรุปเงินเดือน (Other)", ActionName="", Link = "", Sequence = 2, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                     {
                            new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Other Adjust Beam)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryOtherBeam", Sequence = 1 },
                            new MenuItemFeatureModel() { MenuName = "สรุปเงินเดือน (Other Fishing)", ActionName="../"+Host+"SummarySalaryBuilt" ,Link = "ReportSummaryForSalaryOtherFinishing", Sequence = 2 },
                     } },
	            #endregion
                new MenuFeatureModel() { MenuName = "รายงานสรุปความผิดพลาด", ActionName="", Link = "", Sequence = 3, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "แเจ้งเตือนเป้าหมาย (ช่างประกอบ)", ActionName="../"+Host+"Report" ,Link = "ReportWorkLessGoal", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "แเจ้งเตือนเป้าหมาย (ช่างเชื่อม)", ActionName="../"+Host+"Report" ,Link = "ReportWorkLessWeldGoal", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผล NCR", ActionName="../"+Host+"Report",Link = "ReportNCR", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผล VT", ActionName="../"+Host+"Report" ,Link = "ReportVT", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "สรุปผล NG", ActionName="../"+Host+"Report" ,Link = "ReportNG", Sequence = 4 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินพนักงานเจียร์", ActionName="../"+Host+"ShowReport" ,Link = "ReportSummaryForGrinder", Sequence = 5 },
                        new MenuItemFeatureModel() { MenuName = "สรุปเงินตัดเงิน", ActionName="../"+Host+"ShowReport" ,Link = "ReportSummaryForCut", Sequence = 6 },

                 } },
                new MenuFeatureModel() { MenuName = "สำหรับเจ้าหน้าที่", ActionName="ADMIN", Link = "", Sequence = 4, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName =  "โครงสร้างเงินเดือน", ActionName="../"+Host+"Level" ,Link = "InsertStructure", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName =  "โครงสร้างเปอร์เซนต์ราคาคุณภาพ (NCR)", ActionName="../"+Host+"Level" ,Link = "InsertStructureNCR", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName =  "โครงสร้างเปอร์เซนต์ราคาคุณภาพ (NG)", ActionName="../"+Host+"Level" ,Link = "InsertStructureNG", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName =  "โครงสร้างค่าตำแหน่ง", ActionName="../"+Host+"Level",Link = "InsertStructurePosition", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName =  "ปรับเป้าหมายพนักงาน (ประกอบ)", ActionName="../"+Host+"Admin",Link = "GoalManagement", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName =  "ปรับเป้าหมายพนักงาน (เชื่อม)", ActionName="../"+Host+"Admin",Link = "GoalWeldManagement", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก/แก้ไข NCR", ActionName="../"+Host+"Admin",Link = "NCRManagement", Sequence = 5 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึกหักเงินเดือน", ActionName="../"+Host+"Admin" ,Link = "MinusManagement", Sequence = 6 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึกการตรวจ VT", ActionName="../"+Host+"Admin",Link = "CheckVTManagement", Sequence = 7 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Week การทำงาน", ActionName="../"+Host+"Upload" ,Link = "WeekUploadManagement", Sequence = 8 },
                } },
                new MenuFeatureModel() { MenuName = "สำหรับเจ้าหน้าที่ (อัพโหลด)", ActionName="ADMIN", Link = "", Sequence = 4, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload VT", ActionName="../"+Host+"Upload",Link = "VTUploadManagement", Sequence = 9 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload NCR", ActionName="../"+Host+"Upload" ,Link = "NCRUploadManagement", Sequence = 10 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload เงินเพิ่มเติม", ActionName="../"+Host+"Upload" ,Link = "OtherUploadManagement", Sequence = 11 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload Design Change", ActionName="../"+Host+"Upload",Link = "DesignUploadManagement", Sequence = 12 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload NG", ActionName="../"+Host+"Upload" ,Link = "NGUploadManagement", Sequence = 13},
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload NG (NG+2)", ActionName="../"+Host+"Upload" ,Link = "NGUploadNewManagement", Sequence = 13},
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload ให้เชื่อม UT", ActionName="../"+Host+"Upload" ,Link = "ActUTUploadNewManagement", Sequence = 13},
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload หักเงิน Finising", ActionName="../"+Host+"Upload" ,Link = "CutFinisingUploadManagement", Sequence = 13},
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload เป้าหมาย", ActionName="../"+Host+"Upload" ,Link = "GoalUploadManagement", Sequence = 13},
                } },
                    new MenuFeatureModel() { MenuName = "สำหรับเจ้าหน้าที่ (ประวัติ)", ActionName="ADMIN", Link = "", Sequence = 4, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ ปรับเป้าหมายพนักงาน", ActionName="../"+Host+"Admin" ,Link = "AdjustGoalHistory", Sequence = 4 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ หักเงินเดือน", ActionName="../"+Host+"Admin" ,Link = "MinusHistory", Sequence = 6 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ บันทึกการตรวจ VT", ActionName="../"+Host+"Admin",Link = "VTHistory", Sequence = 7 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ Week การทำงาน", ActionName="../"+Host+"Admin" ,Link = "WeekHistory", Sequence = 8 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ เงินเพิ่มเติม", ActionName="../"+Host+"Admin" ,Link = "OtherHistory", Sequence = 11 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ Design Change", ActionName="../"+Host+"Admin",Link = "DesignChangeHistory", Sequence = 12 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ บันทึก %NG", ActionName="../"+Host+"Admin",Link = "NGManagement", Sequence = 13 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ บันทึก %NG( NG+2)", ActionName="../"+Host+"Admin",Link = "NGNewManagement", Sequence = 13 },
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ ให้เชื่อม UT", ActionName="../"+Host+"Admin" ,Link = "ActUTNewManagement", Sequence = 13},
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ หักเงิน Finising", ActionName="../"+Host+"Admin" ,Link = "CutFinisingManagement", Sequence = 13},
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ เป้าหมาย", ActionName="../"+Host+"Admin" ,Link = "GoalUploadManagement", Sequence = 13},

                } },
                new MenuFeatureModel() { MenuName = "สำหรับเจ้าหน้าที่ (PART)", ActionName="", Link = "", Sequence = 4, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName = "ข้อมูล End Tab", ActionName="../"+Host+"Admin" ,Link = "EndTabManagement", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName = "อัพโหลดข้อมูล End Tab", ActionName="../"+Host+"Admin" ,Link = "UploadEndTabManagement", Sequence = 2},
                        new MenuItemFeatureModel() { MenuName = "ข้อมูลตัดเงิน Part", ActionName="../"+Host+"Admin" ,Link = "CutPartManagement", Sequence = 3 },
                        new MenuItemFeatureModel() { MenuName = "อัพโหลดข้อมูลตัดเงิน Part", ActionName="../"+Host+"Admin" ,Link = "UploadCutPartManagement", Sequence = 4},
                       // new MenuItemFeatureModel() { MenuName = "บันทึก Process ย่อย", ActionName="../"+Host+"Admin" ,Link = "ProcessTypeManagement", Sequence = 5},
                        new MenuItemFeatureModel() { MenuName = "Check Symbol", ActionName="../"+Host+"Admin" ,Link = "CheckSymbolManagement", Sequence = 6},
                        new MenuItemFeatureModel() { MenuName = "ข้อมูล Check Symbol", ActionName="../"+Host+"Admin" ,Link = "SymbolManagement", Sequence = 7},
                } },
                 new MenuFeatureModel() { MenuName = "สำหรับเจ้าหน้าที่ (BUILT)", ActionName="ADMIN", Link = "", Sequence = 4, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName =  "โครงสร้างเงินเดือน (Built)", ActionName="../"+Host+"Level" ,Link = "InsertStructureBuilt", Sequence = 1 },
                        new MenuItemFeatureModel() { MenuName =  "โครงสร้างค่าตำแหน่ง", ActionName="../"+Host+"Level",Link = "InsertStructurePositionBuilt", Sequence = 2 },
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload เป้าหมาย (Built)", ActionName="../"+Host+"Upload" ,Link = "GoalUploadManagementBuilt", Sequence = 3},
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ เป้าหมาย (Built)", ActionName="../"+Host+"Admin" ,Link = "GoalUploadManagementBuilt", Sequence = 4},
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload %UT Data By User (Built)", ActionName="../"+Host+"Upload" ,Link = "UTDataByUserUploadBuilt", Sequence = 5},
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ %UT Data By User (Built)", ActionName="../"+Host+"Admin" ,Link = "UTDataByUserUploadBuilt", Sequence = 6},
                        //new MenuItemFeatureModel() { MenuName =  "บันทึก Upload NG Data (Built)", ActionName="../"+Host+"Upload" ,Link = "NGDataUploadBuilt", Sequence = 7},
                        //new MenuItemFeatureModel() { MenuName =  "ประวัติ NG Data (Built)", ActionName="../"+Host+"Admin" ,Link = "NGDataUploadBuilt", Sequence = 8},
                        //new MenuItemFeatureModel() { MenuName =  "บันทึก Upload QA Check UT Repair (Built)", ActionName="../"+Host+"Upload" ,Link = "RepairCheckUploadManagementBuilt", Sequence = 13},
                        //new MenuItemFeatureModel() { MenuName =  "ประวัติ QA Check UT Repair (Built)", ActionName="../"+Host+"Admin" ,Link = "RepairCheckUploadManagementBuilt", Sequence = 13},

                } },
                    new MenuFeatureModel() { MenuName = "สำหรับเจ้าหน้าที่ (OTHER)", ActionName="ADMIN", Link = "", Sequence = 4, MenuItemFeatureModels = new List<MenuItemFeatureModel>()
                    {
                        new MenuItemFeatureModel() { MenuName =  "บันทึก Upload Camber (Other)", ActionName="../"+Host+"Upload" ,Link = "CamberDataUploadOther", Sequence = 1},
                        new MenuItemFeatureModel() { MenuName =  "ประวัติ Camber Data (Other)", ActionName="../"+Host+"Admin" ,Link = "CamberDataUploadOther", Sequence = 2},
                } },

            };
            return result;
        }
    }
}