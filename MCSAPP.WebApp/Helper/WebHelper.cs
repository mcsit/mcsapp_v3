﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using MCSAPP.ServiceGateway;
using MCSAPP.DAL.Data;
using MCSAPP.DAL.Model.Account;
using MCSAPP.Helper.Constant;
using MCSAPP.Security.Cryptographic;
using MCSAPP.WebApp.Models;

namespace  MCSAPP.WebApp.Helper
{
    public class WebHelper
    {
        public static UserApplicationModel MapUserAccountModelToUserApplicationModel(UserAccountModel model)
        {
            var userApplicationModel = new UserApplicationModel(model.Username);
            userApplicationModel.AccessId = model.AccessId;
            userApplicationModel.Role = model.Role;
            userApplicationModel.Username = model.Username;
            userApplicationModel.UserModel = model.UserProfile;

            userApplicationModel.APITrackingConfig = ConfigurationToken.DecodeConfigurationToken(model.TrackingToken);
            return userApplicationModel;
        }

        public static string GetModelErrorToString(ModelStateDictionary model)
        {
            var modelError = model.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage));
            var errorMessage = string.Join(",", modelError);
            return errorMessage;
        }

        public static Func<CultureInfo> GetCurrentCulture = () =>
        {
            return Thread.CurrentThread.CurrentCulture;
        };

        public static string EncodeRequestCode(string reqCode)
        {
            var endedReqCode = InternalCryptographic.EncryptData(string.Concat(Guid.NewGuid(), reqCode));
            return endedReqCode;
        }

        public static string DecodeRequestCode(string endedReqCode)
        {
            var reqDecode = InternalCryptographic.DecryptData(endedReqCode);
            var reqCode = reqDecode.Substring(36, reqDecode.Length - 36);
            return reqCode;
        }
    }
}