﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;

namespace MCSAPP.WebApp.Helper
{
    public class APIAuthorizeFilter : ClientAuthorizeFilter
    {
        public APIAuthorizeFilter()
        { }
        public APIAuthorizeFilter(bool isuser)
            : base(isuser)
        {

        }
        protected override bool OnAuthorizeClient(string user, string pass, HttpActionContext filterContext)
        {
            return base.OnAuthorizeClient(user, pass, filterContext);
        }
    }
}