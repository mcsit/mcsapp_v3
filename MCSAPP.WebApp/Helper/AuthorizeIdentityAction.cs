﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Routing;
using MCSAPP.Helper.Enum;

namespace MCSAPP.WebApp.Helper
{
    public class AuthorizeIdentityActionFilter : System.Web.Mvc.ActionFilterAttribute
    {
        protected AllowAuthorize authorizeAllow = AllowAuthorize.NotAllow;
        public AuthorizeIdentityActionFilter()
        {
        }
        public AuthorizeIdentityActionFilter(AllowAuthorize al)
        {
            authorizeAllow = al;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (authorizeAllow != AllowAuthorize.Anonymous)
            {
                if (HttpContext.Current.Session["UserName"] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary{{ "controller", "Account" },
                                          { "action", "UnAuthorize" }

                                         });
                }
            }
            base.OnActionExecuting(filterContext);
        }


    }
}