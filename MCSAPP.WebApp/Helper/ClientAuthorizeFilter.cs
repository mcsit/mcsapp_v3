﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MCSAPP.WebApp.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ClientAuthorizeFilter : AuthorizationFilterAttribute
    {
        public ClientAuthorizeFilter()
        {

        }
        private readonly bool _Isuser = true;
        public ClientAuthorizeFilter(bool user)
        {
            _Isuser = user;
        }
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (!_Isuser) return;
            if (System.Web.HttpContext.Current.Session != null)
            {
                if (System.Web.HttpContext.Current.Session["UserName"] == null)
                {
                    ResponseAuthRequest(filterContext);
                    return;
                }
            }
            else
            {
                ResponseAuthRequest(filterContext);
                return;
            }

            base.OnAuthorization(filterContext);
        }
        protected virtual bool OnAuthorizeClient(string user, string pass, HttpActionContext filterContext)
        {
            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(pass))
                return false;
            return true;
        }

        private static void ResponseAuthRequest(HttpActionContext filterContext)
        {
            var host_ = filterContext.Request.RequestUri.DnsSafeHost;
            filterContext.Response = filterContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
            filterContext.Response.Headers.Add("Server-Authenticate", "user is not authenticate");
        }
    }
}