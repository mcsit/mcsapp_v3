﻿using MCSAPP.WebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MCSAPP.Helper;
using MCSAPP.WebFactory;
using MCSAPP.DAL.Model;
using Newtonsoft.Json;
namespace MCSAPP.WebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.Filters.Add(new APIAuthorizeFilter());
            ModelBinders.Binders.DefaultBinder = new EmptyStringModelBinder();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                //var logger = ServiceLoggerFactory.GetCurrentLogServiceInstance();
                //Exception exception = Server.GetLastError();
                //Server.ClearError();

                //var allException = GetAllException(exception);
                //var innerException = (allException != null && allException.Any()) ? string.Join("InnerEx : ", allException) : JsonConvert.SerializeObject(new ResultModel() { Result = ResultModel.ResultCode.Fail, Message = "Applicatiuon Error" });
                //logger.ErrorException($"Error GlobalHandle URL : {Request?.Url}, Browser : {Request?.Browser?.Type}, InnerException : {innerException};", exception);

                Response.StatusCode = 500;
                //Response.Write("ระบบไม่สามารถใช้งานได้ กรุณาติดต่อผู้ดูแลระบบ");
              //  Response.Write(innerException);
                Response.End();

            }
            catch (Exception)
            {
                Response.StatusCode = 500;
            }
        }

        private List<string> GetAllException(Exception exception)
        {
            var listException = new List<string>() { LogTemplate.ErrorMessageWithSourceTemplate(exception) };
            Func<Exception, Exception> RecursiveException = null;
            RecursiveException = (ex) =>
            {
                if (ex != null && ex.InnerException != null)
                {
                    listException.Add(LogTemplate.ErrorMessageWithSourceTemplate(exception));
                    return RecursiveException(ex.InnerException);
                }
                return null;
            };
            RecursiveException(exception);
            return listException;
        }

        //Point API and Web Context Session
        //protected void Application_PostAuthorizeRequest()
        //{
        //    if (IsWebApiRequest())
        //    {
        //        HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        //    }
        //}

        //private bool IsWebApiRequest()
        //{
        //    return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative);
        //}
    }

    public class EmptyStringModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            bindingContext.ModelMetadata.ConvertEmptyStringToNull = false;
            Binders = new ModelBinderDictionary() { DefaultBinder = this };
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
