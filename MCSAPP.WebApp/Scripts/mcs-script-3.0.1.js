﻿
/// Javascript By Dev
/// mcs-script v.1 using Vehical System with firefox v.28 or higher 
/// mcs-script v.2 modify  using Career System   for IE8 ,IE 11
/// mcs-script v.3 modify  using Stationery System  for IE8+,firefox,chrome and support response
//(function () {

//}());
(function () {
    var method;
    var noop = function () { };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    //if (!window.console) {
    //    var length = methods.length;
    //    var console_ = (window.console = window.console || {});

    //    while (length--) {
    //        method = methods[length];
    //        if (!console_[method]) {
    //            console_[method] = noop;
    //        }
    //    }

    //    if (!Function.prototype.bind && typeof console != 'undefined' && typeof console.log == 'object') {
    //        Function.prototype.call.call(console.log, console, Array.prototype.slice.call(arguments));
    //    }
    //}

    this.AppHost = "";
    this.ImagePaths = "";
    this.serviceapi = true;
    this.setting = {

    };
    var mcsCore = window.mcs = function (a) {
        var moduleContext = new mcsCore.framework.int(a);
        //this.ElementOwn = moduleContext.ElementContext;
        return moduleContext;
    }, Contexts = [];// document.createElement("DIV");
    mcsCore.Property = { contructor: 'mcs Script', Version: '3.0' };
    if (!window.console) {
        window.console = { log: function (s) { } }
    }


    var DefaultProp = {
        service: { DefualtSender: {} }, autoScreenImgPath: function () {
            return this.WebPath() + "Image/Load.gif";
        },

        WebPath: function () {
            var fullPath = "";

            if (AppHost != "") {
                fullPath = AppHost + "/";
               // fullPath = location.protocol + "//" + location.host + "/" + AppHost + "/";
            } else {
               // alert("Can't find path .Please config mcs.setHost(YOUAPP); or config in file mcs-script at AppHost");
            }
            //} else {
            //    fullPath = location.protocol + "//" + location.host + "/";
            //}
            return fullPath;
        },
        ImagesPath: function () {
            var fullPath = "";
            if (ImagePaths != "") {
                fullPath = location.protocol + "//" + location.host + (AppHost == "" ? "" : ("/" + AppHost)) + "/" + ImagePaths + "/";
            } else {
               // alert("Can't find path .Please config mcs.setImagePath(YOUPATH); or config in file mcs-script at AppHost");
            }
            return fullPath;
        },

    };

    var Defind = {
        service: {
            ApiPath: function () {
                var strP = DefaultProp.WebPath();
                return strP + "";
            },
            Path: function () {
                return DefaultProp.WebPath();
            },
            Method: function () {
                return "GET";
            }
        }
    }


    var DateExtends = Date;


    DateExtends.prototype.yyyymmdd = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = this.getDate().toString();
        return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]); // padding
    };
    DateExtends.prototype.ddMMyyyy = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString();
        var dd = this.getDate().toString();
        return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
    };
    DateExtends.prototype.ddMMyyyy_HHmm = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString();
        var dd = this.getDate().toString();
        var hh = this.getHours();
        var mms = this.getMinutes();
        return (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy + " " + hh + ":" + ((mms == 0 || (mms >= 1 && mms <= 9)) ? "0" + mms : mms);
    };
    DateExtends.prototype.Format = function () {

    }
    DateExtends.prototype.TimestampToDate = function (s) {
        return new Date(s);
    }
    DateExtends.prototype.addDay = function (d) {
        var date = this.setDate(this.getDate() + (d));
        return date;
    }
    DateExtends.prototype.addMonth = function (d) {
        var date = this.setMonth(this.getMonth() + (d));
        return new Date(date);
    }
    DateExtends.prototype.addYear = function (d) {
        var date = this.setYear(this.getFullYear() + (d));
        return new Date(date);
    }
    DateExtends.prototype.getFirstDayInMonth = function (year, month) {
        if (year && month) {
            var FirstDay = new Date(year, month, 1);
            return FirstDay;
        } else {

            var FirstDay = new Date(this.getFullYear(), this.getMonth(), 1);
            return FirstDay;
        }
    }
    DateExtends.prototype.getFullMonth = function () {
        return this.getMonth() + 1;
    }
    DateExtends.prototype.getLastDayInMonth = function (year, month) {
        if (year && month) {
            month = month + 1;
            var LastDay = new Date(year, month, 1);
            LastDay = LastDay.addDay(-1);
            return new Date(LastDay);
        } else {

            var LastDay = new Date(this.getFullYear(), this.getMonth() + 1, 1);
            LastDay = LastDay.addDay(-1);
            return new Date(LastDay);
        }
    }



    var DateTime = function (option) {
        if (arguments.length == 3) {
            return new DateExtends(arguments[0], arguments[1], arguments[2]);
        } else if (arguments.length == 1) {
            return new DateExtends(arguments[0]);
        } else {
            return new DateExtends();
        }

    }
    DateTime.getDateRang = function (ds, de, f) {

    }
    DateTime.convertStringToDate = function (s, optional) {
        if (is.String(s)) {
            if (s != '') {
                var StrDate = s.split('/');
                var dd = parseInt(StrDate[0]);
                var MM = parseInt(StrDate[1]) - 1;
                var yyyy = parseInt(StrDate[2]);
                //console.log(dd.toString() + "-" + MM.toString() + "-" + yyyy);
                var newDate = new Date(yyyy, MM, dd);
                //console.log(newDate)
                return newDate;
            } else {
                if (arguments.length > 1) {
                    return optional;
                } else {
                    return new Date();
                }


            }
        }
    }
    DateTime.convertJsonDateToDateTime = function (s) {
        var date = new Date(parseInt(s.substr(6)));
        var DateStr = s.match(/\d+/)[0] * 1;
        return new Date(DateStr);
    }
    DateTime.convertJsonDateToString = function (s) {
        var date = new Date(parseInt(s.substr(6)));
        var DateStr = s.match(/\d+/)[0] * 1;
        var sDate = new Date(DateStr).ddMMyyyy();
        return sDate;
    }
    DateTime.convertJsonDateToStringDateTime = function (s) {
        var date = new Date(parseInt(s.substr(6)));
        var DateStr = s.match(/\d+/)[0] * 1;
        var sDate = new Date(DateStr).ddMMyyyy_HHmm();
        return sDate;
    }
    DateTime.getDateRangByMonth = function (year, month, f) {
        var dateRang = new Date(year, month, 1), dateArray = [];
        var firstDate = dateRang.getFirstDayInMonth();
        //document.write(firstDate)
        //document.write('</br>')
        var lastDate = dateRang.getLastDayInMonth();

        dateArray.push(new Date(firstDate));
        while (firstDate < lastDate) {
            var currentDate = firstDate;
            //document.write('</br>')
            //document.write(currentDate
            //var firstDay = new Date(year, month, 1);
            //var lastDay = new Date(year, month + 1, 0);
            var dateCur = firstDate.addDay(1);
            dateArray.push(new Date(dateCur));

        }
        return dateArray;
    }

    DateTime.getFirstDayInMonth = new DateExtends().getFirstDayInMonth;
    DateTime.getLastDayInMonth = new DateExtends().getLastDayInMonth;
    DateTime.TimestampToDate = new DateExtends().TimestampToDate;



    var helper = {
        readObj: function (obj, callback) {
            for (var i in obj) {
                if (callback) {
                    try {
                        callback(i + " : " + obj[i]);
                    } catch (e) {
                        callback(ex.message);
                    }
                } else {
                    try {
                        alert(i + " : " + obj[i]);
                    } catch (ex) {
                        alert(ex.message);
                    }
                }

            }

        },
        count: function (obj) {
            return Object.keys(obj).length;
        },
        delay: function (f, t) {
            var timeDelay = setTimeout(function () {
                f();
                clearTimeout(timeDelay);
            }, t || 500);
            //return setInterval(f, t || 10);
        },
        delayRet: function (f, t) {
            return setTimeout(function () { f(); }, t || 500);
        },
        keyCodeDB: function (k) {
            var keylistarray = []
            keylistarray.push({ key: "8", value: "backspace" });
            keylistarray.push({ key: "9", value: "tab" });
            keylistarray.push({ key: "13", value: "enter" });
            keylistarray.push({ key: "16", value: "shift" });
            keylistarray.push({ key: "17", value: "ctrl" });
            keylistarray.push({ key: "18", value: "alt" });
            keylistarray.push({ key: "19", value: "break" });
            keylistarray.push({ key: "20", value: "caplock" });
            keylistarray.push({ key: "27", value: "escape" });
            keylistarray.push({ key: "33", value: "pageup" });
            keylistarray.push({ key: "34", value: "pagedown" });
            keylistarray.push({ key: "35", value: "end" });
            keylistarray.push({ key: "36", value: "home" });
            keylistarray.push({ key: "37", value: "left" });
            keylistarray.push({ key: "38", value: "up" });
            keylistarray.push({ key: "39", value: "right" });
            keylistarray.push({ key: "40", value: "down" });
            keylistarray.push({ key: "45", value: "insert" });
            keylistarray.push({ key: "46", value: "delete" });
            keylistarray.push({ key: "48", value: "0" });
            keylistarray.push({ key: "49", value: "1" });
            keylistarray.push({ key: "50", value: "2" });
            keylistarray.push({ key: "51", value: "3" });
            keylistarray.push({ key: "52", value: "4" });
            keylistarray.push({ key: "53", value: "5" });
            keylistarray.push({ key: "54", value: "6" });
            keylistarray.push({ key: "55", value: "7" });
            keylistarray.push({ key: "56", value: "8" });
            keylistarray.push({ key: "57", value: "9" });

            keylistarray.push({ key: "96", value: "0" });
            keylistarray.push({ key: "97", value: "1" });
            keylistarray.push({ key: "98", value: "2" });
            keylistarray.push({ key: "99", value: "3" });
            keylistarray.push({ key: "100", value: "4" });
            keylistarray.push({ key: "101", value: "5" });
            keylistarray.push({ key: "102", value: "6" });
            keylistarray.push({ key: "103", value: "7" });
            keylistarray.push({ key: "104", value: "8" });
            keylistarray.push({ key: "105", value: "9" });
            keylistarray.push({ key: "106", value: "multiply" });
            keylistarray.push({ key: "107", value: "add" });
            keylistarray.push({ key: "109", value: "subtract" });
            keylistarray.push({ key: "110", value: "decimalpoint" });
            keylistarray.push({ key: "111", value: "devide" });
            keylistarray.push({ key: "144", value: "numlock" });
            keylistarray.push({ key: "145", value: "scrolllock" });
            keylistarray.push({ key: "186", value: "semicolon" });
            keylistarray.push({ key: "187", value: "equalsign" });
            keylistarray.push({ key: "188", value: "comma" });
            keylistarray.push({ key: "189", value: "dash" });
            keylistarray.push({ key: "220", value: "backslash" });
            keylistarray.push({ key: "222", value: "singlequote" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            //keylistarray.push({ key: "", value: "" });
            for (var item in keylistarray) {
                if (keylistarray[item]["key"] == k.toString()) {
                    return keylistarray[item]["value"];
                }
            }
            return "";
        }
    }

    var converts = {
        JsonToObject: function (jsonString) {
            if (is.Function(JSON.parse)) {
                return JSON.parse(jsonString);
            }
            else {
                if (jsonString === "") jsonString = '""';
                eval("var p=" + jsonString + ";");
                return p;
                //return eval('(' + jsonString + ')');
            }
        }, StringToMoney: function (s) {
            if (s) {
                var nFor = s.toString().split('.');
                if (nFor > 1) {
                    return nFor[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                }
                return s.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            } else {
                return "0.00";
            }
        }
        , MoneyToString: function (s) {
            return s.toString().replace(/,/g, '');
        }
    }
    /// variable And Function Global
    ///  
    var PropEle = {
        Height: function () {
            if (Contexts.clientHeight != 0) {
                return Contexts.clientHeight;
            } else if (Contexts.offsetHeight != 0) {
                return Contexts.offsetHeight;
            } else if (Contexts.scrollHeight != 0) {
                return Contexts.scrollHeight;
            } else { return Contexts.style.height; }

        },
        Width: function () {
            if (Contexts.clientWidth != 0) {
                return Contexts.clientWidth;
            } else if (Contexts.offsetWidth != 0) {
                return Contexts.offsetWidth;
            } else if (Contexts.scrollWidth != 0) {
                return Contexts.scrollWidth;
            } else { return Contexts.style.width; }
        }
    };


    var messagePopup = function (ob) {

        var header, content, type, onok, oncancel;
        if (ob) {
            header = ob.header == undefined ? "ข้อมูลผิดพลาด" : ob.header
            content = ob.content == undefined ? "System Error .Please Contact Admin" : (ob.content == "" ? "System Error !.Please Contact Admin" : ob.content)
            type = ob.type == undefined ? "ok" : ob.type

            onok = ob.onok;
            oncancel = ob.oncancel
        }


        var div_ = document.createElement("DIV");
        div_.style.zIndex = "99999";
        div_.id = "Popup_boxMessage";
        div_.style.position = "fixed";
        div_.style.top = "0px";
        div_.style.bottom = "0px";
        div_.style.right = "0px";
        div_.style.left = "0px";
        div_.style.backgroundColor = '#212020';
        div_.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)';
        div_.style.opacity = '0.3';
        var strOk = '<input id="popUp_ok"   type="button" style="cursor:pointer; border:none;background-color: #F0F0F0;color: #3A5FCD; height:35px;width:130px;font-weight:bolder; float:right;margin-right:5px;" value="OK" />';
        var strCancel = ' <input id="popUp_cancel"  type="button" style="cursor: pointer; border: none; background-color: white; color: #CD5555; height: 35px; width: 130px; font-weight: bolder; float: right" value="CANCEL" />';
        var str = '<div style="position:fixed;top:0;bottom:0;right:0;left:0;width:50%;height:215px; margin:auto;background-color: #FFF;z-index: 99999;">' +
     '<div style="position:relative">' +
         '<div style="border-bottom: 1px solid #f3eed1; padding: 0px 25px; ">' +
            ' <h2  style="color:#000088">' + header + '</h2>' +
         '</div>' +
         '<div style="margin-top: 5px; padding: 0px 25px;min-height:36px"> ' +
             '<p style="color:#757575;word-break:break-all">' + content + '</p>' +
          '</div>' +
         '<div style="height: 50px; width: 100%; border-top: 1px solid #f3eed1; margin-top: 35px; padding-top: 5px;background-color: #FFF; ">' +
            '<div style="right:30px;margin-right:15px;">';
        if (type) {
            if (type == "ok-cancel") {
                str += strCancel + strOk;

            } else if (type == "ok") {
                str += strOk;
            }
            else if (type == "cancel") {
                str += strCancel;
            } else {

            }

        } else {
            str += strOk;
        }

        str += ' </div>' +


      ' </div>' +
  ' </div> ' +
' </div>';
        var divInner = document.createElement('div');
        divInner.id = 'Popup_box_inbox';
        divInner.style.zIndex = '999999';
        if (document.addEventListener) { // support css 3


            if (!document.getElementById('Popup_boxMessage')) {
                divInner.innerHTML = str;

                document.body.appendChild(div_);
                document.body.appendChild(divInner);
            }
        } else {
            //div_box.appendChild(img_);
            if (!document.getElementById('Popup_boxMessage')) {
                divInner.innerHTML = str;

                document.body.appendChild(div_);
                document.body.appendChild(divInner);
            }

        }
        var buttonOk = document.getElementById('popUp_ok');
        if (buttonOk) {
            buttonOk.onmouseover = function () {
                this.style.backgroundColor = "#8B8970";
                this.style.color = "#fff";
            }
            buttonOk.onmouseleave = function () {
                this.style.color = "#3A5FCD";
                this.style.backgroundColor = "#fff";
            }
            buttonOk.onclick = function () {

                var parrent = document.getElementById('Popup_boxMessage');

                if (parrent) {
                    var popinbox = document.getElementById('Popup_box_inbox');
                    parrent.parentNode.removeChild(popinbox);
                    parrent.parentNode.removeChild(parrent);
                }

                if (onok) {

                    if (is.Function(onok)) {

                        onok();

                    }
                }

            }
        }
        var buttonCancel = document.getElementById('popUp_cancel');
        if (buttonCancel) {
            buttonCancel.onmouseover = function () {
                this.style.backgroundColor = "#3A5FCD";
                this.style.color = "#fff";
            }
            buttonCancel.onmouseleave = function () {
                this.style.color = "#CD5555";
                this.style.backgroundColor = "#fff";
            }
            buttonCancel.onclick = function () {
                var parrent = document.getElementById('Popup_boxMessage');
                if (parrent) {
                    var popinbox = document.getElementById('Popup_box_inbox');
                    parrent.parentNode.removeChild(popinbox);
                    parrent.parentNode.removeChild(parrent);
                }
                if (oncancel) {
                    if (is.Function(oncancel)) {
                        oncancel();
                    }
                }

            }
        }
    }
    var loading =
        {
            Show: function (callback) {
                var do_complete = document.createElement('DIV');
                var waitImg = document.createElement('DIV');
                waitImg.className = "loader";
                //waitImg.src = window.location.protocol + '//' + window.location.host + '/' + AppHost + '/images/icon/loader.gif';
                //waitImg.style.display = 'block';
                //waitImg.style.margin = 'auto';
                //waitImg.style.height = '150px';
                //waitImg.style.width = '150px';
                //waitImg.style.position = 'absolute';
                //waitImg.style.top = '0';
                //waitImg.style.bottom = '0';
                //waitImg.style.left = '0';
                // waitImg.style.right = '0';
                do_complete.id = 'Do_CompleteID';
                do_complete.style.backgroundColor = '#363535';
                do_complete.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=95)';
                do_complete.style.opacity = '0.95';
                do_complete.style.position = 'fixed';
                do_complete.style.top = '0';
                do_complete.style.bottom = '0';
                do_complete.style.left = '0';
                do_complete.style.right = '0';
                do_complete.style.zIndex = '10000';
                do_complete.appendChild(waitImg);
                document.body.appendChild(do_complete);


                if (callback) {
                    var setT = setTimeout(function () {
                        callback();
                        clearTimeout(setT);
                    }, 500);
                }


            }, Close: function (w, t) {
                var close_ = setTimeout(function () {
                    var do_cm = document.getElementById('Do_CompleteID');
                    if (do_cm) {
                        var parent = do_cm.parentNode;
                        parent.removeChild(do_cm);
                    }
                    clearTimeout(close_);
                }, t || 500);
                if (w) {
                    var setT = setTimeout(function () {
                        w();
                        clearTimeout(setT);
                    }, 500);
                }

            }
        }


    // func use get attribute of element
    // Attribute scope
    var Attrs = new Entity({
        attributeGet: function (a) {
            var element = Contexts;
            var attr = element.getAttribute(a);
            return { valid: attr != null ? true : false, value: attr != null ? attr : "" };

        },
        attributeSet: function (c, v) {
            var element = Contexts;
            element.removeAttribute(c);
            var att = document.createAttribute(c);
            att.value = v;
            element.setAttributeNode(att);
        },
        attributeAdd: function (c, v) {
            var element = Contexts;
            element.removeAttribute(c);
            var att = document.createAttribute(c);
            att.value = v;
            element.setAttributeNode(att);
        }, attributeRemove: function (a) {
            var element = Contexts;
            element.removeAttribute(a);
        }
    });

    var EventValid = new Entity({
        getEvent: function (n) {
            var resE = false;
            if (Contexts["on" + n])
                resE = true;

            return resE;
        }
    });
    var browseris = function () {
        var versionBrowser = "";
        this.BrowserName = "";
        var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        var isFirefox = typeof InstallTrigger !== 'undefined';
        var isFirefoxV = "";
        var isSafari = {};

        var isChrome = !!window.chrome && !isOpera;

        var isIE = false;

        if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
            isIE = true;
            var re = /(msie \d+(\d)*)/gi;
            var found = navigator.userAgent.toLowerCase().match(re);

            try {
                versionBrowser = found.toString().split(" ")[1];
            } catch (e) {
                //  versionBrowser = found.split(" ")[1];

                //   var rv = -1; // Return value assumes failure.
                //  // if (navigator.appName == 'Microsoft Internet Explorer') {
                //       var ua = navigator.userAgent;
                //       var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                //       if (re.exec(ua) != null)
                //           rv = parseFloat(RegExp.$1);
                ////   }
                if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {

                    var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
                    versionBrowser = match ? parseInt(match[1]) : undefined;

                }


            }
        }

        return { Name: { IsIE: isIE, IsFirefox: isFirefox, isOpera: isOpera, isSafari: isSafari, isChrome: isChrome }, Version: versionBrowser };
    }


    var Loadcompleted = function (callback) {
        if (document.addEventListener || event.type === "load" || document.readyState === "complete") {
            Unload();
            if (callback) {
                callback();
            }
        }
    },
       Unload = function () {
           if (document.addEventListener) {
               if (browseris().Name.IsIE && parseInt(browseris().Version) >= 10) {
                   document.removeEventListener("onreadystatechange", Loadcompleted, false);
               } else {
                   // 
                   window.removeEventListener("load", Loadcompleted, false);
               }

           }
           //else {
           //    document.detachEvent("onreadystatechange", completed);
           //    window.detachEvent("onload", completed);
           //}
       };
    var pageCompleted = function (callback) {
        if (browseris().Name.IsIE && parseInt(browseris().Version) >= 10) {
            document.addEventListener("onreadystatechange", Loadcompleted(callback), false);
        } else {


            if (document.attachEvent) {
                document.attachEvent("onreadystatechange", function () {
                    if (document.readyState == "complete") {
                        if (document.detachEvent) {
                            document.detachEvent("onreadystatechange");
                            callback();
                        } else {
                            document.removeEventListener("onreadystatechange");
                            callback();
                        }
                    }
                });
            } else if (document.addEventListener) {

                // document.addEventListener("DOMContentLoaded", Loadcompleted(callback), false);
                window.addEventListener("load", Loadcompleted(callback), false);
            }
        }


    }
    var Selected = function () {
        if (Contexts.options) {
            return { value: Contexts.options[Contexts.selectedIndex].value, text: Contexts.options[Contexts.selectedIndex].text };
        }
    }

    ///mcs is Target
    /// Variable OutSide mcsCore 
    var is = {
        Element: function (o) {
            if (o.nodeType) {
                return true;
            } else if (o.document && o.location) {
                return true;
            } else {
                return false;
            }
            //      var o = Contexts; 
            //  var ret = (o != null && o.nodeName === 1 && o.nodeName === "string"
            //  //  typeof HTMLElement === "object" ? o instanceof HTMLElement :  
            //  //  o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string"
            //);
            //if (o != null && o.nodeType === 1 && o.nodeName === "string")
            //{ return true; } else
            //{ return false; }
            //return ret;
        },
        ElementOf: function (c, s) {
            var o = (c === undefined) ? Contexts : c;
            if (o.nodeName == s)
            { return true } else
            {
                return false;
            }
        },
        Node: function (o) {
            return (
                typeof Node === "object" ? o instanceof Node :
                o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName === "string"
              );
        },
        Array: function (c) {
            if (c) {

                if (typeof c == "array") {
                    return true;
                } else if (c.length > 0) {
                    //return true;
                } else {
                    return false;
                }
            } else {
                if (typeof Contexts == "array") {
                    return true;
                } else {
                    return false;
                }
            }
        },
        Count: function (c) {
            if (c) {
                if (c.length) {
                    return c.length;
                } else {
                    return 0;
                }
            } else if (c == undefined) {
                return 0;
            }
            else {
                if (Contexts) {
                    if (Contexts.length) {
                        return Contexts.length;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            }
        },
        Undefined: function (c) {
            var length_ = arguments.length;
            if (length_ > 0) {
                if (typeof c == "undefined") {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (typeof Contexts == "undefined") {
                    return true;
                } else {
                    return false;
                }
            }
        },
        Object: function (c) {
            if (c) {
                if (typeof c == "object") {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (typeof Contexts == "object") {
                    return true;
                } else {
                    return false;
                }
            }
        },
        Function: function (c) {
            var length_ = arguments.length;
            if (length_ > 0) {
                if (typeof c == "function") {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (typeof Contexts == "function") {
                    return true;
                } else {
                    return false;
                }
            }
        }, Number: function (c) {
            try {
                if (c.length) {
                    return false;
                } else {
                    var io = parseInt(c);
                    var tol = io * 0;
                    if (tol == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } catch (epn) {
                return false;
            }
        }
        ,
        String: function (c) {
            var length_ = arguments.length;
            if (length_ > 0) {
                if (typeof c == "string") {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (typeof Contexts == "string") {
                    return true;
                } else {
                    return false;
                }
            }
        }, Whitespace: function (node) {
            return node.nodeType == 3 && /^\s*$/.test(node.data);
        }
    };


    var callservice =
        {
            service: function (op, callback, async) {

                async = async == undefined ? false : true;
                var method, dataArr = {}, url, subfolder, inpath, isvoid = op.isvoid, responsed = {}, status, callbackhttpstatusOK = callback.success, callbackhttpstatusEror = callback.error;
                method = op.method;
                if (localStorage) {
                    localStorage.clear();
                }
                if (is.Function(op.data)) {
                    dataArr = op.data();
                } else {
                    dataArr = is.Undefined(op.data) == true ? DefaultProp.service.DefualtSender : op.data;
                }


                url = serviceapi == true ? Defind.service.ApiPath() : Defind.service.Path();

                url = url + op.commandData;

                var client = null;
                if (window.XMLHttpRequest) {
                    client = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    client = new ActiveXObject("Microsoft.XMLHTTP");
                }

                var httpMethod = is.Undefined(method) == true ? DefindFunction.defaultProperty.service.method : method;
                if (httpMethod != 'GET' && httpMethod != 'POST') {
                    httpMethod = 'GET';
                }
                var params = {};
                if (is.Object(dataArr)) {
                    if (dataArr.length) {
                        if (dataArr.length > 0)
                            params = params = JSON.stringify(dataArr);
                    } else {
                        params = JSON.stringify(dataArr);
                    }
                }

                client.open(httpMethod, url, async);
                var serverresponseResult = null;
                function AlertDataMessage(messageData) {
                    var divBoxScreenMessage = document.createElement('div');
                    divBoxScreenMessage.style.zIndex = "99999";
                    divBoxScreenMessage.id = 'defaultmessage';
                    divBoxScreenMessage.style.position = 'fixed';
                    divBoxScreenMessage.style.top = '0';
                    divBoxScreenMessage.style.bottom = '0';
                    divBoxScreenMessage.style.right = '0';
                    divBoxScreenMessage.style.left = '0';
                    var divScreenMessage = document.createElement('div');
                    divScreenMessage.style.position = 'fixed';
                    divScreenMessage.style.top = '0';
                    divScreenMessage.style.bottom = '0';
                    divScreenMessage.style.right = '0';
                    divScreenMessage.style.left = '0';
                    divScreenMessage.style.backgroundColor = '#212020';
                    divScreenMessage.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)';
                    divScreenMessage.style.opacity = '0.3';
                    var divMessage = document.createElement('div');
                    divMessage.style.width = '80%';
                    divMessage.style.height = '75px';
                    divMessage.style.position = 'absolute';
                    divMessage.style.top = '0';
                    divMessage.style.bottom = '0';
                    divMessage.style.right = '0';
                    divMessage.style.left = '0';
                    divMessage.style.margin = 'auto';
                    divMessage.style.backgroundColor = '#FFF';
                    divMessage.style.padding = '10px';
                    divMessage.style.borderRadius = '5px';
                    divMessage.style.backgroundColor = '#fff';
                    var paragrapMessage = document.createElement('table');
                    paragrapMessage.style.width = '100%';
                    paragrapMessage.style.height = '100%';
                    var paragrapRow = document.createElement('tr');
                    var paragrapCell = document.createElement('td');
                    paragrapCell.style.color = '#E68035';
                    paragrapCell.style.margin = '5px';
                    paragrapCell.style.padding = '0';
                    paragrapCell.style.textAlign = 'center';
                    paragrapCell.appendChild(document.createTextNode("! " + messageData));
                    paragrapRow.appendChild(paragrapCell);
                    paragrapMessage.appendChild(paragrapRow);
                    var closeMessageDefaut = document.createElement('span');
                    closeMessageDefaut.appendChild(document.createTextNode('X'));
                    closeMessageDefaut.style.border = '1px solid #ECECEC';
                    closeMessageDefaut.style.position = 'absolute';
                    closeMessageDefaut.style.top = "0";
                    closeMessageDefaut.style.right = '0';
                    closeMessageDefaut.style.display = 'inline-block';
                    closeMessageDefaut.style.padding = '0px 9px';
                    closeMessageDefaut.style.color = '#7A7A7A';
                    closeMessageDefaut.style.cursor = 'pointer';
                    closeMessageDefaut.onclick = function () {
                        var box_ = document.getElementById('defaultmessage');
                        var parentBox_ = box_.parentNode;
                        parentBox_.removeChild(box_)
                    }

                    divMessage.appendChild(paragrapMessage);
                    divMessage.appendChild(closeMessageDefaut);

                    divBoxScreenMessage.appendChild(divScreenMessage);
                    divBoxScreenMessage.appendChild(divMessage);
                    document.body.appendChild(divBoxScreenMessage);

                }
                client.onreadystatechange = function () {

                    if (client.readyState == 4) {


                        var responseData = client.response || client.responseText;
                        serverresponseResult = responseData;
                        if (localStorage) {
                            localStorage.setItem('ResourceAudit', "response : " + responseData)
                        }



                        if (client.status == 200) {
                            if (isvoid == true) // not wait for return
                            {
                                return;
                            }
                            if (is.String(responseData)) {

                                var resultdata = [];
                                try {

                                    if (op.dataType) {

                                        if (op.dataType == "text/html") {
                                            responsed.data = responseData
                                            responsed.status = client.status;

                                        } else if (op.dataType == "object/Entity") {
                                            resultdata = converts.JsonToObject(responseData);
                                            responsed.data = is.Undefined(resultdata) == true ? {} : resultdata.Results
                                            responsed.status = client.status;

                                        }
                                        else {
                                            resultdata = converts.JsonToObject(responseData);
                                            responsed.data = is.Undefined(resultdata.Data) == true ? resultdata : resultdata.Data
                                            responsed.status = is.Undefined(resultdata.StatusCode) == true ? "200" : resultdata.StatusCode
                                            responsed.message = is.Undefined(resultdata.Message) == true ? "" : resultdata.Message
                                        }
                                    } else {

                                        //if (client.getResponseHeader("Content-Type") == 'application/json') {
                                        resultdata = converts.JsonToObject(responseData);

                                        if (resultdata.length > 0) {
                                            responsed.data = resultdata;
                                            responsed.status = client.status;

                                        } else {

                                            responsed.data = is.Undefined(resultdata.Data) == true ? resultdata : resultdata.Data
                                            responsed.status = is.Undefined(resultdata.StatusCode) == true ? client.status : resultdata.StatusCode
                                            responsed.message = is.Undefined(resultdata.Message) == true ? "" : resultdata.Message

                                        }
                                        //} else {
                                        //    responsed.data = resultdata;
                                        //    responsed.status = client.status;
                                        //}



                                    }

                                    if (localStorage) {
                                        localStorage.setItem("GetAudit", JSON.stringify(responsed));
                                    }


                                    if (!is.Undefined(callback)) {
                                        if (is.Function(callback)) {
                                            callback(responsed);
                                        } else if (is.Object(callback)) {

                                            if (responsed.status == "200") {

                                                if (callbackhttpstatusOK) {

                                                    callbackhttpstatusOK(responsed);

                                                }
                                            } else {
                                                if (callbackhttpstatusEror) {
                                                    callbackhttpstatusEror(responsed);

                                                }
                                            }
                                        }
                                    }
                                } catch (e) {

                                    AlertDataMessage("Result invalid. please check data return from action ,message : " + e.message);
                                }
                            } else {

                                responsed.data = typeof responseData != 'object' ? converts.JsonToObject(responseData) : responseData;
                                try {
                                    if (localStorage) {
                                        localStorage.setItem("GetAudit", JSON.stringify(responsed));
                                    }
                                } catch (ex) { }
                                responsed.status = client.status;
                                if (!is.Undefined(callback)) {
                                    if (is.Function(callback)) {
                                        callback(responsed);
                                    } else if (is.Object(callback)) {
                                        if (callbackhttpstatusOK) {
                                            callbackhttpstatusOK(responsed);
                                        }
                                    }
                                }
                            }
                        }
                        else if (client.status == 204) {
                            responsed.data = responseData;
                            responsed.status = client.status;
                            if (!is.Undefined(callback)) {
                                if (is.Object(callback)) {
                                    if (callbackhttpstatusEror) {
                                        callbackhttpstatusEror(responsed);
                                    }
                                }
                                //else {
                                //    if (callbackError) {
                                //        callbackError(responsed);
                                //    }
                                //}
                            }
                            //else if (callbackError) {
                            //    callbackError(responsed);
                            //}
                            AlertDataMessage(responsed.data);
                        }

                        else if (client.status == 401) {
                            // responsed.data = "Unauthorized";
                            responsed.data = { message: "คุณไม่มีสิทธิใช่งาน หรือ เนื่องจากไม่มีการใช้ระบบ เกินระยะเวลาที่กำหนด ระบบจะกลับไปสู่หน้าหลักกรุณารอสักครู่", status: 401 };
                            responsed.status = client.status;
                            if (!is.Undefined(callback)) {
                                if (is.Object(callback)) {
                                    if (callbackhttpstatusEror) {
                                        var ret = callbackhttpstatusEror(responsed);
                                        if (ret != false) {
                                            AlertDataMessage(responsed.data.message);
                                            var time_stopper = 0;
                                            time_stopper = setTimeout(function () {
                                                var box_ = document.getElementById('defaultmessage');
                                                if (box_) {
                                                    var parentBox_ = box_.parentNode;
                                                    parentBox_.removeChild(box_);
                                                }
                                                window.location.href = mcs.getWebPath() + '/' + 'Account';
                                                clearTimeout(time_stopper);
                                            }, 3000);
                                        }
                                    }
                                }
                                //else {
                                //    if (callbackError) {
                                //        callbackError(responsed);
                                //    }
                                //}
                            }
                            //else if (callbackError) {
                            //    callbackError(responsed);
                            //}

                        }
                        else if (client.status == 404) {
                            responsed.data = { message: "application can't connect to service status 404 please provide service : " + url, status: 404 };
                            responsed.status = client.status;
                            if (!is.Undefined(callback)) {
                                if (is.Object(callback)) {
                                    if (callbackhttpstatusEror) {
                                        callbackhttpstatusEror(responsed);
                                    }
                                }
                                //else {
                                //    if (callbackError) {
                                //        callbackError(responsed);
                                //    }
                                //}
                            }
                            //else if (callbackError) {
                            //    callbackError(responsed);
                            //}
                            AlertDataMessage(responsed.data.message);


                        } else if (client.status == 500) {

                            var statusError = {};
                            statusError.statusText = client.statusText;
                            try {
                                statusError.status = client.status;
                                statusError.message = converts.JsonToObject(responseData).Message;

                            } catch (e) {
                                // statusError.Message = "";
                                //alert("Service internal server error");

                                //  throw new ClientException(e.message,e);

                            }
                            responsed.data = statusError;
                            responsed.status = client.status;
                            if (!is.Undefined(callback)) {
                                if (is.Object(callback)) {
                                    if (callbackhttpstatusEror) {
                                        callbackhttpstatusEror(responsed);
                                    }
                                }
                                //else {
                                //    if (callbackError) {
                                //        callbackError(responsed);
                                //    }
                                //}
                            }
                            //else if (callbackError) {
                            //    callbackError(responsed);
                            //}
                        }

                        if (client.status != 200) {

                            localStorage.setItem('ErrorAudit', client.response)
                        }


                    }

                }
                if (localStorage) {
                    localStorage.setItem("CallAudit", url);
                    localStorage.setItem("SendAudit", params);
                }
                client.setRequestHeader("Content-Type", "application/json");
                client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                client.send(params);

                return {
                    ToList: function (callback) {
                        var resultObj = converts.JsonToObject(serverresponseResult);
                        var serverObjResult = {}
                        serverObjResult.data = is.Undefined(resultObj.Data) == true ? resultObj : resultObj.Data
                        serverObjResult.status = is.Undefined(resultObj.StatusCode) == true ? "200" : resultObj.StatusCode
                        serverObjResult.message = is.Undefined(resultObj.Message) == true ? "" : resultObj.Message
                        if (callback) { callback(mcsCore.method.ToList(serverObjResult)); } return serverObjResult;
                    }
                }
            }
        }
    var cssClass = {
        classIs: function (c) {
            var ele = Contexts;
            var eleClass = ele.getElementsByClassName(c);
            if (eleClass.length > 0)
                return true;

            return false;


        },
        classed: function (c) {
            Contexts.className = c;
            return this;
        },
        addClassDelay: function (addClass, t) {
            var element = Contexts;

            var adddelay = setTimeout(function () {

                if (element.length) {
                    for (var c = 0; c <= element.length - 1; c++) {
                        var currentClassValue = element[c].className;
                        if (currentClassValue.indexOf(addClass) == -1) {
                            if ((currentClassValue == null) || (currentClassValue === "")) {
                                element[c].className = addClass;
                            } else {

                                element[c].className += " " + addClass;
                            }
                        }
                    }
                } else {
                    var currentClassValue = element.className;
                    if (currentClassValue.indexOf(addClass) == -1) {
                        if ((currentClassValue == null) || (currentClassValue === "")) {
                            element.className = addClass;
                        } else {

                            element.className += " " + addClass;
                        }
                    }
                    clearTimeout(adddelay);
                }
            }, t || 500);

            return this;
        },
        addClass: function (addClass, element) {
            var element = Contexts;
            if (element.length) {
                for (var c = 0; c <= element.length - 1; c++) {
                    var currentClassValue = element[c].className;

                    if (currentClassValue.indexOf(addClass) == -1) {
                        if ((currentClassValue == null) || (currentClassValue === "")) {
                            element[c].className = addClass;
                        } else {

                            element[c].className += " " + addClass;
                        }
                    }
                }
            } else {
                var currentClassValue = element.className;

                if (currentClassValue.indexOf(addClass) == -1) {
                    if ((currentClassValue == null) || (currentClassValue === "")) {
                        element.className = addClass;
                    } else {

                        element.className += " " + addClass;
                    }
                }
            }
            return this;
        },
        removeClass: function (removeClass, element) {
            var element = Contexts;
            if (element.length) {
                for (var c = 0; c <= element.length - 1; c++) {
                    var currentClassValue = element[c].className;

                    if (currentClassValue == removeClass) {
                        element[c].className = "";
                        return;
                    }

                    var classValues = currentClassValue.split(" ");
                    var filteredList = [];

                    for (var i = 0 ; i < classValues.length; i++) {
                        if (removeClass != classValues[i]) {
                            filteredList.push(classValues[i]);
                        }
                    }

                    element[c].className = filteredList.join(" ");
                }
            }
            else {
                var currentClassValue = element.className;

                if (currentClassValue == removeClass) {
                    element.className = "";
                    return;
                }

                var classValues = currentClassValue.split(" ");
                var filteredList = [];

                for (var i = 0 ; i < classValues.length; i++) {
                    if (removeClass != classValues[i]) {
                        filteredList.push(classValues[i]);
                    }
                }

                element.className = filteredList.join(" ");
            }
            return this;
        },
        removeClassDelay: function (removeClass, t) {
            var element = Contexts;

            var removedelay = setTimeout(function () {
                if (element.length) {
                    for (var c = 0; c <= element.length - 1; c++) {
                        var currentClassValue = element[c].className;

                        if (currentClassValue == removeClass) {
                            element[c].className = "";
                            return;
                        }

                        var classValues = currentClassValue.split(" ");
                        var filteredList = [];

                        for (var i = 0 ; i < classValues.length; i++) {
                            if (removeClass != classValues[i]) {
                                filteredList.push(classValues[i]);
                            }
                        }

                        element[c].className = filteredList.join(" ");
                    }
                }
                else {
                    var currentClassValue = element.className;

                    if (currentClassValue == removeClass) {
                        element.className = "";
                        return;
                    }

                    var classValues = currentClassValue.split(" ");
                    var filteredList = [];

                    for (var i = 0 ; i < classValues.length; i++) {
                        if (removeClass != classValues[i]) {
                            filteredList.push(classValues[i]);
                        }
                    }

                    element.className = filteredList.join(" ");
                }
                clearTimeout(removedelay);
            }, t || 500);

            return this;
        },
        replaceClass: function (currentClass, replaceClass, element) {
            var element = Contexts;
            if (element.length) {
                for (var c = 0; c <= element.length - 1; c++) {
                    var currentClassValue = element[c].className;

                    if (currentClassValue.indexOf(currentClass) != -1) {
                        var classC = currentClassValue.split(' ');
                        var strClass = "", strpush = "";
                        for (var cc = 0; cc <= classC.length - 1; cc++) {
                            strpush = classC[cc];
                            if (strpush == currentClass)
                                strpush = replaceClass;

                            strClass += strpush + " ";
                        }
                        strClass = strClass.trim();
                        element[c].className = strClass;// currentClassValue.replace(currentClass, replaceClass).trim();
                    }
                }
            }
            else {
                var currentClassValue = element.className;

                if (currentClassValue.indexOf(currentClass) != -1) {
                    var classC = currentClassValue.split(' ');
                    var strClass = "", strpush = "";
                    for (var c = 0; c <= classC.length - 1; c++) {
                        strpush = classC[c];
                        if (strpush == currentClass)
                            strpush = replaceClass;

                        strClass += strpush + " ";
                    }
                    strClass = strClass.trim();
                    element.className = strClass;// currentClassValue.replace(currentClass, replaceClass).trim();
                }
            }
            return this;
        },
        replaceClassDelay: function (currentClass, replaceClass, t) {
            var element = Contexts;
            var replacedelay = setTimeout(function () {
                if (element.length) {
                    for (var c = 0; c <= element.length - 1; c++) {
                        var currentClassValue = element[c].className;
                        if (currentClassValue.indexOf(currentClass) != -1) {
                            var classC = currentClassValue.split(' ');
                            var strClass = "", strpush = "";
                            for (var cc = 0; cc <= classC.length - 1; cc++) {
                                strpush = classC[cc];
                                if (strpush == currentClass)
                                    strpush = replaceClass;

                                strClass += strpush + " ";
                            }
                            strClass = strClass.trim();
                            element[c].className = strClass;
                        }
                    }
                } else {
                    var currentClassValue = element.className;
                    if (currentClassValue.indexOf(currentClass) != -1) {
                        var classC = currentClassValue.split(' ');
                        var strClass = "", strpush = "";
                        for (var c = 0; c <= classC.length - 1; c++) {
                            strpush = classC[c];
                            if (strpush == currentClass)
                                strpush = replaceClass;

                            strClass += strpush + " ";
                        }
                        strClass = strClass.trim();
                        element.className = strClass;
                    }
                }
                clearTimeout(replacedelay);
            }, t || 500);
            return this;
        },
        cssStyle: function (cs) {
            var element = Contexts;
            if (element.length) {
                for (var c = 0; c <= element.length - 1; c++) {
                    //var objC = helper.count(cs);
                    for (var prop in cs) {
                        element[c].style[prop] = cs[prop];
                    }
                }
            } else {
                //var objC = helper.count(cs);
                for (var prop in cs) {
                    element.style[prop] = cs[prop];
                }
            }

            return this;
        },
        cssStyleDelay: function (cs, t) {
            var element = Contexts;
            var cssdey = setTimeout(function () {

                if (element.length) {
                    for (var c = 0; c <= element.length - 1; c++) {
                        //var objC = helper.count(cs);
                        for (var prop in cs) {
                            element[c].style[prop] = cs[prop];
                        }
                    }
                } else {
                    //var objC = helper.count(cs);
                    for (var prop in cs) {
                        element.style[prop] = cs[prop];
                    }
                }
                clearTimeout(cssdey);
            }, t || 500);

            return this;
        },
        addOrReplaceClassDelay: function (currentClass, replaceClass, t) {
            var element = Contexts;
            var replacedelay = setTimeout(function () {
                if (element.length) {
                    for (var c = 0; c <= element.length - 1; c++) {
                        var currentClassValue = element[c].className;
                        //if (currentClassValue.indexOf(currentClass) != -1) {
                        var classC = currentClassValue.split(' ');
                        var strClass = "", strpush = ""; flagClassDup = false;
                        for (var cc = 0; cc <= classC.length - 1; cc++) {
                            strpush = classC[cc];
                            if (strpush == currentClass) {
                                strpush = replaceClass;
                                flagClassDup = (flagClassDup == false ? true : true);
                            }
                            if (strpush != replaceClass)
                                strClass += strpush + " ";
                        }
                        if (flagClassDup == false) {
                            strClass = strClass + " " + replaceClass;
                        }
                        strClass = strClass.trim();
                        element[c].className = strClass;
                        //  }
                    }
                } else {
                    var currentClassValue = element.className;
                    //  if (currentClassValue.indexOf(currentClass) != -1) {
                    var classC = currentClassValue.split(' ');
                    var strClass = "", strpush = ""; flagClassDup = false;
                    for (var cc = 0; cc <= classC.length - 1; cc++) {
                        strpush = classC[cc];
                        if (strpush != replaceClass) {
                            if (strpush == currentClass) {
                                strpush = replaceClass;
                                flagClassDup = (flagClassDup == false ? true : true);
                            }

                            strClass += strpush + " ";
                        }
                    }
                    if (flagClassDup == false) {
                        strClass = strClass + " " + replaceClass;
                    }
                    strClass = strClass.trim();
                    element.className = strClass;
                    //  }
                }
                clearTimeout(replacedelay);
            }, t || 500);
            return this;
        },

    }


    var control = {
        table: function (tableOptions) {
            var tableContext_ = Contexts;
            var data_ = {}, responseTemp = {}, datatemp = {}, column_ = {}, clientparam = null, firstSort = '', firstSortCol = '', totalPage_ = '0', firstPage = 1, pageshow = 5, pageEnable = false, pageSize = 0;


            if (tableOptions) {
                if (tableOptions.column) {


                    column_ = tableOptions.column;
                    //console.log(JSON.stringify(column_))
                    for (var r = 0; r <= column_.length - 1; r++) {
                        if (column_[r] == null || column_[r] == undefined) {
                            console.log('column not match at' + r)
                            break;
                        }
                        if (typeof column_[r].sorttemplate === "function") {
                            var prop_sort = column_[r].sorttemplate();
                            if (prop_sort) {
                                var temp_order = prop_sort.order == undefined ? '' : prop_sort.order;

                                var temp_cansort = prop_sort.cansort == undefined ? false : prop_sort.cansort;

                                var temp_sorttype = prop_sort.sorttype == undefined ? '' : (prop_sort.sorttype == 'asc' ? 'asc' : (prop_sort.sorttype == 'desc' ? 'desc' : ''));
                                var temp_field = prop_sort.field == undefined ? '' : prop_sort.field;

                                column_[r].cansort = temp_cansort;
                                column_[r].field = temp_field;
                                if (temp_order == '1') {
                                    firstSort = temp_order;
                                    firstSortCol = temp_field;
                                    break;
                                }


                            } else {
                                column_[r].cansort = "";
                                column_[r].field = "";
                                firstSort = "";
                                firstSortCol = "";
                                break;
                            }
                        } else {
                            var head_sort = column_[r].sort == undefined ? '' : (column_[r].sort == 'asc' ? 'asc' : (column_[r].sort == 'desc' ? 'desc' : ''));
                            if (head_sort != '') {
                                firstSort = head_sort;
                                firstSortCol = column_[r].field == undefined ? "" : column_[r].field;

                                break;
                            }
                        }
                    }
                }
                //else {
                //    alert('autogen column')
                //}
                if (tableOptions.page) {
                    pageEnable = (tableOptions.page.enable == undefined ? false : (tableOptions.page.enable == false ? false : true));
                    pageSize = (tableOptions.page.pageSize == undefined ? 0 : tableOptions.page.pageSize);

                } else {
                    pageSize = 0;
                    pageEnable = false;
                }


                //if (!tableOptions.page || tableOptions.page.enable == undefined || tableOptions.page.enable == false)
                //{
                //    tableOptions.page.pageSize = 0;
                //}
                var tablePrrperty = { page: { current: 1, size: pageSize }, sort: { column: firstSortCol, dir: firstSort } };
                //console.log(" parma" + JSON.stringify(tablePrrperty))
                //callData
                if (tableOptions.loaddata) {
                    var param_ = null;
                    if (tableOptions.loaddata.data) {
                        if (typeof tableOptions.loaddata.data == 'object') {
                            param_ = tableOptions.loaddata.data;
                        } else if (typeof tableOptions.loaddata.data == 'string') {
                            param_ = tableOptions.loaddata.data;
                        } else if (typeof tableOptions.loaddata.data == 'function') {
                            param_ = tableOptions.loaddata.data(tablePrrperty);
                        }
                    }


                    clientparam = (param_ == null || param_ == undefined) ? {} : param_;

                } //else if (tableOptions.data) {
                //   data_ = tableOptions.data;

                //  }

                function Genstyle(table) {
                    if (tableOptions.style) {
                        return tableOptions.style(table);
                    }

                }
                function tableNotFound() {
                    var table_ = mcsCore.createTag("table");
                    table_.id = "Notable";
                    table_.className = tableOptions.baseclass == undefined ? "" : tableOptions.baseclass;



                    //create header
                    var tableHead_ = mcsCore.createTag("thead");
                    var rowHead = mcsCore.createTag("tr");
                    for (var r = 0; r <= column_.length - 1; r++) {
                        //var head_bind_field = column_[r].field;
                        var head_bind_head = "";
                        var columnHead = mcsCore.createTag('TH');
                        if (typeof column_[r].headtemplate == 'function') {
                            columnHead = column_[r].headtemplate(columnHead);
                        } else {
                            head_bind_head = column_[r].head;
                        }
                        if (column_[r].hidden == true)
                        { columnHead.style.display = 'none' }


                        columnHead.appendChild(mcsCore.createText(head_bind_head));
                        rowHead.appendChild(columnHead);
                    }
                    tableHead_.appendChild(rowHead);
                    table_.appendChild(tableHead_)

                    var tablebody_ = mcsCore.createTag("tbody");
                    var bodyTr = mcsCore.createTag("tr");
                    var bodyTd = mcsCore.createTag("td");
                    bodyTd.colSpan = column_.length;

                    bodyTd.appendChild(mcsCore.createText('RECORD NO DATA'));
                    bodyTd.style.textAlign = 'center'
                    bodyTr.appendChild(bodyTd);
                    tablebody_.appendChild(bodyTr);
                    table_.appendChild(tablebody_)
                    table_ = Genstyle(table_);


                    tableContext_.innerHTML = '';
                    tableContext_.appendChild(table_);

                }
                //end call data
                function RefeshTable(dataParam) {
                    var divBlockScreen = document.createElement("DIV");
                    divBlockScreen.style.position = "absolute";
                    divBlockScreen.style.top = "0px";
                    divBlockScreen.style.bottom = "0px";
                    divBlockScreen.style.right = "0px";
                    divBlockScreen.style.left = "0px";
                    divBlockScreen.style.backgroundColor = '#212020';
                    divBlockScreen.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)';
                    divBlockScreen.style.opacity = '0.3';

                    var loadLabel = document.createElement("SPAN");
                    loadLabel.innerHTML = 'LOADDING...';
                    loadLabel.style.display = 'inline-block';
                    loadLabel.style.margin = 'auto';
                    divBlockScreen.appendChild(loadLabel);
                    tableContext_.style.position = 'relative';
                    tableContext_.appendChild(divBlockScreen);

                    var _settime = setTimeout(function () {
                        if (tableOptions.loaddata) {
                            callservice.service({ method: tableOptions.loaddata.method, commandData: tableOptions.loaddata.commandData, data: dataParam },
                          {
                              success: function (response) {
                                  if (tableOptions.displayData == true) {
                                      console.log(response)
                                  }
                                  responseTemp = response;

                                  if (tableOptions.loaddata.column) {


                                      column_ = response.data[tableOptions.loaddata.column];
                                      //console.log(JSON.stringify(column_))
                                      for (var r = 0; r <= column_.length - 1; r++) {
                                          if (column_[r] == null || column_[r] == undefined) {
                                              console.log('column not match at' + r)
                                              break;
                                          }
                                          if (typeof column_[r].sorttemplate === "function") {
                                              var prop_sort = column_[r].sorttemplate();
                                              if (prop_sort) {
                                                  var temp_order = prop_sort.order == undefined ? '' : prop_sort.order;

                                                  var temp_cansort = prop_sort.cansort == undefined ? false : prop_sort.cansort;

                                                  var temp_sorttype = prop_sort.sorttype == undefined ? '' : (prop_sort.sorttype == 'asc' ? 'asc' : (prop_sort.sorttype == 'desc' ? 'desc' : ''));
                                                  var temp_field = prop_sort.field == undefined ? '' : prop_sort.field;

                                                  column_[r].cansort = temp_cansort;
                                                  column_[r].field = temp_field;
                                                  if (temp_order == '1') {
                                                      firstSort = temp_order;
                                                      firstSortCol = temp_field;
                                                      break;
                                                  }


                                              } else {
                                                  column_[r].cansort = "";
                                                  column_[r].field = "";
                                                  firstSort = "";
                                                  firstSortCol = "";
                                                  break;
                                              }
                                          }
                                          else {
                                              var head_sort = column_[r].sort == undefined ? '' : (column_[r].sort == 'asc' ? 'asc' : (column_[r].sort == 'desc' ? 'desc' : ''));
                                              if (head_sort != '') {
                                                  firstSort = head_sort;
                                                  firstSortCol = column_[r].field == undefined ? "" : column_[r].field;

                                                  break;
                                              }
                                          }
                                      }
                                  }
                                  //else {
                                  //    alert('autogen column')
                                  //}

                                  if (tableOptions.loaddata.renderObject) {
                                      var dataSource = {}
                                      var strObj = tableOptions.loaddata.renderObject.split('.');

                                      if (strObj.length > 1) {
                                          var obji = 0;

                                          while ((dataSource = response.data[strObj[obji]])) {

                                              if (obji == strObj.length - 1) {
                                                  break;
                                              }
                                              i++;
                                          }
                                      } else {
                                          dataSource = response.data[tableOptions.loaddata.renderObject];
                                      }
                                      data_ = dataSource;
                                  } else {
                                      data_ = response.data;
                                  }


                              },
                              error: function (response) {
                                  if (tableOptions.displayData == true) {
                                      console.log(response)
                                  }
                                  data_ = [];
                              }
                          });
                        } else if (tableOptions.data) {
                            data_ = tableOptions.data;

                        }

                        if (data_.length > 0) {

                            //bindding
                            var table_ = mcsCore.createTag("table");
                            table_.className = tableOptions.baseclass == undefined ? "" : tableOptions.baseclass;
                            table_.id = tableContext_.id + "_table";




                            //create body
                            var tablebody_ = mcsCore.createTag("tbody");

                            for (var rb = 0; rb <= data_.length - 1; rb++) {
                                var rowrec_ = mcsCore.createTag('tr');
                                rowrec_.className = (rb % 2) == 0 ? 'record_OOD' : 'record_EVEN';
                                for (var b = 0; b <= column_.length - 1; b++) {
                                    var columnrec = mcsCore.createTag('td');

                                    if (column_[b].hidden && column_[b].hidden == true) {
                                        columnrec.style.display = 'none';
                                    }
                                    if (typeof column_[b].fieldtemplate == 'function') { // valid template
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldtemplate(option_rec);
                                        column_[b].field = cell_.field;
                                        //if (tablePrrperty.sort.column == '') {
                                        //    var head_sort_temp = column_[b].sort == undefined ? '' : (column_[b].sort == 'asc' ? 'asc' : (column_[b].sort == 'desc' ? 'desc' : ''));
                                        //    if (head_sort_temp != '') {
                                        //        tablePrrperty.sort.column = (cell_.field == undefined) ? "" : cell_.field;
                                        //    }
                                        //}

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                    }
                                    else if (column_[b].fieldType == "delete") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-checkbox"> <img id="' + elementId + '"width="25px" style="cursor:pointer" src="../images/file_delete.png" onclick="' + column_[b].functionCall + '(' + id + ')" /></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;
 
                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "delete";
                                        column_[b].field = tempfield;

                                    }
                                    else if (column_[b].fieldType == "deleteWeek") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-checkbox"> <img id="' + elementId + '"width="25px" style="cursor:pointer" src="../images/file_delete.png" onclick="' + column_[b].functionCall + '(' + id + ')" /></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "deleteWeek";
                                        column_[b].field = tempfield;

                                    }
                                    else if (column_[b].fieldType == "checkbox") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "R_" + id;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-checkbox"> <input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" checked="checked"  value="' + id + '"></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "checkbox";
                                        column_[b].field = tempfield;

                                    }
                                    else if (column_[b].fieldType == "textbox") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var default_var = 0;
                                            var elementId = "R_" + id;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-checkbox"> <input class="process-ck text-right" id="' + elementId + '" type="text" name="textbox"  value="' + default_var + '" style=" width: 50px; "></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "textbox";
                                        column_[b].field = tempfield;

                                    }
                                    else if (column_[b].fieldType == "showGoal") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-check"> <img id="' + elementId + '"width="25px" style="cursor:pointer" src="../images/document_edit.png" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["EMP_CODE"] + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ')" /></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "showGoal";
                                        column_[b].field = tempfield;

                                    }
                                    else if (column_[b].fieldType == "showNG") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-check"> <img id="' + elementId + '"width="25px" style="cursor:pointer" src="../images/document_edit.png" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["RUN_ID"] + "'" + ',' + "'" + data_[rb]["EMP_CODE"] + "'" + ',' + "'" + data_[rb]["FULL_NAME"] + "'" + ',' + "'" + data_[rb]["DEPT_NAME"] + "'" + ',' + "'" + data_[rb]["NG"] + "'" + ')" /></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "showNG";
                                        column_[b].field = tempfield;

                                    }
                                    else if (column_[b].fieldType == "linkEmpCode") {
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            var empcode = dataParam.model.EMP_CODE == undefined ? id : dataParam.model.EMP_CODE;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + empcode + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + dataParam.model.WEEK + "'" + ',' + "'" + column_[b].head + "'" + ')" /></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkEmpCode";
                                        column_[b].field = "EMP_CODE";
                                    }
                                    else if (column_[b].fieldType == "linkEmpProcess") {
                                        var tempfield = column_[b].field;
                                        
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            var empcode = dataParam.model.EMP_CODE == undefined ? id : dataParam.model.EMP_CODE;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            //if (id > 0)
                                            var week = dataParam.model.WEEK == undefined ? 0 : dataParam.model.WEEK;
                                            record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["EMP_CODE"] + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + week + "'" + ',' + "'" + column_[b].head + "'" + ')" /></div>'
                                            //else
                                            //    record.cell.innerHTML = '<div class="g-check"> <label class ="label-check">' + id + '</label></div>'

                                            return record;
                                        }
                                        
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkEmpProcess";
                                        column_[b].field = tempfield;
                                    }
                                    else if (column_[b].fieldType == "linkVT") {
                                        var tempfield = column_[b].field;

                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            var empcode = dataParam.model.EMP_CODE == undefined ? id : dataParam.model.EMP_CODE;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            //if (id > 0)
                                            var week = dataParam.model.WEEK == undefined ? 0 : dataParam.model.WEEK;
                                            record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["DEPT_NAME"] + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + week + "'" + ',' + "'" + column_[b].field + "'" + ',' + "'" + column_[b].head + "'" + ')" /></div>'
                                            //else
                                            //    record.cell.innerHTML = '<div class="g-check"> <label class ="label-check">' + id + '</label></div>'

                                            return record;
                                        }

                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkVT";
                                        column_[b].field = tempfield;
                                    }
                                    else if (column_[b].fieldType == "linkVTWeld") {
                                        var tempfield = column_[b].field;

                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id;
                                            var empcode = dataParam.model.EMP_CODE == undefined ? id : dataParam.model.EMP_CODE;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            //if (id > 0)
                                            var week = dataParam.model.WEEK == undefined ? 0 : dataParam.model.WEEK;
                                            record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["DEPT_NAME"] + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + week + "'" + ',' + "'" + column_[b].field + "'" + ',' + "'" + column_[b].head + "'" + ')" /></div>'
                                            //else
                                            //    record.cell.innerHTML = '<div class="g-check"> <label class ="label-check">' + id + '</label></div>'

                                            return record;
                                        }

                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkVTWeld";
                                        column_[b].field = tempfield;
                                    }
                                    else if (column_[b].fieldType == "linkDate") {
                                        var tempfield = column_[b].field;
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + id + "_" + bf;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["EMP_CODE"] + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.WEEK + "'" + ',' + "'" + column_[b].head + "'" + ')" /></div>'

                                            return record;
                                        }
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkDate";
                                        column_[b].field = tempfield;
                                    }
                                    else if (column_[b].fieldType == "linkPartDetail1") {
                                        var tempfield = column_[b].field;
                                        
                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;
                                            var bh = column_[b].head == undefined ? "" : column_[b].head;;
                                            var id = data_[rb][bf];
                                            var elementId = "PC_" + bf;
                                            var empcode = dataParam.model.EMP_CODE == undefined ? id : dataParam.model.EMP_CODE;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            if (id.replace(',', '') >= 0) {
                                                var week = dataParam.model.WEEK == undefined ? 0 : dataParam.model.WEEK;
                                                record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["EMP_CODE"] + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + week + "'" + ',' + "'" + bh + "'" + ',' + "'" + bh + "'" + ')" /></div>'
                                            }
                                            else {
                                                record.cell.innerHTML = '<div class="text-right">'+id+'</div>';
                                            }
                                                //else
                                            //    record.cell.innerHTML = '<div class="g-check"> <label class ="label-check">' + id + '</label></div>'

                                            return record;
                                        }
                                        
                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkPartDetail1";
                                        column_[b].field = tempfield;
                                    }
                                    else if (column_[b].fieldType == "linkPartDetail2") {
                                        var tempfield = column_[b].field;

                                        column_[b].fieldType = function (record) {
                                            var bf = column_[b].field == undefined ? "" : column_[b].field;
                                            var bh = column_[b].head == undefined ? "" : column_[b].head;;
                                            var id = data_[rb][bf];
                                            var rowType = data_[rb]["ROW_TYPE"];
                                            var elementId = "PC_" + bf;
                                            var empcode = dataParam.model.EMP_CODE == undefined ? id : dataParam.model.EMP_CODE;
                                            //  record.cell.innerHTML = '<div class="g-check"><input class="process-ck" id="' + elementId + '" type="checkbox" name="checkbox" value="' + id + '"><label for="' + elementId + '"><span><span class="collapse">|</span></span></label></div>'
                                            if (rowType == "Footer") {
                                                var week = dataParam.model.WEEK == undefined ? 0 : dataParam.model.WEEK;
                                                record.cell.innerHTML = '<div class="g-check"> <input class="buttonNone" type="button" id="' + elementId + '" value ="' + id + '" onclick="' + column_[b].functionCall + '(' + "'" + data_[rb]["EMP_CODE"] + "'" + ',' + "'" + dataParam.model.MONTH + "'" + ',' + "'" + dataParam.model.YEAR + "'" + ',' + "'" + week + "'" + ',' + "'" + bh + "'" + ',' + "'" + bh + "'" + ')" /></div>'
                                            }
                                            else {
                                                record.cell.innerHTML = '<div class="text-right">' + id + '</div>';
                                            }
                                            //else
                                            //    record.cell.innerHTML = '<div class="g-check"> <label class ="label-check">' + id + '</label></div>'

                                            return record;
                                        }

                                        var option_rec = {
                                            context: rowrec_, field: '', data: data_[rb], cell: columnrec, event: {
                                                onclick: function (ev, me) {
                                                    columnrec.onclick = function () {
                                                        var cell_ = {}, currentRow = {};
                                                        var pr_ = this.parentNode.parentNode.parentNode.tHead.rows;

                                                        if (pr_ != undefined && pr_.length > 0) {

                                                            if (pr_[0].cells != undefined && pr_[0].cells.length > 0) {
                                                                for (var c = 0; c <= pr_[0].cells.length - 1; c++) {
                                                                    var key_ = pr_[0].cells[c].getAttribute("field") == null ? pr_[0].cells[c]['field'] : pr_[0].cells[c].getAttribute("field")
                                                                    cell_[key_] = c;
                                                                }
                                                                var pr_body = this.parentNode;

                                                                if (pr_body != undefined && pr_body.cells.length > 0) {
                                                                    var ic = 0;
                                                                    for (var cls in cell_) {
                                                                        try {
                                                                            if (typeof me == 'function') {
                                                                                var celltag_me = me(pr_body.cells[ic]);
                                                                                cell_[cls] = celltag_me == "" ? pr_body.cells[ic].innerHTML : celltag_me;
                                                                            } else {
                                                                                cell_[cls] = pr_body.cells[ic].innerHTML;
                                                                            }
                                                                        } catch (e) {
                                                                            cell_[cls] = "";
                                                                        }

                                                                        ic++
                                                                    }
                                                                    ic = 0;
                                                                }
                                                            }
                                                        }
                                                        ev(cell_);
                                                    }
                                                }
                                            }
                                        };
                                        if (option_rec.field != "" && option_rec.field == undefined) {
                                            column_[b].field = option_rec.field;

                                        }

                                        var cell_ = column_[b].fieldType(option_rec);
                                        column_[b].field = cell_.field;

                                        rowrec_.appendChild(cell_ == undefined ? columnrec : cell_.cell);
                                        column_[b].fieldType = "linkPartDetail2";
                                        column_[b].field = tempfield;
                                    }
                                    else {

                                        if (column_[b].align != undefined) {
                                            columnrec.style.textAlign = column_[b].align;
                                        }

                                        var bind_field = column_[b].field == undefined ? "" : column_[b].field;;
                                        var value_rec = data_[rb][bind_field];
                                        if (column_[b].format != undefined && typeof column_[b].format === "function") {
                                            value_rec = column_[b].format(value_rec);
                                        }
                                        if (column_[b].style != undefined) {
                                            if (typeof column_[b].style === "string") {
                                                columnrec.setAttribute('style', column_[b].style);
                                            } else {
                                                for (var icc in column_[b].style) {
                                                    columnrec.style[icc] = column_[b].style[icc];
                                                }
                                            }

                                        }
                                        columnrec.appendChild(mcsCore.createText(value_rec));
                                        rowrec_.appendChild(columnrec);
                                    }
                                }

                                if (tableOptions.event) {

                                    if (is.Function(tableOptions.event.onrowsloadcomcomplete)) {
                                        tableOptions.event.onrowsloadcomcomplete(rowrec_, data_[rb]);
                                    }
                                }
                                //function DataRowEvent(objRow)
                                //{
                                //    if (tableOptions.event.onclick != undefined) {
                                //        tableOptions.event.onclick(objRow);
                                //    }
                                //}
                                // rowrec_.setAttribute('field-data', JSON.stringify(data_[rb]))

                                //if (tableOptions.event.onclick != undefined) {
                                //    alert("b")
                                //    tableOptions.event.onclick = function (roWObj) {
                                rowrec_.setAttribute('index', rb);
                                rowrec_.onclick = function () {
                                    if (tableOptions.event.onclick != undefined) {
                                        tableOptions.event.onclick({ rows: this.cells, field: data_[this.getAttribute('index')] });
                                    }

                                }

                                if (tableOptions.event.onselectchange != undefined) {
                                    rowrec_.onclick = tableOptions.event.onselectchange({ rows: rowrec_.cells, field: data_[rb] })
                                }
                                tablebody_.appendChild(rowrec_);
                            }
                            // end create body


                            //create header
                            var tableHead_ = mcsCore.createTag("thead");
                            var rowHead = mcsCore.createTag("tr");
                            for (var r = 0; r <= column_.length - 1; r++) {
                                if (column_[r] == null || column_[r] == undefined) {
                                    console.log('column not match at' + r)
                                    break;
                                }

                                var head_bind_field = column_[r].field == undefined ? "" : column_[r].field;

                                var head_sort = tablePrrperty.sort.column == column_[r].field ? tablePrrperty.sort.dir : '';
                                var columnHead = mcsCore.createTag('TH');
                                columnHead.field = head_bind_field;
                                if (column_[r].cansort) {
                                    columnHead.sort = head_sort;
                                    columnHead.className = head_sort == 'asc' ? "sort_" + table_.id + ' orderasc' : (head_sort == 'desc' ? "sort_" + table_.id + ' orderdesc' : "sort_" + table_.id + ' orderboth')
                                }
                                if (column_[r].hidden && column_[r].hidden == true) {
                                    columnHead.style.display = 'none';
                                }
                                if (typeof column_[r].headtemplate == 'function') {
                                    columnHead = column_[r].headtemplate(columnHead);

                                } else {
                                    var head_bind_head = column_[r].head;
                                    columnHead.appendChild(mcsCore.createText(head_bind_head));
                                }

                                rowHead.appendChild(columnHead);
                            }
                            tableHead_.appendChild(rowHead);
                            //endcreate header


                            var tablefooter_ = mcsCore.createTag('tfoot');
                            var footrow_ = mcsCore.createTag('tr');
                            var footcolumn = mcsCore.createTag('td');
                            if (pageEnable == true) {
                                //create footer

                                footcolumn.colSpan = column_.length;
                                if (tableOptions.page.renderObject) {
                                    var dataSource = {}
                                    var strObj = tableOptions.page.renderObject.split('.');

                                    if (strObj.length > 1) {
                                        var obji = 0;

                                        while ((dataSource = responseTemp.data[strObj[obji]])) {

                                            if (obji == strObj.length - 1) {
                                                break;
                                            }
                                            i++;
                                        }
                                    } else {
                                        dataSource = responseTemp.data[tableOptions.page.renderObject];
                                    }
                                    datatemp = dataSource;
                                } else {
                                    datatemp = responseTemp.data;


                                }
                                //   var pageRenderData = 
                                //create footerTempalte pagging
                                var divPage_ = mcsCore.createTag('div');
                                divPage_.className = 'footbox';
                                var divPageBoxDesc = mcsCore.createTag('div');
                                divPageBoxDesc.style.cssFloat = 'right';
                                divPageBoxDesc.style.styleFloat = 'right';
                                divPageBoxDesc.style.display = 'inline-block';
                                var parag = mcsCore.createTag('span');


                                if (tableOptions.page) {
                                    if (tableOptions.page.rowtotal) {
                                        if (typeof tableOptions.page.rowtotal == "string") {
                                            console.log('row')
                                            parag.innerHTML = "ROW TOTAL :  " + tableOptions.page.rowtotal;
                                        } else {
                                            if (tableOptions.page.rowtotal.field) {


                                                if (datatemp) {
                                                    if (datatemp.length > 0) {
                                                        parag.innerHTML = "ROW TOTAL :  " + datatemp[0][tableOptions.page.rowtotal.field];
                                                    } else if (typeof datatemp == "object") {
                                                        parag.innerHTML = "ROW TOTAL :  " + datatemp[tableOptions.page.rowtotal.field];

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                                parag.className = 'footbox_rowtotal';
                                parag.id = 'footbox';
                                parag.style.display = 'inline-block';
                                parag.style.marginTop = '15px'
                                //parag.style.cssFloat = 'right';
                                divPageBoxDesc.appendChild(parag);
                                var divPageBoxNumber = mcsCore.createTag('div');
                                //divPageBoxNumber.style.styleFloat = 'right';
                                divPageBoxNumber.style.display = 'inline-block';
                                divPageBoxNumber.style.marginTop = '2px';



                                var pageSpan_First = mcsCore.createTag('span');
                                pageSpan_First.className = 'LightTable_pageNume_First' + " LightTable_pageNume_First" + table_.id;
                                pageSpan_First.innerHTML = '&#60;&#60;';
                                divPageBoxNumber.appendChild(pageSpan_First);
                                var pageSpan_ = mcsCore.createTag('span');
                                pageSpan_.className = 'LightTable_pageNume_Prvious' + ' LightTable_pageNume_Prvious' + table_.id;
                                pageSpan_.innerHTML = 'Previous';
                                divPageBoxNumber.appendChild(pageSpan_);
                                if (tableOptions.page) {
                                    if (tableOptions.page.pagetotal) {
                                        if (typeof tableOptions.page.pagetotal == 'string') {
                                            totalPage_ = tableOptions.page.pagetotal;
                                        } else {
                                            if (tableOptions.page.pagetotal.field) {
                                                if (datatemp) {
                                                    if (datatemp.length > 0) {
                                                        totalPage_ = datatemp[0][tableOptions.page.pagetotal.field] == 0 ? 1 : datatemp[0][tableOptions.page.pagetotal.field];
                                                    } else if (typeof datatemp == "object") {
                                                        totalPage_ = datatemp[tableOptions.page.pagetotal.field] == 0 ? 1 : datatemp[tableOptions.page.pagetotal.field];

                                                    }
                                                }
                                                //totalPage_ = datatemp[tableOptions.page.pagetotal.field] == 0 ? 1 : datatemp[tableOptions.page.pagetotal.field];
                                            }

                                        }
                                    }
                                }

                                if (totalPage_ != '0') {

                                    firstPage = firstPage <= 0 ? 1 : firstPage;
                                    var pageLoop = 1;
                                    for (var p = firstPage; p > 0 && p <= totalPage_ && pageLoop <= pageshow; p++) {

                                        var pageSpan_ = mcsCore.createTag('span');
                                        pageSpan_.className = p == tablePrrperty.page.current ? 'LightTable_pageNume LightTable_pageNume' + table_.id + ' LightTable_pageNumeActive' + ' LightTable_pageNumeActive' + table_.id : 'LightTable_pageNume' + ' LightTable_pageNume' + table_.id;
                                        pageSpan_.innerHTML = p;
                                        if (pageLoop == 1)
                                        { pageSpan_.firstPage = 'true'; } else if (pageLoop == totalPage_ || pageLoop == pageshow)
                                        { pageSpan_.lastPage = 'true' }
                                        divPageBoxNumber.appendChild(pageSpan_);
                                        pageLoop++;
                                    }
                                    pageLoop = 0;

                                    var pageSpan_ = mcsCore.createTag('span');
                                    pageSpan_.className = 'LightTable_pageNume_Next' + ' LightTable_pageNume_Next' + table_.id;
                                    pageSpan_.innerHTML = "Next";
                                    divPageBoxNumber.appendChild(pageSpan_);
                                    var pageSpan_Last = mcsCore.createTag('span');
                                    pageSpan_Last.className = 'LightTable_pageNume_Last' + ' LightTable_pageNume_Last' + table_.id;
                                    pageSpan_Last.innerHTML = '>>';
                                    divPageBoxNumber.appendChild(pageSpan_Last);



                                    divPage_.appendChild(divPageBoxDesc);
                                    divPage_.appendChild(divPageBoxNumber);
                                    footcolumn.appendChild(divPage_);
                                }
                                //end pagging



                                footrow_.appendChild(footcolumn);
                                tablefooter_.appendChild(footrow_);
                            }
                            //end create footer.



                            table_.appendChild(tableHead_);
                            table_.appendChild(tablebody_);

                            table_.appendChild(tablefooter_);
                            table_ = Genstyle(table_);
                            tableContext_.innerHTML = '';
                            tableContext_.appendChild(table_);
                            if (tableOptions.event) {
                                if (is.Function(tableOptions.event.ontableloadcomplete)) {
                                    tableOptions.event.ontableloadcomplete(table_);
                                }
                            }
                            //gen event page
                            if (!tableOptions.page) {
                                console.log('Table is not page generate  page property !. Please setting { page :{enable :true or false} }');
                                return;
                            }
                            if (pageEnable) {
                                var pageEvent = document.getElementsByClassName('LightTable_pageNume' + table_.id);
                                if (pageEvent.length > 0) {
                                    for (var ep = 0; ep <= pageEvent.length - 1; ep++) {



                                        pageEvent[ep].onclick = function () {



                                            if (this.lastPage) {
                                                firstPage = this.innerHTML;
                                                if (firstPage != 1 && firstPage != totalPage_) {

                                                    firstPage = firstPage - (pageshow - Math.ceil(pageshow / 2));

                                                } else if (firstPage == totalPage_) {

                                                    firstPage = firstPage - (pageshow - 1);
                                                }
                                            } else if (this.firstPage) {
                                                firstPage = this.innerHTML;
                                                if (firstPage != 1) {

                                                    firstPage = firstPage - (pageshow - Math.ceil(pageshow / 2));

                                                }
                                            }
                                            if (tableOptions.loaddata.data) {

                                                tablePrrperty.page.current = this.innerHTML;

                                                var clientparam = tableOptions.loaddata.data(tablePrrperty);

                                                RefeshTable(clientparam);
                                            }

                                        }

                                    }


                                }
                                var pagePrevious_ = document.getElementsByClassName('LightTable_pageNume_Prvious' + table_.id);
                                if (pagePrevious_.length > 0) {

                                    pagePrevious_[0].onclick = function () {

                                        var pre_ = this.parentNode.getElementsByClassName('LightTable_pageNumeActive' + table_.id);
                                        if (pre_.length > 0) {
                                            if (tableOptions.loaddata.data) {
                                                if (parseInt(pre_[0].innerHTML) > 1) {
                                                    var pre_first = this.parentNode.getElementsByClassName('LightTable_pageNume' + table_.id);
                                                    if (pre_first.length > 0) {
                                                        if (parseInt(pre_first[0].innerHTML) == (parseInt(pre_[0].innerHTML) - 1)) {
                                                            firstPage = (parseInt(pre_[0].innerHTML) - 1);
                                                            if (firstPage != 1) {

                                                                firstPage = firstPage - (pageshow - Math.ceil(pageshow / 2));

                                                            }
                                                        }
                                                    }

                                                    tablePrrperty.page.current = parseInt(pre_[0].innerHTML) - 1;

                                                    var clientparam = tableOptions.loaddata.data(tablePrrperty);

                                                    RefeshTable(clientparam);
                                                }
                                            }
                                        }

                                    }
                                }
                                var pagePrevious_ = document.getElementsByClassName('LightTable_pageNume_Next' + table_.id);
                                if (pagePrevious_.length > 0) {
                                    if (!pagePrevious_[0].onclick) {
                                        pagePrevious_[0].onclick = function () {

                                            var pre_ = this.parentNode.getElementsByClassName('LightTable_pageNumeActive' + table_.id);
                                            if (pre_.length > 0) {
                                                if (tableOptions.loaddata.data) {
                                                    if (parseInt(pre_[0].innerHTML) < parseInt(totalPage_)) {
                                                        var next_last = this.parentNode.getElementsByClassName('LightTable_pageNume' + table_.id);
                                                        console.log(next_last.length)
                                                        if (next_last.length > 0) {
                                                            mcs.readObj(next_last, function (r) { console.log(r) });
                                                            console.log((pageshow - 1))
                                                            if (parseInt(next_last[(next_last.length - 1)].innerHTML) == (parseInt(pre_[0].innerHTML) + 1)) {
                                                                firstPage = (parseInt(pre_[0].innerHTML) + 1);

                                                                if (firstPage != 1 && firstPage != totalPage_) {

                                                                    firstPage = firstPage - (pageshow - Math.ceil(pageshow / 2));

                                                                } else if (firstPage == totalPage_) {

                                                                    firstPage = firstPage - (pageshow - 1);
                                                                }

                                                            }
                                                        }
                                                        //   


                                                        tablePrrperty.page.current = parseInt(pre_[0].innerHTML) + 1;

                                                        var clientparam = tableOptions.loaddata.data(tablePrrperty);

                                                        RefeshTable(clientparam);
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                                var first_page_ = document.getElementsByClassName('LightTable_pageNume_First' + table_.id);
                                if (first_page_.length > 0) {
                                    first_page_[0].onclick = function () {
                                        firstPage = 1;
                                        tablePrrperty.page.current = 1;

                                        var clientparam = tableOptions.loaddata.data(tablePrrperty);

                                        RefeshTable(clientparam);
                                    }
                                }
                                var last_page_ = document.getElementsByClassName('LightTable_pageNume_Last' + table_.id);
                                if (last_page_.length > 0) {
                                    last_page_[0].onclick = function () {
                                        firstPage = totalPage_ - (pageshow - 1);
                                        tablePrrperty.page.current = totalPage_;

                                        var clientparam = tableOptions.loaddata.data(tablePrrperty);

                                        RefeshTable(clientparam);
                                    }
                                }
                            }

                            var sortHead_ = document.getElementsByClassName("sort_" + table_.id);
                            if (sortHead_.length > 0) {
                                for (var s = 0; s <= sortHead_.length - 1; s++) {
                                    sortHead_[s].onclick = function () {
                                        if (tableOptions.loaddata) {
                                            if (tableOptions.loaddata.data) {

                                                if (this.sort == 'asc') {
                                                    this.sort = 'desc';
                                                } else {
                                                    this.sort = 'asc';
                                                }

                                                tablePrrperty.sort.dir = this.sort;
                                                tablePrrperty.sort.column = this.field;
                                                var clientparam = tableOptions.loaddata.data(tablePrrperty);

                                                RefeshTable(clientparam);

                                            }
                                        } else {

                                        }
                                    }
                                }
                            }


                        } else {

                            tableNotFound();

                        }
                        clearTimeout(_settime);
                    }, 100);

                }

                RefeshTable(clientparam);


            } else {
                ///transform table
                if (is.ElementOf(tableContext_, "table")) {
                    alert("wait transform next version");
                } else {
                    alert("invalid argument");
                }
            }

        },
        dropdownlist: function (ddlOption) {
            var elContext = Contexts;
            var parameterData = {}, defaultValue = ddlOption.defaultValue, baseClass = ddlOption.baseClass, mapResult = ddlOption.result, displayTemplete = ddlOption.displayTemplete, itemTemplete = ddlOption.itemTemplete, multiselect = ddlOption.multiselect, multiselection = ddlOption.multiselection || false;
            var elParent = mcs.createTag('div')
            var elSelect = mcs.createTag('div');
            var elOptions = mcs.createTag('div');
            elSelect.id = ddlOption.id;
            elSelect.className = elSelect.className = 'dropdownlist';
            if (multiselection) {
                elSelect.setAttribute('multiselection', 'true');
            }

            elOptions.className = 'option-box';
            elOptions.style.overflowX = 'scroll';
            elOptions.style.height = '180px';

            var elButtonArrow = mcs.createTag('span');

            elButtonArrow.className = 'buttondown'

            if (is.Function(ddlOption.parameter)) {
                parameterData = ddlOption.parameter();
            }
            var activeText = mcs.createTag('div');

            activeText.style.marginRight = '30px';
            activeText.style.width = '100%';
            activeText.className = 'current-active';
            activeText.setAttribute('item-current-active', '')
            if (!is.Undefined(defaultValue)) {
                if (defaultValue.text) {
                    activeText.appendChild(mcs.createText((defaultValue.text || 'please select')));
                    activeText.setAttribute('itemcurrent-dropdown-value', (defaultValue.value || "#9999#"));
                    elSelect.appendChild(activeText);
                } else {
                    activeText.appendChild(mcs.createText((defaultValue || 'please select')));
                    activeText.setAttribute('itemcurrent-dropdown-value', (""));
                    elSelect.appendChild(activeText);
                }
            }
            if (ddlOption.data) {
                var response = { data: ddlOption.data }

                var dropDowElement = null;
                var statusDefalut = defaultValue == undefined ? false : true;
                renderItem(response, function (backEl) { dropDowElement = backEl; }, statusDefalut);
                if (response.data.length == 0) {
                    if (ddlOption.event) {
                        if (ddlOption.event.ondatanotfound) {
                            ddlOption.event.ondatanotfound();
                        }
                    }
                }

                elContext.innerHTML = "";
                elContext.appendChild(dropDowElement);
                if (ddlOption.event) {
                    if (ddlOption.event.onloadcomplete) {
                        ddlOption.event.onloadcomplete();
                    }
                }
            } else {
                mcs.service({ method: ddlOption.method, commandData: ddlOption.commandData, data: parameterData }, {
                    success: function (response) {
                        var dropDowElement = null;
                        if (response.data.length > 0) {
                            var statusDefalut = defaultValue == undefined ? false : true;
                            if (multiselection == false) {
                                if (is.Undefined(defaultValue)) {
                                    activeText.appendChild(mcs.createText(response.data[0][mapResult.text]));
                                    activeText.setAttribute('itemcurrent-dropdown-value', response.data[0][mapResult.value]);
                                    elSelect.appendChild(activeText);
                                }
                            } else {
                                elSelect.appendChild(activeText);
                            }
                            renderItem(response, function (backEl) { dropDowElement = backEl; }, statusDefalut);

                            elContext.innerHTML = "";
                            elContext.appendChild(dropDowElement);
                            if (ddlOption.event) {
                                if (ddlOption.event.onloadcomplete) {
                                    ddlOption.event.onloadcomplete();
                                }
                            }

                        } else {
                            renderItem(response, function (backEl) { dropDowElement = backEl; }, statusDefalut);

                            if (ddlOption.event) {
                                if (ddlOption.event.ondatanotfound) {
                                    ddlOption.event.ondatanotfound();
                                }
                            }

                            elContext.innerHTML = "";
                            elContext.appendChild(dropDowElement);
                            if (ddlOption.event) {
                                if (ddlOption.event.onloadcomplete) {
                                    ddlOption.event.onloadcomplete();
                                }
                            }
                        }


                    }, error: function (response) {
                        if (ddlOption.event) {
                            if (ddlOption.event.ondatanotfound) {
                                ddlOption.event.ondatanotfound();
                            }
                        }
                    }
                });
            }
            function RenderMultiSelection(itemRender) {
                var elSpanVal = itemRender.elementValue;//mcs.createTag('itemselect');
                elSpanVal.className = 'dropMultiValue';

                var elclose = itemRender.elementClose;// mcs.createTag('itemclose');
                elclose.style.border = '1px solid #E0E0E0';
                elclose.style.color = '#7B706F';
                elclose.style.padding = '0px 5px';
                elclose.style.fontSize = '13px';
                elclose.style.backgroundColor = '#fff';
                elclose.style.border = '1px solid #ddd';
                elclose.style.marginRight = '3px';
                elclose.innerHTML = 'X';
                elclose.style.height = '20px'
                elclose.style.position = "absolute";
                elclose.style.top = '0';
                elclose.style.right = '2px';
                elclose.style.bottom = '0';
                elclose.style.margin = 'auto';
                elclose.style.width = '8px';
                elclose.style.borderRadius = '5px';
                elclose.onclick = function () {

                    var targetRelation = itemRender.curentElementContext;
                    var elParent = targetRelation.parentNode;
                    var itemList = elParent.querySelectorAll('[item-dropdown-option]');
                    var targetActive = itemRender.activeText;
                    var tParent = this.parentNode.parentNode;

                    tParent.removeChild(this.parentNode);
                    targetRelation.style.display = '';
                    targetRelation.setAttribute('item-dropdown-option', '');
                    var itemValArrk = [];
                    var itemTextArrk = [];
                    var itemStrk = '';
                    for (var loopk = 0; loopk < itemList.length; loopk++) {
                        if (itemList[loopk].getAttribute('item-dropdown-option') == 'isSelect') {
                            var itemValk = itemList[loopk].getAttribute('item-dropdown-value');
                            var itemTextk = itemList[loopk].getAttribute('item-dropdown-text');


                            itemStrk += (itemStrk == "" ? "" : ",") + itemValk;
                            itemTextArrk.push(itemTextk);
                            itemValArrk.push(itemValk);


                        }
                    }
                    targetActive.setAttribute('itemcurrent-dropdown-value', itemStrk);
                    var itemdatak = { element: { displayText: itemRender.activeText, itemOption: itemRender.curentElementContext }, text: itemTextArrk, value: itemValArrk }
                    if (ddlOption.event) {
                        ddlOption.event.onchange(itemdatak);
                    }


                }

                elclose.onmouseover = function () {

                    this.style.color = '#fff';
                    this.style.backgroundColor = '#7B706F';
                    this.style.border = 'none';
                }
                elclose.onmouseleave = function () {
                    this.style.color = '#7B706F';
                    this.style.backgroundColor = '#fff';
                    this.style.border = '1px solid #ddd';
                }
                elSpanVal.style.display = 'inline-block';
                elSpanVal.style.position = 'relative';
                elSpanVal.style.color = '#000';
                //elSpanVal.style.top = '3px';
                elSpanVal.style.padding = '3px 25px 3px 5px';
                elSpanVal.style.fontSize = '13px';
                elSpanVal.style.backgroundColor = '#fff';
                elSpanVal.style.border = '1px solid #ddd';
                elSpanVal.style.marginBottom = '2px';
                elSpanVal.style.marginLeft = '1px';
                elSpanVal.appendChild(mcs.createText(itemRender.itemTextValue));
                elSpanVal.appendChild(elclose);

                var itemValArr = [];
                var itemTextArr = [];
                var itemStr = '';
                itemRender.activeText.appendChild(elSpanVal);
                if (itemRender.itemSelect.length > 0) {
                    for (var loop = 0; loop < itemRender.itemSelect.length; loop++) {
                        if (itemRender.itemSelect[loop].getAttribute('item-dropdown-option') == 'isSelect') {
                            var itemVal = itemRender.itemSelect[loop].getAttribute('item-dropdown-value');
                            var itemText = itemRender.itemSelect[loop].getAttribute('item-dropdown-text');


                            itemStr += (itemStr == "" ? "" : ",") + itemVal;
                            itemTextArr.push(itemText);
                            itemValArr.push(itemVal);


                        }
                    }
                } else {

                    //console.log(itemRender.itemValue)
                    itemStr += (itemStr == "" ? "" : ",") + itemRender.itemValue;
                    itemTextArr.push(itemRender.itemTextValue);
                    itemValArr.push(itemRender.itemValue);

                }
                itemRender.curentElementContext.style.display = 'none';
                itemRender.activeText.setAttribute('itemcurrent-dropdown-value', itemStr);
                var itemdata = { element: { displayText: itemRender.activeText, itemOption: itemRender.curentElementContext }, text: itemTextArr, value: itemValArr }
                if (ddlOption.event) {
                    ddlOption.event.onchange(itemdata);
                }

            }
            function renderItem(response, callb, defaultVal) {


                defaultVal = defaultVal || false;
                for (var m = 0; m < response.data.length; m++) {
                    var textItem = mcs.createTag('p');
                    textItem.className = 'option';
                    textItem.setAttribute('item-dropdown-option', '');
                    var valuData = "";
                    var valuText = "";

                    if (mapResult) {
                        valuData = response.data[m][mapResult.value];
                        valuText = response.data[m][mapResult.text];
                    } else {
                        valuData = response.data[m].Value;
                        valuText = response.data[m].Text;
                    }
                    textItem.setAttribute('item-dropdown-value', valuData);
                    textItem.setAttribute('item-dropdown-text', valuText);
                    textItem.appendChild(mcs.createText(valuText));

                    if (itemTemplete) {
                        var itemsource = { element: textItem, data: response.data[m] }
                        itemTemplete(itemsource);
                    }
                    if (defaultVal == false && m == 0) {
                        if (multiselection == true) {
                            textItem.setAttribute('item-dropdown-option', 'isSelect');

                            var elSpanVal = mcs.createTag('itemselect');
                            var elclose = mcs.createTag('itemclose');
                            elSelect.appendChild(activeText);
                            RenderMultiSelection({ activeText: activeText, curentElementContext: textItem, elementClose: elclose, elementValue: elSpanVal, itemTextValue: valuText, itemSelect: textItem, itemValue: valuData });
                        } else {
                            activeText.appendChild(mcs.createText(valuText));
                            activeText.setAttribute('itemcurrent-dropdown-value', valuData);
                            elSelect.appendChild(activeText);
                        }
                    }

                    textItem.onclick = function (_select, event) {


                        var curentElementContext = this;
                        var elParent = this.parentNode;
                        var itemSelect = elParent.querySelectorAll('[item-dropdown-option]');

                        if (itemSelect) {
                            if (itemSelect.length > 0) {
                                if (multiselect == true) {
                                    activeText.innerHTML = '';
                                    if (this.getAttribute('item-dropdown-option') == 'isSelect') {
                                        this.setAttribute('item-dropdown-option', '');
                                    } else {
                                        this.setAttribute('item-dropdown-option', 'isSelect');
                                    }
                                    var itemValArr = [];
                                    var itemTextArr = [];
                                    var itemStr = '';
                                    for (var loop = 0; loop < itemSelect.length; loop++) {
                                        if (itemSelect[loop].getAttribute('item-dropdown-option') == 'isSelect') {
                                            var itemVal = itemSelect[loop].getAttribute('item-dropdown-value');
                                            var itemText = itemSelect[loop].getAttribute('item-dropdown-text');
                                            var elSpanVal = mcs.createTag('lebel');
                                            elSpanVal.className = 'dropMultiValue';

                                            elSpanVal.style.color = '#000';
                                            elSpanVal.style.padding = '2px 5px';
                                            elSpanVal.style.fontSize = '13px';
                                            elSpanVal.style.backgroundColor = '#fff';
                                            elSpanVal.style.border = '1px solid #ddd';
                                            elSpanVal.style.marginRight = '3px';
                                            elSpanVal.appendChild(mcs.createText(itemText));
                                            activeText.appendChild(elSpanVal);
                                            itemStr += (itemStr == "" ? "" : ",") + itemVal;
                                            itemTextArr.push(itemText);
                                            itemValArr.push(itemVal);
                                            //if (loop <= itemSelect.length - 2)// before last index loop
                                            //{
                                            //    //  activeText.appendChild(mcs.createText(','));
                                            //    //itemStr += ",";
                                            //}


                                        }
                                    }
                                    activeText.setAttribute('itemcurrent-dropdown-value', itemStr);
                                    var itemdata = { element: null, text: itemTextArr, value: itemValArr }
                                    if (ddlOption.event) {
                                        ddlOption.event.onchange(itemdata);
                                    }

                                }
                                else if (multiselection == true) { // new
                                    this.setAttribute('item-dropdown-option', 'isSelect');
                                    var statMulti = this.getAttribute('eventMultiselect');
                                    if (statMulti) {
                                        //  activeText.innerHTML = '';
                                        this.removeAttribute('eventMultiselect');
                                        for (var loop = 0; loop < itemSelect.length; loop++) {
                                            if (itemSelect[loop].getAttribute('item-dropdown-option') == 'isSelect') {
                                                var itemVal = itemSelect[loop].getAttribute('item-dropdown-value');
                                                var itemText = itemSelect[loop].getAttribute('item-dropdown-text');
                                                var elSpanVal = mcs.createTag('itemselect');
                                                var elclose = mcs.createTag('itemclose');
                                                RenderMultiSelection({ itemTextValue: itemText, activeText: activeText, curentElementContext: itemSelect[loop], elementClose: elclose, elementValue: elSpanVal, itemSelect: itemSelect, itemValue: "" })
                                            }
                                        }

                                    } else {

                                        var itemVal = this.getAttribute('item-dropdown-value');
                                        var itemText = this.getAttribute('item-dropdown-text');
                                        var elSpanVal = mcs.createTag('itemselect');
                                        var elclose = mcs.createTag('itemclose');

                                        RenderMultiSelection({ itemTextValue: itemText, activeText: activeText, curentElementContext: curentElementContext, elementClose: elclose, elementValue: elSpanVal, itemSelect: itemSelect, itemValue: "" })
                                    }

                                }
                                else {
                                    for (var loop = 0; loop < itemSelect.length; loop++) {
                                        itemSelect[loop].setAttribute('item-dropdown-option', '');
                                    }
                                    this.setAttribute('item-dropdown-option', 'isSelect');
                                    var Curtext = this.getAttribute('item-dropdown-text');
                                    var itemdata = { element: this, text: Curtext, value: this.getAttribute('item-dropdown-value') }
                                    if (ddlOption.event) {
                                        ddlOption.event.onchange(itemdata);
                                    }
                                    activeText.innerHTML = itemdata.text;
                                    activeText.setAttribute('itemcurrent-dropdown-value', itemdata.value);
                                    elOptions.style.display = 'none';
                                    if (displayTemplete) {
                                        var displayItem = { element: activeText, text: itemdata.text, value: itemdata.value }
                                        displayTemplete(displayItem);

                                    }
                                }
                            }
                        }


                    }
                    elOptions.appendChild(textItem);

                }


                var expanStatus = false;
                function ExpenDropDown() {
                    elOptions.style.display = 'inherit';
                    elOptions.className = 'option-box open';
                }
                function UnExpenDropDown() {
                    var sett = setTimeout(function () {
                        if (!expanStatus) {
                            elOptions.style.display = 'none';
                            elOptions.className = 'option-box';
                            clearTimeout(sett);
                        }
                    }, 100);
                }
                elOptions.onmouseover = function () {
                    expanStatus = true;
                    elOptions.className = 'option-box open';
                }
                elOptions.onmouseleave = function () {
                    elOptions.className = 'option-box';
                    this.style.display = 'none';
                    expanStatus = false;
                }
                elButtonArrow.onclick = ExpenDropDown;
                activeText.onclick = ExpenDropDown;

                elButtonArrow.onmouseleave = UnExpenDropDown;
                activeText.onmouseleave = UnExpenDropDown;
                if (ddlOption.event) {
                    elSelect.onclick = function (e) {
                        //var oldind = this.selectedIndex;
                        //var text = this.options[this.selectedIndex].text;
                        //var itemdata = { text: text, value: this.value }
                        //if (itemdata.value == "#9999#") {

                        //    //this.size = this.options.length - 1;
                        //    return false;
                        //} else {

                        //    console.log(oldind)
                        //    elSelect.onchange = function () {
                        //        console.log(this.selectedIndex)
                        //        oldind = this.selectedIndex;
                        //    }
                        //}

                        //if (is.Function(ddlOption.event.onchange))
                        //    ddlOption.event.onchange(itemdata);
                    }

                }
                elSelect.appendChild(elOptions);
                elParent.appendChild(elSelect);
                elParent.appendChild(elButtonArrow)

                callb(elParent);



            }

        },
        //dropdownlist: function (ddlOption) {
        //    var parameterData = {}, defaultValue = ddlOption.defaultValue, baseClass = ddlOption.baseClass;
        //    var elParent = mcs.createTag('div')
        //    var elSelect = mcs.createTag('select');
        //    elSelect.id = ddlOption.id;
        //    elSelect.className = baseClass;
        //    var elButtonArrow = mcs.createTag('span'); 
        //    if(is.Function(ddlOption.dataParameter))
        //    {
        //        parameterData  = ddlOption.dataParameter();
        //    }
        //    if(!is.Undefined(defaultValue))
        //    { 
        //        var firstText = mcs.createTag('option');
        //        firstText.appendChild(mcs.createText(defaultValue));
        //        firstText.value ="#9999#";
        //        elSelect.appendChild(firstText)
        //    } 
        //    mcs.service({ method: ddlOption.method, commandData: ddlOption.commandData, data: parameterData }, {
        //        success: function (response) { 
        //            if (response.data.length > 0)
        //            { 
        //                for (var m = 0; m < response.data.length; m++)
        //                {
        //                    var textItem = mcs.createTag('option'); 
        //                    textItem.value = response.data[m].Value;
        //                    textItem.text = response.data[m].Text;
        //                    elSelect.appendChild(textItem); 
        //                }
        //            } 
        //        }, error: function (response) { 
        //        }
        //    }); 
        //    if (ddlOption.event)
        //    {
        //        elSelect.onclick = function (e) {
        //            var oldind = this.selectedIndex;
        //            var text = this.options[this.selectedIndex].text;
        //            var itemdata = { text: text, value: this.value }
        //            if (itemdata.value == "#9999#") { 
        //                //this.size = this.options.length - 1;
        //                return false;
        //            } else { 
        //                console.log(oldind) 
        //                elSelect.onchange = function ()
        //                {
        //                    console.log(this.selectedIndex)
        //                    oldind = this.selectedIndex;
        //                }
        //            } 
        //            if (is.Function(ddlOption.event.onchange))
        //            ddlOption.event.onchange(itemdata);
        //        } 
        //    } 
        //    elParent.appendChild(elSelect);
        //    elParent.appendChild(elButtonArrow)
        //    var elContext = Contexts; 
        //    elContext.appendChild(elParent);
        //},
        category: function () {

        },
        autocomplete: function (autoOption) {

            var parameterData = autoOption.parameterData || {}, elContext = Contexts; localevent = autoOption.event;
            var optionsControl = { startLength: autoOption.startLength || 2, inputdelay: autoOption.startLength || 200 }

            if (is.Function(autoOption.textTemplate)) {
                var textTemp = autoOption.textTemplate();
                textTemp.setAttribute('autocomp-input', autoOption.id);
                elContext.apprndChild(textTemp);
            } else {
                var autoTexts = elContext.querySelectorAll('[autocomp-input]');

                if (autoTexts.length == 0) {

                    var inputText = mcs.createTag('input');
                    inputText.type = 'text';
                    inputText.id = 'text_' + autoOption.id;
                    inputText.setAttribute('autocomp-input', autoOption.id);
                    var timer_;
                    inputText.onkeydown = function (event) {
                        {
                            var autoBoxsOnKey = elContext.querySelectorAll('[autocomp-box]');
                            if (autoBoxsOnKey.length > 0) {
                                event = (event || window.event)
                                var keyCoder = event.keyCode || event.which;
                                if (keyCoder == 38) {//up
                                    if (autoBoxsOnKey[0].children.length > 0) {
                                        var curentSelected = autoBoxsOnKey[0].children[0];
                                        var activeEle = elContext.querySelectorAll('[autocomp-selected]');
                                        if (activeEle.length > 0) {

                                            curentSelected = activeEle[0];

                                            for (var gg = 0; gg < autoBoxsOnKey[0].children.length; gg++) {
                                                autoBoxsOnKey[0].children[gg].removeAttribute('autocomp-selected');
                                                autoBoxsOnKey[0].children[gg].className = '';
                                            }

                                            if (curentSelected.previousSibling) {
                                                curentSelected.previousSibling.setAttribute('autocomp-selected', 'true');
                                                curentSelected.previousSibling.className = 'auto-selectd';

                                                if (is.Object(autoOption.event)) {

                                                    if (is.Function(autoOption.event.onselected)) {
                                                        var itemSelect = { input: inputText, item: curentSelected.previousSibling }
                                                        autoOption.event.onselected(itemSelect);
                                                        return;
                                                    }
                                                    return;
                                                }
                                                inputText.value = curentSelected.previousSibling.innerHTML;

                                            }

                                        } else {
                                            curentSelected.setAttribute('autocomp-selected', 'true');
                                            curentSelected.className = 'auto-selectd';
                                            if (is.Object(autoOption.event)) {

                                                if (is.Function(autoOption.event.onselected)) {
                                                    var itemSelect = { input: inputText, item: curentSelected }
                                                    autoOption.event.onselected(itemSelect);
                                                    return;
                                                }
                                                return;
                                            }
                                            inputText.value = curentSelected.innerHTML;
                                        }


                                    }
                                }
                                else if (keyCoder == 40) {

                                    if (autoBoxsOnKey[0].children.length > 0) {
                                        var curentSelected = autoBoxsOnKey[0].children[0];
                                        var activeEle = elContext.querySelectorAll('[autocomp-selected]');
                                        if (activeEle.length > 0) {

                                            curentSelected = activeEle[0];

                                            for (var gg = 0; gg < autoBoxsOnKey[0].children.length; gg++) {
                                                autoBoxsOnKey[0].children[gg].removeAttribute('autocomp-selected');
                                                autoBoxsOnKey[0].children[gg].className = '';
                                            }
                                            if (curentSelected.nextSibling) {
                                                curentSelected.nextSibling.setAttribute('autocomp-selected', 'true');
                                                curentSelected.nextSibling.className = 'auto-selectd';
                                                if (is.Object(autoOption.event)) {

                                                    if (is.Function(autoOption.event.onselected)) {
                                                        var itemSelect = { input: inputText, item: curentSelected.nextSibling }
                                                        autoOption.event.onselected(itemSelect);
                                                        return;
                                                    }
                                                    return;
                                                }
                                                inputText.value = curentSelected.nextSibling.innerHTML;
                                            }
                                        } else {
                                            curentSelected.setAttribute('autocomp-selected', 'true');
                                            curentSelected.className = 'auto-selectd';
                                            if (is.Object(autoOption.event)) {

                                                if (is.Function(autoOption.event.onselected)) {
                                                    var itemSelect = { input: inputText, item: curentSelected }
                                                    autoOption.event.onselected(itemSelect);
                                                    return;
                                                }
                                                return;
                                            }
                                            inputText.value = curentSelected.innerHTML;
                                        }


                                    }

                                }
                                else if (keyCoder == 13) {
                                    if (autoBoxsOnKey[0].parentNode);
                                    {
                                        autoBoxsOnKey[0].parentNode.removeChild(autoBoxsOnKey[0]);
                                    }

                                    return;
                                }
                                else {
                                    return;
                                }



                            }
                        }


                    }
                    inputText.onkeyup = function (event) {
                        event = (event || window.event)
                        var keyCoder = event.keyCode || event.which;
                        if (keyCoder != 40 && keyCoder != 38 && keyCoder != 13) {
                            var curentEle = this;
                            if (this.value.length >= optionsControl.startLength) {
                                clearTimeout(timer_)
                                timer_ = helper.delayRet(function () {

                                    if (is.Object(localevent)) {

                                        if (is.Function(localevent.onchange)) {
                                            var autoBoxs = elContext.querySelectorAll('[autocomp-box]');
                                            if (autoBoxs.length > 0) {
                                                elContext.removeChild(autoBoxs[0]);
                                            }
                                            var ret = localevent.onchange(curentEle);
                                            autoOption.parameterData = ret;
                                            LoadAutoComplete(autoOption);
                                        }
                                    }
                                }, optionsControl.inputdelay);
                            } else if (this.value == "") {
                                var autoBoxs = elContext.querySelectorAll('[autocomp-box]');
                                if (autoBoxs.length > 0) {
                                    elContext.removeChild(autoBoxs[0]);
                                }
                            }
                        }

                    }

                    elContext.appendChild(inputText);
                }
            }

            function LoadAutoComplete(paramets) {

                mcs.service({ method: paramets.method, commandData: paramets.commandData, data: paramets.parameterData }, {
                    success: function (response) {

                        if (response.data.length > 0) {
                            var elPar = mcs.createTag('div');
                            elPar.className = 'autocomplete-box';
                            elPar.style.top = (inputText.clientHeight + 3) + 'px';
                            elPar.setAttribute('autocomp-box', autoOption.id);
                            if (is.Function(autoOption.autoTemplate)) {
                                for (var m = 0; m < response.data.length; m++) {
                                    var dataResult = autoOption.autoTemplate(response.data[m], inputText.value);
                                    dataResult.onclick = function () {
                                        if (is.Object(autoOption.event)) {

                                            if (is.Function(autoOption.event.onselected)) {
                                                var itemSelect = { input: inputText, item: this }
                                                autoOption.event.onselected(itemSelect);
                                                var autoBoxsonclick = elContext.querySelectorAll('[autocomp-box]');
                                                if (autoBoxsonclick.length > 0) {
                                                    var parentBox = autoBoxsonclick[0].parentNode;
                                                    if (parentBox) {
                                                        parentBox.removeChild(autoBoxsonclick[0]);
                                                    }
                                                }
                                                return;
                                            }
                                            return;
                                        }
                                        inputText.value = this.innerHTML;
                                    }
                                    elPar.appendChild(dataResult)
                                    elContext.appendChild(elPar);
                                }
                            } else {

                                var elParagrap = mcs.createTag('p');
                                elParagrap.appendChild(mcs.createText());
                                elPar.appendChild(elParagrap);
                                elContext.appendChild(elPar);
                            }




                        }
                        if (is.Function(autoOption.event)) {

                            if (is.Function(autoOption.event.autocompleted)) {

                            }
                        }
                    },
                    error: function (response) {

                    }
                });
            }
            //LoadAutoComplete(autoOption);
        },
        tab: function (tabOption) {
            var id = Contexts.id;
            var class_ = tabOption.styleclass;
            var tab_nav = document.getElementById(id + '_tabs');
            if (tab_nav != null) {
                var tab_len = tab_nav.children.length
                for (var t = 0; t <= tab_len - 1; t++) {
                    tab_nav.children[t].children[0].onclick = function () {

                        var refestab = this.parentNode.parentNode.children;
                        for (var f = 0; f <= refestab.length - 1; f++) {
                            refestab[f].className = '';
                        }
                        this.parentNode.className = 'tab_active';

                        var tab_content = document.querySelectorAll(this.hash)[0];

                        var refestab_content = tab_content.parentNode.children;
                        for (var c = 0; c <= refestab_content.length - 1; c++) {
                            refestab_content[c].style.display = 'none';
                        }
                        tab_content.style.display = '';
                    }
                }
            } else {
                throw id + '_tabs' + ': is not support';
            }

        },
        notify: function (f, s, t) {
            var notiType = t;
            var notify = mcsCore.createTag('div');
            notify.id = 'InotifyID';
            var notifyTable = mcsCore.createTag('table');
            notifyTable.style.width = '100%';
            notifyTable.style.height = '100%';
            var notifyrow = mcsCore.createTag('tr');
            var notifyCol = mcsCore.createTag('td');
            var notifyText = mcsCore.createTag('p');
            notifyText.id = 'InotifyContent';
            notifyCol.appendChild(notifyText);
            notifyrow.appendChild(notifyCol);
            notifyTable.appendChild(notifyrow);
            notify.appendChild(notifyTable);
            Contexts.appendChild(notify);
            if (notiType == "S") {
                notify.className = 'InotifyOpen notify-success';

            } else if (notiType == "W") {
                notify.className = 'InotifyOpen notify-warning';
            }
            else if (notiType == "E") {
                notify.className = 'InotifyOpen notify-error';
            }

            notifyText.innerHTML = (s == null ? "Message Not Set" : s);
            if (document.attachEvent) {
                mcsAnimate.animation({ pattern: mcsAnimate.pattern.linear, duration: 0.2, delay: 3 }, function (r) {
                    var _l = (250 * r) - 250;
                    notify.style.right = _l + "px";
                }, function () {
                });
            }
            var step1 = setTimeout(function () {
                clearTimeout(step1);
                var step2 = setTimeout(function () {

                    if (notiType == "S") {
                        notify.className = 'InotifyClose notify-success';
                    } else if (notiType == "W") {
                        notify.className = 'InotifyClose notify-warning';
                    }
                    else if (notiType == "E") {
                        notify.className = 'InotifyClose notify-error';
                    }

                    clearTimeout(step2);
                    var step3 = setTimeout(function () {
                        f();
                        var OwnNotify = document.getElementById('InotifyID');
                        var parNotify = OwnNotify.parentNode;
                        parNotify.removeChild(OwnNotify);

                    }, 300);
                }, 1100);
            }, 2100);
        },

    }
    //Extend Control
    control.dropdownlist.item = {
        getItem: function (callback, sel) {

            var elCur = Contexts;
            var stackItem = [];
            if (sel) {
                var dropOption = elCur.querySelectorAll(sel);
                for (var loop = 0; loop < dropOption.length; loop++) {
                    if (callback) {
                        callback(dropOption[loop]);
                    }
                    stackItem.push(dropOption[loop]);
                }
                return stackItem;
            } else {
                var dropOption = elCur.querySelectorAll('[item-dropdown-value]');
                for (var loop = 0; loop < dropOption.length; loop++) {
                    if (callback) {
                        callback(dropOption[loop]);
                    }
                    stackItem.push(dropOption[loop]);
                }
                return stackItem;
            }
        },
        selected: function () {
            var eleCur = Contexts, value_ = '', text_ = '';
            //var currentActive = eleCur.querySelectorAll('[item-current-active]');
            //if (currentActive.length > 0) {

            //    value_ = currentActive[0].getAttribute('itemcurrent-dropdown-value');
            //    text_ = currentActive[0].innerHTML;
            //}
            var resulStack = [];
            var resultItem = { value: [], text: [] }
            var currentActive = eleCur.querySelectorAll('[item-dropdown-option]');
            if (currentActive.length > 0) {
                for (var yy = 0; yy < currentActive.length; yy++) {
                    var isSel = currentActive[yy].getAttribute('item-dropdown-option');
                    if (isSel == 'isSelect') {
                        var valRes = currentActive[yy].getAttribute('item-dropdown-value');
                        var txtRes = currentActive[yy].getAttribute('item-dropdown-text');

                        resultItem.value.push(valRes);
                        resultItem.text.push(txtRes);
                    }

                }

            }
            if (resultItem.value.length > 0) {
                if (resultItem.value.length == 1) {
                    return { text: resultItem.text[0], value: resultItem.value[0] };
                } else {

                    return resultItem;
                }
            } else {
                return { text: text_, value: value_ }
            }



        }, selectIndex: function () {

        },
        selectValue: function (s, callback) {

            var elCur = Contexts;
            var dropOption = elCur.querySelectorAll('[item-dropdown-value]');
            if (dropOption) {
                if (dropOption.length > 0) {
                    var itemStr = '';
                    var itemTextArr = [];
                    var itemValArr = [];
                    var currentActive = elCur.querySelectorAll('[item-current-active]');
                    currentActive[0].innerHTML = '';
                    var activeitemOption = null;
                    //  console.log(elCur.children[0].children[0].getAttribute('multiselection'))
                    if (elCur.children[0].children[0].getAttribute('multiselection') == "true") {
                        for (var loop = 0; loop < dropOption.length; loop++) {
                            if (is.String(s) || is.Number(s)) {
                                if (dropOption[loop].getAttribute('item-dropdown-value').toString().trim() == s)//.getAttribute('item-dropdown-option');
                                {
                                    activeitemOption = dropOption[loop];
                                    dropOption[loop].setAttribute('item-dropdown-option', 'isSelect');
                                    var textcurent = dropOption[loop].getAttribute('item-dropdown-text');
                                    //if (currentActive.length > 0) {
                                    //    currentActive[0].innerHTML = textcurent;
                                    //    currentActive[0].setAttribute('itemcurrent-dropdown-value', s);
                                    //}
                                    if (is.Function(callback)) {
                                        var selecItem = { element: currentActive[0], text: textcurent }
                                        callback(selecItem);

                                    }
                                } else {
                                    dropOption[loop].setAttribute('item-dropdown-option', '');
                                    dropOption[loop].style.display = '';
                                }
                            } else if (s.length > 0) {
                                var valItem = dropOption[loop].getAttribute('item-dropdown-value');


                                if (s.indexOf(valItem) >= 0) {
                                    dropOption[loop].setAttribute('item-dropdown-option', 'isSelect');
                                    var itemVal = dropOption[loop].getAttribute('item-dropdown-value');
                                    var itemText = dropOption[loop].getAttribute('item-dropdown-text');


                                    activeitemOption = dropOption[loop];
                                    itemStr += (itemStr == "" ? "" : ",") + itemVal;
                                    itemValArr.push(itemVal);


                                } else {
                                    dropOption[loop].setAttribute('item-dropdown-option', '');
                                    dropOption[loop].style.display = '';
                                }

                                if (currentActive.length > 0) {
                                    currentActive[0].setAttribute('itemcurrent-dropdown-value', itemStr);
                                }
                            }


                        }

                        /// call event Click 
                        if (activeitemOption) {
                            activeitemOption.setAttribute('eventMultiselect', 'true');
                            activeitemOption.click();
                        }


                    } else {
                        var activeItemClick = null;
                        for (var loop = 0; loop < dropOption.length; loop++) {
                            if (is.String(s) || is.Number(s)) {
                                if (dropOption[loop].getAttribute('item-dropdown-value').toString().trim() == s)//.getAttribute('item-dropdown-option');
                                {
                                    activeItemClick = dropOption[loop];
                                    dropOption[loop].setAttribute('item-dropdown-option', 'isSelect');
                                    var textcurent = dropOption[loop].getAttribute('item-dropdown-text');
                                    if (currentActive.length > 0) {
                                        currentActive[0].innerHTML = textcurent;
                                        currentActive[0].setAttribute('itemcurrent-dropdown-value', s);
                                    }
                                    if (is.Function(callback)) {
                                        var selecItem = { element: currentActive[0], text: textcurent }
                                        callback(selecItem);

                                    }
                                }
                            } else if (s.length > 0) {
                                var valItem = dropOption[loop].getAttribute('item-dropdown-value');


                                if (s.indexOf(valItem) >= 0)//.getAttribute('item-dropdown-option');
                                {
                                    activeItemClick = dropOption[loop];
                                    dropOption[loop].setAttribute('item-dropdown-option', 'isSelect');
                                    var itemVal = dropOption[loop].getAttribute('item-dropdown-value');
                                    var itemText = dropOption[loop].getAttribute('item-dropdown-text');

                                    var elSpanVal = mcs.createTag('label');
                                    elSpanVal.className = 'dropMultiValue';
                                    elSpanVal.style.color = '#000';
                                    elSpanVal.style.padding = '2px 5px';
                                    elSpanVal.style.fontSize = '13px';
                                    elSpanVal.style.backgroundColor = '#fff';
                                    elSpanVal.style.border = '1px solid #ddd';
                                    elSpanVal.style.marginRight = '3px';
                                    elSpanVal.appendChild(mcs.createText(itemText));


                                    itemStr += (itemStr == "" ? "" : ",") + itemVal;
                                    itemValArr.push(itemVal);

                                    //if (loop <= s.length - 2)// before last index loop
                                    //{
                                    //    //
                                    if (currentActive.length > 0) {
                                        currentActive[0].appendChild(elSpanVal);
                                        //  currentActive[0].appendChild(mcs.createText(','));
                                    }
                                    // 
                                    // itemStr += ",";
                                    //     }

                                }

                                if (currentActive.length > 0) {
                                    currentActive[0].setAttribute('itemcurrent-dropdown-value', itemStr);
                                }
                            }
                            if (activeItemClick)
                            { activeItemClick.click(); }


                        }
                    }


                }
            }

        },
        selectText: function (s) {
            var elCur = Contexts;
            var dropOption = elCur.querySelectorAll('[item-dropdown-text]');
            if (dropOption) {
                if (dropOption.length > 0) {
                    var itemStr = '';
                    var itemTextArr = [];
                    var itemValArr = [];
                    var currentActive = elCur.querySelectorAll('[item-current-active]');

                    for (var loop = 0; loop < dropOption.length; loop++) {
                        if (dropOption[loop].getAttribute('item-dropdown-text').toString().trim() == s)//.getAttribute('item-dropdown-option');
                        {
                            dropOption[loop].setAttribute('item-dropdown-option', 'isSelect');
                            var textcurent = dropOption[loop].getAttribute('item-dropdown-text');
                            var valcurent = dropOption[loop].getAttribute('item-dropdown-value');
                            if (currentActive.length > 0) {
                                currentActive[0].innerHTML = textcurent;
                                currentActive[0].setAttribute('itemcurrent-dropdown-value', valcurent);
                            }
                            dropOption[loop].click();
                        }
                    }
                }
            }


        }
    }
    //End Extend


    mcsCore.framework = mcsCore.prototype = {
        int: function (a) {

            try {
                this.ElementContext = {};
                Contexts = a;

                if (is.Element(a)) {
                    this.ElementContext = a;
                    Contexts = this.ElementContext;
                    return this;
                }
                else if (is.Array(a) || is.Object(a)) {
                    return new DataQuery(a);
                } else {
                    if (is.String(a)) {
                        if (a.substring(0, 1) != "#" && a.substring(0, 1) != "." && a.substring(0, 1) != "[" && a.substring(0, 1) != "*") {
                            var bCon = document.querySelector(a);
                            if (bCon == null) {
                                console.log("id " + a + " " + "is not support")
                                throw " selector is not support";
                            }
                            if (is.Element(bCon)) {

                                this.ElementContext = bCon;
                                Contexts = this.ElementContext;
                                return this;
                            } else {
                                a = "#" + a;
                            }
                        }
                        var nCon = null;
                        if (a.substring(0, 1) == ".") {

                            var clas_ = a.substring(1, a.length);

                            nCon = document.getElementsByClassName(clas_);
                            if (nCon.length > 0) {
                                this.ElementContext = nCon;
                                Contexts = this.ElementContext;
                                return this;
                            } else {
                                Contexts = [];
                                return this;
                            }

                        } else if (a.substring(0, 1) == "[" || a.substring(0, 1) == "*") {
                            nCon = document.querySelectorAll(a);
                        } else {
                            nCon = document.querySelector(a);
                        }
                        if (nCon) {
                            //if (is.Object(nCon)) {

                            //} else {

                            //}
                            if (is.Element(nCon)) {
                                if (is.Object(nCon.style)) {
                                    this.ElementContext = nCon;
                                } else {
                                    this.ElementContext = document.createElement("");
                                }

                            } else {

                                this.ElementContext = nCon;
                            }


                        }

                        Contexts = this.ElementContext;
                        if (!is.Element(Contexts)) {
                            if (Contexts.length >= 0) {
                                return this;
                            }
                            console.log("id " + a + " " + "is not support")
                            throw " selector is not support";
                        }
                        return this;
                    } else {
                        Contexts = null;
                        return null;
                    }
                }

            } catch (e) {
                console.log(e)
                console.log(a);
                this.ElementContext = undefined;
                Contexts = undefined;
                return;
            }
        }, isElement: is.Element,
        isElementOf: is.ElementOf,
        isArray: is.Array,
        isFunction: is.Function,
        isNode: is.Node,
        isUndefined: is.Undefined,
        isObject: is.Object,
        isWhitespace: is.Whitespace


    };

    ///func helper

    function foreachs(obj, fuc) {
        var dataObj = obj;
        //console.log(dataObj)
        var len = dataObj.length;

        for (var k = 0; k < len ; k++) {
            fuc(dataObj[k]);
            k = (dataObj.length < len ? (dataObj.length == 0 ? k = len : (k - 1)) : (dataObj.length == len ? k : (k + 1)));
        }

    }
    ///end helper






    // Region Ext 

    mcsCore.mapProp = function (m) {
        if (typeof m == "object") {
            var ar = arguments.length, funcContext = m;
            for (var r in funcContext) {
                if (r != undefined && r != null) {

                    mcsCore[r] = funcContext[r];
                }
            }
        }
        return this;
    }

    mcsCore.mapMethod = function (m) {
        //return new mapMethodfunc(m);
        // array shift delete first arr
        // pop delete last arr
        // push insert arr

        if (typeof m == "object") {
            var ar = arguments.length, funcContext = m;

            for (var r in funcContext) {
                if (r != undefined && r != null) {

                    mcsCore.framework[r] = funcContext[r];

                }
            }

        }

        return this;
    }
    //EndRegion
    mcsCore.IE8 = function (s) {
        var context_;
        if (typeof s == "string")
        { context_ = document.getElementById(s); } else
        { context_ = s; }
        return new mcsCore.IE8.core(context_);
    }
    mcsCore.IE8.core = function (s) {
        this.context_ = s;
        return this;
    }
    mcsCore.IE8.core.prototype = {
        ImgHover: function (c) {
            if (this.context_ != null) {
                if (this.context_.tagName == "IMG") {
                    var sim_ = this.context_.src;
                    var simTemp_ = sim_;
                    if (sim_ != null) {
                        sim_ = sim_.split('/');
                        if (sim_.length > 0) {
                            sim_ = sim_[sim_.length - 1];
                            sim_ = sim_.split('.');
                            if (sim_.length > 0) {
                                var pic_ = sim_[0];
                                var imgHover_ = simTemp_.replace(pic_, pic_ + "_hover");

                                this.context_.onmouseover = function () {
                                    this.src = imgHover_;
                                    if (c) {
                                        c.style.color = '#f42409';
                                    }
                                }
                                this.context_.onmouseout = function () {
                                    this.src = simTemp_;
                                    if (c) {
                                        c.style.color = '#fff';
                                    }
                                }
                            }
                        }
                    }

                } else {
                    if (c && c.tagName == "IMG") {
                        var sim_ = c.src;
                        var simTemp_ = sim_;
                        if (sim_ != null) {
                            sim_ = sim_.split('/');
                            if (sim_.length > 0) {
                                sim_ = sim_[sim_.length - 1];
                                sim_ = sim_.split('.');
                                if (sim_.length > 0) {
                                    var pic_ = sim_[0];
                                    var imgHover_ = simTemp_.replace(pic_, pic_ + "_hover");

                                    this.context_.onmouseover = function () {

                                        if (c) {
                                            c.src = imgHover_;
                                            this.style.color = '#f42409';

                                        }
                                    }
                                    this.context_.onmouseout = function () {

                                        if (c) {
                                            c.src = simTemp_;
                                            this.style.color = '#fff';
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }

        },
        ImgHoverWith: function (El) {
            if (this.context_ != null) {
                if (this.context_.tagName == "IMG") {
                    var sim_ = this.context_.src;
                    var currentContext = this.context_;
                    var simTemp_ = sim_;
                    if (sim_ != null) {
                        sim_ = sim_.split('/');
                        if (sim_.length > 0) {
                            sim_ = sim_[sim_.length - 1];
                            sim_ = sim_.split('.');
                            if (sim_.length > 0) {
                                var pic_ = sim_[0];
                                var imgHover_ = simTemp_.replace(pic_, pic_ + "_hover");
                                El.onmouseover = function () {
                                    currentContext.src = imgHover_;
                                }
                                this.context_.onmouseover = function () {
                                    this.src = imgHover_;
                                }
                                El.onmouseout = function () {
                                    currentContext.src = simTemp_;
                                }
                                this.context_.onmouseout = function () {
                                    this.src = simTemp_;
                                }
                            }
                        }
                    }

                } else { throw new NotsupportException("ImgHover :" + "is not support"); }
            }

        },
        DataBind: function () {
            function foo() {
                window.addEventListener("keydown", this, false); // control all event int at use in function
            }

            foo.prototype.bar = "foobar";

            foo.prototype.handleEvent = function (e) {
                document.write(e.type);
                //console.log(e.type);
                //console.log(this.bar);
            };
        }
    };


    //Region  Exten
    mcsCore.mapMethod({
        WinProp: {
            getWidth: function () {

                if (self.innerHeight) {
                    return self.innerWidth;
                }

                if (document.documentElement && document.documentElement.clientHeight) {
                    return document.documentElement.clientWidth;
                }

                if (document.body) {
                    return document.body.clientWidth;
                }
            },
            getHeight: function () {
                if (self.innerHeight) {
                    return self.innerHeight;
                }

                if (document.documentElement && document.documentElement.clientHeight) {
                    return document.documentElement.clientHeight;
                }

                if (document.body) {
                    return document.body.clientHeight;
                }
            },
            get: function () {
                var myWidth = 0, myHeight = 0;
                if (typeof (window.innerWidth) == 'number') {
                    //Non-IE
                    myWidth = window.innerWidth;
                    myHeight = window.innerHeight;
                } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                    //IE 6+ in 'standards compliant mode'
                    myWidth = document.documentElement.clientWidth;
                    myHeight = document.documentElement.clientHeight;
                } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                    //IE 4 compatible
                    myWidth = document.body.clientWidth;
                    myHeight = document.body.clientHeight;
                }
                return { width: myWidth, height: myHeight }
            }
        }
    });
    mcsCore.mapMethod({
        notifyField: function () {

            var sp_notify = mcsCore.createTag('span');
            if (document.body.setAttribute) {
                sp_notify.setAttribute("name", 'notify_requiredfield')
            } else { sp_notify.name = 'notify_requiredfield' }

            sp_notify.style.color = '#f63307';
            sp_notify.innerHTML = '!'
            sp_notify.style.fontSize = '20px';
            sp_notify.style.fontWeight = 'bolder';
            sp_notify.style.top = (parseInt(mcsCore(Contexts).Position().top) - 20) + 'px';
            sp_notify.style.left = parseInt(mcsCore(Contexts).Position().left) + (parseInt(mcsCore(Contexts).Width() + 10)) + 'px';
            sp_notify.style.position = 'fixed';

            if (Contexts.nodeName != undefined) {
                //Contexts.parentNode.insertBefore(sp_notify, element);

                Contexts.parentNode.insertBefore(sp_notify, Contexts.nextSibling);

                var wait_ = setTimeout(function () {
                    //  var tag_nameNotify = document.getElementsByName('notify_requiredfield'); 
                    var parNo = sp_notify.parentNode;
                    parNo.removeChild(sp_notify);
                    //if (tag_nameNotify.length > 0) {
                    // for (var n = 0; n <= tag_nameNotify.length - 1; n++) {
                    //var parNo = tag_nameNotify[n].parentNode;
                    //parNo.removeChild(tag_nameNotify[n]);

                    //}
                    // }
                    clearTimeout(wait_);
                }, 3000);
            }
        }
    });
    mcsCore.mapMethod({
        Value: function (t) {

            if (is.String(t) || is.Number(t)) {

                if (Contexts != null) {
                    if (Contexts.nodeName) {
                        if (Contexts.nodeName.toLowerCase() == 'input') {

                            Contexts.value = t;
                        } else if (Contexts.nodeName.toLowerCase() == 'span' || Contexts.nodeName.toLowerCase() == 'p'
                            || Contexts.nodeName.toLowerCase() == 'label') {

                            Contexts.innerHTML = t;
                        } else if (Contexts.nodeName.toLowerCase() == 'textarea') {
                            Contexts.value = t;
                        }


                    } else {

                    }
                } else {

                    return "";
                }
            }
            else if (is.Function(t)) {



            }
            else {

                if (Contexts != null) {
                    if (Contexts.nodeName) {
                        if (Contexts.nodeName.toLowerCase() == 'input') {

                            return Contexts.value;
                        } else if (Contexts.nodeName.toLowerCase() == 'span' || Contexts.nodeName.toLowerCase() == 'p'
                            || Contexts.nodeName.toLowerCase() == 'label') {
                            return Contexts.innerHTML;
                        } else if (Contexts.nodeName.toLowerCase() == 'textarea') {
                            return Contexts.value;
                        }
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            }

        }
    });
    mcsCore.mapMethod({
        Position: function () {
            if (is.Element(Contexts)) {
                var thisTag = Contexts
                var curentTag = thisTag.getBoundingClientRect();
                var scrtop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
                var scrleft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
                var clientTOP = document.documentElement.clientTop || document.body.clientTop || 0;
                var clientLEFT = document.documentElement.clientLeft || document.body.clientLeft || 0;

                var tops = curentTag.top + scrtop - clientTOP;
                var lefts = curentTag.left + scrleft - clientLEFT;

                return { top: tops, left: lefts };
            } else {
                var element = document.getElementById(id);
                var rect = element.getBoundingClientRect();
                var elementLeft, elementTop; //x and y
                var scrollTop = document.documentElement.scrollTop ?
                                document.documentElement.scrollTop : document.body.scrollTop;
                var scrollLeft = document.documentElement.scrollLeft ?
                                 document.documentElement.scrollLeft : document.body.scrollLeft;
                elementTop = rect.top + scrollTop;
                elementLeft = rect.left + scrollLeft;
                return { top: elementTop, left: elementLeft }
            }
        }
    });
    mcsCore.mapMethod({
        //addClass: cssClass.addClass,
        //removeClass: cssClass.removeClass,
        //switchClass: cssClass.switchClass,
        classAdd: cssClass.addClass,
        classDelayAdd: cssClass.addClassDelay,
        classRemove: cssClass.removeClass,
        classDelayRemove: cssClass.removeClassDelay,
        classReplace: cssClass.replaceClass,
        classDelayReplace: cssClass.replaceClassDelay,
        classDelayAddOrReplace: cssClass.addOrReplaceClassDelay,
        css: cssClass.cssStyle,
        classed: cssClass.classed,
        classIs: cssClass.classIs,
        cssDelay: cssClass.cssStyleDelay
    });
    mcsCore.mapMethod({
        displayHide: function () {
            if (Contexts.length) {
                for (var t = 0; t <= Contexts.length - 1; t++) {
                    Contexts[t].style.display = 'none';
                }
            } else {
                Contexts.style.display = 'none';
            }
            return this;

        }, displayShow: function (d) {
            if (Contexts.length) {
                for (var t = 0; t <= Contexts.length - 1; t++) {
                    Contexts[t].style.display = (d || '');
                }
            } else {
                Contexts.style.display = (d || '');
            }
            return this;

        }
    });
    mcsCore.mapMethod({
        html: function (h) {
            if (is.Undefined(h)) {
                return Contexts.innerHTML;
            } else {
                Contexts.innerHTML = h;
            }
        }
    });
    mcsCore.mapMethod({
        insertElementBefore: function (newEl, elex) {
            var elContext = Contexts;
            elContext.insertBefore(newEl, elex);
        }
    });

    mcs.mapMethod({
        queryContext: function (c, f) {
            var eleCur = Contexts;

            var nElement = eleCur.querySelectorAll(c);
            var elStack = [];

            if (nElement) {

                if (nElement.length > 0) {
                    if (nElement.length == 1) {
                        if (f) {
                            f(nElement[0]);
                        }
                        Contexts = nElement[0];
                        this.ElementContext = nElement[0];
                    } else {
                        for (var elm = 0; elm < nElement.length; elm++) {
                            if (f) {
                                f(nElement[elm]);
                            }
                            elStack.push(nElement[elm]);
                        }
                        Contexts = elStack;
                        this.ElementContext = elStack;
                    }


                } else {
                    if (f) {
                        f(nElement);
                    }
                    Contexts = nElement;
                    this.ElementContext = nElement;
                }




            }


            return this;
        }
    });

    mcsCore.mapMethod({
        select: Selected,
        selected: function (value) {
            var op = Contexts.options;
            if (op.length > 0) {
                for (var i = 0; i <= op.length - 1; i++) {
                    if (op[i] != undefined && op[i].value == value) {
                        Contexts.selectedIndex = i;
                        break;
                    }
                }
            }
        },
        focus: function () {
            var ele = Contexts;
            ele.focus();
        }
    });
    mcsCore.mapMethod({
        elementValidate: function (f) {
            if (Contexts) {
                f(true);
            } else {
                f(false);
                console.log('element not found');
            }
        }
    });
    mcsCore.mapMethod({ Width: PropEle.Width, Height: PropEle.Height });
    //mcsCore.mapMethod({ set: {} });
    mcsCore.mapMethod({
        //eventAdd: function (evName, func) {
        //    switch (evName) {
        //        case "keydown":
        //            if (!Contexts.onkeydown) {
        //                Contexts.onkeydown = func;
        //            }
        //            break;
        //        case "keypress":
        //            if (!Contexts.onkeypress) {
        //                Contexts.onkeypress = func;
        //            }
        //            break;
        //        case "click":
        //            document.addEventListener("onclick", func, false);
        //            break;
        //        case "mouseover":
        //            if (!Contexts.onmouseover) {
        //                Contexts.onmouseover = func;
        //            }
        //            break;
        //        case "mouseleave":
        //            if (!Contexts.onmouseleave) {
        //                Contexts.onmouseleave = func;
        //            }
        //            break;
        //        case "scroll":
        //            if (!Contexts.onscroll) {
        //                Contexts.onscroll = func;;
        //            }
        //            break;

        //    }
        //},
        eventOn: function (evName, func, delay) {
            switch (evName) {
                case "keydown":

                    if (!Contexts.onkeydown) {
                        if (delay) {
                            mcs.delay(function () {
                                Contexts.onkeydown = func;
                            }, delay);
                        } else {
                            Contexts.onkeydown = func;
                        }

                    }
                    break;
                case "keyEnter":
                    if (!Contexts.onkeydown) {
                        Contexts.onkeydown = function (event) {
                            event = (event || window.event)
                            var keyCoder = event.keyCode || event.which;
                            if (keyCoder == 13) {
                                func();
                            }
                        }

                    }
                    break;
                case "keypress":
                    if (!Contexts.onkeypress) {
                        Contexts.onkeypress = func;
                    }
                    break;
                case "click":
                    if (!Contexts.onclick) {
                        Contexts.onclick = func;
                    }
                    break;
                case "mouseover":
                    if (!Contexts.onmouseover) {
                        Contexts.onmouseover = func;
                    }
                    break;
                case "mouseleave":
                    if (!Contexts.onmouseleave) {
                        Contexts.onmouseleave = func;
                    }
                    break;
                case "scroll":
                    if (!Contexts.onscroll) {
                        Contexts.onscroll = func;;
                    }
                    break;

            }
            return { click: "", keydown: "", keypress: "", mouseover: "", mouseleave: "", scroll: "", keyEnter: "" };
        },
        event:
        {
            onClick: function (func) {
                if (!Contexts.onclick) {
                    Contexts.onclick = func;
                }
            },
            onKeydown: function () {
                if (!Contexts.onkeydown) {
                    Contexts.onkeydown = func;
                }
            },
            onKeypress: function () {
                if (!Contexts.onkeypress) {
                    Contexts.onkeypress = func;
                }
            },
            onMouseover: function () {
                if (!Contexts.onmouseover) {
                    Contexts.onmouseover = func;
                }
            },
            onMouseleave: function () {
                if (!Contexts.onmouseleave) {
                    Contexts.onmouseleave = func;
                }
            },
            onScroll: function () {
                if (!Contexts.onscroll) {
                    Contexts.onscroll = func;
                }
            }

        }
    });
    mcsCore.mapMethod(Attrs);
    mcsCore.mapMethod({ getSize: PropEle });
    mcsCore.mapMethod(EventValid);
    mcsCore.mapMethod({
        Disable: function (f) {

            if (Contexts.length > 0) {
                for (var loop = 0; loop < Contexts.length; loop++) {
                    if (Contexts[loop].nodeType) {
                        var curEle = Contexts[loop];
                        curEle.disabled = f;
                    }
                }

            } else {
                Contexts.disabled = f;
            }

        }
    });
    mcsCore.mapMethod({ appendChild: function (el) { Contexts.appendChild(el) }, removeChild: function (el) { Contexts.removeChild(el); } });
    mcsCore.mapMethod({
        Imgload: function (path, callback) {
            var context = Contexts;
            var img = new Image();
            img.onload = function () { context.src = img.src; callback(); context = null; };
            img.onerror = function () {
                context.src = "";
                callback();
            }
            console.log(path)
            img.src = path;

        }
    });
    mcsCore.mapMethod({ Control: control });
    mcsCore.mapMethod({
        foreach: function (call) {
            var currentContext = Contexts;
            foreachs(currentContext, function (res) {
                call(res);
            });
            //for (var t = 0; t <= currentContext.length - 1; t++) {

            //}
        }
    });
    mcsCore.mapMethod({
        htmlText: function (strHtml) {
            var element = Contexts;
            if (is.Undefined(strHtml)) {
                return element.innerHTML;
            } else {
                element.innerHTML = strHtml;
            }
        }
    });
    mcsCore.mapMethod({
        input: {
            cancelKey: function (k) {

                var elCur = Contexts;
                elCur.onkeypress = function (event) {
                    event = (event || window.event)
                    var keyCoder = event.keyCode || event.which;
                    var numberKey = String.fromCharCode(keyCoder);
                    if (is.Array(k)) {
                        for (var ke = 0; ke < k.length; ke++) {
                            if (k[ke] == numberKey) {
                                return false;
                            }
                        }
                    } else {
                        if (is.String(k)) {
                            if (k == numberKey) {
                                return false;
                            }
                            else {
                                return true;
                            }
                        } else {
                            return true;
                        }
                    }
                    return true;
                }

            },
            cancelInput: function (k, kc) {
                //k is key =  a b c 
                //kc is keycode = 102 20  45
                var elCur = Contexts;
                elCur.onkeydown = function (event) {
                    event = (event || window.event)
                    var keyCoder = event.keyCode || event.which;
                    var ctrlDown = event.ctrlKey || event.metaKey
                    if (ctrlDown) {
                        return false;
                    }
                    var numberKey = helper.keyCodeDB(keyCoder);//String.fromCharCode(keyCoder);
                    if (k) {
                        if (is.Array(k)) {
                            for (var ke = 0; ke < k.length; ke++) {
                                if (k[ke] == numberKey) {
                                    return true;
                                }
                            }
                        } else {
                            if (is.String(k)) {
                                if (k == numberKey) {
                                    return true;
                                }
                                else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                    } else if (kc) {
                        if (is.Array(kc)) {
                            for (var ke = 0; ke < kc.length; ke++) {
                                if (kc[ke] == keyCoder) {
                                    return true;
                                }
                            }
                        } else {
                            if (is.String(kc)) {
                                if (kc == keyCoder) {
                                    return true;
                                }
                                else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                    }

                    return false;
                }

            },
            allowInputRegex: function (regex) {
                // pattern test "98754a".match(rex)

                // /^([a-zA-Z0-9_-]){3,5}$/  can put =>  3,5 length

                // /^[a-zA-Z0-9&@.$%\-,():;` ]+$/  can put =>  A-Z a-z 0-9 & @ . $ % - , ( ) : ; ` - space 

                // var rex = /^[A-Za-z0-9_.]+$/; can put => a-z 09


            },
            numberOnly: function (op) {
                if (Contexts) {
                    if (!Contexts.onkeydown) {
                        Contexts.onkeydown = function (event) {
                            var optionsNum = op || {};
                            event = (event || window.event)
                            var keyCoder = event.keyCode || event.which;
                            var ctrlKey = 17, vKey = 86, cKey = 67;
                            var ctrlDown = event.ctrlKey || event.metaKey
                            var shiftKey = event.shiftKey || event.metaKey;
                            var hasKey = event.key || undefined;
                            if (ctrlDown || shiftKey) {
                                return false;
                            }
                            var numberKey = hasKey || helper.keyCodeDB(keyCoder);// String.fromCharCode(keyCoder);
                            if (numberKey == '1' || numberKey == '2' ||
                                numberKey == '3' || numberKey == '4' ||
                                numberKey == '5' || numberKey == '6' ||
                                numberKey == '7' || numberKey == '8' ||
                                numberKey == '9' || numberKey == '0'

                               ) {
                                if (optionsNum.length) {
                                    if (this.value.length < optionsNum.length) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                                return true;
                            } else if (keyCoder == '8' ||
                                keyCoder == '37' ||
                                keyCoder == '39') {
                                if (numberKey == "'" || numberKey == "%") {
                                    return false;
                                } else {
                                    return true;
                                }
                            } else {
                                return false;
                            }


                        }
                        Contexts.onmousedown = function (event) {
                            if (event.button == 2) {
                                if (event.preventDefault) {
                                    event.preventDefault();
                                }
                                return false;
                            }
                        }
                        //Contexts.onkeyup = function () {
                        //    var val = this.value.replace(/,/g, '');
                        //    var valpoint = val.split('.');
                        //    if (valpoint.length >1) {
                        //        val = valpoint[0];
                        //        var nval = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"); 
                        //        this.value = nval + '.' + valpoint[1];
                        //    } else {
                        //        var nval = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                        //        this.value = nval;
                        //    }
                        //}

                    }

                }
            },
            moneyOnly: function (opt) {
                if (!Contexts.onkeydown) {
                    Contexts.onkeydown = function (event) {
                        event = (event || window.event)
                        var keyCoder = event.keyCode || event.which;
                        var ctrlKey = 17, vKey = 86, cKey = 67, shiftKey = 16;
                        var ctrlDown = event.ctrlKey || event.metaKey;
                        var shiftKey = event.shiftKey || event.metaKey;
                        var hasKey = event.key || undefined;
                        if (ctrlDown || shiftKey) {
                            return false;
                        }

                        var numberKey = hasKey || helper.keyCodeDB(keyCoder);//String.fromCharCode(keyCoder);
                        if (numberKey == '1' || numberKey == '2' ||
                            numberKey == '3' || numberKey == '4' ||
                            numberKey == '5' || numberKey == '6' ||
                            numberKey == '7' || numberKey == '8' ||
                            numberKey == '9' || numberKey == '0'
                            || numberKey == '.'
                           ) {
                            var selectPosition = 0;
                            var input = this
                            if (!input) return; // No (input) element found
                            if ('selectionStart' in input) {
                                // Standard-compliant browsers
                                selectPosition = input.selectionStart;
                            } else if (document.selection) {
                                // IE
                                input.focus();
                                var sel = document.selection.createRange();
                                var selLen = document.selection.createRange().text.length;
                                sel.moveStart('character', -input.value.length);
                                selectPosition = sel.text.length - selLen;
                            }


                            var val = this.value.toString().insertAt(selectPosition, numberKey).replace(/,/g, ''); // this.value.replace(/,/g, '') + numberKey;
                            var numStr = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
                            if (numStr.test(val.toString())) {
                                var valpoint = val.split('.');
                                if (valpoint.length > 1) {
                                    var regex = /^\d+(?:\.\d{0,2})$/;
                                    if (regex.test(val.toString())) {

                                        return true;
                                    } else {
                                        //alert('...')
                                        return false;
                                    }

                                }
                                return true;
                            } else {
                                return false;
                            }
                        } else if (keyCoder == '8' ||
                            keyCoder == '37' ||
                            keyCoder == '39') {
                            if (numberKey == "'" || numberKey == "%") {
                                return false;
                            } else {
                                return true;
                            }

                        } else {

                            return false;
                        }


                    }
                    Contexts.onkeyup = function (event) {

                        event = (event || window.event)
                        var keyCoder = event.keyCode || event.which;
                        if (
                            keyCoder == '37' ||
                            keyCoder == '39')
                        { return true; }

                        var valueIndx = this.value.length;
                        var val = this.value.replace(/,/g, '');

                        if (val.length > 1) {
                            if (parseInt(val) == 0) {
                                this.value = "0";
                                return false;
                            }
                        }
                        val = (val == "" ? "0" : val)
                        var valpoint = val.split('.');
                        var selectPosition = 0;
                        var input = this
                        if (!input) return; // No (input) element found
                        if ('selectionStart' in input) {
                            // Standard-compliant browsers
                            selectPosition = input.selectionStart;
                        } else if (document.selection) {
                            // IE
                            input.focus();
                            var sel = document.selection.createRange();
                            var selLen = document.selection.createRange().text.length;
                            sel.moveStart('character', -input.value.length);
                            selectPosition = sel.text.length - selLen;
                        }
                        if (valpoint.length > 1) {
                            val = valpoint[0];
                            //val = parseFloat(val).toFixed(2);
                            var nval = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                            this.value = nval + '.' + valpoint[1];
                            if (this.value.length > valueIndx) {
                                selectPosition += 1;
                            }
                            if (this != null) {
                                if (this.createTextRange) {
                                    var range = this.createTextRange();
                                    range.move('character', selectPosition);
                                    range.select();
                                }
                                else {
                                    if (this.selectionStart) {
                                        this.focus();
                                        this.setSelectionRange(selectPosition, selectPosition);
                                    }
                                    else
                                        this.focus();
                                }
                            }


                        } else {
                            val = parseInt(val);

                            var nval = val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                            if (nval == "NaN") {
                                return false;
                            }
                            this.value = nval;
                            if (this.value.length > valueIndx) {
                                selectPosition += 1;
                            }
                            if (this != null) {
                                if (this.createTextRange) {
                                    var range = this.createTextRange();
                                    range.move('character', selectPosition);
                                    range.select();
                                }
                                else {
                                    if (this.selectionStart) {
                                        this.focus();
                                        this.setSelectionRange(selectPosition, selectPosition);
                                    }
                                    else
                                        this.focus();
                                }
                            }
                        }

                    }
                    Contexts.onblur = function () {
                        (this.value.length > 1)
                        {
                            if (this.value.substring(this.value.length - 1, this.value.length) == ".") {
                                this.value = this.value + "00";
                            } else if (this.value.substring(this.value.length - 2, this.value.length - 1) == ".") {
                                this.value = this.value + "0";
                            }
                        }


                    }
                }
            },
            limitLength: function (len) {
                var curElement = Contexts;
                curElement.onkeypress = function (event) {
                    event = (event || window.event)
                    var keyCoder = event.keyCode || event.which;
                    var numberKey = String.fromCharCode(keyCoder);
                    if (keyCoder == '8') {
                        return true;
                    }
                    if (len) {
                        if (this.value.length < len) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }, curElement.onblur = function () {
                    if (this.value.length >= len) {
                        var newVal = this.value.substring(0, len);
                        this.value = newVal;
                    }
                }

            },
            cancelRightClick: function () {
                var elCur = Contexts;
                elCur.onmousedown = function (event) {
                    if (event.button == 2) {
                        return false;
                    }
                }
            }
        }
    });
    mcsCore.mapMethod({
        requireField: function (options, callbacks) {
            var eleCur = Contexts;
            var nameAtt = options.name, type = options.type || "String", errorMessage = options.errorMessage, regex = options.regex, validtemplete = options.validtemplete;

            if (is.Function(validtemplete)) {

            } else {
                if (type == 'String') {
                    if (is.Array(nameAtt)) {
                        for (var loop = 0; loop < nameAtt.length; loop++) {
                            if (nameAtt[loop].value.trim() == '') {
                                if (is.Function(callbacks)) {
                                    callbacks(errorMessage || "error is not handler ");
                                }
                                //throw "is not validate";
                            }
                        }
                    } else {
                        //document.getElementById().getAttribute('item-validate-type')  //="String" 
                        //document.getElementById().getAttribute('item-validate-message')

                        if (eleCur.value.trim() == '') {
                            if (is.Function(callbacks)) {
                                callbacks(errorMessage || "error is not handler ");
                            }
                            //throw "is not validate";
                        }
                    }
                } else if (type == "Number")
                { } else if (type == "Date")
                { }
            }

        }
    });


    mcsCore.mapMethod({
        findChild: function (el) {
            var element = Contexts;
            if (element.childNodes.length > 0) {
                return mcs(element.querySelector(el));
            } else {
                return mcs(null);
            }


        },
        findParent: function (op) {
            var o_name = op.name;
            var o_id = op.id;
            var o_tagname = op.tagname;
            var o_class = op.className;
            var o_attribute_name = op.attribute == undefined ? "" : op.attribute.name;
            var o_attribute_val = op.attribute == undefined ? "" : op.attribute.value;

            var element = Contexts;
            var Obj = element.parentNode;
            if (op.name != undefined) {
                while (Obj.getAttribute('name') != op.name) {
                    Obj = Obj.parentNode;
                }

                return new mcs(Obj);
            }
            if (op.id != undefined) {
                while (Obj.id != op.id) {
                    Obj = Obj.parentNode;
                }
                return new mcs(Obj);
            }
            if (op.tagname != undefined) {

                while ((Obj.localName || Obj.nodeName).toLowerCase() != op.tagname) {
                    Obj = Obj.parentNode;
                }
                return new mcs(Obj);
            }
            if (op.className != undefined) {


                while (Obj.className != op.className) {
                    Obj = Obj.parentNode;
                    var classValues = Obj.split(" ");
                    var filteredList = [];

                    for (var i = 0 ; i < classValues.length; i++) {
                        if (op.className == classValues[i]) {
                            return new mcs(Obj);
                        }

                    }
                }
            }
            if (op.attribute != undefined) {
                while (Obj.getAttribute(op.attribute.name) != op.op.attribute.value) {
                    Obj = Obj.parentNode;
                }
                return new mcs(Obj);
            }


        }
    });
    mcsCore.mapMethod({
        elementNextsubling: function (el) {
            var elements = Contexts;
            if (el == undefined) {
                return new mcs(elements.nextSibling);
            } else {
                if (elements.length) {
                    //for (var i = 0; i < elements.length; i++) {
                    //    if (elements[i].localName == el) {
                    // var previous = elements[i - 1];
                    //var next = elements[i + 1];
                    //return mcs(next);
                    //function next(elem) {
                    do {
                        elements = elements.nextSibling;
                    } while (elements && elements.nodeType !== 1 && elements.localName == el);

                    return new mcs(elements);
                    // }
                    //    }
                    //}

                } else {
                    return mcs(null);
                }
            }

        }
    });

    mcsCore.mapProp({
        getExtensionFile: function () {
        }, validateFileType: function (fn, fa) {
            var fileExtension = fn.replace(/^.*\./, '');
            var result = fa.indexOf(fileExtension);
            if (result >= 0) {
                return true;
            } else {
                return false;
            }
        }
    });

    mcsCore.mapProp({
        formValidate: function (op) {
            var strcontext = op.attributeName != undefined ? '[' + op.attributeName + ']' : '[item-validate]';
            var ele = document.querySelectorAll(strcontext);
            mcs.delay(function () {
                if (is.Count(ele) > 0) {
                    for (var loop = 0; loop < ele.length; loop++) {
                        //   item-validate="costcenter"  item-validate-datatype="String" item-validate-usetpye="" item-validate-message="กรุณาเลือก Cost Center ของท่าน"
                        var curElements = ele[loop];
                        var messageError = '';
                        if (curElements.getAttribute('item-validate-message')) {
                            messageError = curElements.getAttribute('item-validate-message') || 'กรุณากรอกข้อมูล';
                        }
                        if (curElements.getAttribute('item-validate-datatype')) {
                            messageError += '\nประเภทข้อมูล :' + dataTypeConvert(curElements.getAttribute('item-validate-datatype'));
                        }
                        if (curElements.getAttribute('item-validate-usetype')) {
                            messageError += '\nการใช้งาน :' + (curElements.getAttribute('item-validate-usetype') || 'ไม่ได้กำหนด');
                        }


                        var leftEl = curElements.offsetLeft - 22;
                        var topEl = curElements.offsetTop;//+ curElements.offsetHeight;

                        while ((curElements = curElements.offsetParent)) {

                            leftEl += curElements.offsetLeft;
                            topEl += curElements.offsetTop;

                        }
                        if (ele[loop].style.display == 'none') {
                            //is not
                        } else {

                            var eleTemplete = document.getElementById('validator_' + ele[loop].id);
                            if (eleTemplete) {
                                eleTemplete.style.position = 'absolute';
                                eleTemplete.title = messageError;
                                eleTemplete.style.left = leftEl + 'px';
                                eleTemplete.style.top = topEl + 'px';
                            } else {
                                if (is.Function(op.templete)) {
                                    eleTemplete = op.templete(ele[loop]);
                                    eleTemplete.id = 'validator_' + ele[loop].id;
                                    if (ele[loop].getAttribute('item-validate-targetid')) {
                                        var inputValid = document.getElementById(ele[loop].getAttribute('item-validate-targetid'));

                                        inputValid.setAttribute('item-validate-relation', eleTemplete.id)
                                    } else {
                                        ele[loop].setAttribute('item-validate-relation', eleTemplete.id)
                                    }


                                    eleTemplete.style.position = 'absolute';
                                    eleTemplete.title = messageError;
                                    eleTemplete.style.left = leftEl + 'px';
                                    eleTemplete.style.top = topEl + 'px';
                                    eleTemplete.style.cursor = 'pointer';
                                    document.body.appendChild(eleTemplete);
                                    //mcs.delay(function (cur) {
                                    //    var thiscon = cur;
                                    //    document.body.removeChild(eleTemplete);
                                    //}, op.showTime || 5000);
                                } else {
                                    var spanElValid = mcs.createTag('span');
                                    spanElValid.innerHTML = '!Error';
                                    spanElValid.title = '';
                                    spanElValid.style.color = '#F5391A';
                                    spanElValid.style.position = 'absolute';
                                    spanElValid.style.left = leftEl + 'px';
                                    spanElValid.style.top = topEl + 'px';
                                    document.body.appendChild(spanElValid);
                                    mcs.delay(function () {
                                        document.body.removeChild(spanElValid);
                                    }, op.showTime || 5000);


                                }
                            }
                        }
                    }
                } else {

                }

            }, op.startDelay || 1000);
            function dataTypeConvert(s) {

                switch (s) {
                    case null:
                        return 'ไม่ได้กำหนด';
                        break;
                    case '':
                        return 'ไม่ได้กำหนด';

                        break;
                    case undefined:

                        return 'ไม่ได้กำหนด';
                        break;
                    case 'Date':
                        return 'วันที่'
                        break;
                    case 'String':
                        return 'ข้อความ'
                        break;
                    case 'Int':
                        return 'ตัวเลข'
                        break;
                    case 'Money':
                        return 'ค่าเงิน'
                        break;
                }
            }

        },
        validate: {
            inputIsValid: function (op) {
                var ele = {};

                if (op) {
                    var strcontext = op.attributeName != undefined ? '[' + op.attributeName + ']' : '[item-validate]';
                    ele = document.querySelectorAll(strcontext);
                } else {
                    ele = document.querySelectorAll('item-validate');
                }

                if (ele.length > 0) {
                    for (var loop = 0; loop < ele.length ; loop++) {
                        var value = '';
                        var elementContext = ele[loop];
                        var itemType = ele[loop].getAttribute('item-validate-datatype');
                        var itemMessage = ele[loop].getAttribute('item-validate-message');
                        var itemCondition = ele[loop].getAttribute('item-validate');
                        if (elementContext.nodeName) {
                            if (elementContext.nodeName.toLowerCase() == 'input') {
                                value = elementContext.value;
                            } else if (elementContext.nodeName.toLowerCase() == 'span' || elementContext.nodeName.toLowerCase() == 'p'
                                || elementContext.nodeName.toLowerCase() == 'label') {

                                value = elementContext.innerHTML;
                            } else if (elementContext.nodeName.toLowerCase() == 'textarea') {
                                value = elementContext.value;
                            }
                            if (itemCondition) {
                                var findCondition = itemCondition.split(':');
                                if (findCondition.length == 2) {
                                    if (findCondition[0] == 'EqualTo') {
                                        if (value == findCondition[0]) {
                                            return true;
                                        }
                                        return false;
                                    } else if (findCondition[0] == 'NotEqualTo') {
                                        if (value != findCondition[0]) {
                                            return true;
                                        }
                                        return false;
                                    }
                                    else if (findCondition[0] == 'GreaterThan') {
                                        if (value > findCondition[0]) {
                                            return true;
                                        }
                                        return false;
                                    }
                                    else if (findCondition[0] == 'LessThan') {
                                        if (value < findCondition[0]) {
                                            return true;
                                        }
                                        return false;
                                    }
                                    else if (findCondition[0] == 'GreaterThanOrEqualTo') {
                                        if (value >= findCondition[0]) {
                                            return true;
                                        }
                                        return false;
                                    }
                                    else if (findCondition[0] == 'LessThanOrEqualTo') {
                                        if (value <= findCondition[0]) {
                                            return true;
                                        }
                                        return false;
                                    }

                                }

                            }



                        } else {

                        }
                    }

                }
                function validateDate(format, datestr) {
                    if (format.toLowerCase() == 'dd/mm/yyyy') {

                        var re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
                        if (form.startdate.value != '') {
                            if (re.test(datestr)) { // day value between 1 and 31 
                                if (regs[1] < 1 || regs[1] > 31) {
                                    return false;
                                } // month value between 1 and 12 
                                if (regs[2] < 1 || regs[2] > 12) {
                                    return false;
                                } // year value between 1902 and 2016 
                                if (regs[3] < 1902 || regs[3] > (new Date()).getFullYear()) {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                            return true;
                        }
                    } else {

                    }
                }

                //    [Is]
                //    [EqualTo]
                //    [NotEqualTo]
                //    [GreaterThan]
                //    [LessThan]
                //    [GreaterThanOrEqualTo]
                //    [LessThanOrEqualTo]
            }
        }

    });
    mcsCore.mapProp({
        ///<sumary>
        /// Lowcase Name and Replace white space
        ///</sumary>
        TableToJson: function (tableID) {
            var eleTable = document.querySelector(tableID);
            var table = eleTable;
            var data = [];
            var headers = [];
            for (var i = 0; i < table.rows[0].cells.length; i++) {
                headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
            }
            // go through cells
            for (var i = 1; i < table.rows.length; i++) {
                var tableRow = table.rows[i]; var rowData = {};
                for (var j = 0; j < tableRow.cells.length; j++) {
                    rowData[headers[j]] = tableRow.cells[j].innerHTML;
                } data.push(rowData);
            }
            return data;
        }
    });
    //mcsCore.mapProp(is);
    mcsCore.mapProp({
        //DateEx: DateExtends,
        DateTime: DateTime
    });
    mcsCore.mapProp({ getDeivces: function () { return Devices(); } });
    mcsCore.mapProp({
        getResolution: function () {
            return { Width: screen.availWidth, Height: screen.availHeight }
        },
        getBrowserScreen: function () {
            var w = window,
                 d = document,
                 e = d.documentElement,
                 g = d.getElementsByTagName('body')[0],
                 x = w.innerWidth || e.clientWidth || g.clientWidth,
                 y = w.innerHeight || e.clientHeight || g.clientHeight;
            return { Width: x, Height: y }
        }, getWindownScroll: function () {
            var top = window.pageYOffset || document.documentElement.scrollTop,
            left = window.pageXOffset || document.documentElement.scrollLeft;
            return { Top: top, Left: left }
        }
    });
    mcsCore.mapProp({ readObj: helper.readObj });
    mcsCore.mapProp(callservice);
    //mcsCore.mapProp({
    //    getLanguage: function () {

    //        var getLang = document.getElementsByClassName("languageActive");
    //        console.log("atrr " + getLang.length)
    //        if (getLang.length == 2) {
    //            for (var t = 0 ; t <= getLang.length - 1; t++) {
    //                console.log(getLang[t].getAttribute("data-active"));
    //                if (getLang[t].getAttribute("data-active") == "true") {
    //                    return getLang[t].innerHTML;
    //                }
    //            }
    //        } else {
    //            alert(' id : tag_language is not declare');
    //        }
    //        //var ele = mcs('tag_language');
    //        //if (ele) {
    //        //    if (ele.ElementContext.innerHTML == "") {
    //        //        alert("return from mcs.script getLanguage EN");
    //        //        return "EN";
    //        //    } else {
    //        //        return ele.ElementContext.innerHTML;
    //        //    }
    //        //} else {
    //        //    alert(' id : tag_language is not declare');
    //        //}
    //    }
    //});
    mcsCore.mapProp({
        isNull: function (n) {
            if (n == null || n == undefined || n.length == 0) {
                return true;
            } else {
                return false;
            }
        },
        isNotNull: function (n) {
            if (n == null || n == undefined || n.length == 0) {
                return false;
            } else {
                return true;
            }
        }
    });
    mcsCore.mapProp({
        getQueryString: function (name) {

            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

        }
    });
    mcsCore.mapProp({
        delay: helper.delay
    });

    mcsCore.mapProp({
        ValidImage: function (oInput) {
            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
    })
    mcsCore.mapProp({ PopUp: messagePopup });
    mcsCore.mapProp({ pageComplete: pageCompleted });
    mcsCore.mapProp({ Loadding: loading });
    mcsCore.mapProp({ Browser: browseris });
    mcsCore.mapProp({ setHost: function (p) { AppHost = p; } });
    mcsCore.mapProp({ setImagePath: function (p) { ImagePaths = p; } });
    mcsCore.mapProp({ getWebPath: DefaultProp.WebPath, getImagePath: DefaultProp.ImagesPath });
    mcsCore.mapProp({ Convert: converts });
    mcsCore.mapProp({ isUndefined: is.Undefined });
    mcsCore.mapProp({ isArray: is.Array });
    mcsCore.mapProp({ isFunction: is.Function });
    mcsCore.mapProp({ isObject: is.Object });
    mcsCore.mapProp({
        Trace: {
            Error: function () {
                var myWindow = window.open("", "mWindow", "width=200,height=100");   // Opens a new window
                myWindow.document.write(localStorage.getItem('ErrorAudit'));   // Text in the new window
                //myWindow.opener.document.write();  // Text in the window that created the new
            },
            Info: function () {
                var myWindow = window.open("", "mWindow", "width=200,height=100");
                myWindow.document.write(localStorage.getItem('GetAudit'));

                localStorage.setItem("CallAudit", url);
                localStorage.setItem("SendAudit", params);
            },
            Url: function () {
                var myWindow = window.open("", "mWindow", "width=200,height=100");
                myWindow.document.write(localStorage.getItem('CallAudit'));

                localStorage.setItem("SendAudit", params);
            },
            SendParameter: function () {
                var myWindow = window.open("", "mWindow", "width=200,height=100");
                myWindow.document.write(localStorage.getItem('SendAudit'));
            }

        }
    });
    mcsCore.mapProp({
        createTag: function (t) {
            try {

                var ele = document.createElement(t);
                if (is.Element(ele)) {
                    return ele;
                } else {
                    return { isNotElement: "" }
                }
            } catch (e) {
                return { isNotElement: "" }
            }
        },
        createText: function (t) {
            try {
                var ele = document.createTextNode(t);
                return ele;
            } catch (e) {
                return { isNotElement: "" }
            }
        }
    });





    //EndRegion Exten 
    // ----- init ----- // 
    mcsCore.framework.int.prototype = mcsCore.framework;






    // FIX Query Data From Collection
    var DataQuery = function (d) {
        return new DataQuery.ProcessData(d);
    }
    DataQuery.func = {
        Context: function (d) {
            this.DataContexts = [];
            this.DataContexts = d || [];
            return this;
        },
        foreach: function (call) {
            var currentContext = this.DataContexts;
            for (var t = 0; t <= currentContext.length - 1; t++) {
                call(currentContext[t]);
            }
        },
        Select: function () {

            return this;
        },
        Where: function (f) {
            var newArray = [], len = this.DataContexts.length;
            for (var i = 0; i < len; i++) {
                if (f.apply(this.DataContexts[i], [this.DataContexts[i], i])) {
                    newArray[newArray.length] = this.DataContexts[i];
                }
            }
            this.DataContexts = newArray;
            return this;
        },
        GroupBy: function () {
            return this;
        }, ToArray: function () {
            return this.DataContexts;
        }
    }
    DataQuery.ProcessData = function (d) {
        DataQuery.func.Context(d);
        return this;
    }
    DataQuery.ProcessData.prototype = DataQuery.func.Context();


})(window);



//Valid Device OnClient
function Devices() {
    var userAgent = window.navigator.userAgent.toLowerCase();
    var find = function (needle) {
        return userAgent.indexOf(needle) !== -1;
    };
    var device = {};

    device.ios = function () {
        return device.iphone() || device.ipod() || device.ipad();
    };

    device.iphone = function () {
        return !device.windows() && find('iphone');
    };

    device.ipod = function () {
        return find('ipod');
    };

    device.ipad = function () {
        return find('ipad');
    };

    device.android = function () {
        return !device.windows() && find('android');
    };

    device.androidPhone = function () {
        return device.android() && find('mobile');
    };

    device.androidTablet = function () {
        return device.android() && !find('mobile');
    };

    device.blackberry = function () {
        return find('blackberry') || find('bb10') || find('rim');
    };

    device.blackberryPhone = function () {
        return device.blackberry() && !find('tablet');
    };

    device.blackberryTablet = function () {
        return device.blackberry() && find('tablet');
    };

    device.windows = function () {
        return find('windows');
    };

    device.windowsPhone = function () {
        return device.windows() && find('phone');
    };

    device.windowsTablet = function () {
        return device.windows() && (find('touch') && !device.windowsPhone());
    };

    device.fxos = function () {
        return (find('(mobile;') || find('(tablet;')) && find('; rv:');
    };

    device.fxosPhone = function () {
        return device.fxos() && find('mobile');
    };

    device.fxosTablet = function () {
        return device.fxos() && find('tablet');
    };

    device.meego = function () {
        return find('meego');
    };

    device.cordova = function () {
        return window.cordova && location.protocol === 'file:';
    };

    device.nodeWebkit = function () {
        return typeof window.process === 'object';
    };

    device.mobile = function () {
        return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego();
    };

    device.tablet = function () {
        return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet();
    };

    device.desktop = function () {
        return !device.tablet() && !device.mobile();
    };
    var ResDevice = function () {

        if (device.ios()) {
            if (device.ipad()) {
                dvName = ("ios ipad tablet");
            } else if (device.iphone()) {
                dvName = ("ios iphone mobile");
            } else if (device.ipod()) {
                dvName = ("ios ipod mobile");
            }
        } else if (device.android()) {
            if (device.androidTablet()) {
                dvName = ("android tablet");
            } else {
                dvName = ("android mobile");
            }
        } else if (device.blackberry()) {
            if (device.blackberryTablet()) {
                dvName = ("blackberry tablet");
            } else {
                dvName = ("blackberry mobile");
            }
        } else if (device.windows()) {
            if (device.windowsTablet()) {
                dvName = ("windows tablet");
            } else if (device.windowsPhone()) {
                dvName = ("windows mobile");
            } else {
                dvName = ("windows desktop");
            }
        } else if (device.fxos()) {
            if (device.fxosTablet()) {
                dvName = ("fxos tablet");
            } else {
                dvName = ("fxos mobile");
            }
        } else if (device.meego()) {
            dvName = ("meego mobile");
        } else if (device.nodeWebkit()) {
            dvName = ("node-webkit");
        } else if (device.television()) {
            dvName = ("television");
        } else if (device.desktop()) {
            dvName = ("desktop");
        }

        if (device.cordova()) {
            dvName = ("cordova");
        }
        return dvName;
    }

    return ResDevice();

}

window.onerror = function (msg, url, line, col, error) {

    //var extra = !col ? '' : '\ncolumn: ' + col;
    //extra += !error ? '' : '\nerror: ' + error;
    //alert("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);
    //var suppressErrorAlert = true;
    //return suppressErrorAlert;
};
function Entity(values) {
    var e = values;
    e._isEntity = true;
    return e;
};
function NotsupportException(m) {
    this.message = m;
}
function ClientException(msg, ex) {
    this.Message = msg;
    this.exception = ex;
}


//FIX IE8 Method  Regis Missing Method
if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [],
            k;
        for (k in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                keys.push(k);
            }
        }
        return keys;
    };
}
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}
if (typeof String.prototype.insertAt !== 'function') {
    String.prototype.insertAt = function (idx, str) {
        var f = this.toString().substr(0, idx);
        var m = str;
        var l = this.toString().substr(idx);
        return f + m + l;
    }
}
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
             ? Math.ceil(from)
             : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] === obj) { return i; }
        }
        return -1;
    }
}
try {
    ///<sumary>fix IE8</sumary>
    if (window.attachEvent) {
        if (Element.prototype.nodeName) {
            Element.prototype.localName = Element.prototype.nodeName.toLowerCase();
        } else {
            Element.prototype.localName = "";
        }
    }

    HTMLTableElement.prototype.rowsItem = function () {
        if (window.attachEvent) {
            if (this.children) {
                if (this.children.length == 0) {
                    return [];
                } else {
                    if (this.children[0].nodeName.toLowerCase() == 'tr') {
                        return this.children;
                    } else {
                        console.log('on rowcount .this table children[0] isnot tr ')
                        return [];
                    }
                }

            } else {
                return [];
            }
        } else {
            return this.rows;
        }
    }

    if (!document.getElementsByClassName) {

        var indexOf = [].indexOf || function (prop) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] === prop) return i;
            }
            return -1;
        };
        getElementsByClassName = function (className, context) {
            var elems = document.querySelectorAll ? context.querySelectorAll("." + className) : (function () {
                var all = context.getElementsByTagName("*"),
                    elements = [],
                    i = 0;
                for (; i < all.length; i++) {
                    if (all[i].className.test(new RegExp('(\\s|^)' + searchClass + '(\\s|$)')))
                        elements.push(all[i]);
                }
                return elements;
            })();
            return elems;
        };
        document.getElementsByClassName = function (className) {
            return getElementsByClassName(className, document);
        };
        if (Element) {
            Element.prototype.getElementsByClassName = function (className) {
                return getElementsByClassName(className, this);
            };
        }
    } else {

    }
} catch (ex) {
    alert("is Not support : " + ex.message)
}


// FIX Animation Using Javascript
; (function () {
    var mcsAnime = window.mcsAnimate = function () {
        return new mcsAnime.framework.init();
    }
    mcsAnime.framework = mcsAnime.prototype =
    {
        init: function () {
            return this;
        }, Obprop: function () {
        }, Elementer: function (s) {
            return document.getElementById(s);
        }

    };
    mcsAnime.render = function (s) {
        var sector = document.getElementById(s.substring(1))
        return animate(sector);
    }

    mcsAnime.perform = {
        start0to100: { level1: "0-0%", Level2: "0.2-20%", Level3: "0.5-50%", Level4: "0.8-80%", Level5: "1-100%" },
        start25to100: { level1: "0-0%", Level2: "0.2-4%", Level3: "0.5-25%", Level4: "0.8-64%", Level5: "1-100%" }
    };

    mcsAnime.framework.init.prototype = mcsAnime.framework;
    function animate(s) {
        animate[0] = s;
        return animate;
    }
    animate.slideImage = function (op) {
        var context = animate[0];
        var timeUnit = 0;
        animatePerform(op, function (res) {
            timeUnit = timeUnit + 500;
            context.style.left = "-" + (op.to * res) + "px"
            if (timeUnit == 500)
                timeUnit = 0;
        });
    }
    animate.warpIn = function (op) {
        var context = animate[0];
        animatePerform(op, function (res) {
            var to_ = op == null ? 0.5 : (op.to == undefined ? 0.5 : op.to)

            context.style.opacity = to_ * res;
            context.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + ((to_ * 100) * res) + ')';
            var w = (WinEx().get().height / 2);
            var h = (WinEx().get().width / 2);
            context.style.top = (h - (res * h)) + "px";
            context.style.right = (w - (res * w)) + "px";
            context.style.left = (w - (res * w)) + "px";
            context.style.bottom = (h - (res * h)) + "px";


            return;
        }, function () { context.style.display = "inline"; });
    }
    animate.warpout = function (op) {
        var context = animate[0];
        animatePerform(op, function (res) {
            context.style.opacity = 1 - res;
            var calop = ((1 - res) * 100);
            context.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + calop + ')';

            context.style.top = (200 * res) + "px";
            context.style.right = (400 * res) + "px";
            context.style.left = (400 * res) + "px";
            context.style.bottom = (200 * res) + "px";
            return;
        }, function () {
            context.style.display = "none";
        });
    }
    animate.slide = function ()
    { }
    animate.rotate = function (op) {
        var context = animate[0];
        switch (op) {
            case 0: context.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=0)'; break;
            case 90: context.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=1)'; break;
            case 180: context.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=2)'; break;
            case 270: context.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=3)'; break;

        }
    }
    function animatePerform(obj, p, ic) {
        var delay = obj.delay, complete = obj.complete, duration = obj.duration, pattern = obj.pattern, clear = obj.clear;
        obj = obj == null ? {} : obj;
        var startDate = new Date;
        var framerate = mcsAnime.delay(function () {
            processPerfrom(obj, p, function () {
                clearInterval(framerate);
                if (obj.complete) obj.complete();
                if (ic) { ic(); }
            }, startDate);
        }, obj.delay);

    }
    function processPerfrom(obj, p, c, t) {
        var transitionstart = new Date - t;
        var transitionrate = (transitionstart / ((obj.duration * 1000) || 1000));
        transitionrate = (obj.clear || true) == true ? transitionrate <= 1 ? transitionrate : 1 : transitionrate;

        var transitioncal = obj.pattern == undefined ? mcsAnimate.pattern.linear(transitionrate) : obj.pattern(transitionrate);

        p(transitioncal);
        if (transitionrate == 1) {
            if (obj.clear || true) {
                c(0);
            }
        }

    }



    //function sheeter() {
    //    return sheeter;
    //}
    //sheeter.prototype = sheeter.option = {
    //    addClass: function (element, classToAdd) {

    //        var currentClassValue = element.className;

    //        if (currentClassValue.indexOf(classToAdd) == -1) {
    //            if ((currentClassValue == null) || (currentClassValue === "")) {
    //                element.className = classToAdd;
    //            } else {
    //                element.className += " " + classToAdd;
    //            }
    //        }

    //    }, removeClass:
    //        function (element, classToRemove)
    //    {
    //        var currentClassValue = element.className;

    //        if (currentClassValue == classToRemove) {
    //            element.className = "";
    //            return;
    //        }

    //        var classValues = currentClassValue.split(" ");
    //        var filteredList = [];

    //        for (var i = 0 ; i < classValues.length; i++) {
    //            if (classToRemove != classValues[i]) {
    //                filteredList.push(classValues[i]);
    //            }
    //        }

    //        element.className = filteredList.join(" ");
    //    }, getSheet: function () {
    //        return document.styleSheets[0];
    //    },
    //    createSheet: function () {
    //        var style = document.createElement("style");
    //        style.appendChild(document.createTextNode(""));
    //        document.head.appendChild(style);
    //        return style.sheet;
    //        //sheet.insertRule("header { float: left; opacity: 0.8; }", 1);
    //        //sheet.insertRule("@media only screen and (max-width : 1140px) { header { display: none; } }");
    //        //multi state
    //        //sheet.insertRule("@media all { selector1 { rule : value } selector2 { rule : value } }", 0);
    //        //
    //        //sheet.addRule("#myList li", "float: left; background: red !important;", 1);
    //    }, addCSSRule:
    //        function (sheet, selector, rules, index) {
    //        if ("insertRule" in sheet) {
    //            sheet.insertRule(selector + "{" + rules + "}", index);
    //        }
    //        else if ("addRule" in sheet) {
    //            sheet.addRule(selector, rules, index);
    //        }
    //        // addCSSRule(document.styleSheets[0], "header", "float: left");

    //        },
    //    pushCSS: function (sheet, selector, rules) {
    //        var index = sheet.cssRules.length - 1;
    //        for (var i = index; i > 0; i--) {
    //            var current_style = sheet.cssRules[i];
    //            if (current_style.selectorText === selector) {
    //                rules = current_style.style.cssText + rules;
    //                sheet.deleteRule(i);
    //                index = i;
    //            }
    //        }
    //        if (sheet.insertRule) {
    //            sheet.insertRule(selector + "{" + rules + "}", index);
    //        }
    //        else {
    //            sheet.addRule(selector, rules, index);
    //        }
    //        return sheet.cssRules[index].cssText;
    //    },
    //    clearCSSRules: function (sheet) {

    //        var i = sheet.cssRules.length - 1;
    //        while (i >= 0) {

    //            if ("deleteRule" in sheet) { sheet.deleteRule(i); }
    //            else if ("removeRule" in sheet) { sheet.removeRule(i); }

    //            i--;

    //        }
    //    }

    //}
    function microCss() {
        return microCss;
    }

    microCss.opacity = function (o) {
        return "filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=" + o + ");";
    }
    mcsAnime.delay = function (f, t) {
        return setInterval(f, t || 10);
    }
    function pattern() {
        return pattern;
    }
    pattern.lineProcess =
    {
        linear: function (p) {
            return p;
        },
        quad: function (p) {
            //   return Math.pow(p, 2) slow start
            return Math.pow(p, 5)
        },
        circ: function (p) {
            return 1 - Math.sin(Math.acos(p))
        },
        back: function (p) {
            return Math.pow(p, 2) * ((1.5 + 1) * (p - 1.5))
        },
        bounce: function (p) {
            for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                if (p >= (7 - 4 * a) / 11) {
                    return -Math.pow((11 - 6 * a - 11 * p) / 4, 2) + Math.pow(b, 2)
                }
            }
        },
        swing: function (p) {
            return 0.5 - Math.cos(p * Math.PI) / 2;
        },
        elastic: function (p) {
            return Math.pow(2, 10 * (p - 1)) * Math.cos(20 * Math.PI * 1.5 / 3 * p)
        },
        makeEaseOut: function (delta) {
            return function (progress) {
                return 1 - delta(1 - progress)
            }
        },
        makeEaseInOut: function (delta) {
            return function (progress) {
                if (progress < .5)
                    return delta(2 * progress) / 2
                else
                    return (2 - delta(2 * (1 - progress))) / 2
            }
        }
    }

    mcsAnimate.animation = animatePerform;
    mcsAnime.pattern = pattern.lineProcess;
    mcsAnimate.prototype = mcsAnimate.position = function () { }
})();

function animationTest() {
    function animate(opts) {
        var start = new Date
        var id = setInterval(function () {
            var timePassed = new Date - start
            var progress = timePassed / opts.duration
            if (progress > 1) progress = 1
            var delta = opts.delta(progress)
            opts.step(delta)
            if (progress == 1) { clearInterval(id) }
        }, opts.delay || 10)
    }
    function elastic(progress) { return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * 1.5 / 3 * progress) }
    function linear(progress) { return progress }
    function quad(progress) { return Math.pow(progress, 2) }
    function quint(progress) { return Math.pow(progress, 5) }
    function circ(progress) { return 1 - Math.sin(Math.acos(progress)) }
    function back(progress) { return Math.pow(progress, 2) * ((1.5 + 1) * progress - 1.5) }
    function bounce(progress) {
        for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
            if (progress >= (7 - 4 * a) / 11) {
                return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
            }
        }
    }
    function makeEaseInOut(delta) {
        return function (progress) {
            if (progress < .5)
                return delta(2 * progress) / 2
            else
                return (2 - delta(2 * (1 - progress))) / 2
        }
    }
    function makeEaseOut(delta) { return function (progress) { return 1 - delta(1 - progress) } }
    function move(elem) {

        var left = 0

        function frame() {

            left++

            elem.style.left = left + 'px'

            if (left == 100)
                clearInterval(id)
        }

        var id = setInterval(frame, 10)
    }

}



//<input type="text" class="textfield" value="" id="extra7" name="extra7" onkeypress="return isNumber(event)" />
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//<template id="sdtemplate">
//  <style>
//    p { color: orange; }
//</style>
//<p>I'm in Shadow DOM. My markup was stamped from a &lt;template&gt;.</p>
//</template>

//<script>
//var proto = Object.create(HTMLElement.prototype, {
//    createdCallback: {
//        value: function() {
//            var t = document.querySelector('#sdtemplate');
//            var clone = document.importNode(t.content, true);
//            this.createShadowRoot().appendChild(clone);
//        }
//    }
//});
//document.registerElement('x-foo-from-template', {prototype: proto});
//</script>

//$(document).ready(function() {
//    $("#txtboxToFilter").keydown(function (e) {
//    // Allow: backspace, delete, tab, escape, enter and .
//        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
//    // Allow: Ctrl+A, Command+A
//            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
//    // Allow: home, end, left, right, down, up
//            (e.keyCode >= 35 && e.keyCode <= 40)) {
//    // let it happen, don't do anything
//                 return;
//}
//    // Ensure that it is a number and stop the keypress
//        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
//            e.preventDefault();
//}
//});
//});


//jQuery.fn.ForceNumericOnly =
//function()
//{
//    return this.each(function()
//{
//        $(this).keydown(function(e)
//{
//            var key = e.charCode || e.keyCode || 0;
//    // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
//    // home, end, period, and numpad decimal
//            return (
//                key == 8 || 
//                key == 9 ||
//                key == 13 ||
//                key == 46 ||
//                key == 110 ||
//                key == 190 ||
//                (key >= 35 && key <= 40) ||
//                (key >= 48 && key <= 57) ||
//                (key >= 96 && key <= 105));
//});
//});
//};


//$(document).ready(function() {
//    $("#txtboxToFilter").keydown(function (e) {
//    // Allow: backspace, delete, tab, escape, enter and .
//        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
//    // Allow: Ctrl+A
//            (e.keyCode == 65 && e.ctrlKey === true) ||
//    // Allow: Ctrl+C
//            (e.keyCode == 67 && e.ctrlKey === true) ||
//    // Allow: Ctrl+X
//            (e.keyCode == 88 && e.ctrlKey === true) ||
//    // Allow: home, end, left, right
//            (e.keyCode >= 35 && e.keyCode <= 39)) {
//    // let it happen, don't do anything
//                 return;
//}
//    // Ensure that it is a number and stop the keypress
//        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
//            e.preventDefault();
//}
//});
//});

//function toFixed(num, pre) {
//    num *= Math.pow(10, pre);
//    num = (Math.round(num, pre) + (((num - Math.round(num, pre)) >= 0.5) ? 1 : 0)) / Math.pow(10, pre);
//    return num.toFixed(pre);
//}

//a = 162.295; // a = 162.295
//toFixed(a, 2); // 162.30

//    var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.value),
//        val = this.value;

//    if(!valid){
//        console.log("Invalid input!");
//        this.value = val.substring(0, val.length - 1);
//}
//});


//var regex  = /^\d+(?:\.\d{0,2})$/;
//var numStr = "123.20";
//if (regex.test(numStr))
//{    alert("Number is valid");
//}



