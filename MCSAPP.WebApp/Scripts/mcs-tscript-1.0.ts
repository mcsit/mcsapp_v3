﻿class Student {
    fullName: string;
    constructor(public firstName, public middleInitial, public lastName) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }

}

interface Person {
    firstName: string;
    lastName: string;

}

function greeter(person: Person) {
    
    return "Hello, " + person.firstName + " " + person.lastName;
}

//var user = new Student("Jane", "M.", "User");

//document.body.innerHTML = greeter(user);


declare module Backbone {
    export class Model {
        constructor(attr?, opts?);
        get(name: string): any;
        set(name: string, val: any): void;
        set(obj: any): void;
        save(attr?, opts?): void;
        destroy(): void;
        bind(ev: string, f: Function, ctx?: any): void;
        toJSON(): any;
    }
    }
