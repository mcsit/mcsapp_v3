﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MCSAPP.WebApp.Startup))]
namespace MCSAPP.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
