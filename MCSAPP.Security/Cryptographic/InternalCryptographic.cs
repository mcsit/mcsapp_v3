﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.Security.Cryptographic
{
    public class InternalCryptographic
    {
        private const string password = "สเตชันนารี่เอไอเอส";
        private const PaddingMode paddingMode = PaddingMode.ISO10126;
        private static byte[] rgbSalt = Encoding.UTF8.GetBytes("AIS");

        /// <summary>
        /// EncryptData
        /// </summary>
        /// <param name="data">String Data</param>
        /// <returns></returns>
        public static string EncryptData(string data)
        {
            var encBytes = EncryptDataByte(Encoding.UTF8.GetBytes(data), password);
            return Convert.ToBase64String(encBytes);
        }

        /// <summary>
        /// DecryptData
        /// </summary>
        /// <param name="data">String Data</param>
        /// <returns></returns>
        public static string DecryptData(string data)
        {
            var encBytes = Convert.FromBase64String(data);
            var decBytes = DecryptDataByte(encBytes, password);
            return Encoding.UTF8.GetString(decBytes);
        }

        /// <summary>
        /// Encrypt Password as byte.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private static byte[] EncryptDataByte(byte[] data, string password)
        {
            var passwordDeriveBytes = new PasswordDeriveBytes(password, rgbSalt);
            var rijndaelManaged = new RijndaelManaged() { Padding = paddingMode };
            var encryptor = rijndaelManaged.CreateEncryptor(passwordDeriveBytes.GetBytes(16), passwordDeriveBytes.GetBytes(16));

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream encStream = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    encStream.Write(data, 0, data.Length);
                    encStream.FlushFinalBlock();
                    return msEncrypt.ToArray();
                }
            }
        }

        /// <summary>
        /// Decrypet Password as byte.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private static byte[] DecryptDataByte(byte[] data, string password)
        {
            var passwordDeriveBytes = new PasswordDeriveBytes(password, rgbSalt);
            var rijndaelManaged = new RijndaelManaged() { Padding = paddingMode };
            var decryptor = rijndaelManaged.CreateDecryptor(passwordDeriveBytes.GetBytes(16), passwordDeriveBytes.GetBytes(16));

            using (MemoryStream msDecrypt = new MemoryStream(data))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    var fromEncrypt = new byte[data.Length];
                    var read = csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
                    if (read < fromEncrypt.Length)
                    {
                        var clearBytes = new byte[read];
                        Buffer.BlockCopy(fromEncrypt, 0, clearBytes, 0, read);
                        return clearBytes;
                    }
                    return fromEncrypt;
                }
            }
        }
    }
}
