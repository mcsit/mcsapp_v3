﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.DAL.Data.RepositoryData;

namespace MCSAPP.DAL.Data
{
    public class ObjDataFactory
    {
        public static IAdminData GetAdminData()
        {
            return new AdminData();
        }
        public static IAppData GetAppData()
        {
            return new AppData();
        }
        public static ILevelData GetLevelData()
        {
            return new LevelData();
        }
        public static ILogData GetLogData()
        {
            return new LogData();
        }
        public static IWeekData GetWeekData()
        {
            return new WeekData();
        }
        public static IUserDepartmentData GetUserDepartmentData()
        {
            return new UserDepartmentData();
        }

        public static IWorkData GetWorkData()
        {
            return new WorkData();
        }
    }
}
