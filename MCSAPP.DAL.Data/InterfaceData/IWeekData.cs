﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data.RepositoryData;


namespace MCSAPP.DAL.Data.InterfaceData
{
    public interface IWeekData
    {
        List<WeekModel> GetWeekByMonth(DateModel model);
        List<GoalModel> GetTxnMonthByWeek(DateModel model);
        List<GoalSumModel> GetSummaryByWork(DateModel model);
        List<SararyModel> GetSararyMonthByWeek(DateModel model);

        List<GoalSumModel> GetSummaryWorkByCode(DateModel model);
        List<SararyModel> GetSararyMonthByCode(DateModel model);

        List<SumSalaryModel> GetSalaryFab(DateModel model);
        List<DayModel> GetWorkLessGoal(DateModel model);

        List<WorkModel> GetWorkMonth(DateModel model);
        List<WorkModel> GetWorkMonthByDay(DateModel model);

        List<DayModel> GetWorkByDay(DateModel model);
        List<WeeksModel> GetWorkByWeek(DateModel model);

        List<DayModel> GetSalaryByDay(DateModel model);
        List<WeeksModel> GetSalaryByWeek(DateModel model);

        List<DayModel> GetSalaryGroupByDay(DateModel model);
        List<WeeksModel> GetSalaryGroupByWeek(DateModel model);

        List<DayModel> GetWorkByDayAndCode(DateModel model);
        List<DayModel> GetSalaryByDayAndCode(DateModel model);

        List<DataFilterModel> GetDays(DateModel model);
        List<DataFilterModel> GetWeeks(DateModel model);
        DataFilterModel GetMaxWeeks(DateModel model);
        List<DataValueModel> GetHeaderWeek(DateModel model);
        List<DataValueModel> GetTypeProcess(DateModel model);

        List<ReviseModel> GetReviseByDayAndCode(DateModel model);
        List<ReviseModel> GetOtherByDayAndCode(DateModel model);

        List<NCRModel> GetReportNCR(DateModel model);
        List<NCRModel> GetReportVT(DateModel model);

        List<GoalSumModel> GetSummaryWorkDayForWeek(DateModel model);
        List<SalarySummaryModel> GetSummarySalaryDayForWeek(DateModel model);
        List<GoalSumModel> GetSummaryWorkDayForWeekLess(DateModel model);
        List<GoalSumModel> GetSummaryWorkDayForWeekWeldLess(DateModel model);
        List<PlusModel> GetSummarySalaryPlus(DateModel model);

        List<PlusModel> GetSummarySalaryMinus(DateModel model);
        List<PlusModel> GetSummarySalaryTotal(DateModel model);

        List<SummaryWorkModel> GetSummaryWorkDayForWeekWeld(DateModel model);
        List<SummarySalaryModel> GetSummarySalaryForWeekWeld(DateModel model);

        List<SummarySalaryModel> GetSummarySalaryForWeekWeldSum(DateModel model);
        List<SummarySalaryModel> GetSummarySalaryForWeekSum(DateModel model);
    }
}
