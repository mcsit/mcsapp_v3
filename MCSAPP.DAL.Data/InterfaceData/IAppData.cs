﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data.RepositoryData;
namespace MCSAPP.DAL.Data.InterfaceData
{
    public interface IAppData
    {
        List<PartDetail2Model> GetPartDetail(DateModel model);
        List<PartDetail2Model> GetOtherDetail(DateModel model);
        List<VTListModel> GetVTDetail(DateModel model);

        List<VTListModel> GetVTForAdd(DateModel model);
        List<UploadTypeModel> GetHeaderExcel(DateModel model);

    }
}
