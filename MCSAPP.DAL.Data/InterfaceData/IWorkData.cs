﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;
using MCSAPP.DAL.Data.RepositoryData;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.DAL.Data.InterfaceData
{
    public interface IWorkData
    {
        List<UploadTypeModel> GetHeaderExcel(DateModel model);

        AllSummaryForWorkModel GetSummaryForWorkByAutoGas1(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByAutoGas2(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByAutoGas3(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByTaper(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByPart(DateModel model);

        PartDetailModel GetDetailSummaryForWorkByAutoGas1(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByAutoGas2(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByAutoGas3(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByTaper(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByPart(DateModel model);

        PartDetailModel GetDetailForCheckSymbol(DateModel model);

        AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas1(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas2(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas3(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByTaper(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByPart(DateModel model);

        AllShowSummaryModel GetSummaryForGrinder(DateModel model);
        AllShowSummaryModel GetSummaryForCut(DateModel model);

        ResultModel SetUploadEndTab(UploadEndTabModel model);
        ResultModel SetUploadEndTabLog(UploadEndTabModel model);

        List<UploadEndTabModel> GetUploadEndTabLog(DateModel model);
        List<UploadEndTabModel> GetUploadEndTab(DateModel model);

        ResultModel DelUploadEndTabLog(UploadEndTabModel model);
        ResultModel DelUploadEndTab(UploadEndTabModel model);

        //CutPart
        ResultModel SetUploadCutPart(UploadCutPartModel model);

        ResultModel SetUploadCutPartLog(UploadCutPartModel model);

        ResultModel DelUploadCutPartLog(UploadCutPartModel model);

        ResultModel DelUploadCutPart(UploadCutPartModel model);

        List<UploadCutPartModel> GetUploadCutPartLog(DateModel model);

        List<UploadCutPartModel> GetUploadCutPart(DateModel model);

        //Process
        List<ProcessTypeModel> GetMasterProcessType(SearchModel model);

        ResultModel SetMasterProcessType(ProcessTypeModel model);

        ResultModel DeleteMasterProcessType(ProcessTypeModel model);

        ResultModel SetCheckSymbol(List<PartDatilSummaryForWorkModel> data);

     

        ResultModel DelCheckSymbolData(List<DataValueModel> data);

        PartDetailModel GetDetailForCheckSymbolData(DateModel model);

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2
        // Summary Work
        // Built Beam
        // 2018-01-26 chaiwud.ta
        // Bulit Up Beam 
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBeam(DateModel model);
        // Saw Beam
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltSawBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltSawBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltSawBeam(DateModel model);
        // Adjust Beam
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAdjustBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAdjustBeam(DateModel model);
        // Drill & Shot Blast
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillShotBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model);

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByOtherBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBeam(DateModel model);
        // Other Adjust Box
        AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByOtherBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBox(DateModel model);

        // 2018-07-26 T.Chaiwud #OTHER_FINISHING
        // Other Finishing
        AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherFinishing(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByOtherFinishing(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherFinishing(DateModel model);
        #endregion

        // Built Box
        // 2018-02-12 by chaiwud.ta
        // FabDiaphragmBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model);
        // BuiltUpBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBox(DateModel model);
        // AutoRootBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAutoRootBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAutoRootBox(DateModel model);
        // DrillSesnetBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model);
        // GougingRepairBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltGougingRepairBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model);
        // FacingBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFacingBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltFacingBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFacingBox(DateModel model);
        #endregion
    }
}
