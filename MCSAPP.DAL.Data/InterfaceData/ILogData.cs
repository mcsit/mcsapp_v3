﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data.RepositoryData;

namespace MCSAPP.DAL.Data.InterfaceData
{
   public  interface ILogData
    {
        int WriteLogError(string PAGE_NAME, string FUNC_NAME, string ERROR_MSG);
        int WriteLogActivity(string pageName, string funcName, string actionMsg, string userUpdate);
    }
}
