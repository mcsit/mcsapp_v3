﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MCSAPP.DAL.Data.InterfaceData
{
    public interface ICommandData : IDisposable
    {
        #region Properties section
        System.Data.IDbCommand Command
        {
            get;
            set;
        }
        #endregion

        void SetStoreProcedure(string storeProcedure);
        void SetSQLCommand(string sqlCommand);
        void SetParameter(string parameterName, System.Data.DbType dataType, ParameterDirection direction);
        void SetParameter(string parameterName, System.Data.DbType dataType, ParameterDirection direction, object values);
        System.Data.DataSet ExecuteDataSet();
        System.Data.DataTable ExecuteDataTable();
        System.Data.DataRow ExecuteDataRow();
        System.Data.DataRowCollection ExecuteDataRowCollection();
        int ExecuteNonQuery();
        string ExecuteGetReturnString(System.Data.IDbDataParameter returnParameter);
        int ExecuteGetReturnInt(System.Data.IDbDataParameter returnParameter);
    }
}
