﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Data.RepositoryData;


namespace MCSAPP.DAL.Data.InterfaceData
{
    public interface IAdminData
    {
        ResultModel SetTxnAdjustGoalData(AdjustGoalModel model);
        ResultModel SetTxnNcrData(NcrDataModel model);
        ResultModel SetNCRDataLog(NcrDataModel model);
        ResultModel DelNCRDetailLog(NcrDataModel model);
        ResultModel SetTxnVTData(VTDataModel model);
        ResultModel SetTxnDimensionData(DimDataModel model);
        ResultModel SetTxnOtherData(OtherDataModel model);
        ResultModel SetTxnWeekData(WeekDataModel model);
        ResultModel SetVTDetail(VTListModel model);
        ResultModel SetVTDetailLog(VTListModel model);
        ResultModel SetVTDetailUpload(VTListModel model);
        ResultModel DelVTDetailLog(VTListModel model);
        List<VTListModel> GetVTDetailLog(DateModel model);
        List<VTListModel> GetVTDetailAll(DateModel model);

        List<NCRDetailModel> GetReportNCRDetail(DateModel model);
        List<NGDetailModel> GetReportNGDetail(DateModel model);
        List<VTDetailModel> GetReportVTDetail(DateModel model);
        List<NcrDataModel> GetNCRLog(DateModel model);

        GoalSumModel GetWeekLessForAdjust(DateModel model);
        List<GoalSumModel> GetAdjustGoalData(DateModel model);
        List<MonthModel> GetCalendar(DateModel model);

        ResultModel SetTxnMinusData(MinusDataModel model);

        //List<MinusDataModel> GetReportMinusData(ReportSearchModel model);
        //List<OtherDataModel> GetReportOtherData(ReportSearchModel model);

        ResultModel SetDesingLog(DesingModel model);
        ResultModel SetDesing(DesingModel model);
        ResultModel DelDesingLog(DesingModel model);
        List<DesingModel> GetDesingLog(DateModel model);

        ResultModel SetUploadOtherLog(OtherModel model);
        ResultModel SetUploadOther(OtherModel model);
        ResultModel DelUploadOtheLog(OtherModel model);
        List<OtherModel> GetUploadOtheLog(DateModel model);


        ResultModel SetUploadWeekLog(UploadWeekModel model);
        ResultModel SetUploadWeek(UploadWeekModel model);
        ResultModel DelUploadWeekLog(UploadWeekModel model);
        List<UploadWeekModel> GetUploadWeekLog(DateModel model);

        ResultModel DeleteAdjustGoal(DeleteModel model);

        ResultModel DeleteDesingChange(DeleteModel model);

        ResultModel DeleteDimension(DeleteModel model);

        ResultModel DeleteMinus(DeleteModel model);

        ResultModel DeleteOther(DeleteModel model);

        ResultModel DeletePlus(DeleteModel model);

        ResultModel DeleteVTData(DeleteModel model);

        ResultModel DeleteWeek(DeleteModel model);


        List<ReportDesignChangeModel> GetReportDesingChangeData(ReportSearchModel model);

        List<ReportDimensionModel> GetReportDimensionData(ReportSearchModel model);

        List<ReportMinusModel> GetReportMinusData(ReportSearchModel model);

        List<ReportDimensionModel> GetReportOtherData(ReportSearchModel model);

        List<ReportDimensionModel> GetReportPlusData(ReportSearchModel model);

        List<VTListModel> GetReportVTData(ReportSearchModel model);

        List<UploadWeekModel> GetReportWeekData(ReportSearchModel model);

        ResultModel SetUploadNG(NGDataModel model);

        ResultModel SetUploadNGLog(NGDataModel model);

        ResultModel DelUploadNGLog(NGDataModel model);

        List<NGDataModel> GetUploadNGLog(DateModel model);

        List<NGDataModel> GetUploadNG(DateModel model);

        ResultModel DelUploadNG(NGDataModel model);

        ResultModel UpdateUploadNG(NGDataModel model);

        ResultModel SetUploadActUT(UploadActUTModel model);

        ResultModel SetUploadActUTLog(UploadActUTModel model);

        ResultModel DelUploadActUTLog(UploadActUTModel model);

        ResultModel DelUploadActUT(UploadActUTModel model);

        List<UploadActUTModel> GetUploadActUTLog(DateModel model);

        List<UploadActUTModel> GetUploadActUT(DateModel model);

        ResultModel SetUploadCutFinising(UploadCutFinisingModel model);

        ResultModel SetUploadCutFinisingLog(UploadCutFinisingModel model);

        ResultModel DelUploadCutFinisingLog(UploadCutFinisingModel model);

        ResultModel DelUploadCutFinising(UploadCutFinisingModel model);

        List<UploadCutFinisingModel> GetUploadCutFinisingLog(DateModel model);

        List<UploadCutFinisingModel> GetUploadCutFinising(DateModel model);

        ResultModel SetUploadGoal(UploadGoalModel model);

        ResultModel SetUploadGoalLog(UploadGoalModel model);

        ResultModel DelUploadGoalLog(UploadGoalModel model);

        ResultModel DelUploadGoal(UploadGoalModel model);

        List<UploadGoalModel> GetUploadGoalLog(DateModel model);

        List<UploadGoalModel> GetUploadGoal(DateModel model);

        //
        ResultModel SetUploadActUTNew(UploadActUTModel model);
        ResultModel SetUploadActUTNewLog(UploadActUTModel model);

        List<UploadActUTModel> GetUploadActUTNewLog(DateModel model);
        List<UploadActUTModel> GetUploadActUTNew(DateModel model);

        ResultModel DelUploadActUTNewLog(UploadActUTModel model);
        ResultModel DelUploadActUTNew(UploadActUTModel model);

        //

        ResultModel SetUploadNGNew(NGDataModel model);
        ResultModel SetUploadNGNewLog(NGDataModel model);

        ResultModel DelUploadNGNewLog(NGDataModel model);
        ResultModel DelUploadNGNew(NGDataModel model);

        List<NGDataModel> GetUploadNGNewLog(DateModel model);
        List<NGDataModel> GetUploadNGNew(DateModel model);

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        ResultModel SetUploadGoalBuilt(UploadGoalBuiltModel model);
        ResultModel SetUploadGoalLogBuilt(UploadGoalBuiltModel model);
        ResultModel DelUploadGoalLogBuilt(UploadGoalBuiltModel model);
        ResultModel DelUploadGoalBuilt(UploadGoalBuiltModel model);
        List<UploadGoalBuiltModel> GetUploadGoalLogBuilt(DateModel model);
        List<UploadGoalBuiltModel> GetUploadGoalBuilt(DateModel model);

        // UTRepairCheck
        ResultModel SetUploadUTRepairCheckLogBuilt(UTRepairCheckModelBuilt model);
        ResultModel SetUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model);
        ResultModel DelUploadUTRepairCheckLogBuilt(UTRepairCheckModelBuilt model);
        ResultModel DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model);
        List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckLogBuilt(DateModel model);
        List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckBuilt(ReportSearchModel model);

        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        ResultModel SetUploadUTDataByUserLogBuilt(UTDataByUserModelBuilt model);
        ResultModel SetUploadUTDataByUserBuilt(UTDataByUserModelBuilt model);
        ResultModel DelUploadUTDataByUserLogBuilt(UTDataByUserModelBuilt model);
        ResultModel DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model);
        List<UTDataByUserModelBuilt> GetUploadUTDataByUserLogBuilt(DateModel model);
        List<UTDataByUserModelBuilt> GetUploadUTDataByUserBuilt(DateModel model);

        // NGDataBuilt
        // 2018-04-17 by chaiwud.ta
        ResultModel SetUploadNGDataLogBuilt(NGDataModelBuilt model);
        ResultModel SetUploadNGDataBuilt(NGDataModelBuilt model);
        ResultModel DelUploadNGDataLogBuilt(NGDataModelBuilt model);
        ResultModel DelUploadNGDataBuilt(NGDataModelBuilt model);
        List<NGDataModelBuilt> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model);
        List<NGDataModelBuilt> GetUploadNGDataBuilt(ReportSearchBuiltModel model);

        // OtherCamber
        // 2018-04-23 by chaiwud.ta
        ResultModel SetUploadCamberDataLogOther(CamberDataModelOther model);
        ResultModel SetUploadCamberDataOther(CamberDataModelOther model);
        ResultModel DelUploadCamberDataLogOther(CamberDataModelOther model);
        ResultModel DelUploadCamberDataOther(CamberDataModelOther model);
        List<CamberDataModelOther> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model);
        List<CamberDataModelOther> GetUploadCamberDataOther(ReportSearchOtherFinishModel model);
        #endregion
    }
}
