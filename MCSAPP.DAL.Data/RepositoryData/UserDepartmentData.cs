﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class UserDepartmentData : IUserDepartmentData
    {
        public List<DepartmentModel> GetGroupName(SearchModel model)
        {
            List<DepartmentModel> ccr = new List<DepartmentModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_GROUP_NAME");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@CODE", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@ALL", DbType.String, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DepartmentModel
                            {
                                DEPT_CODE = DataConverter.GetString(item["DEPT_CODE"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                SUP_DEPT_CODE = DataConverter.GetString(item["SUP_DEPT_CODE"]),
                                SUP_DEPT_NAME = DataConverter.GetString(item["SUP_DEPT_NAME"]),
                                TOTAL_COUNT = DataConverter.GetInteger(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_GROUP_NAME", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UserModel> GetSearchUser(SearchModel model)
        {
            List<UserModel> ccr = new List<UserModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SEARCH_USER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input,model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UserModel
                            {
                                EMP_CODE = DataConverter.GetString(item["US_ID"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_CODE = DataConverter.GetString(item["DEPT_CODE"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                SUP_DEPT_CODE = DataConverter.GetString(item["SUP_DEPT_CODE"]),
                                SUP_DEPT_NAME = DataConverter.GetString(item["SUP_DEPT_NAME"]),
                                ROLE_NAME = DataConverter.GetString(item["ROLE_NAME"]),
                                TOTAL_COUNT = DataConverter.GetInteger(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SEARCH_USER", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public UserModel GetLogin(UserLoginModel model)
        {
            UserModel ccr = new UserModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("GET_LOGIN_USER_EMP");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@USER_NAME", DbType.String, ParameterDirection.Input, model.Username);
                    command.SetParameter("@USER_PWD", DbType.String, ParameterDirection.Input, model.Password);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        ccr.EMP_CODE = DataConverter.GetString(item["US_ID"]);
                        ccr.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]);
                        ccr.DEPT_CODE = DataConverter.GetString(item["DEPT_CODE"]);
                        ccr.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]);
                        ccr.SUP_DEPT_CODE = DataConverter.GetString(item["SUP_DEPT_CODE"]);
                        ccr.SUP_DEPT_NAME = DataConverter.GetString(item["SUP_DEPT_NAME"]);
                       
                    }
                   
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "GET_LOGIN_USER_EMP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetMainType(SearchModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MAIN_TYPE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ALL", DbType.Int16, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["NAME"]),
                                Value = DataConverter.GetString(item["ID"]),
                             
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MAIN_TYPE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetLevelType(SearchModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_LEVEL_TYPE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    command.SetParameter("@TYPE_ID", DbType.Int32, ParameterDirection.Input, model.TYPE);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) {
                        ccr.Add(new DataValueModel
                        {
                            Text ="",
                            Value = "",

                        });
                        return ccr;
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["NAME"]),
                                Value = DataConverter.GetString(item["ID"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_LEVEL_TYPE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetMainTypeForProcess(SearchModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MAIN_TYPE_FOR_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ALL", DbType.String, ParameterDirection.Input, model.ALL);
                    command.SetParameter("@SUP_DEPT_CODE", DbType.String, ParameterDirection.Input, model.SUP_DEPT_CODE);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["NAME"]),
                                Value = DataConverter.GetString(item["ID"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MAIN_TYPE_FOR_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetHeaderForProcess(SearchModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_HEADER_FOR_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ALL", DbType.String, ParameterDirection.Input, model.ALL);
                    command.SetParameter("@SUP_DEPT_CODE", DbType.String, ParameterDirection.Input, model.SUP_DEPT_CODE);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["NAME"]),
                                Value = DataConverter.GetString(item["ID"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_HEADER_FOR_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetHeaderWeek(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WEEK_HEADER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["WEEK_HEADER"]),
                                Value = DataConverter.GetString(item["WEEK_SUB"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WEEK_HEADER", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetProjectForCheckSymbol(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_PROJECT_FOR_CHECK_SYMBOL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@GROUP", DbType.Int32, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["PJ_NAME"]),
                                Value = DataConverter.GetString(item["PJ_ID"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_PROJECT_FOR_CHECK_SYMBOL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetProjectItemForCheckSymbol(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_PROJECT_ITEM_FOR_CHECK_SYMBOL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@PJ_ID", DbType.Int32, ParameterDirection.Input, model.PJ_ID);
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["PD_ITEM"]),
                                Value = DataConverter.GetString(item["PD_ITEM"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_PROJECT_ITEM_FOR_CHECK_SYMBOL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetProjectForCheckSymbolData(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_PROJECT_FOR_CHECK_SYMBOL_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@GROUP", DbType.Int32, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["PJ_NAME"]),
                                Value = DataConverter.GetString(item["PJ_ID"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_PROJECT_FOR_CHECK_SYMBOL_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetProjectItemForCheckSymbolData(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_PROJECT_ITEM_FOR_CHECK_SYMBOL_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@PJ_NAME", DbType.Int32, ParameterDirection.Input, model.PJ_NAME);
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["PD_ITEM"]),
                                Value = DataConverter.GetString(item["PD_ITEM"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_PROJECT_ITEM_FOR_CHECK_SYMBOL_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
    }
}

