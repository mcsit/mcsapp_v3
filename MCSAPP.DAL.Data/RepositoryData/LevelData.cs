﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class LevelData : ILevelData
    {
        public List<LevelModel> GetMasterLevel(SearchModel model)
        {
            List<LevelModel> ccr = new List<LevelModel>();
          
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MST_LEVEL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    foreach (DataRow item in result.Tables[0].Rows)
                    {
                        ccr.Add(new LevelModel { LEVEL_ID = DataConverter.GetInteger(item["LEVEL_ID"]),
                                                 LEVEL_NAME = DataConverter.GetString(item["LEVEL_NAME"]),
                                                 LEVEL_GOAL = DataConverter.GetInteger(item["LEVEL_GOAL"]),
                                                 TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                                 LEVEL_SALARY = DataConverter.GetDouble(item["LEVEL_SALARY"]),
                                                LEVEL_QUALITY = DataConverter.GetDouble(item["LEVEL_QUALITY"]),
                                                LEVEL_PROCEDURE = DataConverter.GetDouble(item["LEVEL_PROCEDURE"]),
                                                SALARY = DataConverter.GetDouble(item["SALARY"]),
                            TYPE_NAME = DataConverter.GetString(item["TYPE_NAME"]),
                        });
                    }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MST_LEVEL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<LevelGroupModel> GetMasterLevelGroup(SearchModel model)
        {
            List<LevelGroupModel> ccr = new List<LevelGroupModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MST_LEVEL_GROUP");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new LevelGroupModel
                            {
                                LEVEL_ID = DataConverter.GetInteger(item["LEVEL_ID"]),
                                GOAL_DAY_GROUP = DataConverter.GetInteger(item["GOAL_DAY_GROUP"]),
                                MIN_SALARY = DataConverter.GetInteger(item["MIN_SALARY"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                LEVEL_SALARY = DataConverter.GetDouble(item["LEVEL_SALARY"]),
                                LEVEL_QUALITY = DataConverter.GetDouble(item["LEVEL_QUALITY"]),
                                LEVEL_PROCEDURE = DataConverter.GetDouble(item["LEVEL_PROCEDURE"]),
                                TYPE_NAME = DataConverter.GetString(item["TYPE_NAME"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MST_LEVEL_GROUP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ProcessModel> GetMasterProcess(SearchModel model)
        {
            List<ProcessModel> ccr = new List<ProcessModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ProcessModel
                            {
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                PROCESS_ID = DataConverter.GetInteger(item["PROCESS_ID"]),
                                PROCESS_NAME = DataConverter.GetString(item["PROCESS_NAME"]),
                                PRICE_UNIT = DataConverter.GetInteger(item["PRICE_UNIT"]),
                                MAIN_PROCESS_NAME   = DataConverter.GetString(item["MAIN_PROCESS_NAME"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<CertModel> GetMasterCert()
        {
            List<CertModel> ccr = new List<CertModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_CERT");
                    command.Command.Parameters.Clear();
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new CertModel
                            {
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                CERT_ID = DataConverter.GetInteger(item["CERT_ID"]),
                                CERT_NAME = DataConverter.GetString(item["CERT_NAME"]),
                                CERT_SHORT_NAME = DataConverter.GetString(item["CERT_SHORT_NAME"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_CERT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<CertListModel> GetMasterCertList()
        {
            List<CertListModel> ccr = new List<CertListModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_CERT_LIST");
                    command.Command.Parameters.Clear();
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new CertListModel
                            {
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_CODE = DataConverter.GetString(item["DEPT_CODE"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                CERT_NAME = DataConverter.GetString(item["CERT_NAME"]),
                                CERT_START = DataConverter.GetString(item["CERT_START"]),
                                CERT_END = DataConverter.GetString(item["CERT_END"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_CERT_LIST", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<LevelNCRModel> GetMasterLevelNCR(SearchModel model)
        {
            List<LevelNCRModel> ccr = new List<LevelNCRModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MST_LEVEL_NCR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new LevelNCRModel
                            {
                                LEVEL_ID = DataConverter.GetInteger(item["LEVEL_ID"]),
                                
                                LEVEL_GOAL = DataConverter.GetInteger(item["LEVEL_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                MAIN_MIN = DataConverter.GetDouble(item["MAIN_MIN"]),
                                MAIN_MAX = DataConverter.GetDouble(item["MAIN_MAX"]),
                                TEMPO_MIN = DataConverter.GetDouble(item["TEMPO_MIN"]),
                                TEMPO_MAX = DataConverter.GetDouble(item["TEMPO_MAX"]),
                                PRICE_QULITY = DataConverter.GetDouble(item["PRICE_QULITY"]),
                                TYPE_NAME = DataConverter.GetString(item["TYPE_NAME"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MST_LEVEL_NCR", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<LevelNGModel> GetMasterLevelNG(SearchModel model)
        {
            List<LevelNGModel> ccr = new List<LevelNGModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MST_LEVEL_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new LevelNGModel
                            {
                                LEVEL_ID = DataConverter.GetInteger(item["LEVEL_ID"]),

                                LEVEL_GOAL = DataConverter.GetInteger(item["LEVEL_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                NG_MIN = DataConverter.GetDouble(item["NG_MIN"]),
                                NG_MAX = DataConverter.GetDouble(item["NG_MAX"]),
                               
                                PRICE_UT = DataConverter.GetDouble(item["PRICE_UT"]),
                                PRICE_QUALITY = DataConverter.GetDouble(item["PRICE_QUALITY"]),
                                PROCEDURE_DESC = DataConverter.GetString(item["PROCEDURE_DESC"]),
                                QUALITY_DESC = DataConverter.GetString(item["QUALITY_DESC"]),
                                TYPE_NAME = DataConverter.GetString(item["TYPE_NAME"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MST_LEVEL_NG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel SetMasterCert(CertModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_CERT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@CERT_ID", DbType.Int32, ParameterDirection.Input, model.CERT_ID);
                    command.SetParameter("@CERT_NAME", DbType.String, ParameterDirection.Input, model.CERT_NAME);
                    command.SetParameter("@CERT_SHORT_NAME", DbType.String, ParameterDirection.Input, model.CERT_SHORT_NAME);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_CERT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetMasterGroup(LevelGroupModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_GROUP");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@GOAL_DAY_GROUP", DbType.Int32, ParameterDirection.Input, model.GOAL_DAY_GROUP);
                    command.SetParameter("@LEVEL_SALARY", DbType.Double, ParameterDirection.Input, model.LEVEL_SALARY);
                    command.SetParameter("@LEVEL_QUALITY", DbType.Double, ParameterDirection.Input, model.LEVEL_QUALITY);
                    command.SetParameter("@LEVEL_PROCEDURE", DbType.Double, ParameterDirection.Input, model.LEVEL_PROCEDURE);
                    command.SetParameter("@MIN_SALARY", DbType.Double, ParameterDirection.Input, model.MIN_SALARY);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    command.SetParameter("@TYPE_ID", DbType.Int32, ParameterDirection.Input, model.TYPE_ID);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_GROUP", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetMasterLevel(LevelModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_LEVEL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@LEVEL_NAME", DbType.String, ParameterDirection.Input, model.LEVEL_NAME);
                    command.SetParameter("@LEVEL_GOAL", DbType.Int32, ParameterDirection.Input, model.LEVEL_GOAL);
                    command.SetParameter("@LEVEL_SALARY", DbType.Double, ParameterDirection.Input, model.LEVEL_SALARY);
                    command.SetParameter("@LEVEL_QUALITY", DbType.Double, ParameterDirection.Input, model.LEVEL_QUALITY);
                    command.SetParameter("@LEVEL_PROCEDURE", DbType.Double, ParameterDirection.Input, model.LEVEL_PROCEDURE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    command.SetParameter("@SALARY", DbType.String, ParameterDirection.Input, model.SALARY);
                    command.SetParameter("@TYPE_ID", DbType.String, ParameterDirection.Input, model.TYPE_ID);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_LEVEL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetMasterLevelNCR(LevelNCRModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_LEVEL_NCR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@MAIN_MIN", DbType.String, ParameterDirection.Input, model.MAIN_MIN);
                    command.SetParameter("@MAIN_MAX", DbType.Int32, ParameterDirection.Input, model.MAIN_MAX);
                    command.SetParameter("@TEMPO_MIN", DbType.Double, ParameterDirection.Input, model.TEMPO_MIN);
                    command.SetParameter("@TEMPO_MAX", DbType.Double, ParameterDirection.Input, model.TEMPO_MAX);
                    command.SetParameter("@PRICE_QULITY", DbType.Double, ParameterDirection.Input, model.PRICE_QULITY);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_LEVEL_NCR", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetMasterLevelNG(LevelNGModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_LEVEL_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@NG_MIN", DbType.String, ParameterDirection.Input, model.NG_MIN);
                    command.SetParameter("@NG_MAX", DbType.Int32, ParameterDirection.Input, model.NG_MAX);
                    command.SetParameter("@PRICE_QUALITY", DbType.Double, ParameterDirection.Input, model.PRICE_QUALITY);
                    command.SetParameter("@PRICE_UT", DbType.Double, ParameterDirection.Input, model.PRICE_UT);
                    command.SetParameter("@QUALITY", DbType.Int32, ParameterDirection.Input, model.QUALITY);
                    command.SetParameter("@PROCEDURE", DbType.Int32, ParameterDirection.Input, model.PROCEDURE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_LEVEL_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetMasterProcess(ProcessModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@PROCESS_ID", DbType.Int32, ParameterDirection.Input, model.PROCESS_ID);
                    command.SetParameter("@PROCESS_NAME", DbType.String, ParameterDirection.Input, model.PROCESS_NAME);
                    command.SetParameter("@PRICE_UNIT", DbType.Int32, ParameterDirection.Input, model.PRICE_UNIT);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    command.SetParameter("@MAIN_PROCESS", DbType.Int32, ParameterDirection.Input, model.MAIN_PROCESS);
                    command.SetParameter("@IS_LEVEL", DbType.Int32, ParameterDirection.Input, model.IS_LEVEL);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs. CODE = DataConverter.GetString(item["ACTION_RESULT_CODE"]);
                        rs.MESSAGE = DataConverter.GetString(item["ACTION_RESULT_MSG"]);
                        rs.SUCCESS = DataConverter.GetString(item["ACTION_RESULT_CODE"]) == "01";
                       // rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterCert(CertModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_CERT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@CERT_ID", DbType.Int32, ParameterDirection.Input, model.CERT_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_CERT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterGroup(LevelGroupModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_GROUP");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_GROUP", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterLevel(LevelModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_LEVEL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_LEVEL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterLevelNCR(LevelNCRModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_LEVEL_NCR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_LEVEL_NCR", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterLevelNG(LevelNGModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_LEVEL_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_LEVEL_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterProcess(ProcessModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@PROCESS_ID", DbType.Int32, ParameterDirection.Input, model.PROCESS_ID);     
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
        
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs.CODE = DataConverter.GetString(item["ACTION_RESULT_CODE"]);
                        rs.MESSAGE = DataConverter.GetString(item["ACTION_RESULT_MSG"]);
                        rs.SUCCESS = DataConverter.GetString(item["ACTION_RESULT_CODE"]) == "01";
                        // rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public DataFilterModel GetPriceByProcess(ProcessModel model)
        {
            DataFilterModel rs = new DataFilterModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_PRICE_BY_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.PROCESS_ID);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs.Value = DataConverter.GetString(item["PRICE_UNIT"]);
                
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_PRICE_BY_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        #region Built Beam/Box by chaiwud.ta
        // 2018-02-10 by chaiwud.ta 
        public List<LevelBuiltModel> GetMasterBuiltLevel(SearchModel model)
        {
            List<LevelBuiltModel> ccr = new List<LevelBuiltModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MST_LEVEL_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new LevelBuiltModel
                            {
                                LEVEL_ID = DataConverter.GetInteger(item["LEVEL_ID"]),
                                LEVEL_NAME = DataConverter.GetString(item["LEVEL_NAME"]),
                                LEVEL_GOAL = DataConverter.GetDouble(item["LEVEL_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                LEVEL_SALARY = DataConverter.GetDouble(item["LEVEL_SALARY"]),
                                LEVEL_QUALITY = DataConverter.GetDouble(item["LEVEL_QUALITY"]),
                                LEVEL_PROCEDURE = DataConverter.GetDouble(item["LEVEL_PROCEDURE"]),
                                SALARY = DataConverter.GetDouble(item["SALARY"]),
                                TYPE_NAME = DataConverter.GetString(item["TYPE_NAME"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_MST_LEVEL_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel SetMasterBuiltLevel(LevelBuiltModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_LEVEL_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@LEVEL_NAME", DbType.String, ParameterDirection.Input, model.LEVEL_NAME);
                    command.SetParameter("@LEVEL_GOAL", DbType.Double, ParameterDirection.Input, model.LEVEL_GOAL);
                    command.SetParameter("@LEVEL_SALARY", DbType.Double, ParameterDirection.Input, model.LEVEL_SALARY);
                    command.SetParameter("@LEVEL_QUALITY", DbType.Double, ParameterDirection.Input, model.LEVEL_QUALITY);
                    command.SetParameter("@LEVEL_PROCEDURE", DbType.Double, ParameterDirection.Input, model.LEVEL_PROCEDURE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    command.SetParameter("@SALARY", DbType.String, ParameterDirection.Input, model.SALARY);
                    command.SetParameter("@TYPE_ID", DbType.String, ParameterDirection.Input, model.TYPE_ID);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_LEVEL_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel DeleteMasterBuiltLevel(LevelBuiltModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_LEVEL_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@LEVEL_ID", DbType.Int32, ParameterDirection.Input, model.LEVEL_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_LEVEL_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        #endregion
    }
}
