﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.Helper;
using MCSAPP.ServiceGateway;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class CommandData : IDisposable, ICommandData
    {
        #region Private member section
        private bool disposed = false;
        private System.Data.IDbCommand cmd;
        private string strdbOwner = ConfigurationManager.AppSettings["OwnerDatabase"].ToString();
        private int commandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["DatabaseTimeOut"].ToString());
        #endregion

        #region Private methods seciton
        private void Dispose(bool disposing)
        {
            try
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        if (cmd.Connection.State != System.Data.ConnectionState.Closed)
                        {
                            cmd.Connection.Close();
                            cmd.Connection.Dispose();
                        }
                    }
                }
                disposed = true;
            }
            catch (Exception ex)
            {
              //  new WriteLogData().WriteLogError_File("Dispose failed", ex.Message);
            } //dispose แล้วผ่านไป
        }
        public System.Data.IDbCommand Command
        {
            get
            {
                return this.cmd;
            }
            set
            {
                this.cmd = value;
            }
        }
        #endregion

        #region Constructor section
        public CommandData()
        {
            this.cmd = new SqlCommand();
        }
        #endregion

        #region Destructor section
        ~CommandData()
        {
            Dispose(false);
        }
        #endregion

        #region Public methods section

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private string GetConnectionString()
        {
            string connectionstring = "";
            try
            {
                connectionstring = WebServiceServer.ConnectionString();
            }
            catch (Exception ex)
            {
                //new WriteLogData().WriteLogError_File("GetConnectionString error", "Connectionstring :" + connectionstring + "|" + ex.Message);
                throw new Exception("GetConnectionString error : " + ex.Message);
            }
            return connectionstring;
        }
        public void SetStoreProcedure(string storeProcedure)
        {
            this.cmd.Parameters.Clear();
            this.cmd.CommandText = strdbOwner + storeProcedure;
            this.cmd.CommandType = CommandType.StoredProcedure;
        }
        public void SetSQLCommand(string sqlCommand)
        {
            this.cmd.CommandText = sqlCommand;
            this.cmd.CommandType = System.Data.CommandType.Text;
        }
        public void SetParameter(string parameterName, DbType dataType, ParameterDirection direction)
        {
            SetParameter(parameterName, dataType, direction, null);
        }
        public void SetParameter(string parameterName, System.Data.DbType dataType, ParameterDirection direction, object values)
        {
            SqlParameter param = new SqlParameter(parameterName, dataType);
            param.Direction = direction;
            values = values.GetType().FullName.Equals("System.String") ? values.ToString().Trim() : values;
            param.Value = values;
            this.cmd.Parameters.Add(param);
        }
        public System.Data.DataSet ExecuteDataSet()
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                try
                {
                    conn.Open();
                    this.cmd.Connection = conn;
                    this.cmd.CommandTimeout = commandTimeout;
                    IDbDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = this.cmd;
                    dataAdapter.Fill(ds);
                }
                catch (Exception ex)
                {
                    Dispose();
                   // new WriteLogData().WriteLogError_File("ExecuteDataSet failed", ex.Message);
                }
                finally
                {
                    Dispose();
                }
            }
            return ds;
        }
        public System.Data.DataTable ExecuteDataTable()
        {
            System.Data.DataSet ds = this.ExecuteDataSet();
            if (ds.Tables.Count > 0)
                return ds.Tables[0].Copy();
            else
                return null;
        }
        public System.Data.DataRow ExecuteDataRow()
        {
            System.Data.DataTable dt = this.ExecuteDataTable();

            if ((dt != null) && (dt.Rows.Count > 0))
                return dt.Rows[0];
            else
                return null;
        }
        public System.Data.DataRowCollection ExecuteDataRowCollection()
        {
            System.Data.DataSet dts = this.ExecuteDataSet();
            if (dts.Tables[0] != null && dts.Tables[0].Rows.Count > 0)
            {
                return dts.Tables[0].Rows;
            }
            return null;
        }
        public int ExecuteNonQuery()
        {
            int ret = 0;
            using (this.cmd.Connection = new SqlConnection(GetConnectionString()))
            {
                try
                {
                    this.cmd.Connection.Open();
                    this.cmd.CommandTimeout = commandTimeout;
                    //ถ้า ret = -1 แสดงว่า Execute ผ่าน | 0 ไม่ผ่าน
                    ret = this.cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Dispose();
                    //new WriteLogData().WriteLogError_File("ExecuteNonQuery failed", ex.Message);
                    throw new Exception(ex.Message);
                }
                finally
                {
                    Dispose();
                }
                return ret;
            }
        }
        public string ExecuteGetReturnString(System.Data.IDbDataParameter returnParameter)
        {
            this.ExecuteNonQuery();
            if (!returnParameter.Value.Equals(DBNull.Value))
            {
                string result = returnParameter.Value.ToString();
                if (!result.Equals("0"))
                {
                    return result;
                }
            }
            return null;
        }
        public int ExecuteGetReturnInt(IDbDataParameter returnParameter)
        {
            this.ExecuteNonQuery();
            if (!returnParameter.Value.Equals(DBNull.Value))
            {
                int result = DataConverter.GetInteger(returnParameter.Value);
                if (!result.Equals(0))
                    return result;
            }
            return int.MinValue;
        }

        #endregion
    }
}
