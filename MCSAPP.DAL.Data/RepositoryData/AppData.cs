﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;
namespace MCSAPP.DAL.Data.RepositoryData
{
    public class AppData : IAppData
    {
        public List<PartDetail2Model> GetPartDetail(DateModel model)
        {
            List<PartDetail2Model> ccr = new List<PartDetail2Model>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_MONTH_" + model.TYPE_STORE);
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new PartDetail2Model
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_ID = DataConverter.GetInteger(item["PJ_ID"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                REV_NO = DataConverter.GetString(item["REV_NO"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                DESIGN_CHANGE = DataConverter.GetString(item["DESIGN_CHANGE"]),
                                PD_WEIGHT = DataConverter.GetDouble(item["PD_WEIGHT"]),
                                PD_LENGTH = DataConverter.GetDouble(item["PD_LENGTH"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                FAB_PLAN = DataConverter.GetInteger(item["FAB_PLAN"]),
                                FAB_ACTUAL = DataConverter.GetInteger(item["FAB_ACTUAL"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_MONTH_" + model.TYPE_STORE ?? "", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<PartDetail2Model> GetOtherDetail(DateModel model)
        {
            List<PartDetail2Model> ccr = new List<PartDetail2Model>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_MONTH_" + model.TYPE_STORE);
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new PartDetail2Model
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),                          
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),                              
                                FAB_ACTUAL = DataConverter.GetInteger(item["FAB_ACTUAL"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),                              
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_MONTH_" + model.TYPE_STORE ??"", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<VTListModel> GetVTDetail(DateModel model)
        {
            List<VTListModel> ccr = new List<VTListModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_DETAIL_" + model.TYPE_STORE);
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new VTListModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                REV_NO = DataConverter.GetString(item["REV_NO"]),
                                CHECK_NO = DataConverter.GetString(item["CHECK_NO"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                POINT = DataConverter.GetString(item["POINT"]),
                                PROBLEM = DataConverter.GetString(item["PROBLEM"]),
                                DWG = DataConverter.GetString(item["DWG"]),
                                ACT = DataConverter.GetString(item["ACT"]),
                                WRONG = DataConverter.GetString(item["WRONG"]),
                                EDIT = DataConverter.GetString(item["EDIT"]),
                                CHECKER = DataConverter.GetString(item["CHECKER"]),
                                CHECK_DATE = DataConverter.GetString(item["CHECK_DATE"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DETAIL_" + model.TYPE_STORE ?? "", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<VTListModel> GetVTForAdd(DateModel model)
        {
            List<VTListModel> ccr = new List<VTListModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_DETAIL_" + model.TYPE_STORE + "_FOR_ADD");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new VTListModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                REV_NO = DataConverter.GetString(item["REV_NO"]),
                                CHECK_NO = DataConverter.GetString(item["CHECK_NO"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                POINT = DataConverter.GetString(item["POINT"]),
                                PROBLEM = DataConverter.GetString(item["PROBLEM"]),
                                DWG = DataConverter.GetString(item["DWG"]),
                                ACT = DataConverter.GetString(item["ACT"]),
                                WRONG = DataConverter.GetString(item["WRONG"]),
                                EDIT = DataConverter.GetString(item["EDIT"]),
                                CHECKER = DataConverter.GetString(item["CHECKER"]),
                                CHECK_DATE = DataConverter.GetString(item["CHECK_DATE"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DETAIL_" + model.TYPE_STORE ?? "" + "_FOR_ADD", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadTypeModel> GetHeaderExcel(DateModel model)
        {
            List<UploadTypeModel> ccr = new List<UploadTypeModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_HEADER_EXCEL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@TYPE_FILE", DbType.String, ParameterDirection.Input, model.TYPE_FILE);
                    command.SetParameter("@TYPE_SHOW", DbType.String, ParameterDirection.Input, model.TYPE_SHOW);
          
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadTypeModel
                            {
                                Text = DataConverter.GetString(item["HEADER"]),
                                Value = DataConverter.GetString(item["HEADER_SHOW"]),
                                Type = DataConverter.GetString(item["HEADER_TYPE"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_HEADER_EXCEL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
    }
}
