﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class WorkData : IWorkData
    {
        public List<UploadTypeModel> GetHeaderExcel(DateModel model)
        {
            List<UploadTypeModel> ccr = new List<UploadTypeModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_HEADER_EXCEL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@TYPE_FILE", DbType.String, ParameterDirection.Input, model.TYPE_FILE);
                    command.SetParameter("@TYPE_SHOW", DbType.String, ParameterDirection.Input, model.TYPE_SHOW);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadTypeModel
                            {
                                Text = DataConverter.GetString(item["HEADER"]),
                                Value = DataConverter.GetString(item["HEADER_SHOW"]),
                                Type = DataConverter.GetString(item["HEADER_TYPE"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_HEADER_EXCEL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForWorkModel GetSummaryForWorkByAutoGas1(DateModel model)
        {
            AllSummaryForWorkModel ccr = new AllSummaryForWorkModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkModel d = new PartSummaryForWorkModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForWorkModel GetSummaryForWorkByAutoGas2(DateModel model)
        {
            AllSummaryForWorkModel ccr = new AllSummaryForWorkModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_2");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkModel d = new PartSummaryForWorkModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;

                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForWorkModel GetSummaryForWorkByAutoGas3(DateModel model)
        {
            AllSummaryForWorkModel ccr = new AllSummaryForWorkModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_3");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkModel d = new PartSummaryForWorkModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForWorkModel GetSummaryForWorkByTaper(DateModel model)
        {
            AllSummaryForWorkModel ccr = new AllSummaryForWorkModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_WORK_BY_TAPER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkModel d = new PartSummaryForWorkModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForWorkModel GetSummaryForWorkByPart(DateModel model)
        {
            AllSummaryForWorkModel ccr = new AllSummaryForWorkModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_WORK_BY_PART");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkModel d = new PartSummaryForWorkModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public PartDetailModel GetDetailSummaryForWorkByAutoGas1(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@PROCESS_TYPE", DbType.String, ParameterDirection.Input, model.PROCESS_TYPE);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY"]),
                                ACT_LENGTH = DataConverter.GetDecimal(item["ACT_LENGTH"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public PartDetailModel GetDetailSummaryForWorkByAutoGas2(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_2");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),


                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public PartDetailModel GetDetailSummaryForWorkByAutoGas3(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_3");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),


                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public PartDetailModel GetDetailSummaryForWorkByTaper(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_TAPER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),


                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public PartDetailModel GetDetailSummaryForWorkByPart(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_Part");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@PROCESS_TYPE", DbType.String, ParameterDirection.Input, model.PROCESS_TYPE);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY"]),
                                ACT_LENGTH = DataConverter.GetDecimal(item["ACT_LENGTH"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),


                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public PartDetailModel GetDetailForCheckSymbol(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_FOR_CHECK_SYMBOL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@PJ_ID", DbType.String, ParameterDirection.Input, model.PJ_ID);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@DEPT_CODE", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACTUAL_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),


                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas1(DateModel model)
        {
            AllSummaryForSalaryModel ccr = new AllSummaryForSalaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_SALARY_BY_AUTOGAS_1");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryModel d = new PartSummaryForSalaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas2(DateModel model)
        {
            AllSummaryForSalaryModel ccr = new AllSummaryForSalaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_SALARY_BY_AUTOGAS_2");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryModel d = new PartSummaryForSalaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas3(DateModel model)
        {
            AllSummaryForSalaryModel ccr = new AllSummaryForSalaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_SALARY_BY_AUTOGAS_3");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryModel d = new PartSummaryForSalaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByTaper(DateModel model)
        {
            AllSummaryForSalaryModel ccr = new AllSummaryForSalaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_SALARY_BY_TAPER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryModel d = new PartSummaryForSalaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllSummaryForSalaryModel GetSummaryForSalaryByPart(DateModel model)
        {
            AllSummaryForSalaryModel ccr = new AllSummaryForSalaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_FOR_SALARY_BY_PART");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryModel d = new PartSummaryForSalaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_FOR_WORK_BY_AUTOGAS_1", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllShowSummaryModel GetSummaryForGrinder(DateModel model)
        {
            AllShowSummaryModel ccr = new AllShowSummaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_GRINDER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<ShowSummaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ShowSummaryModel d = new ShowSummaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetString(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetString(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetString(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetString(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetString(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetString(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetString(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetString(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetString(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetString(item["DATA_10"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_GRINDER", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public AllShowSummaryModel GetSummaryForCut(DateModel model)
        {
            AllShowSummaryModel ccr = new AllShowSummaryModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_SUMMARY_SHOW_CUT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<ShowSummaryModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ShowSummaryModel d = new ShowSummaryModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetString(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetString(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetString(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetString(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetString(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetString(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetString(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetString(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetString(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetString(item["DATA_10"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_SUMMARY_SHOW_CUT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        //New Endtab

        public ResultModel SetUploadEndTab(UploadEndTabModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_END_TAB");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@PRICE", DbType.Double, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadEndTabLog(UploadEndTabModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_END_TAB_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@PRICE", DbType.Double, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadEndTabModel> GetUploadEndTabLog(DateModel model)
        {
            List<UploadEndTabModel> ccr = new List<UploadEndTabModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_END_TAB_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0'));
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadEndTabModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                PRICE = DataConverter.GetDouble(item["PRICE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadEndTabModel> GetUploadEndTab(DateModel model)
        {
            List<UploadEndTabModel> ccr = new List<UploadEndTabModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_END_TAB");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0'));
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadEndTabModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                PRICE = DataConverter.GetDouble(item["PRICE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadEndTabLog(UploadEndTabModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_END_TAB_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadEndTab(UploadEndTabModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_END_TAB");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        //Part
        public ResultModel SetUploadCutPart(UploadCutPartModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_CUT_PART");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@AMOUNT", DbType.Double, ParameterDirection.Input, model.AMOUNT);
                    command.SetParameter("@EMP_CODE_RECIVE", DbType.String, ParameterDirection.Input, model.EMP_CODE_RECIVE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_CUT_PART", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadCutPartLog(UploadCutPartModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {

                    command.SetStoreProcedure("SP_SET_UPLOAD_CUT_PART_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@AMOUNT", DbType.Double, ParameterDirection.Input, model.AMOUNT);
                    command.SetParameter("@EMP_CODE_RECIVE", DbType.String, ParameterDirection.Input, model.EMP_CODE_RECIVE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_CUT_PART_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadCutPartModel> GetUploadCutPartLog(DateModel model)
        {
            List<UploadCutPartModel> ccr = new List<UploadCutPartModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_CUT_PART_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadCutPartModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                EMP_CODE_RECIVE = DataConverter.GetString(item["EMP_CODE_RECIVE"]),
                                FULL_NAME_RECIVE = DataConverter.GetString(item["FULL_NAME_RECIVE"]),
                                DEPT_NAME_RECIVE = DataConverter.GetString(item["DEPT_NAME_RECIVE"]),
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                WEEK_NAME = DataConverter.GetString(item["WEEK_NAME"]),
                                AMOUNT = DataConverter.GetDouble(item["AMOUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_CUT_PART_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadCutPartModel> GetUploadCutPart(DateModel model)
        {
            List<UploadCutPartModel> ccr = new List<UploadCutPartModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_CUT_PART");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadCutPartModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                EMP_CODE_RECIVE = DataConverter.GetString(item["EMP_CODE_RECIVE"]),
                                FULL_NAME_RECIVE = DataConverter.GetString(item["FULL_NAME_RECIVE"]),
                                DEPT_NAME_RECIVE = DataConverter.GetString(item["DEPT_NAME_RECIVE"]),
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                AMOUNT = DataConverter.GetDouble(item["AMOUNT"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadCutPartLog(UploadCutPartModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_CUT_PART_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@EMP_CODE_RECIVE", DbType.String, ParameterDirection.Input, model.EMP_CODE_RECIVE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_CUT_PART_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadCutPart(UploadCutPartModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_CUT_PART");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_CUT_PART", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        //Process
        public List<ProcessTypeModel> GetMasterProcessType(SearchModel model)
        {
            List<ProcessTypeModel> ccr = new List<ProcessTypeModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_MST_PROCESS_TYPE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@DEPT_CODE", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ProcessTypeModel
                            {
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                ROW_ID = DataConverter.GetInteger(item["ROW_NUM"]),
                                ID = DataConverter.GetInteger(item["ID"]),
                                MAIN_PROCESS = DataConverter.GetString(item["DEPT_NAME"]),
                                PROCESS_NAME = DataConverter.GetString(item["PROCESS_NAME"]),
                                USER_UPDATE = DataConverter.GetString(item["USER_UPDATE"]),
                                TIME_UPDATE = DataConverter.GetString(item["TIME_UPDATE"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel SetMasterProcessType(ProcessTypeModel model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_MST_PROCESS_TYPE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@DEPT_CODE", DbType.Int32, ParameterDirection.Input, model.MAIN_PROCESS);
                    command.SetParameter("@PROCESS_NAME", DbType.String, ParameterDirection.Input, model.PROCESS_NAME);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs.CODE = DataConverter.GetString(item["ACTION_RESULT_CODE"]);
                        rs.MESSAGE = DataConverter.GetString(item["ACTION_RESULT_MSG"]);
                        rs.SUCCESS = DataConverter.GetString(item["ACTION_RESULT_CODE"]) == "01";
                        // rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_MST_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMasterProcessType(ProcessTypeModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DELETE_MST_PROCESS_TYPE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DELETE_MST_GROUP", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetCheckSymbol(List<PartDatilSummaryForWorkModel> data)
        {
            ResultModel rs = new ResultModel();

            string s = DataConverter.SerializeObject(data);
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_SET_FOR_CHECK_SYMBOL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@STRING", DbType.String, ParameterDirection.Input, s);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_SET_FOR_CHECK_SYMBOL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }


        public ResultModel DelCheckSymbolData(List<DataValueModel> data)
        {
            ResultModel rs = new ResultModel();

            string s = DataConverter.SerializeObject(data);
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_DEL_FOR_CHECK_SYMBOL_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@STRING", DbType.String, ParameterDirection.Input, s);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_SET_FOR_CHECK_SYMBOL_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public PartDetailModel GetDetailForCheckSymbolData(DateModel model)
        {
            PartDetailModel ccr = new PartDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("PART_SP_GET_FOR_CHECK_SYMBOL_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@PJ_NAME", DbType.String, ParameterDirection.Input, model.PJ_NAME);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@DEPT_CODE", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),



                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartDatilSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new PartDatilSummaryForWorkModel
                            {
                                ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                SYMBOL = DataConverter.GetString(item["SYMBOL"]),
                                PART_ITEM = DataConverter.GetInteger(item["PART_ITEM"]),
                                PART_SIZE = DataConverter.GetString(item["PART_SIZE"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                HOLE = DataConverter.GetInteger(item["HOLE"]),
                                TAPER = DataConverter.GetDecimal(item["TAPER"]),
                                PLAN_QTY = DataConverter.GetInteger(item["PLAN_QTY"]),
                                ACT_QTY = DataConverter.GetInteger(item["ACT_QTY_SAVE"]),
                                ACT_GROUP = DataConverter.GetString(item["ACT_GROUP"]),
                                ACTUAL_USER = DataConverter.GetString(item["ACT_USER"]),
                                ACT_NAME = DataConverter.GetString(item["ACT_NAME"]),
                                ACT_DATE = DataConverter.GetString(item["ACT_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),


                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "PART_SP_GET_FOR_CHECK_SYMBOL_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2

        // Built Beam/Box Store Connect 
        // 2018-01-26 by chaiwud.ta
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, "12001");

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBeam(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // Saw
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_SAW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_SAW", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, "12002");

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltSawBeam(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_SAW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_SAW", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // Adjust
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_ADJUST");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_ADJUST", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, "12004");

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAdjustBeam(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_ADJUST");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_ADJUST", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // Drill & Shot Blast
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_DRILL_SHOT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_DRILL_SHOT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT2");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT2", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_DRILL_SHOT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;


                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_DRILL_SHOT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBeam(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("OTHER_SP_GET_SUMMARY_FOR_WORK_BY_ADJUST");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "OTHER_SP_GET_SUMMARY_FOR_WORK_BY_ADJUST", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByOtherBeam(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_BEAM");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.TYPE_STORE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_REV = DataConverter.GetString(item["PD_REV"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                PD_DWG = DataConverter.GetString(item["PD_DWG"]),
                                BUILT_SIZE = DataConverter.GetString(item["BUILT_SIZE"]),
                                PD_BLOCK = DataConverter.GetString(item["PD_BLOCK"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                //-------
                                //----
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_BEAM", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBeam(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("OTHER_SP_GET_SUMMARY_FOR_SALARY_BY_ADJUST");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_OTHER_ADJUST", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // Other Adjust Box
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_ADJUST");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_BOX", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByOtherBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.TYPE_STORE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_REV = DataConverter.GetString(item["PD_REV"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                PD_DWG = DataConverter.GetString(item["PD_DWG"]),
                                BUILT_SIZE = DataConverter.GetString(item["BUILT_SIZE"]),
                                PD_BLOCK = DataConverter.GetString(item["PD_BLOCK"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                                //----
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                            });

                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_BOX", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_ADJUST");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_ADJUST", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        // Other Finishing
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherFinishing(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("OTHER_SP_GET_SUMMARY_FOR_WORK_BY_FINISHING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "OTHER_SP_GET_SUMMARY_FOR_WORK_BY_FINISHING", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByOtherFinishing(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("OTHER_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_FINISHING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.TYPE_STORE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                PD_REV = DataConverter.GetString(item["PD_REV"]),
                                PD_CODE = DataConverter.GetString(item["PD_CODE"]),
                                PD_DWG = DataConverter.GetString(item["PD_DWG"]),
                                BUILT_SIZE = DataConverter.GetString(item["BUILT_SIZE"]),
                                PD_BLOCK = DataConverter.GetString(item["PD_BLOCK"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                                //----
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                            });

                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "OTHER_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_OTHER_FINISHING", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherFinishing(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("OTHER_SP_GET_SUMMARY_FOR_SALARY_BY_FINISHING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "OTHER_SP_GET_SUMMARY_FOR_SALARY_BY_FINISHING", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        #endregion

        // Built Box 
        // 2018-02-12 by chaiwud.ta 
        // FabDiaphragmBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_FAB_DIAPHRAGM
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_DIAPHRAGM");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_FAB_DIAPHRAGM
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, "11002");

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_FAB_DIAPHRAGM
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_DIAPHRAGM");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {
                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // BuiltUpBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP_BOX
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.TYPE_STORE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                BUILT_SIZE = DataConverter.GetString(item["BUILT_SIZE"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP_BOX", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP_BOX
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // AutoRootBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_AUTOROOT
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_AUTOROOT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_AUTOROOT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    string set_process = "";
                    // BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT

                    set_process = model.TYPE_STORE;

                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_AUTOROOT_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, set_process);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                // AUTO ROOT
                                BUILT_LENGTH = DataConverter.GetDecimal(item["BUILT_LENGTH"]), // BUILT_LENGTH
                                HOLE = DataConverter.GetInteger(item["HOLE"]), // BUILT_LENGTH
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAutoRootBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP_BOX
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_AUTOROOT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // DrillSesnetBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_DRILL_SESNET");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_DRILL_SESNET", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_DRILLSESNET_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.TYPE_STORE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_DRILL_SESNET
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_DRILL_SESNET");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_DRILLSESNET", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // GougingRepairBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_GOUGING_REPAIR
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_GOUGING_REPAIR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_GOUGING_REPAIR", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    string set_process = "";
                    if (model.TYPE_STORE.ToUpper() == "GOUGING" || model.TYPE_STORE.ToUpper() == "ผลงาน GOUGING")
                    {
                        set_process = "11009";
                    }
                    else if (model.TYPE_STORE.ToUpper() == "REPAIR" || model.TYPE_STORE.ToUpper() == "ผลงาน REPAIR")
                    {
                        set_process = "11010";
                    }
                    // BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_BOX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, set_process);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_GOUGING_REPAIR
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_GOUGING_REPAIR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_GOUGING_REPAIR", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        // FacingBox
        public AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            AllSummaryForWorkBuiltModel ccr = new AllSummaryForWorkBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_FACING
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_WORK_BY_FACING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TEXT": d.Text = DataConverter.GetString(item["TEXT"]); break;
                                    case "VALUES": d.Value = DataConverter.GetString(item["VALUES"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForWorkBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForWorkBuiltModel d = new PartSummaryForWorkBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_WORK_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public BuiltDetailModel GetDetailSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            BuiltDetailModel ccr = new BuiltDetailModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT
                    command.SetStoreProcedure("BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, "11012");

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Header.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["TYPE_HEADER"]),
                                Value = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<BuiltDetailSummaryForWorkModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            ccr.Data.Add(new BuiltDetailSummaryForWorkModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE_NAME = DataConverter.GetString(item["BUILT_TYPE_NAME"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetInteger(item["BUILT_NO"]),
                                PLACE = DataConverter.GetString(item["PLACE"]),
                                BUILT_WEIGHT = DataConverter.GetDecimal(item["BUILT_WEIGHT"]),
                                BUILT_PLAN = DataConverter.GetDecimal(item["BUILT_PLAN"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                UG_NAME = DataConverter.GetString(item["UG_NAME"]),
                                PA_PLAN_DATE = DataConverter.GetString(item["PA_PLAN_DATE"]),
                                ACT_USER = DataConverter.GetString(item["ACT_USER"]),
                                FULLNAME = DataConverter.GetString(item["FULLNAME"]),
                                PA_ACTUAL_DATE = DataConverter.GetString(item["PA_ACTUAL_DATE"]),
                                BUILT_ACTUAL = DataConverter.GetDecimal(item["BUILT_ACTUAL"]),
                                PA_NO = DataConverter.GetInteger(item["PA_NO"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_DETAIL_SP_GET_SUMMARY_FOR_WORK_BY_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFacingBox(DateModel model)
        {
            AllSummaryForSalaryBuiltModel ccr = new AllSummaryForSalaryBuiltModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    // BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_FACING
                    command.SetStoreProcedure("BUILT_BOX_SP_GET_SUMMARY_FOR_SALARY_BY_FACING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        ccr.Header = new List<DataValueModel>();
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DataValueModel d = new DataValueModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "TYPE_HEADER": d.Text = DataConverter.GetString(item["TYPE_HEADER"]); break;
                                    case "DATA_HEADER": d.Value = DataConverter.GetString(item["DATA_HEADER"]); break;
                                }
                            }
                            ccr.Header.Add(d);
                        }
                    }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[1]))
                    {
                        ccr.Data = new List<PartSummaryForSalaryBuiltModel>();
                        foreach (DataRow item in result.Tables[1].Rows)
                        {
                            PartSummaryForSalaryBuiltModel d = new PartSummaryForSalaryBuiltModel();
                            foreach (DataColumn column in result.Tables[1].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    // split column
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]); break;
                                    case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["GOAL_DAY"]); break;
                                    // case "GOAL_DAY": d.GOAL_DAY = DataConverter.GetString(item["EMP_GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    //
                                    case "GOAL_REAL": d.GOAL_REAL = DataConverter.GetDouble(item["GOAL_REAL"]); break;
                                    case "GOAL_SUM": d.GOAL_SUM = DataConverter.GetDouble(item["GOAL_SUM"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "ROW_TYPE": d.ROW_TYPE = DataConverter.GetString(item["ROW_TYPE"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetDouble(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetDouble(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetDouble(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;

                                }
                            }
                            ccr.Data.Add(d);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "BUILT_BEAM_SP_GET_SUMMARY_FOR_SALARY_BY_BUILT_UP", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        #endregion
    }


}
