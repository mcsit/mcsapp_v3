﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class WeekData : IWeekData
    {
        public List<WeekModel> GetWeekByMonth(DateModel model)
        {
            List<WeekModel> ccr = new List<WeekModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WEEK_BY_MONTH");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input,model.YEAR);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new WeekModel
                            {
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                WEEK_NAME = DataConverter.GetString(item["WEEK_NAME"]),
                        });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WEEK_BY_MONTH", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<GoalModel> GetTxnMonthByWeek(DateModel model)
        {
            List<GoalModel> ccr = new List<GoalModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_TXN_MONTH_BY_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new GoalModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetString(item["EMP_GOAL"]),
                                D_01 = DataConverter.GetInteger(item["01"]),
                                D_02 = DataConverter.GetInteger(item["02"]),
                                D_03 = DataConverter.GetInteger(item["03"]),
                                D_04 = DataConverter.GetInteger(item["04"]),
                                D_05 = DataConverter.GetInteger(item["05"]),
                                D_06 = DataConverter.GetInteger(item["06"]),
                                D_07 = DataConverter.GetInteger(item["07"]),
                                D_08 = DataConverter.GetInteger(item["08"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_TXN_MONTH_BY_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<GoalSumModel> GetSummaryByWork(DateModel model)
        {
            List<GoalSumModel> ccr = new List<GoalSumModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_BY_WORK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new GoalSumModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                OTHER = DataConverter.GetInteger(item["OTHER"]),
                                REVISE = DataConverter.GetInteger(item["REVISE"]),
                                WELD = DataConverter.GetInteger(item["WELD"]),
                                DIMENSION = DataConverter.GetInteger(item["DIMENSION"]),
      
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_BY_WORK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SararyModel> GetSararyMonthByWeek(DateModel model)
        {
            List<SararyModel> ccr = new List<SararyModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SARARY_MONTH_BY_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new SararyModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetString(item["EMP_GOAL"]),
                                D_01 = DataConverter.GetDouble(item["01"]),
                                D_02 = DataConverter.GetDouble(item["02"]),
                                D_03 = DataConverter.GetDouble(item["03"]),
                                D_04 = DataConverter.GetDouble(item["04"]),
                                D_05 = DataConverter.GetDouble(item["05"]),
                                D_06 = DataConverter.GetDouble(item["06"]),
                                D_07 = DataConverter.GetDouble(item["07"]),
                                D_08 = DataConverter.GetDouble(item["08"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SARARY_MONTH_BY_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<GoalSumModel> GetSummaryWorkByCode(DateModel model)
        {
            List<GoalSumModel> ccr = new List<GoalSumModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_WORK_BY_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new GoalSumModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                OTHER = DataConverter.GetInteger(item["OTHER"]),
                                REVISE = DataConverter.GetInteger(item["REVISE"]),
                                WELD = DataConverter.GetInteger(item["WELD"]),
                                DIMENSION = DataConverter.GetInteger(item["DIMENSION"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_BY_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SararyModel> GetSararyMonthByCode(DateModel model)
        {
            List<SararyModel> ccr = new List<SararyModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SARARY_MONTH_BY_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new SararyModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetString(item["EMP_GOAL"]),
                                D_01 = DataConverter.GetDouble(item["01"]),
                                D_02 = DataConverter.GetDouble(item["02"]),
                                D_03 = DataConverter.GetDouble(item["03"]),
                                D_04 = DataConverter.GetDouble(item["04"]),
                                D_05 = DataConverter.GetDouble(item["05"]),
                                D_06 = DataConverter.GetDouble(item["06"]),
                                D_07 = DataConverter.GetDouble(item["07"]),
                                D_08 = DataConverter.GetDouble(item["08"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SARARY_MONTH_BY_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SumSalaryModel> GetSalaryFab(DateModel model)
        {
            List<SumSalaryModel> ccr = new List<SumSalaryModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SARARY_FAB");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);


                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new SumSalaryModel
                            {
                                ROW_ID = DataConverter.GetInteger(item["ROW_ID"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),

                                SALARY = DataConverter.GetInteger(item["SALARY"]),
                                SALARY_POSITION = DataConverter.GetInteger(item["SALARY_POSITION"]),
                                SALARY_QUALITY = DataConverter.GetInteger(item["SALARY_QUALITY"]),
                                SALARY_SUM = DataConverter.GetInteger(item["SALARY_SUM"]),
 
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SARARY_FAB", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DayModel> GetWorkLessGoal(DateModel model)
        {
            List<DayModel> ccr = new List<DayModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_LESS_GOAL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DayModel d = new DayModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "SUM_GOAL": d.SUM_GOAL = DataConverter.GetDoubleNull(item["SUM_GOAL"]); break;
                                    case "SUM_WORK": d.SUM_WORK = DataConverter.GetDoubleNull(item["SUM_WORK"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "01": d.D_01 = DataConverter.GetDoubleNull(item["01"]); break;
                                    case "02": d.D_02 = DataConverter.GetDoubleNull(item["02"]); break;
                                    case "03": d.D_03 = DataConverter.GetDoubleNull(item["03"]); break;
                                    case "04": d.D_04 = DataConverter.GetDoubleNull(item["04"]); break;
                                    case "05": d.D_05 = DataConverter.GetDoubleNull(item["05"]); break;
                                    case "06": d.D_06 = DataConverter.GetDoubleNull(item["06"]); break;
                                    case "07": d.D_07 = DataConverter.GetDoubleNull(item["07"]); break;
                                    case "08": d.D_08 = DataConverter.GetDoubleNull(item["08"]); break;
                                    case "09": d.D_09 = DataConverter.GetDoubleNull(item["09"]); break;
                                    case "10": d.D_10 = DataConverter.GetDoubleNull(item["10"]); break;
                                    case "11": d.D_11 = DataConverter.GetDoubleNull(item["11"]); break;
                                    case "12": d.D_12 = DataConverter.GetDoubleNull(item["12"]); break;
                                    case "13": d.D_13 = DataConverter.GetDoubleNull(item["13"]); break;
                                    case "14": d.D_14 = DataConverter.GetDoubleNull(item["14"]); break;
                                    case "15": d.D_15 = DataConverter.GetDoubleNull(item["15"]); break;
                                    case "16": d.D_16 = DataConverter.GetDoubleNull(item["16"]); break;
                                    case "17": d.D_17 = DataConverter.GetDoubleNull(item["17"]); break;
                                    case "18": d.D_18 = DataConverter.GetDoubleNull(item["18"]); break;
                                    case "19": d.D_19 = DataConverter.GetDoubleNull(item["19"]); break;
                                    case "20": d.D_20 = DataConverter.GetDoubleNull(item["20"]); break;
                                    case "21": d.D_21 = DataConverter.GetDoubleNull(item["21"]); break;
                                    case "22": d.D_22 = DataConverter.GetDoubleNull(item["22"]); break;
                                    case "23": d.D_23 = DataConverter.GetDoubleNull(item["23"]); break;
                                    case "24": d.D_24 = DataConverter.GetDoubleNull(item["24"]); break;
                                    case "25": d.D_25 = DataConverter.GetDoubleNull(item["25"]); break;
                                    case "26": d.D_26 = DataConverter.GetDoubleNull(item["26"]); break;
                                    case "27": d.D_27 = DataConverter.GetDoubleNull(item["27"]); break;
                                    case "28": d.D_28 = DataConverter.GetDoubleNull(item["28"]); break;
                                    case "29": d.D_29 = DataConverter.GetDoubleNull(item["29"]); break;
                                    case "30": d.D_30 = DataConverter.GetDoubleNull(item["30"]); break;
                                    case "31": d.D_31 = DataConverter.GetDoubleNull(item["31"]); break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_LESS_GOAL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<WorkModel> GetWorkMonth(DateModel model)
        {
            List<WorkModel> ccr = new List<WorkModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_MONTH_" + model.TYPE_STORE);
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);    
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new WorkModel
                            {
                                PROJECT_NAME = DataConverter.GetString(item["PROJECT_NAME"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                ACTUAL = DataConverter.GetInteger(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_MONTH_" + model.TYPE_STORE ?? "", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<WorkModel> GetWorkMonthByDay(DateModel model)
        {
            List<WorkModel> ccr = new List<WorkModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_MONTH_DAY");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.Int16, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new WorkModel
                            {
                                PROJECT_NAME = DataConverter.GetString(item["PROJECT_NAME"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                ACTUAL = DataConverter.GetInteger(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                EMP_GOAL = DataConverter.GetDouble(item["EMP_GOAL"]),
                                SALARY = DataConverter.GetDouble(item["SALARY"]),
                                QUALITY = DataConverter.GetDouble(item["QUALITY"]),
                                PROCEDURE =DataConverter.GetDouble(item["PROCEDURE"]),
                                LEVEL_SALARY = DataConverter.GetDouble(item["LEVEL_SALARY"]),
                                LEVEL_QUALITY = DataConverter.GetDouble(item["LEVEL_QUALITY"]),
                                LEVEL_PROCEDURE = DataConverter.GetDouble(item["LEVEL_PROCEDURE"]),
                                SUM_SALARY = DataConverter.GetDouble(item["SUM_SALARY"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_MONTH_DAY", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DayModel> GetWorkByDay(DateModel model)
        {
            List<DayModel> ccr = new List<DayModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_BY_DAY");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                      
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DayModel d = new DayModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "01": d.D_01 = DataConverter.GetDoubleNull(item["01"]); break;
                                    case "02": d.D_02 = DataConverter.GetDoubleNull(item["02"]); break;
                                    case "03": d.D_03 = DataConverter.GetDoubleNull(item["03"]); break;
                                    case "04": d.D_04 = DataConverter.GetDoubleNull(item["04"]); break;
                                    case "05": d.D_05 = DataConverter.GetDoubleNull(item["05"]); break;
                                    case "06": d.D_06 = DataConverter.GetDoubleNull(item["06"]); break;
                                    case "07": d.D_07 = DataConverter.GetDoubleNull(item["07"]); break;
                                    case "08": d.D_08 = DataConverter.GetDoubleNull(item["08"]); break;
                                    case "09": d.D_09 = DataConverter.GetDoubleNull(item["09"]); break;
                                    case "10": d.D_10 = DataConverter.GetDoubleNull(item["10"]); break;
                                    case "11": d.D_11 = DataConverter.GetDoubleNull(item["11"]); break;
                                    case "12": d.D_12 = DataConverter.GetDoubleNull(item["12"]); break;
                                    case "13": d.D_13 = DataConverter.GetDoubleNull(item["13"]); break;
                                    case "14": d.D_14 = DataConverter.GetDoubleNull(item["14"]); break;
                                    case "15": d.D_15 = DataConverter.GetDoubleNull(item["15"]); break;
                                    case "16": d.D_16 = DataConverter.GetDoubleNull(item["16"]); break;
                                    case "17": d.D_17 = DataConverter.GetDoubleNull(item["17"]); break;
                                    case "18": d.D_18 = DataConverter.GetDoubleNull(item["18"]); break;
                                    case "19": d.D_19 = DataConverter.GetDoubleNull(item["19"]); break;
                                    case "20": d.D_20 = DataConverter.GetDoubleNull(item["20"]); break;
                                    case "21": d.D_21 = DataConverter.GetDoubleNull(item["21"]); break;
                                    case "22": d.D_22 = DataConverter.GetDoubleNull(item["22"]); break;
                                    case "23": d.D_23 = DataConverter.GetDoubleNull(item["23"]); break;
                                    case "24": d.D_24 = DataConverter.GetDoubleNull(item["24"]); break;
                                    case "25": d.D_25 = DataConverter.GetDoubleNull(item["25"]); break;
                                    case "26": d.D_26 = DataConverter.GetDoubleNull(item["26"]); break;
                                    case "27": d.D_27 = DataConverter.GetDoubleNull(item["27"]); break;
                                    case "28": d.D_28 = DataConverter.GetDoubleNull(item["28"]); break;
                                    case "29": d.D_29 = DataConverter.GetDoubleNull(item["29"]); break;
                                    case "30": d.D_30 = DataConverter.GetDoubleNull(item["30"]); break;
                                    case "31": d.D_31 = DataConverter.GetDoubleNull(item["31"]); break;
                                }
                            }

                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_BY_DAY", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<WeeksModel> GetWorkByWeek(DateModel model)
        {
            List<WeeksModel> ccr = new List<WeeksModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_BY_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new WeeksModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                                WEEK_1 = DataConverter.GetDoubleNull(item["1"]),
                                WEEK_2 = DataConverter.GetDoubleNull(item["2"]),
                                WEEK_3 = DataConverter.GetDoubleNull(item["3"]),
                                WEEK_4 = DataConverter.GetDoubleNull(item["4"]),
                                WEEK_5 = DataConverter.GetDoubleNull(item["5"]),
   
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_BY_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DayModel> GetSalaryByDay(DateModel model)
        {
            List<DayModel> ccr = new List<DayModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SALARY_BY_DAY");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DayModel d = new DayModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "01": d.D_01 = DataConverter.GetDoubleNull(item["01"]); break;
                                    case "02": d.D_02 = DataConverter.GetDoubleNull(item["02"]); break;
                                    case "03": d.D_03 = DataConverter.GetDoubleNull(item["03"]); break;
                                    case "04": d.D_04 = DataConverter.GetDoubleNull(item["04"]); break;
                                    case "05": d.D_05 = DataConverter.GetDoubleNull(item["05"]); break;
                                    case "06": d.D_06 = DataConverter.GetDoubleNull(item["06"]); break;
                                    case "07": d.D_07 = DataConverter.GetDoubleNull(item["07"]); break;
                                    case "08": d.D_08 = DataConverter.GetDoubleNull(item["08"]); break;
                                    case "09": d.D_09 = DataConverter.GetDoubleNull(item["09"]); break;
                                    case "10": d.D_10 = DataConverter.GetDoubleNull(item["10"]); break;
                                    case "11": d.D_11 = DataConverter.GetDoubleNull(item["11"]); break;
                                    case "12": d.D_12 = DataConverter.GetDoubleNull(item["12"]); break;
                                    case "13": d.D_13 = DataConverter.GetDoubleNull(item["13"]); break;
                                    case "14": d.D_14 = DataConverter.GetDoubleNull(item["14"]); break;
                                    case "15": d.D_15 = DataConverter.GetDoubleNull(item["15"]); break;
                                    case "16": d.D_16 = DataConverter.GetDoubleNull(item["16"]); break;
                                    case "17": d.D_17 = DataConverter.GetDoubleNull(item["17"]); break;
                                    case "18": d.D_18 = DataConverter.GetDoubleNull(item["18"]); break;
                                    case "19": d.D_19 = DataConverter.GetDoubleNull(item["19"]); break;
                                    case "20": d.D_20 = DataConverter.GetDoubleNull(item["20"]); break;
                                    case "21": d.D_21 = DataConverter.GetDoubleNull(item["21"]); break;
                                    case "22": d.D_22 = DataConverter.GetDoubleNull(item["22"]); break;
                                    case "23": d.D_23 = DataConverter.GetDoubleNull(item["23"]); break;
                                    case "24": d.D_24 = DataConverter.GetDoubleNull(item["24"]); break;
                                    case "25": d.D_25 = DataConverter.GetDoubleNull(item["25"]); break;
                                    case "26": d.D_26 = DataConverter.GetDoubleNull(item["26"]); break;
                                    case "27": d.D_27 = DataConverter.GetDoubleNull(item["27"]); break;
                                    case "28": d.D_28 = DataConverter.GetDoubleNull(item["28"]); break;
                                    case "29": d.D_29 = DataConverter.GetDoubleNull(item["29"]); break;
                                    case "30": d.D_30 = DataConverter.GetDoubleNull(item["30"]); break;
                                    case "31": d.D_31 = DataConverter.GetDoubleNull(item["31"]); break;
                                }
                            }

                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SALARY_BY_DAY", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<WeeksModel> GetSalaryByWeek(DateModel model)
        {
            List<WeeksModel> ccr = new List<WeeksModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SALARY_BY_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            WeeksModel d = new WeeksModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1": d.WEEK_1 = DataConverter.GetDoubleNull(item["1"]); break;
                                    case "2": d.WEEK_2 = DataConverter.GetDoubleNull(item["2"]); break;
                                    case "3": d.WEEK_3 = DataConverter.GetDoubleNull(item["3"]); break;
                                    case "4": d.WEEK_4 = DataConverter.GetDoubleNull(item["4"]); break;
                                    case "5": d.WEEK_5 = DataConverter.GetDoubleNull(item["5"]); break;
                                 
                                }
                            }
                            ccr.Add(d);
                            //ccr.Add(new WeeksModel
                            //{

                            //    EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                            //    FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                            //    EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                            //    DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                            //    TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            //    WEEK_1 = DataConverter.GetDoubleNull(item["1"]),
                            //    WEEK_2 = DataConverter.GetDoubleNull(item["2"]),
                            //    WEEK_3 = DataConverter.GetDoubleNull(item["3"]),
                            //    WEEK_4 = DataConverter.GetDoubleNull(item["4"]),
                            //    WEEK_5 = DataConverter.GetDoubleNull(item["5"]),

                            //});
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SALARY_BY_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DayModel> GetSalaryGroupByDay(DateModel model)
        {
            List<DayModel> ccr = new List<DayModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SALARY_GROUP_BY_DAY");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DayModel d = new DayModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "01": d.D_01 = DataConverter.GetDoubleNull(item["01"]); break;
                                    case "02": d.D_02 = DataConverter.GetDoubleNull(item["02"]); break;
                                    case "03": d.D_03 = DataConverter.GetDoubleNull(item["03"]); break;
                                    case "04": d.D_04 = DataConverter.GetDoubleNull(item["04"]); break;
                                    case "05": d.D_05 = DataConverter.GetDoubleNull(item["05"]); break;
                                    case "06": d.D_06 = DataConverter.GetDoubleNull(item["06"]); break;
                                    case "07": d.D_07 = DataConverter.GetDoubleNull(item["07"]); break;
                                    case "08": d.D_08 = DataConverter.GetDoubleNull(item["08"]); break;
                                    case "09": d.D_09 = DataConverter.GetDoubleNull(item["09"]); break;
                                    case "10": d.D_10 = DataConverter.GetDoubleNull(item["10"]); break;
                                    case "11": d.D_11 = DataConverter.GetDoubleNull(item["11"]); break;
                                    case "12": d.D_12 = DataConverter.GetDoubleNull(item["12"]); break;
                                    case "13": d.D_13 = DataConverter.GetDoubleNull(item["13"]); break;
                                    case "14": d.D_14 = DataConverter.GetDoubleNull(item["14"]); break;
                                    case "15": d.D_15 = DataConverter.GetDoubleNull(item["15"]); break;
                                    case "16": d.D_16 = DataConverter.GetDoubleNull(item["16"]); break;
                                    case "17": d.D_17 = DataConverter.GetDoubleNull(item["17"]); break;
                                    case "18": d.D_18 = DataConverter.GetDoubleNull(item["18"]); break;
                                    case "19": d.D_19 = DataConverter.GetDoubleNull(item["19"]); break;
                                    case "20": d.D_20 = DataConverter.GetDoubleNull(item["20"]); break;
                                    case "21": d.D_21 = DataConverter.GetDoubleNull(item["21"]); break;
                                    case "22": d.D_22 = DataConverter.GetDoubleNull(item["22"]); break;
                                    case "23": d.D_23 = DataConverter.GetDoubleNull(item["23"]); break;
                                    case "24": d.D_24 = DataConverter.GetDoubleNull(item["24"]); break;
                                    case "25": d.D_25 = DataConverter.GetDoubleNull(item["25"]); break;
                                    case "26": d.D_26 = DataConverter.GetDoubleNull(item["26"]); break;
                                    case "27": d.D_27 = DataConverter.GetDoubleNull(item["27"]); break;
                                    case "28": d.D_28 = DataConverter.GetDoubleNull(item["28"]); break;
                                    case "29": d.D_29 = DataConverter.GetDoubleNull(item["29"]); break;
                                    case "30": d.D_30 = DataConverter.GetDoubleNull(item["30"]); break;
                                    case "31": d.D_31 = DataConverter.GetDoubleNull(item["31"]); break;
                                }
                            }

                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SALARY_GROUP_BY_DAY", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<WeeksModel> GetSalaryGroupByWeek(DateModel model)
        {
            List<WeeksModel> ccr = new List<WeeksModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SALARY_GROUP_BY_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            WeeksModel d = new WeeksModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1": d.WEEK_1 = DataConverter.GetDoubleNull(item["1"]); break;
                                    case "2": d.WEEK_2 = DataConverter.GetDoubleNull(item["2"]); break;
                                    case "3": d.WEEK_3 = DataConverter.GetDoubleNull(item["3"]); break;
                                    case "4": d.WEEK_4 = DataConverter.GetDoubleNull(item["4"]); break;
                                    case "5": d.WEEK_5 = DataConverter.GetDoubleNull(item["5"]); break;

                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SALARY_GROUP_BY_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataFilterModel> GetDays(DateModel model)
        {
            List<DataFilterModel> ccr = new List<DataFilterModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_DAYS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataFilterModel
                            {
                                Value = DataConverter.GetString(item["dates"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DAYS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataFilterModel> GetWeeks(DateModel model)
        {
            List<DataFilterModel> ccr = new List<DataFilterModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WEEK_BY_MONTH_SUB");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataFilterModel
                            {
                                Value = DataConverter.GetString(item["WEEK_SUB"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WEEK_BY_MONTH_SUB", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetTypeProcess(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_HEADER_PROCESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.TYPE_STORE);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                
                                Value = DataConverter.GetString(item["TYPE_HEADER"]),
                                Text = DataConverter.GetString(item["DATA_HEADER"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_HEADER_PROCESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DataValueModel> GetHeaderWeek(DateModel model)
        {
            List<DataValueModel> ccr = new List<DataValueModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WEEK_HEADER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@ALL", DbType.Int32, ParameterDirection.Input, model.ALL);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DataValueModel
                            {
                                Text = DataConverter.GetString(item["WEEK_HEADER"]),
                                Value = DataConverter.GetString(item["WEEK_SUB"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WEEK_HEADER", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public DataFilterModel GetMaxWeeks(DateModel model)
        {
            DataFilterModel ccr = new DataFilterModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WEEK_MAX");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        ccr.Value = DataConverter.GetString(result.Tables[0].Rows[0]["WEEK_SUB"]);
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WEEK_MAX", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DayModel> GetWorkByDayAndCode(DateModel model)
        {
            List<DayModel> ccr = new List<DayModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_WORK_BY_DAY_AND_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@EMP_DODE", DbType.Int16, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))

                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DayModel d = new DayModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "01": d.D_01 = DataConverter.GetDoubleNull(item["01"]); break;
                                    case "02": d.D_02 = DataConverter.GetDoubleNull(item["02"]); break;
                                    case "03": d.D_03 = DataConverter.GetDoubleNull(item["03"]); break;
                                    case "04": d.D_04 = DataConverter.GetDoubleNull(item["04"]); break;
                                    case "05": d.D_05 = DataConverter.GetDoubleNull(item["05"]); break;
                                    case "06": d.D_06 = DataConverter.GetDoubleNull(item["06"]); break;
                                    case "07": d.D_07 = DataConverter.GetDoubleNull(item["07"]); break;
                                    case "08": d.D_08 = DataConverter.GetDoubleNull(item["08"]); break;
                                    case "09": d.D_09 = DataConverter.GetDoubleNull(item["09"]); break;
                                    case "10": d.D_10 = DataConverter.GetDoubleNull(item["10"]); break;
                                    case "11": d.D_11 = DataConverter.GetDoubleNull(item["11"]); break;
                                    case "12": d.D_12 = DataConverter.GetDoubleNull(item["12"]); break;
                                    case "13": d.D_13 = DataConverter.GetDoubleNull(item["13"]); break;
                                    case "14": d.D_14 = DataConverter.GetDoubleNull(item["14"]); break;
                                    case "15": d.D_15 = DataConverter.GetDoubleNull(item["15"]); break;
                                    case "16": d.D_16 = DataConverter.GetDoubleNull(item["16"]); break;
                                    case "17": d.D_17 = DataConverter.GetDoubleNull(item["17"]); break;
                                    case "18": d.D_18 = DataConverter.GetDoubleNull(item["18"]); break;
                                    case "19": d.D_19 = DataConverter.GetDoubleNull(item["19"]); break;
                                    case "20": d.D_20 = DataConverter.GetDoubleNull(item["20"]); break;
                                    case "21": d.D_21 = DataConverter.GetDoubleNull(item["21"]); break;
                                    case "22": d.D_22 = DataConverter.GetDoubleNull(item["22"]); break;
                                    case "23": d.D_23 = DataConverter.GetDoubleNull(item["23"]); break;
                                    case "24": d.D_24 = DataConverter.GetDoubleNull(item["24"]); break;
                                    case "25": d.D_25 = DataConverter.GetDoubleNull(item["25"]); break;
                                    case "26": d.D_26 = DataConverter.GetDoubleNull(item["26"]); break;
                                    case "27": d.D_27 = DataConverter.GetDoubleNull(item["27"]); break;
                                    case "28": d.D_28 = DataConverter.GetDoubleNull(item["28"]); break;
                                    case "29": d.D_29 = DataConverter.GetDoubleNull(item["29"]); break;
                                    case "30": d.D_30 = DataConverter.GetDoubleNull(item["30"]); break;
                                    case "31": d.D_31 = DataConverter.GetDoubleNull(item["31"]); break;
                                }
                            }

                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_WORK_BY_DAY_AND_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<DayModel> GetSalaryByDayAndCode(DateModel model)
        {
            List<DayModel> ccr = new List<DayModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SALARY_BY_DAY_AND_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@EMP_CODE", DbType.Int16, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            DayModel d = new DayModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "01": d.D_01 = DataConverter.GetDoubleNull(item["01"]); break;
                                    case "02": d.D_02 = DataConverter.GetDoubleNull(item["02"]); break;
                                    case "03": d.D_03 = DataConverter.GetDoubleNull(item["03"]); break;
                                    case "04": d.D_04 = DataConverter.GetDoubleNull(item["04"]); break;
                                    case "05": d.D_05 = DataConverter.GetDoubleNull(item["05"]); break;
                                    case "06": d.D_06 = DataConverter.GetDoubleNull(item["06"]); break;
                                    case "07": d.D_07 = DataConverter.GetDoubleNull(item["07"]); break;
                                    case "08": d.D_08 = DataConverter.GetDoubleNull(item["08"]); break;
                                    case "09": d.D_09 = DataConverter.GetDoubleNull(item["09"]); break;
                                    case "10": d.D_10 = DataConverter.GetDoubleNull(item["10"]); break;
                                    case "11": d.D_11 = DataConverter.GetDoubleNull(item["11"]); break;
                                    case "12": d.D_12 = DataConverter.GetDoubleNull(item["12"]); break;
                                    case "13": d.D_13 = DataConverter.GetDoubleNull(item["13"]); break;
                                    case "14": d.D_14 = DataConverter.GetDoubleNull(item["14"]); break;
                                    case "15": d.D_15 = DataConverter.GetDoubleNull(item["15"]); break;
                                    case "16": d.D_16 = DataConverter.GetDoubleNull(item["16"]); break;
                                    case "17": d.D_17 = DataConverter.GetDoubleNull(item["17"]); break;
                                    case "18": d.D_18 = DataConverter.GetDoubleNull(item["18"]); break;
                                    case "19": d.D_19 = DataConverter.GetDoubleNull(item["19"]); break;
                                    case "20": d.D_20 = DataConverter.GetDoubleNull(item["20"]); break;
                                    case "21": d.D_21 = DataConverter.GetDoubleNull(item["21"]); break;
                                    case "22": d.D_22 = DataConverter.GetDoubleNull(item["22"]); break;
                                    case "23": d.D_23 = DataConverter.GetDoubleNull(item["23"]); break;
                                    case "24": d.D_24 = DataConverter.GetDoubleNull(item["24"]); break;
                                    case "25": d.D_25 = DataConverter.GetDoubleNull(item["25"]); break;
                                    case "26": d.D_26 = DataConverter.GetDoubleNull(item["26"]); break;
                                    case "27": d.D_27 = DataConverter.GetDoubleNull(item["27"]); break;
                                    case "28": d.D_28 = DataConverter.GetDoubleNull(item["28"]); break;
                                    case "29": d.D_29 = DataConverter.GetDoubleNull(item["29"]); break;
                                    case "30": d.D_30 = DataConverter.GetDoubleNull(item["30"]); break;
                                    case "31": d.D_31 = DataConverter.GetDoubleNull(item["31"]); break;
                                }
                            }

                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SALARY_BY_DAY_AND_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ReviseModel> GetReviseByDayAndCode(DateModel model)
        {
            List<ReviseModel> ccr = new List<ReviseModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REVISE_BY_DAY_AND_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReviseModel
                            {
                                PROJECT_NAME = DataConverter.GetString(item["PROJECT_NAME"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                ACTUAL = DataConverter.GetInteger(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                PRICE = DataConverter.GetDoubleNull(item["PRICE"]),
                                SALARY = DataConverter.GetDoubleNull(item["SALARY"]),                          
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REVISE_BY_DAY_AND_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ReviseModel> GetOtherByDayAndCode(DateModel model)
        {
            List<ReviseModel> ccr = new List<ReviseModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_OTHER_BY_DAY_AND_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DAY", DbType.String, ParameterDirection.Input, model.DAY);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReviseModel
                            {
                                
                               
                                ACTUAL = DataConverter.GetInteger(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                PRICE = DataConverter.GetDoubleNull(item["PRICE"]),
                                SALARY = DataConverter.GetDoubleNull(item["SALARY"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_OTHER_BY_DAY_AND_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<NCRModel> GetReportNCR(DateModel model)
        {
            List<NCRModel> ccr = new List<NCRModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_NCR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            NCRModel d = new NCRModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1": d.WEEK_1 = DataConverter.GetString(item["1"]); break;
                                    case "2": d.WEEK_2 = DataConverter.GetString(item["2"]); break;
                                    case "3": d.WEEK_3 = DataConverter.GetString(item["3"]); break;
                                    case "4": d.WEEK_4 = DataConverter.GetString(item["4"]); break;
                                    case "5": d.WEEK_5 = DataConverter.GetString(item["5"]); break;

                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_NCR", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
      

        public List<NCRModel> GetReportVT(DateModel model)
        {
            List<NCRModel> ccr = new List<NCRModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_VT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            NCRModel d = new NCRModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1": d.WEEK_1 = DataConverter.GetString(item["1"]); break;
                                    case "2": d.WEEK_2 = DataConverter.GetString(item["2"]); break;
                                    case "3": d.WEEK_3 = DataConverter.GetString(item["3"]); break;
                                    case "4": d.WEEK_4 = DataConverter.GetString(item["4"]); break;
                                    case "5": d.WEEK_5 = DataConverter.GetString(item["5"]); break;

                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_VT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<GoalSumModel> GetSummaryWorkDayForWeek(DateModel model)
        {
            List<GoalSumModel> ccr = new List<GoalSumModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_WORK_DAY_FOR_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                           ccr.Add(new GoalSumModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                GOAL = DataConverter.GetString(item["GOAL"]) + '('+DataConverter.GetString(item["GOAL_DAY"]) + ')',
                                PART = DataConverter.GetInteger(item["PART"]),
                                OTHER = DataConverter.GetInteger(item["OTHER"]),
                                REVISE = DataConverter.GetInteger(item["REVISE"]),
                                WELD = DataConverter.GetInteger(item["WELD"]),
                                DIMENSION = DataConverter.GetInteger(item["DIMENSION"]),
                                DEDUCT = DataConverter.GetInteger(item["DEDUCT"]),
                               TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SalarySummaryModel> GetSummarySalaryDayForWeek(DateModel model)
        {
            List<SalarySummaryModel> ccr = new List<SalarySummaryModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_DAY_FOR_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        { 
                          ccr.Add(new SalarySummaryModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')',
                                LEVEL_SALARY = DataConverter.GetDouble(item["LEVEL_SALARY"]),
                              LEVEL_QUALITY = DataConverter.GetDouble(item["LEVEL_QUALITY"]),
                              LEVEL_PROCEDURE = DataConverter.GetDouble(item["LEVEL_PROCEDURE"]),
                              SUM_PART = DataConverter.GetDouble(item["SUM_PART"]),
                              OVER_GOAL = DataConverter.GetInteger(item["OVER_GOAL"]),
                              SALARY = DataConverter.GetDouble(item["SALARY"]),
                              SALARY_QUALITY = DataConverter.GetDouble(item["SALARY_QUALITY"]),
                              SALARY_PROCEDURE = DataConverter.GetDouble(item["SALARY_PROCEDURE"]),
                              OTHER_SALARY = DataConverter.GetDouble(item["OTHER_SALARY"]),
                              REVISE_SALARY = DataConverter.GetDouble(item["REVISE_SALARY"]),
                              WELD_SALARY = DataConverter.GetDouble(item["WELD_SALARY"]),
                              DIMENSION_SALARY = DataConverter.GetDouble(item["DIMENSION_SALARY"]),
                              MAIN = DataConverter.GetDouble(item["MAIN"]),
                              TEMPO = DataConverter.GetDouble(item["TEMPO"]),
                              VT_INSPEC = DataConverter.GetInteger(item["VT_INSPEC"]),
                              VT_QPC = DataConverter.GetInteger(item["VT_QPC"]),
                              VT_OTHER = DataConverter.GetInteger(item["VT_OTHER"]),
                              VT_DESIGN_CHANGE = DataConverter.GetInteger(item["DESIGN_CHANGE"]),
                              MINUS = DataConverter.GetDouble(item["MINUS"]),
                              PLUS = DataConverter.GetDouble(item["PLUS"]),
                              SUM_SALARY = DataConverter.GetDouble(item["SUM_SALARY"]),
                              POSITION = DataConverter.GetDouble(item["POSITION"]),
                              TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                              PART = DataConverter.GetInteger(item["PART"]),
                              OTHER = DataConverter.GetInteger(item["OTHER"]),
                              REVISE = DataConverter.GetInteger(item["REVISE"]),
                              WELD = DataConverter.GetInteger(item["WELD"]),
                              DIMENSION = DataConverter.GetInteger(item["DIMENSION"]),
                              FIX_SALARY = DataConverter.GetDouble(item["FIX_SALARY"]),
                              MINUS_2 = DataConverter.GetDouble(item["MINUS_2"]),
                              POSITION_2 = DataConverter.GetDouble(item["POSITION_2"]),
                              SUM_SALARY_2 = DataConverter.GetDouble(item["SUM_SALARY_2"]),
                          });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_SALARY_DAY_FOR_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<GoalSumModel> GetSummaryWorkDayForWeekLess(DateModel model)
        {
            List<GoalSumModel> ccr = new List<GoalSumModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_LESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new GoalSumModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')',
                                //PART = DataConverter.GetInteger(item["PART"]),
                                //OTHER = DataConverter.GetInteger(item["OTHER"]),
                                //REVISE = DataConverter.GetInteger(item["REVISE"]),
                                //WELD = DataConverter.GetInteger(item["WELD"]),
                                //DIMENSION = DataConverter.GetInteger(item["DIMENSION"]),
                                OVER_GOAL = DataConverter.GetInteger(item["OVER_GOAL"]),
                                SUM_GOAL = DataConverter.GetInteger(item["SUM_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_LESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<GoalSumModel> GetSummaryWorkDayForWeekWeldLess(DateModel model)
        {
            List<GoalSumModel> ccr = new List<GoalSumModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_WELD_LESS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new GoalSumModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]),
                                GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')',
                                //PART = DataConverter.GetInteger(item["PART"]),
                                //OTHER = DataConverter.GetInteger(item["OTHER"]),
                                //REVISE = DataConverter.GetInteger(item["REVISE"]),
                                //WELD = DataConverter.GetInteger(item["WELD"]),
                                //DIMENSION = DataConverter.GetInteger(item["DIMENSION"]),
                                OVER_GOAL = DataConverter.GetInteger(item["OVER_GOAL"]),
                                SUM_GOAL = DataConverter.GetInteger(item["SUM_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_LESS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<PlusModel> GetSummarySalaryMinus(DateModel model)
        {
            List<PlusModel> ccr = new List<PlusModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_MINUS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new PlusModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                FIX_SALARY = DataConverter.GetDouble(item["FIX_SALARY"]),
                                POSITION_SALARY = DataConverter.GetDouble(item["POSITION_SALARY"]),
                                ALL_SALARY = DataConverter.GetDouble(item["ALL_SALARY"]),
                                SUM_SALARY = DataConverter.GetDouble(item["SUM_SALARY"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_SALARY_MINUS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<PlusModel> GetSummarySalaryPlus(DateModel model)
        {
            List<PlusModel> ccr = new List<PlusModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_PLUS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new PlusModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                FIX_SALARY = DataConverter.GetDouble(item["FIX_SALARY"]),
                                POSITION_SALARY = DataConverter.GetDouble(item["POSITION_SALARY"]),
                                ALL_SALARY = DataConverter.GetDouble(item["ALL_SALARY"]),
                                SUM_SALARY = DataConverter.GetDouble(item["SUM_SALARY"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_SALARY_PLUS", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<PlusModel> GetSummarySalaryTotal(DateModel model)
        {
            List<PlusModel> ccr = new List<PlusModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_TOTAL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new PlusModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                FIX_SALARY = DataConverter.GetDouble(item["FIX_SALARY"]),
                                POSITION_SALARY = DataConverter.GetDouble(item["POSITION_SALARY"]),
                                ALL_SALARY = DataConverter.GetDouble(item["ALL_SALARY"]),
                                SUM_SALARY = DataConverter.GetDouble(item["SUM_SALARY"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_SALARY_TOTAL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SummaryWorkModel> GetSummaryWorkDayForWeekWeld(DateModel model)
        {
            List<SummaryWorkModel> ccr = new List<SummaryWorkModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_WELD");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            SummaryWorkModel d = new SummaryWorkModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL": d.GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetInteger(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetInteger(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetInteger(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetInteger(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetInteger(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetInteger(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetInteger(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetInteger(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetInteger(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetInteger(item["DATA_10"]); break;
                                   
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_WELD", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SummarySalaryModel> GetSummarySalaryForWeekWeld(DateModel model)
        {
            List<SummarySalaryModel> ccr = new List<SummarySalaryModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_DAY_FOR_WEEK_WELD");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            SummarySalaryModel d = new SummarySalaryModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "CERT": d.CERT = DataConverter.GetString(item["CERT"]); break;
                                    case "WORK_TYPE": d.WORK_TYPE = DataConverter.GetString(item["WORK_TYPE"]); break;
                                    case "GOAL": d.GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetString(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetString(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetString(item["DATA_40"]); break;
                                    case "DATA_41": d.DATA_41 = DataConverter.GetDouble(item["DATA_41"]); break;
                                    case "DATA_42": d.DATA_42 = DataConverter.GetDouble(item["DATA_42"]); break;
                                    case "DATA_43": d.DATA_43 = DataConverter.GetDouble(item["DATA_43"]); break;
                                    case "DATA_44": d.DATA_44 = DataConverter.GetDouble(item["DATA_44"]); break;
                                    case "DATA_45": d.DATA_45 = DataConverter.GetDouble(item["DATA_45"]); break;
                                    case "DATA_46": d.DATA_46 = DataConverter.GetDouble(item["DATA_46"]); break;
                                    case "DATA_47": d.DATA_47 = DataConverter.GetDouble(item["DATA_47"]); break;
                                    case "DATA_48": d.DATA_48 = DataConverter.GetDouble(item["DATA_48"]); break;
                                    case "DATA_49": d.DATA_49 = DataConverter.GetDouble(item["DATA_49"]); break;
                                    case "DATA_50": d.DATA_50 = DataConverter.GetDouble(item["DATA_50"]); break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_WELD", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SummarySalaryModel> GetSummarySalaryForWeekWeldSum(DateModel model)
        {
            List<SummarySalaryModel> ccr = new List<SummarySalaryModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_DAY_FOR_WEEK_WELD_SUM");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            SummarySalaryModel d = new SummarySalaryModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL": d.GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetString(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetString(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetString(item["DATA_40"]); break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_WELD", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<SummarySalaryModel> GetSummarySalaryForWeekSum(DateModel model)
        {
            List<SummarySalaryModel> ccr = new List<SummarySalaryModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_SALARY_DAY_FOR_WEEK_SUM");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            SummarySalaryModel d = new SummarySalaryModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "ROW_NUM": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "EMP_CODE": d.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]); break;
                                    case "FULL_NAME": d.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]); break;
                                    case "EMP_GOAL": d.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]); break;
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "GOAL": d.GOAL = DataConverter.GetString(item["GOAL"]) + '(' + DataConverter.GetString(item["GOAL_DAY"]) + ')'; break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "DATA_1": d.DATA_1 = DataConverter.GetDouble(item["DATA_1"]); break;
                                    case "DATA_2": d.DATA_2 = DataConverter.GetDouble(item["DATA_2"]); break;
                                    case "DATA_3": d.DATA_3 = DataConverter.GetDouble(item["DATA_3"]); break;
                                    case "DATA_4": d.DATA_4 = DataConverter.GetDouble(item["DATA_4"]); break;
                                    case "DATA_5": d.DATA_5 = DataConverter.GetDouble(item["DATA_5"]); break;
                                    case "DATA_6": d.DATA_6 = DataConverter.GetDouble(item["DATA_6"]); break;
                                    case "DATA_7": d.DATA_7 = DataConverter.GetDouble(item["DATA_7"]); break;
                                    case "DATA_8": d.DATA_8 = DataConverter.GetDouble(item["DATA_8"]); break;
                                    case "DATA_9": d.DATA_9 = DataConverter.GetDouble(item["DATA_9"]); break;
                                    case "DATA_10": d.DATA_10 = DataConverter.GetDouble(item["DATA_10"]); break;
                                    case "DATA_11": d.DATA_11 = DataConverter.GetDouble(item["DATA_11"]); break;
                                    case "DATA_12": d.DATA_12 = DataConverter.GetDouble(item["DATA_12"]); break;
                                    case "DATA_13": d.DATA_13 = DataConverter.GetDouble(item["DATA_13"]); break;
                                    case "DATA_14": d.DATA_14 = DataConverter.GetDouble(item["DATA_14"]); break;
                                    case "DATA_15": d.DATA_15 = DataConverter.GetDouble(item["DATA_15"]); break;
                                    case "DATA_16": d.DATA_16 = DataConverter.GetDouble(item["DATA_16"]); break;
                                    case "DATA_17": d.DATA_17 = DataConverter.GetDouble(item["DATA_17"]); break;
                                    case "DATA_18": d.DATA_18 = DataConverter.GetDouble(item["DATA_18"]); break;
                                    case "DATA_19": d.DATA_19 = DataConverter.GetDouble(item["DATA_19"]); break;
                                    case "DATA_20": d.DATA_20 = DataConverter.GetDouble(item["DATA_20"]); break;
                                    case "DATA_21": d.DATA_21 = DataConverter.GetDouble(item["DATA_21"]); break;
                                    case "DATA_22": d.DATA_22 = DataConverter.GetDouble(item["DATA_22"]); break;
                                    case "DATA_23": d.DATA_23 = DataConverter.GetDouble(item["DATA_23"]); break;
                                    case "DATA_24": d.DATA_24 = DataConverter.GetDouble(item["DATA_24"]); break;
                                    case "DATA_25": d.DATA_25 = DataConverter.GetDouble(item["DATA_25"]); break;
                                    case "DATA_26": d.DATA_26 = DataConverter.GetDouble(item["DATA_26"]); break;
                                    case "DATA_27": d.DATA_27 = DataConverter.GetDouble(item["DATA_27"]); break;
                                    case "DATA_28": d.DATA_28 = DataConverter.GetDouble(item["DATA_28"]); break;
                                    case "DATA_29": d.DATA_29 = DataConverter.GetDouble(item["DATA_29"]); break;
                                    case "DATA_30": d.DATA_30 = DataConverter.GetDouble(item["DATA_30"]); break;
                                    case "DATA_31": d.DATA_31 = DataConverter.GetDouble(item["DATA_31"]); break;
                                    case "DATA_32": d.DATA_32 = DataConverter.GetDouble(item["DATA_32"]); break;
                                    case "DATA_33": d.DATA_33 = DataConverter.GetDouble(item["DATA_33"]); break;
                                    case "DATA_34": d.DATA_34 = DataConverter.GetDouble(item["DATA_34"]); break;
                                    case "DATA_35": d.DATA_35 = DataConverter.GetDouble(item["DATA_35"]); break;
                                    case "DATA_36": d.DATA_36 = DataConverter.GetDouble(item["DATA_36"]); break;
                                    case "DATA_37": d.DATA_37 = DataConverter.GetDouble(item["DATA_37"]); break;
                                    case "DATA_38": d.DATA_38 = DataConverter.GetString(item["DATA_38"]); break;
                                    case "DATA_39": d.DATA_39 = DataConverter.GetString(item["DATA_39"]); break;
                                    case "DATA_40": d.DATA_40 = DataConverter.GetString(item["DATA_40"]); break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_WELD", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
    }
}
