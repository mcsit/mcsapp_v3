﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class AdminData : IAdminData
    {
        public ResultModel SetTxnAdjustGoalData(AdjustGoalModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_ADJUST_GOAL_NEW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@SUM_PART", DbType.Int32, ParameterDirection.Input, model.SUM_PART);
                    command.SetParameter("@CURRENT_GOAL", DbType.Int32, ParameterDirection.Input, model.CURRENT_GOAL);
                    command.SetParameter("@ADJUST_GOAL", DbType.Int32, ParameterDirection.Input, model.ADJUST_GOAL);
                    command.SetParameter("@NEW_GOAL", DbType.Int32, ParameterDirection.Input, model.NEW_GOAL);                   
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    command.SetParameter("@wEEK_NO", DbType.Int32, ParameterDirection.Input, model.WEEK_NO);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError( this.GetType().FullName, "SP_SET_ADJUST_GOAL_NEW", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetTxnNcrData(NcrDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_NCR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@MAIN", DbType.Double, ParameterDirection.Input, model.MAIN);
                    command.SetParameter("@TEMPO", DbType.Double, ParameterDirection.Input, model.TEMPO);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_NCR", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetNCRDataLog(NcrDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_NCR_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@MAIN", DbType.Double, ParameterDirection.Input, model.MAIN);
                    command.SetParameter("@TEMPO", DbType.Double, ParameterDirection.Input, model.TEMPO);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_NCR_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelNCRDetailLog(NcrDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_NCR_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            { 
                new LogData().WriteLogError( this.GetType().FullName, "SP_DEL_NCR_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetTxnVTData(VTDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_VT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@VT", DbType.Double, ParameterDirection.Input, model.VT);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError( this.GetType().FullName, "SP_SET_VT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetTxnDimensionData(DimDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_TXN_DIMENSION_DATA");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@PRICE", DbType.Int32, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@ACTUAL", DbType.Int32, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                  

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError( this.GetType().FullName, "SP_SET_TXN_DIMENSION_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetTxnOtherData(OtherDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {

                    command.SetStoreProcedure("SP_SET_TXN_OTHER_DATA");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@PRICE", DbType.Int32, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@ACTUAL", DbType.Int32, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@PROCESS", DbType.Int32, ParameterDirection.Input, model.PROCESS);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError( this.GetType().FullName, "SP_SET_TXN_DIMENSION_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetTxnWeekData(WeekDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_YEAR", DbType.String, ParameterDirection.Input, model.WEEK_YEAR);
                    command.SetParameter("@WEEK_MONTH", DbType.Int32, ParameterDirection.Input, model.WEEK_MONTH);
                    command.SetParameter("@WEEK_DAY", DbType.String, ParameterDirection.Input, model.WEEK_DAY);
                    command.SetParameter("@WEEK_SUB", DbType.Double, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@WEEK_NO", DbType.Double, ParameterDirection.Input, model.WEEK_NO);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError( this.GetType().FullName, "SP_SET_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetVTDetail(VTListModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_VT_DETAIL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@TYPE_VT", DbType.String, ParameterDirection.Input, model.TYPE_VT);
                    command.SetParameter("@STATUS", DbType.String, ParameterDirection.Input, model.STATUS);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@REV_NO", DbType.String, ParameterDirection.Input, model.REV_NO);
                    command.SetParameter("@CHECK_NO", DbType.Int32, ParameterDirection.Input, model.CHECK_NO);
                    command.SetParameter("@PART", DbType.Int32, ParameterDirection.Input, model.PART);
                    command.SetParameter("@POINT", DbType.String, ParameterDirection.Input, model.POINT);
                    command.SetParameter("@PROBLEM", DbType.String, ParameterDirection.Input, model.PROBLEM);
                    command.SetParameter("@DWG", DbType.String, ParameterDirection.Input, model.DWG);
                    command.SetParameter("@ACT", DbType.String, ParameterDirection.Input, model.ACT);
                    command.SetParameter("@WRONG", DbType.String, ParameterDirection.Input, model.WRONG);
                    command.SetParameter("@EDIT", DbType.String, ParameterDirection.Input, model.EDIT);
                    command.SetParameter("@CHECKER", DbType.String, ParameterDirection.Input, model.CHECKER);
                    command.SetParameter("@CHECK_DATE", DbType.String, ParameterDirection.Input, model.CHECK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError( this.GetType().FullName, "SP_SET_VT_DETAIL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetVTDetailLog(VTListModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_VT_DETAIL_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@TYPE_VT", DbType.String, ParameterDirection.Input, model.TYPE_VT);
                    command.SetParameter("@STATUS", DbType.String, ParameterDirection.Input, model.STATUS);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@REV_NO", DbType.String, ParameterDirection.Input, model.REV_NO);
                    command.SetParameter("@CHECK_NO", DbType.Int32, ParameterDirection.Input, model.CHECK_NO);
                    command.SetParameter("@PART", DbType.Int32, ParameterDirection.Input, model.PART);
                    command.SetParameter("@POINT", DbType.String, ParameterDirection.Input, model.POINT);
                    command.SetParameter("@PROBLEM", DbType.String, ParameterDirection.Input, model.PROBLEM);
                    command.SetParameter("@DWG", DbType.String, ParameterDirection.Input, model.DWG);
                    command.SetParameter("@ACT", DbType.String, ParameterDirection.Input, model.ACT);
                    command.SetParameter("@WRONG", DbType.String, ParameterDirection.Input, model.WRONG);
                    command.SetParameter("@EDIT", DbType.String, ParameterDirection.Input, model.EDIT);
                    command.SetParameter("@CHECKER", DbType.String, ParameterDirection.Input, model.CHECKER);
                    command.SetParameter("@CHECK_DATE", DbType.String, ParameterDirection.Input, model.CHECK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_VT_DETAIL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetVTDetailUpload(VTListModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_VT_DETAIL_UPLOAD");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@TYPE_VT", DbType.String, ParameterDirection.Input, model.TYPE_VT);
                    command.SetParameter("@STATUS", DbType.String, ParameterDirection.Input, model.STATUS);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@REV_NO", DbType.String, ParameterDirection.Input, model.REV_NO);
                    command.SetParameter("@CHECK_NO", DbType.Int32, ParameterDirection.Input, model.CHECK_NO);
                    command.SetParameter("@PART", DbType.Int32, ParameterDirection.Input, model.PART);
                    command.SetParameter("@POINT", DbType.String, ParameterDirection.Input, model.POINT);
                    command.SetParameter("@PROBLEM", DbType.String, ParameterDirection.Input, model.PROBLEM);
                    command.SetParameter("@DWG", DbType.String, ParameterDirection.Input, model.DWG);
                    command.SetParameter("@ACT", DbType.String, ParameterDirection.Input, model.ACT);
                    command.SetParameter("@WRONG", DbType.String, ParameterDirection.Input, model.WRONG);
                    command.SetParameter("@EDIT", DbType.String, ParameterDirection.Input, model.EDIT);
                    command.SetParameter("@CHECKER", DbType.String, ParameterDirection.Input, model.CHECKER);
                    command.SetParameter("@CHECK_DATE", DbType.String, ParameterDirection.Input, model.CHECK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_VT_DETAIL_UPLOAD", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelVTDetailLog(VTListModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_VT_DETAIL_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@TYPE_VT", DbType.String, ParameterDirection.Input, model.TYPE_VT);
                    command.SetParameter("@STATUS", DbType.String, ParameterDirection.Input, model.STATUS);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@REV_NO", DbType.String, ParameterDirection.Input, model.REV_NO);
                    command.SetParameter("@CHECK_NO", DbType.Int32, ParameterDirection.Input, model.CHECK_NO);
                    command.SetParameter("@PART", DbType.Int32, ParameterDirection.Input, model.PART);
                    command.SetParameter("@POINT", DbType.String, ParameterDirection.Input, model.POINT);
                    command.SetParameter("@PROBLEM", DbType.String, ParameterDirection.Input, model.PROBLEM);
                    command.SetParameter("@DWG", DbType.String, ParameterDirection.Input, model.DWG);
                    command.SetParameter("@ACT", DbType.String, ParameterDirection.Input, model.ACT);
                    command.SetParameter("@WRONG", DbType.String, ParameterDirection.Input, model.WRONG);
                    command.SetParameter("@EDIT", DbType.String, ParameterDirection.Input, model.EDIT);
                    command.SetParameter("@CHECKER", DbType.String, ParameterDirection.Input, model.CHECKER);
                    command.SetParameter("@CHECK_DATE", DbType.String, ParameterDirection.Input, model.CHECK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_VT_DETAIL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetDesingLog(DesingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_DESING_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@NUMBER_DESING", DbType.String, ParameterDirection.Input, model.NUMBER_DESING);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@DATE", DbType.String, ParameterDirection.Input, model.DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_DESING_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetDesing(DesingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_DESING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@NUMBER_DESING", DbType.String, ParameterDirection.Input, model.NUMBER_DESING);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@DATE", DbType.String, ParameterDirection.Input, model.DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_DESING", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelDesingLog(DesingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_DESING_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@NUMBER_DESING", DbType.String, ParameterDirection.Input, model.NUMBER_DESING);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@ITEM", DbType.Int32, ParameterDirection.Input, model.ITEM);
                    command.SetParameter("@DATE", DbType.String, ParameterDirection.Input, model.DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_DESING_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<DesingModel> GetDesingLog(DateModel model)
        {
            List<DesingModel> ccr = new List<DesingModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_DESING_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new DesingModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                NUMBER_DESING = DataConverter.GetString(item["NUMBER_DESING"]),
                                DATE = DataConverter.GetString(item["DATE"]),                            

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DESING_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<VTListModel> GetVTDetailLog(DateModel model)
        {
            List<VTListModel> ccr = new List<VTListModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_DETAIL_VT_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE_VT", DbType.String, ParameterDirection.Input, model.TYPE_STORE);
                    command.SetParameter("@WEEK", DbType.Int16, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new VTListModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                REV_NO = DataConverter.GetString(item["REV_NO"]),
                                CHECK_NO = DataConverter.GetString(item["CHECK_NO"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                POINT = DataConverter.GetString(item["POINT"]),
                                PROBLEM = DataConverter.GetString(item["PROBLEM"]),
                                DWG = DataConverter.GetString(item["DWG"]),
                                ACT = DataConverter.GetString(item["ACT"]),
                                WRONG = DataConverter.GetString(item["WRONG"]),
                                EDIT = DataConverter.GetString(item["EDIT"]),
                                CHECKER = DataConverter.GetString(item["CHECKER"]),
                                CHECK_DATE = DataConverter.GetString(item["CHECK_DATE"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DETAIL_VT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<NcrDataModel> GetNCRLog(DateModel model)
        {
            List<NcrDataModel> ccr = new List<NcrDataModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_NCR_LOG");
                    command.Command.Parameters.Clear();
                
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NcrDataModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                MAIN = DataConverter.GetDouble(item["MAIN"]),
                                TEMPO = DataConverter.GetDouble(item["TEMPO"]),

                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_NCR_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<VTListModel> GetVTDetailAll(DateModel model)
        {
            List<VTListModel> ccr = new List<VTListModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_DETAIL_VT_ALL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@TYPE_VT", DbType.String, ParameterDirection.Input, model.TYPE_STORE);
                    command.SetParameter("@TYPE_INSERT", DbType.Int16, ParameterDirection.Input, model.TYPE_INSERT);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new VTListModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                REV_NO = DataConverter.GetString(item["REV_NO"]),
                                CHECK_NO = DataConverter.GetString(item["CHECK_NO"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                POINT = DataConverter.GetString(item["POINT"]),
                                PROBLEM = DataConverter.GetString(item["PROBLEM"]),
                                DWG = DataConverter.GetString(item["DWG"]),
                                ACT = DataConverter.GetString(item["ACT"]),
                                WRONG = DataConverter.GetString(item["WRONG"]),
                                EDIT = DataConverter.GetString(item["EDIT"]),
                                CHECKER = DataConverter.GetString(item["CHECKER"]),
                                CHECK_DATE = DataConverter.GetString(item["CHECK_DATE"]),
                                TYPE_INSERT = DataConverter.GetString(item["TYPE_INSERT"]),
                                TYPE_VT  = DataConverter.GetString(item["TYPE_VT"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DETAIL_VT_ALL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<NCRDetailModel> GetReportNCRDetail(DateModel model)
        {
            List<NCRDetailModel> ccr = new List<NCRDetailModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_NCR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            NCRDetailModel d = new NCRDetailModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1":
                                        var d1 = DataConverter.GetString(item["1"]).Split('|');
                                        d.MAIN_1 = DataConverter.GetString(d1[0].TrimStart().TrimEnd());
                                        d.TEMPO_1 = DataConverter.GetString(d1[1].TrimStart().TrimEnd());
                                        break;
                                    case "2":
                                        var d2 = DataConverter.GetString(item["2"]).Split('|');
                                        d.MAIN_2 = DataConverter.GetString(d2[0].TrimStart().TrimEnd());
                                        d.TEMPO_2 = DataConverter.GetString(d2[1].TrimStart().TrimEnd());
                                        break;
                                    case "3":
                                        var d3 = DataConverter.GetString(item["3"]).Split('|');
                                        d.MAIN_3 = DataConverter.GetString(d3[0].TrimStart().TrimEnd());
                                        d.TEMPO_3 = DataConverter.GetString(d3[1].TrimStart().TrimEnd());
                                        break;
                                    case "4":
                                        var d4 = DataConverter.GetString(item["4"]).Split('|');
                                        d.MAIN_4 = DataConverter.GetString(d4[0].TrimStart().TrimEnd());
                                        d.TEMPO_4 = DataConverter.GetString(d4[1].TrimStart().TrimEnd());
                                        break;
                                    case "5":
                                        var d5 = DataConverter.GetString(item["5"]).Split('|');
                                        d.MAIN_5 = DataConverter.GetString(d5[0].TrimStart().TrimEnd());
                                        d.TEMPO_5 = DataConverter.GetString(d5[1].TrimStart().TrimEnd());
                                        break;
                                    case "6":
                                        var d6 = DataConverter.GetString(item["6"]).Split('|');
                                        d.MAIN_6 = DataConverter.GetString(d6[0].TrimStart().TrimEnd());
                                        d.TEMPO_6 = DataConverter.GetString(d6[1].TrimStart().TrimEnd());
                                        break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_NCR", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<NGDetailModel> GetReportNGDetail(DateModel model)
        {
            List<NGDetailModel> ccr = new List<NGDetailModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            NGDetailModel d = new NGDetailModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1":
                                        var d1 = DataConverter.GetString(item["1"]).Split('|');
                                        d.NG_1 = DataConverter.GetString(d1[0].TrimStart().TrimEnd());
                                        break;
                                    case "2":
                                        var d2 = DataConverter.GetString(item["2"]).Split('|');
                                        d.NG_2 = DataConverter.GetString(d2[0].TrimStart().TrimEnd());
                                        break;
                                    case "3":
                                        var d3 = DataConverter.GetString(item["3"]).Split('|');
                                        d.NG_3 = DataConverter.GetString(d3[0].TrimStart().TrimEnd());
                                        break;
                                    case "4":
                                        var d4 = DataConverter.GetString(item["4"]).Split('|');
                                        d.NG_4 = DataConverter.GetString(d4[0].TrimStart().TrimEnd());
                          
                                        break;
                                    case "5":
                                        var d5 = DataConverter.GetString(item["5"]).Split('|');
                                        d.NG_5 = DataConverter.GetString(d5[0].TrimStart().TrimEnd());
 
                                        break;
                                    case "6":
                                        var d6 = DataConverter.GetString(item["6"]).Split('|');
                                        d.NG_6 = DataConverter.GetString(d6[0].TrimStart().TrimEnd());
                                        break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_NG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<VTDetailModel> GetReportVTDetail(DateModel model)
        {
            List<VTDetailModel> ccr = new List<VTDetailModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_VT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            VTDetailModel d = new VTDetailModel();
                            foreach (DataColumn column in result.Tables[0].Columns)
                            {

                                switch (column.ColumnName)
                                {
                                    case "DEPT_NAME": d.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]); break;
                                    case "TOTAL_COUNT": d.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]); break;
                                    case "1":
                                        var d1 = DataConverter.GetString(item["1"]).Split('|');
                                        d.INSPEC_1 = DataConverter.GetString(d1[0].TrimStart().TrimEnd());
                                        d.QPC_1 = DataConverter.GetString(d1[1].TrimStart().TrimEnd());
                                        d.OTHER_1 = DataConverter.GetString(d1[2].TrimStart().TrimEnd());
                                        d.DESIGN_1 = DataConverter.GetString(d1[3].TrimStart().TrimEnd());
                                        break;
                                    case "2":
                                        var d2 = DataConverter.GetString(item["2"]).Split('|');
                                        d.INSPEC_2 = DataConverter.GetString(d2[0].TrimStart().TrimEnd());
                                        d.QPC_2= DataConverter.GetString(d2[1].TrimStart().TrimEnd());
                                        d.OTHER_2 = DataConverter.GetString(d2[2].TrimStart().TrimEnd());
                                        d.DESIGN_2 = DataConverter.GetString(d2[3].TrimStart().TrimEnd());
                                        break;
                                    case "3":
                                        var d3 = DataConverter.GetString(item["3"]).Split('|');
                                        d.INSPEC_3 = DataConverter.GetString(d3[0].TrimStart().TrimEnd());
                                        d.QPC_3 = DataConverter.GetString(d3[1].TrimStart().TrimEnd());
                                        d.OTHER_3 = DataConverter.GetString(d3[2].TrimStart().TrimEnd());
                                        d.DESIGN_3 = DataConverter.GetString(d3[3].TrimStart().TrimEnd());
                                        break;
                                    case "4":
                                        var d4 = DataConverter.GetString(item["4"]).Split('|');
                                        d.INSPEC_4 = DataConverter.GetString(d4[0].TrimStart().TrimEnd());
                                        d.QPC_4 = DataConverter.GetString(d4[1].TrimStart().TrimEnd());
                                        d.OTHER_4 = DataConverter.GetString(d4[2].TrimStart().TrimEnd());
                                        d.DESIGN_4 = DataConverter.GetString(d4[3].TrimStart().TrimEnd());
                                        break;
                                    case "5":
                                        var d5 = DataConverter.GetString(item["5"]).Split('|');
                                        d.INSPEC_5 = DataConverter.GetString(d5[0].TrimStart().TrimEnd());
                                        d.QPC_5 = DataConverter.GetString(d5[1].TrimStart().TrimEnd());
                                        d.OTHER_5 = DataConverter.GetString(d5[2].TrimStart().TrimEnd());
                                        d.DESIGN_5 = DataConverter.GetString(d5[3].TrimStart().TrimEnd());
                                        break;
                                    case "6":
                                        var d6 = DataConverter.GetString(item["6"]).Split('|');
                                        d.INSPEC_6 = DataConverter.GetString(d6[0].TrimStart().TrimEnd());
                                        d.QPC_6 = DataConverter.GetString(d6[1].TrimStart().TrimEnd());
                                        d.OTHER_6 = DataConverter.GetString(d6[2].TrimStart().TrimEnd());
                                        d.DESIGN_6 = DataConverter.GetString(d6[3].TrimStart().TrimEnd());
                                        break;
                                }
                            }
                            ccr.Add(d);
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_VT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public GoalSumModel GetWeekLessForAdjust(DateModel model)
        {
            GoalSumModel rs = new GoalSumModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_LESS_BY_CODE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH.PadLeft(2, '0'));
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs.EMP_CODE = DataConverter.GetString(item["EMP_CODE"]);
                        rs.FULL_NAME = DataConverter.GetString(item["FULL_NAME"]);
                        rs.DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]);
                        rs.EMP_GOAL = DataConverter.GetInteger(item["EMP_GOAL"]);
                        rs.GOAL = DataConverter.GetString(item["GOAL"]);
                        rs.GOAL_DAY = DataConverter.GetInteger(item["GOAL_DAY"]);
                        rs.PART = DataConverter.GetInteger(item["SUM_PART"]);
                        //rs.REVISE = DataConverter.GetInteger(item["REVISE"]);
                        rs.ADJUST_GOAL = DataConverter.GetString(item["ADJUST_GOAL"]);
                        rs.NEW_GOAL = DataConverter.GetString(item["NEW_GOAL"]);

                        rs.TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]);

                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_SUMMARY_WORK_DAY_FOR_WEEK_LESS_BY_CODE", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<GoalSumModel> GetAdjustGoalData(DateModel model)
        {
            List<GoalSumModel> ccr = new List<GoalSumModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_ADJUST_GOAL_DATA_NEW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    command.SetParameter("@WEEK_NO", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new GoalSumModel
                            {
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                SUM_PART = DataConverter.GetInteger(item["SUM_PART"]),
                                CURRENT_GOAL = DataConverter.GetString(item["CURRENT_GOAL"]),
                                ADJUST_GOAL = DataConverter.GetString(item["ADJUST_GOAL"]),
                                NEW_GOAL = DataConverter.GetString(item["NEW_GOAL"]),
                                ID = DataConverter.GetInteger(item["ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                WEEK_NO = DataConverter.GetInteger(item["WEEK_NO"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_ADJUST_GOAL_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<MonthModel> GetCalendar(DateModel model)
        {
            List<MonthModel> ccr = new List<MonthModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_CALENDAR");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@MONTH", DbType.String, ParameterDirection.Input, model.MONTH);
                    command.SetParameter("@YEAR", DbType.String, ParameterDirection.Input, model.YEAR);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new MonthModel
                            {
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                MON = DataConverter.GetString(item["Mon"]),
                                TUE = DataConverter.GetString(item["Tue"]),
                                WED = DataConverter.GetString(item["Wed"]),
                                THU = DataConverter.GetString(item["Thu"]),
                                FRI = DataConverter.GetString(item["Fri"]),
                                SAT = DataConverter.GetString(item["Sat"]),
                                SUN = DataConverter.GetString(item["Sun"]),
                               
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_CALENDAR", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel SetTxnMinusData(MinusDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {

                    command.SetStoreProcedure("SP_SET_TXN_MINUS_DATA");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@PRICE", DbType.Int32, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@REMARK", DbType.Int32, ParameterDirection.Input, model.REMARK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_TXN_MINUS_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        //public List<MinusDataModel> GetReportMinusData(ReportSearchModel model)
        //{
        //    List<MinusDataModel> ccr = new List<MinusDataModel>();

        //    try
        //    {
        //        using (CommandData command = new CommandData())
        //        {
        //            command.SetStoreProcedure("SP_GET_REPORT_MINUS_DATA");
        //            command.Command.Parameters.Clear();
        //            command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
        //            command.SetParameter("@FULL_NAME", DbType.String, ParameterDirection.Input, model.FULL_NAME);
        //            var result = command.ExecuteDataSet();
        //            if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
        //            if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
        //                foreach (DataRow item in result.Tables[0].Rows)
        //                {
        //                    ccr.Add(new MinusDataModel
        //                    {
        //                        EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
        //                        FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
        //                        DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
        //                        PRICE = DataConverter.GetDouble(item["PRICE"]),
        //                        ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
        //                        REMARK = DataConverter.GetString(item["REMARK"]),
        //                        TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
        //                    });
        //                }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_MINUS_DATA", e.Message);
        //        throw new Exception(e.Message);
        //    }
        //    return ccr;
        //}

        //public List<OtherDataModel> GetReportOtherData(ReportSearchModel model)
        //{
        //    List<OtherDataModel> ccr = new List<OtherDataModel>();

        //    try
        //    {
        //        using (CommandData command = new CommandData())
        //        {
        //            command.SetStoreProcedure("SP_GET_REPORT_OTHER_DATA");
        //            command.Command.Parameters.Clear();
        //            command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
        //            command.SetParameter("@FULL_NAME", DbType.String, ParameterDirection.Input, model.FULL_NAME);
        //            command.SetParameter("@PROCESS_ID", DbType.Int32, ParameterDirection.Input, model.PROCESS_ID);
        //            var result = command.ExecuteDataSet();
        //            if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
        //            if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
        //                foreach (DataRow item in result.Tables[0].Rows)
        //                {
        //                    ccr.Add(new OtherDataModel
        //                    {
        //                        EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
        //                        FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
        //                        DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
        //                        PRICE = DataConverter.GetDouble(item["PRICE"]),
        //                        ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
        //                        ACTUAL = DataConverter.GetInteger(item["ACTUAL"]),
        //                        PROCESS_NAME = DataConverter.GetString(item["PROCESS_NAME"]),
        //                        TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
        //                    });
        //                }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_OTHER_DATA", e.Message);
        //        throw new Exception(e.Message);
        //    }
        //    return ccr;
        //}

        public ResultModel SetUploadOtherLog(OtherModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_OTHER_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@TYPE_LOAD", DbType.String, ParameterDirection.Input, model.TYPE_LOAD);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.PROCESS);
                    command.SetParameter("@PRICE", DbType.String, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@ACTUAL", DbType.Int32, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_OTHER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadOther(OtherModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_OTHER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@TYPE_LOAD", DbType.String, ParameterDirection.Input, model.TYPE_LOAD);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.PROCESS);
                    command.SetParameter("@PRICE", DbType.String, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@ACTUAL", DbType.Int32, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_OTHER", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadOtheLog(OtherModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_OTHER_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@PROCESS", DbType.String, ParameterDirection.Input, model.PROCESS);
                    command.SetParameter("@PRICE", DbType.String, ParameterDirection.Input, model.PRICE);
                    command.SetParameter("@ACTUAL", DbType.Int32, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_OTHER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<OtherModel> GetUploadOtheLog(DateModel model)
        {
            List<OtherModel> ccr = new List<OtherModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_OTHER_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE_STORE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new OtherModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                PRICE = DataConverter.GetDouble(item["PRICE"]),
                                ACTUAL = DataConverter.GetInteger(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                TYPE_LOAD = DataConverter.GetString(item["TYPE_LOAD"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_OTHER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel SetUploadWeekLog(UploadWeekModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_WEEK_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_NO", DbType.String, ParameterDirection.Input, model.WEEK_NO);
                    command.SetParameter("@WEEK_DATE", DbType.String, ParameterDirection.Input, model.WEEK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_WEEK_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadWeek(UploadWeekModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_NO", DbType.String, ParameterDirection.Input, model.WEEK_NO);
                    command.SetParameter("@WEEK_DATE", DbType.String, ParameterDirection.Input, model.WEEK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadWeekLog(UploadWeekModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_WEEK_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_NO", DbType.String, ParameterDirection.Input, model.WEEK_NO);
                    command.SetParameter("@WEEK_DATE", DbType.String, ParameterDirection.Input, model.WEEK_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "GetDDLAdmin", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadWeekModel> GetUploadWeekLog(DateModel model)
        {
            List<UploadWeekModel> ccr = new List<UploadWeekModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_WEEK_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadWeekModel
                            {
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                WEEK_NO = DataConverter.GetString(item["WEEK_NO"]),
                                WEEK_DATE = DataConverter.GetString(item["WEEK_DATE"]),
                             
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_WEEK_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }


        // Delete
        public ResultModel DeleteAdjustGoal(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_ADJUST_GOAL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_ADJUST_GOAL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteDesingChange(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_DESING_CHANGE");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_DESING_CHANGE", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteDimension(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_DIMENSION");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_DIMENSION", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteMinus(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_MINUS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_MINUS", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteOther(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_OTHER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_OTHER", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeletePlus(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_PLUS");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_PLUS", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteVTData(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_VT_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_VT_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DeleteWeek(DeleteModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_WEEK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_YEAR", DbType.String, ParameterDirection.Input, model.WEEK_YEAR);
                    command.SetParameter("@WEEK_MONTH", DbType.Int32, ParameterDirection.Input, model.WEEK_MONTH);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_WEEK", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        // Show Edit

        public List<ReportDesignChangeModel> GetReportDesingChangeData(ReportSearchModel model)
        {
            List<ReportDesignChangeModel> ccr = new List<ReportDesignChangeModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_DESING_CHANGE_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReportDesignChangeModel
                            {
                                ID = DataConverter.GetInteger(item["ID"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                DWG = DataConverter.GetString(item["DWG"]),
                                WRONG = DataConverter.GetString(item["WRONG"]),
                                CHECK_DATE = DataConverter.GetString(item["CHECK_DATE"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_DESING_CHANGE_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ReportDimensionModel> GetReportDimensionData(ReportSearchModel model)
        {
            List<ReportDimensionModel> ccr = new List<ReportDimensionModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_DIMENSION_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReportDimensionModel
                            {
                                ID = DataConverter.GetInteger(item["ID"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                PRICE = DataConverter.GetDouble(item["PRICE"]),
                                ACTUAL = DataConverter.GetDouble(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_DIMENSION_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ReportMinusModel> GetReportMinusData(ReportSearchModel model)
        {
            List<ReportMinusModel> ccr = new List<ReportMinusModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_MINUS_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReportMinusModel
                            {
                                ID = DataConverter.GetInteger(item["ID"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),                     
                                PRICE = DataConverter.GetDouble(item["PRICE"]),                         
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                REMARK = DataConverter.GetString(item["REMARK"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_DIMENSION_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ReportDimensionModel> GetReportOtherData(ReportSearchModel model)
        {
            List<ReportDimensionModel> ccr = new List<ReportDimensionModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_OTHER_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReportDimensionModel
                            {
                                ID = DataConverter.GetInteger(item["ID"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                PRICE = DataConverter.GetDouble(item["PRICE"]),
                                ACTUAL = DataConverter.GetDouble(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_DIMENSION_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<ReportDimensionModel> GetReportPlusData(ReportSearchModel model)
        {
            List<ReportDimensionModel> ccr = new List<ReportDimensionModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_PLUS_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new ReportDimensionModel
                            {
                                ID = DataConverter.GetInteger(item["ID"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                PROCESS = DataConverter.GetString(item["PROCESS"]),
                                PRICE = DataConverter.GetDouble(item["PRICE"]),
                                ACTUAL = DataConverter.GetDouble(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_DIMENSION_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<VTListModel> GetReportVTData(ReportSearchModel model)
        {
            List<VTListModel> ccr = new List<VTListModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_VT_DATA");
                    command.Command.Parameters.Clear();
                   
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new VTListModel
                            {
                                ID = DataConverter.GetInteger(item["ID"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                ITEM = DataConverter.GetInteger(item["ITEM"]),
                                REV_NO = DataConverter.GetString(item["REV_NO"]),
                                CHECK_NO = DataConverter.GetString(item["CHECK_NO"]),
                                PART = DataConverter.GetInteger(item["PART"]),
                                POINT = DataConverter.GetString(item["POINT"]),
                                PROBLEM = DataConverter.GetString(item["PROBLEM"]),
                                DWG = DataConverter.GetString(item["DWG"]),
                                ACT = DataConverter.GetString(item["ACT"]),
                                WRONG = DataConverter.GetString(item["WRONG"]),
                                EDIT = DataConverter.GetString(item["EDIT"]),
                                CHECKER = DataConverter.GetString(item["CHECKER"]),
                                CHECK_DATE = DataConverter.GetString(item["CHECK_DATE"]),
                             
                               
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_DETAIL_VT_ALL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadWeekModel> GetReportWeekData(ReportSearchModel model)
        {
            List<UploadWeekModel> ccr = new List<UploadWeekModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_REPORT_WEEK_DATA");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);

                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; };
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadWeekModel
                            {
                                
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                WEEK_YEAR = DataConverter.GetString(item["WEEK_YEAR"]),
                                WEEK_MONTH = DataConverter.GetString(item["WEEK_MONTH"]),
                                ROW_NUM = DataConverter.GetString(item["ROW_NUM"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_REPORT_DESING_CHANGE_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }


        public ResultModel SetUploadNG(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@NG", DbType.Double, ParameterDirection.Input, model.NG);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadNGLog(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_NG_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@NG", DbType.Double, ParameterDirection.Input, model.NG);                   
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadNGLog(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_NG_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<NGDataModel> GetUploadNGLog(DateModel model)
        {
            List<NGDataModel> ccr = new List<NGDataModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_NG_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NGDataModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                NG = DataConverter.GetDouble(item["NG"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<NGDataModel> GetUploadNG(DateModel model)
        {
            List<NGDataModel> ccr = new List<NGDataModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_NG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SUM", DbType.String, ParameterDirection.Input, model.SUM);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NGDataModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                WEEK_YEAR = DataConverter.GetString(item["WEEK_YEAR"]),
                                WEEK_NAME = DataConverter.GetString(item["WEEK_NAME"]),
                                GROUP_NAME = DataConverter.GetString(item["EMP_CODE"]),                          
                                NG = DataConverter.GetDouble(item["NG"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                RUN_ID = DataConverter.GetString(item["RUN_ID"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_NG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadNG(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.String, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
               
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel UpdateUploadNG(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_UPD_UPLOAD_NG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.String, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@NG", DbType.Double, ParameterDirection.Input, model.NG);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_UPD_UPLOAD_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }


        public ResultModel SetUploadActUT(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_ACT_UT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadActUTLog(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_ACT_UT_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadActUTModel> GetUploadActUTLog(DateModel model)
        {
            List<UploadActUTModel> ccr = new List<UploadActUTModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_ACT_UT_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadActUTModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                              
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadActUTModel> GetUploadActUT(DateModel model)
        {
            List<UploadActUTModel> ccr = new List<UploadActUTModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_ACT_UT");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadActUTModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                               
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadActUTLog(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_ACT_UT_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadActUT(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_ACT_UT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }


        public ResultModel SetUploadCutFinising(UploadCutFinisingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_CUT_FINISING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@AMOUNT", DbType.Double, ParameterDirection.Input, model.AMOUNT);
                    command.SetParameter("@EMP_CODE_RECIVE", DbType.String, ParameterDirection.Input, model.EMP_CODE_RECIVE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_CUT_FINISING", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadCutFinisingLog(UploadCutFinisingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData()) { 

                    command.SetStoreProcedure("SP_SET_UPLOAD_CUT_FINISING_LOG");
                    command.Command.Parameters.Clear();
                
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@AMOUNT", DbType.Double, ParameterDirection.Input, model.AMOUNT);
                    command.SetParameter("@EMP_CODE_RECIVE", DbType.String, ParameterDirection.Input, model.EMP_CODE_RECIVE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_CUT_FINISING_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadCutFinisingModel> GetUploadCutFinisingLog(DateModel model)
        {
            List<UploadCutFinisingModel> ccr = new List<UploadCutFinisingModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_CUT_FINISING_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadCutFinisingModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                EMP_CODE_RECIVE = DataConverter.GetString(item["EMP_CODE_RECIVE"]),
                                FULL_NAME_RECIVE = DataConverter.GetString(item["FULL_NAME_RECIVE"]),
                                DEPT_NAME_RECIVE = DataConverter.GetString(item["DEPT_NAME_RECIVE"]),
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                                WEEK_NAME = DataConverter.GetString(item["WEEK_NAME"]),
                                AMOUNT = DataConverter.GetDouble(item["AMOUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_CUT_FINISING_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadCutFinisingModel> GetUploadCutFinising(DateModel model)
        {
            List<UploadCutFinisingModel> ccr = new List<UploadCutFinisingModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_CUT_FINISING");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadCutFinisingModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                EMP_CODE_RECIVE = DataConverter.GetString(item["EMP_CODE_RECIVE"]),
                                FULL_NAME_RECIVE = DataConverter.GetString(item["FULL_NAME_RECIVE"]),
                                DEPT_NAME_RECIVE = DataConverter.GetString(item["DEPT_NAME_RECIVE"]),
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                AMOUNT = DataConverter.GetDouble(item["AMOUNT"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadCutFinisingLog(UploadCutFinisingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_CUT_FINISING_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@EMP_CODE_RECIVE", DbType.String, ParameterDirection.Input, model.EMP_CODE_RECIVE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_CUT_FINISING_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadCutFinising(UploadCutFinisingModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_CUT_FINISING");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_CUT_FINISING", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadGoal(UploadGoalModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_GOAL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@LEVEL", DbType.Int32, ParameterDirection.Input, model.LEVEL_NAME);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_GOAL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadGoalLog(UploadGoalModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_GOAL_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@LEVEL_NAME", DbType.String, ParameterDirection.Input, model.LEVEL_NAME);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_GOAL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadGoalModel> GetUploadGoalLog(DateModel model)
        {
            List<UploadGoalModel> ccr = new List<UploadGoalModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_GOAL_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadGoalModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                LEVEL_NAME = DataConverter.GetString(item["LEVEL_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadGoalModel> GetUploadGoal(DateModel model)
        {
            List<UploadGoalModel> ccr = new List<UploadGoalModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_GOAL");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadGoalModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                LEVEL_NAME = DataConverter.GetString(item["LEVEL_NAME"]),
                                LEVEL_GOAL = DataConverter.GetInteger(item["LEVEL_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_GOAL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadGoalLog(UploadGoalModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_GOAL_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_GOAL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadGoal(UploadGoalModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_GOAL");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_GOAL", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }


        //New NG +4

        public ResultModel SetUploadActUTNew(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_ACT_UT_NEW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@NG", DbType.Double, ParameterDirection.Input, model.NG);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadActUTNewLog(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_ACT_UT_NEW_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@NG", DbType.Double, ParameterDirection.Input, model.NG);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UploadActUTModel> GetUploadActUTNewLog(DateModel model)
        {
            List<UploadActUTModel> ccr = new List<UploadActUTModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_ACT_UT_NEW_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0') );
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadActUTModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                NG = DataConverter.GetDouble(item["NG"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<UploadActUTModel> GetUploadActUTNew(DateModel model)
        {
            List<UploadActUTModel> ccr = new List<UploadActUTModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_ACT_UT_NEW");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0') );
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadActUTModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                NG = DataConverter.GetDouble(item["NG"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public ResultModel DelUploadActUTNewLog(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_ACT_UT_NEW_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_ACT_UT_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadActUTNew(UploadActUTModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_ACT_UT_NEW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_ACT_UT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        //

        public ResultModel SetUploadNGNew(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_NG_NEW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@TOTAL_JOINT", DbType.Double, ParameterDirection.Input, model.TOTAL_JOINT);
                    command.SetParameter("@JOINT_NG", DbType.Double, ParameterDirection.Input, model.JOINT_NG);
                    command.SetParameter("@PERCENT_NG", DbType.Double, ParameterDirection.Input, model.PERCENT_NG);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel SetUploadNGNewLog(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_NG_NEW_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.GROUP_NAME);
                    command.SetParameter("@TOTAL_JOINT", DbType.Double, ParameterDirection.Input, model.TOTAL_JOINT);
                    command.SetParameter("@JOINT_NG", DbType.Double, ParameterDirection.Input, model.JOINT_NG);
                    command.SetParameter("@PERCENT_NG", DbType.Double, ParameterDirection.Input, model.PERCENT_NG);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadNGNewLog(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_NG_NEW_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public ResultModel DelUploadNGNew(NGDataModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_NG_NEW");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.String, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_NG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<NGDataModel> GetUploadNGNewLog(DateModel model)
        {
            List<NGDataModel> ccr = new List<NGDataModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_NG_NEW_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0'));

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NGDataModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                TOTAL_JOINT = DataConverter.GetDouble(item["TOTAL_JOINT"]),
                                JOINT_NG = DataConverter.GetDouble(item["JOINT_NG"]),
                                PERCENT_NG = DataConverter.GetDouble(item["PERCENT_NG"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        public List<NGDataModel> GetUploadNGNew(DateModel model)
        {
            List<NGDataModel> ccr = new List<NGDataModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_NG_NEW");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0'));
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NGDataModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                RUN_ID = DataConverter.GetString(item["RUN_ID"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                TOTAL_JOINT = DataConverter.GetDouble(item["TOTAL_JOINT"]),
                                JOINT_NG = DataConverter.GetDouble(item["JOINT_NG"]),
                                PERCENT_NG = DataConverter.GetDouble(item["PERCENT_NG"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),

                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_NG_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        public ResultModel SetUploadGoalBuilt(UploadGoalBuiltModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_GOAL_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@LEVEL", DbType.Int32, ParameterDirection.Input, model.LEVEL_NAME);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_GOAL_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel SetUploadGoalLogBuilt(UploadGoalBuiltModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_GOAL_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@LEVEL_NAME", DbType.String, ParameterDirection.Input, model.LEVEL_NAME);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);


                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {

                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_GOAL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public List<UploadGoalBuiltModel> GetUploadGoalLogBuilt(DateModel model)
        {
            List<UploadGoalBuiltModel> ccr = new List<UploadGoalBuiltModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_GOAL_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadGoalBuiltModel
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                LEVEL_NAME = DataConverter.GetString(item["LEVEL_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_GOAL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public List<UploadGoalBuiltModel> GetUploadGoalBuilt(DateModel model)
        {
            List<UploadGoalBuiltModel> ccr = new List<UploadGoalBuiltModel>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_GOAL_BUILT");
                    command.Command.Parameters.Clear();
                    string _monthStr = model.MONTH.ToString();
                    if (model.MONTH.Length == 1)
                    {
                        _monthStr = "0" + model.MONTH;
                    }
                       
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + _monthStr);
                    command.SetParameter("@DEPT_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);

                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UploadGoalBuiltModel
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                DEPT_NAME = DataConverter.GetString(item["DEPT_NAME"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                LEVEL_NAME = DataConverter.GetString(item["LEVEL_NAME"]),
                                LEVEL_GOAL = DataConverter.GetDecimal(item["LEVEL_GOAL"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_GOAL", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadGoalLogBuilt(UploadGoalBuiltModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_GOAL_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_GOAL_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel DelUploadGoalBuilt(UploadGoalBuiltModel model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_GOAL_BUILT");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_GOAL_BUILT", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        // 2018-02-28
        // Upload UTRepairCheck
        #region Upload UTRepairCheck
        // Page Upload
        public ResultModel SetUploadUTRepairCheckLogBuilt(UTRepairCheckModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_BUILT_UT_REPAIR_CHECK_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@PJ_NAME", DbType.String, ParameterDirection.Input, model.PJ_NAME);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@ITEM_NO", DbType.String, ParameterDirection.Input, model.ITEM_NO);
                    command.SetParameter("@BUILT_TYPE", DbType.String, ParameterDirection.Input, model.BUILT_TYPE);
                    command.SetParameter("@ACTUAL", DbType.Decimal, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@ACTUAL_CHECK", DbType.String, ParameterDirection.Input, model.ACTUAL_CHECK);
                    command.SetParameter("@REMARK", DbType.String, ParameterDirection.Input, model.REMARK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.UPDATE_BY);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_UT_REPAIR_CHECK_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel SetUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_BUILT_UT_REPAIR_CHECK");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@PJ_NAME", DbType.String, ParameterDirection.Input, model.PJ_NAME);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@ITEM_NO", DbType.String, ParameterDirection.Input, model.ITEM_NO);
                    command.SetParameter("@BUILT_TYPE", DbType.String, ParameterDirection.Input, model.BUILT_TYPE);
                    command.SetParameter("@ACTUAL", DbType.Int32, ParameterDirection.Input, model.ACTUAL);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@ACTUAL_CHECK", DbType.String, ParameterDirection.Input, model.ACTUAL_CHECK);
                    command.SetParameter("@REMARK", DbType.String, ParameterDirection.Input, model.REMARK);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.UPDATE_BY);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_BUILT_UT_REPAIR_CHECK", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckLogBuilt(DateModel model)
        {
            List<UTRepairCheckModelBuilt> ccr = new List<UTRepairCheckModelBuilt>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_BUILT_UT_REPAIR_CHECK_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@TYPE", DbType.String, ParameterDirection.Input, model.TYPE_STORE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UTRepairCheckModelBuilt
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE = DataConverter.GetString(item["BUILT_TYPE"]),
                                ACTUAL = DataConverter.GetDecimal(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                ACTUAL_CHECK = DataConverter.GetString(item["ACTUAL_CHECK"]),
                                REMARK = DataConverter.GetString(item["REMARK"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_BUILT_UT_REPAIR_CHECK_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadUTRepairCheckLogBuilt(UTRepairCheckModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_BUILT_UT_REPAIR_CHECK_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@PJ_NAME", DbType.String, ParameterDirection.Input, model.PJ_NAME);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@ITEM_NO", DbType.String, ParameterDirection.Input, model.ITEM_NO);
                    command.SetParameter("@BUILT_TYPE", DbType.String, ParameterDirection.Input, model.BUILT_TYPE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.UPDATE_BY);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_BUILT_UT_REPAIR_CHECK", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        // PageView
        public List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckBuilt(ReportSearchModel model)
        {
            List<UTRepairCheckModelBuilt> ccr = new List<UTRepairCheckModelBuilt>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_BUILT_UT_REPAIR_CHECK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UTRepairCheckModelBuilt
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                PJ_NAME = DataConverter.GetString(item["PJ_NAME"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                ITEM_NO = DataConverter.GetInteger(item["ITEM_NO"]),
                                BUILT_TYPE = DataConverter.GetString(item["BUILT_TYPE"]),
                                PC_NAME = DataConverter.GetString(item["PC_NAME"]),
                                ACTUAL = DataConverter.GetDecimal(item["ACTUAL"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                ACTUAL_CHECK = DataConverter.GetString(item["ACTUAL_CHECK"]),
                                FULLNAME_CHECK = DataConverter.GetString(item["FULLNAME_CHECK"]),
                                REMARK = DataConverter.GetString(item["REMARK"]),
                                UPDATE_BY = DataConverter.GetString(item["UPDATE_BY"]),
                                UPDATE_DATE = DataConverter.GetString(item["UPDATE_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_BUILT_UT_REPAIR_CHECK", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_BUILT_UT_REPAIR_CHECK");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.UPDATE_BY);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_BUILT_UT_REPAIR_CHECK", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        // End Page upload
        #endregion

        #region Upload UT Data By User 
        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        public ResultModel SetUploadUTDataByUserLogBuilt(UTDataByUserModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_BUILT_UT_DATA_BY_USER_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    // new
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input,"");
                    command.SetParameter("@UT1", DbType.Double, ParameterDirection.Input, model.UT1);
                    command.SetParameter("@UT2", DbType.Double, ParameterDirection.Input, model.UT2);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_BUILT_UT_DATA_BY_USER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel SetUploadUTDataByUserBuilt(UTDataByUserModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_BUILT_UT_DATA_BY_USER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.Int32, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, "");
                    command.SetParameter("@UT1", DbType.String, ParameterDirection.Input, model.UT1);
                    command.SetParameter("@UT2", DbType.Double, ParameterDirection.Input, model.UT2);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_BUILT_UT_DATA_BY_USER", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UTDataByUserModelBuilt> GetUploadUTDataByUserLogBuilt(DateModel model)
        {
            List<UTDataByUserModelBuilt> ccr = new List<UTDataByUserModelBuilt>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_BUILT_UT_DATA_BY_USER_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0')); 
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UTDataByUserModelBuilt
                            {
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                UT1 = DataConverter.GetDecimal(item["UT1"]),
                                UT2 = DataConverter.GetDecimal(item["UT2"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_BUILT_UT_REPAIR_CHECK_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadUTDataByUserLogBuilt(UTDataByUserModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_BUILT_UT_DATA_BY_USER_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.WEEK_KEY);
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK_SUB);
                    command.SetParameter("@EMP_CODE", DbType.String, ParameterDirection.Input, model.EMP_CODE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_BUILT_UT_DATA_BY_USER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<UTDataByUserModelBuilt> GetUploadUTDataByUserBuilt(DateModel model)
        {
            List<UTDataByUserModelBuilt> ccr = new List<UTDataByUserModelBuilt>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_BUILT_UT_DATA_BY_USER");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@WEEK_KEY", DbType.String, ParameterDirection.Input, model.YEAR + model.MONTH.PadLeft(2, '0'));
                    command.SetParameter("@WEEK_SUB", DbType.String, ParameterDirection.Input, model.WEEK);
                    command.SetParameter("@GROUP_NAME", DbType.String, ParameterDirection.Input, model.DEPT_CODE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new UTDataByUserModelBuilt
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                WEEK_KEY = DataConverter.GetString(item["WEEK_KEY"]),
                                WEEK_SUB = DataConverter.GetInteger(item["WEEK_SUB"]),
                                EMP_CODE = DataConverter.GetString(item["EMP_CODE"]),
                                UT1 = DataConverter.GetDecimal(item["UT1"]),
                                UT2 = DataConverter.GetDecimal(item["UT2"]),
                                FULL_NAME = DataConverter.GetString(item["FULL_NAME"]),
                                GROUP_NAME = DataConverter.GetString(item["GROUP_NAME"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_BUILT_UT_DATA_BY_USER", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_BUILT_UT_DATA_BY_USER");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_BUILT_UT_DATA_BY_USER", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        #endregion

        #region Upload NG Data Built
        // NGDataBuilt
        // writing store connection
        public ResultModel SetUploadNGDataLogBuilt(NGDataModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            decimal _SAW_NG_LENGTH = 0;
            decimal _SAW_NG_JOINT = 0;
            decimal _SAW_TOTAL = 0;
            decimal _AUTOROOT_NG_LENGTH = 0;
            decimal _AUTOROOT_NG_JOINT = 0;
            decimal _AUTOROOT_TOTAL = 0;
            decimal _ESW_NG_LENGTH = 0;
            decimal _ESW_NG_JOINT = 0;
            decimal _ESW_TOTAL = 0;

            _SAW_NG_LENGTH = model.SAW_NG_LENGTH < 0 ? 0 : model.SAW_NG_LENGTH;
            _SAW_NG_JOINT = model.SAW_NG_JOINT < 0 ? 0 : model.SAW_NG_JOINT;
            _SAW_TOTAL = model.SAW_TOTAL < 0 ? 0 : model.SAW_TOTAL;
            _AUTOROOT_NG_LENGTH = model.AUTOROOT_NG_LENGTH < 0 ? 0 : model.AUTOROOT_NG_LENGTH;
            _AUTOROOT_NG_JOINT = model.AUTOROOT_NG_JOINT < 0 ? 0 : model.AUTOROOT_NG_JOINT;
            _AUTOROOT_TOTAL = model.AUTOROOT_TOTAL < 0 ? 0 : model.AUTOROOT_TOTAL;
            _ESW_NG_LENGTH = model.ESW_NG_LENGTH < 0 ? 0 : model.ESW_NG_LENGTH;
            _ESW_NG_JOINT = model.ESW_NG_JOINT < 0 ? 0 : model.ESW_NG_JOINT;
            _ESW_TOTAL = model.ESW_TOTAL < 0 ? 0 : model.ESW_TOTAL;

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_BUILT_NG_DATA_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@DATE", DbType.String, ParameterDirection.Input, model.DATE);
                    // new
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@CODE", DbType.String, ParameterDirection.Input, model.CODE);
                    command.SetParameter("@SKIN_PLATE", DbType.Decimal, ParameterDirection.Input, model.SKIN_PLATE);
                    command.SetParameter("@DIAPHRAGM", DbType.String, ParameterDirection.Input, model.DIAPHRAGM);
                    command.SetParameter("@SAW_NG_LENGTH", DbType.String, ParameterDirection.Input, model.SAW_NG_LENGTH);
                    command.SetParameter("@SAW_NG_JOINT", DbType.String, ParameterDirection.Input, model.SAW_NG_JOINT);
                    command.SetParameter("@SAW_TOTAL", DbType.String, ParameterDirection.Input, model.SAW_TOTAL);
                    command.SetParameter("@SAW_D", DbType.String, ParameterDirection.Input, model.SAW_D);
                    command.SetParameter("@SAW_K", DbType.String, ParameterDirection.Input, model.SAW_K);
                    command.SetParameter("@SAW_WELDER", DbType.String, ParameterDirection.Input, model.SAW_WELDER);
                    command.SetParameter("@AUTOROOT_NG_LENGTH", DbType.String, ParameterDirection.Input, model.AUTOROOT_NG_LENGTH);
                    command.SetParameter("@AUTOROOT_NG_JOINT", DbType.String, ParameterDirection.Input, model.AUTOROOT_NG_JOINT);
                    command.SetParameter("@AUTOROOT_TOTAL", DbType.String, ParameterDirection.Input, model.AUTOROOT_TOTAL);
                    command.SetParameter("@AUTOROOT_D", DbType.String, ParameterDirection.Input, model.AUTOROOT_D);
                    command.SetParameter("@AUTOROOT_K", DbType.String, ParameterDirection.Input, model.AUTOROOT_K);
                    command.SetParameter("@AUTOROOT_WELDER", DbType.String, ParameterDirection.Input, model.AUTOROOT_WELDER);
                    command.SetParameter("@ESW_NG_LENGTH", DbType.String, ParameterDirection.Input, model.ESW_NG_LENGTH);
                    command.SetParameter("@ESW_NG_JOINT", DbType.String, ParameterDirection.Input, model.ESW_NG_JOINT);
                    command.SetParameter("@ESW_TOTAL", DbType.String, ParameterDirection.Input, model.ESW_TOTAL);
                    command.SetParameter("@ESW_D", DbType.String, ParameterDirection.Input, model.ESW_D);
                    command.SetParameter("@ESW_X", DbType.String, ParameterDirection.Input, model.ESW_X);
                    command.SetParameter("@ESW_SIDE", DbType.String, ParameterDirection.Input, model.ESW_SIDE);
                    command.SetParameter("@ESW_WELDER", DbType.String, ParameterDirection.Input, model.ESW_WELDER);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_BUILT_UT_DATA_BY_USER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel SetUploadNGDataBuilt(NGDataModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_BUILT_NG_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@RUN_ID", DbType.String, ParameterDirection.Input, model.RUN_ID);
        
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_BUILT_NG_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<NGDataModelBuilt> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model)
        {
            List<NGDataModelBuilt> ccr = new List<NGDataModelBuilt>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_BUILT_NG_DATA_LOG");
                    command.Command.Parameters.Clear();
               
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NGDataModelBuilt
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                DATE = DataConverter.GetString(item["DATE"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                CODE = DataConverter.GetString(item["CODE"]),
                                SKIN_PLATE = DataConverter.GetDecimal(item["SKIN_PLATE"]),
                                DIAPHRAGM = DataConverter.GetDecimal(item["DIAPHRAGM"]),
                                SAW_NG_LENGTH = DataConverter.GetDecimal(item["SAW_NG_LENGTH"]),
                                SAW_NG_JOINT = DataConverter.GetDecimal(item["SAW_NG_JOINT"]),
                                SAW_TOTAL = DataConverter.GetDecimal(item["SAW_TOTAL"]),
                                SAW_D = DataConverter.GetString(item["SAW_D"]),
                                SAW_K = DataConverter.GetString(item["SAW_K"]),
                                SAW_WELDER = DataConverter.GetString(item["SAW_WELDER"]),
                                AUTOROOT_NG_LENGTH = DataConverter.GetDecimal(item["AUTOROOT_NG_LENGTH"]),
                                AUTOROOT_NG_JOINT = DataConverter.GetDecimal(item["AUTOROOT_NG_JOINT"]),
                                AUTOROOT_TOTAL = DataConverter.GetDecimal(item["AUTOROOT_TOTAL"]),
                                AUTOROOT_D = DataConverter.GetString(item["AUTOROOT_D"]),
                                AUTOROOT_K = DataConverter.GetString(item["AUTOROOT_K"]),
                                AUTOROOT_WELDER = DataConverter.GetString(item["AUTOROOT_WELDER"]),
                                ESW_NG_LENGTH = DataConverter.GetDecimal(item["ESW_NG_LENGTH"]),
                                ESW_NG_JOINT = DataConverter.GetDecimal(item["ESW_NG_JOINT"]),
                                ESW_TOTAL = DataConverter.GetDecimal(item["ESW_TOTAL"]),
                                ESW_D = DataConverter.GetString(item["ESW_D"]),
                                ESW_X = DataConverter.GetString(item["ESW_X"]),
                                ESW_SIDE = DataConverter.GetString(item["ESW_SIDE"]),
                                ESW_WELDER = DataConverter.GetString(item["ESW_WELDER"]),
                                USER_UPDATE = DataConverter.GetString(item["UPDATE_BY"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_BUILT_UT_DATA_BY_USER_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadNGDataLogBuilt(NGDataModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_BUILT_NG_DATA_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@RUN_ID", DbType.String, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_BUILT_NG_DATA_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<NGDataModelBuilt> GetUploadNGDataBuilt(ReportSearchBuiltModel model)
        {
            List<NGDataModelBuilt> ccr = new List<NGDataModelBuilt>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_BUILT_NG_DATA");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new NGDataModelBuilt
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),
                                DATE = DataConverter.GetString(item["DATE"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                CODE = DataConverter.GetString(item["CODE"]),
                                NO_ORDER = DataConverter.GetInteger(item["NO_ORDER"]),
                                SKIN_PLATE = DataConverter.GetDecimal(item["SKIN_PLATE"]),
                                DIAPHRAGM = DataConverter.GetDecimal(item["DIAPHRAGM"]),
                                SAW_NG_LENGTH = DataConverter.GetDecimal(item["SAW_NG_LENGTH"]),
                                SAW_NG_JOINT = DataConverter.GetDecimal(item["SAW_NG_JOINT"]),
                                SAW_TOTAL = DataConverter.GetDecimal(item["SAW_TOTAL"]),
                                SAW_D = DataConverter.GetString(item["SAW_D"]),
                                SAW_K = DataConverter.GetString(item["SAW_K"]),
                                SAW_WELDER = DataConverter.GetString(item["SAW_WELDER"]),
                                AUTOROOT_NG_LENGTH = DataConverter.GetDecimal(item["AUTOROOT_NG_LENGTH"]),
                                AUTOROOT_NG_JOINT = DataConverter.GetDecimal(item["AUTOROOT_NG_JOINT"]),
                                AUTOROOT_TOTAL = DataConverter.GetDecimal(item["AUTOROOT_TOTAL"]),
                                AUTOROOT_D = DataConverter.GetString(item["AUTOROOT_D"]),
                                AUTOROOT_K = DataConverter.GetString(item["AUTOROOT_K"]),
                                AUTOROOT_WELDER = DataConverter.GetString(item["AUTOROOT_WELDER"]),
                                ESW_NG_LENGTH = DataConverter.GetDecimal(item["ESW_NG_LENGTH"]),
                                ESW_NG_JOINT = DataConverter.GetDecimal(item["ESW_NG_JOINT"]),
                                ESW_TOTAL = DataConverter.GetDecimal(item["ESW_TOTAL"]),
                                ESW_D = DataConverter.GetString(item["ESW_D"]),
                                ESW_X = DataConverter.GetString(item["ESW_X"]),
                                ESW_SIDE = DataConverter.GetString(item["ESW_SIDE"]),
                                ESW_WELDER = DataConverter.GetString(item["ESW_WELDER"]),
                                USER_UPDATE = DataConverter.GetString(item["UPDATE_BY"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_BUILT_NG_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadNGDataBuilt(NGDataModelBuilt model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_BUILT_NG_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@RUN_ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_BUILT_UT_DATA_BY_USER", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        #endregion

        #region Upload Camber Data Other
        // OtherCamber
        // 2018-04-23 by chaiwud.ta
        public ResultModel SetUploadCamberDataLogOther(CamberDataModelOther model)
        {
            ResultModel rs = new ResultModel();
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_OTHER_CAMBER_DATA_LOG");
                    command.Command.Parameters.Clear();
               
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@PD_ITEM", DbType.Int32, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@CUTTING_PLAN", DbType.String, ParameterDirection.Input, model.CUTTING_PLAN);
                    command.SetParameter("@BUILT_NO", DbType.String, ParameterDirection.Input, model.BUILT_NO);
                    command.SetParameter("@ACTUAL_GROUP", DbType.String, ParameterDirection.Input, model.ACTUAL_GROUP);
                    command.SetParameter("@ACTUAL_DATE", DbType.String, ParameterDirection.Input, model.ACTUAL_DATE);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_OTHER_CAMBER_DATA_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }
        public ResultModel SetUploadCamberDataOther(CamberDataModelOther model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_UPLOAD_OTHER_CAMBER_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@RUN_ID", DbType.String, ParameterDirection.Input, model.RUN_ID);

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_SET_UPLOAD_OTHER_CAMBER_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<CamberDataModelOther> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model)
        {
            List<CamberDataModelOther> ccr = new List<CamberDataModelOther>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_OTHER_CAMBER_DATA_LOG");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new CamberDataModelOther
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetString(item["BUILT_NO"]),
                                ACTUAL_GROUP = DataConverter.GetString(item["ACTUAL_GROUP"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                USER_UPDATE = DataConverter.GetString(item["UPDATE_BY"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }

                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_OTHER_CAMBER_DATA_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadCamberDataLogOther(CamberDataModelOther model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_OTHER_CAMBER_DATA_LOG");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@RUN_ID", DbType.String, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_OTHER_CAMBER_DATA_LOG", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        public List<CamberDataModelOther> GetUploadCamberDataOther(ReportSearchOtherFinishModel model)
        {
            List<CamberDataModelOther> ccr = new List<CamberDataModelOther>();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_GET_UPLOAD_OTHER_CAMBER_DATA");
                    command.Command.Parameters.Clear();

                    command.SetParameter("@START_DATE", DbType.String, ParameterDirection.Input, model.START_DATE);
                    command.SetParameter("@END_DATE", DbType.String, ParameterDirection.Input, model.END_DATE);
                    command.SetParameter("@PROJECT", DbType.String, ParameterDirection.Input, model.PROJECT);
                    command.SetParameter("@PD_ITEM", DbType.String, ParameterDirection.Input, model.PD_ITEM);
                    command.SetParameter("@CUTTING_PLAN", DbType.String, ParameterDirection.Input, model.CUTTING_PLAN);
                    command.SetParameter("@ACTUAL_GROUP", DbType.String, ParameterDirection.Input, model.GROUP);
                  
                    command.SetParameter("@SEARCH", DbType.String, ParameterDirection.Input, model.SEARCH);
                    command.SetParameter("@SORT", DbType.String, ParameterDirection.Input, model.SORT);
                    command.SetParameter("@SORT_TYPE", DbType.Int16, ParameterDirection.Input, model.SORT_TYPE);
                    command.SetParameter("@TAKE", DbType.Int16, ParameterDirection.Input, model.TAKE);
                    command.SetParameter("@SKIP", DbType.Int16, ParameterDirection.Input, model.SKIP);
                    var result = command.ExecuteDataSet();
                    if (!DataConverter.CheckRowOnDataSet(result)) { return ccr; }
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                        foreach (DataRow item in result.Tables[0].Rows)
                        {
                            ccr.Add(new CamberDataModelOther
                            {
                                RUN_ID = DataConverter.GetInteger(item["RUN_ID"]),
                                ROW_NUM = DataConverter.GetInteger(item["ROW_NUM"]),                        
                                PROJECT = DataConverter.GetString(item["PROJECT"]),
                                PD_ITEM = DataConverter.GetInteger(item["PD_ITEM"]),
                                CUTTING_PLAN = DataConverter.GetString(item["CUTTING_PLAN"]),
                                BUILT_NO = DataConverter.GetString(item["BUILT_NO"]),
                                ACTUAL_GROUP = DataConverter.GetString(item["ACTUAL_GROUP"]),
                                ACTUAL_DATE = DataConverter.GetString(item["ACTUAL_DATE"]),
                                USER_UPDATE = DataConverter.GetString(item["UPDATE_BY"]),
                                UPDATE_DATE = DataConverter.GetString(item["UPDATE_DATE"]),
                                TOTAL_COUNT = DataConverter.GetIntegerNull(item["TOTAL_COUNT"]),
                            });
                        }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_GET_UPLOAD_OTHER_CAMBER_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return ccr;
        }
        public ResultModel DelUploadCamberDataOther(CamberDataModelOther model)
        {
            ResultModel rs = new ResultModel();

            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_DEL_UPLOAD_OTHER_CAMBER_DATA");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@RUN_ID", DbType.Int32, ParameterDirection.Input, model.RUN_ID);
                    command.SetParameter("@USER_UPDATE", DbType.String, ParameterDirection.Input, model.USER_UPDATE);

                    var result = command.ExecuteDataSet();
                    if (DataConverter.CheckRowOnDataTable(result.Tables[0]))
                    {
                        var item = result.Tables[0].Rows[0];
                        rs = new ResultModel(item);
                    }
                }
            }
            catch (Exception e)
            {
                new LogData().WriteLogError(this.GetType().FullName, "SP_DEL_UPLOAD_OTHER_CAMBER_DATA", e.Message);
                throw new Exception(e.Message);
            }
            return rs;
        }

        #endregion

        #endregion
    }
}
