﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using MCSAPP.DAL.Data.InterfaceData;

namespace MCSAPP.DAL.Data.RepositoryData
{
    public class LogData :ILogData
    {
        public int WriteLogError(string PAGE_NAME, string FUNC_NAME ,string ERROR_MSG)
        {
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_LOG_ERROR");
                    command.Command.Parameters.Clear();

               
                    command.SetParameter("@page_name", DbType.String, ParameterDirection.Input,PAGE_NAME);
                    command.SetParameter("@func_name", DbType.String, ParameterDirection.Input, FUNC_NAME);
                    command.SetParameter("@error_msg", DbType.String, ParameterDirection.Input,ERROR_MSG);
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public int WriteLogActivity(string pageName, string funcName, string actionMsg, string userUpdate)
        {
            try
            {
                using (CommandData command = new CommandData())
                {
                    command.SetStoreProcedure("SP_SET_LOG_ACTIVITY");
                    command.Command.Parameters.Clear();
                    command.SetParameter("@page_name", DbType.String, ParameterDirection.Input, pageName ?? Convert.DBNull);
                    command.SetParameter("@func_name", DbType.String, ParameterDirection.Input, funcName ?? Convert.DBNull);
                    command.SetParameter("@action_msg", DbType.String, ParameterDirection.Input, actionMsg ?? Convert.DBNull);
                    command.SetParameter("@user_update", DbType.String, ParameterDirection.Input, userUpdate ?? Convert.DBNull);
                    return command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                WriteLogError( this.GetType().FullName, "WriteLogActivity", e.Message);
                throw new Exception(e.Message);
            }
        }
    }
}
