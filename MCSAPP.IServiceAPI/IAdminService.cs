﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
namespace MCSAPP.IServiceAPI
{
     public  interface IAdminService
    {
        long WriteLogError(LogModel model);
        long WriteLogActivity(LogModel model);
        ResultModel SetTxnAdjustGoalData(AdjustGoalModel model);
        ResultModel SetTxnNcrData(List<NcrDataModel> model);
        ResultModel SetNCRDataLog(List<NcrDataModel> model);
        ResultModel DelNCRDetailLog(List<NcrDataModel> model);
        ResultModel SetTxnVTData(List<VTDataModel> model);
        ResultModel SetTxnDimensionData(DimDataModel model);
        ResultModel SetTxnOtherData(OtherDataModel model);
        ResultModel SetTxnWeekData(WeekDataModel model);
        ResultModel SetTxnMinusData(MinusDataModel model);
        ResultModel SetVTDetail(List<VTListModel> model);
        ResultModel SetVTDetailLog(List<VTListModel> model);
        ResultModel SetVTDetailUpload(List<VTListModel> model);
        ResultModel DelVTDetailLog(List<VTListModel> model);
        List<VTListModel> GetVTDetailLog(DateModel model);
        List<VTListModel> GetVTDetailAll(DateModel model);

        NCRDetailHeaderModel GetReportNCRDetail(DateModel model);
        NGDetailHeaderModel GetReportNGDetail(DateModel model);
        List<NcrDataModel> GetNCRLog(DateModel model);
        VTDetailHeaderModel GetReportVTDetail(DateModel model);
        GoalSumModel GetWeekLessForAdjust(DateModel model);
        List<GoalSumModel> GetAdjustGoalData(DateModel model);
        List<MonthModel> GetCalendar(DateModel model);

        //List<MinusDataModel> GetReportMinusData(ReportSearchModel model);
        //List<OtherDataModel> GetReportOtherData(ReportSearchModel model);

        ResultModel SetDesingLog(List<DesingModel> model);
        ResultModel SetDesing(List<DesingModel> model);
        ResultModel DelDesingLog(List<DesingModel> model);
        List<DesingModel> GetDesingLog(DateModel model);


        ResultModel SetUploadOtherLog(List<OtherModel> model);
        ResultModel SetUploadOther(List<OtherModel> model);
        ResultModel DelUploadOtherLog(List<OtherModel> model);
        List<OtherModel> GetUploadOtherLog(DateModel model);


        ResultModel SetUploadWeekLog(List<UploadWeekModel> model);
        ResultModel SetUploadWeek(List<UploadWeekModel> model);
        ResultModel DelUploadWeekLog(List<UploadWeekModel> model);
        List<UploadWeekModel> GetUploadWeekLog(DateModel model);


        ResultModel DeleteAdjustGoal(DeleteModel model);

        ResultModel DeleteDesingChange(DeleteModel model);

        ResultModel DeleteDimension(DeleteModel model);

        ResultModel DeleteMinus(DeleteModel model);

        ResultModel DeleteOther(DeleteModel model);

        ResultModel DeletePlus(DeleteModel model);

        ResultModel DeleteVTData(DeleteModel model);

        ResultModel DeleteWeek(DeleteModel model);

        List<ReportDesignChangeModel> GetReportDesingChangeData(ReportSearchModel model);

        List<ReportDimensionModel> GetReportDimensionData(ReportSearchModel model);

        List<ReportMinusModel> GetReportMinusData(ReportSearchModel model);

        List<ReportDimensionModel> GetReportOtherData(ReportSearchModel model);

        List<ReportDimensionModel> GetReportPlusData(ReportSearchModel model);

        List<VTListModel> GetReportVTData(ReportSearchModel model);

        List<UploadWeekModel> GetReportWeekData(ReportSearchModel model);


        ResultModel SetUploadNG(List<NGDataModel> model);

        ResultModel SetUploadNGLog(List<NGDataModel> model);

        ResultModel DelUploadNGLog(List<NGDataModel> model);

        List<NGDataModel> GetUploadNGLog(DateModel model);

        List<NGDataModel> GetUploadNG(DateModel model);

        ResultModel DelUploadNG(NGDataModel model);

        ResultModel UpdateUploadNG(NGDataModel model);

        ResultModel SetUploadActUTLog(List<UploadActUTModel> model);
        ResultModel SetUploadActUT(List<UploadActUTModel> model);  
        ResultModel DelUploadActUTLog(List<UploadActUTModel> model);
        ResultModel DelUploadActUT(UploadActUTModel model);
        List<UploadActUTModel> GetUploadActUTLog(DateModel model);
        List<UploadActUTModel> GetUploadActUT(DateModel model);

        ResultModel SetUploadCutFinisingLog(List<UploadCutFinisingModel> model);
        ResultModel SetUploadCutFinising(List<UploadCutFinisingModel> model);
        ResultModel DelUploadCutFinisingLog(List<UploadCutFinisingModel> model);
        ResultModel DelUploadCutFinising(UploadCutFinisingModel model);
        List<UploadCutFinisingModel> GetUploadCutFinisingLog(DateModel model);
        List<UploadCutFinisingModel> GetUploadCutFinising(DateModel model);


        ResultModel SetUploadGoalLog(List<UploadGoalModel> model);
        ResultModel SetUploadGoal(List<UploadGoalModel> model);
        ResultModel DelUploadGoalLog(List<UploadGoalModel> model);
        ResultModel DelUploadGoal(UploadGoalModel model);
        List<UploadGoalModel> GetUploadGoalLog(DateModel model);
        List<UploadGoalModel> GetUploadGoal(DateModel model);

        //
        ResultModel SetUploadActUTNew(List<UploadActUTModel> model);
        ResultModel SetUploadActUTNewLog(List<UploadActUTModel> model);

        List<UploadActUTModel> GetUploadActUTNewLog(DateModel model);
        List<UploadActUTModel> GetUploadActUTNew(DateModel model);

        ResultModel DelUploadActUTNewLog(List<UploadActUTModel> model);
        ResultModel DelUploadActUTNew(UploadActUTModel model);

        //

        ResultModel SetUploadNGNew(List<NGDataModel> model);
        ResultModel SetUploadNGNewLog(List<NGDataModel> model);

        ResultModel DelUploadNGNewLog(List<NGDataModel> model);
        ResultModel DelUploadNGNew(NGDataModel model);

        List<NGDataModel> GetUploadNGNewLog(DateModel model);
        List<NGDataModel> GetUploadNGNew(DateModel model);


        List<UploadTypeModel> GetHeaderExcel(DateModel model);

        //EndTab
        ResultModel SetUploadEndTab(List<UploadEndTabModel> model);
        ResultModel SetUploadEndTabLog(List<UploadEndTabModel> model);

        List<UploadEndTabModel> GetUploadEndTabLog(DateModel model);
        List<UploadEndTabModel> GetUploadEndTab(DateModel model);

        ResultModel DelUploadEndTabLog(List<UploadEndTabModel> model);
        ResultModel DelUploadEndTab(UploadEndTabModel model);


        ResultModel SetUploadCutPartLog(List<UploadCutPartModel> model);
        ResultModel SetUploadCutPart(List<UploadCutPartModel> model);
        ResultModel DelUploadCutPartLog(List<UploadCutPartModel> model);
        ResultModel DelUploadCutPart(UploadCutPartModel model);
        List<UploadCutPartModel> GetUploadCutPartLog(DateModel model);
        List<UploadCutPartModel> GetUploadCutPart(DateModel model);

        //Process
        List<ProcessTypeModel> GetMasterProcessType(SearchModel model);

        ResultModel SetMasterProcessType(ProcessTypeModel model);

        ResultModel DeleteMasterProcessType(ProcessTypeModel model);

        ResultModel SetCheckSymbol(List<PartDatilSummaryForWorkModel> model);

        ResultModel DelCheckSymbolData(List<DataValueModel> data);
        PartDetailModel GetDetailForCheckSymbolData(DateModel model);

        #region Goal Built Beam/Box by chaiwud.ta 
        // GoalB
        // 2018-02-12 by chaiwud.ta
        ResultModel SetUploadGoalLogBuilt(List<UploadGoalBuiltModel> model);
        ResultModel SetUploadGoalBuilt(List<UploadGoalBuiltModel> model);
        ResultModel DelUploadGoalLogBuilt(List<UploadGoalBuiltModel> model);
        ResultModel DelUploadGoalBuilt(UploadGoalBuiltModel model);
        List<UploadGoalBuiltModel> GetUploadGoalLogBuilt(DateModel model);
        List<UploadGoalBuiltModel> GetUploadGoalBuilt(DateModel model);

        // UTRepairCheck
        // 2018-03-01 by chaiwud.ta
        ResultModel SetUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model);
        ResultModel SetUploadUTRepairCheckBuilt(List<UTRepairCheckModelBuilt> model);

        ResultModel DelUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model);
        ResultModel DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model);

        List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckLogBuilt(DateModel model);
        List<UTRepairCheckModelBuilt> GetUploadUTRepairCheckBuilt(ReportSearchModel model);

        #region UTDataByUser
        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        ResultModel SetUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model);
        ResultModel SetUploadUTDataByUserBuilt(List<UTDataByUserModelBuilt> model);
        ResultModel DelUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model);
        ResultModel DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model);
        List<UTDataByUserModelBuilt> GetUploadUTDataByUserLogBuilt(DateModel model);
        List<UTDataByUserModelBuilt> GetUploadUTDataByUserBuilt(DateModel model);

        // NGDataBuilt
        // 2018-04-17 by chaiwud.ta
        ResultModel SetUploadNGDataLogBuilt(List<NGDataModelBuilt> model);
        ResultModel SetUploadNGDataBuilt(List<NGDataModelBuilt> model);
        ResultModel DelUploadNGDataLogBuilt(List<NGDataModelBuilt> model);
        ResultModel DelUploadNGDataBuilt(NGDataModelBuilt model);
        List<NGDataModelBuilt> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model);
        List<NGDataModelBuilt> GetUploadNGDataBuilt(ReportSearchBuiltModel model);

        // OtherCamber
        // 2018-04-23 by chaiwud.ta
        ResultModel SetUploadCamberDataLogOther(List<CamberDataModelOther> model);
        ResultModel SetUploadCamberDataOther(List<CamberDataModelOther> model);
        ResultModel DelUploadCamberDataLogOther(List<CamberDataModelOther> model);
        ResultModel DelUploadCamberDataOther(CamberDataModelOther model);
        List<CamberDataModelOther> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model);
        List<CamberDataModelOther> GetUploadCamberDataOther(ReportSearchOtherFinishModel model);
        #endregion
        #endregion
    }
}
