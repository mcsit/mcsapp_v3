﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;

namespace MCSAPP.IServiceAPI
{
    public interface IUserDepartmentService
    {
        List<DepartmentModel> GetGroupName(SearchModel model);

        List<UserModel> GetSearchUser(SearchModel model);

        UserAccountModel GetLogin(UserLoginModel model);

        List<DataValueModel> GetMainType(SearchModel model);

        List<DataValueModel> GetLevelType(SearchModel model);

        List<DataValueModel> GetMainTypeForProcess(SearchModel model);

        List<DataValueModel> GetHeaderForProcess(SearchModel model);

        List<DataValueModel> GetHeaderWeek(DateModel model);

        List<DataValueModel> GetProjectForCheckSymbol(DateModel model);

        List<DataValueModel> GetProjectItemForCheckSymbol(DateModel model);

        List<DataValueModel> GetProjectForCheckSymbolData(DateModel model);

        List<DataValueModel> GetProjectItemForCheckSymbolData(DateModel model);

    }
}
