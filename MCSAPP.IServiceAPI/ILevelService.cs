﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;

namespace MCSAPP.IServiceAPI
{
    public interface ILevelService
    {
        List<LevelModel> GetMasterLevel(SearchModel model);

        List<LevelNCRModel> GetMasterLevelNCR(SearchModel model);

        List<LevelNGModel> GetMasterLevelNG(SearchModel model);

        List<LevelGroupModel> GetMasterLevelGroup(SearchModel model);

        List<ProcessModel> GetMasterProcess(SearchModel model);

        List<CertModel> GetMasterCert();

        List<CertListModel> GetMasterCertList();


        ResultModel SetMasterCert(CertModel model);

        ResultModel SetMasterGroup(LevelGroupModel model);

        ResultModel SetMasterLevel(LevelModel model);

        ResultModel SetMasterLevelNCR(LevelNCRModel model);

        ResultModel SetMasterLevelNG(LevelNGModel model);

        ResultModel SetMasterProcess(ProcessModel model);

        ResultModel DeleteMasterCert(CertModel model);

        ResultModel DeleteMasterGroup(LevelGroupModel model);

        ResultModel DeleteMasterLevel(LevelModel model);

        ResultModel DeleteMasterLevelNCR(LevelNCRModel model);

        ResultModel DeleteMasterLevelNG(LevelNGModel model);

        ResultModel DeleteMasterProcess(ProcessModel model);

        DataFilterModel GetPriceByProcess(ProcessModel model);

        #region Built Beam/Box by chaiwud.ta
        // 2018-02-10 by chaiwud.ta  
        // Level Built Beam/Box
        List<LevelBuiltModel> GetMasterBuiltLevel(SearchModel model);
        ResultModel SetMasterBuiltLevel(LevelBuiltModel model);
        ResultModel DeleteMasterBuiltLevel(LevelBuiltModel model);
        #endregion
    }
}
