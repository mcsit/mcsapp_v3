﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;

namespace MCSAPP.IServiceAPI
{
    public interface IWeekService
    {
        List<DataFilterModel> GetDays(DateModel model);
        List<DataFilterModel> GetWeeks(DateModel model);
        DataFilterModel GetMaxWeeks(DateModel model);
        List<DataValueModel> GetHeaderWeek(DateModel model);

        List<WeekModel> GetWeekByMonth(DateModel model);
        List<GoalModel> GetTxnMonthByWeek(DateModel model);
        List<GoalSumModel> GetSummaryByWork(DateModel model);
        List<SararyModel> GetSararyMonthByWeek(DateModel model);
        GoalHeaderModel GetSummaryWorkByCode(DateModel model);
        List<SararyModel> GetSararyMonthByCode(DateModel model);
        List<SumSalaryModel> GetSalaryFab(DateModel model);
        DayHeaderModel GetWorkLessGoal(DateModel model);
        List<WorkModel> GetWorkMonth(DateModel model);
        List<WorkModel> GetWorkMonthByDay(DateModel model);
        DayHeaderModel GetWorkByDay(DateModel model);
        WeekHeaderModel GetWorkByWeek(DateModel model);
        DayHeaderModel GetSalaryByDay(DateModel model);
        WeekHeaderModel GetSalaryByWeek(DateModel model);
        DayHeaderModel GetSalaryGroupByDay(DateModel model);
        WeekHeaderModel GetSalaryGroupByWeek(DateModel model);

        DayHeaderModel GetSalaryByDayAndCode(DateModel model);
        DayHeaderModel GetWorkByDayAndCode(DateModel model);

        List<ReviseModel> GetReviseByDayAndCode(DateModel model);
        List<ReviseModel> GetOtherByDayAndCode(DateModel model);

        NCRHeaderModel GetReportNCR(DateModel model);
        NCRHeaderModel GetReportVT(DateModel model);

        GoalHeaderModel GetSummaryWorkDayForWeek(DateModel model);
        List<SalarySummaryModel> GetSummarySalaryDayForWeek(DateModel model);
        GoalHeaderModel GetSummaryWorkDayForWeekLess(DateModel model);
        GoalHeaderModel GetSummaryWorkDayForWeekWeldLess(DateModel model);

        List<PlusModel> GetSummarySalaryPlus(DateModel model);
        List<PlusModel> GetSummarySalaryMinus(DateModel model);
        List<PlusModel> GetSummarySalaryTotal(DateModel model);

        SummaryWorkHeaderModel GetSummaryWorkDayForWeekWeld(DateModel model);
        SummarySalaryHeaderModel GetSummarySalaryForWeekWeld(DateModel model);

        SummarySalaryHeaderModel GetSummarySalaryForWeekWeldSum(DateModel model);
        SummarySalaryHeaderModel GetSummarySalaryForWeekSum(DateModel model);
    }
}
