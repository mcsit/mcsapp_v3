﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.IServiceAPI
{
    public interface IWorkService
    {
        AllSummaryForWorkModel GetSummaryForWorkByAutoGas1(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByAutoGas2(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByAutoGas3(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByTaper(DateModel model);
        AllSummaryForWorkModel GetSummaryForWorkByPart(DateModel model);

        PartDetailModel GetDetailSummaryForWorkByAutoGas1(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByAutoGas2(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByAutoGas3(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByTaper(DateModel model);
        PartDetailModel GetDetailSummaryForWorkByPart(DateModel model);

        PartDetailModel GetDetailForCheckSymbol(DateModel model);

        AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas1(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas2(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByAutoGas3(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByTaper(DateModel model);
        AllSummaryForSalaryModel GetSummaryForSalaryByPart(DateModel model);

        AllShowSummaryModel GetSummaryForGrinder(DateModel model);
        AllShowSummaryModel GetSummaryForCut(DateModel model);

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2

        // 2018-01-26 by chaiwud.ta
        // Built Up Beam
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBeam(DateModel model);
        // ** 2018-02-01 Add Page **
        // Saw Beam 
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltSawBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltSawBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltSawBeam(DateModel model);
        // Adjust Beam
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAdjustBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAdjustBeam(DateModel model);
        // Drill & Shot Blast
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillShotBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model);
        // ** 2018-02-01 Add Page **

        #region Other Beam/Box 
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBeam(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByOtherBeam(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBeam(DateModel model);
        // Other Adjust Box
        AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByOtherBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherBox(DateModel model);

        // Other Adjust Finishing
        AllSummaryForWorkBuiltModel GetSummaryForWorkByOtherFinishing(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByOtherFinishing(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByOtherFinishing(DateModel model);
        #endregion

        // Built Box 
        // 2018-02-12 by chaiwud.ta 
        // Fab Diaphagm -> FabDiaphragmBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model);
        // Built Up Box -> BuiltUpBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltUpBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltUpBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltUpBox(DateModel model);
        // Auto Root Box -> AutoRootBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltAutoRootBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltAutoRootBox(DateModel model);
        // Drill & Sesnet Box -> DrillSesnetBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model);
        // Gouging & Repair Box -> GougingRepairBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltGougingRepairBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model);
        // Facing Box -> FacingBox
        AllSummaryForWorkBuiltModel GetSummaryForWorkByBuiltFacingBox(DateModel model);
        BuiltDetailModel GetDetailSummaryForWorkByBuiltFacingBox(DateModel model);
        AllSummaryForSalaryBuiltModel GetSummaryForSalaryByBuiltFacingBox(DateModel model);
        #endregion
    }
}
