﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public interface ILevelServiceWrapper
    {
        Task<List<LevelModel>> GetLevel(SearchModel model);
        Task<List<LevelNCRModel>> GetLevelNCR(SearchModel model);
        Task<List<LevelNGModel>> GetLevelNG(SearchModel model);

        Task<List<LevelGroupModel>> GetMasterLevelGroup(SearchModel model);

        Task<List<ProcessModel>> GetMasterProcess(SearchModel model);

        Task<List<CertModel>> GetMasterCert();

        Task<List<CertListModel>> GetMasterCertList();

        Task<ResultModel> SetMasterCert(CertModel model);

        Task<ResultModel> SetMasterGroup(LevelGroupModel model);

        Task<ResultModel> SetMasterLevel(LevelModel model);

        Task<ResultModel> SetMasterLevelNCR(LevelNCRModel model);


        Task<ResultModel> SetMasterLevelNG(LevelNGModel model);

        Task<ResultModel> SetMasterProcess(ProcessModel model);

        Task<ResultModel> DeleteMasterCert(CertModel model);

        Task<ResultModel> DeleteMasterGroup(LevelGroupModel model);

        Task<ResultModel> DeleteMasterLevel(LevelModel model);

        Task<ResultModel> DeleteMasterLevelNCR(LevelNCRModel model);

        Task<ResultModel> DeleteMasterLevelNG(LevelNGModel model);

        Task<ResultModel> DeleteMasterProcess(ProcessModel model);

        Task<DataFilterModel> GetPriceByProcess(ProcessModel model);

        #region Built Beam/Box by chaiwud.ta
        // 2018-02-10 by chaiwud.ta  
        // Levelbuilt
        Task<List<LevelBuiltModel>> GetBuiltLevel(SearchModel model);
        Task<ResultModel> SetMasterBuiltLevel(LevelBuiltModel model);
        Task<ResultModel> DeleteMasterBuiltLevel(LevelBuiltModel model);
        #endregion
    }
}
