﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
    public  class LeaveAPI
    {
        private static string RoutePrefix = MethodService.LevelService.UrlPrefix;

        public static string GetLevel = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetLevel);

        public static string GetLevelNCR = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetLevelNCR);

        public static string GetLevelNG = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetLevelNG);

        public static string GetMasterLevelGroup = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetMasterLevelGroup);

        public static string GetMasterProcess = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetMasterProcess);

        public static string GetMasterCert = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetMasterCert);

        public static string GetMasterCertList = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetMasterCertList);

        public static string SetMasterCert = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterCert);

        public static string SetMasterGroup = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterGroup);

        public static string SetMasterLevel = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterLevel);

        public static string SetMasterLevelNCR = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterLevelNCR);

        public static string SetMasterLevelNG = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterLevelNG);

        public static string SetMasterProcess = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterProcess);

        public static string DeleteMasterCert = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterCert);

        public static string DeleteMasterGroup = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterGroup);

        public static string DeleteMasterLevel = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterLevel);

        public static string DeleteMasterLevelNCR = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterLevelNCR);

        public static string DeleteMasterLevelNG = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterLevelNG);



        public static string DeleteMasterProcess = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterProcess);


        public static string GetPriceByProcess = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetPriceByProcess);

        #region Built Beam/Box by chaiwud.ta
        // 2018-02-10 by chaiwud.ta 
        public static string GetBuiltLevel = string.Concat(RoutePrefix, "/", MethodService.LevelService.GetBuiltLevel);
        public static string SetMasterBuiltLevel = string.Concat(RoutePrefix, "/", MethodService.LevelService.SetMasterBuiltLevel);
        public static string DeleteMasterBuiltLevel = string.Concat(RoutePrefix, "/", MethodService.LevelService.DeleteMasterBuiltLevel);
        #endregion
    }
}
