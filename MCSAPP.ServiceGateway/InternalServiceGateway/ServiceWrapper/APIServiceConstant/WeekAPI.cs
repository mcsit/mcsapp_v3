﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
     public class WeekAPI
    {
        private static string RoutePrefix = MethodService.WeekService.UrlPrefix;

        public static string GetGroupName = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetGroupName);
        public static string GetSearchUser = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSearchUser);

        public static string GetDays = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetDays);
        public static string GetMaxWeeks = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetMaxWeeks);
        public static string GetHeaderWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetHeaderWeek);

        public static string GetWeekByMonth = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWeekByMonth);
        public static string GetTxnMonthByWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetTxnMonthByWeek);

        public static string GetSummaryByWork = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummaryByWork);
        public static string GetSararyMonthByWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSararyMonthByWeek);
        public static string GetSummaryWorkByCode = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummaryWorkByCode);
        public static string GetSararyMonthByCode = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSararyMonthByCode);

        public static string GetSalaryFab = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSalaryFab);

        public static string GetWorkLessGoal = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWorkLessGoal);

        public static string GetWorkMonth = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWorkMonth);


        public static string GetWorkMonthByDay = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWorkMonthByDay);

        public static string GetWorkByDay = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWorkByDay);

        public static string GetWorkByWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWorkByWeek);

        public static string GetSalaryByDay = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSalaryByDay);

        public static string GetSalaryByWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSalaryByWeek);

        public static string GetSalaryGroupByDay = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSalaryGroupByDay);

        public static string GetSalaryGroupByWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSalaryGroupByWeek);

        public static string GetSalaryByDayAndCode = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSalaryByDayAndCode);

        public static string GetWorkByDayAndCode = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetWorkByDayAndCode);

        public static string GetReviseByDayAndCode = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetReviseByDayAndCode);

        public static string GetOtherByDayAndCode = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetOtherByDayAndCode);


        public static string GetReportNCR = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetReportNCR);

        public static string GetReportVT = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetReportVT);

        public static string GetSummaryWorkDayForWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummaryWorkDayForWeek);
        public static string GetSummarySalaryDayForWeek = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryDayForWeek);
        public static string GetSummaryWorkDayForWeekLess = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummaryWorkDayForWeekLess);
        public static string GetSummaryWorkDayForWeekWeldLess = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummaryWorkDayForWeekWeldLess);
        public static string GetSummarySalaryPlus = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryPlus);
        public static string GetSummarySalaryMinus = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryMinus);
        public static string GetSummarySalaryTotal = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryTotal);

        public static string GetSummaryWorkDayForWeekWeld = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummaryWorkDayForWeekWeld);
        public static string GetSummarySalaryForWeekWeld = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryForWeekWeld);

        public static string GetSummarySalaryForWeekWeldSum = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryForWeekWeldSum);
        public static string GetSummarySalaryForWeekSum = string.Concat(RoutePrefix, "/", MethodService.WeekService.GetSummarySalaryForWeekSum);

    }
}
