﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
    public class WorkAPI
    {
        private static string RoutePrefix = MethodService.WorkService.UrlPrefix;
        public static string GetSummaryForWorkByAutoGas1 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByAutoGas1);
        public static string GetSummaryForWorkByAutoGas2 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByAutoGas2);
        public static string GetSummaryForWorkByAutoGas3 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByAutoGas3);
        public static string GetSummaryForWorkByTaper = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByTaper);
        public static string GetSummaryForWorkByPart = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByPart);

        public static string GetDetailSummaryForWorkByAutoGas1 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByAutoGas1);
        public static string GetDetailSummaryForWorkByAutoGas2 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByAutoGas2);
        public static string GetDetailSummaryForWorkByAutoGas3 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByAutoGas3);
        public static string GetDetailSummaryForWorkByTaper = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByTaper);
        public static string GetDetailSummaryForWorkByPart = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByPart);

        public static string GetDetailForCheckSymbol = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailForCheckSymbol);

        public static string GetSummaryForSalaryByAutoGas1 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByAutoGas1);
        public static string GetSummaryForSalaryByAutoGas2 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByAutoGas2);
        public static string GetSummaryForSalaryByAutoGas3 = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByAutoGas3);
        public static string GetSummaryForSalaryByTaper = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByTaper);
        public static string GetSummaryForSalaryByPart = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByPart);


        public static string GetSummaryForGrinder = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForGrinder);
        public static string GetSummaryForCut = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForCut);

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2

        // Built Beam/Box
        // 2018-01-26 by chaiwud.ta 
        // Built Up Beam
        public static string GetSummaryForWorkByBuiltUpBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltUpBeam);
        public static string GetDetailSummaryForWorkByBuiltUpBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltUpBeam);
        public static string GetSummaryForSalaryByBuiltUpBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltUpBeam);
        // Saw Beam
        public static string GetSummaryForWorkByBuiltSawBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltSawBeam);
        public static string GetDetailSummaryForWorkByBuiltSawBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltSawBeam);
        public static string GetSummaryForSalaryByBuiltSawBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltSawBeam);
        // Adjust Beam
        public static string GetSummaryForWorkByBuiltAdjustBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltAdjustBeam);
        public static string GetDetailSummaryForWorkByBuiltAdjustBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltAdjustBeam);
        public static string GetSummaryForSalaryByBuiltAdjustBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltAdjustBeam);
        // Drill & Shot Blast
        public static string GetSummaryForWorkByBuiltDrillShotBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltDrillShotBeam);
        public static string GetDetailSummaryForWorkByBuiltDrillShotBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltDrillShotBeam);
        public static string GetSummaryForSalaryByBuiltDrillShotBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltDrillShotBeam);

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        public static string GetSummaryForWorkByOtherBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByOtherBeam);
        public static string GetDetailSummaryForWorkByOtherBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByOtherBeam);
        public static string GetSummaryForSalaryByOtherBeam = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByOtherBeam);
        // Other Adjust Box
        public static string GetSummaryForWorkByOtherBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByOtherBox);
        public static string GetDetailSummaryForWorkByOtherBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByOtherBox);
        public static string GetSummaryForSalaryByOtherBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByOtherBox);

        // Other Adjust Finishing
        public static string GetSummaryForWorkByOtherFinishing = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByOtherFinishing);
        public static string GetDetailSummaryForWorkByOtherFinishing = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByOtherFinishing);
        public static string GetSummaryForSalaryByOtherFinishing = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByOtherFinishing);
        #endregion

        // Built Box
        // 2018-02-12 by chaiwud.ta
        // FabDiaphragmBox
        public static string GetSummaryForWorkByBuiltFabDiaphragmBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltFabDiaphragmBox);
        public static string GetDetailSummaryForWorkByBuiltFabDiaphragmBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltFabDiaphragmBox);
        public static string GetSummaryForSalaryByBuiltFabDiaphragmBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltFabDiaphragmBox);
        // BuiltUpBox
        public static string GetSummaryForWorkByBuiltUpBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltUpBox);
        public static string GetDetailSummaryForWorkByBuiltUpBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltUpBox);
        public static string GetSummaryForSalaryByBuiltUpBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltUpBox);
        // AutoRootBox
        public static string GetSummaryForWorkByBuiltAutoRootBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltAutoRootBox);
        public static string GetDetailSummaryForWorkByBuiltAutoRootBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltAutoRootBox);
        public static string GetSummaryForSalaryByBuiltAutoRootBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltAutoRootBox);
        // DrillSesnetBox
        public static string GetSummaryForWorkByBuiltDrillSesnetBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltDrillSesnetBox);
        public static string GetDetailSummaryForWorkByBuiltDrillSesnetBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltDrillSesnetBox);
        public static string GetSummaryForSalaryByBuiltDrillSesnetBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltDrillSesnetBox);
        // GougingRepairBox
        public static string GetSummaryForWorkByBuiltGougingRepairBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltGougingRepairBox);
        public static string GetDetailSummaryForWorkByBuiltGougingRepairBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltGougingRepairBox);
        public static string GetSummaryForSalaryByBuiltGougingRepairBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltGougingRepairBox);
        // FacingBox
        public static string GetSummaryForWorkByBuiltFacingBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForWorkByBuiltFacingBox);
        public static string GetDetailSummaryForWorkByBuiltFacingBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetDetailSummaryForWorkByBuiltFacingBox);
        public static string GetSummaryForSalaryByBuiltFacingBox = string.Concat(RoutePrefix, "/", MethodService.WorkService.GetSummaryForSalaryByBuiltFacingBox);
        #endregion
    }
}
