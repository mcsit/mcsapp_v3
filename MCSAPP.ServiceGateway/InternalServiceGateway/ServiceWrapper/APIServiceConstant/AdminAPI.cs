﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
    public class AdminAPI
    {
        private static string RoutePrefix = MethodService.AdminService.UrlPrefix;

        public static string WriteLogError = string.Concat(RoutePrefix, "/", MethodService.AdminService.WriteLogError);

        public static string WriteLogActivity = string.Concat(RoutePrefix, "/", MethodService.AdminService.WriteLogActivity);

        public static string SetTxnAdjustGoalData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnAdjustGoalData);

        public static string SetTxnNcrData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnNcrData);
        public static string SetNCRDataLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetNCRDataLog);
        public static string DelNCRDetailLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelNCRDetailLog);

        public static string SetTxnVTData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnVTData);

        public static string SetTxnDimensionData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnDimensionData);

        public static string SetTxnOtherData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnOtherData);

        public static string SetTxnMinusData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnMinusData);

        public static string SetTxnWeekData = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetTxnWeekData);

        public static string SetVTDetail = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetVTDetail);

        public static string SetVTDetailLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetVTDetailLog);

        public static string SetVTDetailUpload = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetVTDetailUpload);

        public static string DelVTDetailLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelVTDetailLog);

        public static string GetVTDetailLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetVTDetailLog);
        public static string GetVTDetailAll = string.Concat(RoutePrefix, " / ", MethodService.AdminService.GetVTDetailAll);

        public static string GetReportNCRDetail = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportNCRDetail);
        public static string GetReportNGDetail = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportNGDetail);
        public static string GetNCRLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetNCRLog);

        public static string GetReportVTDetail = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportVTDetail);

        public static string GetWeekLessForAdjust = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetWeekLessForAdjust);

        public static string GetAdjustGoalData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetAdjustGoalData);

        public static string GetCalendar = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetCalendar);

        //public static string GetReportMinusData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportMinusData);

        //public static string GetReportOtherData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportOtherData);

        public static string SetDesingLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetDesingLog);

        public static string SetDesing = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetDesing);

        public static string DelDesingLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelDesingLog);

        public static string GetDesingLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetDesingLog);

        public static string SetUploadOtherLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadOtherLog);

        public static string SetUploadOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadOther);

        public static string DelUploadOtherLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadOtherLog);

        public static string GetUploadOtherLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadOtherLog);

        public static string SetUploadWeekLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadWeekLog);

        public static string SetUploadWeek = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadWeek);

        public static string DelUploadWeekLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadWeekLog);

        public static string GetUploadWeekLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadWeekLog);

        public static string DeleteAdjustGoal = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteAdjustGoal);

        public static string DeleteDesingChange = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteDesingChange);

        public static string DeleteDimension = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteDimension);

        public static string DeleteMinus = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteMinus);

        public static string DeleteOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteOther);

        public static string DeletePlus = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeletePlus);

        public static string DeleteVTData = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteVTData);

        public static string DeleteWeek = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteWeek);


        public static string GetReportDesingChangeData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportDesingChangeData);

        public static string GetReportDimensionData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportDimensionData);

        public static string GetReportMinusData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportMinusData);

        public static string GetReportOtherData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportOtherData);

        public static string GetReportPlusData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportPlusData);

        public static string GetReportVTData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportVTData);

        public static string GetReportWeekData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetReportWeekData);

        public static string SetUploadNGLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadNGLog);

        public static string SetUploadNG = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadNG);

        public static string DelUploadNGLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadNGLog);

        public static string GetUploadNGLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadNGLog);

        public static string GetUploadNG = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadNG);

        public static string DelUploadNG = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadNG);

        public static string UpdateUploadNG = string.Concat(RoutePrefix, "/", MethodService.AdminService.UpdateUploadNG);

        public static string SetUploadActUTLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadActUTLog);

        public static string SetUploadActUT = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadActUT);

        public static string DelUploadActUTLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadActUTLog);

        public static string DelUploadActUT = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadActUT);

        public static string GetUploadActUTLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadActUTLog);

        public static string GetUploadActUT = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadActUT);

        public static string SetUploadCutFinisingLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadCutFinisingLog);

        public static string SetUploadCutFinising = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadCutFinising);

        public static string DelUploadCutFinisingLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadCutFinisingLog);

        public static string DelUploadCutFinising = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadCutFinising);

        public static string GetUploadCutFinisingLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadCutFinisingLog);

        public static string GetUploadCutFinising = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadCutFinising);

        public static string SetUploadGoalLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadGoalLog);

        public static string SetUploadGoal = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadGoal);

        public static string DelUploadGoalLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadGoalLog);

        public static string DelUploadGoal = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadGoal);

        public static string GetUploadGoalLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadGoalLog);

        public static string GetUploadGoal = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadGoal);


        public static string SetUploadNGNewLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadNGNewLog);

        public static string SetUploadNGNew = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadNGNew);

        public static string DelUploadNGNewLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadNGNewLog);

        public static string DelUploadNGNew = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadNGNew);

        public static string GetUploadNGNewLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadNGNewLog);

        public static string GetUploadNGNew = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadNGNew);



        public static string SetUploadActUTNewLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadActUTNewLog);

        public static string SetUploadActUTNew = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadActUTNew);

        public static string DelUploadActUTNewLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadActUTNewLog);

        public static string DelUploadActUTNew = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadActUTNew);

        public static string GetUploadActUTNewLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadActUTNewLog);

        public static string GetUploadActUTNew = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadActUTNew);

        public static string GetHeaderExcel = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetHeaderExcel);

        public static string SetUploadEndTabLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadEndTabLog);

        public static string SetUploadEndTab = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadEndTab);

        public static string DelUploadEndTabLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadEndTabLog);

        public static string DelUploadEndTab = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadEndTab);

        public static string GetUploadEndTabLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadEndTabLog);

        public static string GetUploadEndTab = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadEndTab);

        public static string SetUploadCutPartLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadCutPartLog);

        public static string SetUploadCutPart = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadCutPart);

        public static string DelUploadCutPartLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadCutPartLog);

        public static string DelUploadCutPart = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadCutPart);

        public static string GetUploadCutPartLog = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadCutPartLog);

        public static string GetUploadCutPart = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadCutPart);


        public static string GetMasterProcessType = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetMasterProcessType);

        public static string SetMasterProcessType = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetMasterProcessType);

        public static string DeleteMasterProcessType = string.Concat(RoutePrefix, "/", MethodService.AdminService.DeleteMasterProcessType);


        public static string SetCheckSymbol = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetCheckSymbol);

        public static string DelCheckSymbolData = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelCheckSymbolData);

        public static string GetDetailForCheckSymbolData = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetDetailForCheckSymbolData);

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        // 2018-02-12 by chaiwud.ta
        public static string SetUploadGoalLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadGoalLogBuilt);
        public static string SetUploadGoalBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadGoalBuilt);
        public static string DelUploadGoalLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadGoalLogBuilt);
        public static string DelUploadGoalBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadGoalBuilt);
        public static string GetUploadGoalLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadGoalLogBuilt);
        public static string GetUploadGoalBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadGoalBuilt);

        // UTRepairCheck
        // 2018-03-01 by chaiwud.ta
        public static string SetUploadUTRepairCheckLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadUTRepairCheckLogBuilt);
        public static string SetUploadUTRepairCheckBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadUTRepairCheckBuilt);
        public static string DelUploadUTRepairCheckLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadUTRepairCheckLogBuilt);
        public static string DelUploadUTRepairCheckBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadUTRepairCheckBuilt);
        public static string GetUploadUTRepairCheckLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadUTRepairCheckLogBuilt);
        public static string GetUploadUTRepairCheckBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadUTRepairCheckBuilt);

        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        public static string SetUploadUTDataByUserLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadUTDataByUserLogBuilt);
        public static string SetUploadUTDataByUserBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadUTDataByUserBuilt);
        public static string DelUploadUTDataByUserLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadUTDataByUserLogBuilt);
        public static string DelUploadUTDataByUserBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadUTDataByUserBuilt);
        public static string GetUploadUTDataByUserLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadUTDataByUserLogBuilt);
        public static string GetUploadUTDataByUserBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadUTDataByUserBuilt);

        // NGDataBuilt
        public static string SetUploadNGDataLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadNGDataLogBuilt);
        public static string SetUploadNGDataBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadNGDataBuilt);
        public static string DelUploadNGDataLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadNGDataLogBuilt);
        public static string DelUploadNGDataBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadNGDataBuilt);
        public static string GetUploadNGDataLogBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadNGDataLogBuilt);
        public static string GetUploadNGDataBuilt = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadNGDataBuilt);


        // OtherCamber
        // 2018-04-23 by chaiwud.ta
        public static string SetUploadCamberDataLogOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadCamberDataLogOther);
        public static string SetUploadCamberDataOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.SetUploadCamberDataOther);
        public static string DelUploadCamberDataLogOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadCamberDataLogOther);
        public static string DelUploadCamberDataOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.DelUploadCamberDataOther);
        public static string GetUploadCamberDataLogOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadCamberDataLogOther);
        public static string GetUploadCamberDataOther = string.Concat(RoutePrefix, "/", MethodService.AdminService.GetUploadCamberDataOther);
        #endregion

    }
}
