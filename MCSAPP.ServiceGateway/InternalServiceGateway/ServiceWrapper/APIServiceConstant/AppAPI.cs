﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
    public class AppAPI
    {
        private static string RoutePrefix = MethodService.AppService.UrlPrefix;

        public static string GetPartDetail = string.Concat(RoutePrefix, "/", MethodService.AppService.GetPartDetail);

        public static string GetOtherDetail = string.Concat(RoutePrefix, "/", MethodService.AppService.GetOtherDetail);

        public static string GetVTDetail = string.Concat(RoutePrefix, "/", MethodService.AppService.GetVTDetail);

        public static string GetVTForAdd = string.Concat(RoutePrefix, "/", MethodService.AppService.GetVTForAdd);

        public static string GetHeaderExcel = string.Concat(RoutePrefix, "/", MethodService.AppService.GetHeaderExcel);

    }
}
