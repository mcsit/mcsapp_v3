﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
   public  class UserDepartmentAPI
    {
        private static string RoutePrefix = MethodService.UserDepartmentService.UrlPrefix;

       // public static string GetGroupName = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetGroupName);

        private static string RouteReportPrefix = MethodService.WeekService.UrlPrefix;

        public static string GetGroupName = string.Concat(RouteReportPrefix, "/", MethodService.UserDepartmentService.GetGroupName);

        public static string GetMainType = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetMainType);

        public static string GetLevelType = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetLevelType);

        public static string GetMainTypeForProcess = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetMainTypeForProcess);

        public static string GetHeaderForProcess = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetHeaderForProcess);

        public static string GetHeaderWeek = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetHeaderWeek);

        public static string GetProjectForCheckSymbol = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetProjectForCheckSymbol);

        public static string GetProjectItemForCheckSymbol = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetProjectItemForCheckSymbol);

        public static string GetProjectForCheckSymbolData = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetProjectForCheckSymbolData);

        public static string GetProjectItemForCheckSymbolData = string.Concat(RoutePrefix, "/", MethodService.UserDepartmentService.GetProjectItemForCheckSymbolData);


    }
}
