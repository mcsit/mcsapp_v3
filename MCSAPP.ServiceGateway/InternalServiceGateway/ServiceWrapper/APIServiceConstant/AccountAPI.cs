﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant
{
    public class AccountAPI
    {
        private static string RoutePrefix = MethodService.AccountService.UrlPrefix;

        public static string GetLogin = string.Concat(RoutePrefix, "/", MethodService.AccountService.GetLogin);

    }
}
