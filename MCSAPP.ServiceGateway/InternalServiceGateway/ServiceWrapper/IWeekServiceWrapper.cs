﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public interface IWeekServiceWrapper
    {

        Task<List<DepartmentModel>> GetGroupName(SearchModel model);
        Task<List<UserModel>> GetSearchUser(SearchModel model);

        Task<List<WeekModel>> GetWeekByMonth(DateModel model);

        Task<List<GoalModel>> GetTxnMonthByWeek(DateModel model);

        Task<List<GoalSumModel>> GetSummaryByWork(DateModel model);

        Task<List<SararyModel>> GetSararyMonthByWeek(DateModel model);

        Task<GoalHeaderModel> GetSummaryWorkByCode(DateModel model);

        Task<List<SararyModel>> GetSararyMonthByCode(DateModel model);

        Task<List<SumSalaryModel>> GetSalaryFab(DateModel model);

        Task<DayHeaderModel> GetWorkLessGoal(DateModel model);

        Task<List<WorkModel>> GetWorkMonth(DateModel model);

        Task<List<WorkModel>> GetWorkMonthByDay(DateModel model);

        Task<DayHeaderModel> GetWorkByDay(DateModel model);

        Task<WeekHeaderModel> GetWorkByWeek(DateModel model);

        Task<DayHeaderModel> GetSalaryByDay(DateModel model);

        Task<WeekHeaderModel> GetSalaryByWeek(DateModel model);

        Task<DayHeaderModel> GetSalaryGroupByDay(DateModel model);

        Task<WeekHeaderModel> GetSalaryGroupByWeek(DateModel model);

        Task<DayHeaderModel> GetWorkByDayAndCode(DateModel model);

        Task<DayHeaderModel> GetSalaryByDayAndCode(DateModel model);

        Task<List<ReviseModel>> GetReviseByDayAndCode(DateModel model);

        Task<List<ReviseModel>> GetOtherByDayAndCode(DateModel model);

        Task<NCRHeaderModel> GetReportNCR(DateModel model);

        Task<NCRHeaderModel> GetReportVT(DateModel model);

        Task<GoalHeaderModel> GetSummaryWorkDayForWeek(DateModel model);

        Task<DataFilterModel> GetMaxWeeks(DateModel model);

        Task<List<DataValueModel>> GetHeaderWeek(DateModel model);
        Task<List<SalarySummaryModel>> GetSummarySalaryDayForWeek(DateModel model);
        Task<GoalHeaderModel> GetSummaryWorkDayForWeekLess(DateModel model);
        Task<GoalHeaderModel> GetSummaryWorkDayForWeekWeldLess(DateModel model);

        Task<List<PlusModel>> GetSummarySalaryPlus(DateModel model);
        Task<List<PlusModel>> GetSummarySalaryMinus(DateModel model);
        Task<List<PlusModel>> GetSummarySalaryTotal(DateModel model);
        //Task<List<DataFilterModel>> GetDays(DateModel model);

        Task<SummaryWorkHeaderModel> GetSummaryWorkDayForWeekWeld(DateModel model);
        Task<SummarySalaryHeaderModel> GetSummarySalaryForWeekWeld(DateModel model);

        Task<SummarySalaryHeaderModel> GetSummarySalaryForWeekWeldSum(DateModel model);
        Task<SummarySalaryHeaderModel> GetSummarySalaryForWeekSum(DateModel model);
    }
}
