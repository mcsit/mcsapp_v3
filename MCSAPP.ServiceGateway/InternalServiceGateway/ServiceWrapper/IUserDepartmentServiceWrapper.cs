﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public interface IUserDepartmentServiceWrapper
    {
        Task<List<DataValueModel>> GetMainType(SearchModel model);
        Task<List<DataValueModel>> GetLevelType(SearchModel model);
        Task<List<DataValueModel>> GetMainTypeForProcess(SearchModel model);
        Task<List<DataValueModel>> GetHeaderForProcess(SearchModel model);

        Task<List<DataValueModel>> GetHeaderWeek(DateModel model);
        Task<List<DepartmentModel>> GetGroupName(SearchModel model);

        Task<List<DataValueModel>> GetProjectForCheckSymbol(DateModel model);
        Task<List<DataValueModel>> GetProjectItemForCheckSymbol(DateModel model);

        Task<List<DataValueModel>> GetProjectForCheckSymbolData(DateModel model);
        Task<List<DataValueModel>> GetProjectItemForCheckSymbolData(DateModel model);
    }
}
