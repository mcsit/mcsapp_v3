﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public class AccountServiceWrapper : IAccountServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public AccountServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        //public async Task<UserAccountModel> GetLogin(UserLoginModel model)
        //{
        //    var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UserLoginModel, UserAccountModel>(AccountAPI.GetLogin, model);
        //    return result.Data;
        //}
        public async Task<DataVerifyModel<UserAccountModel>> GetLogin(UserLoginModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UserLoginModel, UserAccountModel>(AccountAPI.GetLogin, model);
            return result;
        }
    }
}
