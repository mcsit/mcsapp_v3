﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public interface IWorkServiceWrapper
    {
        Task<AllSummaryForWorkModel> GetSummaryForWorkByAutoGas1(DateModel model);
        Task<AllSummaryForWorkModel> GetSummaryForWorkByAutoGas2(DateModel model);
        Task<AllSummaryForWorkModel> GetSummaryForWorkByAutoGas3(DateModel model);
        Task<AllSummaryForWorkModel> GetSummaryForWorkByTaper(DateModel model);
        Task<AllSummaryForWorkModel> GetSummaryForWorkByPart(DateModel model);

        Task<PartDetailModel> GetDetailSummaryForWorkByAutoGas1(DateModel model);
        Task<PartDetailModel> GetDetailSummaryForWorkByAutoGas2(DateModel model);
        Task<PartDetailModel> GetDetailSummaryForWorkByAutoGas3(DateModel model);
        Task<PartDetailModel> GetDetailSummaryForWorkByTaper(DateModel model);
        Task<PartDetailModel> GetDetailSummaryForWorkByPart(DateModel model);

        Task<PartDetailModel> GetDetailForCheckSymbol(DateModel model);

        Task<AllSummaryForSalaryModel> GetSummaryForSalaryByAutoGas1(DateModel model);
        Task<AllSummaryForSalaryModel> GetSummaryForSalaryByAutoGas2(DateModel model);
        Task<AllSummaryForSalaryModel> GetSummaryForSalaryByAutoGas3(DateModel model);
        Task<AllSummaryForSalaryModel> GetSummaryForSalaryByTaper(DateModel model);
        Task<AllSummaryForSalaryModel> GetSummaryForSalaryByPart(DateModel model);

        Task<AllShowSummaryModel> GetSummaryForGrinder(DateModel model);
        Task<AllShowSummaryModel> GetSummaryForCut(DateModel model);

        #region Built Beam/Box by chaiwud.ta
        // #CBB1 #CBB2

        // Built Beam/Box
        // 2018-01-26 by chaiwud.ta
        // Built Up Beam
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltUpBeam(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltUpBeam(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltUpBeam(DateModel model);
        // Saw Beam
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltSawBeam(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltSawBeam(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltSawBeam(DateModel model);
        // Adjust Beam
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltAdjustBeam(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltAdjustBeam(DateModel model);
        // Drill & Shot Blast
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltDrillShotBeam(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model);
        // 2018-02-01

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByOtherBeam(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByOtherBeam(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByOtherBeam(DateModel model);
        // Other Adjust Box
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByOtherBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByOtherBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByOtherBox(DateModel model);

        // Other Finishing
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByOtherFinishing(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByOtherFinishing(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByOtherFinishing(DateModel model);
        #endregion

        // Built Box
        // 2018-02-12 by chaiwud.ta 

        // FabDiaphragmBox
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model);
        // BuiltUpBox
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltUpBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltUpBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltUpBox(DateModel model);
        // AutoRootBox
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltAutoRootBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltAutoRootBox(DateModel model);
        // DrillSesnetBox
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model);
        // GougingRepairBox 
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltGougingRepairBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model);
        // FacingBox
        Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltFacingBox(DateModel model);
        Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltFacingBox(DateModel model);
        Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltFacingBox(DateModel model);
        #endregion
    }
}
