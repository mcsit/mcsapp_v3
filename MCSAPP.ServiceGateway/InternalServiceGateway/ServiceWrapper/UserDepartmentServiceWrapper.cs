﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public  class UserDepartmentServiceWrapper :IUserDepartmentServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public UserDepartmentServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        public async Task<List<DataValueModel>> GetMainType(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<DataValueModel>>(UserDepartmentAPI.GetMainType, model);
            return result.Data;
        }

        public async Task<List<DataValueModel>> GetLevelType(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<DataValueModel>>(UserDepartmentAPI.GetLevelType, model);
            return result.Data;
        }

        public async Task<List<DataValueModel>> GetMainTypeForProcess(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<DataValueModel>>(UserDepartmentAPI.GetMainTypeForProcess, model);
            return result.Data;
        }
        public async Task<List<DataValueModel>> GetHeaderForProcess(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<DataValueModel>>(UserDepartmentAPI.GetHeaderForProcess, model);
            return result.Data;
        }
        public async Task<List<DataValueModel>> GetHeaderWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataValueModel>>(UserDepartmentAPI.GetHeaderWeek, model);
            return result.Data;
        }

        public async Task<List<DepartmentModel>> GetGroupName(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<DepartmentModel>>(UserDepartmentAPI.GetGroupName, model);
            return result.Data;
        }

        public async Task<List<DataValueModel>> GetProjectForCheckSymbol(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataValueModel>>(UserDepartmentAPI.GetProjectForCheckSymbol, model);
            return result.Data;
        }

        public async Task<List<DataValueModel>> GetProjectItemForCheckSymbol(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataValueModel>>(UserDepartmentAPI.GetProjectItemForCheckSymbol, model);
            return result.Data;
        }

        public async Task<List<DataValueModel>> GetProjectForCheckSymbolData(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataValueModel>>(UserDepartmentAPI.GetProjectForCheckSymbolData, model);
            return result.Data;
        }

        public async Task<List<DataValueModel>> GetProjectItemForCheckSymbolData(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataValueModel>>(UserDepartmentAPI.GetProjectItemForCheckSymbolData, model);
            return result.Data;
        }
    }
}
