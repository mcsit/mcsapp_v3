﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public class WorkServiceWrapper : IWorkServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public WorkServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        public async Task<AllSummaryForWorkModel> GetSummaryForWorkByAutoGas1(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkModel>(WorkAPI.GetSummaryForWorkByAutoGas1, model);
            return result.Data;
        }
        public async Task<AllSummaryForWorkModel> GetSummaryForWorkByAutoGas2(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkModel>(WorkAPI.GetSummaryForWorkByAutoGas2, model);
            return result.Data;
        }
        public async Task<AllSummaryForWorkModel> GetSummaryForWorkByAutoGas3(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkModel>(WorkAPI.GetSummaryForWorkByAutoGas3, model);
            return result.Data;
        }
        public async Task<AllSummaryForWorkModel> GetSummaryForWorkByTaper(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkModel>(WorkAPI.GetSummaryForWorkByTaper, model);
            return result.Data;
        }
        public async Task<AllSummaryForWorkModel> GetSummaryForWorkByPart(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkModel>(WorkAPI.GetSummaryForWorkByPart, model);
            return result.Data;
        }

        public async Task<PartDetailModel> GetDetailSummaryForWorkByAutoGas1(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(WorkAPI.GetDetailSummaryForWorkByAutoGas1, model);
            return result.Data;
        }

        public async Task<PartDetailModel> GetDetailSummaryForWorkByAutoGas2(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(WorkAPI.GetDetailSummaryForWorkByAutoGas2, model);
            return result.Data;
        }

        public async Task<PartDetailModel> GetDetailSummaryForWorkByAutoGas3(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(WorkAPI.GetDetailSummaryForWorkByAutoGas3, model);
            return result.Data;
        }

        public async Task<PartDetailModel> GetDetailSummaryForWorkByTaper(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(WorkAPI.GetDetailSummaryForWorkByTaper, model);
            return result.Data;
        }

        public async Task<PartDetailModel> GetDetailSummaryForWorkByPart(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(WorkAPI.GetDetailSummaryForWorkByPart, model);
            return result.Data;
        }

        public async Task<PartDetailModel> GetDetailForCheckSymbol(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(WorkAPI.GetDetailForCheckSymbol, model);
            return result.Data;
        }

        public async Task<AllSummaryForSalaryModel> GetSummaryForSalaryByAutoGas1(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryModel>(WorkAPI.GetSummaryForSalaryByAutoGas1, model);
            return result.Data;
        }

        public async Task<AllSummaryForSalaryModel> GetSummaryForSalaryByAutoGas2(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryModel>(WorkAPI.GetSummaryForSalaryByAutoGas2, model);
            return result.Data;
        }

        public async Task<AllSummaryForSalaryModel> GetSummaryForSalaryByAutoGas3(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryModel>(WorkAPI.GetSummaryForSalaryByAutoGas3, model);
            return result.Data;
        }

        public async Task<AllSummaryForSalaryModel> GetSummaryForSalaryByTaper(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryModel>(WorkAPI.GetSummaryForSalaryByTaper, model);
            return result.Data;
        }

        public async Task<AllSummaryForSalaryModel> GetSummaryForSalaryByPart(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryModel>(WorkAPI.GetSummaryForSalaryByPart, model);
            return result.Data;
        }

        public async Task<AllShowSummaryModel> GetSummaryForGrinder(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllShowSummaryModel>(WorkAPI.GetSummaryForGrinder, model);
            return result.Data;
        }

        public async Task<AllShowSummaryModel> GetSummaryForCut(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllShowSummaryModel>(WorkAPI.GetSummaryForCut, model);
            return result.Data;
        }

        #region Built Beam/Box by chaiwud.ta
        // #CBB1

        // 2018-02-01 by chaiwud.ta 
        // Built Beam
        // Built Up Beam
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltUpBeam, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltUpBeam, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltUpBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltUpBeam, model);
            return result.Data;
        }
        // Saw
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltSawBeam, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltSawBeam, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltSawBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltSawBeam, model);
            return result.Data;
        }
        // Adjust
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltAdjustBeam, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltAdjustBeam, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltAdjustBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltAdjustBeam, model);
            return result.Data;
        }
        // Drill & Shot
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltDrillShotBeam, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltDrillShotBeam, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltDrillShotBeam, model);
            return result.Data;
        }

        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByOtherBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByOtherBeam, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByOtherBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByOtherBeam, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByOtherBeam(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByOtherBeam, model);
            return result.Data;
        }
        // Other Adjust Box
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByOtherBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByOtherBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByOtherBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByOtherBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByOtherBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByOtherBox, model);
            return result.Data;
        }

        // Other Finishing
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByOtherFinishing(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByOtherFinishing, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByOtherFinishing(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByOtherFinishing, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByOtherFinishing(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByOtherFinishing, model);
            return result.Data;
        }
        #endregion


        // Built Box
        // 2018-02-12 by chaiwud.ta

        // FabDiaphragmBox
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltFabDiaphragmBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltFabDiaphragmBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltFabDiaphragmBox, model);
            return result.Data;
        }
        // BuiltUpBox
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltUpBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltUpBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltUpBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltUpBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltUpBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltUpBox, model);
            return result.Data;
        }
        // AutoRootBox
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltAutoRootBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltAutoRootBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltAutoRootBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltAutoRootBox, model);
            return result.Data;
        }
        // DrillSesnetBox
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltDrillSesnetBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltDrillSesnetBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltDrillSesnetBox, model);
            return result.Data;
        }
        // GougingRepairBox
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltGougingRepairBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltGougingRepairBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltGougingRepairBox, model);
            return result.Data;
        }
        // FacingBox
        public async Task<AllSummaryForWorkBuiltModel> GetSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForWorkBuiltModel>(WorkAPI.GetSummaryForWorkByBuiltFacingBox, model);
            return result.Data;
        }
        public async Task<BuiltDetailModel> GetDetailSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, BuiltDetailModel>(WorkAPI.GetDetailSummaryForWorkByBuiltFacingBox, model);
            return result.Data;
        }
        public async Task<AllSummaryForSalaryBuiltModel> GetSummaryForSalaryByBuiltFacingBox(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, AllSummaryForSalaryBuiltModel>(WorkAPI.GetSummaryForSalaryByBuiltFacingBox, model);
            return result.Data;
        }
        #endregion
    }
}
