﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public class AdminServiceWrapper : IAdminServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public AdminServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        public void WriteLogError(LogModel model)
        {
             httpClientServiceAPI.PostToApiServiceVoidRespond<LogModel>(AdminAPI.WriteLogError, model);
        }
        public void WriteLogActivity(LogModel model)
        {
            httpClientServiceAPI.PostToApiServiceVoidRespond<LogModel>(AdminAPI.WriteLogActivity, model);
        }
        public async Task<ResultModel> SetTxnAdjustGoalData(AdjustGoalModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<AdjustGoalModel, ResultModel>(AdminAPI.SetTxnAdjustGoalData, model);
            return result.Data;
        }
        public async Task<ResultModel> SetTxnNcrData(List<NcrDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NcrDataModel>, ResultModel>(AdminAPI.SetTxnNcrData, model);
            return result.Data;
        }
        public async Task<ResultModel> SetNCRDataLog(List<NcrDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NcrDataModel>, ResultModel>(AdminAPI.SetNCRDataLog, model);
            return result.Data;
        }
        public async Task<ResultModel> DelNCRDetailLog(List<NcrDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NcrDataModel>, ResultModel>(AdminAPI.DelNCRDetailLog, model);
            return result.Data;
        }
        public async Task<ResultModel> SetTxnVTData(List<VTDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<VTDataModel>, ResultModel>(AdminAPI.SetTxnVTData, model);
            return result.Data;
        }
        public async Task<ResultModel> SetTxnDimensionData(DimDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DimDataModel, ResultModel>(AdminAPI.SetTxnDimensionData, model);
            return result.Data;
        }
        public async Task<ResultModel> SetTxnOtherData(OtherDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<OtherDataModel, ResultModel>(AdminAPI.SetTxnOtherData, model);
            return result.Data;
        }
        public async Task<ResultModel> SetTxnMinusData(MinusDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<MinusDataModel, ResultModel>(AdminAPI.SetTxnMinusData, model);
            return result.Data;
        }
        public async Task<ResultModel> SetTxnWeekData(WeekDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<WeekDataModel, ResultModel>(AdminAPI.SetTxnWeekData, model);
            return result.Data;
        }

        public async Task<ResultModel> SetVTDetail(List<VTListModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<VTListModel>, ResultModel>(AdminAPI.SetVTDetail, model);
            return result.Data;
        }

        public async Task<ResultModel> SetVTDetailLog(List<VTListModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<VTListModel>, ResultModel>(AdminAPI.SetVTDetailLog, model);
            return result.Data;
        }

        public async Task<ResultModel> SetVTDetailUpload(List<VTListModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<VTListModel>, ResultModel>(AdminAPI.SetVTDetailUpload, model);
            return result.Data;
        }

        public async Task<ResultModel> DelVTDetailLog(List<VTListModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<VTListModel>, ResultModel>(AdminAPI.DelVTDetailLog, model);
            return result.Data;
        }


        public async Task<List<VTListModel>> GetVTDetailLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<VTListModel>>(AdminAPI.GetVTDetailLog, model);
            return result.Data;
        }
        public async Task<List<VTListModel>> GetVTDetailAll(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<VTListModel>>(AdminAPI.GetVTDetailAll, model);
            return result.Data;
        }
        public async Task<NCRDetailHeaderModel> GetReportNCRDetail(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, NCRDetailHeaderModel>(AdminAPI.GetReportNCRDetail, model);
            return result.Data;
        }
        public async Task<NGDetailHeaderModel> GetReportNGDetail(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, NGDetailHeaderModel>(AdminAPI.GetReportNGDetail, model);
            return result.Data;
        }
        public async Task<List<NcrDataModel>> GetNCRLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<NcrDataModel>>(AdminAPI.GetNCRLog, model);
            return result.Data;
        }
        public async Task<VTDetailHeaderModel> GetReportVTDetail(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, VTDetailHeaderModel>(AdminAPI.GetReportVTDetail, model);
            return result.Data;
        }

        public async Task<GoalSumModel> GetWeekLessForAdjust(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, GoalSumModel>(AdminAPI.GetWeekLessForAdjust, model);
            return result.Data;
        }

        public async Task<List<GoalSumModel>> GetAdjustGoalData(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<GoalSumModel>>(AdminAPI.GetAdjustGoalData, model);
            return result.Data;
        }
        public async Task<List<MonthModel>> GetCalendar(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<MonthModel>>(AdminAPI.GetCalendar, model);
            return result.Data;
        }

        //public async Task<List<MinusDataModel>> GetReportMinusData(ReportSearchModel model)
        //{
        //    var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<MinusDataModel>>(AdminAPI.GetReportMinusData, model);
        //    return result.Data;
        //}

        //public async Task<List<OtherDataModel>> GetReportOtherData(ReportSearchModel model)
        //{
        //    var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<OtherDataModel>>(AdminAPI.GetReportOtherData, model);
        //    return result.Data;
        //}

        public async Task<ResultModel> SetDesing(List<DesingModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<DesingModel>, ResultModel>(AdminAPI.SetDesing, model);
            return result.Data;
        }

        public async Task<ResultModel> SetDesingLog(List<DesingModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<DesingModel>, ResultModel>(AdminAPI.SetDesingLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelDesingLog(List<DesingModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<DesingModel>, ResultModel>(AdminAPI.DelDesingLog, model);
            return result.Data;
        }


        public async Task<List<DesingModel>> GetDesingLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DesingModel>>(AdminAPI.GetDesingLog, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadOther(List<OtherModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<OtherModel>, ResultModel>(AdminAPI.SetUploadOther, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadOtherLog(List<OtherModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<OtherModel>, ResultModel>(AdminAPI.SetUploadOtherLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadOtherLog(List<OtherModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<OtherModel>, ResultModel>(AdminAPI.DelUploadOtherLog, model);
            return result.Data;
        }


        public async Task<List<OtherModel>> GetUploadOtherLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<OtherModel>>(AdminAPI.GetUploadOtherLog, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadWeek(List<UploadWeekModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadWeekModel>, ResultModel>(AdminAPI.SetUploadWeek, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadWeekLog(List<UploadWeekModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadWeekModel>, ResultModel>(AdminAPI.SetUploadWeekLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadWeekLog(List<UploadWeekModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadWeekModel>, ResultModel>(AdminAPI.DelUploadWeekLog, model);
            return result.Data;
        }


        public async Task<List<UploadWeekModel>> GetUploadWeekLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadWeekModel>>(AdminAPI.GetUploadWeekLog, model);
            return result.Data;
        }


        //Delete
        public async Task<ResultModel> DeleteAdjustGoal(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteAdjustGoal, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteDesingChange(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteDesingChange, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteDimension(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteDimension, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteMinus(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteMinus, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteOther(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteOther, model);
            return result.Data;
        }
        public async Task<ResultModel> DeletePlus(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeletePlus, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteVTData(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteVTData, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteWeek(DeleteModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DeleteModel, ResultModel>(AdminAPI.DeleteWeek, model);
            return result.Data;
        }

        public async Task<List<ReportDesignChangeModel>> GetReportDesingChangeData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<ReportDesignChangeModel>>(AdminAPI.GetReportDesingChangeData, model);
            return result.Data;
        }

        public async Task<List<ReportDimensionModel>> GetReportDimensionData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<ReportDimensionModel>>(AdminAPI.GetReportDimensionData, model);
            return result.Data;
        }

        public async Task<List<ReportMinusModel>> GetReportMinusData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<ReportMinusModel>>(AdminAPI.GetReportMinusData, model);
            return result.Data;
        }

        public async Task<List<ReportDimensionModel>> GetReportOtherData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<ReportDimensionModel>>(AdminAPI.GetReportOtherData, model);
            return result.Data;
        }

        public async Task<List<ReportDimensionModel>> GetReportPlusData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<ReportDimensionModel>>(AdminAPI.GetReportPlusData, model);
            return result.Data;
        }

        public async Task<List<VTListModel>> GetReportVTData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<VTListModel>>(AdminAPI.GetReportVTData, model);
            return result.Data;
        }

        public async Task<List<UploadWeekModel>> GetReportWeekData(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<UploadWeekModel>>(AdminAPI.GetReportWeekData, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadNG(List<NGDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModel>, ResultModel>(AdminAPI.SetUploadNG, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadNGLog(List<NGDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModel>, ResultModel>(AdminAPI.SetUploadNGLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadNGLog(List<NGDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModel>, ResultModel>(AdminAPI.DelUploadNGLog, model);
            return result.Data;
        }


        public async Task<List<NGDataModel>> GetUploadNGLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<NGDataModel>>(AdminAPI.GetUploadNGLog, model);
            return result.Data;
        }
        public async Task<List<NGDataModel>> GetUploadNG(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<NGDataModel>>(AdminAPI.GetUploadNG, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadNG(NGDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<NGDataModel, ResultModel>(AdminAPI.DelUploadNG, model);
            return result.Data;
        }
        public async Task<ResultModel> UpdateUploadNG(NGDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<NGDataModel, ResultModel>(AdminAPI.UpdateUploadNG, model);
            return result.Data;
        }


        public async Task<ResultModel> SetUploadActUT(List<UploadActUTModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadActUTModel>, ResultModel>(AdminAPI.SetUploadActUT, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadActUTLog(List<UploadActUTModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadActUTModel>, ResultModel>(AdminAPI.SetUploadActUTLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadActUTLog(List<UploadActUTModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadActUTModel>, ResultModel>(AdminAPI.DelUploadActUTLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadActUT(UploadActUTModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadActUTModel, ResultModel>(AdminAPI.DelUploadActUT, model);
            return result.Data;
        }

        public async Task<List<UploadActUTModel>> GetUploadActUTLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadActUTModel>>(AdminAPI.GetUploadActUTLog, model);
            return result.Data;
        }
        public async Task<List<UploadActUTModel>> GetUploadActUT(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadActUTModel>>(AdminAPI.GetUploadActUT, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadCutFinising(List<UploadCutFinisingModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadCutFinisingModel>, ResultModel>(AdminAPI.SetUploadCutFinising, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadCutFinisingModel>, ResultModel>(AdminAPI.SetUploadCutFinisingLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadCutFinisingModel>, ResultModel>(AdminAPI.DelUploadCutFinisingLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadCutFinising(UploadCutFinisingModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadCutFinisingModel, ResultModel>(AdminAPI.DelUploadCutFinising, model);
            return result.Data;
        }

        public async Task<List<UploadCutFinisingModel>> GetUploadCutFinisingLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadCutFinisingModel>>(AdminAPI.GetUploadCutFinisingLog, model);
            return result.Data;
        }
        public async Task<List<UploadCutFinisingModel>> GetUploadCutFinising(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadCutFinisingModel>>(AdminAPI.GetUploadCutFinising, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadGoal(List<UploadGoalModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadGoalModel>, ResultModel>(AdminAPI.SetUploadGoal, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadGoalLog(List<UploadGoalModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadGoalModel>, ResultModel>(AdminAPI.SetUploadGoalLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadGoalLog(List<UploadGoalModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadGoalModel>, ResultModel>(AdminAPI.DelUploadGoalLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadGoal(UploadGoalModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadGoalModel, ResultModel>(AdminAPI.DelUploadGoal, model);
            return result.Data;
        }

        public async Task<List<UploadGoalModel>> GetUploadGoalLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadGoalModel>>(AdminAPI.GetUploadGoalLog, model);
            return result.Data;
        }
        public async Task<List<UploadGoalModel>> GetUploadGoal(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadGoalModel>>(AdminAPI.GetUploadGoal, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadNGNew(List<NGDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModel>, ResultModel>(AdminAPI.SetUploadNGNew, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadNGNewLog(List<NGDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModel>, ResultModel>(AdminAPI.SetUploadNGNewLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadNGNewLog(List<NGDataModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModel>, ResultModel>(AdminAPI.DelUploadNGNewLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadNGNew(NGDataModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<NGDataModel, ResultModel>(AdminAPI.DelUploadNGNew, model);
            return result.Data;
        }

        public async Task<List<NGDataModel>> GetUploadNGNewLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<NGDataModel>>(AdminAPI.GetUploadNGNewLog, model);
            return result.Data;
        }
        public async Task<List<NGDataModel>> GetUploadNGNew(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<NGDataModel>>(AdminAPI.GetUploadNGNew, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadActUTNew(List<UploadActUTModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadActUTModel>, ResultModel>(AdminAPI.SetUploadActUTNew, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadActUTNewLog(List<UploadActUTModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadActUTModel>, ResultModel>(AdminAPI.SetUploadActUTNewLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadActUTNewLog(List<UploadActUTModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadActUTModel>, ResultModel>(AdminAPI.DelUploadActUTNewLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadActUTNew(UploadActUTModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadActUTModel, ResultModel>(AdminAPI.DelUploadActUTNew, model);
            return result.Data;
        }

        public async Task<List<UploadActUTModel>> GetUploadActUTNewLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadActUTModel>>(AdminAPI.GetUploadActUTNewLog, model);
            return result.Data;
        }
        public async Task<List<UploadActUTModel>> GetUploadActUTNew(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadActUTModel>>(AdminAPI.GetUploadActUTNew, model);
            return result.Data;
        }


        public async Task<List<UploadTypeModel>> GetHeaderExcel(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadTypeModel>>(AdminAPI.GetHeaderExcel, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadEndTab(List<UploadEndTabModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadEndTabModel>, ResultModel>(AdminAPI.SetUploadEndTab, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadEndTabLog(List<UploadEndTabModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadEndTabModel>, ResultModel>(AdminAPI.SetUploadEndTabLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadEndTabLog(List<UploadEndTabModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadEndTabModel>, ResultModel>(AdminAPI.DelUploadEndTabLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadEndTab(UploadEndTabModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadEndTabModel, ResultModel>(AdminAPI.DelUploadEndTab, model);
            return result.Data;
        }

        public async Task<List<UploadEndTabModel>> GetUploadEndTabLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadEndTabModel>>(AdminAPI.GetUploadEndTabLog, model);
            return result.Data;
        }
        public async Task<List<UploadEndTabModel>> GetUploadEndTab(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadEndTabModel>>(AdminAPI.GetUploadEndTab, model);
            return result.Data;
        }

        //CutPart
        public async Task<ResultModel> SetUploadCutPart(List<UploadCutPartModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadCutPartModel>, ResultModel>(AdminAPI.SetUploadCutPart, model);
            return result.Data;
        }

        public async Task<ResultModel> SetUploadCutPartLog(List<UploadCutPartModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadCutPartModel>, ResultModel>(AdminAPI.SetUploadCutPartLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadCutPartLog(List<UploadCutPartModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadCutPartModel>, ResultModel>(AdminAPI.DelUploadCutPartLog, model);
            return result.Data;
        }

        public async Task<ResultModel> DelUploadCutPart(UploadCutPartModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadCutPartModel, ResultModel>(AdminAPI.DelUploadCutPart, model);
            return result.Data;
        }

        public async Task<List<UploadCutPartModel>> GetUploadCutPartLog(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadCutPartModel>>(AdminAPI.GetUploadCutPartLog, model);
            return result.Data;
        }
        public async Task<List<UploadCutPartModel>> GetUploadCutPart(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadCutPartModel>>(AdminAPI.GetUploadCutPart, model);
            return result.Data;
        }
        //
        public async Task<List<ProcessTypeModel>> GetMasterProcessType(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<ProcessTypeModel>>(AdminAPI.GetMasterProcessType, model);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterProcessType(ProcessTypeModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ProcessTypeModel, ResultModel>(AdminAPI.SetMasterProcessType, model);
            return result.Data;
        }

        public async Task<ResultModel> DeleteMasterProcessType(ProcessTypeModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ProcessTypeModel, ResultModel>(AdminAPI.DeleteMasterProcessType, model);
            return result.Data;
        }

        public async Task<ResultModel> SetCheckSymbol(List<PartDatilSummaryForWorkModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<PartDatilSummaryForWorkModel>, ResultModel>(AdminAPI.SetCheckSymbol, model);
            return result.Data;
        }


        public async Task<ResultModel> DelCheckSymbolData(List<DataValueModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<DataValueModel>, ResultModel>(AdminAPI.DelCheckSymbolData, model);
            return result.Data;
        }


        public async Task<PartDetailModel> GetDetailForCheckSymbolData(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, PartDetailModel>(AdminAPI.GetDetailForCheckSymbolData, model);
            return result.Data;
        }

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        // 2018-02-12 byy chaiwud.ta
        public async Task<ResultModel> SetUploadGoalBuilt(List<UploadGoalBuiltModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadGoalBuiltModel>, ResultModel>(AdminAPI.SetUploadGoalBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadGoalLogBuilt(List<UploadGoalBuiltModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadGoalBuiltModel>, ResultModel>(AdminAPI.SetUploadGoalLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadGoalLogBuilt(List<UploadGoalBuiltModel> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UploadGoalBuiltModel>, ResultModel>(AdminAPI.DelUploadGoalLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadGoalBuilt(UploadGoalBuiltModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UploadGoalBuiltModel, ResultModel>(AdminAPI.DelUploadGoalBuilt, model);
            return result.Data;
        }
        public async Task<List<UploadGoalBuiltModel>> GetUploadGoalLogBuilt(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadGoalBuiltModel>>(AdminAPI.GetUploadGoalLogBuilt, model);
            return result.Data;
        }
        public async Task<List<UploadGoalBuiltModel>> GetUploadGoalBuilt(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadGoalBuiltModel>>(AdminAPI.GetUploadGoalBuilt, model);
            return result.Data;
        }

        // UTRepairCheck
        // 2018-03-01 by chaiwud.ta
        public async Task<ResultModel> SetUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UTRepairCheckModelBuilt>, ResultModel>(AdminAPI.SetUploadUTRepairCheckLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadUTRepairCheckBuilt(List<UTRepairCheckModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UTRepairCheckModelBuilt>, ResultModel>(AdminAPI.SetUploadUTRepairCheckBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UTRepairCheckModelBuilt>, ResultModel>(AdminAPI.DelUploadUTRepairCheckLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UTRepairCheckModelBuilt, ResultModel>(AdminAPI.DelUploadUTRepairCheckBuilt, model);
            return result.Data;
        }
        public async Task<List<UTRepairCheckModelBuilt>> GetUploadUTRepairCheckLogBuilt(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UTRepairCheckModelBuilt>>(AdminAPI.GetUploadUTRepairCheckLogBuilt, model);
            return result.Data;
        }
        public async Task<List<UTRepairCheckModelBuilt>> GetUploadUTRepairCheckBuilt(ReportSearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchModel, List<UTRepairCheckModelBuilt>>(AdminAPI.GetUploadUTRepairCheckBuilt, model);
            return result.Data;
        }

        #region UTDataByUser
        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        public async Task<ResultModel> SetUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UTDataByUserModelBuilt>, ResultModel>(AdminAPI.SetUploadUTDataByUserLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadUTDataByUserBuilt(List<UTDataByUserModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UTDataByUserModelBuilt>, ResultModel>(AdminAPI.SetUploadUTDataByUserBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<UTDataByUserModelBuilt>, ResultModel>(AdminAPI.DelUploadUTDataByUserLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<UTDataByUserModelBuilt, ResultModel>(AdminAPI.DelUploadUTDataByUserBuilt, model);
            return result.Data;
        }
        public async Task<List<UTDataByUserModelBuilt>> GetUploadUTDataByUserLogBuilt(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UTDataByUserModelBuilt>>(AdminAPI.GetUploadUTDataByUserLogBuilt, model);
            return result.Data;
        }
        public async Task<List<UTDataByUserModelBuilt>> GetUploadUTDataByUserBuilt(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UTDataByUserModelBuilt>>(AdminAPI.GetUploadUTDataByUserBuilt, model);
            return result.Data;
        }
        #endregion

        // NGDataBuily
        public async Task<ResultModel> SetUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModelBuilt>, ResultModel>(AdminAPI.SetUploadNGDataLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadNGDataBuilt(List<NGDataModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModelBuilt>, ResultModel>(AdminAPI.SetUploadNGDataBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<NGDataModelBuilt>, ResultModel>(AdminAPI.DelUploadNGDataLogBuilt, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadNGDataBuilt(NGDataModelBuilt model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<NGDataModelBuilt, ResultModel>(AdminAPI.DelUploadNGDataBuilt, model);
            return result.Data;
        }
        public async Task<List<NGDataModelBuilt>> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchBuiltModel, List<NGDataModelBuilt>>(AdminAPI.GetUploadNGDataLogBuilt, model);
            return result.Data;
        }
        public async Task<List<NGDataModelBuilt>> GetUploadNGDataBuilt(ReportSearchBuiltModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchBuiltModel, List<NGDataModelBuilt>>(AdminAPI.GetUploadNGDataBuilt, model);
            return result.Data;
        }

        // CamberOther
        // 2018-04-23 by chaiwud.ta
        public async Task<ResultModel> SetUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<CamberDataModelOther>, ResultModel>(AdminAPI.SetUploadCamberDataLogOther, model);
            return result.Data;
        }
        public async Task<ResultModel> SetUploadCamberDataOther(List<CamberDataModelOther> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<CamberDataModelOther>, ResultModel>(AdminAPI.SetUploadCamberDataOther, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<List<CamberDataModelOther>, ResultModel>(AdminAPI.DelUploadCamberDataLogOther, model);
            return result.Data;
        }
        public async Task<ResultModel> DelUploadCamberDataOther(CamberDataModelOther model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<CamberDataModelOther, ResultModel>(AdminAPI.DelUploadCamberDataOther, model);
            return result.Data;
        }
        public async Task<List<CamberDataModelOther>> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchOtherFinishModel, List<CamberDataModelOther>>(AdminAPI.GetUploadCamberDataLogOther, model);
            return result.Data;
        }
        public async Task<List<CamberDataModelOther>> GetUploadCamberDataOther(ReportSearchOtherFinishModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ReportSearchOtherFinishModel, List<CamberDataModelOther>>(AdminAPI.GetUploadCamberDataOther, model);
            return result.Data;
        }
        #endregion
    }
}
