﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;


namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public class WeekServiceWrapper : IWeekServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public WeekServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        public async Task<List<DepartmentModel>> GetGroupName(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<DepartmentModel>>(WeekAPI.GetGroupName, model);
            return result.Data;
        }
        public async Task<List<UserModel>> GetSearchUser(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<UserModel>>(WeekAPI.GetSearchUser, model);
            return result.Data;
        }

        public async Task<List<WeekModel>> GetWeekByMonth(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<WeekModel>>(WeekAPI.GetWeekByMonth, model);
            return result.Data;
        }
        public async Task<List<GoalModel>> GetTxnMonthByWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<GoalModel>>(WeekAPI.GetTxnMonthByWeek, model);
            return result.Data;
        }
        public async Task<List<GoalSumModel>> GetSummaryByWork(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<GoalSumModel>>(WeekAPI.GetSummaryByWork, model);
            return result.Data;
        }

        public async Task<List<SararyModel>> GetSararyMonthByWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<SararyModel>>(WeekAPI.GetSararyMonthByWeek, model);
            return result.Data;
        }

        public async Task<GoalHeaderModel> GetSummaryWorkByCode(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, GoalHeaderModel>(WeekAPI.GetSummaryWorkByCode, model);
            return result.Data;
        }

        public async Task<List<SararyModel>> GetSararyMonthByCode(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<SararyModel>>(WeekAPI.GetSararyMonthByCode, model);
            return result.Data;
        }

        public async Task<List<SumSalaryModel>> GetSalaryFab(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<SumSalaryModel>>(WeekAPI.GetSalaryFab, model);
            return result.Data;
        }

        public async Task<DayHeaderModel> GetWorkLessGoal(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DayHeaderModel>(WeekAPI.GetWorkLessGoal, model);
            return result.Data;
        }

        public async Task<List<WorkModel>> GetWorkMonth(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<WorkModel>>(WeekAPI.GetWorkMonth, model);
            return result.Data;
        }

        public async Task<List<WorkModel>> GetWorkMonthByDay(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<WorkModel>>(WeekAPI.GetWorkMonthByDay, model);
            return result.Data;
        }

        public async Task<DayHeaderModel> GetWorkByDay(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DayHeaderModel>(WeekAPI.GetWorkByDay, model);
            return result.Data;
        }

        public async Task<WeekHeaderModel> GetWorkByWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, WeekHeaderModel>(WeekAPI.GetWorkByWeek, model);
            return result.Data;
        }

        public async Task<DayHeaderModel>  GetSalaryByDay(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DayHeaderModel>(WeekAPI.GetSalaryByDay, model);
            return result.Data;
        }

        public async Task<WeekHeaderModel> GetSalaryByWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, WeekHeaderModel>(WeekAPI.GetSalaryByWeek, model);
            return result.Data;
        }

        public async Task<DayHeaderModel> GetSalaryGroupByDay(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DayHeaderModel>(WeekAPI.GetSalaryGroupByDay, model);
            return result.Data;
        }

        public async Task<WeekHeaderModel> GetSalaryGroupByWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, WeekHeaderModel>(WeekAPI.GetSalaryGroupByWeek, model);
            return result.Data;
        }

        public async Task<DayHeaderModel> GetSalaryByDayAndCode(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DayHeaderModel>(WeekAPI.GetSalaryByDayAndCode, model);
            return result.Data;
        }

        public async Task<DayHeaderModel> GetWorkByDayAndCode(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DayHeaderModel>(WeekAPI.GetWorkByDayAndCode, model);
            return result.Data;
        }


        public async Task<List<ReviseModel>> GetReviseByDayAndCode(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<ReviseModel>>(WeekAPI.GetReviseByDayAndCode, model);
            return result.Data;
        }

        public async Task<List<ReviseModel>> GetOtherByDayAndCode(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<ReviseModel>>(WeekAPI.GetOtherByDayAndCode, model);
            return result.Data;
        }

        public async Task<NCRHeaderModel> GetReportNCR(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, NCRHeaderModel>(WeekAPI.GetReportNCR, model);
            return result.Data;
        }

        public async Task<NCRHeaderModel> GetReportVT(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, NCRHeaderModel>(WeekAPI.GetReportVT, model);
            return result.Data;
        }

        public async Task<GoalHeaderModel> GetSummaryWorkDayForWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, GoalHeaderModel>(WeekAPI.GetSummaryWorkDayForWeek, model);
            return result.Data;
        }

        public async Task<DataFilterModel> GetMaxWeeks(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, DataFilterModel>(WeekAPI.GetMaxWeeks, model);
            return result.Data;
        }
        public async Task<List<DataValueModel>> GetHeaderWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataValueModel>>(WeekAPI.GetHeaderWeek, model);
            return result.Data;
        }
        public async Task<List<SalarySummaryModel>> GetSummarySalaryDayForWeek(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<SalarySummaryModel>>(WeekAPI.GetSummarySalaryDayForWeek, model);
            return result.Data;
        }
        public async Task<GoalHeaderModel> GetSummaryWorkDayForWeekLess(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, GoalHeaderModel>(WeekAPI.GetSummaryWorkDayForWeekLess, model);
            return result.Data;
        }
        public async Task<GoalHeaderModel> GetSummaryWorkDayForWeekWeldLess(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, GoalHeaderModel>(WeekAPI.GetSummaryWorkDayForWeekWeldLess, model);
            return result.Data;
        }
        public async Task<List<PlusModel>> GetSummarySalaryPlus(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<PlusModel>>(WeekAPI.GetSummarySalaryPlus, model);
            return result.Data;
        }
        public async Task<List<PlusModel>> GetSummarySalaryMinus(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<PlusModel>>(WeekAPI.GetSummarySalaryMinus, model);
            return result.Data;
        }
        public async Task<List<PlusModel>> GetSummarySalaryTotal(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<PlusModel>>(WeekAPI.GetSummarySalaryTotal, model);
            return result.Data;
        }

        public async Task<SummaryWorkHeaderModel> GetSummaryWorkDayForWeekWeld(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, SummaryWorkHeaderModel>(WeekAPI.GetSummaryWorkDayForWeekWeld, model);
            return result.Data;
        }

        public async Task<SummarySalaryHeaderModel> GetSummarySalaryForWeekWeld(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, SummarySalaryHeaderModel>(WeekAPI.GetSummarySalaryForWeekWeld, model);
            return result.Data;
        }

        public async Task<SummarySalaryHeaderModel> GetSummarySalaryForWeekWeldSum(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, SummarySalaryHeaderModel>(WeekAPI.GetSummarySalaryForWeekWeldSum, model);
            return result.Data;
        }

        public async Task<SummarySalaryHeaderModel> GetSummarySalaryForWeekSum(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, SummarySalaryHeaderModel>(WeekAPI.GetSummarySalaryForWeekSum, model);
            return result.Data;
        }
        //public List<DataFilterModel> GetDays(DateModel model)
        //{
        //    var result = httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<DataFilterModel>>(WeekAPI.GetDays, model);
        //    return result.Result;
        //}
    }
}
