﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public class LevelServiceWrapper : ILevelServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public LevelServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        public async Task<List<LevelModel>> GetLevel(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<LevelModel>>(LeaveAPI.GetLevel, model);
            return result.Data;
        }

        public async Task<List<LevelNCRModel>> GetLevelNCR(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<LevelNCRModel>>(LeaveAPI.GetLevelNCR, model);
            return result.Data;
        }


        public async Task<List<LevelNGModel>> GetLevelNG(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<LevelNGModel>>(LeaveAPI.GetLevelNG, model);
            return result.Data;
        }

        public async Task<List<LevelGroupModel>> GetMasterLevelGroup(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<LevelGroupModel>>(LeaveAPI.GetMasterLevelGroup, model);
            return result.Data;
        }

        public async Task<List<ProcessModel>> GetMasterProcess(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<ProcessModel>>(LeaveAPI.GetMasterProcess, model);
            return result.Data;
        }

        public async Task<List<CertModel>> GetMasterCert()
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<CertModel, List<CertModel>>(LeaveAPI.GetMasterCert, null);
            return result.Data;
        }

        public async Task<List<CertListModel>> GetMasterCertList()
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<CertListModel, List<CertListModel>>(LeaveAPI.GetMasterCertList, null);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterCert(CertModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<CertModel, ResultModel>(LeaveAPI.SetMasterCert, model);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterGroup(LevelGroupModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelGroupModel, ResultModel>(LeaveAPI.SetMasterGroup, model);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterLevel(LevelModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelModel, ResultModel>(LeaveAPI.SetMasterLevel, model);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterLevelNCR(LevelNCRModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelNCRModel, ResultModel>(LeaveAPI.SetMasterLevelNCR, model);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterLevelNG(LevelNGModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelNGModel, ResultModel>(LeaveAPI.SetMasterLevelNG, model);
            return result.Data;
        }

        public async Task<ResultModel> SetMasterProcess(ProcessModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ProcessModel, ResultModel>(LeaveAPI.SetMasterProcess, model);
            return result.Data;
        }

        public async Task<ResultModel> DeleteMasterCert(CertModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<CertModel, ResultModel>(LeaveAPI.DeleteMasterCert, model);
            return result.Data;
        }

        public async Task<ResultModel> DeleteMasterGroup(LevelGroupModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelGroupModel, ResultModel>(LeaveAPI.DeleteMasterGroup, model);
            return result.Data;
        }

        public async Task<ResultModel> DeleteMasterLevel(LevelModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelModel, ResultModel>(LeaveAPI.DeleteMasterLevel, model);
            return result.Data;
        }

        public async Task<ResultModel> DeleteMasterLevelNCR(LevelNCRModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelNCRModel, ResultModel>(LeaveAPI.DeleteMasterLevelNCR, model);
            return result.Data;
        }

        public async Task<ResultModel> DeleteMasterLevelNG(LevelNGModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelNGModel, ResultModel>(LeaveAPI.DeleteMasterLevelNG, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteMasterProcess(ProcessModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ProcessModel, ResultModel>(LeaveAPI.DeleteMasterProcess, model);
            return result.Data;
        }

        public async Task<DataFilterModel> GetPriceByProcess(ProcessModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<ProcessModel, DataFilterModel>(LeaveAPI.GetPriceByProcess, model);
            return result.Data;
        }

        #region Built Beam/Box by chaiwud.ta
        // 2018-02-10 by chaiwud.ta 
        public async Task<List<LevelBuiltModel>> GetBuiltLevel(SearchModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<SearchModel, List<LevelBuiltModel>>(LeaveAPI.GetBuiltLevel, model);
            return result.Data;
        }
        public async Task<ResultModel> SetMasterBuiltLevel(LevelBuiltModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelBuiltModel, ResultModel>(LeaveAPI.SetMasterBuiltLevel, model);
            return result.Data;
        }
        public async Task<ResultModel> DeleteMasterBuiltLevel(LevelBuiltModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<LevelBuiltModel, ResultModel>(LeaveAPI.DeleteMasterLevel, model);
            return result.Data;
        }
        #endregion
    }
}
