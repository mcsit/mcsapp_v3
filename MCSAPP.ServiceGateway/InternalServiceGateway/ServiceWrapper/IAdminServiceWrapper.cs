﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public interface IAdminServiceWrapper
    {

        void WriteLogError(LogModel model);

        void WriteLogActivity(LogModel model);

        Task<ResultModel> SetTxnAdjustGoalData(AdjustGoalModel model);

        Task<ResultModel> SetTxnNcrData(List<NcrDataModel> model);
        Task<ResultModel> SetNCRDataLog(List<NcrDataModel> model);
        Task<ResultModel> DelNCRDetailLog(List<NcrDataModel> model);
        Task<ResultModel> SetTxnVTData(List<VTDataModel> model);

        Task<ResultModel> SetTxnDimensionData(DimDataModel model);

        Task<ResultModel> SetTxnOtherData(OtherDataModel model);
        Task<ResultModel> SetTxnMinusData(MinusDataModel model);

        Task<ResultModel> SetTxnWeekData(WeekDataModel model);
        Task<ResultModel> SetVTDetail(List<VTListModel> model);
        Task<ResultModel> SetVTDetailLog(List<VTListModel> model);
        Task<ResultModel> SetVTDetailUpload(List<VTListModel> model);
        Task<ResultModel> DelVTDetailLog(List<VTListModel> model);

        Task<List<VTListModel>> GetVTDetailLog(DateModel model);
        Task<List<VTListModel>> GetVTDetailAll(DateModel model);

        Task<NCRDetailHeaderModel> GetReportNCRDetail(DateModel model);
        Task<NGDetailHeaderModel> GetReportNGDetail(DateModel model);
        Task<List<NcrDataModel>> GetNCRLog(DateModel model);
        Task<VTDetailHeaderModel> GetReportVTDetail(DateModel model);
        Task<GoalSumModel> GetWeekLessForAdjust(DateModel model);
        Task<List<GoalSumModel>> GetAdjustGoalData(DateModel model);
        Task<List<MonthModel>> GetCalendar(DateModel model);

        //Task<List<MinusDataModel>> GetReportMinusData(ReportSearchModel model);
        //Task<List<OtherDataModel>> GetReportOtherData(ReportSearchModel model);

        Task<ResultModel> SetDesing(List<DesingModel> model);
        Task<ResultModel> SetDesingLog(List<DesingModel> model);
        Task<ResultModel> DelDesingLog(List<DesingModel> model);
        Task<List<DesingModel>> GetDesingLog(DateModel model);


        Task<ResultModel> SetUploadOther(List<OtherModel> model);
        Task<ResultModel> SetUploadOtherLog(List<OtherModel> model);
        Task<ResultModel> DelUploadOtherLog(List<OtherModel> model);
        Task<List<OtherModel>> GetUploadOtherLog(DateModel model);

        Task<ResultModel> SetUploadWeek(List<UploadWeekModel> model);
        Task<ResultModel> SetUploadWeekLog(List<UploadWeekModel> model);
        Task<ResultModel> DelUploadWeekLog(List<UploadWeekModel> model);
        Task<List<UploadWeekModel>> GetUploadWeekLog(DateModel model);

        Task<ResultModel> DeleteAdjustGoal(DeleteModel model);

        Task<ResultModel> DeleteDesingChange(DeleteModel model);

        Task<ResultModel> DeleteDimension(DeleteModel model);

        Task<ResultModel> DeleteMinus(DeleteModel model);

        Task<ResultModel> DeleteOther(DeleteModel model);

        Task<ResultModel> DeletePlus(DeleteModel model);

        Task<ResultModel> DeleteVTData(DeleteModel model);

        Task<ResultModel> DeleteWeek(DeleteModel model);

        Task<List<ReportDesignChangeModel>> GetReportDesingChangeData(ReportSearchModel model);

        Task<List<ReportDimensionModel>> GetReportDimensionData(ReportSearchModel model);

        Task<List<ReportMinusModel>> GetReportMinusData(ReportSearchModel model);

        Task<List<ReportDimensionModel>> GetReportOtherData(ReportSearchModel model);

        Task<List<ReportDimensionModel>> GetReportPlusData(ReportSearchModel model);

        Task<List<VTListModel>> GetReportVTData(ReportSearchModel model);

        Task<List<UploadWeekModel>> GetReportWeekData(ReportSearchModel model);

        Task<ResultModel> SetUploadNG(List<NGDataModel> model);
        Task<ResultModel> SetUploadNGLog(List<NGDataModel> model);
        Task<ResultModel> DelUploadNGLog(List<NGDataModel> model);
        Task<List<NGDataModel>> GetUploadNGLog(DateModel model);

        Task<List<NGDataModel>> GetUploadNG(DateModel model);

        Task<ResultModel> DelUploadNG(NGDataModel model);
        Task<ResultModel> UpdateUploadNG(NGDataModel model);

        Task<ResultModel> SetUploadActUTLog(List<UploadActUTModel> model);
        Task<ResultModel> SetUploadActUT(List<UploadActUTModel> model);
        Task<ResultModel> DelUploadActUTLog(List<UploadActUTModel> model);
        Task<ResultModel> DelUploadActUT(UploadActUTModel model);
        Task<List<UploadActUTModel>> GetUploadActUTLog(DateModel model);
        Task<List<UploadActUTModel>> GetUploadActUT(DateModel model);

        Task<ResultModel> SetUploadCutFinisingLog(List<UploadCutFinisingModel> model);
        Task<ResultModel> SetUploadCutFinising(List<UploadCutFinisingModel> model);
        Task<ResultModel> DelUploadCutFinisingLog(List<UploadCutFinisingModel> model);
        Task<ResultModel> DelUploadCutFinising(UploadCutFinisingModel model);
        Task<List<UploadCutFinisingModel>> GetUploadCutFinisingLog(DateModel model);
        Task<List<UploadCutFinisingModel>> GetUploadCutFinising(DateModel model);

        Task<ResultModel> SetUploadGoalLog(List<UploadGoalModel> model);
        Task<ResultModel> SetUploadGoal(List<UploadGoalModel> model);
        Task<ResultModel> DelUploadGoalLog(List<UploadGoalModel> model);
        Task<ResultModel> DelUploadGoal(UploadGoalModel model);
        Task<List<UploadGoalModel>> GetUploadGoalLog(DateModel model);
        Task<List<UploadGoalModel>> GetUploadGoal(DateModel model);

        //
        Task<ResultModel> SetUploadActUTNew(List<UploadActUTModel> model);
        Task<ResultModel> SetUploadActUTNewLog(List<UploadActUTModel> model);

        Task<List<UploadActUTModel>> GetUploadActUTNewLog(DateModel model);
        Task<List<UploadActUTModel>> GetUploadActUTNew(DateModel model);

        Task<ResultModel> DelUploadActUTNewLog(List<UploadActUTModel> model);
        Task<ResultModel> DelUploadActUTNew(UploadActUTModel model);

        //

        Task<ResultModel> SetUploadNGNew(List<NGDataModel> model);
        Task<ResultModel> SetUploadNGNewLog(List<NGDataModel> model);

        Task<ResultModel> DelUploadNGNewLog(List<NGDataModel> model);
        Task<ResultModel> DelUploadNGNew(NGDataModel model);

        Task<List<NGDataModel>> GetUploadNGNewLog(DateModel model);
        Task<List<NGDataModel>> GetUploadNGNew(DateModel model);

        Task<List<UploadTypeModel>> GetHeaderExcel(DateModel model);

        Task<ResultModel> SetUploadEndTab(List<UploadEndTabModel> model);
        Task<ResultModel> SetUploadEndTabLog(List<UploadEndTabModel> model);

        Task<List<UploadEndTabModel>> GetUploadEndTabLog(DateModel model);
        Task<List<UploadEndTabModel>> GetUploadEndTab(DateModel model);

        Task<ResultModel> DelUploadEndTabLog(List<UploadEndTabModel> model);
        Task<ResultModel> DelUploadEndTab(UploadEndTabModel model);

        //Cut Part
        Task<ResultModel> SetUploadCutPartLog(List<UploadCutPartModel> model);
        Task<ResultModel> SetUploadCutPart(List<UploadCutPartModel> model);
        Task<ResultModel> DelUploadCutPartLog(List<UploadCutPartModel> model);
        Task<ResultModel> DelUploadCutPart(UploadCutPartModel model);
        Task<List<UploadCutPartModel>> GetUploadCutPartLog(DateModel model);
        Task<List<UploadCutPartModel>> GetUploadCutPart(DateModel model);

        Task<List<ProcessTypeModel>> GetMasterProcessType(SearchModel model);

        Task<ResultModel> SetMasterProcessType(ProcessTypeModel model);
        Task<ResultModel> DeleteMasterProcessType(ProcessTypeModel model);

        Task<ResultModel> SetCheckSymbol(List<PartDatilSummaryForWorkModel> model);

        Task<ResultModel> DelCheckSymbolData(List<DataValueModel> model);
        Task<PartDetailModel> GetDetailForCheckSymbolData(DateModel model);

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        // 2018-02-12 by chaiwud.ta
        Task<ResultModel> SetUploadGoalLogBuilt(List<UploadGoalBuiltModel> model);
        Task<ResultModel> SetUploadGoalBuilt(List<UploadGoalBuiltModel> model);
        Task<ResultModel> DelUploadGoalLogBuilt(List<UploadGoalBuiltModel> model);
        Task<ResultModel> DelUploadGoalBuilt(UploadGoalBuiltModel model);
        Task<List<UploadGoalBuiltModel>> GetUploadGoalLogBuilt(DateModel model);
        Task<List<UploadGoalBuiltModel>> GetUploadGoalBuilt(DateModel model);

        // UTRepairCheck
        // 2018-03-01 by chaiwd.ta
        Task<ResultModel> SetUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model);
        Task<ResultModel> SetUploadUTRepairCheckBuilt(List<UTRepairCheckModelBuilt> model);
        Task<ResultModel> DelUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model);
        Task<ResultModel> DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model);
        Task<List<UTRepairCheckModelBuilt>> GetUploadUTRepairCheckLogBuilt(DateModel model);
        Task<List<UTRepairCheckModelBuilt>> GetUploadUTRepairCheckBuilt(ReportSearchModel model);

        #region UTDataByUser
        // UTDataByUser
        // 2018-03-15 by chaiwud.ta
        Task<ResultModel> SetUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model);
        Task<ResultModel> SetUploadUTDataByUserBuilt(List<UTDataByUserModelBuilt> model);
        Task<ResultModel> DelUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model);
        Task<ResultModel> DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model);
        Task<List<UTDataByUserModelBuilt>> GetUploadUTDataByUserLogBuilt(DateModel model);
        Task<List<UTDataByUserModelBuilt>> GetUploadUTDataByUserBuilt(DateModel model);
        #endregion

        // NGDataBuilt
        Task<ResultModel> SetUploadNGDataLogBuilt(List<NGDataModelBuilt> model);
        Task<ResultModel> SetUploadNGDataBuilt(List<NGDataModelBuilt> model);
        Task<ResultModel> DelUploadNGDataLogBuilt(List<NGDataModelBuilt> model);
        Task<ResultModel> DelUploadNGDataBuilt(NGDataModelBuilt model);
        Task<List<NGDataModelBuilt>> GetUploadNGDataLogBuilt(ReportSearchBuiltModel model);
        Task<List<NGDataModelBuilt>> GetUploadNGDataBuilt(ReportSearchBuiltModel model);

        // CamberOther
        // 2018-04-23 by chaiwud.ta
        Task<ResultModel> SetUploadCamberDataLogOther(List<CamberDataModelOther> model);
        Task<ResultModel> SetUploadCamberDataOther(List<CamberDataModelOther> model);
        Task<ResultModel> DelUploadCamberDataLogOther(List<CamberDataModelOther> model);
        Task<ResultModel> DelUploadCamberDataOther(CamberDataModelOther model);
        Task<List<CamberDataModelOther>> GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model);
        Task<List<CamberDataModelOther>> GetUploadCamberDataOther(ReportSearchOtherFinishModel model);
        #endregion
    }
}
