﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.Account;


namespace MCSAPP.ServiceGateway.ServiceWrapper
{
   public  interface IAccountServiceWrapper
    {
        Task<DataVerifyModel<UserAccountModel>> GetLogin(UserLoginModel model);
    }
}
