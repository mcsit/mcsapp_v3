﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public interface IAppServiceWrapper
    {
        Task<List<PartDetail2Model>> GetPartDetail(DateModel model);
        Task<List<PartDetail2Model>> GetOtherDetail(DateModel model);
        Task<List<VTListModel>> GetVTDetail(DateModel model);

        Task<List<VTListModel>> GetVTForAdd(DateModel model);

        Task<List<UploadTypeModel>> GetHeaderExcel(DateModel model);
    }
}
