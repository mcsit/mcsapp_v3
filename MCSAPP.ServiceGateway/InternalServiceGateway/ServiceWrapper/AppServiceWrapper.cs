﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper.APIServiceConstant;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway.ServiceWrapper
{
    public class AppServiceWrapper : IAppServiceWrapper
    {
        private readonly IHttpClientServiceAPI httpClientServiceAPI;

        public AppServiceWrapper(IHttpClientServiceAPI httpClientServiceAPI)
        {
            this.httpClientServiceAPI = httpClientServiceAPI;
        }

        public async Task<List<PartDetail2Model>> GetPartDetail(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<PartDetail2Model>>(AppAPI.GetPartDetail, model);
            return result.Data;
        }
        public async Task<List<PartDetail2Model>> GetOtherDetail(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<PartDetail2Model>>(AppAPI.GetOtherDetail, model);
            return result.Data;
        }
        public async Task<List<VTListModel>> GetVTDetail(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<VTListModel>>(AppAPI.GetVTDetail, model);
            return result.Data;
        }

        public async Task<List<VTListModel>> GetVTForAdd(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<VTListModel>>(AppAPI.GetVTForAdd, model);
            return result.Data;
        }

        public async Task<List<UploadTypeModel>> GetHeaderExcel(DateModel model)
        {
            var result = await httpClientServiceAPI.PostToApiServiceRespondDataModel<DateModel, List<UploadTypeModel>>(AppAPI.GetHeaderExcel, model);
            return result.Data;
        }
    }
}
