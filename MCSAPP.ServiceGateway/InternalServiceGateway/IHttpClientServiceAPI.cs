﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using MCSAPP.DAL.Model;

namespace MCSAPP.ServiceGateway
{
    public interface IHttpClientServiceAPI
    {
        Task<string> PostAsFormUrlEncodeContentToApiwithToken(string targetApi, string tokenKey, params KeyValuePair<string, string>[] data);
        Task<DataVerifyModel<T>> PostAsFormUrlEncodeContentToApiwithToken<T>(string targetApi, string tokenKey, params KeyValuePair<string, string>[] data) where T : class, new();
        Task<DataVerifyModel<T>> PostImageToApiServiceRespondDataModel<T>(string targetApi, IList<HttpPostedFileBase> HttpPostedFilesBase) where T : class, new();
        Task<JsonVerifyModel> PostJsonModelToApiServiceRespondJsonModel<T>(string targetApi, T model);
        Task<string> PostJsonModelToApiServiceRespondString<T>(string targetApi, T model);
        Task<DataVerifyModel<TOutput>> PostToApiServiceRespondDataModel<TInput, TOutput>(string targetApi, TInput model)
            where TOutput : class, new();
        Task<DataVerifyModel<TOutput>> PostToApiServiceRespondDataModel<TOutput>(string targetApi) where TOutput : class, new();
        void PostToApiServiceVoidRespond<T>(string targetApi, T model) where T : class;
        void SetAPITrackingModel(APITrackingConfigModel trackingModel);
        void setAPITrackingModel(long accessId, string userStamp);
    }
}