﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.DAL.Model;
using MCSAPP.Security.Cryptographic;
using Newtonsoft.Json;

namespace MCSAPP.ServiceGateway
{
    public class ConfigurationToken
    {
        public static string GenerateConfigurationTokenCode(APITrackingConfigModel model)
        {
            var jsonModel = JsonConvert.SerializeObject(model);
            var encodeJsonModel = InternalCryptographic.EncryptData(jsonModel);
            return encodeJsonModel;
        }

        /// <summary>
        /// Decode ConfigToken and Callback when Decode or Deserialize Fail.
        /// </summary>
        /// <param name="tokenTracking">token Tracking String</param>
        /// <param name="callback">Callback when DecodeUnsuccess.</param>
        /// <returns></returns>
        public static APITrackingConfigModel DecodeConfigurationToken(string tokenTracking, Action<string, Exception, string> callback = null)
        {
            var result = new APITrackingConfigModel();
            try
            {
                var decodeJsonModel = InternalCryptographic.DecryptData(tokenTracking);
                result = JsonConvert.DeserializeObject<APITrackingConfigModel>(decodeJsonModel);
            }
            catch(Exception ex)
            {
                if (callback != null)
                {
                    callback.Invoke("Error AlreadyCatch When Decode APITrackingConfigModel", ex, "ConfigurationToken.DecodeConfigurationToken");
                }
            }
            return result;
        }
    }
}
