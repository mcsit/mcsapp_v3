﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetHttp = System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using System.Web;
using MCSAPP.DAL.Model;
using System.IO;
using MCSAPP.Helper.Constant;
using System.Diagnostics;
using MCSAPP.Helper;

namespace  MCSAPP.ServiceGateway
{
    /// <summary>
    /// Header ()
    /// </summary>
    public class HttpClientServiceAPI : IHttpClientServiceAPI
    {

        #region Http Gobal
        private readonly string serviceLogPath = "api/Utility/Log/Error";
        private string webServiceUri = Configuration.ApiServiceUri;
        private APITrackingConfigModel trackingModel;

        private NetworkCredential networkCredential;

        public HttpClientServiceAPI(NetworkCredential networkCredential, APITrackingConfigModel trackingModel)
        {
            this.networkCredential = networkCredential;
            this.trackingModel = trackingModel ?? new APITrackingConfigModel();
        }
        private async Task<HttpResponseMessage> HttpClientPostAsync<TInput>(string requestUri, TInput model)
        {
            var stopWatch = new Stopwatch();
            try
            {
                using (var client = new NetHttp.HttpClient(new HttpClientHandler() { Credentials = networkCredential }) { BaseAddress = new Uri(webServiceUri) })
                {
                    client.Timeout = TimeSpan.FromMilliseconds(Configuration.InternalServiceAPITimeOut);
                    var inputContent = (typeof(TInput) == typeof(string)) ? (model as string) : JsonConvert.SerializeObject(model);
                    var content = new StringContent(inputContent, Encoding.UTF8, "application/json");
                    SetTrackingEncode(content);
                    stopWatch.Start();
                    var respond = await client.PostAsync(requestUri, content);
                    return respond;
                }
            }
            catch (Exception ex)
            {
                //ex.Source = LogTemplate.WriteHttpClientExceptionSource("HttpClientPostAsync", requestUri, stopWatch.ElapsedMilliseconds);
                throw ex;
            }
        }

        private string HandlesErrorRespond(NetHttp.HttpResponseMessage respondsMessage)
        {
            //DO handles Error Response (API UnHandle Error)
            var content = respondsMessage?.Content;
            var contentMessage = content != null ? content.ReadAsStringAsync().Result : string.Empty;
            var errorMessage = string.Format("Call API Error!!\n HttpStatusCode:{0};\n RequestMessage:{1};\n Headers:{2};\n Content:{3}",
                respondsMessage?.StatusCode,
                JsonConvert.SerializeObject(respondsMessage?.RequestMessage),
                JsonConvert.SerializeObject(respondsMessage?.Headers),
                JsonConvert.SerializeObject(contentMessage));
            return errorMessage;
        }
        #endregion



        #region Disable Http Getmethods
        //public async Task<T> GetService<T>(string targetApi) where T : class, new()
        //{
        //    var result = new T();
        //    using (var client = new NetHttp.HttpClient(new NetHttp.HttpClientHandler() { Credentials = networkCredential }))
        //    {
        //        client.BaseAddress = new Uri(webServiceUri);
        //        var response = await client.GetAsync(targetApi);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var serviceRespond = await response.Content.ReadAsAsync<ResultModel<T>>();
        //            result = serviceRespond.Data;
        //        }
        //        else
        //        {
        //            HandlesErrorRespond(response);
        //        }
        //    }
        //    return result;
        //}

        #endregion



        #region WebApp Use it 
        /// <summary>
        /// Post to API with generic input and output ResultModel (For Handles Reponds HTTP Status and Message)
        /// </summary>
        /// <typeparam name="T">Output model in ResultModel</typeparam>
        /// <typeparam name="S">Send Model</typeparam>
        /// <param name="targetApi">target api uri</param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<DataVerifyModel<TOutput>> PostToApiServiceRespondDataModel<TInput, TOutput>(string targetApi, TInput model)
            where TOutput : class, new()
        {
            var result = new DataVerifyModel<TOutput>();
            try
            {
                var response = await HttpClientPostAsync(targetApi, model);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    var serviceResponse = await response.Content.ReadAsAsync<DataVerifyModel<TOutput>>();
                    result = serviceResponse;
                }
                else
                {
                    result.Message = HandlesErrorRespond(response);
                    result.StatusCode = HttpStatusCode.InternalServerError;
                }
                return result;
            }
            catch (Exception ex)
            {
                PostServiceLoger(serviceLogPath, "Service Gateway : Connect by :" + targetApi +" Exception => " + ex.ToString());
                return result;
            }
        }

        public async Task<DataVerifyModel<TOutput>> PostToApiServiceRespondDataModel<TOutput>(string targetApi)
    where TOutput : class, new()
        {
            var result = new DataVerifyModel<TOutput>();
            var response = await HttpClientPostAsync(targetApi, "");
            if (response.IsSuccessStatusCode)
            {
                var serviceResponse = await response.Content.ReadAsAsync<DataVerifyModel<TOutput>>();
                result = serviceResponse;
            }
            else
            {
                result.Message = HandlesErrorRespond(response);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }
            return result;
        }

        /// <summary>
        /// Post to API with json input and out put ResultModel (For Handles Reponds HTTP Status and Message)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetApi"></param>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public async Task<JsonVerifyModel> PostJsonModelToApiServiceRespondJsonModel<T>(string targetApi, T model)
        {
            var result = new JsonVerifyModel();
            var response = await HttpClientPostAsync(targetApi, model);
            if (response.IsSuccessStatusCode)
            {
                var serviceRespond = await response.Content.ReadAsAsync<JsonVerifyModel>();
                result = serviceRespond;
            }
            else
            {
                result.Message = HandlesErrorRespond(response);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }
            return result;
        }

        public async Task<string> PostJsonModelToApiServiceRespondString<T>(string targetApi, T model)
        {
            string result = "";
            var response = await HttpClientPostAsync(targetApi, model);
            if (response.IsSuccessStatusCode)
            {
                var serviceRespond = await response.Content.ReadAsStringAsync();
                result = serviceRespond;
            }
            return result;
        }


        /// <summary>
        /// Post to API with generic input model and void respond. (Use for Log)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetApi"></param>
        /// <param name="model"></param>
        public void PostToApiServiceVoidRespond<T>(string targetApi, T model) where T : class
        {
            try
            {
                using (var client = new NetHttp.HttpClient(new HttpClientHandler() { Credentials = networkCredential }) { BaseAddress = new Uri(webServiceUri) })
                {
                    var inputContent = (typeof(T) == typeof(string)) ? (model as string) : JsonConvert.SerializeObject(model);
                    var content = new StringContent(inputContent, Encoding.UTF8, "application/json");
                    SetTrackingEncode(content);
                    var result = client.PostAsync(targetApi, content).Result;
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion



        #region StampLoger Service Oncall Exception
        /// <summary>
        /// Post to API with generic input model and void respond. (Use for Log)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetApi"></param>
        /// <param name="model"></param>
        private async void PostServiceLoger<T>(string targetApi, T model) where T : class
        {
            try
            {
                using (var client = new NetHttp.HttpClient(new HttpClientHandler() { Credentials = networkCredential }) { BaseAddress = new Uri(webServiceUri) })
                {
                    var inputContent = (typeof(T) == typeof(string)) ? (model as string) : JsonConvert.SerializeObject(model);
                    var content = new StringContent(inputContent, Encoding.UTF8, "application/json");
                    SetTrackingEncode(content);
                    await Task.Run(() =>
                    {
                        return client.PostAsync(targetApi, content).Result;
                    });
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

         
        #region Admin Call
        /// <summary>
        /// Post As FormUrlEncodedContent.
        /// </summary>
        /// <param name="targetApi"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<DataVerifyModel<T>> PostAsFormUrlEncodeContentToApiwithToken<T>(string targetApi, string tokenKey, params KeyValuePair<string, string>[] data) where T : class, new()
        {
            var result = new DataVerifyModel<T>();
            var content = new FormUrlEncodedContent(data.ToList());
            using (var client = new NetHttp.HttpClient(new HttpClientHandler() { Credentials = networkCredential }))
            {
                client.BaseAddress = new Uri(webServiceUri);
                var response = await client.PostAsync(string.Format("{0}?token={1}", targetApi, HttpUtility.UrlEncode(tokenKey)), content);
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<DataVerifyModel<T>>();
                }
                else
                {
                    result.Message = HandlesErrorRespond(response);
                    result.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            return result;
        }

        public async Task<string> PostAsFormUrlEncodeContentToApiwithToken(string targetApi, string tokenKey, params KeyValuePair<string, string>[] data)
        {
            var result = string.Empty;


            var content = new FormUrlEncodedContent(data.ToList());
            using (var client = new NetHttp.HttpClient(new HttpClientHandler() { Credentials = networkCredential }))
            {
                client.BaseAddress = new Uri(webServiceUri);
                var response = await client.PostAsync(string.Format("{0}?token={1}", targetApi, HttpUtility.UrlEncode(tokenKey)), content);
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    HandlesErrorRespond(response);
                }
            }
            return result;

        }
        #endregion


        #region Impersonate

        /// <summary>
        /// Use for Upload files.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="targetApi"></param>
        /// <param name="HttpPostedFilesBase"></param>
        /// <returns></returns>
        public async Task<DataVerifyModel<T>> PostImageToApiServiceRespondDataModel<T>(string targetApi, IList<HttpPostedFileBase> HttpPostedFilesBase)
             where T : class, new()
        {
            var result = new DataVerifyModel<T>();
            try
            {
                using (var content = new MultipartFormDataContent())
                {
                    foreach (var HttpPostedFileBaseItem in HttpPostedFilesBase)
                    {
                        content.Add(CreateFileContent(HttpPostedFileBaseItem.InputStream, HttpPostedFileBaseItem.FileName, HttpPostedFileBaseItem.ContentType));
                    }
                    SetTrackingEncode(content);
                    using (var client = new NetHttp.HttpClient(new HttpClientHandler() { Credentials = networkCredential }))
                    {
                        client.BaseAddress = new Uri(webServiceUri);

                        var response = await client.PostAsync(targetApi, content);
                        if (response.IsSuccessStatusCode)
                        {
                            var serviceResponse = await response.Content.ReadAsAsync<DataVerifyModel<T>>();
                            result = serviceResponse;
                        }
                        else
                        {
                            result.Message = HandlesErrorRespond(response);
                            result.StatusCode = HttpStatusCode.InternalServerError;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var exception = new Exception("Error when call PostImageToApiServiceRespondDataModel", ex);
                throw exception;
            }
            return result;
        }

        private StreamContent CreateFileContent(Stream stream, string fileName, string contentType, string parametername = "files")
        {
            var fileContent = new StreamContent(stream);
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = string.Format("\"{0}\"", parametername),
                FileName = string.Format("\"{0}\"", fileName)
            };
            // the extra quotes are key here
            fileContent.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            return fileContent;
        }
        #endregion


        public void SetAPITrackingModel(APITrackingConfigModel trackingModel)
        {
            this.trackingModel = trackingModel;
        }

        public void setAPITrackingModel(long accessId, string userStamp)
        {
            this.trackingModel = new APITrackingConfigModel() { AccessId = accessId, UserStamp = userStamp, IsLogIncomming = trackingModel.IsLogIncomming };
        }

        /// <summary>
        /// Add HTTP Header
        /// </summary>
        /// <param name="content"></param>
        private void SetTrackingEncode(HttpContent content)
        {
            var tokenString = ConfigurationToken.GenerateConfigurationTokenCode(trackingModel);
            content.Headers.Add(SystemConstant.TrackingHeader, tokenString);
        }
    }
}

