﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace MCSAPP.ServiceGateway
{
    public class WebServiceServer
    {
        public static string ConnectionString()
        {
            try
            {
                string con = string.Empty;
                con = ConfigurationManager.ConnectionStrings["Connection"].ToString();
                return con;
            }
            catch (Exception ex)
            {
               
                throw new Exception(ex.Message + " : ConnectionString() db WebServiceGateway failed!");
            }
        }
    }
}
