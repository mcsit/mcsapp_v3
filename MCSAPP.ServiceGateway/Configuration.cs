﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper;

namespace  MCSAPP.ServiceGateway
{
    public enum AuthenType
    {
         OM =1,
         DB = 2,
         LDAP =3,
         WS = 4,
         COA = 5,
        EXPENSE = 6,
        FILE = 7,
        SAP = 8
    }
    public class Configuration
    {

        public static bool GetLocalAuthen(AuthenType local)
        {
            switch (local)
            {
                case AuthenType.DB :
                    return ConfigurationHelper.GetAppSettting("LocalAuthenDB", true);
                case AuthenType.LDAP:
                    return ConfigurationHelper.GetAppSettting("LocalAuthenLDAP", true);
                case AuthenType.OM:
                    return ConfigurationHelper.GetAppSettting("LocalAuthenOM", true);
                case AuthenType.WS:
                    return ConfigurationHelper.GetAppSettting("LocalAuthen", true);
                case AuthenType.SAP:
                    return ConfigurationHelper.GetAppSettting("SAPUseLocalAuthen", true);
                     
                default:
                    return true;
            }
        }
        public static string GetProjectCode(AuthenType projectCode)
        {
            switch (projectCode)
            {
                case AuthenType.DB:
                    return ConfigurationHelper.GetAppSettting("ProjectCodeDB");
                case AuthenType.LDAP:
                    return ConfigurationHelper.GetAppSettting("LDAPProjectCode");
                case AuthenType.OM:
                    return ConfigurationHelper.GetAppSettting("OMProjectCode");
                case AuthenType.WS:
                    return ConfigurationHelper.GetAppSettting("ProjectCode");
                case AuthenType.COA:
                    return ConfigurationHelper.GetAppSettting("COAProjectCode");
                case AuthenType.FILE:
                    return ConfigurationHelper.GetAppSettting("FileResourceProjectCode");
                case AuthenType.SAP:
                    return ConfigurationHelper.GetAppSettting("SAPProjectCode");
                    
                default:
                    return "";
            }
        }
        public static string GetUserName(AuthenType userty)
        {
            switch (userty)
            {
                case AuthenType.WS:
                    return ConfigurationHelper.GetAppSettting("User_Credential", "abs_0001");
                case AuthenType.SAP:
                    return ConfigurationHelper.GetAppSettting("SAPUsername");
                default:
                    return "";
            }
        }
        public static string GetPassword(AuthenType userty)
        {
            switch (userty)
            {
                case AuthenType.WS:
                    return ConfigurationHelper.GetAppSettting("Password_Credential", "Ais@coth");
                case AuthenType.SAP:
                    return ConfigurationHelper.GetAppSettting("SAPPassword");
                default:
                    return "";
            }
        }
        public static string GetDomain(AuthenType userty)
        {
            switch (userty)
            {
                case AuthenType.WS:
                    return ConfigurationHelper.GetAppSettting("domain_Credential", "corp-ais900");
                default:
                    return "";
            }

        }

        public static string GetUrlBYProjectType(AuthenType projectCode)
        {
            switch (projectCode)
            {
                case AuthenType.DB:
                    return "";
                case AuthenType.LDAP:
                    return "";
                case AuthenType.OM:
                    return "";
                case AuthenType.WS:
                    return "";
                case AuthenType.COA:
                    return "";
                case AuthenType.FILE:
                    return "";
                case AuthenType.SAP:
                    return ConfigurationHelper.GetAppSettting("SAPURL");

                default:
                    return "";
            }
        }
        //ConnectionString
        public static readonly string ConnectionStringDefault = "Data Source=.\\sqlexpress;Initial Catalog=StationeryDb;Integrated Security=True; providerName=System.Data.SqlClient;";
        public static readonly string ConnectionString = ConfigurationHelper.GetAppSettting("ConnectionStringTest", ConnectionStringDefault);

        //MOA System
        public static readonly bool LocalAuthen = ConfigurationHelper.GetAppSettting("LocalAuthen", true);
        public static readonly string ApiServiceUri = ConfigurationHelper.GetAppSettting("ApiServiceUri");
        public static readonly string MOAProjectCode = ConfigurationHelper.GetAppSettting("ProjectCode");// WEB APP TO WEB SERVICE



        
      

        public static readonly int WebServiceTimeOut = ConfigurationHelper.GetAppSettting("WebServiceTimeOut", 3600000);

        //API Trace Incomming Outgoing Log
        public static readonly bool TraceMessageHandler = ConfigurationHelper.GetAppSettting("IsTraceMessageHandlerLog", true);

        //Web to APIs Service
        public static readonly int InternalServiceAPITimeOut = ConfigurationHelper.GetAppSettting("InternalServiceAPITimeOut", 30000);

        //AuthenDb Service
        public static readonly bool LocalAuthenDB = ConfigurationHelper.GetAppSettting("LocalAuthenDB", true);
        public static readonly string ProjectCodeDB = ConfigurationHelper.GetAppSettting("ProjectCodeDB");// Database
        public static readonly string AuthenDBConfigServiceUri = ConfigurationHelper.GetAppSettting("AuthenDBConfigServiceUri");
        public static readonly string UserCredential = ConfigurationHelper.GetAppSettting("User_Credential", "abs_0001");
        public static readonly string PasswordCredential = ConfigurationHelper.GetAppSettting("Password_Credential", "Ais@coth");
        public static readonly string DomainCredential = ConfigurationHelper.GetAppSettting("domain_Credential", "corp-ais900");
        public static readonly string ProjectCodeAuthenDB = ConfigurationHelper.GetAppSettting("ProjectCodeAuthenDB", "MCP2ADTST");

        //OM Service
        public static readonly bool LocalAuthenOM = ConfigurationHelper.GetAppSettting("LocalAuthenOM", true);
        public static readonly string OMServiceUri = ConfigurationHelper.GetAppSettting("OMServiceUri");
        public static readonly string ProjectCodeOM = ConfigurationHelper.GetAppSettting("OMProjectCode");
        public static readonly string OMCode = ConfigurationHelper.GetAppSettting("OMCode");
        public static readonly string OMUsername = ConfigurationHelper.GetAppSettting("OMUsername");
        public static readonly string OMPassword = ConfigurationHelper.GetAppSettting("OMPassword");
        public static readonly string OMDomain = ConfigurationHelper.GetAppSettting("OMDomain");


        //LDAP Service
        public static readonly bool LocalAuthenLDAP = ConfigurationHelper.GetAppSettting("LocalAuthenLDAP", true);
        public static readonly string LDAPServiceUri = ConfigurationHelper.GetAppSettting("LDAPServiceUri");
        public static readonly string LDAPProjectCode = ConfigurationHelper.GetAppSettting("LDAPProjectCode");
        public static readonly string LDAPProjectCodeT = ConfigurationHelper.GetAppSettting("ProjectCode");
        public static readonly string LDAPUsername = ConfigurationHelper.GetAppSettting("LDAPUsername");
        public static readonly string LDAPPassword = ConfigurationHelper.GetAppSettting("LDAPPassword");
        public static readonly string LDAPDomain = ConfigurationHelper.GetAppSettting("LDAPDomain");

        public static readonly string FileResourceUsername = ConfigurationHelper.GetAppSettting("FileResourceUsername");
        public static readonly string FileResourcePassword = ConfigurationHelper.GetAppSettting("FileResourcePassword");
        public static readonly string FileResourceDomain = ConfigurationHelper.GetAppSettting("FileResourceDomain");


        //Autheb User
        public static readonly bool LocalAuthenUser = ConfigurationHelper.GetAppSettting("LocalAuthenUser", true);
        public static readonly string AuthenUser = ConfigurationHelper.GetAppSettting("AuthenUser");
        public static readonly string AuthenPassword = ConfigurationHelper.GetAppSettting("AuthenPassword");
        public static readonly string LDAPProjectIDAuthenticate = ConfigurationHelper.GetAppSettting("LDAPProjectIDAuthenticate");
        public static readonly int? LDAPProjectLevelAuthenticate = ConfigurationHelper.GetAppSettting<int?>("LDAPProjectLevelAuthenticate", null);

        //Mail Server
        public static readonly string MailServer = ConfigurationHelper.GetAppSettting("MailServer");
        public static readonly string MailFromServer = ConfigurationHelper.GetAppSettting("MailFromServer");

        //File Resource
        public static readonly bool UseImpersonate = ConfigurationHelper.GetAppSettting("UseImpersonate", false);
        public static readonly string FileResourceProjectCode = ConfigurationHelper.GetAppSettting("FileResourceProjectCode");
        public static readonly string FileResourceFilePath = ConfigurationHelper.GetAppSettting("FileResourceFilePath");

        //COA
        public static readonly string COAProjectCode = ConfigurationHelper.GetAppSettting("COAProjectCode"); 
        public static readonly string COAURL = ConfigurationHelper.GetAppSettting("COAURL");
        public static readonly string COACredential = ConfigurationHelper.GetAppSettting("COACredential");
        public static readonly bool COAUseCredential = ConfigurationHelper.GetAppSettting("COAUseCredential", true);
        public static readonly bool COAUseLocalAuthen = ConfigurationHelper.GetAppSettting("COAUseLocalAuthen", true);

        //Expense
        public static readonly string projectCodeExpense = ConfigurationHelper.GetAppSettting("projectCodeExpense");
        public static readonly string ExpenseUrl = ConfigurationHelper.GetAppSettting("ExpenseUrl");
        public static readonly string ExpenseCredential = ConfigurationHelper.GetAppSettting("ExpenseCredential");
        public static readonly bool ExpenseLocalAuthen = ConfigurationHelper.GetAppSettting("ExpenseLocalAuthen", true);

        //SAP
      //  public static readonly string SAPProjectCode = ConfigurationHelper.GetAppSettting("SAPProjectCode");
      //  public static readonly string SAPURL = ConfigurationHelper.GetAppSettting("SAPURL");
        public static readonly string SAPClientCertHash = ConfigurationHelper.GetAppSettting("SAPClientCertHash");
        public static readonly string SAPCredential = ConfigurationHelper.GetAppSettting("SAPCredential");
        public static readonly int SAPTimeOut = ConfigurationHelper.GetAppSettting("SAPTimeOut", 25000);
        // public static readonly bool SAPUseCredential = ConfigurationHelper.GetAppSettting("SAPUseCredential", true);
        // public static readonly bool SAPUseLocalAuthen = ConfigurationHelper.GetAppSettting("SAPUseLocalAuthen", true);
    }
}
