﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.ServiceGateway
{
    public class MethodService
    {
        public class LevelService
        {
            public const string UrlPrefix = "api/Level";
            public const string GetLevel = "GetLevel";
            public const string GetLevelNCR = "GetLevelNCR";
            public const string GetLevelNG = "GetLevelNG";
            public const string GetMasterLevelGroup = "GetMasterLevelGroup";
            public const string GetMasterProcess = "GetMasterProcess";
            public const string GetMasterCert = "GetMasterCert";
            public const string GetMasterCertList = "GetMasterCertList";

            public const string SetMasterCert = "SetMasterCert";
            public const string SetMasterGroup = "SetMasterGroup";
            public const string SetMasterLevel = "SetMasterLevel";
            public const string SetMasterLevelNCR = "SetMasterLevelNCR";
            public const string SetMasterLevelNG = "SetMasterLevelNG";
            public const string SetMasterProcess = "SetMasterProcess";

            public const string DeleteMasterCert = "DeleteMasterCert";
            public const string DeleteMasterGroup = "DeleteMasterGroup";
            public const string DeleteMasterLevel = "DeleteMasterLevel";
            public const string DeleteMasterLevelNCR = "DeleteMasterLevelNCR";
            public const string DeleteMasterLevelNG = "DeleteMasterLevelNG";
            public const string DeleteMasterProcess = "DeleteMasterProcess";

            public const string GetPriceByProcess = "GetPriceByProcess";

            #region Built Beam/Box by chaiwud.ta
            // 2018-02-10 by chaiwud.ta 
            public const string GetBuiltLevel = "GetBuiltLevel";
            public const string SetMasterBuiltLevel = "SetMasterBuiltLevel";
            public const string DeleteMasterBuiltLevel = "DeleteMasterBuiltLevel";
            #endregion
        }
        public class WeekService
        {
            public const string UrlPrefix = "api/Report";

            public const string GetGroupName = "GetGroupName";
            public const string GetSearchUser = "GetSearchUser";

            public const string GetWeekByMonth = "GetWeekByMonth";
            public const string GetTxnMonthByWeek = "GetTxnMonthByWeek";
            public const string GetSummaryByWork = "GetSummaryByWork";
            public const string GetSararyMonthByWeek = "GetSararyMonthByWeek";
            public const string GetSummaryWorkByCode = "GetSummaryWorkByCode";
            public const string GetSararyMonthByCode = "GetSararyMonthByCode";
            public const string GetSalaryFab = "GetSalaryFab";

            public const string GetWorkLessGoal = "GetWorkLessGoal";

            public const string GetWorkMonth = "GetWorkMonth";
            public const string GetWorkMonthByDay = "GetWorkMonthByDay";

            public const string GetWorkByDay = "GetWorkByDay";
            public const string GetWorkByWeek = "GetWorkByWeek";

            public const string GetSalaryByDay = "GetSalaryByDay";
            public const string GetSalaryByWeek = "GetSalaryByWeek";

            public const string GetSalaryGroupByDay = "GetSalaryGroupByDay";
            public const string GetSalaryGroupByWeek = "GetSalaryGroupByWeek";

            public const string GetWorkByDayAndCode = "GetWorkByDayAndCode";
            public const string GetSalaryByDayAndCode = "GetSalaryByDayAndCode";

            public const string GetDays = "GetDays";
            public const string GetMaxWeeks = "GetMaxWeeks";
            public const string GetHeaderWeek = "GetHeaderWeek";

            public const string GetReviseByDayAndCode = "GetReviseByDayAndCode";
            public const string GetOtherByDayAndCode = "GetOtherByDayAndCode";


            public const string GetReportNCR = "GetReportNCR";
            public const string GetReportVT = "GetReportVT";

            public const string GetSummaryWorkDayForWeek = "GetSummaryWorkDayForWeek";
            public const string GetSummarySalaryDayForWeek = "GetSummarySalaryDayForWeek";
            public const string GetSummaryWorkDayForWeekLess = "GetSummaryWorkDayForWeekLess";

            public const string GetSummaryWorkDayForWeekWeldLess = "GetSummaryWorkDayForWeekWeldLess";

            public const string GetSummarySalaryMinus = "GetSummarySalaryMinus";
            public const string GetSummarySalaryPlus = "GetSummarySalaryPlus";
            public const string GetSummarySalaryTotal = "GetSummarySalaryTotal";

            public const string GetSummaryWorkDayForWeekWeld = "GetSummaryWorkDayForWeekWeld";
            public const string GetSummarySalaryForWeekWeld = "GetSummarySalaryForWeekWeld";

            public const string GetSummarySalaryForWeekWeldSum = "GetSummarySalaryForWeekWeldSum";
            public const string GetSummarySalaryForWeekSum = "GetSummarySalaryForWeekSum";
        }

        public class UserDepartmentService
        {
            public const string UrlPrefix = "api/UserDepartment";
            public const string GetGroupName = "GetGroupName";
            public const string GetMainType = "GetMainType";
            public const string GetLevelType = "GetLevelType";
            public const string GetMainTypeForProcess = "GetMainTypeForProcess";
            public const string GetHeaderForProcess = "GetHeaderForProcess";

            public const string GetHeaderWeek = "GetHeaderWeek";

            public const string GetProjectForCheckSymbol = "GetProjectForCheckSymbol";
            public const string GetProjectItemForCheckSymbol = "GetProjectItemForCheckSymbol";

            public const string GetProjectForCheckSymbolData = "GetProjectForCheckSymbolData";
            public const string GetProjectItemForCheckSymbolData = "GetProjectItemForCheckSymbolData";
        }
        public class AdminService
        {
            public const string UrlPrefix = "api/Admin";

            public const string WriteLogError = "WriteLogError";

            public const string WriteLogActivity = "WriteLogActivity";

            public const string SetTxnAdjustGoalData = "SetTxnAdjustGoalData";

            public const string SetTxnNcrData = "SetTxnNcrData";

            public const string SetNCRDataLog = "SetNCRDataLog";
            public const string DelNCRDetailLog = "DelNCRDetailLog";

            public const string SetTxnVTData = "SetTxnVTData";

            public const string SetTxnDimensionData = "SetTxnDimensionData";

            public const string SetTxnOtherData = "SetTxnOtherData";

            public const string SetTxnMinusData = "SetTxnMinusData";

            public const string SetTxnWeekData = "SetTxnWeekData";

            public const string SetVTDetail = "SetVTDetail";

            public const string SetVTDetailLog = "SetVTDetailLog";
            public const string SetVTDetailUpload = "SetVTDetailUpload";
            public const string DelVTDetailLog = "DelVTDetailLog";

            public const string GetVTDetailLog = "GetVTDetailLog";
            public const string GetVTDetailAll = "GetVTDetailAll";

            public const string GetReportNCRDetail = "GetReportNCRDetail";

            public const string GetReportNGDetail = "GetReportNGDetail";

            public const string GetNCRLog = "GetNCRLog";

            public const string GetReportVTDetail = "GetReportVTDetail";

            public const string GetWeekLessForAdjust = "GetWeekLessForAdjust";
            public const string GetAdjustGoalData = "GetAdjustGoalData";
            public const string GetCalendar = "GetCalendar";

            //public const string GetReportMinusData = "GetReportMinusData";
            //public const string GetReportOtherData = "GetReportOtherData";

            public const string SetDesingLog = "SetDesingLog";
            public const string SetDesing = "SetDesing";
            public const string DelDesingLog = "DelDesingLog";
            public const string GetDesingLog = "GetDesingLog";

            public const string SetUploadOtherLog = "SetUploadOtherLog";
            public const string SetUploadOther = "SetUploadOther";
            public const string DelUploadOtherLog = "DelUploadOtherLog";
            public const string GetUploadOtherLog = "GetUploadOtherLog";

            public const string SetUploadWeekLog = "SetUploadWeekLog";
            public const string SetUploadWeek = "SetUploadWeek";
            public const string DelUploadWeekLog = "DelUploadWeekLog";
            public const string GetUploadWeekLog = "GetUploadWeekLog";

            public const string DeleteAdjustGoal = "DeleteAdjustGoal";
            public const string DeleteDesingChange = "DeleteDesingChange";
            public const string DeleteDimension = "DeleteDimension";
            public const string DeleteMinus = "DeleteMinus";
            public const string DeleteOther = "DeleteOther";
            public const string DeletePlus = "DeletePlus";
            public const string DeleteVTData = "DeleteVTData";
            public const string DeleteWeek = "DeleteWeek";

            public const string GetReportDesingChangeData = "GetReportDesingChangeData";
            public const string GetReportDimensionData = "GetReportDimensionData";
            public const string GetReportMinusData = "GetReportMinusData";
            public const string GetReportOtherData = "GetReportOtherData";
            public const string GetReportPlusData = "GetReportPlusData";
            public const string GetReportVTData = "GetReportVTData";

            public const string GetReportWeekData = "GetReportWeekData";


            public const string SetUploadNGLog = "SetUploadNGLog";
            public const string SetUploadNG = "SetUploadNG";
            public const string DelUploadNGLog = "DelUploadNGLog";
            public const string GetUploadNGLog = "GetUploadNGLog";
            public const string GetUploadNG = "GetUploadNG";
            public const string DelUploadNG = "DelUploadNG";

            public const string UpdateUploadNG = "UpdateUploadNG";


            public const string SetUploadActUTLog = "SetUploadActUTLog";
            public const string SetUploadActUT = "SetUploadActUT";
            public const string DelUploadActUTLog = "DelUploadActUTLog";
            public const string DelUploadActUT = "DelUploadActUT";
            public const string GetUploadActUTLog = "GetUploadActUTLog";
            public const string GetUploadActUT = "GetUploadActUT";

            public const string SetUploadCutFinisingLog = "SetUploadCutFinisingLog";
            public const string SetUploadCutFinising = "SetUploadCutFinising";
            public const string DelUploadCutFinisingLog = "DelUploadCutFinisingLog";
            public const string DelUploadCutFinising = "DelUploadCutFinising";
            public const string GetUploadCutFinisingLog = "GetUploadCutFinisingLog";
            public const string GetUploadCutFinising = "GetUploadCutFinising";

            public const string SetUploadGoalLog = "SetUploadGoalLog";
            public const string SetUploadGoal = "SetUploadGoal";
            public const string DelUploadGoalLog = "DelUploadGoalLog";
            public const string DelUploadGoal = "DelUploadGoal";
            public const string GetUploadGoalLog = "GetUploadGoalLog";
            public const string GetUploadGoal = "GetUploadGoal";

            public const string SetUploadNGNewLog = "SetUploadNGNewLog";
            public const string SetUploadNGNew = "SetUploadNGNew";
            public const string DelUploadNGNewLog = "DelUploadNGNewLog";
            public const string DelUploadNGNew = "DelUploadNGNew";
            public const string GetUploadNGNewLog = "GetUploadNGNewLog";
            public const string GetUploadNGNew = "GetUploadNGNew";


            public const string SetUploadActUTNewLog = "SetUploadActUTNewLog";
            public const string SetUploadActUTNew = "SetUploadActUTNew";
            public const string DelUploadActUTNewLog = "DelUploadActUTNewLog";
            public const string DelUploadActUTNew = "DelUploadActUTNew";
            public const string GetUploadActUTNewLog = "GetUploadActUTNewLog";
            public const string GetUploadActUTNew = "GetUploadActUTNew";

            public const string GetHeaderExcel = "GetHeaderExcel";

            public const string SetUploadEndTabLog = "SetUploadEndTabLog";
            public const string SetUploadEndTab = "SetUploadEndTab";
            public const string DelUploadEndTabLog = "DelUploadEndTabLog";
            public const string DelUploadEndTab = "DelUploadEndTab";
            public const string GetUploadEndTabLog = "GetUploadEndTabLog";
            public const string GetUploadEndTab = "GetUploadEndTab";

            public const string SetUploadCutPartLog = "SetUploadCutPartLog";
            public const string SetUploadCutPart = "SetUploadCutPart";
            public const string DelUploadCutPartLog = "DelUploadCutPartLog";
            public const string DelUploadCutPart = "DelUploadCutPart";
            public const string GetUploadCutPartLog = "GetUploadCutPartLog";
            public const string GetUploadCutPart = "GetUploadCutPart";

            public const string GetMasterProcessType = "GetMasterProcessType";
            public const string SetMasterProcessType = "SetMasterProcessType";
            public const string DeleteMasterProcessType = "DeleteMasterProcessType";

            public const string SetCheckSymbol = "SetCheckSymbol";

            public const string GetDetailForCheckSymbolData = "GetDetailForCheckSymbolData";
            public const string DelCheckSymbolData = "DelCheckSymbolData";

            #region Built Beam/Box by chaiwud.ta
            // GoalB
            // 2018-02-12 by chaiwud.ta
            public const string SetUploadGoalLogBuilt = "SetUploadGoalLogBuilt";
            public const string SetUploadGoalBuilt = "SetUploadGoalBuilt";
            public const string DelUploadGoalLogBuilt = "DelUploadGoalLogBuilt";
            public const string DelUploadGoalBuilt = "DelUploadGoalBuilt";
            public const string GetUploadGoalLogBuilt = "GetUploadGoalLogBuilt";
            public const string GetUploadGoalBuilt = "GetUploadGoalBuilt";

            // UTRepairCheck
            // 2018-03-01 by chaiwud.ta
            public const string SetUploadUTRepairCheckLogBuilt = "SetUploadUTRepairCheckLogBuilt";
            public const string SetUploadUTRepairCheckBuilt = "SetUploadUTRepairCheckBuilt";
            public const string GetUploadUTRepairCheckLogBuilt = "GetUploadUTRepairCheckLogBuilt";
            public const string DelUploadUTRepairCheckLogBuilt = "DelUploadUTRepairCheckLogBuilt";
            public const string GetUploadUTRepairCheckBuilt = "GetUploadUTRepairCheckBuilt";
            public const string DelUploadUTRepairCheckBuilt = "DelUploadUTRepairCheckBuilt";

            // UTDataByUser 
            // 2018-03-15 by chaiwud.ta
            public const string SetUploadUTDataByUserLogBuilt = "SetUploadUTDataByUserLogBuilt";
            public const string SetUploadUTDataByUserBuilt = "SetUploadUTDataByUserBuilt";
            public const string GetUploadUTDataByUserLogBuilt = "GetUploadUTDataByUserLogBuilt";
            public const string DelUploadUTDataByUserLogBuilt = "DelUploadUTDataByUserLogBuilt";
            public const string GetUploadUTDataByUserBuilt = "GetUploadUTDataByUserBuilt";
            public const string DelUploadUTDataByUserBuilt = "DelUploadUTDataByUserBuilt";

            // NGDataBuilt
            public const string SetUploadNGDataLogBuilt = "SetUploadNGDataLogBuilt";
            public const string SetUploadNGDataBuilt = "SetUploadNGDataBuilt";
            public const string DelUploadNGDataLogBuilt = "DelUploadNGDataLogBuilt";
            public const string DelUploadNGDataBuilt = "DelUploadNGDataBuilt";
            public const string GetUploadNGDataLogBuilt = "GetUploadNGDataLogBuilt";
            public const string GetUploadNGDataBuilt = "GetUploadNGDataBuilt";

            // OtherCamber
            // 2018-04-23 by chaiwud.ta
            public const string SetUploadCamberDataLogOther = "SetUploadCamberDataLogOther";
            public const string SetUploadCamberDataOther = "SetUploadCamberDataOther";
            public const string DelUploadCamberDataLogOther = "DelUploadCamberDataLogOther";
            public const string DelUploadCamberDataOther = "DelUploadCamberDataOther";
            public const string GetUploadCamberDataLogOther = "GetUploadCamberDataLogOther";
            public const string GetUploadCamberDataOther = "GetUploadCamberDataOther";
            #endregion

        }

        public class AccountService
        {
            public const string UrlPrefix = "api/Account";
            public const string GetLogin = "GetLogin";
        }

        public class AppService
        {
            public const string UrlPrefix = "api/App";
            public const string GetPartDetail = "GetPartDetail";
            public const string GetOtherDetail = "GetOtherDetail";
            public const string GetVTDetail = "GetVTDetail";
            public const string GetVTForAdd = "GetVTForAdd";

            public const string GetHeaderExcel = "GetHeaderExcel";
        }

        public class WorkService
        {
            public const string UrlPrefix = "api/Work";
            public const string GetSummaryForWorkByAutoGas1 = "GetSummaryForWorkByAutoGas1";
            public const string GetSummaryForWorkByAutoGas2 = "GetSummaryForWorkByAutoGas2";
            public const string GetSummaryForWorkByAutoGas3 = "GetSummaryForWorkByAutoGas3";
            public const string GetSummaryForWorkByTaper = "GetSummaryForWorkByTaper";
            public const string GetSummaryForWorkByPart = "GetSummaryForWorkByPart";

            public const string GetDetailSummaryForWorkByAutoGas1 = "GetDetailSummaryForWorkByAutoGas1";
            public const string GetDetailSummaryForWorkByAutoGas2 = "GetDetailSummaryForWorkByAutoGas2";
            public const string GetDetailSummaryForWorkByAutoGas3 = "GetDetailSummaryForWorkByAutoGas3";
            public const string GetDetailSummaryForWorkByTaper = "GetDetailSummaryForWorkByTaper";
            public const string GetDetailSummaryForWorkByPart = "GetDetailSummaryForWorkByPart";

            public const string GetDetailForCheckSymbol = "GetDetailForCheckSymbol";

            public const string GetSummaryForSalaryByAutoGas1 = "GetSummaryForSalaryByAutoGas1";
            public const string GetSummaryForSalaryByAutoGas2 = "GetSummaryForSalaryByAutoGas2";
            public const string GetSummaryForSalaryByAutoGas3 = "GetSummaryForSalaryByAutoGas3";
            public const string GetSummaryForSalaryByTaper = "GetSummaryForSalaryByTaper";
            public const string GetSummaryForSalaryByPart = "GetSummaryForSalaryByPart";

            public const string GetSummaryForGrinder = "GetSummaryForGrinder";

            public const string GetSummaryForCut = "GetSummaryForCut";

            #region Built Beam/Box by chaiwud.ta
            // Built Beam
            // 2018-01-26 by chaiwud.ta 
            public const string GetSummaryForWorkByBuiltUpBeam = "GetSummaryForWorkByBuiltUpBeam";
            public const string GetDetailSummaryForWorkByBuiltUpBeam = "GetDetailSummaryForWorkByBuiltUpBeam";
            public const string GetSummaryForSalaryByBuiltUpBeam = "GetSummaryForSalaryByBuiltUpBeam";

            public const string GetSummaryForWorkByBuiltSawBeam = "GetSummaryForWorkByBuiltSawBeam";
            public const string GetDetailSummaryForWorkByBuiltSawBeam = "GetDetailSummaryForWorkByBuiltSawBeam";
            public const string GetSummaryForSalaryByBuiltSawBeam = "GetSummaryForSalaryByBuiltSawBeam";

            public const string GetSummaryForWorkByBuiltAdjustBeam = "GetSummaryForWorkByBuiltAdjustBeam";
            public const string GetDetailSummaryForWorkByBuiltAdjustBeam = "GetDetailSummaryForWorkByBuiltAdjustBeam";
            public const string GetSummaryForSalaryByBuiltAdjustBeam = "GetSummaryForSalaryByBuiltAdjustBeam";

            public const string GetSummaryForWorkByBuiltDrillShotBeam = "GetSummaryForWorkByBuiltDrillShotBeam";
            public const string GetDetailSummaryForWorkByBuiltDrillShotBeam = "GetDetailSummaryForWorkByBuiltDrillShotBeam";
            public const string GetSummaryForSalaryByBuiltDrillShotBeam = "GetSummaryForSalaryByBuiltDrillShotBeam";

            #region Other Beam/Box
            // 2018-05-04 T.Chaiwud #OTHER_BB
            // Other Adjust Beam
            public const string GetSummaryForWorkByOtherBeam = "GetSummaryForWorkByOtherBeam";
            public const string GetDetailSummaryForWorkByOtherBeam = "GetDetailSummaryForWorkByOtherBeam";
            public const string GetSummaryForSalaryByOtherBeam = "GetSummaryForSalaryByOtherBeam";
            // Other Adjust Box
            public const string GetSummaryForWorkByOtherBox = "GetSummaryForWorkByOtherBox";
            public const string GetDetailSummaryForWorkByOtherBox = "GetDetailSummaryForWorkByOtherBox";
            public const string GetSummaryForSalaryByOtherBox = "GetSummaryForSalaryByOtherBox";

            // Other Finishing
            public const string GetSummaryForWorkByOtherFinishing = "GetSummaryForWorkByOtherFinishing";
            public const string GetDetailSummaryForWorkByOtherFinishing = "GetDetailSummaryForWorkByOtherFinishing";
            public const string GetSummaryForSalaryByOtherFinishing = "GetSummaryForSalaryByOtherFinishing";
            #endregion

            // Built Box
            // FabDiaphragmBox
            public const string GetSummaryForWorkByBuiltFabDiaphragmBox = "GetSummaryForWorkByBuiltFabDiaphragmBox";
            public const string GetDetailSummaryForWorkByBuiltFabDiaphragmBox = "GetDetailSummaryForWorkByBuiltFabDiaphragmBox";
            public const string GetSummaryForSalaryByBuiltFabDiaphragmBox = "GetSummaryForSalaryByBuiltFabDiaphragmBox";
            // BuiltUpBox
            public const string GetSummaryForWorkByBuiltUpBox = "GetSummaryForWorkByBuiltUpBox";
            public const string GetDetailSummaryForWorkByBuiltUpBox = "GetDetailSummaryForWorkByBuiltUpBox";
            public const string GetSummaryForSalaryByBuiltUpBox = "GetSummaryForSalaryByBuiltUpBox";
            // AutoRootBox
            public const string GetSummaryForWorkByBuiltAutoRootBox = "GetSummaryForWorkByBuiltAutoRootBox";
            public const string GetDetailSummaryForWorkByBuiltAutoRootBox = "GetDetailSummaryForWorkByBuiltAutoRootBox";
            public const string GetSummaryForSalaryByBuiltAutoRootBox = "GetSummaryForSalaryByBuiltAutoRootBox";
            // DrillSesnetBox
            public const string GetSummaryForWorkByBuiltDrillSesnetBox = "GetSummaryForWorkByBuiltDrillSesnetBox";
            public const string GetDetailSummaryForWorkByBuiltDrillSesnetBox = "GetDetailSummaryForWorkByBuiltDrillSesnetBox";
            public const string GetSummaryForSalaryByBuiltDrillSesnetBox = "GetSummaryForSalaryByBuiltDrillSesnetBox";
            // GougingRepairBox
            public const string GetSummaryForWorkByBuiltGougingRepairBox = "GetSummaryForWorkByBuiltGougingRepairBox";
            public const string GetDetailSummaryForWorkByBuiltGougingRepairBox = "GetDetailSummaryForWorkByBuiltGougingRepairBox";
            public const string GetSummaryForSalaryByBuiltGougingRepairBox = "GetSummaryForSalaryByBuiltGougingRepairBox";
            // FacingBox
            public const string GetSummaryForWorkByBuiltFacingBox = "GetSummaryForWorkByBuiltFacingBox";
            public const string GetDetailSummaryForWorkByBuiltFacingBox = "GetDetailSummaryForWorkByBuiltFacingBox";
            public const string GetSummaryForSalaryByBuiltFacingBox = "GetSummaryForSalaryByBuiltFacingBox";
            #endregion
        }
    }
}
