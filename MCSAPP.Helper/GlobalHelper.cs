﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper.Constant;
using MCSAPP.Helper.Extension;

namespace MCSAPP.Helper
{
    public class GlobalHelper
    {
        /// <summary>
        /// return systemtime.
        /// </summary>
        public static Func<DateTime> SystemTimeNow = () => { return DateTime.Now; };

        public static Func<DateTime, string> ConvertToString = (input) =>
        {
            return input.ToString("dd/MM/yyyy/hh/ss");
        };

        public static DateTime DateTimeParseSpecificFormat(string input, string format = "dd/MM/yyyy/hh/ss")
        {
            var result = DateTime.ParseExact(input, format, CultureInfo.InvariantCulture);
            return result;
        }
        public static string GetDate(DateTime? input)
        {
            if (input == null)
            {
                return string.Empty;
            }
            else
            {
                return ((DateTime)input).ToString("yyyyMMdd", CultureInfo.InvariantCulture);
            }

        }
        public static string GetTime(DateTime? input)
        {
            if (input == null)
            {
                return string.Empty;
            }
            else
            {
                return ((DateTime)input).ToString("hh:mm:ss", CultureInfo.InvariantCulture);
            }

        }
        public static string GetString(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                return Convert.ToString(Object).Trim();
            }
        }

        public static DateTime DateTimeSqlMinSafe(DateTime dateTime)
        {
            var result = (dateTime > SystemConstant.SqlMinDate) ? dateTime : SystemConstant.SqlMinDate;
            return result;
        }

        public static int GetInteger(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return 0;
            }
            else if(Object is int)
            {
                return (int)Object;
            }
            else
            {
                return Convert.ToInt32(Object);
            }
        }

        public static double GetDouble(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(Object);
            }
        }

        public static bool GetBoolean(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return false;
            }
            else
            {
                if (Object.ToString() == "-1" || Object.ToString() == "1" || Object.ToString().ToUpper() == "Y")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public static bool IsNotNull(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                if (value.ToUpper() != "NULL" && value.ToUpper() != "NAN")
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsNotNull(object value)
        {
            if (value != null && value.ToString().ToUpper() != "NULL")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Y: Success, N: Error, E: Unable
        /// </summary>
        /// <param name="httpCode"></param>
        /// <returns></returns>
        public static string CheckHttpStatusToShortString(HttpStatusCode httpCode)
        {
            var result = "N";
            if ((int)httpCode <= 299)
            {
                result = "Y";
            }
            else if ((int)httpCode >= 400 && (int)httpCode <= 499)
            {
                result = "E";
            }
            else
            {
                result = "N";
            }
            return result;
        }
        /// <summary>
        /// Y: Success, N: Error, E: Unable
        /// </summary>
        /// <param name="httpCode"></param>
        /// <returns></returns>
        public static string CheckHttpStatusToShortString(int httpCode)
        {
            var result = "N";
            if ((int)httpCode <= 299)
            {
                result = "Y";
            }
            else if ((int)httpCode >= 400 && (int)httpCode <= 499)
            {
                result = "E";
            }
            else
            {
                result = "N";
            }
            return result;
        }
        /// <summary>
        /// Get Class Fieldvalue (MemberType == MemberTypes.Field) to Dictionary.
        /// MOA.Helper.GlobalHelper.ReflectionFieldvalueToDictionary<MOA.Helper.Configuration>();
        /// Use for track App Config
        /// </summary>
        /// <typeparam name="T">Any class</typeparam>
        /// <param name="tBase">Class</param>
        /// <returns>Dictionary field value.</returns>
        public static Dictionary<string, string> ReflectionFieldvalueToDictionary<T>() where T : class, new()
        {
            var result = new Dictionary<string, string>();
            foreach (MemberInfo memberinfoItem in typeof(T).GetMembers().Where(x => x.MemberType == MemberTypes.Field))
            {
                object value = null;
                var val = (memberinfoItem as FieldInfo).GetValue(value);
                var fieldValue = val ?? "Null";
                result.Add(memberinfoItem.Name, fieldValue.ToString());
            }
            return result;
        }

        [DebuggerStepThrough]
        public static string CreateConnectionFormat(string serverName, string databaseName, string userName, string password, ConnectionStringFormat connectionStringFormat = ConnectionStringFormat.StandardFormat)
        {
            switch (connectionStringFormat)
            {
                case ConnectionStringFormat.EFDatabaseFirstFormat:
                    return CreateConnectionStringEFDatabaseFirstFormat(serverName, databaseName, userName, password);
                case ConnectionStringFormat.StandardFormat:
                default:
                    return CreateConnectionStringStandardFormat(serverName, databaseName, userName, password);
            }
        }

        [DebuggerStepThrough]
        private static string CreateConnectionStringStandardFormat(string serverName, string databaseName, string userName, string password)
        {
            return string.Format(SystemConstant.StandardConnectionStringFormat, serverName, databaseName, userName, password);
        }

        [DebuggerStepThrough]
        private static string CreateConnectionStringEFDatabaseFirstFormat(string serverName, string databaseName, string userName, string password)
        {
            var connection = string.Format(SystemConstant.EFDatabaseFirstConnectionStringFormat, serverName, databaseName, userName, password);
            return connection;
        }

        public static bool CheckRowOnDataSet(DataSet DataSet)
        {
            try
            {
                if (DataSet == null) return false;
                if (DataSet.Tables.Count <= 0) return false;
                if (DataSet.Tables[0].Rows.Count <= 0) return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static bool CheckRowOnDataTable(DataTable DataTable)
        {
            try
            {
                if (DataTable == null) return false;
                if (DataTable.Rows.Count <= 0) return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool CheckRowOnDataRow(DataRow[] _DataRow)
        {
            try
            {
                if (_DataRow == null) return false;
                if (_DataRow.Length <= 0) return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string GetDateTime_ddmmyyyyHHmm(object Object)
        {
            try
            {
                if (DBNull.Value == Object || Object == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToDateTime(Object).ToString("dd/MM/yyyy HH:mm");
                }
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static List<string> SplitNameFormFullName(string fullName)
        {
            var fullNameArr = fullName.Split(' ').ToList();
            if (fullNameArr.Count <= 2)
            {
                return fullNameArr;
            }
            else
            {
                string fName = fullNameArr.First();
                string lName = string.Join(" ", fullNameArr.Skip(1).ToArray());
                return new List<string> { fName, lName };
            }
        }




    }
}
