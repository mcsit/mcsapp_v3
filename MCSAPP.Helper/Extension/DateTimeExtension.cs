﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.Helper.Extension
{
    public static class DateTimeExtension
    {
        public static DateTime DateTimeMinSafe(this DateTime inputDateTime)
        {
            return GlobalHelper.DateTimeSqlMinSafe(inputDateTime);
        }
    }
}
