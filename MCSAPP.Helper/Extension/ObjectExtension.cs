﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.Helper.Extension
{
    public static class ObjectExtension
    {
        public static string ToStringNullSefe(this object input)
        {
            if (input != null)
            {
                return input.ToString();
            }
            return string.Empty;
        }
    }
}
