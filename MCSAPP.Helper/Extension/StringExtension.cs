﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.Helper.Extension
{
    public static class StringExtension
    {
        [DebuggerStepThrough]
        public static string NullSafe(this string stringInput)
        {
            return (stringInput ?? string.Empty).Trim();
        }
    }
}
