﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.Helper.Extension
{
    public static class ExceptionExtension
    {
        public static List<string> GetAllExceptionByRecurSive(this Exception exception)
        {
            var listException = new List<string>();
            Func<Exception, Exception> RecursiveException = null;
            RecursiveException = (ex) =>
            {
                listException.Add(ex.Message);
                if (ex != null && ex.InnerException != null)
                {
                    listException.Add(ex.InnerException.Message);
                    return RecursiveException(ex.InnerException);
                }
                return null;
            };
            RecursiveException(exception);
            return listException;
        }
    }
}
