﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  MCSAPP.Helper.Constant
{
    public struct SystemConstant
    {
        public static DateTime SqlMinDate = SqlDateTime.MinValue.Value;

        public static string THCulture = "th-TH";

        public static string ENCulture = "en-US";

        public static string StandardConnectionStringFormat = "Data Source={0};Initial Catalog={1};UID={2};PWD={3};";

        public static string EFDatabaseFirstConnectionStringFormat = @"metadata=res://*/MOAContext.csdl|res://*/MOAContext.ssdl|res://*/MOAContext.msl;provider=System.Data.SqlClient;provider connection string='Data Source={0};initial catalog={1};MultipleActiveResultSets=true;User ID={2};Password={3}'";

        //public const string EFDatabaseFirstConnectionStringFormat = @"Data Source={0};Initial Catalog={1};user id={2};password={3};Integrated Security=True";
        
        public const string WebApplicationType = " AIS.MCP.UIL.WebApp";

        #region Header
        public const string TrackingHeader = "Tracking";
        public const string UserStampHeader = "St";
        public const string AccessIdHeader = "Ac";
        public const string WriteTraceLog = "Tc";
        #endregion

        #region Application Role
        public const string AdminRole = "admin";
        public const string ManagerManagementRole = "management";
        public const string ManagerRole = "manager";
        public const string marketingRole = "market";
        public const string UserRole = "user";
        #endregion
    }
}
