﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.Helper
{
    public class LogTemplate
    {
        public static string ErrorExceptionTemplate(string message, string methodName, Exception exception)
        {
            return string.Format("{0} \n Methodsname : {1} \n Exception : {2} \n InnerException : {3}", message, methodName, exception.Message, exception.InnerException?.Message);
        }

        public static string ErrorExceptionTemplate(string message, string methodName, string extraData, Exception exception)
        {
            return string.Format("{0} \n Methodsname : {1} \n ExtraData : {2} \n Exception : {3} \n InnerException : {4}", message, methodName, extraData, exception.Message, exception.InnerException?.Message);
        }

        public static string ErrorMessageWithSourceTemplate(Exception ex)
        {
            return string.Format("Message : {0} ;Source : {1};", ex?.Message, ex?.Source);
        }

        public static string WriteHttpClientExceptionSource(string message, string requestUri, long elapsed)
        {
            return string.Format("ExceptionOn : {0} ;Request : {1} ; Elapsed : {2};", message, requestUri, elapsed);
        }
    }
}
