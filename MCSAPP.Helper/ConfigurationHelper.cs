﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.Helper.Extension;

namespace MCSAPP.Helper
{
    public class ConfigurationHelper
    {
        /// <summary>
        /// Get AppConfig String resulr.
        /// </summary>
        /// <param name="key">appconfig key</param>
        /// <param name="defaulValue">default value when not found target value.</param>
        /// <returns>value of config.</returns>
        public static string GetAppSettting(string key, string defaulValue = "")
        {
            var result = defaulValue;
            var val = ConfigurationManager.AppSettings[key].NullSafe();
            if (!string.IsNullOrWhiteSpace(val))
            {
                result = val;
            }
            return result;
        }

        /// <summary>
        /// Get appconfig by specific return type.
        /// </summary>
        /// <typeparam name="T">type of return</typeparam>
        /// <param name="key">appconfig key</param>
        /// <param name="defaulValue">default value when not found target value.</param>
        /// <returns>value of config</returns>
        public static T GetAppSettting<T>(string key, T defaulValue)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter != null)
            {
                var result = defaulValue;
                var val = ConfigurationManager.AppSettings[key].NullSafe();
                if (!string.IsNullOrWhiteSpace(val))
                {
                    result = (T)converter.ConvertFromString(val);
                }
                return result;
            }
            return default(T);
        }
    }
}
