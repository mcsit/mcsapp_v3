﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


namespace MCSAPP.Helper
{
    public class DataConverter
    {
        private static CultureInfo cult = new CultureInfo("en-GB");//("en-AU");en-US|th-TH|en-GB
        public static string CheckType(object Object,string type)
        {
            string result = GetString( Object);
            DateTime d;
            Int32 i;
            Double f;
            if (type.Equals("D")) {
                if (DateTime.TryParse(Object.ToString(), out d)){ result = null; }
            }
            if (type.Equals("I"))
            {
                if (Int32.TryParse(Object.ToString(), out i)) { result = null; }
            }
            if (type.Equals("F"))
            {
                if (Double.TryParse(Object.ToString(), out f)) { result = null; }
            }
            return result;
        }
        public static string GetString(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(Object);
            }
        }
        public static string GetString(string strToCheck)
        {
            if (strToCheck == null || strToCheck == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                return strToCheck;
            }
        }

        public static long GetNumber(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return long.MinValue;
            }
            else
            {
                return Convert.ToInt64(Object);
            }
        }
        public static byte[] GetByte(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                //byte[] b = new byte[0];
                //return b;
                return null;
            }
            else
            {
                return (byte[])Object;
            }
        }
        public static int GetInteger(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return int.MinValue;
            }
            else
            {
                return Convert.ToInt32(Object);
            }
        }
        public static double GetDouble(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return double.MinValue;
            }
            else
            {
                return Convert.ToDouble(Object);
            }
        }
        public static decimal GetDecimal(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return decimal.MinValue;
            }
            else
            {
                if (Object.ToString() == String.Empty)
                    return 0;
                else
                    return Convert.ToDecimal(Object);
            }
        }
        public static bool GetBoolean(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return false;
            }
            else
            {
                //int obj = Convert.ToInt32(Object);
                //if (obj == 1)
                if (Object.ToString() == "-1" || Object.ToString() == "1" || Object.ToString().ToUpper() == "Y" || Object.ToString().ToUpper() == "TRUE")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static TimeSpan GetTimeSpan(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return TimeSpan.MinValue;
            }
            else
            {
                return (TimeSpan)Object;
            }
        }
        public static DateTime GetDateTime(object Object)
        {
            try
            {
                if (DBNull.Value == Object || Object == null || Object.ToString() == string.Empty)
                {
                    return DateTime.MinValue;
                }
                else
                {
                    string ssss = Object.ToString();
                    var sss = Convert.ToDateTime(Object, cult).ToString();
                    return Convert.ToDateTime(Object, cult);
                }
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public static long? GetNumberNull(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                return Convert.ToInt64(Object);
            }
        }
        public static int? GetIntegerNull(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(Object);
            }
        }
        public static double? GetDoubleNull(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                return Convert.ToDouble(Object);
            }
        }
        public static decimal? GetDecimalNull(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                if (Object.ToString() == String.Empty)
                    return 0;
                else
                    return Convert.ToDecimal(Object);
            }
        }
        public static bool? GetBooleanNull(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                //int obj = Convert.ToInt32(Object);
                //if (obj == 1)
                if (Object.ToString() == "1" || Object.ToString().ToUpper() == "Y")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static TimeSpan? GetTimeSpanNull(object Object)
        {
            if (DBNull.Value == Object || Object == null)
            {
                return null;
            }
            else
            {
                return (TimeSpan)Object;
            }
        }
        public static DateTime? GetDateTimeNull(object Object)
        {
            try
            {
                if (DBNull.Value == Object || Object == null)
                {
                    return null;
                }
                else
                {

                    return Convert.ToDateTime(Object, cult);
                }
            }
            catch
            {
                return null;
            }
        }


        public static int Convert2Int(object Object)
        {
            if (Object == null || DBNull.Value == Object) return 0;
            if (Object is string)
                if (((string)Object) == string.Empty) return 0;
            return Convert.ToInt32(Object);
        }
        //public static string GetDate(object Object)
        //{
        //    return GetDate(GetDateTime(Object));
        //}
        //public static DateTime GetDate(string strDate)
        //{
        //    if (strDate == null || strDate == string.Empty)
        //    {
        //        return DateTime.MinValue;
        //    }
        //    else
        //    {
        //        return Convert.ToDateTime(strDate, cult);
        //    }
        //}
        public static DateTime GetDateTimeNow()
        {
            return Convert.ToDateTime(GetDateTime_ddMMyyyyHHmmssfff(DateTime.Now));
        }
        public static DateTime GetDateTimeToday()
        {
            return Convert.ToDateTime(GetDate_ddMMyyyy(DateTime.Today));
        }
        public static string GetDate_ddMMyyyy(DateTime aDate)
        {
            return aDate.ToString("dd/MM/yyyy", cult);
        }
        public static string GetDate_MMyyyy(DateTime aDate)
        {
            return aDate.ToString("MM/yyyy", cult);
        }
        public static string GetDate_yyyMMdd(DateTime aDate)
        {
            return aDate.ToString("yyyy-MM-dd", cult);
        }
        public static string GetDate_ddMMyyyyHHmmss(DateTime aDate)
        {
            return aDate.ToString("ddMMyyyyHHmmss", cult);
        }
        public static string GetDate_ddMMyy(DateTime aDate)
        {
            return aDate.ToString("dd/MM/yy", cult);
        }
        public static string GetDateTime_ddMMyyHHmm(DateTime aDate)
        {
            string strDateTime = aDate.ToString("dd/MM/yy", cult);
            strDateTime = strDateTime + " "
                + String.Format("{0:0#;#0;00}", aDate.Hour) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Minute);
            return strDateTime;
        }
        public static string GetDateTime_ddMMyyyyHHmm(DateTime aDate)
        {
            string strDateTime = aDate.ToString("dd/MM/yyyy", cult);
            strDateTime = strDateTime + " "
                + String.Format("{0:0#;#0;00}", aDate.Hour) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Minute);
            return strDateTime;
        }
        public static string GetDateTime_ddMMyyyyHHmmss(DateTime aDate)
        {
            string strDateTime = aDate.ToString("dd/MM/yyyy", cult);
            strDateTime = strDateTime + " "
                + String.Format("{0:0#;#0;00}", aDate.Hour) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Minute) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Second);
            return strDateTime;
        }
        public static string GetDateTime_ddMMyyyyHHmmssfff(DateTime aDate)
        {
            string strDateTime = aDate.ToString("dd/MM/yyyy", cult);
            strDateTime = strDateTime + " "
                + String.Format("{0:0#;#0;00}", aDate.Hour) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Minute) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Second) + "."
                 + String.Format("{0:0#;#0;00}", aDate.Millisecond);
            return strDateTime;
        }
        public static string GetDateTime_HHmm(DateTime aDate)
        {
            string strDateTime = String.Format("{0:0#;#0;00}", aDate.Hour) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Minute);
            return strDateTime;
        }
        public static string GetDateTime_yyyyMMddTHHmmss(DateTime aDate)
        {
            string strDateTime = aDate.ToString("yyyy-MM-dd", cult);
            strDateTime = strDateTime + "T"
                + String.Format("{0:0#;#0;00}", aDate.Hour) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Minute) + ":"
                + String.Format("{0:0#;#0;00}", aDate.Second);
            return strDateTime;
        }

        public static string GetDateTime_K2_yyyyMMddHHmmss(DateTime aDate)
        {
            string strDateTime = aDate.ToString("yyyyMMdd", cult);
            strDateTime = strDateTime + ""
                + String.Format("{0:0#;#0;00}", aDate.Hour) + ""
                + String.Format("{0:0#;#0;00}", aDate.Minute) + ""
                + String.Format("{0:0#;#0;00}", aDate.Second);
            return strDateTime;
        }
        public static DateTime StringToDateTime(string DateTimeFormatString)
        {
            try
            {
                //DateTimeFormatString : ค่าวันที่ๆ ได้จาก UI "dd/MM/yyyy","dd/MM/yyyy HH:mm"                
                string[] strDate = DateTimeFormatString.Split('/');
                DateTime newDateTime = new DateTime();
                if (strDate.Length >= 3)
                {
                    if (strDate[2].Length.Equals(4))  //มีจำนวนปี ไม่มีเวลาต่อท้าย (yyyy)
                    {
                        if (Convert.ToInt32(strDate[2]) > 2500) // ค.ศ. - 543 (กรณีระบบมอง พ.ศ เป็น ค.ศ แล้ว Auto Convert เพิ่มปี)
                        {
                            strDate[2] = Convert.ToString(Convert.ToInt32(strDate[2]) - 543);
                        }
                        else if (Convert.ToInt32(strDate[2]) < 1500)  // ค.ศ. - 543 (กรณีระบบมอง ค.ศ เป็น พ.ศ แล้ว Auto Convert ลดปี)
                        {
                            strDate[2] = Convert.ToString(Convert.ToInt32(strDate[2]) + 543);
                        }
                        newDateTime = new DateTime(Convert.ToInt32(strDate[2]),
                                            Convert.ToInt32(strDate[1]),
                                            Convert.ToInt32(strDate[0]),
                                            0, 0, 0);
                    }
                    else if (strDate[2].Length > 4)  //มีหน่วยเวลาส่งเข้ามาด้วย
                    {
                        string[] strTime = strDate[2].Substring(4, strDate[2].Length - 4).Trim().Split(':'); //เอาปีออกให้เหลือแต่เวลา
                        string strYear = strDate[2].Substring(0, 4);
                        if (Convert.ToInt32(strYear) > 2500) // ค.ศ. - 543 (กรณีระบบมอง พ.ศ เป็น ค.ศ แล้ว Auto Convert เพิ่มปี)
                        {
                            strYear = Convert.ToString(Convert.ToInt32(strYear) - 543);
                        }
                        else if (Convert.ToInt32(strYear) < 1500)  // ค.ศ. - 543 (กรณีระบบมอง ค.ศ เป็น พ.ศ แล้ว Auto Convert ลดปี)
                        {
                            strYear = Convert.ToString(Convert.ToInt32(strYear) + 543);
                        }
                        if (strTime.Length == 2) //HH:mm
                        {
                            newDateTime = new DateTime(Convert.ToInt32(strYear),
                                            Convert.ToInt32(strDate[1]),
                                            Convert.ToInt32(strDate[0]),
                                            Convert.ToInt32(strTime[0]),
                                            Convert.ToInt32(strTime[1]),
                                            0);
                        }
                        else if (strTime.Length == 3)
                        {
                            strTime[2] = strTime[2].ToUpper().Replace("AM", "");    //กรณีอาจจะมี Type Time AM ติดมา
                            strTime[2] = strTime[2].ToUpper().Replace("PM", "");    //กรณีอาจจะมี Type Time PM ติดมา
                            newDateTime = new DateTime(Convert.ToInt32(strYear),
                                           Convert.ToInt32(strDate[1]),
                                           Convert.ToInt32(strDate[0]),
                                           Convert.ToInt32(strTime[0]),
                                           Convert.ToInt32(strTime[1]),
                                           Convert.ToInt32(strTime[2]));
                        }
                    }
                    return newDateTime;  //dd/MM/yyyy hh:mm:ss
                }
                else { return DateTime.MinValue; } //01/01/0001 00:00:00                   
            }
            catch (Exception ex)
            {
                throw new Exception("ไม่สามารถ Convert String : " + DateTimeFormatString + " ให้อยู่ในรูปแบบวันที่ได้ เนื่องจาก : " + ex.Message);
            }

        }


        public static bool DataIsNumeric(object objData)
        {
            try
            {
                bool isNum;
                double retNum; //กำหนดตัวแปรเพื่อเก็บรวบรวมออกพารามิเตอร์ที่แปลงด้วยวิธีการ TryParse เพราะว่าถ้าการแปลงล้มเหลวพารามิเตอร์จะถูกกำหนดให้เป็นศูนย์               
                //การแปลงนี้ ไม่ว่าสามารถทำได้หรือไม่ isNum จะมีค่าเป็น true หรือ false เสมอ
                isNum = Double.TryParse(Convert.ToString(objData), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
                return isNum;
            }
            catch (Exception)
            {
                return false; //error ใดๆ ให้ return false ทุกกรณี
            }
        }

        private static readonly Regex FormatDateDDMMYYYY = new Regex(@"(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"); //dd/MM/yyyy

        /// <summary>
        /// ตรวจสอบ Format Date DD/MM/YYYY กรณีที่ format ถูกต้อง return true, ไม่ถูกต้องจะ return false;
        /// </summary>
        /// <param name="objValue">DD/MM/YYYY</param>
        /// <returns>true</returns>
        public static bool ParseFormatDateTime(object objValue)
        {
            string strToDate = objValue.ToString().Trim();  //format จะต้องเป็น dd/MM/yyyy : dd ต้องไม่เกิน 31 ,MM ต้องไม่เกิน 12 ,yyyy ไม่ต่ำกว่า 1900 ไม่เกิน 2099 
            if (strToDate.Trim() == "")
            {
                return false; //ไม่มีการส่งค่าเข้ามา
            }
            else
            {
                if (FormatDateDDMMYYYY.Match(strToDate).Success)
                {
                    strToDate = strToDate.Substring(0, 2) + "/" + strToDate.Substring(3, 2) + "/" + strToDate.Substring(6, 4);
                    return true;
                }
                else { return false; } //format ไม่ตรงทุกกรณี          
            }
        }

        ///<value>1000</value>
        ///<returns>1,000.00</returns>
        public static string GetCurrencyFormat(Double Currency)
        {
            string sCurrency = String.Format("{0:#,##0.00;#,##0.00;0.00}", Currency);
            if (Currency < 0) sCurrency = "-" + sCurrency;
            return sCurrency;
        }

        ///<value>1000</value>
        ///<returns>1,000.0000</returns>
        public static string GetCurrency4DigitFormat(Double Currency)
        {
            string sCurrency = String.Format("{0:#,##0.0000;#,##0.0000;0.0000}", Currency);
            if (Currency < 0) sCurrency = "-" + sCurrency;
            return sCurrency;
        }

        ///<value>1000</value>
        ///<returns>1000.0000</returns>
        public static string Get4DigitFormat(Double Currency)
        {
            string sCurrency = String.Format("{0:###0.0000;###0.0000;0.0000}", Currency);
            if (Currency < 0) sCurrency = "-" + sCurrency;
            return sCurrency;
        }

        ///<value>1000.00</value>
        ///<returns>1000</returns>
        public static string NonZeroNumber(Double Number)
        {
            string sCurrency = String.Format("{0:###0;#,##0;0.00}", Number);
            return sCurrency;
        }

        ///<value>1000.00</value>
        ///<returns>1,000</returns>
        public static string GetIntegerFormat(Double Currency)
        {
            string sCurrency = String.Format("{0:#,##0;(#,##0);0}", Currency);
            return sCurrency;
        }

        ///<value>1000,2</value>
        ///<returns>1,000.00</returns>
        public static string GetNumberFormat(object obj, int dec)
        {
            string NumberFormat = "0.00";
            double value = 0.00;
            try
            {

                if (obj != null)
                {
                    value = Convert.ToDouble(obj.ToString().Replace(",", ""));
                    NumberFormat = value.ToString("N" + dec);
                    return NumberFormat;
                }
                else
                {
                    return NumberFormat;
                }
            }
            catch
            {
                return NumberFormat;
            }

        }

        ///<value>abc</value>
        ///<returns>616263</returns>
        public static string ConvertStringToHexa(string StringMsg)
        {
            try
            {
                StringBuilder strb = new StringBuilder();
                char[] chrBuffer = StringMsg.ToCharArray();
                byte[] plainText = System.Text.Encoding.GetEncoding("windows-874").GetBytes(chrBuffer);
                for (int i = 0; i < StringMsg.Length; i++)
                    strb.Append(String.Format("{0:X}", plainText[i]));
                return strb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        ///<value>616263</value>
        ///<returns>abc</returns>
        public static string ConvertHexaToString(string HexaCode)
        {
            try
            {
                Encoding unicd = Encoding.Default;
                byte[] buffer = new byte[HexaCode.Length / 2];
                for (int i = 0; i < HexaCode.Length; i += 2)
                    buffer[i / 2] = (byte)Convert.ToByte(HexaCode.Substring(i, 2), 16);

                return unicd.GetString(buffer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool CheckRowOnDataSet(DataSet DataSet)
        {
            try
            {
                if (DataSet == null) return false;
                if (DataSet.Tables.Count <= 0) return false;
                if (DataSet.Tables[0].Rows.Count <= 0) return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool CheckRowOnDataTable(DataTable DataTable)
        {
            try
            {
                if (DataTable == null) return false;
                if (DataTable.Rows.Count <= 0) return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static System.Data.DataSet ConvertDataTableToDataSet(System.Data.DataTable dt)
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            if (CheckRowOnDataTable(dt) == false) return ds;
            ds.Tables.Add(dt.Copy());
            return ds;
        }

        public static DataTable GetDataTableFromDataSet(DataSet DataSet, string DataTableName = "")
        {
            try
            {
                DataTable dt = null;
                if (DataSet == null) return dt;
                if (DataSet.Tables.Count <= 0) return dt;
                int cRow = DataSet.Tables.Count;
                if (DataTableName.Trim() == "")
                {
                    dt = DataSet.Tables[0];
                }
                else
                {
                    for (int i = 0; i < cRow; i++)
                    {
                        if (DataSet.Tables[i].TableName == DataTableName)
                        {
                            dt = DataSet.Tables[i];
                            break;
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string CleanNull(object obj)
        {
            if (obj == null) return "";
            return obj.ToString();
        }

        public static DataTable ConvertEntityToDataTable(Object[] arrayEntity)
        {
            PropertyInfo[] properties = arrayEntity.GetType().GetElementType().GetProperties();
            DataTable dt = CreateDataTable(properties);
            if (arrayEntity.Length != 0)
            {
                foreach (object obj in arrayEntity)
                    FillData(properties, dt, obj);
            }
            return dt;
        }
        private static DataTable CreateDataTable(PropertyInfo[] properties)
        {
            DataTable dt = new DataTable();
            DataColumn dc = null;
            foreach (PropertyInfo pi in properties)
            {
                dc = new DataColumn();
                dc.ColumnName = pi.Name;
                dc.DataType = Nullable.GetUnderlyingType(pi.PropertyType) ?? pi.PropertyType; //pi.PropertyType;
                dt.Columns.Add(dc);
            }
            return dt;
        }
        private static void FillData(PropertyInfo[] properties, DataTable dt, Object obj)
        {
            DataRow dr = dt.NewRow();
            foreach (PropertyInfo pi in properties)
            {
                dr[pi.Name] = pi.GetValue(obj, null) ?? Convert.DBNull; //pi.GetValue(obj, null);
            }
            dt.Rows.Add(dr);
        }
        public static string SerializeObject<T>(T obj)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(obj.GetType());
                StringWriter stringWriter = new StringWriter();
                xs.Serialize(stringWriter, obj);
                stringWriter.Flush();
                return stringWriter.ToString();
            }
            catch (Exception e) { Console.WriteLine(e); return null; }
        }
    }
}
