﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCSAPP.Helper
{
    public enum ConnectionStringFormat
    {
        StandardFormat = 0,
        EFDatabaseFirstFormat = 1
    }
}
