﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace  MCSAPP.WebServiceAPI.Authentication
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class BasicAuthorizeAPI : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var authorization = actionContext.Request.Headers.Authorization;
            if (authorization != null)
            {
                //TODO Remove (Hardcode user for example)  Z29kZ29kYWRtaW46MXEydzNlNHI1dA==
                var scheme = authorization.Scheme;
                if (scheme == "Basic")
                {
                    var authParameter = authorization.Parameter;
                    byte[] data = Convert.FromBase64String(authParameter);
                    string decodedString = System.Text.Encoding.UTF8.GetString(data);
                    var userData = decodedString.Split(':');
                    var userGodAdmin = "godgodadmin";
                    var userPassword = "1q2w3e4r5t";
                    if (userData.Count() == 2 && userData[0] == userGodAdmin && userData[1] == userPassword)
                    {
                        IPrincipal principal = new GenericPrincipal(new GenericIdentity(userGodAdmin), new string[] { "godadmin" });
                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;
                        return;
                    }
                }
            }
            HandleUnauthorizedRequest(actionContext);
        }
    }
}