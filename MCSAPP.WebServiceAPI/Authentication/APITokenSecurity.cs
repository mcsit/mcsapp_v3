﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using  MCSAPP.Helper;
using  MCSAPP.Security.Cryptographic;

namespace  MCSAPP.WebServiceAPI.Authentication
{
    public class APITokenSecurity
    {
        //Random When need New TokenSecurity every new Instance
        private static readonly string TokenSecurityKey = string.Concat(Guid.NewGuid(), Guid.NewGuid()); //"asdUIopnm1"
        private static readonly string sequenceFormat1 = "1:{2}:{0}:{3}:{1}";
        private static readonly string sequenceFormat2 = "2:{0}:{1}:{2}:{3}";
        private static readonly string sequenceFormat3 = "3:{3}:{2}:{1}:{0}";

        public class TokenModel
        {
            public TokenModel()
            {
                Roles = new List<string>();
                SecurityCode = string.Empty;
            }

            public string Username { get; set; }

            public List<string> Roles { get; set; }

            public DateTime LoginTime { get; set; }

            public string SecurityCode { get; set; }
        }

        public static string GenerateToken(TokenModel model)
        {
            var roles = string.Join(";", model.Roles);
            var loginTime = GlobalHelper.ConvertToString(model.LoginTime);
            var formatTemplate = CreateRandomTemplateFormat();
            var tokenData = string.Format(formatTemplate, model.Username, roles, loginTime, TokenSecurityKey);
            var tokenKey = InternalCryptographic.EncryptData(tokenData);
            return tokenKey;
        }

        private static string CreateRandomTemplateFormat()
        {
            var random = new Random();
            var rad = random.Next(1, 4);
            switch (rad)
            {
                case 1:
                    return sequenceFormat1;
                case 2:
                    return sequenceFormat2;
                case 3:
                default:
                    return sequenceFormat3;
            }
        }

        private static TokenModel GetTokenModelBytokenData(string[] tokenData)
        {
            switch (tokenData[0])
            {
                case "1":
                    return new TokenModel() { Username = tokenData[2], Roles = tokenData[4].Split(';').ToList(), LoginTime = GlobalHelper.DateTimeParseSpecificFormat(tokenData[1]), SecurityCode = tokenData[3] };
                case "2":
                    return new TokenModel() { Username = tokenData[1], Roles = tokenData[2].Split(';').ToList(), LoginTime = GlobalHelper.DateTimeParseSpecificFormat(tokenData[3]), SecurityCode = tokenData[4] };
                case "3":
                default:
                    return new TokenModel() { Username = tokenData[4], Roles = tokenData[3].Split(';').ToList(), LoginTime = GlobalHelper.DateTimeParseSpecificFormat(tokenData[2]), SecurityCode = tokenData[1] };
            }
        }

        public static bool VerifyToken(string tokenKey, out TokenModel model)
        {
            model = null;
            try
            {
                var result = InternalCryptographic.DecryptData(tokenKey);
                var tokenData = result.Split(':');
                if (tokenData.Length == 5)
                {
                    var tokenModel = GetTokenModelBytokenData(tokenData);
                    if (tokenModel.LoginTime <= GlobalHelper.SystemTimeNow().AddDays(1) && tokenModel.SecurityCode == TokenSecurityKey)
                    {
                        model = new TokenModel() { Username = tokenModel.Username, Roles = tokenModel.Roles, LoginTime = tokenModel.LoginTime };
                        return true;
                    }
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        /// <summary>
        /// Get Principal by tokenKey
        /// </summary>
        /// <param name="tokenKey"></param>
        /// <returns>null when token not valid</returns>
        public static IPrincipal CreatePrincipalByTokenKey(string tokenKey)
        {
            IPrincipal principal = null;
            TokenModel model;
            if (VerifyToken(tokenKey, out model))
            {
                principal = new GenericPrincipal(new GenericIdentity(model.Username), model.Roles.ToArray());
            }
            return principal;
        }
    }
}