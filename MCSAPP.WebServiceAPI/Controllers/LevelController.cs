﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Net;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.Linq;
using MCSAPP.ServiceFactory;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceAPI;
using MCSAPP.DAL.Data;
using System.Web.Http.Description;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class LevelController : BaseApiController
    {

        public LevelController()
        {
            var accessId = GetAccessId();
            var userStamp = GetUserStamp();
        }

        [HttpPost]
        [ResponseType(typeof(List<LevelModel>))]
        [Route("api/Level/"+ MethodService.LevelService.GetLevel)]
        public IHttpActionResult GetLevel(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterLevel(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<LevelModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<LevelNCRModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetLevelNCR)]
        public IHttpActionResult GetLevelNCR(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterLevelNCR(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<LevelNCRModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<LevelNGModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetLevelNG)]
        public IHttpActionResult GetLevelNG(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterLevelNG(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<LevelNCRModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<LevelGroupModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetMasterLevelGroup)]
        public IHttpActionResult GetMasterLevelGroup(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterLevelGroup(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<LevelGroupModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<ProcessModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetMasterProcess)]
        public IHttpActionResult GetMasterProcess(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterProcess(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<ProcessModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<CertModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetMasterCert)]
        public IHttpActionResult GetMasterCert()
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterCert();

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<CertModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<CertListModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetMasterCertList)]
        public IHttpActionResult GetMasterCertList()
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterCertList();

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<CertListModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterCert)]
        public IHttpActionResult SetMasterCert(CertModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterCert(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterGroup)]
        public IHttpActionResult SetMasterGroup(LevelGroupModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterGroup(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterProcess)]
        public IHttpActionResult SetMasterProcess(ProcessModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterProcess(model);
                    //   return APIDataModelRespond<ResultModel>();
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK,string.Empty);

                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterLevel)]
        public IHttpActionResult SetMasterLevel(LevelModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterLevel(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterLevelNCR)]
        public IHttpActionResult SetMasterLevelNCR(LevelNCRModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterLevelNCR(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterLevelNG)]
        public IHttpActionResult SetMasterLevelNG(LevelNGModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterLevelNG(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterCert)]
        public IHttpActionResult DeleteMasterCert(CertModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterCert(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterGroup)]
        public IHttpActionResult DeleteMasterGroup(LevelGroupModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterGroup(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterProcess)]
        public IHttpActionResult DeleteMasterProcess(ProcessModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterProcess(model);
                    //   return APIDataModelRespond<ResultModel>();
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);

                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterLevel)]
        public IHttpActionResult DeleteMasterLevel(LevelModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterLevel(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterLevelNCR)]
        public IHttpActionResult DeleteMasterLevelNCR(LevelNCRModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterLevelNCR(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterLevelNG)]
        public IHttpActionResult DeleteMasterLevelNG(LevelNGModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterLevelNG(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(DataFilterModel))]
        [Route("api/Level/" + MethodService.LevelService.GetPriceByProcess)]
        public IHttpActionResult GetPriceByProcess(ProcessModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.GetPriceByProcess(model);
                    //   return APIDataModelRespond<ResultModel>();
                    return APIDataModelRespond(new DataFilterModel() { Value = result.Value }, HttpStatusCode.OK, string.Empty);

                }
               
                catch (Exception ex)
                {
                    return APIDataModelRespond(new DataFilterModel() { Value = "" }, HttpStatusCode.OK, string.Empty);
                }
            }
            return APIDataModelRespond(new DataFilterModel() { Value = "" }, HttpStatusCode.OK, string.Empty);
        }


        // chaiwudd.ta 2018-02-10
        [HttpPost]
        [ResponseType(typeof(List<LevelBuiltModel>))]
        [Route("api/Level/" + MethodService.LevelService.GetBuiltLevel)]
        public IHttpActionResult GetBuiltLevel(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.LevelServiceInstance().GetMasterBuiltLevel(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<LevelBuiltModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.SetMasterBuiltLevel)] // SetMasterBLevel
        public IHttpActionResult SetMasterBuiltLevel(LevelBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.SetMasterBuiltLevel(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Level/" + MethodService.LevelService.DeleteMasterBuiltLevel)]
        public IHttpActionResult DeleteMasterBuiltLevel(LevelBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.LevelServiceInstance();
                    var result = service.DeleteMasterBuiltLevel(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
    }
}
