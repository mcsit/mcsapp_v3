﻿using System;
using MCSAPP.Helper.Enum;

namespace MCS.WebServiceAPI.Controllers
{
    internal class AuthorizeIdentityActionFilterAttribute : Attribute
    {
        private AllowAuthorize anonymous;

        public AuthorizeIdentityActionFilterAttribute(AllowAuthorize anonymous)
        {
            this.anonymous = anonymous;
        }
    }
}