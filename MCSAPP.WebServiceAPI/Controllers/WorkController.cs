﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.Linq;
using MCSAPP.ServiceFactory;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceAPI;
using MCSAPP.DAL.Data;
using System.Web.Http.Description;
using MCSAPP.DAL.Model.Work;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class WorkController : BaseApiController
    {
        public WorkController()
        {
            var accessId = GetAccessId();
            var userStamp = GetUserStamp();
        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByAutoGas1)]
        public IHttpActionResult GetSummaryForWorkByAutoGas1(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByAutoGas1(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByAutoGas2)]
        public IHttpActionResult GetSummaryForWorkByAutoGas2(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByAutoGas2(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByAutoGas3)]
        public IHttpActionResult GetSummaryForWorkByAutoGas3(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByAutoGas3(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByTaper)]
        public IHttpActionResult GetSummaryForWorkByTaper(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByTaper(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByPart)]
        public IHttpActionResult GetSummaryForWorkByPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByPart(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByAutoGas1)]
        public IHttpActionResult GetDetailSummaryForWorkByAutoGas1(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByAutoGas1(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByAutoGas2)]
        public IHttpActionResult GetDetailSummaryForWorkByAutoGas2(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByAutoGas2(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByAutoGas3)]
        public IHttpActionResult GetDetailSummaryForWorkByAutoGas3(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByAutoGas3(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByTaper)]
        public IHttpActionResult GetDetailSummaryForWorkByTaper(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByTaper(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByPart)]
        public IHttpActionResult GetDetailSummaryForWorkByPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByPart(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailForCheckSymbol)]
        public IHttpActionResult GetDetailForCheckSymbol(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailForCheckSymbol(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByAutoGas1)]
        public IHttpActionResult GetSummaryForSalaryByAutoGas1(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByAutoGas1(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByAutoGas2)]
        public IHttpActionResult GetSummaryForSalaryByAutoGas2(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByAutoGas2(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByAutoGas3)]
        public IHttpActionResult GetSummaryForSalaryByAutoGas3(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByAutoGas3(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByTaper)]
        public IHttpActionResult GetSummaryForSalaryByTaper(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByTaper(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByPart)]
        public IHttpActionResult GetSummaryForSalaryByPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByPart(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(AllShowSummaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForGrinder)]
        public IHttpActionResult GetSummaryForGrinder(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForGrinder(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(AllShowSummaryModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForCut)]
        public IHttpActionResult GetSummaryForCut(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForCut(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        #region Built Beam/Box
        // #CBB1 #CBB2

        // Built Beam
        // 2018-01-26 chaiwud.ta

        // Built Up Beam
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltUpBeam)]
        public IHttpActionResult GetSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltUpBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltUpBeam)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltUpBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltUpBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltUpBeam)]
        public IHttpActionResult GetSummaryForSalaryByBuiltUpBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltUpBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        // Saw
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltSawBeam)]
        public IHttpActionResult GetSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltSawBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltSawBeam)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltSawBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltSawBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltSawBeam)]
        public IHttpActionResult GetSummaryForSalaryByBuiltSawBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltSawBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        // Adjust
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltAdjustBeam)]
        public IHttpActionResult GetSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltAdjustBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltAdjustBeam)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltAdjustBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltAdjustBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltAdjustBeam)]
        public IHttpActionResult GetSummaryForSalaryByBuiltAdjustBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltAdjustBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        // Drill & Shot Blast
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltDrillShotBeam)]
        public IHttpActionResult GetSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltDrillShotBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltDrillShotBeam)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltDrillShotBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltDrillShotBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltDrillShotBeam)]
        public IHttpActionResult GetSummaryForSalaryByBuiltDrillShotBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltDrillShotBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }


        #region Other Beam/Box
        // 2018-05-04 T.Chaiwud #OTHER_BB
        // Other Adjust Beam
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByOtherBeam)]
        public IHttpActionResult GetSummaryForWorkByOtherBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByOtherBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByOtherBeam)]
        public IHttpActionResult GetDetailSummaryForWorkByOtherBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByOtherBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByOtherBeam)]
        public IHttpActionResult GetSummaryForSalaryByOtherBeam(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByOtherBeam(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        // Other Adjust Box
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByOtherBox)]
        public IHttpActionResult GetSummaryForWorkByOtherBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByOtherBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByOtherBox)]
        public IHttpActionResult GetDetailSummaryForWorkByOtherBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByOtherBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByOtherBox)]
        public IHttpActionResult GetSummaryForSalaryByOtherBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByOtherBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        // Other Finishing
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByOtherFinishing)]
        public IHttpActionResult GetSummaryForWorkByOtherFishing(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByOtherFinishing(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByOtherFinishing)]
        public IHttpActionResult GetDetailSummaryForWorkByOtherFinishing(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByOtherFinishing(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByOtherFinishing)]
        public IHttpActionResult GetSummaryForSalaryByOtherFinishing(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByOtherFinishing(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        #endregion

        // Built Box

        // FabDiaphragmBox
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltFabDiaphragmBox)]
        public IHttpActionResult GetSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltFabDiaphragmBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltFabDiaphragmBox)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltFabDiaphragmBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltFabDiaphragmBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltFabDiaphragmBox)]
        public IHttpActionResult GetSummaryForSalaryByBuiltFabDiaphragmBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltFabDiaphragmBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        // BuiltUpBox
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltUpBox)]
        public IHttpActionResult GetSummaryForWorkByBuiltUpBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltUpBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltUpBox)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltUpBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltUpBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltUpBox)]
        public IHttpActionResult GetSummaryForSalaryByBuiltUpBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltUpBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        // AutoRootBox
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltAutoRootBox)]
        public IHttpActionResult GetSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltAutoRootBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltAutoRootBox)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltAutoRootBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltAutoRootBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltAutoRootBox)]
        public IHttpActionResult GetSummaryForSalaryByBuiltAutoRootBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltAutoRootBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        // DrillSesnetBox
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltDrillSesnetBox)]
        public IHttpActionResult GetSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltDrillSesnetBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltDrillSesnetBox)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltDrillSesnetBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltDrillSesnetBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltDrillSesnetBox)]
        public IHttpActionResult GetSummaryForSalaryByBuiltDrillSesnetBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltDrillSesnetBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        // GougingRepairBox
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltGougingRepairBox)]
        public IHttpActionResult GetSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltGougingRepairBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltGougingRepairBox)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltGougingRepairBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltGougingRepairBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltGougingRepairBox)]
        public IHttpActionResult GetSummaryForSalaryByBuiltGougingRepairBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltGougingRepairBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        // FacingBox 
        [HttpPost]
        [ResponseType(typeof(AllSummaryForWorkBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForWorkByBuiltFacingBox)]
        public IHttpActionResult GetSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForWorkByBuiltFacingBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(BuiltDetailModel))]
        [Route("api/Work/" + MethodService.WorkService.GetDetailSummaryForWorkByBuiltFacingBox)]
        public IHttpActionResult GetDetailSummaryForWorkByBuiltFacingBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetDetailSummaryForWorkByBuiltFacingBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(AllSummaryForSalaryBuiltModel))]
        [Route("api/Work/" + MethodService.WorkService.GetSummaryForSalaryByBuiltFacingBox)]
        public IHttpActionResult GetSummaryForSalaryByBuiltFacingBox(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WorkServiceInstance().GetSummaryForSalaryByBuiltFacingBox(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        #endregion
    }
}