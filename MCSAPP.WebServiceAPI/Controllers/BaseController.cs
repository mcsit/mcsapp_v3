﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
//using MCSAPP.ServiceAPI.Factory;
using MCSAPP.ServiceGateway;
using MCSAPP.DAL.Model;
using MCSAPP.DAL.Model.ModelInterface;
using MCSAPP.Helper;
using MCSAPP.Helper.Constant;
using MCSAPP.WebServiceAPI.Authentication;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class BaseApiController : ApiController
    {

        /// <summary>
        /// Call When Execute Service
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, System.Threading.CancellationToken cancellationToken)
        {
            VerifyTokenPrincipal(controllerContext);
            //Already Get AccessId
            var configTrack = ReadEncodeAPIConfigulation(controllerContext);
            HttpContext.Current.Items["AccessId"] = configTrack.AccessId;
            HttpContext.Current.Items["UserStamp"] = configTrack.UserStamp;
            try
            {
                return base.ExecuteAsync(controllerContext, cancellationToken);
            }
            catch (HttpResponseException hre)
            {
                var reason = hre?.Response?.ReasonPhrase;
                //var logger = NLoggerFactory.GetCurrentNLoggerAuthenticatedInstance();
               // logger.ErrorException($"Error form request : {controllerContext.Request.RequestUri}, Reason : {reason}, RespondError : {hre.Response.Content.ToString()}", hre);
                throw hre;
            }
        }

        protected void SetInitialUserAccess(long accessId, string userStamp)
        {
            HttpContext.Current.Items["AccessId"] = accessId;
            HttpContext.Current.Items["UserStamp"] = userStamp;
            //var logger = NLoggerFactory.GetCurrentNLoggerAuthenticatedInstance();
           // logger.SetAPITrackingModel(new APITrackingConfigModel() { AccessId = accessId, UserStamp = userStamp });
        }

        protected long GetAccessId()
        {
            var accessId = 0L;
            var currentAccessId = HttpContext.Current.Items["AccessId"];
            if (GlobalHelper.IsNotNull(currentAccessId))
            {
                long.TryParse(currentAccessId.ToString(), out accessId);
            }
            return accessId;
        }

        protected string GetUserStamp()
        {
            var currentUserStamp = HttpContext.Current.Items["UserStamp"];
            if (GlobalHelper.IsNotNull(currentUserStamp))
            {
                return currentUserStamp.ToString();
            }
            return "";
        }

        private APITrackingConfigModel ReadEncodeAPIConfigulation(HttpControllerContext controllerContext)
        {
            var tokenTracking = controllerContext.Request.Headers.FirstOrDefault(x => x.Key == SystemConstant.TrackingHeader);
            if (tokenTracking.Value != null && tokenTracking.Value.Any())
            {
                var trackingHeaderModel = ConfigurationToken.DecodeConfigurationToken(tokenTracking.Value.First());
                //  SetInitialUserAccess(trackingHeaderModel.AccessId, trackingHeaderModel.UserStamp);
                return trackingHeaderModel;
            }
            else
            {
                //TODO Remove this
                return new APITrackingConfigModel();
                //throw new Exception("APITrackingConfigModel Data Not Found");
            }
        }

        /// <summary>
        /// Verify User APIs by Token from Header
        /// </summary>
        /// <param name="controllerContext"></param>
        private void VerifyTokenPrincipal(HttpControllerContext controllerContext)
        {
            var queryString = controllerContext.Request.RequestUri.ParseQueryString();
            var token = queryString["token"];
            if (token != null)  //Token from QueryString
            {
                var principal = APITokenSecurity.CreatePrincipalByTokenKey(token);
                HttpContext.Current.User = principal;
                Thread.CurrentPrincipal = principal;
            }
            else // Token from Header
            {
                var authorization = controllerContext.Request.Headers.Authorization;
                if (authorization != null)
                {
                    var scheme = authorization.Scheme;
                    if (scheme == "Bearer")
                    {
                        var tokenKey = authorization.Parameter;
                        var principal = APITokenSecurity.CreatePrincipalByTokenKey(tokenKey);
                        HttpContext.Current.User = principal;
                        Thread.CurrentPrincipal = principal;
                    }
                }
            }

        }

        /// <summary>
        /// Use for Responds to web APIs.
        /// </summary>
        /// <typeparam name="T">Response Type</typeparam>
        /// <param name="data">Response Model</param>
        /// <param name="statusCode">status VerifyCode</param>
        /// <param name="message">Error Message</param>
        /// <returns>API Responds</returns>
        protected OkNegotiatedContentResult<DataVerifyModel<T>> APIDataModelRespond<T>(T data, HttpStatusCode statusCode = HttpStatusCode.OK, string message = "") where T : class, new()
        {
            var model = new DataVerifyModel<T>() { Data = data, Message = message, StatusCode = statusCode };
            return Ok(model);
        }

        protected OkNegotiatedContentResult<DataVerifyModel<T>> APIDataModelRespond<T>(HttpStatusCode statusCode = HttpStatusCode.OK, string message = "") where T : class, new()
        {
            var model = new DataVerifyModel<T>() { Data = new T(), Message = message, StatusCode = statusCode };
            return Ok(model);
        }

        protected string GetModelStateError()
        {
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return string.Join(",", modelError);
        }

        protected OkNegotiatedContentResult<JsonVerifyModel> APIRepondsModelError()
        {
            var model = new JsonVerifyModel() { Data = "", Message = GetModelStateError(), StatusCode = HttpStatusCode.NotAcceptable };
            return Ok(model);
        }

        /// <summary>
        /// Use for Responds to web APIs.
        /// </summary>
        /// <param name="data">Response JsonModel</param>
        /// <param name="statusCode">status VerifyCode</param>
        /// <param name="message">Error Message</param>
        /// <returns>API Responds</returns>
        protected OkNegotiatedContentResult<JsonVerifyModel> APIDataModelRespond(string data, HttpStatusCode statusCode = HttpStatusCode.OK, string message = "")
        {
            var model = new JsonVerifyModel() { Data = data, Message = message, StatusCode = statusCode };
            return Ok(model);
        }
    }
}
