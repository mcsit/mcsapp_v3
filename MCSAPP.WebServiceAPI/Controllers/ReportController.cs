﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.Linq;
using MCSAPP.ServiceFactory;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceAPI;
using MCSAPP.DAL.Data;
using System.Web.Http.Description;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class ReportController : BaseApiController
    {

        public ReportController()
        {
            var accessId = GetAccessId();
            var userStamp = GetUserStamp();
        }


        [HttpPost]
        [ResponseType(typeof(List<DepartmentModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetGroupName)]
        public IHttpActionResult GetGroupName(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetGroupName(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DepartmentModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UserModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSearchUser)]
        public IHttpActionResult GetSearchUser(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetSearchUser(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DepartmentModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<WeekModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetWeekByMonth)]
        public IHttpActionResult GetWeekByMonth(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWeekByMonth(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<WeekModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<GoalModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetTxnMonthByWeek)]
        public IHttpActionResult GetTxnMonthByWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetTxnMonthByWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<GoalSumModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSummaryByWork)]
        public IHttpActionResult GetSummaryByWork(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummaryByWork(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalSumModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<SararyModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSararyMonthByWeek)]
        public IHttpActionResult GetSararyMonthByWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSararyMonthByWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalSumModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<GoalSumModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSummaryWorkByCode)]
        public IHttpActionResult GetSummaryWorkByCode(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummaryWorkByCode(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalSumModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<SararyModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSararyMonthByCode)]
        public IHttpActionResult GetSararyMonthByCode(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSararyMonthByCode(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalSumModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<SumSalaryModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSalaryFab)]
        public IHttpActionResult GetSalaryFab(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSalaryFab(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalSumModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(DayHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetWorkLessGoal)]
        public IHttpActionResult GetWorkLessGoal(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWorkLessGoal(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<WorkModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetWorkMonth)]
        public IHttpActionResult GetWorkMonth(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWorkMonth(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<WorkModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<WorkModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetWorkMonthByDay)]
        public IHttpActionResult GetWorkMonthByDay(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWorkMonthByDay(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<WorkModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(DayHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetWorkByDay)]
        public IHttpActionResult GetWorkByDay(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWorkByDay(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new DayHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(WeekHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetWorkByWeek)]
        public IHttpActionResult GetWorkByWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWorkByWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new WeekHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(DayHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSalaryByDay)]
        public IHttpActionResult GetSalaryByDay(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSalaryByDay(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new DayHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(WeekHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSalaryByWeek)]
        public IHttpActionResult GetSalaryByWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSalaryByWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new WeekHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(DayHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSalaryGroupByDay)]
        public IHttpActionResult GetSalaryGroupByDay(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSalaryGroupByDay(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new DayHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(WeekHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSalaryGroupByWeek)]
        public IHttpActionResult GetSalaryGroupByWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSalaryGroupByWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new WeekHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(DayHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSalaryByDayAndCode)]
        public IHttpActionResult GetSalaryByDayAndCode(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSalaryByDayAndCode(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new DayHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(DayHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetWorkByDayAndCode)]
        public IHttpActionResult GetWorkByDayAndCode(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetWorkByDayAndCode(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new DayHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReviseModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetReviseByDayAndCode)]
        public IHttpActionResult GetReviseByDayAndCode(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetReviseByDayAndCode(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<ReviseModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReviseModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetOtherByDayAndCode)]
        public IHttpActionResult GetOtherByDayAndCode(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetOtherByDayAndCode(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<ReviseModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(NCRHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetReportNCR)]
        public IHttpActionResult GetReportNCR(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetReportNCR(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new NCRHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(NCRHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetReportVT)]
        public IHttpActionResult GetReportVT(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetReportVT(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new NCRHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(GoalHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummaryWorkDayForWeek)]
        public IHttpActionResult GetSummaryWorkDayForWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummaryWorkDayForWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new GoalHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(DataFilterModel))]
        [Route("api/Report/" + MethodService.WeekService.GetMaxWeeks)]
        public IHttpActionResult GetMaxWeeks(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetMaxWeeks(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new DataFilterModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetHeaderWeek)]
        public IHttpActionResult GetHeaderWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetHeaderWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<SalarySummaryModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryDayForWeek)]
        public IHttpActionResult GetSummarySalaryDayForWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryDayForWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<SalarySummaryModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(GoalHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummaryWorkDayForWeekLess)]
        public IHttpActionResult GetSummaryWorkDayForWeekLess(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummaryWorkDayForWeekLess(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new GoalHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(GoalHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummaryWorkDayForWeekWeldLess)]
        public IHttpActionResult GetSummaryWorkDayForWeekWeldLess(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummaryWorkDayForWeekWeldLess(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new GoalHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<PlusModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryPlus)]
        public IHttpActionResult GetSummarySalaryPlus(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryPlus(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<PlusModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<PlusModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryMinus)]
        public IHttpActionResult GetSummarySalaryMinus(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryMinus(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<PlusModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<PlusModel>))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryTotal)]
        public IHttpActionResult GetSummarySalaryTotal(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryTotal(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<PlusModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(SummaryWorkHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummaryWorkDayForWeekWeld)]
        public IHttpActionResult GetSummaryWorkDayForWeekWeld(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummaryWorkDayForWeekWeld(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new SummaryWorkHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(SummarySalaryHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryForWeekWeld)]
        public IHttpActionResult GetSummarySalaryForWeekWeld(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryForWeekWeld(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new SummaryWorkHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(SummarySalaryHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryForWeekWeldSum)]
        public IHttpActionResult GetSummarySalaryForWeekWeldSum(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryForWeekWeldSum(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new SummaryWorkHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(SummarySalaryHeaderModel))]
        [Route("api/Report/" + MethodService.WeekService.GetSummarySalaryForWeekSum)]
        public IHttpActionResult GetSummarySalaryForWeekSum(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.WeekServiceInstance().GetSummarySalaryForWeekSum(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new SummaryWorkHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        //[HttpPost]
        //[ResponseType(typeof(List<DataFilterModel>))]
        //[Route("api/Report/" + MethodService.WeekService.GetDays)]
        //public IHttpActionResult GetDays(DateModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var service = BusinessServiceFactories.WeekServiceInstance().GetDays(model);

        //        return APIDataModelRespond(service);
        //    }
        //    var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
        //    //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
        //    return APIDataModelRespond(new List<DataFilterModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        //}
    }
}
