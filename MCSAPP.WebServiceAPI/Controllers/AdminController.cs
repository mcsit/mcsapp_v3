﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Net;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.Linq;
using MCSAPP.ServiceFactory;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceAPI;
using MCSAPP.DAL.Data;
using System.Web.Http.Description;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class AdminController : BaseApiController
    {
        public AdminController()
        {
            var accessId = GetAccessId();
            var userStamp = GetUserStamp();
        }

        [HttpPost]
        [ResponseType(typeof(void))]
        [Route("api/Admin/" + MethodService.AdminService.WriteLogError)]
        public void WriteLogError(LogModel model)
        {          
                BusinessServiceFactories.AdminServiceInstance().WriteLogError(model);
        }

        [ResponseType(typeof(void))]
        [Route("api/Admin/" + MethodService.AdminService.WriteLogActivity)]
        public void WriteLogActivity(LogModel model)
        {
            BusinessServiceFactories.AdminServiceInstance().WriteLogActivity(model);
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnAdjustGoalData)]
        public IHttpActionResult SetTxnAdjustGoalData(AdjustGoalModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnAdjustGoalData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnNcrData)]
        public IHttpActionResult SetTxnNcrData(List<NcrDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnNcrData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetNCRDataLog)]
        public IHttpActionResult SetNCRDataLog(List<NcrDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetNCRDataLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelNCRDetailLog)]
        public IHttpActionResult DelNCRDetailLog(List<NcrDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelNCRDetailLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnVTData)]
        public IHttpActionResult SetTxnVTData(List<VTDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnVTData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnDimensionData)]
        public IHttpActionResult SetTxnDimensionData(DimDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnDimensionData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnOtherData)]
        public IHttpActionResult SetTxnOtherData(OtherDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnOtherData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnMinusData)]
        public IHttpActionResult SetTxnMinusData(MinusDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnMinusData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetTxnWeekData)]
        public IHttpActionResult SetTxnWeekData(WeekDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetTxnWeekData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetVTDetail)]
        public IHttpActionResult SetVTDetail(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetVTDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetVTDetailLog)]
        public IHttpActionResult SetVTDetailLog(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetVTDetailLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetVTDetailUpload)]
        public IHttpActionResult SetVTDetailUpload(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetVTDetailUpload(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelVTDetailLog)]
        public IHttpActionResult DelVTDetailLog(List<VTListModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelVTDetailLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

      
        [HttpPost]
        [ResponseType(typeof(List<VTListModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetVTDetailAll)]
        public IHttpActionResult GetVTDetailAll(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetVTDetailLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<VTListModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<VTListModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetVTDetailLog)]
        public IHttpActionResult GetVTDetailLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetVTDetailLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<VTListModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(NCRDetailHeaderModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportNCRDetail)]
        public IHttpActionResult GetReportNCRDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportNCRDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new NCRDetailHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(NGDetailHeaderModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportNGDetail)]
        public IHttpActionResult GetReportNGDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportNGDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new NCRDetailHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<NcrDataModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetNCRLog)]
        public IHttpActionResult GetNCRLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetNCRLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<NcrDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(VTDetailHeaderModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportVTDetail)]
        public IHttpActionResult GetReportVTDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportVTDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new VTDetailHeaderModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(GoalSumModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetWeekLessForAdjust)]
        public IHttpActionResult GetWeekLessForAdjust(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetWeekLessForAdjust(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new GoalSumModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<GoalSumModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetAdjustGoalData)]
        public IHttpActionResult GetAdjustGoalData(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetAdjustGoalData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<GoalSumModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<MonthModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetCalendar)]
        public IHttpActionResult GetCalendar(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetCalendar(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MonthModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        //[HttpPost]
        //[ResponseType(typeof(List<MinusDataModel>))]
        //[Route("api/Admin/" + MethodService.AdminService.GetReportMinusData)]
        //public IHttpActionResult GetReportMinusData(ReportSearchModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var service = BusinessServiceFactories.AdminServiceInstance().GetReportMinusData(model);

        //        return APIDataModelRespond(service);
        //    }
        //    var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
        //    //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
        //    return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        //}

        //[HttpPost]
        //[ResponseType(typeof(List<OtherDataModel>))]
        //[Route("api/Admin/" + MethodService.AdminService.GetReportOtherData)]
        //public IHttpActionResult GetReportOtherData(ReportSearchModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var service = BusinessServiceFactories.AdminServiceInstance().GetReportOtherData(model);

        //        return APIDataModelRespond(service);
        //    }
        //    var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
        //    //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
        //    return APIDataModelRespond(new List<OtherDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        //}

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetDesing)]
        public IHttpActionResult SetDesing(List<DesingModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetDesing(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetDesingLog)]
        public IHttpActionResult SetDesingLog(List<DesingModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetDesingLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelDesingLog)]
        public IHttpActionResult DelDesingLog(List<DesingModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelDesingLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetDesingLog)]
        public IHttpActionResult GetDesingLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetDesingLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadOther)]
        public IHttpActionResult SetUploadOther(List<OtherModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadOtherLog)]
        public IHttpActionResult SetUploadOtherLog(List<OtherModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadOtherLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadOtherLog)]
        public IHttpActionResult DelUploadOtherLog(List<OtherModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadOtherLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadOtherLog)]
        public IHttpActionResult GetUploadOtherLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadOtherLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadWeek)]
        public IHttpActionResult SetUploadWeek(List<UploadWeekModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadWeekLog)]
        public IHttpActionResult SetUploadWeekLog(List<UploadWeekModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadWeekLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadWeekLog)]
        public IHttpActionResult DelUploadWeekLog(List<UploadWeekModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadWeekLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadWeekLog)]
        public IHttpActionResult GetUploadWeekLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadWeekLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        //Delete
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteAdjustGoal)]
        public IHttpActionResult DeleteAdjustGoal(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteAdjustGoal(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteDesingChange)]
        public IHttpActionResult DeleteDesingChange(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteDesingChange(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteDimension)]
        public IHttpActionResult DeleteDimension(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteDimension(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteMinus)]
        public IHttpActionResult DeleteMinus(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteMinus(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteOther)]
        public IHttpActionResult DeleteOther(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeletePlus)]
        public IHttpActionResult DeletePlus(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeletePlus(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteVTData)]
        public IHttpActionResult DeleteVTData(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteVTData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteWeek)]
        public IHttpActionResult DeleteWeek(DeleteModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DeleteWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReportDesignChangeModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportDesingChangeData)]
        public IHttpActionResult GetReportDesingChangeData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportDesingChangeData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReportDimensionModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportDimensionData)]
        public IHttpActionResult GetReportDimensionData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportDimensionData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReportMinusModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportMinusData)]
        public IHttpActionResult GetReportMinusData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportMinusData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReportDimensionModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportOtherData)]
        public IHttpActionResult GetReportOtherData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportOtherData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<ReportDimensionModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportPlusData)]
        public IHttpActionResult GetReportPlusData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportPlusData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<VTListModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportVTData)]
        public IHttpActionResult GetReportVTData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportVTData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<UploadWeekModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetReportWeekData)]
        public IHttpActionResult GetReportWeekData(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetReportWeekData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<MinusDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadNG)]
        public IHttpActionResult SetUploadNG(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadNG(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadNGLog)]
        public IHttpActionResult SetUploadNGLog(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadNGLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadNGLog)]
        public IHttpActionResult DelUploadNGLog(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadNGLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<NGDataModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadNGLog)]
        public IHttpActionResult GetUploadNGLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadNGLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<NGDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<NGDataModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadNG)]
        public IHttpActionResult GetUploadNG(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadNG(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<NGDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadNG)]
        public IHttpActionResult DelUploadNG(NGDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadNG(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.UpdateUploadNG)]
        public IHttpActionResult UpdateUploadNG(NGDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().UpdateUploadNG(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadActUT)]
        public IHttpActionResult SetUploadActUT(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadActUT(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadActUTLog)]
        public IHttpActionResult SetUploadActUTLog(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadActUTLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadActUTLog)]
        public IHttpActionResult DelUploadActUTLog(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadActUTLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadActUT)]
        public IHttpActionResult DelUploadActUT(UploadActUTModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadActUT(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadActUTModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadActUTLog)]
        public IHttpActionResult GetUploadActUTLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadActUTLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<UploadActUTModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadActUT)]
        public IHttpActionResult GetUploadActUT(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadActUT(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadCutFinising)]
        public IHttpActionResult SetUploadCutFinising(List<UploadCutFinisingModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadCutFinising(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadCutFinisingLog)]
        public IHttpActionResult SetUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadCutFinisingLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniCutFinisingIf(modelError.Any(), striCutFinising.Format("ModelStateInvalid {0}", striCutFinising.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadCutFinisingLog)]
        public IHttpActionResult DelUploadCutFinisingLog(List<UploadCutFinisingModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadCutFinisingLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniCutFinisingIf(modelError.Any(), striCutFinising.Format("ModelStateInvalid {0}", striCutFinising.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadCutFinising)]
        public IHttpActionResult DelUploadCutFinising(UploadCutFinisingModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadCutFinising(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadCutFinisingModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadCutFinisingLog)]
        public IHttpActionResult GetUploadCutFinisingLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadCutFinisingLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniCutFinisingIf(modelError.Any(), striCutFinising.Format("ModelStateInvalid {0}", striCutFinising.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadCutFinisingModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<UploadCutFinisingModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadCutFinising)]
        public IHttpActionResult GetUploadCutFinising(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadCutFinising(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadGoal)]
        public IHttpActionResult SetUploadGoal(List<UploadGoalModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadGoal(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadGoalLog)]
        public IHttpActionResult SetUploadGoalLog(List<UploadGoalModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadGoalLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniGoalIf(modelError.Any(), striGoal.Format("ModelStateInvalid {0}", striGoal.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadGoalLog)]
        public IHttpActionResult DelUploadGoalLog(List<UploadGoalModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadGoalLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniGoalIf(modelError.Any(), striGoal.Format("ModelStateInvalid {0}", striGoal.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadGoal)]
        public IHttpActionResult DelUploadGoal(UploadGoalModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadGoal(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadGoalModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadGoalLog)]
        public IHttpActionResult GetUploadGoalLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadGoalLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniGoalIf(modelError.Any(), striGoal.Format("ModelStateInvalid {0}", striGoal.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadGoalModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<UploadGoalModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadGoal)]
        public IHttpActionResult GetUploadGoal(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadGoal(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadNGNew)]
        public IHttpActionResult SetUploadNGNew(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadNGNew(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadNGNewLog)]
        public IHttpActionResult SetUploadNGNewLog(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadNGNewLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadNGNewLog)]
        public IHttpActionResult DelUploadNGNewLog(List<NGDataModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadNGNewLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<NGDataModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadNGNewLog)]
        public IHttpActionResult GetUploadNGNewLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadNGNewLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<NGDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<NGDataModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadNGNew)]
        public IHttpActionResult GetUploadNGNew(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadNGNew(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<NGDataModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadNGNew)]
        public IHttpActionResult DelUploadNGNew(NGDataModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadNGNew(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
      

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadActUTNew)]
        public IHttpActionResult SetUploadActUTNew(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadActUTNew(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadActUTNewLog)]
        public IHttpActionResult SetUploadActUTNewLog(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadActUTNewLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadActUTNewLog)]
        public IHttpActionResult DelUploadActUTNewLog(List<UploadActUTModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadActUTNewLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadActUTNew)]
        public IHttpActionResult DelUploadActUTNew(UploadActUTModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadActUTNew(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadActUTModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadActUTNewLog)]
        public IHttpActionResult GetUploadActUTNewLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadActUTNewLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<UploadActUTModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadActUTNew)]
        public IHttpActionResult GetUploadActUTNew(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadActUTNew(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<UploadTypeModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetHeaderExcel)]
        public IHttpActionResult GetHeaderExcel(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetHeaderExcel(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadTypeModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadEndTab)]
        public IHttpActionResult SetUploadEndTab(List<UploadEndTabModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadEndTab(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadEndTabLog)]
        public IHttpActionResult SetUploadEndTabLog(List<UploadEndTabModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadEndTabLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadEndTabLog)]
        public IHttpActionResult DelUploadEndTabLog(List<UploadEndTabModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadEndTabLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadEndTab)]
        public IHttpActionResult DelUploadEndTab(UploadEndTabModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadEndTab(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadEndTabModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadEndTabLog)]
        public IHttpActionResult GetUploadEndTabLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadEndTabLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadEndTabModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<UploadEndTabModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadEndTab)]
        public IHttpActionResult GetUploadEndTab(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadEndTab(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadEndTabModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        //

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadCutPart)]
        public IHttpActionResult SetUploadCutPart(List<UploadCutPartModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadCutPart(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadCutPartLog)]
        public IHttpActionResult SetUploadCutPartLog(List<UploadCutPartModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadCutPartLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniCutPartIf(modelError.Any(), striCutPart.Format("ModelStateInvalid {0}", striCutPart.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadCutPartLog)]
        public IHttpActionResult DelUploadCutPartLog(List<UploadCutPartModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadCutPartLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniCutPartIf(modelError.Any(), striCutPart.Format("ModelStateInvalid {0}", striCutPart.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadCutPart)]
        public IHttpActionResult DelUploadCutPart(UploadCutPartModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadCutPart(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadCutPartModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadCutPartLog)]
        public IHttpActionResult GetUploadCutPartLog(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadCutPartLog(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniCutPartIf(modelError.Any(), striCutPart.Format("ModelStateInvalid {0}", striCutPart.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadCutPartModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(List<UploadCutPartModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadCutPart)]
        public IHttpActionResult GetUploadCutPart(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadCutPart(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarniActUTIf(modelError.Any(), striActUT.Format("ModelStateInvalid {0}", striActUT.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadCutPartModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }



        [HttpPost]
        [ResponseType(typeof(List<ProcessTypeModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetMasterProcessType)]
        public IHttpActionResult GetMasterProcessType(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetMasterProcessType(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<ProcessTypeModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetMasterProcessType)]
        public IHttpActionResult SetMasterProcessType(ProcessTypeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.AdminServiceInstance();
                    var result = service.SetMasterProcessType(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DeleteMasterProcessType)]
        public IHttpActionResult DeleteMasterProcessType(ProcessTypeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.AdminServiceInstance();
                    var result = service.DeleteMasterProcessType(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetCheckSymbol)]
        public IHttpActionResult SetCheckSymbol(List<PartDatilSummaryForWorkModel> model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.AdminServiceInstance();
                    var result = service.SetCheckSymbol(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }


        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelCheckSymbolData)]
        public IHttpActionResult DelCheckSymbolData(List<DataValueModel> model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var service = BusinessServiceFactories.AdminServiceInstance();
                    var result = service.DelCheckSymbolData(model);
                    return APIDataModelRespond(new ResultModel() { CODE = result.CODE, MESSAGE = result.MESSAGE }, HttpStatusCode.OK, string.Empty);
                }
                catch (InvalidOperationException ioe)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = ioe.Message }, HttpStatusCode.OK, ioe.Message);
                }
                catch (Exception ex)
                {
                    return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "การบันทึกข้อมูลขัดข้อง" }, HttpStatusCode.ExpectationFailed, ex.Message);
                }
            }
            return APIDataModelRespond(new ResultModel() { CODE = "99", MESSAGE = "ข้อมูลไม่ถูกต้อง" }, HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(PartDetailModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetDetailForCheckSymbolData)]
        public IHttpActionResult GetDetailForCheckSymbolData(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetDetailForCheckSymbolData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        #region Built Beam/Box by chaiwud.ta
        // GoalB
        [HttpPost]
        [ResponseType(typeof(List<UploadGoalBuiltModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadGoalLogBuilt)]
        public IHttpActionResult GetUploadGoalLogBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadGoalLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<UploadGoalBuiltModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadGoalBuilt)]
        public IHttpActionResult SetUploadGoalBuilt(List<UploadGoalBuiltModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadGoalBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        // View Goal
        [HttpPost]
        [ResponseType(typeof(List<UploadGoalModel>))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadGoalBuilt)]
        public IHttpActionResult GetUploadGoalBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadGoalBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new List<UploadActUTModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadGoalBuilt)]
        public IHttpActionResult DelUploadGoalBuilt(UploadGoalBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadGoalBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadGoalLogBuilt)]
        public IHttpActionResult DelUploadGoalLogBuilt(List<UploadGoalBuiltModel> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadGoalLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        
        /// UTRepairCheck
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadUTRepairCheckBuilt)]
        public IHttpActionResult SetUploadUTRepairCheckBuilt(List<UTRepairCheckModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadUTRepairCheckBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadUTRepairCheckLogBuilt)]
        public IHttpActionResult SetUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadUTRepairCheckLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadUTRepairCheckLogBuilt)]
        public IHttpActionResult GetUploadUTRepairCheckLogBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadUTRepairCheckLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadUTRepairCheckBuilt)]
        public IHttpActionResult GetUploadUTRepairCheckBuilt(ReportSearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadUTRepairCheckBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadUTRepairCheckLogBuilt)]
        public IHttpActionResult DelUploadUTRepairCheckLogBuilt(List<UTRepairCheckModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadUTRepairCheckLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadUTRepairCheckBuilt)]
        public IHttpActionResult DelUploadUTRepairCheckBuilt(UTRepairCheckModelBuilt model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadUTRepairCheckBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        #region UTDataByUser
        // UTDataByUser
        // Upload
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadUTDataByUserBuilt)]
        public IHttpActionResult SetUploadUTDataByUserBuilt(List<UTDataByUserModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadUTDataByUserBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadUTDataByUserLogBuilt)]
        public IHttpActionResult SetUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadUTDataByUserLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadUTDataByUserLogBuilt)]
        public IHttpActionResult GetUploadUTDataByUserLogBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadUTDataByUserLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadUTDataByUserBuilt)]
        public IHttpActionResult GetUploadUTRepairCheckBuilt(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadUTDataByUserBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadUTDataByUserLogBuilt)]
        public IHttpActionResult DelUploadUTDataByUserLogBuilt(List<UTDataByUserModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadUTDataByUserLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadUTDataByUserBuilt)]
        public IHttpActionResult DelUploadUTDataByUserBuilt(UTDataByUserModelBuilt model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadUTDataByUserBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        #endregion

        // NGDataBuilt
        #region NGDataBuilt
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadNGDataBuilt)]
        public IHttpActionResult SetUploadNGDataBuilt(List<NGDataModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadNGDataBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadNGDataLogBuilt)]
        public IHttpActionResult SetUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadNGDataLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadNGDataLogBuilt)]
        public IHttpActionResult GetUploadNGDataLogBuilt(ReportSearchBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadNGDataLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadNGDataBuilt)]
        public IHttpActionResult GetUploadNGDataBuilt(ReportSearchBuiltModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadNGDataBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadNGDataLogBuilt)]
        public IHttpActionResult DelUploadNGDataLogBuilt(List<NGDataModelBuilt> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadNGDataLogBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadNGDataBuilt)]
        public IHttpActionResult DelUploadNGDataBuilt(NGDataModelBuilt model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadNGDataBuilt(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        #endregion

        #region CamberOther
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadCamberDataOther)]
        public IHttpActionResult SetUploadCamberDataOther(List<CamberDataModelOther> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadCamberDataOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.SetUploadCamberDataLogOther)]
        public IHttpActionResult SetUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().SetUploadCamberDataLogOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadCamberDataLogOther)]
        public IHttpActionResult GetUploadCamberDataLogOther(ReportSearchOtherFinishModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadCamberDataLogOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.GetUploadCamberDataOther)]
        public IHttpActionResult GetUploadCamberDataOther(ReportSearchOtherFinishModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().GetUploadCamberDataOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");
        }

        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadCamberDataLogOther)]
        public IHttpActionResult DelUploadCamberDataLogOther(List<CamberDataModelOther> model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadCamberDataLogOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        [HttpPost]
        [ResponseType(typeof(ResultModel))]
        [Route("api/Admin/" + MethodService.AdminService.DelUploadCamberDataOther)]
        public IHttpActionResult DelUploadCamberDataOther(CamberDataModelOther model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AdminServiceInstance().DelUploadCamberDataOther(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            return APIDataModelRespond(new ResultModel(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
        #endregion
        #endregion

    }
}