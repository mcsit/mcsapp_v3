﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.Linq;
using MCSAPP.ServiceFactory;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceAPI;
using MCSAPP.DAL.Data;
using System.Web.Http.Description;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class AppController : BaseApiController
    {
        public AppController()
        {
            var accessId = GetAccessId();
            var userStamp = GetUserStamp();
        }

        [HttpPost]
        [ResponseType(typeof(List<PartDetailModel>))]
        [Route("api/App/" + MethodService.AppService.GetPartDetail)]
        public IHttpActionResult GetPartDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AppServiceInstance().GetPartDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<PartDetailModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<PartDetailModel>))]
        [Route("api/App/" + MethodService.AppService.GetOtherDetail)]
        public IHttpActionResult GetOtherDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AppServiceInstance().GetOtherDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<PartDetailModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<VTListModel>))]
        [Route("api/App/" + MethodService.AppService.GetVTDetail)]
        public IHttpActionResult GetVTDetail(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AppServiceInstance().GetVTDetail(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<VTListModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<VTListModel>))]
        [Route("api/App/" + MethodService.AppService.GetVTForAdd)]
        public IHttpActionResult GetVTForAdd(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AppServiceInstance().GetVTForAdd(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<VTListModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<UploadTypeModel>))]
        [Route("api/App/" + MethodService.AppService.GetHeaderExcel)]
        public IHttpActionResult GetHeaderExcel(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.AppServiceInstance().GetHeaderExcel(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<UploadTypeModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
    }
}