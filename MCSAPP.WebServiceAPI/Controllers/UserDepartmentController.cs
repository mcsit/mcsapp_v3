﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;
using System.Linq;
using MCSAPP.ServiceFactory;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceAPI;
using MCSAPP.DAL.Data;
using System.Web.Http.Description;

namespace MCSAPP.WebServiceAPI.Controllers
{
    public class UserDepartmentController : BaseApiController
    {
        // GET: UserDepartment
        public UserDepartmentController()
        {
            var accessId = GetAccessId();
            var userStamp = GetUserStamp();
        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetMainType)]
        public IHttpActionResult GetMainType(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetMainType(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetLevelType)]
        public IHttpActionResult GetLevelType(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetLevelType(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetMainTypeForProcess)]
        public IHttpActionResult GetMainTypeForProcess(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetMainTypeForProcess(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetHeaderForProcess)]
        public IHttpActionResult GetHeaderForProcess(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetHeaderForProcess(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetHeaderWeek)]
        public IHttpActionResult GetHeaderWeek(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetHeaderWeek(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DepartmentModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetGroupName)]
        public IHttpActionResult GetGroupName(SearchModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetGroupName(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetProjectForCheckSymbol)]
        public IHttpActionResult GetProjectForCheckSymbol(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetProjectForCheckSymbol(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetProjectItemForCheckSymbol)]
        public IHttpActionResult GetProjectItemForCheckSymbol(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetProjectItemForCheckSymbol(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }

        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetProjectForCheckSymbolData)]
        public IHttpActionResult GetProjectForCheckSymbolData(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetProjectForCheckSymbolData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }


        [HttpPost]
        [ResponseType(typeof(List<DataValueModel>))]
        [Route("api/UserDepartment/" + MethodService.UserDepartmentService.GetProjectItemForCheckSymbolData)]
        public IHttpActionResult GetProjectItemForCheckSymbolData(DateModel model)
        {
            if (ModelState.IsValid)
            {
                var service = BusinessServiceFactories.UserDepartmentServiceInstance().GetProjectItemForCheckSymbolData(model);

                return APIDataModelRespond(service);
            }
            var modelError = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            //logger.WarningIf(modelError.Any(), string.Format("ModelStateInvalid {0}", string.Join(",", modelError)), "CostCenterController.GetAllCostCenterByReqCode");
            return APIDataModelRespond(new List<DataValueModel>(), System.Net.HttpStatusCode.ExpectationFailed, "Model State invalid.");

        }
    }
}