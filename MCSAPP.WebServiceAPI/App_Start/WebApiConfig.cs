﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using MCSAPP.DAL.Model;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace MCSAPP.WebServiceAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Web API Format
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.Add(config.Formatters.JsonFormatter);

            //RegisMessageLoggingHandler
            //config.MessageHandlers.Add(new MessageLoggingHandler());

            ////Regis Fileformatter
            //config.Formatters.Add(new FileMediaFormatter());

            ////Regis ExceptionHandler
            //config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());
        }
    }
}
