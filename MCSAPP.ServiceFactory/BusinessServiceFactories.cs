﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MCSAPP.DAL.Data.InterfaceData;

using MCSAPP.IServiceAPI;
using MCSAPP.ServiceAPI;


namespace MCSAPP.ServiceFactory
{
    public class BusinessServiceFactories
    {
        public static IAdminService AdminServiceInstance()
        {
            return new AdminService();
        }
        public static IAppService AppServiceInstance()
        {
            return new AppService();
        }
        public static ILevelService LevelServiceInstance()
        {   
            return new LevelService();
        }
        public static IWeekService WeekServiceInstance()
        {
            return new WeekService();
        }
        public static IUserDepartmentService UserDepartmentServiceInstance()
        {
            return new UserDepartmentService();
        }
        public static IWorkService WorkServiceInstance()
        {
            return new WorkService();
        }
    }
}
