﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceAPI;
using MCSAPP.ServiceGateway;
using MCSAPP.DAL.Model;
using MCSAPP.Helper;

namespace  MCSAPP.WebFactory
{
    public class ServiceLoggerFactory
    {
        private static INLogger logger;
        public static INLogger GetCurrentLogServiceInstance(APITrackingConfigModel trackingModel = null)
        {
            if (logger == null)
            {
                NewLogServiceInstance(trackingModel);
            }
            return logger;
        }

        /// <summary>
        /// Use for first Web Request Only.
        /// </summary>
        public static void NewLogServiceInstance(APITrackingConfigModel trackingModel = null)
        {
            //var httpClient = HttpClientFactory.HttpClientAuthenticate(trackingModel);
            //logger = new ServiceLogger(httpClient);
            HttpClientFactory.NewHttpClientAuthenticateInstance(trackingModel);
            var httpClient = HttpClientFactory.HttpClientAuthenticate(trackingModel);
            logger = new ServiceLogger(httpClient);
        }
    }
}
