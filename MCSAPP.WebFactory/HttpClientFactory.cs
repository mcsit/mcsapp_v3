﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using MCSAPP.IServiceAPI;
using MCSAPP.ServiceGateway;
using MCSAPP.DAL.Model;


namespace  MCSAPP.WebFactory
{
    public class HttpClientFactory
    {
        private static IHttpClientServiceAPI httpClientServiceAPI;

        public static IHttpClientServiceAPI HttpClientAuthenticate(APITrackingConfigModel trackingModel = null)
        {
            if (httpClientServiceAPI == null)
            {
                NewHttpClientAuthenticateInstance(trackingModel ?? new APITrackingConfigModel() { WriteTraceLog = true });
            }
            return httpClientServiceAPI;
        }

        public static void NewHttpClientAuthenticateInstance(APITrackingConfigModel trackingModel = null)
        {

            var netWorkCredential = CreateServiceCredential();
            httpClientServiceAPI = new HttpClientServiceAPI(netWorkCredential, trackingModel);
        }
        public static object GetCredential()
        { 
            var neWorkCredential = CreateServiceCredential();
            return neWorkCredential;
        }

        public static object GetCredentialTest(AuthenType projectCode)
        {
            var neWorkCredential = CreateServiceCredential();
            return neWorkCredential;
        }

        public static NetworkCredential CreateServiceCredential()
        {
            var localAuthen = true;
            if (localAuthen)
            {
                //var userAccount = GetUserAccountByProjectCode(projectType, projectCodeStr);
                //if (userAccount != null)
                //{
                var userName = "";
                var password = "";
                var domainName = "";

                return new NetworkCredential(userName, password, domainName);
                //}
                //var logMessage = string.Format("Cannot Login Authenb {0} (UserAccount is null) on MethodName: CreateServiceCredential", "CreateServiceCredential", strProject);
                //throw new Exception(logMessage);
            }
            else
            {
                return new NetworkCredential("", "", "");
            }
        }

    }
}
