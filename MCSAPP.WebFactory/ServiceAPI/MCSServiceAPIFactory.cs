﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MCSAPP.ServiceGateway;
using MCSAPP.ServiceGateway.ServiceWrapper;
using MCSAPP.DAL.Model;
using MCSAPP.WebFactory;
using MCSAPP.IServiceAPI;
using MCSAPP.ServiceGateway.InternalServiceGateway.ServiceWrapper;

namespace  MCSAPP.ServiceAPIFactory.ServiceAPI
{
    public class MCSServiceAPIFactory
    {
        //public static object getCre(string projectCode)
        //{
        //    return HttpClientFactory.GetCredentialTest(projectCode);
        //}
        public static IAccountServiceWrapper NewAccountServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);

            var instance = new AccountServiceWrapper(httpClientServiceAPI);
            return instance;
        }
        public static IAdminServiceWrapper NewAdminServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);

            var instance = new AdminServiceWrapper(httpClientServiceAPI);
            return instance;
        }
        public static IAppServiceWrapper NewAppServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);

            var instance = new AppServiceWrapper(httpClientServiceAPI);
            return instance;
        }
        public static ILevelServiceWrapper NewLevelServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);
          
            var instance = new LevelServiceWrapper(httpClientServiceAPI);
            return instance;
        }
        public static IWeekServiceWrapper NewWeekServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);

            var instance = new WeekServiceWrapper(httpClientServiceAPI);
            return instance;
        }
        public static IUserDepartmentServiceWrapper NewUserDepartmentServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);

            var instance = new UserDepartmentServiceWrapper(httpClientServiceAPI);
            return instance;
        }

        public static IWorkServiceWrapper NewWorkServiceWrapperInstance(APITrackingConfigModel trackingModel = null)
        {
            var httpClientServiceAPI = HttpClientFactory.HttpClientAuthenticate(trackingModel);

            var instance = new WorkServiceWrapper(httpClientServiceAPI);
            return instance;
        }
    }

}
